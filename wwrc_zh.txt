<resources>
    <!--App Name-->
    <string name="app_name">wcircle_pro</string>

    <!--Home Layout String-->
    <string name="version_msg">新的版本可用。请更新在谷歌Play商店。</string>
    <string name="maintenance_msg">对不起，服务器正在维护。 请稍后再试。</string>
    <string name="maintenance">维护中！</string>
    <string name="update">请更新！</string>

    <!--Dashboard Layout String-->
    <string name="welcome">欢迎来到</string>
    <string name="wcircle_pro">wcircle_pro 手机联网</string>

    <!--Calendar Layout String-->
    <string name="search">搜索</string>

    <!--Login Layout String-->
    <string name="email_example">email@wcircle_pro.com.my</string>
    <string name="forget_password">忘了密码？</string>
    <string name="login1">登录</string>
    <string name="sign_up1">注册</string>
    <string name="powered_applab">Powered by App Lab Sdn Bhd</string>
    <string name="login_account">登录你的帐户</string>

    <!--Forget Password Layout String-->
    <string name="forget_password1">忘了密码</string>
    <string name="submit1">提交</string>
    <string name="cancel1">取消</string>
    <string name="fg_message">为了通过电子邮件接收您的访问代码，请输入您在注册过程中提供的地址。</string>

    <!--Sign Up Layout String-->
    <string name="email">电子邮件</string>
    <string name="password">密码</string>
    <string name="confirm_password">确认密码</string>
    <string name="current_password">当前密码</string>
    <string name="new_password">新密码</string>
    <string name="submit">提交</string>
    <string name="cancel">取消</string>

    <!--New Calendar Layout String-->
    <string name="title">标题</string>
    <string name="start_date">开始日期</string>
    <string name="start_time">开始时间</string>
    <string name="end_date">结束日期</string>
    <string name="end_time">时间结束</string>
    <string name="add_location">添加地点</string>
    <string name="add_description">添加描述</string>
    <string name="all_day">一整天</string>
    <string name="invite_attendees">邀请参加者</string>
    <string name="invited_attendees">受邀的参加者</string>

    <!--Select Attendees Layout String-->
    <string name="done">完成</string>
    <string name="selected_attendees">选定的参加者</string>

    <!--Messenger Layout String-->
    <string name="group_msg">请提供组主题和可选的组图标。</string>
    <string name="group_msg1">在这里输入组主题...</string>

    <!--Profile Layout String-->
    <string name="email1">电子邮件：</string>
    <string name="hp">手提电话号码：</string>
    <string name="hp1">手提电话号码</string>
    <string name="contact">公司电话号码：</string>
    <string name="hod">部门主管：</string>
    <string name="employee_no">员工号码：</string>
    <string name="dob">出生日期：</string>
    <string name="profile">简历</string>

    <!--Leave Layout String-->
    <string name="name">姓名：</string>
    <string name="department">部门：</string>
    <string name="position">职位：</string>
    <string name="report_to">通知：</string>
    <string name="type_of_leave">假期类型：</string>
    <string name="reason">原因：</string>
    <string name="no_of_days">天数：</string>
    <string name="to_date">结束日期：</string>
    <string name="from_date">开始日期：</string>
    <string name="apply_on">申请日期：</string>
    <string name="status">状况：</string>
    <string name="remarks">备注：</string>
    <string name="edit">编辑</string>
    <string name="cancel_leave">取消休假</string>
    <string name="al">年假</string>
    <string name="filter">筛选：</string>

    <!--Media Gallery Layout String-->
    <string name="new1">新</string>
    <string name="save">保存</string>


    <!--News Details Layout String-->
    <string name="by">由 </string>
    <string name="on"> 在 </string>

    <!--Calendar Event Layout String-->
    <string name="cancelled">取消</string>
    <string name="title1">标题：</string>
    <string name="all_day1">一整天：</string>
    <string name="location1">位置：</string>
    <string name="attendees">参加者：</string>


    <!--window accept reject calendar Layout String-->
    <string name="from">从 ：</string>
    <string name="to">至 ：</string>
    <string name="location">地点 ：</string>
    <string name="created_by">申请者 ：</string>
    <string name="description">描述 ： </string>
    <string name="attend">参加</string>
    <string name="reject">拒绝</string>
    <string name="later">之后</string>
    <string name="acknowledge">确认</string>

    <!--window accept reject employee leave Layout String-->
    <string name="apply_by">申请者：</string>
    <string name="type">类型：</string>
    <string name="total_days">总天数：</string>
    <string name="reason1">原因 ： </string>
    <string name="approve">批准</string>

    <!--window action Layout String-->
    <string name="action">操作</string>
    <string name="download">下载</string>
    <string name="rename">重命名</string>
    <string name="remove">删除</string>

    <!--window calendar event details Layout String-->
    <string name="calendar_event_msg">你真的要删除这和它的内容吗？</string>
    <string name="yes">是</string>
    <string name="no">不是</string>
    <string name="attention">注意</string>
    <string name="delete">删除</string>
    <string name="reject_msg">你想从被邀请者列表中删除您的名字？</string>

    <!--window call Layout String-->
    <string name="call">通话</string>
    <string name="mobile">手提电话</string>
    <string name="office_tel">公司电话</string>

    <!--window catalogue Layout String-->
    <string name="details">详细信息：</string>
    <string name="file_size">文件大小：</string>
    <string name="uploaded_at">上传时间：</string>
    <string name="uploaded_by">上传者：</string>

    <!--window drive Layout String-->
    <string name="file_name">文件名：</string>
    <string name="modified_at">修改时间：</string>

    <!--window error Layout String-->
    <string name="ok">好</string>
    <string name="error">错误</string>

    <!--window folder Layout String-->
    <string name="create_folder">创建文件夹</string>
    <string name="folder_name1">文件夹名称</string>
    <string name="name1">名称</string>

    <!--window leave reason Layout String-->
    <string name="remarks1">备注</string>
    <string name="reason2">原因</string>

    <!--window my leave Layout String-->
    <string name="my_leave">我的假期</string>
    <string name="reported_to">通知：</string>
    <string name="status1">状况：</string>
    <string name="remarks2">备注：</string>

    <!--window leave reason Layout String-->
    <string name="upload_image">上传图片</string>
    <string name="gallery1">媒体库</string>
    <string name="camera">相机</string>


    <!-- My Drive-->
    <string name="action_folder">创建文件夹</string>
    <string name="action_refresh">刷新</string>
    <string name="action_upload">上传</string>

    <string-array name="month">
        <item>一月</item>
        <item>二月</item>
        <item>三月</item>
        <item>四月</item>
        <item>五月</item>
        <item>六月</item>
        <item>七月</item>
        <item>八月</item>
        <item>九月</item>
        <item>十月</item>
        <item>十一月</item>
        <item>十二月</item>
    </string-array>

    <!--Tabs-->
    <string-array name="tabs">
        <item>TAB 1</item>
        <item>TAB 2</item>
        <item>TAB 3</item>
        <item>TAB 4</item>
        <item>TAB 5</item>
        <item>TAB 6</item>
    </string-array>
    <string-array name="leave_filter">
        <item>未读</item>
        <item>所有</item>
        <item>待定</item>
        <item>批准</item>
        <item>拒绝</item>
        <item>取消</item>
    </string-array>
    <string-array name="calendar_tabs">
        <item>时间表</item>
        <item>月份</item>
    </string-array>
    <string-array name="chat_tabs">
        <item>聊天</item>
        <item>小组</item>
    </string-array>
    <string-array name="leave_tabs">
        <item>我的假期</item>
        <item>员工假期</item>
    </string-array>
    <string-array name="employee_tabs">
        <item>最近</item>
        <item>办公室</item>
        <item>其他</item>
    </string-array>
    <string-array name="add_contact_tabs">
        <item>我的最爱</item>
        <item>办公室</item>
        <item>其他</item>
    </string-array>

    <!--Calendar-->
    <string name="monday">星期一</string>
    <string name="tuesday">星期二</string>
    <string name="wednesday">星期三</string>
    <string name="thursday">星期四</string>
    <string name="friday">星期五</string>
    <string name="saturday">星期六</string>
    <string name="sunday">星期日</string>

    <!--Menu-->
    <string-array name="Menu_Items">
        <item>My Dashboard</item>
        <item>My Favourites</item>
        <item>Calendar</item>
        <item>News</item>
        <item>Event</item>
        <item>Messenger</item>
        <item>Staff Directory</item>
        <item>Leave</item>
        <item>Download Centre</item>
        <item>Media Gallery</item>
        <item>Product Catalogue</item>
        <item>My Drive</item>
        <item>My Profile</item>
        <item>Log Out</item>
    </string-array>

    <!--Others-->
    <string name="search_hint">Search</string>
    <string name="lblString">Mobile wcircle_pro is currently down for maintenance.
        We expect to be back in a couple hours. Thanks for your patience.</string>
    <string name="no_network_connection_toast">No network connection found</string>
    <string name="action_settings">Settings</string>
    <string name="action_clear_employee">清除最近</string>
    <string name="action_new_chat">新的聊天室</string>
    <string name="action_create_group">创建组</string>
    <string name="action_gallery">画</string>
    <string name="action_add_people">添加/删除成员</string>
    <string name="action_group_info">小组资料</string>
    <string name="action_close_group">关闭小组</string>
    <string name="action_exit_group">退出小组</string>
    <string name="action_delete_group">删除小组</string>
    <string name="action_camera">相机</string>
    <string name="action_menu">功能表</string>
    <string name="action_member">新增会员</string>
    <string name="next">下一步</string>
    <string name="hello_blank_fragment">Hello blank fragment</string>
    <string name="drawer_open">打开</string>
    <string name="drawer_close">关闭</string>
    <string name="hello_world">你好，世界</string>
    <string name="selected_count">所选项目</string>
    <string name="lipsum">Lorem ipsum dolor sit amet, ut duis lorem provident sed felis blandit, condimentum donec lectus ipsum et mauris, morbi porttitor interdum feugiat nulla donec sodales, vestibulum nisl primis a molestie vestibulum quam, sapien mauris metus risus suspendisse magnis. Augue viverra nulla faucibus egestas eu, a etiam id congue rutrum ante, arcu tincidunt donec quam felis at ornare, iaculis ligula sodales venenatis commodo volutpat neque, suspendisse elit praesent tellus felis mi amet. Inceptos amet tempor lectus lorem est non, ac donec ac libero neque mauris, tellus ante metus eget leo consequat. Scelerisque dolor curabitur pretium blandit ut feugiat, amet lacus pulvinar justo convallis ut, sed natoque ipsum urna posuere nibh eu. Sed at sed vulputate sit orci, facilisis a aliquam tellus quam aliquam, eu aliquam donec at molestie ante, pellentesque mauris lorem ultrices libero faucibus porta, imperdiet adipiscing sit hac diam ut nulla. Lacus enim elit pulvinar donec vehicula dapibus, accumsan purus officia cursus dolor sapien, eu amet dis mauris mi nulla ut. Non accusamus etiam pede non urna tempus, vestibulum aliquam tortor eget pharetra sodales, in vestibulum ut justo orci nulla, lobortis purus sem semper consectetuer magni purus. Dolor a leo vestibulum amet ut sit, arcu ut eaque urna fusce aliquet turpis, sed fermentum sed vestibulum nisl pede, tristique enim lorem posuere in laborum ut. Vestibulum id id justo leo nulla, magna lobortis ullamcorper et dignissim pellentesque, duis suspendisse quis id lorem ante. Vivamus a nullam ante adipiscing amet, mi vel consectetuer nunc aenean pede quisque, eget rhoncus dis porttitor habitant nunc vivamus, duis cubilia blandit non donec justo dictumst, praesent vitae nulla nam pulvinar urna. Adipiscing adipiscing justo urna pulvinar imperdiet nullam, vitae fusce rhoncus proin nonummy suscipit, ullamcorper amet et non potenti platea ultrices, mauris nullam sapien nunc justo vel, eu semper pellentesque arcu fusce augue. Malesuada mauris nibh sit a a scelerisque, velit sem lectus tellus convallis consectetuer, ultricies auctor a ante eros amet sed.\n\n
Risus lacus duis leo platea wisi, felis maecenas rutrum in id in donec, non id a potenti libero eget, posuere elit ea sed pellentesque quis. Sunt lacus urna lorem elit duis, nibh donec purus quisque consectetuer dolor, neque vestibulum proin ornare eros nonummy phasellus. Iaculis cras eu at egestas dolor montes, viverra quisque malesuada consectetuer semper maecenas, a sed vitae donec tempor aliqua metus, ornare mollis suscipit et erat fusce, sit orci aut auctor elementum fames aliquam. Platea dui integer magnis non metus, minus dignissimos ante massa nostra et, rutrum sapien egestas quis sapien donec donec. Erat sit a eros aenean natoque, quam libero id lorem enim proin, lorem ipsum fermentum mattis metus et. Aliquam aliquet suscipit purus conubia at neque, platea vivamus vestibulum nulla quibusdam senectus, et morbi lectus malesuada gravida donec, elementum sit convallis pellentesque velit amet. Et eveniet viverra vehicula consectetuer justo, provident sed commodo non lacinia velit, tempor phasellus vel leo nisl cras, vivamus et arcu interdum dui eu amet. Volutpat wisi rhoncus vel turpis diam quibusdam, dapibus elit est quisque cubilia mauris, nulla elit magna tempor accumsan bibendum, lorem varius sed interdum eget mattis, scelerisque egestas feugiat donec dui molestie. Leo facilisis nisl sit montes ligula sed, enim commodo consectetuer nunc est et, ut sed vehicula dolor luctus elit. Fermentum cras donec eget nibh est vel, sed justo risus et pharetra diam, eu vivamus egestas ligula risus diam, sed justo eget hac ut mauris. Vestibulum diam nec vitae mi eget suspendisse, aenean arcu purus facilisis purus class in, id aliquam sit id scelerisque sapien etiam. Ut nullam sit sed at mauris lobortis, consequat dolor autem ipsum euismod nulla, elit quis proin eget conubia varius, erat arcu massa mus in mauris, scelerisque ut eu sollicitudin libero leo urna.\n\n
Consectetuer luctus tempor elit ut dolor ligula, quis dui per dui hendrerit ante sagittis, in quisque pretium in eleifend enim. Condimentum iaculis vitae feugiat dis tellus vel, lectus dolor nec dui nulla nascetur, et pellentesque curabitur lorem leo velit eget. Id nascetur arcu lobortis suspendisse imperdiet urna, natoque nascetur ante in porta a, interdum hendrerit mi bibendum platea tellus, urna in enim ornare vestibulum faucibus enim. Leo fusce egestas ante nec volutpat, in tempor vel facilisis potenti ut, pede at non lorem a commodo, nulla dolor orci interdum vestibulum nulla. Dui nulla vestibulum quisque a pharetra porta, integer nec ipsum nec sed dui pharetra, magna et dignissim ipsum sed dictum, litora eros vivamus scelerisque libero ipsum. Sed ac ac lorem molestie adipiscing morbi, pellentesque imperdiet nunc quis morbi amet ante, libero dui ligula nec risus neque et, velit nonummy phasellus et facilisi amet, ligula in elementum non sapien pulvinar faucibus. Eu leo ut posuere sed aliquet, tincidunt vel urna volutpat tempus sem, sit felis aliquet vestibulum condimentum sit, amet nibh vel tellus purus ullamcorper libero, nulla vestibulum pede ut vestibulum pretium. Eu nulla vestibulum a neque in metus, quisquam nam sed cursus eget luctus, pede ultrices nec sed dignissim pellentesque, sit class cursus metus nulla placerat mauris, consequat mollis neque vivamus amet pede. Mauris dolor nulla diam eros bibendum, quam ante vestibulum morbi non ligula vel, molestie curabitur rhoncus nulla euismod interdum non. Nulla fringilla lorem mollis ad massa, sit molestie nibh lorem arcu volutpat, accumsan commodo lectus eu et donec, sit tempor tempus rutrum in curabitur amet. Nec urna euismod a tincidunt commodo, eu pede turpis libero vitae viverra, ante vestibulum nam non habitasse potenti, mauris imperdiet in in nunc convallis. Et nostra wisi in est accumsan vehicula, quisque vitae felis mauris sed vulputate nec, ante imperdiet sollicitudin massa iaculis massa sit.\n\n
Quam libero nulla netus eu porta curae, ut nulla bibendum facilisis et urna sed, quis congue vestibulum aliquam interdum etiam. Nulla vel lobortis ullamcorper vitae excepturi, neque urna feugiat lectus vel lacinia, massa pretium orci eu metus neque vulputate. Imperdiet ac velit rhoncus nulla malesuada nullam, nec pulvinar justo gravida lorem rutrum magna, habitasse repudiandae mi eros vestibulum ante, nec euismod dui iaculis in turpis pretium, ac id metus egestas proin lacus lectus. Laoreet lorem nec vitae risus erat arcu, vitae quam ut in ante tristique, porta dolor pede quam et odio nam, arcu lacus sem congue ante cursus massa. Et mattis sagittis erat accumsan fusce quam, vehicula ligula beatae natoque fusce sodales conubia, habitasse metus cum magnis viverra nam cursus, egestas urna wisi primis blandit eu magna, eget libero elit lacus lorem dis aliquam. Ut mauris ante natoque lacus massa, justo a lectus sodales enim adipiscing id, accumsan ut ipsum vestibulum sed enim auctor, vitae congue tincidunt id phasellus lacinia scelerisque, tincidunt sapien nulla euismod volutpat iaculis. Platea sociis nec aliquet nec molestie, in mi et augue sapien in vivamus, integer fames proin vitae in ullamcorper et. Fringilla etiam sapiente rhoncus suspendisse nec id, lobortis cras eget egestas dui ac nec, justo lacus ut lorem bibendum quia eros, eget a gravida id donec nunc suscipit, porta sed in sodales non rutrum. Lectus vel dui elementum pellentesque magna aliquam, vitae non sit pede et fusce nibh, id id deserunt ornare dui sit condimentum, in adipiscing imperdiet turpis nam aliquet, facilisis metus magna lacus wisi facilisis tortor. Vulputate elit accumsan quam amet ligula, suspendisse lacus mi nonummy integer urna, libero nulla nunc varius in odio, laoreet nulla amet placerat amet nec. Consectetuer vel massa hendrerit vitae iaculis id, sed ut ut laudantium odio in, elit vestibulum duis ante maecenas interdum in, neque vehicula ultrices varius in quam, pede tellus pellentesque sed nullam quis.</string>

    <!--PushLink api key-->
    <string name="push_link_api_key">t1bp6eih188nbvfe</string>

    <!--Api Key-->
    <string name="API_KEY">AIzaSyD570mqyVCeYjdfWHjf511rDYAfsJ_hnT8</string>

    <!--Activity Names-->
    <string name="title_activity_sub">SubActivity</string>
    <string name="title_activity_main">首页</string>
    <string name="title_activity_activity_using_tab_library">ActivityUsingTabLibrary</string>
    <string name="title_activity_my_dashboard">我的信息中心</string>
    <string name="title_activity_my_favorite">我的最爱</string>
    <string name="title_activity_calendar">日历</string>
    <string name="title_activity_add_calendar_item">新建日历项</string>
    <string name="title_activity_edit_calendar_item">编辑日历项</string>
    <string name="title_activity_add_location">添加地点</string>
    <string name="title_activity_add_contact">选择参加者</string>
    <string name="title_activity_news">新闻</string>
    <string name="title_activity_news_details">新闻详情</string>
    <string name="title_activity_event">活动</string>
    <string name="title_activity_event_details">活动</string>
    <string name="title_activity_chat">聊天室</string>
    <string name="title_activity_group">添加新组</string>
    <string name="title_activity_employee">职员名册</string>
    <string name="title_activity_employee_details">员名册详细</string>
    <string name="title_activity_leave">假期</string>
    <string name="title_activity_application">假期申请表格</string>
    <string name="title_activity_edit_application">假期详细内容</string>
    <string name="title_activity_download">下载中心</string>
    <string name="title_activity_folder">夹</string>
    <string name="title_activity_gallery">媒体库</string>
    <string name="title_activity_image">图像</string>
    <string name="title_activity_image_slider">图像滑块</string>
    <string name="title_activity_catalogue">产品目录</string>
    <string name="title_activity_sub_catalogue">分录</string>
    <string name="title_activity_drive">我的硬盘</string>
    <string name="title_activity_profile">我的简历</string>
    <string name="title_activity_login">登录</string>
    <string name="title_activity_register">注册</string>
    <string name="title_activity_password">密码</string>
    <string name="title_activity_dashboard">信息中心</string>
    <string name="title_activity_chat_image_view">ChatImageViewActivity</string>
    <string name="title_activity_employee_leave">员工假期</string>
    <string name="title_activity_employee_leave_details">工作人员留下详细内容</string>
    <string name="title_activity_home">HomeActivity</string>
    <string name="title_activity_on_leave">OnLeaveActivity</string>
    <string name="title_activity_cardroid_sampel">CardroidSampelActivity</string>
    <string name="title_activity_chat_room_group">ChatRoomGroupActivity</string>
    <string name="title_activity_edit_profile">编辑个人资料</string>
    <string name="title_activity_change_password">更改密码</string>
    <string name="title_activity_calendar_event_details">日历活动</string>
    <string name="title_activity_news_image_sliding">NewsImageSlidingActivity</string>
    <string name="title_activity_file_list">文件浏览器</string>
    <string name="title_activity_favorite_details">FavoriteDetailsActivity</string>
    <string name="title_activity_employee_image_sliding">EmployeeImageSlidingActivity</string>
    <string name="title_activity_profile_image_sliding">ProfileImageSlidingActivity</string>
    <string name="title_activity_invite_member">InviteMemberActivity</string>
    <string name="title_activity_branch">BranchActivity</string>
    <string name="title_activity_add_group_member">选择成员</string>
    <string name="title_activity_setting">设置</string>
    <string name="title_activity_group_info">小组资料</string>
    <string name="title_activity_log_out">登出</string>
</resources>
