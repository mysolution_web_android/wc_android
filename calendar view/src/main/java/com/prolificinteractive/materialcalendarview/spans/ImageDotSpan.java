package com.prolificinteractive.materialcalendarview.spans;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.style.LineBackgroundSpan;

/**
 * Created by user on 6/5/2016.
 */
public class ImageDotSpan implements LineBackgroundSpan {
    /**
     * Default radius used
     */
    public static final float DEFAULT_RADIUS = 3;

    private final float radius;
    private Bitmap mBitmap;
    private float mDividedNumber = 2.5f;

    /**
     * Create a span to draw a dot using default radius and color
     *
     * @see #DEFAULT_RADIUS
     */
    public ImageDotSpan() {
        this.radius = DEFAULT_RADIUS;
    }

    public ImageDotSpan(Bitmap bitmap, float dividedNumber) {
        this.radius = DEFAULT_RADIUS;
        this.mBitmap = bitmap;
        mDividedNumber = dividedNumber;
    }

    /**
     * Create a span to draw a dot using a specified radius
     *
     * @param radius radius for the dot
     * @see #ImageDotSpan(float)
     */
    public ImageDotSpan(float radius) {
        this.radius = radius;
    }

    @Override
    public void drawBackground(
            Canvas canvas, Paint paint,
            int left, int right, int top, int baseline, int bottom,
            CharSequence charSequence,
            int start, int end, int lineNum
    ) {
        if (mBitmap != null) {
            canvas.drawBitmap(mBitmap, (right - mBitmap.getWidth()) / 2, bottom + radius, paint);
        } else {
            int oldColor = paint.getColor();
            canvas.drawCircle((left + right) / 2, bottom + radius, radius, paint);
            paint.setColor(oldColor);
        }
    }
}
