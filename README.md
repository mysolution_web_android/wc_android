# Work Circle Pro Documentation

Dashboard 
--------------

The dashboard page is programmed in DashboardActivity.java. 

The recycler view with the adapter is programmed in DashboardAdapter.java 

The calendar pop up window is programmed in DashboardCalendarDialogFragment.java 

The employee leave pop up window is programmed in DashboardEmployeeLeaveDialogFragment.java 

The my leave pop up window is programmed in DashboardMyLeaveDialogFragment.java 

The API : (api/Account/Indicator) is programmed in DashboardActivity.java under getGson() function. 

The calendar api (api/Calendar/List?FromDate={FromDate}&NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) for pop up window is programmed in DashboardActivity.java under getCalendarGson() function. 

The Employee leave api (api/Leave/Other?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}&status={status}) for pop up window is programmed in DashboardActivity.java under getEmployeeLeaveGson() function. 

The My leave api (api/Leave/My?LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}&status={status}) for pop up window is programmed in DashboardActivity.java under getMyLeaveGson() function. 


Favorite 
---------------
The favorite list page is programmed in FavoriteActivity.java 

The favorite list api (api/Employee/FavouriteContact?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) is programmed in HttpHelper.java under getFavoriteGson() function 

The recyclerView with adapter is programmed in ContactAdapter.java 

The favorite details page is programmed in FavoriteDetailsActivity.java 

The favorite details api (api/Employee/{id}) is programmed in HttpHelper.java under getSingleFavoriteGson() function. 

Calendar 
-----------
The calendar page with 2 tabs is programmed in CalendarActivity.java 

The Schedule page is programmed in ScheduleFragment.java 

The recycler view with adapter is programmed in ScheduleAdapter.java 

The Api for schedule list is (api/Calendar/Month/List?FromDate={FromDate}&ToDate={ToDate}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) in HttpHelper.java in getScheduleGson() 

The month page is programmed in MonthFragment.java 

The month view is programmed in MonthCustomFragment.java 

The month view with adapter is programmed in MonthCustomAdapter.java 

The api for month is (api/Calendar/Month/List?FromDate={FromDate}&ToDate={ToDate}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) in HttpHelper.java in getScheduleGson() 

The calendar details is programmed in CalendarEventDetailsActivity.java 

The Api to get single calendar is (api/Calendar/{id}) in HttpHelper.java under getSingleCalendarGson(). 

The Api for re invite staff  is rejected in CalendarEventAdapter.java is (api/Calendar/Reinvite)  in HttpHelper.java under reinviteAttendee(). 

The Api for accept the calendar event(api/Calendar/Attendees/Attend?id={id}&Reminder={Reminder}) is in CalendarDialogFragment.java under attentEvent() 

The Api for reject the calendar event ( api/Calendar/Attendees/Rejected?id={id}&RemoveFromList={RemoveFromList}) in HttpHelper.java under rejectEvent(). 

The edit calendar or new calendar page is in CalendarDetailsActivity,.java 

The Api for edit calendar (api/Calendar) in HttpHelper.java under putCalendarData(). 

The Api for add new calendar(api/Calendar) in HttpHelper.java under postCalendarData() 

The Api to get Favorite list ( api/Employee/FavouriteContact?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty})is under HttpHelper.java under the getFavoriteGson() 

The Api to get Office/Branch Staff List (api/Employee/ByCountry?CountryCode={CountryCode}&SearchKey={SearchKey}&NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) is in HttpHelper.java under the getDepartmentGson() 

The Api to get Country list (api/Country?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty})is in HttpHelper.java under getCountryGson(). 

News 
--------------
The News list page is programmed in NewsActivity.java 

The News list api (api/News?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) is programmed in HttpHelper.java under getNewsGson() function 

The recyclerView with adapter is programmed in NewsAdapter.java 

The News details page is programmed in NewsDetailsActivity.java 
 
 
Event 
-----------
The Event list page is programmed in EventActivity.java 

The Event list api ( api/Event?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) is programmed in HttpHelper.java under getEventGson() function 

The recyclerView with adapter is programmed in EventAdapter.java 

The Event details page is programmed in EventDetailsActivity.java 

The api to get single event (api/Event/{id}) is programmed in HttpHelper.java under getSingleEventGson() function.


Catalogue 
-------------
The catalogue list page is programmed in CatalogueActivity.java, SubCatalogueOneActivity.java, SubCatalogueTwoActivity.java, SubCatalogueThreeActivity.java, SubCatalogueFourActivity.java. 

The recycler view with adapter is programmed in CatalogueAdapter.java and SubCatalogueAdapter.java. 

The Api to get the list of catalogue (api/ProductCatalog/{id}?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) is programmed in HttpHelper.java under getCatalogueGson() function. 

 

Download 
---------------
The download list page is programmed in DownloadActivity.java, FolderOneActivity.java, FolderTwoActivity.java, FolderThreeActivity.java, FolderFourActivity.java. 

The recycler view with adapter is programmed in DownloadAdapter.java and FolderAdapter.java. 

The Api to get the list of download (api/DownloadCentre/{id}?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) is programmed in HttpHelper.java under getDownloadGson() function. 

 

Gallery 
-----------
The album list is programmed in GalleryActivity.java 

The recycler view with adapter is programmed in GalleryAdapter.java 

The Image list is programmed in ImageActivity.java 

The gridView with adapter is programmed in ImageGridAdapter.java 

The api for album list (api/Gallery?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) is in HttpHelper.java under getAlbumGson() function. 

The api for image list (api/Gallery/{id}?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) is in HttpHelper.java under getImageGson() function. 

 

Employee 
---------
The employee page with sliding tab is programmed in EmployeeActivity.java 

The recent list is programmed in RecentEmployeeFragment.java 

The recycler view with the adapter is programmed in RecentEmployeeAdapter.java 

The api to get recent list (api/Employee/RecentContact?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) in HttpHelper.java under getRecentGson() function. 

The office list is programmed in OfficeEmployeeFragment.java 

The recycler view with the adapter is programmed in EmployeeAdapter.java 

The api to get office list (api/Employee/RecentContact?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) in HttpHelper.java under getOfficeGson() function. 

The country list is programmed in BranchEmployeeFragment.java 

The recycler view with the adapter is programmed in CountryAdapter.java 

The api to get country list (api/Country?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) in HttpHelper.java under getCountryGson() function. 

The branch list is programmed in BranchEmployeeFragment.java 

The recycler view with the adapter is programmed in EmployeeAdapter.java 

The api to get branch list (api/Employee/ByCountry?CountryCode={CountryCode}&SearchKey={SearchKey}&NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}) in HttpHelper.java under getBranchEmployeeGson() function. 

 

Leave 
-----------
The my leave list is programmed in MyLeaveFragment.java 

The recycler view with adapter is programmed in MyLeaveAdapter.java 

The api to get my leave (api/Leave/My?LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}&status={status}) is in HttpHelper.java under getMyleaveGson() function. 

The employee leave list is programmed in EmployeeLeaveFragment.java. 

The recycler view with adapter is programmed in EmployeeLeaveAdapter.java 

The api to get employee leave (api/Leave/Other?NoPerPage={NoPerPage}&PageNo={PageNo}&LastUpdated={LastUpdated}&ReturnEmpty={ReturnEmpty}&status={status} ) is in HttpHelper.java under getEmployeeLeavwGson() function. 

The api to get single leave (api/Leave?id={id}) is in HttpHelper.java under getSingleLeaveGson() function. 

The leave details is programmed in EditApplicationActivity.java 

The api to approve leave (api/Leave/Other/Approved) is programmed in RemarksDialogFragment.java under putApprove() funciton. 

The api to reject leave (api/Leave/Other/Reject) is programmed in RemarksDialogFragment.java under putReject() funciton. 

The api to delete leave (api/Leave/My?id={id}&Remark={Remark}) is programmed in RemarksDialogFragment.java under deleteLeave() function 

The edit/new leave is programmed in ApplicationActivity.java. 

The api to submit new leave (api/Leave/My)is programmed in ApplicationActivity.java under postData() funciton. 

The api to submit new leave (api/Leave/My)is programmed in ApplicationActivity.java under putData() funciton. 

 

Drive 
-----------------
The drive list page is programmed in DriveActivity.java 

The recycler view with adapter is programmed in DriveAdapter.java 

The drive list has few operation: RefreshOperation.java, RemoveOperation.java, RenameFileOperation.java, UpdateFileOperation.java 

The base url for drive is programmed in string.xml under the name server_base_url. 

 

Messenger 
-----------------
The messenger page with sliding tab is programmed in ChatActivity.java 

The single chat list is programmed is ChatSingleFragment.java 

The group chat list is programmed in ChatMultipleFragment.java 

The single/group chat adapter is programmed in ChatAdapter.java 

The single chat room is programmed in CharRoomFragment.java 

The group chat room is programmed in ChatRoomGroupFragment.java 

All the xmpp function is programmed in BackgroundXMPPService.java 

The host, server ip, service are programmed in Config.java 

 

Login 
---------------
The page for login is programmed in LoginActivity.java 

The api for login is (http://app.wwrc.com/mintranet/TOKEN) is programmed in LoginActivity.java under authentication() function. 

The push device infor (api/Device) is programmed in LoginActivity.java under pushDevice() function. 

Get Employee details (api/Employee/{id}) is programmed in LoginActivity.java under getProfile() function. 

 

Register 
-------------------
The register page is programmed in RegisterActivity.java 

The api (api/Account/Register) to register the user is programmed in RegisterActivity.java, under register() function. 

 

Forget password 
-----------------------
The forget password page is programmed in PasswordActivity.java 

The api(api/Employee/ForgetPassword) for forget password is programmed in PasswordActivity.java under the getGson() function.  

 

Profile 
---------------------
The profile page is programmed in ProfileActivity.java 

The api (api/Employee/{id}) to get profile details is programmed in ProfileActivity.java under getGson() function. 

The edit profile page is programmed in EditProfileActivity.java 

The api (api/Account) to edit the profile page is programmed in ProfileUploadService.java under putProfileImage() function. 

 

Utilities 
--------------------
The base url for all api is programmed in Utils.java under the name API_URL 

 

Push Notification 
------------------------
The background service to receive the push notification is programmed in AirshipReceiver.java 

The config file for push notification is programmed in assets/airshipconfig.properties 

To gcm token for individual phone, is located at AppController.java under the setChannelId() function. 


 

Api Reference
----------------
http://www.workcircle.com.my/wcapi/Help 

 

Library Reference 
-----------------------
https://github.com/flavienlaurent/datetimepicker 

https://github.com/bumptech/glide 

https://github.com/umano/AndroidSlidingUpPanel 

https://github.com/JodaOrg/joda-time 

https://github.com/traex/RippleEffect 

https://github.com/OrangeGangsters/SwipyRefreshLayout 

https://github.com/igniterealtime/Smack 

https://github.com/jgilfelt/SystemBarTint 

http://docs.urbanairship.com/platform/android.html 

 
Flow Changes 
----------------
![](http://35.197.160.118/wp-content/uploads/2018/08/mobile-intranet.jpg)