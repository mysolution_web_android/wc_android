package com.applab.wcircle_pro;

import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;
import android.util.Log;

import com.applab.wcircle_pro.Calendar.AttendeesProvider;
import com.applab.wcircle_pro.Utils.DBHelper;

public class AttendeesProviderTest extends ProviderTestCase2<AttendeesProvider> {

    private static final String TAG = AttendeesProviderTest.class.getName();
    private static MockContentResolver resolve;
    public AttendeesProviderTest(Class<AttendeesProvider> providerClass, String providerAuthority) {
        super(providerClass, providerAuthority);
        // TODO Auto-generated constructor stub
    }

    public AttendeesProviderTest() {
        super(AttendeesProvider.class,"com.applab.wcircle_pro.Calendar.attendeesprovider");
    }


    @Override
    public void setUp() {
        try {
            Log.i(TAG, "Entered Setup");
            super.setUp();
            resolve = this.getMockContentResolver();
        }
        catch(Exception e) {


        }
    }

    @Override
    public void tearDown() {
        try{
            super.tearDown();
        }
        catch(Exception e) {


        }
    }

//    public void testInsert() throws ParseException {
//        long rowId = 0;
//        Log.i("TAG","Basic Insert Test");
//        Date date = new Date();
//        SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
//        String currDate = format.format(date);
//        String startDate = "";
//        String endDate="";
//
//        startDate = "2015-06-15 00:55:20";
//        date = format.parse(startDate);
//        startDate = format.format(date);
//
//        endDate =  "2015-07-27 00:57:20";
//        date = format.parse(endDate);
//        endDate = format.format(date);
//
//        ContentValues contentValues = new ContentValues();
//        DBHelper helper = new DBHelper(getContext());
//        contentValues.put(helper.CALENDAR_COLUMN_TITLE, "ABC");
//        contentValues.put(helper.CALENDAR_COLUMN_DESCRIPTION, "ABC");
//        contentValues.put(helper.CALENDAR_COLUMN_COLOR, 3);
//        contentValues.put(helper.CALENDAR_COLUMN_IS_ALL_DAY, 0);
//        contentValues.put(helper.CALENDAR_COLUMN_LOCATION, "abc");
//        contentValues.put(helper.CALENDAR_COLUMN_START_DATE, startDate);
//        contentValues.put(helper.CALENDAR_COLUMN_END_DATE, endDate);
//        contentValues.put(helper.CALENDAR_COLUMN_CREATE_BY, "System");
//        contentValues.put(helper.CALENDAR_COLUMN_CREATE_DATE, currDate);
//        contentValues.put(helper.CALENDAR_COLUMN_UPDATE_BY, "System");
//        contentValues.put(helper.CALENDAR_COLUMN_UPDATE_DATE, currDate);
//        Uri uri=getContext().getContentResolver().insert(AttendeesProvider.CONTENT_URI,contentValues);
//        rowId = ContentUris.parseId(uri);
//        assertTrue(rowId!=0);
//    }

    /*public void testBulkInsert() {
        long rowId = 0;
        Log.i("TAG","Basic BulkInsert Test");
        RestfulAPI api = new RestfulAPI();
        JSONObject jsonObj;
        ArrayList<Products> arrProducts=new ArrayList<>();
        try{
            // Call the Contact Method in API
            jsonObj = api.GetContactDetails("","");
            JSONParser parser = new JSONParser();
            arrProducts =parser.parseProducts(jsonObj);
            ContentValues[] contentValueses= new ContentValues[arrProducts.size()];
            for (int i = 0; i < arrProducts.size(); i++){
                ContentValues contentValues = new ContentValues();
                contentValues.put("name", arrProducts.get(i).get_productname());
                contentValues.put("phone", arrProducts.get(i).get_phone());
                contentValues.put("email", arrProducts.get(i).get_email());
                contentValues.put("street", arrProducts.get(i).get_street());
                contentValues.put("place",arrProducts.get(i).get_place());
                contentValueses[i]=contentValues;
            }
            rowId = getContext().getContentResolver().bulkInsert(ContactProvider.CONTENT_URI, contentValueses);
            assertTrue(rowId!=0);
        }catch(Exception ex){
            Log.i(TAG,"Error :"+ex.toString());
        }

    }*/

   /* public void testUpdate() {
        int rowId = 1;
        Log.i("TAG","Basic Update Test");
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", "gsdfgdfgfd");
        int id=getContext().getContentResolver().update(ContactProvider.CONTENT_URI,contentValues,null,null);
        int id=getContext().getContentResolver().update(ContactProvider.CONTENT_URI,contentValues,"id = ?",new String[] {Integer.toString(1)});
        assertTrue(id!=0);
    }*/
    public void testDelete() {
        Log.i("TAG","Basic Delete Test");
        int id=getContext().getContentResolver().delete(AttendeesProvider.CONTENT_URI, DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=?",
                new String[]{"2", String.valueOf(1)});
        //int id=getContext().getContentResolver().delete(ContactProvider.CONTENT_URI,"id=?",new String[] {Integer.toString(1));
        assertTrue(id==2);
    }

    //    public void testQuery() {
//        ArrayList array_list = new ArrayList();
//        Uri uri = ContactProvider.CONTENT_URI;
//        String[] projection = { DBHelper.CONTACTS_COLUMN_ID,
//                DBHelper.CONTACTS_COLUMN_NAME,DBHelper.CONTACTS_COLUMN_EMAIL,
//                DBHelper.CONTACTS_COLUMN_STREET,DBHelper.CONTACTS_COLUMN_CITY,
//                DBHelper.CONTACTS_COLUMN_PHONE};
//        Cursor cursor = getContext().getContentResolver().query(uri, projection,
//                null, null, DBHelper.CONTACTS_COLUMN_NAME);
//        assertEquals(cursor.getCount(), 0); //check number of returned rows
//        assertEquals(cursor.getColumnCount(), 6); //check number of returned columns
//        cursor.moveToFirst();
//        while(cursor.isAfterLast() == false){
//            array_list.add(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACTS_COLUMN_NAME)));
//            cursor.moveToNext();
//        }
//    }
//    public void testQuery() {
//        Uri uri = AttendeesProvider.CONTENT_URI;
//        String[] projection = { DBHelper.ATTENDEES_COLUMN_ID};
//        String where = DBHelper.ATTENDEES_COLUMN_CALENDAR_ID +"=?";
//        String[] selectionArgs = {"1"};
//        Cursor cursor = getContext().getContentResolver().query(uri, projection,
//                where, selectionArgs, null);
//        assertEquals(cursor.getCount(), 1); //check number of returned rows
//        assertEquals(cursor.getColumnCount(), 1); //check number of returned columns
//    }

}
