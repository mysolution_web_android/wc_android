package com.applab.wcircle_pro;

import android.database.Cursor;
import android.net.Uri;
import android.test.ServiceTestCase;
import android.util.Log;

import com.applab.wcircle_pro.News.NewsProvider;
import com.applab.wcircle_pro.News.SyncAdapter;
import com.applab.wcircle_pro.News.SyncService;
import com.applab.wcircle_pro.Utils.DBHelper;

/**
 * Created by user on 4/22/2015.
 */
public class SyncAdapterTest extends ServiceTestCase<SyncService> {
    public SyncAdapterTest() {
        super(SyncService.class);
    }
    public void testIncormingJSON() {
        try{
            SyncAdapter adapter = new SyncAdapter(getContext(), false);
//            adapter.volleyData();
            Uri uri = NewsProvider.CONTENT_URI;
            String[] projection = { DBHelper.NEWS_COLUMN_ID};
            Cursor cursor = getContext().getContentResolver().query(uri, projection,
                    null, null, DBHelper.NEWS_COLUMN_ID);
            assertEquals(cursor.getCount(), 0); //check number of returned rows
            assertEquals(cursor.getColumnCount(), 1); //check number of returned columns
        }catch(Exception ex){
            Log.i("testIncormingJSON", ex.toString());
        }
    }
}
