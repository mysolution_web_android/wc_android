package com.applab.wcircle_pro;

import android.database.Cursor;
import android.net.Uri;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;
import android.util.Log;

import com.applab.wcircle_pro.Event.EventProvider;
import com.applab.wcircle_pro.Utils.DBHelper;

public class EventProviderTest extends ProviderTestCase2<EventProvider> {

    private static final String TAG = EventProviderTest.class.getName();
    private static MockContentResolver resolve;
    public EventProviderTest(Class<EventProvider> providerClass,String providerAuthority) {
        super(providerClass, providerAuthority);
        // TODO Auto-generated constructor stub
    }

    public EventProviderTest() {
        super(EventProvider.class,"com.applab.wcircle_pro.event.eventprovider");
    }


    @Override
    public void setUp() {
        try {
            Log.i(TAG, "Entered Setup");
            super.setUp();
            resolve = this.getMockContentResolver();
        }
        catch(Exception e) {


        }
    }

    @Override
    public void tearDown() {
        try{
            super.tearDown();
        }
        catch(Exception e) {


        }
    }

//    public void testInsert() throws ParseException {
//        long rowId = 0;
//        Log.i("TAG","Basic Insert Test");
//        Date date = new Date();
//        SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
//        String currDate = format.format(date);
//        String eventDate = "";
//        eventDate = "2015-06-15 00:55:20";
//        date = format.parse(eventDate);
//        eventDate = format.format(date);
//
//        ContentValues contentValues = new ContentValues();
//        DBHelper helper = new DBHelper(getContext());
//        contentValues.put(helper.EVENT_COLUMN_TITLE,"Sport Day");
//        contentValues.put(helper.EVENT_COLUMN_DESCRIPTION,"Description");
//        contentValues.put(helper.EVENT_COLUMN_LOCATION, "Kuala Lumpur Convention Centre");
//        contentValues.put(helper.EVENT_COLUMN_BANNER, "http://hdwallpaperd.com/wp-content" +
//                                                        "/uploads/new-wallpaper-17.jpg");
//        contentValues.put(helper.EVENT_COLUMN_ICON, "http://empireminecraft.com/attachments" +
//                "                                   /avengers-jpeg.9485/");
//        contentValues.put(helper.EVENT_COLUMN_PRESENTER, "Menara Kuala Lumpur (KL Tower)");
//        contentValues.put(helper.EVENT_COLUMN_DATE, eventDate);
//        contentValues.put(helper.EVENT_COLUMN_CREATE_BY, "System");
//        contentValues.put(helper.EVENT_COLUMN_CREATE_DATE, currDate);
//        contentValues.put(helper.EVENT_COLUMN_UPDATE_BY, "System");
//        contentValues.put(helper.EVENT_COLUMN_UPDATE_DATE, currDate);
//        Uri uri=getContext().getContentResolver().insert(EventProvider.CONTENT_URI,contentValues);
//        rowId = ContentUris.parseId(uri);
//        assertTrue(rowId!=0);
//    }

    /*public void testBulkInsert() {
        long rowId = 0;
        Log.i("TAG","Basic BulkInsert Test");
        RestfulAPI api = new RestfulAPI();
        JSONObject jsonObj;
        ArrayList<Products> arrProducts=new ArrayList<>();
        try{
            // Call the Contact Method in API
            jsonObj = api.GetContactDetails("","");
            JSONParser parser = new JSONParser();
            arrProducts =parser.parseProducts(jsonObj);
            ContentValues[] contentValueses= new ContentValues[arrProducts.size()];
            for (int i = 0; i < arrProducts.size(); i++){
                ContentValues contentValues = new ContentValues();
                contentValues.put("name", arrProducts.get(i).get_productname());
                contentValues.put("phone", arrProducts.get(i).get_phone());
                contentValues.put("email", arrProducts.get(i).get_email());
                contentValues.put("street", arrProducts.get(i).get_street());
                contentValues.put("place",arrProducts.get(i).get_place());
                contentValueses[i]=contentValues;
            }
            rowId = getContext().getContentResolver().bulkInsert(ContactProvider.CONTENT_URI, contentValueses);
            assertTrue(rowId!=0);
        }catch(Exception ex){
            Log.i(TAG,"Error :"+ex.toString());
        }

    }*/

   /* public void testUpdate() {
        int rowId = 1;
        Log.i("TAG","Basic Update Test");
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", "gsdfgdfgfd");
        int id=getContext().getContentResolver().update(ContactProvider.CONTENT_URI,contentValues,null,null);
        int id=getContext().getContentResolver().update(ContactProvider.CONTENT_URI,contentValues,"id = ?",new String[] {Integer.toString(1)});
        assertTrue(id!=0);
    }*/
//    public void testDelete() {
//        Log.i("TAG","Basic Delete Test");
//        int id=getContext().getContentResolver().delete(EventProvider.CONTENT_URI,null,null);
//        //int id=getContext().getContentResolver().delete(ContactProvider.CONTENT_URI,"id=?",new String[] {Integer.toString(1));
//        assertTrue(id!=0);
//    }

    //    public void testQuery() {
//        ArrayList array_list = new ArrayList();
//        Uri uri = ContactProvider.CONTENT_URI;
//        String[] projection = { DBHelper.CONTACTS_COLUMN_ID,
//                DBHelper.CONTACTS_COLUMN_NAME,DBHelper.CONTACTS_COLUMN_EMAIL,
//                DBHelper.CONTACTS_COLUMN_STREET,DBHelper.CONTACTS_COLUMN_CITY,
//                DBHelper.CONTACTS_COLUMN_PHONE};
//        Cursor cursor = getContext().getContentResolver().query(uri, projection,
//                null, null, DBHelper.CONTACTS_COLUMN_NAME);
//        assertEquals(cursor.getCount(), 0); //check number of returned rows
//        assertEquals(cursor.getColumnCount(), 6); //check number of returned columns
//        cursor.moveToFirst();
//        while(cursor.isAfterLast() == false){
//            array_list.add(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACTS_COLUMN_NAME)));
//            cursor.moveToNext();
//        }
//    }

    public void testQuery() {
        Uri uri = EventProvider.CONTENT_URI;
        String[] projection = {DBHelper.EVENT_COLUMN_ID};
        Cursor cursor = getContext().getContentResolver().query(uri, projection,
                null, null, null);
        assertEquals(cursor.getCount(), 0); //check number of returned rows
        assertEquals(cursor.getColumnCount(), 1); //check number of returned columns
    }
}
