package com.applab.wcircle_pro;

import android.database.Cursor;
import android.net.Uri;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;
import android.util.Log;

import com.applab.wcircle_pro.News.NewsProvider;
import com.applab.wcircle_pro.Utils.DBHelper;

public class NewsProviderTest extends ProviderTestCase2<NewsProvider> {

    private static final String TAG = NewsProviderTest.class.getName();
    private static MockContentResolver resolve;
    public NewsProviderTest(Class<NewsProvider> providerClass,String providerAuthority) {
        super(providerClass, providerAuthority);
        // TODO Auto-generated constructor stub
    }

    public NewsProviderTest() {
        super(NewsProvider.class,"com.applab.wcircle_pro.news.newsprovider");
    }


    @Override
    public void setUp() {
        try {
            Log.i(TAG, "Entered Setup");
            super.setUp();
            resolve = this.getMockContentResolver();
        }
        catch(Exception e) {


        }
    }

    @Override
    public void tearDown() {
        try{
            super.tearDown();
        }
        catch(Exception e) {


        }
    }

//    public void testInsert() throws ParseException {
//        long rowId = 0;
//        Log.i("TAG","Basic Insert Test");
//        Date date = new Date();
//        SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
//        String currDate = format.format(date);
//        String postedDate = "";
//        postedDate = "2015-06-15 00:55:20";
//        date = format.parse(postedDate);
//        postedDate = format.format(date);
//
//        ContentValues contentValues = new ContentValues();
//        DBHelper helper = new DBHelper(getContext());
//        contentValues.put(helper.NEWS_COLUMN_TITLE,
//                "wcircle_pro is the absolute best way to share Company Nws & Announcement");
//        contentValues.put(helper.NEWS_COLUMN_DESCRIPTION, "As technology progresses, new tools are " +
//                "popping up all the time. Some of these serve as passing trends that will inevitably go " +
//                "the way of the dinosaur, but others come along with staying power that simply cannot be denied.");
//        contentValues.put(helper.NEWS_COLUMN_NAME, "Micheal Lee");
//        contentValues.put(helper.NEWS_COLUMN_DATE, postedDate);
//        contentValues.put(helper.NEWS_COLUMN_CREATE_BY, "System");
//        contentValues.put(helper.NEWS_COLUMN_CREATE_DATE, currDate);
//        contentValues.put(helper.NEWS_COLUMN_UPDATE_BY, "System");
//        contentValues.put(helper.NEWS_COLUMN_UPDATE_DATE, currDate);
//        Uri uri=getContext().getContentResolver().insert(NewsProvider.CONTENT_URI,contentValues);
//        rowId = ContentUris.parseId(uri);
//        assertTrue(rowId!=0);
//    }

    /*public void testBulkInsert() {
        long rowId = 0;
        Log.i("TAG","Basic BulkInsert Test");
        RestfulAPI api = new RestfulAPI();
        JSONObject jsonObj;
        ArrayList<Products> arrProducts=new ArrayList<>();
        try{
            // Call the Contact Method in API
            jsonObj = api.GetContactDetails("","");
            JSONParser parser = new JSONParser();
            arrProducts =parser.parseProducts(jsonObj);
            ContentValues[] contentValueses= new ContentValues[arrProducts.size()];
            for (int i = 0; i < arrProducts.size(); i++){
                ContentValues contentValues = new ContentValues();
                contentValues.put("name", arrProducts.get(i).get_productname());
                contentValues.put("phone", arrProducts.get(i).get_phone());
                contentValues.put("email", arrProducts.get(i).get_email());
                contentValues.put("street", arrProducts.get(i).get_street());
                contentValues.put("place",arrProducts.get(i).get_place());
                contentValueses[i]=contentValues;
            }
            rowId = getContext().getContentResolver().bulkInsert(ContactProvider.CONTENT_URI, contentValueses);
            assertTrue(rowId!=0);
        }catch(Exception ex){
            Log.i(TAG,"Error :"+ex.toString());
        }

    }*/

   /* public void testUpdate() {
        int rowId = 1;
        Log.i("TAG","Basic Update Test");
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", "gsdfgdfgfd");
        int id=getContext().getContentResolver().update(ContactProvider.CONTENT_URI,contentValues,null,null);
        int id=getContext().getContentResolver().update(ContactProvider.CONTENT_URI,contentValues,"id = ?",new String[] {Integer.toString(1)});
        assertTrue(id!=0);
    }*/
//    public void testDelete() {
//        Log.i("TAG","Basic Delete Test");
//        int id=getContext().getContentResolver().delete(ContactProvider.CONTENT_URI,null,null);
//        //int id=getContext().getContentResolver().delete(ContactProvider.CONTENT_URI,"id=?",new String[] {Integer.toString(1));
//        assertTrue(id!=0);
//    }

    //    public void testQuery() {
//        ArrayList array_list = new ArrayList();
//        Uri uri = ContactProvider.CONTENT_URI;
//        String[] projection = { DBHelper.CONTACTS_COLUMN_ID,
//                DBHelper.CONTACTS_COLUMN_NAME,DBHelper.CONTACTS_COLUMN_EMAIL,
//                DBHelper.CONTACTS_COLUMN_STREET,DBHelper.CONTACTS_COLUMN_CITY,
//                DBHelper.CONTACTS_COLUMN_PHONE};
//        Cursor cursor = getContext().getContentResolver().query(uri, projection,
//                null, null, DBHelper.CONTACTS_COLUMN_NAME);
//        assertEquals(cursor.getCount(), 0); //check number of returned rows
//        assertEquals(cursor.getColumnCount(), 6); //check number of returned columns
//        cursor.moveToFirst();
//        while(cursor.isAfterLast() == false){
//            array_list.add(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACTS_COLUMN_NAME)));
//            cursor.moveToNext();
//        }
//    }

    public void testQuery() {
        Uri uri = NewsProvider.CONTENT_URI;
        String[] projection = {DBHelper.NEWS_COLUMN_ID};
        Cursor cursor = getContext().getContentResolver().query(uri, projection,
                null, null, null);
        assertEquals(cursor.getCount(), 1); //check number of returned rows
        assertEquals(cursor.getColumnCount(), 1); //check number of returned columns
    }
}
