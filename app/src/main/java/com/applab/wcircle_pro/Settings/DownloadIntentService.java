package com.applab.wcircle_pro.Settings;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.applab.wcircle_pro.Drive.DownloadFileOperation;
import com.applab.wcircle_pro.Drive.OCFile;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.OwnCloudClientFactory;
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory;
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener;
import com.owncloud.android.lib.resources.files.FileUtils;

/**
 * Created by user on 2/10/2015.
 */

public class DownloadIntentService extends IntentService implements OnDatatransferProgressListener {
    private String TAG = "DOWNLOAD";
    private String filePath;
    private OwnCloudClient mClient;
    private OCFile file;
    private DownloadFileOperation operation;
    private int id;

    public DownloadIntentService() {
        super("DownloadIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle bundle = intent.getExtras();

        Uri serverUri = Uri.parse(getString(R.string.server_base_url));
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, getBaseContext(), true);
        mClient.setCredentials(OwnCloudCredentialsFactory.newBasicCredentials(Utils.getProfile(getBaseContext()).getEmail(),
                BackupActivity.pref.getString("own_password", null)));

        file = bundle.getParcelable("OCFile");
        id = bundle.getInt("id");

        operation = new DownloadFileOperation(getBaseContext(), file.getRemotePath(),
                FileUtils.PATH_SEPARATOR + getBaseContext().getString(R.string.messenger_backup_folder), file);
        filePath = operation.getSavedPath();
        operation.addDatatransferProgressListener(this);

        if (Utils.getUsableSpace() < file.getLength()) {
            Utils.showError(getBaseContext(), Utils.CODE_LOCAL_SIZE_EXCEEDED, Utils.LOCAL_SIZE_EXCEED);
        } else {
            operation.execute(mClient);
        }
    }

    @Override
    public void onTransferProgress(long progressRate, final long totalTransferredSoFar, final long totalToTransfer, String fileAbsoluteName) {
        final NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent pIntent = PendingIntent.getActivity(this, 0, Utils.getIntent(filePath, getBaseContext()), PendingIntent.FLAG_CANCEL_CURRENT);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        String[] result = file.getRemotePath().split("/");
        mBuilder.setContentTitle(result[result.length - 1])
                .setContentText("Download in progress")
                .setSmallIcon(R.mipmap.icon)
                .setContentIntent(pIntent).build();

        // Start a lengthy operation in a background thread
        mBuilder.setProgress((int) totalToTransfer, (int) totalTransferredSoFar, false);
        // Displays the progress bar for the first time.
        //mNotifyManager.notify(id, mBuilder.build());
        // When the loop is finished, updates the notification
        if ((int) totalToTransfer == (int) totalTransferredSoFar) {
            mBuilder.setContentText("Download complete")
                    // Removes the progress bar
                    .setProgress(0, 0, false);
            //mNotifyManager.notify(id, mBuilder.build());
            Intent intent = new Intent(BackupActivity.ACTION);
            intent.putExtra("isDownloaded", true);
            intent.putExtra("OCFile", (Parcelable) file);
            LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(intent);
        }
    }
}
