package com.applab.wcircle_pro.Settings;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.R;

import java.util.ArrayList;

/**
 * Created by user on 5/2/2015.
 */

public class BackupAdapter extends RecyclerView.Adapter<SettingsViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Settings> arrayList;

    public BackupAdapter(Context context, ArrayList<Settings> arrayList) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public SettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("SettingsViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_settings_row, parent, false);
        SettingsViewHolder holder = new SettingsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SettingsViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        holder.img.setImageResource(arrayList.get(position).image);
        holder.img.setVisibility(View.GONE);
        holder.txtName.setText(arrayList.get(position).name);
    }

    @Override
    public int getItemCount() {
        return (arrayList == null) ? 0 : arrayList.size();
    }

}
