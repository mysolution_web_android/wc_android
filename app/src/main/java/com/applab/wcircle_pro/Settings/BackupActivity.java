package com.applab.wcircle_pro.Settings;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.ChatProvider;
import com.applab.wcircle_pro.Chat.db.GroupChatProvider;
import com.applab.wcircle_pro.Chat.db.GroupProvider;
import com.applab.wcircle_pro.Chat.db.HttpHelper;
import com.applab.wcircle_pro.Chat.model.MessageModel;
import com.applab.wcircle_pro.Chat.model.Msg;
import com.applab.wcircle_pro.Chat.model.array;
import com.applab.wcircle_pro.Chat.model.subject;
import com.applab.wcircle_pro.Drive.*;
import com.applab.wcircle_pro.Profile.Profile;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.Gson;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.OwnCloudClientFactory;
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory;
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener;
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.FileUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;

public class BackupActivity extends AppCompatActivity implements OnRemoteOperationListener, OnDatatransferProgressListener {
    //region variable
    private Toolbar toolbar;
    private TextView txtToolbarTitle;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private BackupAdapter adapter;
    private ArrayList<Settings> arrayList = new ArrayList<>();
    public static String ACTION = "BACKUP";
    private RelativeLayout fadeRL;
    private ProgressBar progressBar;
    private File file;
    private String TAG = "BACKUP";
    private OwnCloudClient mClient;
    private Handler mHandler = new Handler();
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    private String parentId = "0";
    //endregion

    //region create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger_backup);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_backup));
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        setSettings();
        adapter = new BackupAdapter(this, arrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        setTouchListener();
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        fadeRL.setOnClickListener(fadeRLOnClickListener);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        setVisibilityRlAndProgressBar(false);

        pref = getApplicationContext().getSharedPreferences("OWNCLOUD PASSWORD", 0);
        editor = pref.edit();

        Uri serverUri = Uri.parse(getString(R.string.server_base_url));
        String password = pref.getString("own_password", null);
        String userId = Utils.getProfile(this).getEmail();

        mHandler = new Handler();

        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true);
        mClient.setCredentials(OwnCloudCredentialsFactory.newBasicCredentials(userId, password));
    }
    //endregion

    private View.OnClickListener fadeRLOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private void setVisibilityRlAndProgressBar(boolean isVisible) {
        if (isVisible) {
            fadeRL.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            fadeRL.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    private void setSettings() {
        Settings settings = new Settings();
        settings.image = R.mipmap.ic_action_upload_light;
        settings.name = getResources().getString(R.string.action_export);
        arrayList.add(settings);

        settings = new Settings();
        settings.image = R.mipmap.ic_action_download_light;
        settings.name = getResources().getString(R.string.action_import);
        arrayList.add(settings);
    }

    public void convertToJson(Context context) {
        setVisibilityRlAndProgressBar(true);
        Msg msg = new Msg();
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ChatProvider.CONTENT_URI, null, null, null, null);
            if (cursor != null) {
                if (cursor.getCount() == 0) {
                    setVisibilityRlAndProgressBar(false);
                    Utils.showError(BackupActivity.this, Utils.CODE_NO_MESSENGE, Utils.NO_MESSAGE);
                } else if (cursor.moveToFirst()) {
                    ArrayList<array> arrays = new ArrayList<>();
                    for (int i = 0; i < cursor.getCount(); i++) {
                        MessageModel current = MessageModel.getForJson(context, cursor, i);
                        array array = new array();
                        if (current != null) {
                            array.setBody(URLEncoder.encode(current.getMessage(), "UTF-8").replace("+", "%20"));
                            array.setId(current.getThreadId());
                            array.setFrom(current.getFrom());
                            array.setTo(current.getTo());
                            if (current.getIsGroup()) {
                                if (current.getFrom().equals(Utils.getProfile(BackupActivity.this).getOXUser())) {
                                    array.setFrom(current.getFrom());
                                    array.setTo(current.getGroupId());
                                } else {
                                    array.setFrom(current.getGroupId());
                                    array.setTo(Utils.getProfile(BackupActivity.this).getOXUser());
                                }
                            }
                            array.setType(current.getType());
                            subject subject = new subject();
                            subject.setSenddate(current.getTime());
                            subject.setStatus("0");
                            subject.setGroupid(current.getGroupId() == null ? null : current.getGroupId().equals("") ? null : current.getGroupId());
                            subject.setFrom(current.getFrom());
                            subject.setGroupname(current.getGroupName() == null ? null : current.getGroupName().equals("") ? null : current.getGroupName());
                            subject.setIsdelete(String.valueOf(current.getIsDelete()));
                            subject.setIsgroup(String.valueOf(current.getIsGroup()));
                            subject.setIsimage(String.valueOf(current.getIsImage()));
                            subject.setIsgroupupdate(String.valueOf(current.getIsGroupUpdate()));
                            array.setSubject(subject);
                            arrays.add(array);
                        } else {
                            setVisibilityRlAndProgressBar(false);
                        }
                    }
                    msg.setArrays(arrays);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            setVisibilityRlAndProgressBar(false);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (msg.getArrays() != null) {
                if (msg.getArrays().size() > 0) {
                    Gson gson = new Gson();
                    String json = gson.toJson(msg);
                    writeToFile(json, context);
                }
            }
        }
    }

    public void writeToFile(String json, Context context) {
        String timestamp = String.valueOf(new Date().getTime());
        String filename = timestamp + ".bak";
        try {
            StringBuilder builder = new StringBuilder();
            builder.append(json);

            File file = new File(Environment.getExternalStorageDirectory() + "/" +
                    context.getResources().getString(R.string.folder_name) + "/" + "/" + context.getResources().getString(R.string.my_drive) + "/" +
                    context.getResources().getString(R.string.messenger_backup_folder));
            if (!file.exists()) {
                file.mkdirs();
            }
            BufferedWriter bos = new BufferedWriter(new FileWriter(file.getAbsolutePath() + "/" + filename));
            bos.write(builder.toString());
            bos.flush();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
            setVisibilityRlAndProgressBar(false);
        } finally {
            file = new File(Environment.getExternalStorageDirectory() + "/" +
                    context.getResources().getString(R.string.folder_name) + "/" +
                    context.getResources().getString(R.string.my_drive) + "/" +
                    "/" + context.getResources().getString(R.string.messenger_backup_folder) + "/" + filename);
            UploadDialogFragment uploadDialogFragment = UploadDialogFragment.newInstance(BackupActivity.this);
            uploadDialogFragment.setCancelable(false);
            uploadDialogFragment.show(getSupportFragmentManager(), "");
        }
    }

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (position == 1) {
                    if (Utils.isConnectingToInternet(BackupActivity.this)) {
                        deleteAll(BackupActivity.this);
                        setVisibilityRlAndProgressBar(true);
                        RefreshOperation operation = new RefreshOperation(BackupActivity.this, true, "0",
                                FileUtils.PATH_SEPARATOR, null, null, false);
                        operation.execute(mClient, BackupActivity.this, mHandler);
                        parentId = "0";
                    } else {
                        Utils.showNoConnection(BackupActivity.this);
                    }
                } else if (position == 0) {
                    if (Utils.isConnectingToInternet(BackupActivity.this)) {
                        deleteAll(BackupActivity.this);
                        convertToJson(BackupActivity.this);
                    } else {
                        Utils.showNoConnection(BackupActivity.this);
                    }
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    public static void deleteAll(Context context) {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/" +
                context.getResources().getString(R.string.folder_name) + "/" + "/" +
                context.getResources().getString(R.string.my_drive) + "/" +
                context.getResources().getString(R.string.messenger_backup_folder));
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return;
            }
        } else {
            boolean isDirDeleted = Utils.deleteDirAll(mediaStorageDir);
            if (isDirDeleted) {
                mediaStorageDir.mkdirs();
            }
        }
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, SettingsActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_backup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                startActivity(new Intent(BackupActivity.this, SettingsActivity.class));
                finish();
                return true;
            default:
                return false;
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                if (intent.getBooleanExtra("isComplete", false)) {
                    setVisibilityRlAndProgressBar(false);
                    Utils.showError(BackupActivity.this, "", "Success uploaded");
                } else if (intent.getBooleanExtra("isFail", false)) {
                    setVisibilityRlAndProgressBar(false);
                } else if (intent.getBooleanExtra("isCreated", false)) {
                    OCFile ocFile = new OCFile();
                    ocFile.setRemotePath("/" + getString(R.string.messenger_backup_folder) + "/" + file.getName());
                    ocFile.setMimeType(Utils.getLocalFileMimiType(BackupActivity.this, file));

                    Intent mIntent = new Intent(BackupActivity.this, UploadIntentService.class);
                    mIntent.putExtra("OCFile", (Parcelable) ocFile);
                    mIntent.putExtra("StoragePath", file.getAbsolutePath());
                    mIntent.putExtra("ParentId", "0");
                    startService(mIntent);
                } else if (intent.getBooleanExtra("isDownloaded", false)) {
                    OCFile file = intent.getParcelableExtra("OCFile");
                    handlerControl(file);
                } else if (intent.getBooleanExtra("isUpload", false)) {
                    final CreateFolderOperation mCreateOperation = new CreateFolderOperation(getBaseContext(),
                            true, "0", FileUtils.PATH_SEPARATOR, getBaseContext().getString(R.string.messenger_backup_folder), false, true);
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                mCreateOperation.execute(mClient);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                }
            } else if (action.equals(DriveActivity.ACTION)) {
                if (!intent.getBooleanExtra("error", false)) {
                    if (parentId.equals("0")) {
                        Cursor cursor = null;
                        try {
                            cursor = getContentResolver().query(FileProvider.CONTENT_URI, null,
                                    DBHelper.FILE_REMOTE_PATH + "=?", new String[]{FileUtils.PATH_SEPARATOR
                                            + getString(R.string.messenger_backup_folder) + FileUtils.PATH_SEPARATOR}, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                for (int i = 0; i < cursor.getCount(); i++) {
                                    final OCFile file = DriveActivity.getOCFile(cursor, i);
                                    if (file.getName().equals(getString(R.string.messenger_backup_folder))) {
                                        parentId = file.getRemoteId();
                                        break;
                                    }
                                }
                            } else {
                                setVisibilityRlAndProgressBar(false);
                                Utils.showError(BackupActivity.this, Utils.CODE_NO_BACKUP, Utils.NO_BACKUP);
                            }
                        } catch (Exception ex) {
                            setVisibilityRlAndProgressBar(false);
                            ex.fillInStackTrace();
                        } finally {
                            if (cursor != null) {
                                cursor.close();
                            }
                            RefreshOperation operation = new RefreshOperation(BackupActivity.this, true, parentId,
                                    FileUtils.PATH_SEPARATOR + getString(R.string.messenger_backup_folder) + FileUtils.PATH_SEPARATOR, null, null, false);
                            operation.execute(mClient, BackupActivity.this, mHandler);
                        }
                    } else {
                        Cursor cursor = null;
                        try {
                            cursor = getContentResolver().query(FileProvider.CONTENT_URI, null, DBHelper.FILE_REMOTE_PATH + " LIKE ? AND " + DBHelper.FILE_CONTENT_TYPE + "!=? AND " + DBHelper.FILE_NAME + " LIKE ? ",
                                    new String[]{"%" + FileUtils.PATH_SEPARATOR + getString(R.string.messenger_backup_folder) + FileUtils.PATH_SEPARATOR + "%", "DIR", "%.bak%"}, DBHelper.FILE_MODIFIED + " DESC ");
                            if (cursor != null && cursor.moveToFirst()) {
                                final OCFile file = DriveActivity.getOCFile(cursor, 0);
                                Intent mIntent = new Intent(BackupActivity.this, DownloadIntentService.class);
                                mIntent.putExtra("OCFile", (Parcelable) file);
                                mIntent.putExtra("id", cursor.getInt(cursor.getColumnIndex(DBHelper.FILE_ID)));
                                BackupActivity.this.startService(mIntent);
                            } else {
                                setVisibilityRlAndProgressBar(false);
                                Utils.showError(BackupActivity.this, Utils.CODE_NO_BACKUP, Utils.NO_BACKUP);
                            }
                        } catch (Exception ex) {
                            setVisibilityRlAndProgressBar(false);
                            ex.fillInStackTrace();
                        } finally {
                            if (cursor != null) {
                                cursor.close();
                            }
                        }
                    }
                } else if (intent.getBooleanExtra("error", false)) {
                    setVisibilityRlAndProgressBar(false);
                    Utils.showError(BackupActivity.this, Utils.CODE_UNKNOWN_DRIVE_ACCOUNT, Utils.UNKNOWN_DRIVE_ACCOUNT);
                } else if (intent.getBooleanExtra("connection_not_available", false)) {
                    Utils.showNoConnection(BackupActivity.this);
                    setVisibilityRlAndProgressBar(false);
                }
            }
        }
    };

    public void handlerControl(final OCFile file) {
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                File mFile = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.folder_name)
                        + "/" + getResources().getString(R.string.my_drive) + file.getRemotePath());
                if (mFile.exists()) {
                    readSavedData(mFile);
                } else {
                    Intent intent = new Intent(BackupActivity.ACTION);
                    intent.putExtra("isDownloaded", true);
                    LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(intent);
                    ChatDBHelper.resetCount(getBaseContext());
                }
            }
        };
        handler.postDelayed(r, 1000);
    }

    public void readSavedData(File file) {
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
            }
            br.close();
            Gson gson = new Gson();
            Msg msg = gson.fromJson(text.toString(), Msg.class);
            ArrayList<MessageModel> messageModels = new ArrayList<>();
            if (msg != null) {
                if (msg.getArrays() != null) {
                    for (int i = 0; i < msg.getArrays().size(); i++) {
                        MessageModel messageModel = new MessageModel();
                        messageModel.setTo(msg.getArrays().get(i).getTo());
                        messageModel.setFrom(msg.getArrays().get(i).getFrom());
                        messageModel.setIsGroup(Boolean.valueOf(msg.getArrays().get(i).getSubject().getIsgroup()));
                        if (messageModel.getIsGroup()) {
                            messageModel.setFrom(msg.getArrays().get(i).getSubject().getFrom());
                            messageModel.setTo(msg.getArrays().get(i).getSubject().getGroupid());
                        }
                        messageModel.setMessage(URLDecoder.decode(msg.getArrays().get(i).getBody(), "UTF-8"));
                        messageModel.setThreadId(msg.getArrays().get(i).getId());
                        messageModel.setGroupId(msg.getArrays().get(i).getSubject().getGroupid() == null ? "" : msg.getArrays().get(i).getSubject().getGroupid());
                        messageModel.setGroupName(msg.getArrays().get(i).getSubject().getGroupname() == null ? "" : msg.getArrays().get(i).getSubject().getGroupname());
                        messageModel.setIsGroupUpdate(Boolean.valueOf(msg.getArrays().get(i).getSubject().getIsgroupupdate()));
                        messageModel.setIsImage(Boolean.valueOf(msg.getArrays().get(i).getSubject().getIsimage()));
                        messageModel.setIsDelete(Boolean.valueOf(msg.getArrays().get(i).getSubject().getIsdelete()));
                        messageModel.setTime(msg.getArrays().get(i).getSubject().getSenddate());
                        messageModel.setType(msg.getArrays().get(i).getType());
                        messageModel.setExtra("0");
                        messageModel.setMine(Utils.getProfile(getBaseContext()).getOXUser().equals(messageModel.getFrom()));
                        boolean isMessageExists = ChatDBHelper.isMessageExists(messageModel.getThreadId(), getBaseContext());

                        if (!isMessageExists) {
                            ChatDBHelper.addMessage(messageModel, getBaseContext());
                            messageModels.add(messageModel);
                            if (messageModel.getIsGroup()) {
                                if (!HttpHelper.isGroupExist(getBaseContext(), messageModel.getGroupId())) {
                                    try {
                                        ContentValues contentValues = new ContentValues();
                                        contentValues.put(DBHelper.GROUP_COLUMN_GROUP_ID, messageModel.getGroupId());
                                        contentValues.put(DBHelper.GROUP_COLUMN_NAME, messageModel.getGroupName());
                                        contentValues.put(DBHelper.GROUP_COLUMN_IS_DELETE, String.valueOf(messageModel.getIsDelete()));
                                        getContentResolver().insert(GroupProvider.CONTENT_URI, contentValues);
                                    } catch (Exception ex) {
                                        ex.fillInStackTrace();
                                    }
                                }

                                Profile profile = Utils.getProfile(getBaseContext());
                                if (!ChatDBHelper.isGroupMemberOxUserExists(getBaseContext(), messageModel.getGroupId(), profile.getOXUser())) {
                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put(DBHelper.GROUP_CHAT_COLUMN_GROUP_ID, messageModel.getGroupId());
                                    contentValues.put(DBHelper.GROUP_CHAT_COLUMN_OX_USER, profile.getOXUser());
                                    contentValues.put(DBHelper.GROUP_CHAT_COLUMN_CONTACT_NO, profile.getContactNo());
                                    contentValues.put(DBHelper.GROUP_CHAT_COLUMN_COUNTRY_IMAGE, profile.getCountryImage());
                                    contentValues.put(DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID, profile.getId());
                                    contentValues.put(DBHelper.GROUP_CHAT_COLUMN_POSITION, profile.getPosition());
                                    contentValues.put(DBHelper.GROUP_CHAT_COLUMN_IMAGE, profile.getProfileImage());
                                    contentValues.put(DBHelper.GROUP_CHAT_COLUMN_NAME, profile.getName());
                                    contentValues.put(DBHelper.GROUP_CHAT_COLUMN_OFFICE_NO, profile.getOfficeNo());
                                    getContentResolver().insert(GroupChatProvider.CONTENT_URI, contentValues);
                                }
                            }
                        }
                    }
                }
            }

            for (int j = 0; j < messageModels.size(); j++) {
                if (messageModels.get(j).getIsGroup()) {
                    Cursor cursor = null;
                    try {
                        cursor = getContentResolver().query(ChatProvider.CONTENT_URI, null, DBHelper.CHAT_COLUMN_GROUP_ID + "=?",
                                new String[]{messageModels.get(j).getGroupId()}, DBHelper.CHAT_COLUMN_CHAT_ID + " DESC ");
                        if (cursor != null && cursor.moveToFirst()) {
                            MessageModel messageModel = MessageModel.getForJson(getBaseContext(), cursor, 0);
                            boolean isMessageExist = ChatDBHelper.isLastMessageExist(messageModel.getThreadId(), getBaseContext());
                            if (!isMessageExist) {
                                ChatDBHelper.addLastMsg(messageModel, getBaseContext());
                            }
                        }
                    } catch (Exception ex) {
                        setVisibilityRlAndProgressBar(false);
                        ex.fillInStackTrace();
                    } finally {
                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                } else {
                    Cursor cursor = null;
                    try {
                        cursor = getContentResolver().query(ChatProvider.CONTENT_URI, null, DBHelper.CHAT_COLUMN_FROM + "=? AND " + DBHelper.CHAT_COLUMN_TO + "=?",
                                new String[]{messageModels.get(j).getFrom(), messageModels.get(j).getTo()}, DBHelper.CHAT_COLUMN_CHAT_ID + " DESC ");
                        if (cursor != null && cursor.moveToFirst()) {
                            MessageModel messageModel = MessageModel.getForJson(getBaseContext(), cursor, 0);
                            messageModel.setGroupId("");
                            messageModel.setGroupName("");
                            boolean isMessageExist = ChatDBHelper.isLastMessageExist(messageModel.getThreadId(), getBaseContext());
                            if (!isMessageExist) {
                                ChatDBHelper.addLastMsg(messageModel, getBaseContext());
                            }
                        }
                    } catch (Exception ex) {
                        ex.fillInStackTrace();
                    } finally {
                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                }
            }
        } catch (IOException e) {
            setVisibilityRlAndProgressBar(false);
            e.fillInStackTrace();
        } finally {
            setVisibilityRlAndProgressBar(false);
            Utils.showError(BackupActivity.this, "", "Success import");
            ChatDBHelper.resetCount(getBaseContext());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(ACTION);
        iff.addAction(DriveActivity.ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    public void onTransferProgress(long progressRate, long totalTransferredSoFar, long totalToTransfer, String fileAbsoluteName) {

    }

    @Override
    public void onRemoteOperationFinish(RemoteOperation caller, RemoteOperationResult result) {

    }
}
