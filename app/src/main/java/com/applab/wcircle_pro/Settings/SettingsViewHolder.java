package com.applab.wcircle_pro.Settings;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 4/12/2015.
 */
public class SettingsViewHolder extends RecyclerView.ViewHolder {
    TextView txtName;
    ImageView img;

    public SettingsViewHolder(View itemView) {
        super(itemView);
        txtName = (TextView) itemView.findViewById(R.id.txtName);
        img = (ImageView) itemView.findViewById(R.id.img);
    }
}
