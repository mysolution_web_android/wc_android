package com.applab.wcircle_pro.Settings;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.applab.wcircle_pro.Menu.NavigationDrawerFragment;
import com.applab.wcircle_pro.Profile.ChangePasswordActivity;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {
    //region variable
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private TextView txtToolbarTitle;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private SettingsAdapter adapter;
    private ArrayList<Settings> arrayList = new ArrayList<>();
    private TextView version, company;
    //endregion

    //region create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_setting));
        version = (TextView) findViewById(R.id.version);
        version.setText(this.getResources().getString(R.string.ver) + " " + Utils.getAppVersion(this));
        company = (TextView) findViewById(R.id.company);
        company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = getResources().getString(R.string.company_url);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        drawerFragment.setSelectedPosition(13);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        setSettings();
        adapter = new SettingsAdapter(this, arrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        setTouchListener();
        toolbar.setNavigationIcon(R.mipmap.action_arrow_prev_white);
        toolbar.setNavigationOnClickListener(toolbarOnClickListener);
    }
    //endregion

    private void setSettings() {
        Settings settings = new Settings();
        settings.image = R.mipmap.menu_change_password;
        settings.name = getResources().getString(R.string.title_activity_change_password);
        arrayList.add(settings);

        settings = new Settings();
        settings.image = R.mipmap.menu_backup;
        settings.name = getResources().getString(R.string.title_activity_backup);
        arrayList.add(settings);
    }

    private View.OnClickListener toolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utils.clearPreviousActivity(SettingsActivity.this);
        }
    };

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (position == 0) {
                    Intent intent = new Intent(SettingsActivity.this, ChangePasswordActivity.class);
                    startActivity(intent);
                } else if (position == 1) {
                    startActivity(new Intent(SettingsActivity.this, BackupActivity.class));
                    finish();
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }


    @Override
    public void onBackPressed() {
        Utils.clearPreviousActivity(SettingsActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return drawerFragment.mDrawerToggle.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
    }
}
