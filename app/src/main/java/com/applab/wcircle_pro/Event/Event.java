package com.applab.wcircle_pro.Event;

import android.database.Cursor;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class Event {

    @Expose
    private String Id;
    @Expose
    private String Title;
    @Expose
    private String Description;
    @Expose
    private String Location;
    @Expose
    private String StartDateTime;
    @Expose
    private String EndDateTime;
    @Expose
    private String CreatedDate;
    @Expose
    private String StartTime;

    private String fileTitle;
    private String capacity;
    private String date;
    private int img;
    private int icon;

    @Expose
    private String TypeId;
    @Expose
    private String Type;
    @Expose
    private String AllDay;
    @Expose
    private String FeatureImage;
    @Expose
    private String Banner;
    @Expose
    private String Message;

    @Expose
    private boolean NewCreate = false;

    public boolean getNewCreate() {
        return NewCreate;
    }

    public void setNewCreate(boolean newCreate) {
        NewCreate = newCreate;
    }

    /**
     * @return The Message
     */
    public String getMessage() {
        return Message;
    }

    /**
     * @param Message The Message
     */
    public void setMessage(String Message) {
        this.Message = Message;
    }

    @Expose
    private List<Attachment> Attachments = new ArrayList<Attachment>();

    public String getTypeId() {
        return TypeId;
    }

    public void setTypeId(String typeId) {
        TypeId = typeId;
    }

    public String getAllDay() {
        return AllDay;
    }

    public void setAllDay(String allDay) {
        AllDay = allDay;
    }

    public List<Attachment> getAttachments() {
        return Attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        Attachments = attachments;
    }

    public String getBanner() {
        return Banner;
    }

    public void setBanner(String banner) {
        Banner = banner;
    }

    public String getFeatureImage() {
        return FeatureImage;
    }

    public void setFeatureImage(String featureImage) {
        FeatureImage = featureImage;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    /**
     * @return The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * @return The Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param Title The Title
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * @return The Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description The Description
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     * @param Location The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     * @return The StartDateTime
     */
    public String getStartDateTime() {
        return StartDateTime;
    }

    /**
     * @param StartDateTime The StartDateTime
     */
    public void setStartDateTime(String StartDateTime) {
        this.StartDateTime = StartDateTime;
    }

    /**
     * @return The EndDateTime
     */
    public String getEndDateTime() {
        return EndDateTime;
    }

    /**
     * @param EndDateTime The EndDateTime
     */
    public void setEndDateTime(String EndDateTime) {
        this.EndDateTime = EndDateTime;
    }

    /**
     * @return The CreatedDate
     */
    public String getCreatedDate() {
        return CreatedDate;
    }

    /**
     * @param CreatedDate The CreatedDate
     */
    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    /**
     * @return The CreatedDate
     */
    public String getStartTime() {
        return StartTime;
    }

    /**
     * @param StartTime The StartTime
     */
    public void setStartTime(String StartTime) {
        this.StartTime = StartTime;
    }

    public static Event getEvent(Cursor cursor, int position) {
        Event event = new Event();
        cursor.moveToPosition(position);
        event.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_TITLE)));
        event.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_DESCRIPTION)));
        event.setStartDateTime(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_START_DATE)));
        event.setEndDateTime(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_END_DATE)));
        event.setLocation(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_LOCATION)));
        event.setAllDay(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_ALL_DAY)));
        event.setType(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_TYPE)));
        event.setTypeId(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_TYPE_ID)));
        event.setFeatureImage(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_FEATURE_IMAGE)));
        event.setBanner(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_BANNER)));
        event.setCreatedDate(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_CREATE_DATE)));
        return event;
    }
}