package com.applab.wcircle_pro.Event;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by user on 27/10/2015.
 */
public class DownloadDialogFragment extends DialogFragment {
    private String mMessage, mUrl, mFileName, mId;
    private TextView mTxtTitle, mTxtMessage;
    private ImageView mBtnCancel;
    private LinearLayout mBtnYes, mBtnNo;
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private String TAG = "DOWNLOAD EVENT";

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DownloadDialogFragment newInstance(Context context, String message, String url,
                                                     String fileName, String id) {
        DownloadDialogFragment frag = new DownloadDialogFragment();
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putString("url", url);
        args.putString("fileName", fileName);
        args.putString("id", id);
        mContext = context;
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mMessage = getArguments().getString("message");
        mUrl = getArguments().getString("url");
        mFileName = getArguments().getString("fileName");
        mId = getArguments().getString("id");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_dialog, null);
        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtMessage = (TextView) v.findViewById(R.id.txtMessage);
        mTxtTitle.setText(getString(R.string.download));
        mTxtMessage.setText(mMessage);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnYes = (LinearLayout) v.findViewById(R.id.btnYes);
        mBtnNo = (LinearLayout) v.findViewById(R.id.btnNo);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);
        mBtnYes.setOnClickListener(btnYesOnClickListener);
        mBtnNo.setOnClickListener(btnNoOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DownloadDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnYesOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utils.downloadFile(mUrl, mContext, mFileName, mContext.getResources().getString(R.string.attachment), "Attachment", mId);
            if (Utils.getStatus(Utils.EVENT_DOWNLOAD, mContext)) {
                HttpHelper.getGsonDownload(mId, mContext, TAG);
            }
            DownloadDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnNoOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DownloadDialogFragment.this.getDialog().cancel();
        }
    };
}

