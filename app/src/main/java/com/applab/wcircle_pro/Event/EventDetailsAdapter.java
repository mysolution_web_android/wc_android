package com.applab.wcircle_pro.Event;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

public class EventDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private Cursor cursorEvent, cursorAttachment;
    private String TAG = "EVENT";
    private boolean isLongClick = false;
    private FragmentManager fragmentManager;

    public EventDetailsAdapter(Context context, Cursor cursorEvent, Cursor cursorAttachment, FragmentManager fragmentManager) {
        this.inflater = LayoutInflater.from(context);
        this.cursorAttachment = cursorAttachment;
        this.cursorEvent = cursorEvent;
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    class EventDetails extends RecyclerView.ViewHolder {
        ProgressBar progressBar;
        ImageView banner;
        TextView txtEventTitle, txtDate, txtTime, txtLocation, txtTitleLocation, txtDesc;

        public EventDetails(View itemView) {
            super(itemView);
            banner = (ImageView) itemView.findViewById(R.id.banner);
            txtEventTitle = (TextView) itemView.findViewById(R.id.txtEventTitle);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtLocation = (TextView) itemView.findViewById(R.id.txtLocation);
            txtTitleLocation = (TextView) itemView.findViewById(R.id.txtTitleLocation);
            txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);
            txtTime = (TextView) itemView.findViewById(R.id.txtTime);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress);
        }
    }

    class EventAttachment extends RecyclerView.ViewHolder {
        ImageView imgIcon, imgStatus;
        TextView txtTitle, txtCapacity, txtDate;
        LinearLayout lvData;

        public EventAttachment(View itemView) {
            super(itemView);
            imgIcon = (ImageView) itemView.findViewById(R.id.imgIcon);
            imgStatus = (ImageView) itemView.findViewById(R.id.imgStatus);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtCapacity = (TextView) itemView.findViewById(R.id.txtCapacity);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            lvData = (LinearLayout) itemView.findViewById(R.id.lvData);
        }
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 1;
        if (position == 0) {
            viewType = 0;
        }
        return viewType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("EventDetailsViewHolder", "onCreateViewHolder called");
        if (viewType == 0) {
            View view = inflater.inflate(R.layout.custom_event_details, parent, false);
            return new EventDetails(view);
        } else {
            View view = inflater.inflate(R.layout.custom_event_attachment, parent, false);
            return new EventAttachment(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        switch (holder.getItemViewType()) {
            case 0:
                if (position == 0) {
                    Event event = Event.getEvent(cursorEvent, 0);
                    if (cursorEvent != null) {
                        final EventDetails eventDetailsHolder = (EventDetails) holder;
                        if (event.getBanner() != null) {
                            if (event.getBanner().equals("")) {
                                eventDetailsHolder.banner.setVisibility(View.GONE);
                            } else {
                                eventDetailsHolder.banner.setVisibility(View.VISIBLE);
                                Glide.with(context)
                                        .load(event.getBanner())
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(new GlideDrawableImageViewTarget(eventDetailsHolder.banner) {
                                            @Override
                                            public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                                                super.onResourceReady(drawable, anim);
                                                eventDetailsHolder.progressBar.setVisibility(View.GONE);
                                            }
                                        });
                            }
                        } else {
                            eventDetailsHolder.banner.setVisibility(View.GONE);
                        }
                        eventDetailsHolder.txtEventTitle.setText(event.getTitle());
                        String range = "";
                        if (event.getStartDateTime() != null) {
                            if (Utils.setCalendarDate(Utils.DATE_FORMAT, "d/M/yyyy", event.getStartDateTime()).equals(Utils.setCalendarDate(Utils.DATE_FORMAT, "d/M/yyyy", event.getEndDateTime()))) {
                                if (!event.getAllDay().equals("Yes")) {
                                    range = Utils.setDate(Utils.DATE_FORMAT, "MMM dd, h:mm a", event.getStartDateTime()) + " - " + Utils.setDate(Utils.DATE_FORMAT, "h:mm a", event.getEndDateTime());
                                } else {
                                    range = Utils.setDate(Utils.DATE_FORMAT, "MMM dd, h.mm a", event.getStartDateTime()) + " " + context.getString(R.string.all_day);
                                }
                            } else {
                                if (!event.getAllDay().equals("Yes")) {
                                    range = Utils.setDate(Utils.DATE_FORMAT, "MMM dd, h.mm a", event.getStartDateTime()) + " - " + Utils.setDate(Utils.DATE_FORMAT, "MMM dd, h.mm a", event.getEndDateTime());
                                } else {
                                    range = Utils.setDate(Utils.DATE_FORMAT, "MMM dd, h.mm a", event.getStartDateTime()) + " - " + Utils.setDate(Utils.DATE_FORMAT, "MMM dd, ", event.getEndDateTime()) + context.getString(R.string.all_day);
                                }
                            }
                        }
                        Log.i(TAG, "Date :" + event.getStartDateTime());
                        eventDetailsHolder.txtDate.setText(range);
                        eventDetailsHolder.txtTime.setText(event.getStartTime());
                        eventDetailsHolder.txtLocation.setText(event.getLocation());
                        eventDetailsHolder.txtDesc.setText(event.getDescription());
                        eventDetailsHolder.progressBar.setVisibility(View.GONE);
                        eventDetailsHolder.txtTitleLocation.setText(event.getTitle());
                    }
                }
                break;
            case 1:
                if (position > 0) {
                    if (cursorAttachment != null) {
                        if (cursorAttachment.getCount() > 0) {
                            Attachment attachment = Attachment.getAttachment(cursorAttachment, position - 1);
                            EventAttachment eventAttachmentHolder = (EventAttachment) holder;
                            if (attachment.getPath() != null) {
                                if (Utils.localFileExists(attachment.getPath())) {
                                    eventAttachmentHolder.imgStatus.setVisibility(View.VISIBLE);
                                } else {
                                    eventAttachmentHolder.imgStatus.setVisibility(View.GONE);
                                }
                            }
                            eventAttachmentHolder.txtTitle.setText(attachment.getTitle());
                            eventAttachmentHolder.txtCapacity.setText(attachment.getFileSize());
                            String[] result = attachment.getAttachment().split("\\.");
                            String type = result[result.length - 1];
                            eventAttachmentHolder.imgIcon.setImageResource(Utils.getFileType(type));
                            String uploadDate = attachment.getUploadDate();
                            uploadDate = Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", uploadDate);
                            eventAttachmentHolder.txtDate.setText(uploadDate);
                            eventAttachmentHolder.lvData.setTag(attachment);
                        }
                    }
                }
                break;
        }
    }

    private Attachment getAttachment(int position, Cursor cursor) {
        cursor.moveToPosition(position);
        Attachment attachment = new Attachment();
        attachment.setAttachment(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_ATTACHMENT)));
        attachment.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_DESCRIPTION)));
        attachment.setEventId(cursor.getInt(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_EVENT_ID)));
        attachment.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_ID)));
        attachment.setFileSize(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_FILE_SIZE)));
        attachment.setUploadDate(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_UPLOAD_DATE)));
        attachment.setUploadBy(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_UPLOAD_BY)));
        attachment.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_TITLE)));
        return attachment;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        int total = 0;
        if (cursorEvent != null) {
            if (cursorEvent.getCount() > 0) {
                total += cursorEvent.getCount();
                if (cursorAttachment != null) {
                    if (cursorAttachment.getCount() > 0) {
                        total += cursorAttachment.getCount();
                    }
                }
            }
        }
        if (cursorEvent != null && cursorAttachment != null) {
            if (total > cursorEvent.getCount() + cursorAttachment.getCount()) {
                total = cursorEvent.getCount() + cursorAttachment.getCount();
            }
        }
        return total;
    }


    public Cursor swapCursorEvent(Cursor cursorEvent) {
        if (this.cursorEvent == cursorEvent) {
            return null;
        }
        Cursor oldCursor = this.cursorEvent;
        this.cursorEvent = cursorEvent;
        if (cursorEvent != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    public Cursor swapCursorAttachment(Cursor cursorAttachment) {
        if (this.cursorAttachment == cursorAttachment) {
            return null;
        }
        Cursor oldCursor = this.cursorAttachment;
        this.cursorAttachment = cursorAttachment;
        if (cursorAttachment != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            EventDetailsAdapter.this.notifyDataSetChanged();
        }
    };

    Handler handler1 = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            EventDetailsAdapter.this.notifyDataSetChanged();
        }
    };
}
