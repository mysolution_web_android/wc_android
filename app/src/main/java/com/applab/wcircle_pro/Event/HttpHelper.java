package com.applab.wcircle_pro.Event;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Dashboard.DashboardProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 23/11/2015.
 */

public class HttpHelper {

    //region Event
    public static void getEventGson(Context context, final int pageNo, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        final Date lastUpdateDate = getEventLastUpdate(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Event?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) +
                "&PageNo=" + Utils.encode(pageNo) + "&LastUpdated=" +
                Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getEventRow(context, pageNo) > 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Event?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER)
                    + "&PageNo=" + Utils.encode(pageNo) + "&LastUpdated=" +
                    Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getEventRow(context, pageNo) > 0);
        }
        final String finalUrl = url;
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<EventChecking> mGsonRequest = new GsonRequest<EventChecking>(
                Request.Method.GET,
                finalUrl,
                EventChecking.class,
                headers,
                responseEventListener(context, pageNo, txtError, TAG),
                errorEventListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<EventChecking> responseEventListener(final Context context, final int pageNo, final TextView txtError, final String TAG) {
        return new Response.Listener<EventChecking>() {
            @Override
            public void onResponse(EventChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getEvent().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getEventLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(EventProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(EventCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(EventCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertEvent(response, context, pageNo, TAG);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(EventProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(EventCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(EventCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertEvent(response, context, pageNo, TAG);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(EventProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(EventCheckingProvider.CONTENT_URI, null, null);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(EventCheckingProvider.CONTENT_URI, null, null);
                                }
                                insertEvent(response, context, pageNo, TAG);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(EventProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(EventCheckingProvider.CONTENT_URI, null, null);
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    public static int getEventRow(Context context, int pageNo) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(EventProvider.CONTENT_URI,
                    null, DBHelper.EVENT_COLUMN_EVENT_ID + "=?",
                    new String[]{String.valueOf(pageNo)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getEventLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.EVENT_CHECKING_COLUMN_LAST_UPDATE;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(EventCheckingProvider.CONTENT_URI,
                    null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static Response.ErrorListener errorEventListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }

    public static boolean isEventExists(Context context, Object eventId) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(EventProvider.CONTENT_URI, null,
                    DBHelper.EVENT_COLUMN_EVENT_ID + "=?", new String[]{String.valueOf(eventId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;

    }

    public static boolean isAttachmentExists(Context context, Object attachmentId) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(AttachmentProvider.CONTENT_URI, null,
                    DBHelper.ATTACHMENT_COLUMN_ATTACHMENT_ID + "=?", new String[]{String.valueOf(attachmentId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;

    }

    public static void insertEvent(EventChecking result, Context context, int pageNo, String TAG) {
        DBHelper helper = new DBHelper(context);
        File file = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" + context.getResources().getString(R.string.attachment));
        try {
            ContentValues contentValuesEventChecking = new ContentValues();
            contentValuesEventChecking.put(DBHelper.EVENT_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesEventChecking.put(DBHelper.EVENT_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesEventChecking.put(DBHelper.EVENT_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesEventChecking.put(DBHelper.EVENT_CHECKING_COLUMN_LEVEL, pageNo);
            context.getContentResolver().insert(EventCheckingProvider.CONTENT_URI, contentValuesEventChecking);
            ContentValues[] contentValueses = new ContentValues[result.getEvent().size()];
            for (int i = 0; i < result.getEvent().size(); i++) {
                ContentValues contentValues = new ContentValues();
                String startDate = result.getEvent().get(i).getStartDateTime();
                String endDate = result.getEvent().get(i).getEndDateTime();
                String createDate = result.getEvent().get(i).getCreatedDate();
                contentValues.put(DBHelper.EVENT_COLUMN_EVENT_ID, result.getEvent().get(i).getId());
                contentValues.put(DBHelper.EVENT_COLUMN_TITLE, result.getEvent().get(i).getTitle());
                contentValues.put(DBHelper.EVENT_COLUMN_DESCRIPTION, result.getEvent().get(i).getDescription());
                contentValues.put(DBHelper.EVENT_COLUMN_LOCATION, result.getEvent().get(i).getLocation());
                contentValues.put(DBHelper.EVENT_COLUMN_LEVEL, pageNo);
                contentValues.put(DBHelper.EVENT_COLUMN_START_DATE, startDate);
                contentValues.put(DBHelper.EVENT_COLUMN_END_DATE, endDate);
                contentValues.put(DBHelper.EVENT_COLUMN_TYPE_ID, result.getEvent().get(i).getTypeId());
                contentValues.put(DBHelper.EVENT_COLUMN_TYPE, result.getEvent().get(i).getType());
                contentValues.put(DBHelper.EVENT_COLUMN_ALL_DAY, result.getEvent().get(i).getAllDay());
                contentValues.put(DBHelper.EVENT_COLUMN_FEATURE_IMAGE, result.getEvent().get(i).getFeatureImage());
                contentValues.put(DBHelper.EVENT_COLUMN_BANNER, result.getEvent().get(i).getBanner());
                contentValues.put(DBHelper.EVENT_COLUMN_CREATE_DATE, createDate);
                contentValues.put(DBHelper.EVENT_COLUMN_NEW_CREATE, String.valueOf(result.getEvent().get(i).getNewCreate()));
                ContentValues[] contentValuesesAttachment = new ContentValues[result.getEvent().get(i).getAttachments().size()];
                List<Attachment> arrayList = result.getEvent().get(i).getAttachments();
                for (int j = 0; i < arrayList.size(); j++) {
                    String uploadDate = arrayList.get(j).getUploadDate();
                    ContentValues contentValues1 = new ContentValues();
                    contentValues1.put(DBHelper.ATTACHMENT_COLUMN_ATTACHMENT_ID, arrayList.get(j).getId());
                    contentValues1.put(DBHelper.ATTACHMENT_COLUMN_EVENT_ID, result.getEvent().get(i).getId());
                    contentValues1.put(DBHelper.ATTACHMENT_COLUMN_TITLE, result.getEvent().get(i).getTitle());
                    contentValues1.put(DBHelper.ATTACHMENT_COLUMN_FILE_SIZE, arrayList.get(j).getFileSize());
                    contentValues1.put(DBHelper.ATTACHMENT_COLUMN_DESCRIPTION, arrayList.get(j).getDescription());
                    contentValues1.put(DBHelper.ATTACHMENT_COLUMN_ATTACHMENT, arrayList.get(j).getAttachment());
                    contentValues1.put(DBHelper.ATTACHMENT_COLUMN_UPLOAD_BY, arrayList.get(j).getUploadBy());
                    contentValues1.put(DBHelper.ATTACHMENT_COLUMN_UPLOAD_DATE, uploadDate);
                    String[] split = arrayList.get(j).getAttachment().split("/");
                    contentValues1.put(DBHelper.ATTACHMENT_COLUMN_PATH, file.getAbsolutePath() + "/" + split[split.length - 1]);
                    if (isAttachmentExists(context, arrayList.get(i).getId())) {
                        context.getContentResolver().delete(AttachmentProvider.CONTENT_URI,
                                DBHelper.ATTACHMENT_COLUMN_ATTACHMENT_ID + "=?",
                                new String[]{String.valueOf(arrayList.get(i).getId())});
                    }
                    contentValuesesAttachment[j] = contentValues1;
                }
                boolean isEmpty = true;
                for (Object ob : contentValuesesAttachment) {
                    if (ob != null) {
                        isEmpty = false;
                    }
                }
                long rowsInserted = 0;
                if (!isEmpty) {
                    rowsInserted = context.getContentResolver().bulkInsert(AttachmentProvider.CONTENT_URI, contentValuesesAttachment);
                    Log.i(TAG, String.valueOf(rowsInserted));
                }
                if (isEventExists(context, result.getEvent().get(i).getId())) {
                    context.getContentResolver().delete(EventProvider.CONTENT_URI, DBHelper.EVENT_COLUMN_EVENT_ID + "=?",
                            new String[]{String.valueOf(result.getEvent().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (Object ob : contentValueses) {
                if (ob != null) {
                    isEmpty = false;
                }
            }
            long rowsInserted = 0;
            if (!isEmpty) {
                rowsInserted = context.getContentResolver().bulkInsert(EventProvider.CONTENT_URI, contentValueses);
                Log.i(TAG, String.valueOf(rowsInserted));
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }
    //endregion

    //region Single Event
    public static void getSingleEventGson(Context context, int id, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String url = Utils.API_URL + "api/Event" + "/" + Utils.encode(id);
        GsonRequest<Event> mGsonRequest = new GsonRequest<Event>(
                Request.Method.GET,
                Utils.API_URL + "api/Event" + "/" + Utils.encode(id),
                Event.class,
                headers,
                responseSingleEventListener(context, id, txtError, TAG),
                errorSingleEventListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Event> responseSingleEventListener(final Context context,
                                                                       final int id, final TextView txtError,
                                                                       final String TAG) {
        return new Response.Listener<Event>() {
            @Override
            public void onResponse(Event response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        insertAttachment(response, context, TAG);
                    }
                }
            }
        };
    }

    public static Response.ErrorListener errorSingleEventListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }


    public static void insertAttachment(Event result, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" + context.getResources().getString(R.string.attachment));
            String startDate = result.getStartDateTime();
            String endDate = result.getEndDateTime();
            String createDate = result.getCreatedDate();
            ContentValues contentValuesEvent = new ContentValues();
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_EVENT_ID, result.getId());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_TITLE, result.getTitle());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_DESCRIPTION, result.getDescription());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_LOCATION, result.getLocation());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_START_DATE, startDate);
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_END_DATE, endDate);
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_TYPE_ID, result.getTypeId());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_TYPE, result.getType());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_ALL_DAY, result.getAllDay());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_FEATURE_IMAGE, result.getFeatureImage());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_BANNER, result.getBanner());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_CREATE_DATE, createDate);
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_NEW_CREATE, String.valueOf(result.getNewCreate()));
            if (!isEventExists(context, result.getId())) {
                context.getContentResolver().insert(EventProvider.CONTENT_URI, contentValuesEvent);
            } else {
                context.getContentResolver().update(EventProvider.CONTENT_URI, contentValuesEvent, DBHelper.EVENT_COLUMN_EVENT_ID + "=?", new String[]{String.valueOf(result.getId())});
            }
            ContentValues[] contentValuesesAttachment = new ContentValues[result.getAttachments().size()];
            List<Attachment> arrayList = result.getAttachments();
            context.getContentResolver().delete(AttachmentProvider.CONTENT_URI, null, null);
            for (int j = 0; j < arrayList.size(); j++) {
                String uploadDate = arrayList.get(j).getUploadDate();
                ContentValues contentValues1 = new ContentValues();
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_ATTACHMENT_ID, arrayList.get(j).getId());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_EVENT_ID, arrayList.get(j).getEventId());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_FILE_SIZE, arrayList.get(j).getFileSize());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_TITLE, arrayList.get(j).getTitle());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_DESCRIPTION, arrayList.get(j).getDescription());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_ATTACHMENT, arrayList.get(j).getAttachment());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_UPLOAD_BY, arrayList.get(j).getUploadBy());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_UPLOAD_DATE, uploadDate);
                String[] split = arrayList.get(j).getAttachment().split("/");
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_PATH, file.getAbsolutePath() + "/" + split[split.length - 1]);
                if (isAttachmentExists(context, arrayList.get(j).getId())) {
                    context.getContentResolver().delete(AttachmentProvider.CONTENT_URI, DBHelper.ATTACHMENT_COLUMN_ATTACHMENT_ID + "=?", new String[]{String.valueOf(arrayList.get(j).getId())});
                }
                contentValuesesAttachment[j] = contentValues1;
            }
            boolean isEmpty = true;
            for (Object ob : contentValuesesAttachment) {
                if (ob != null) {
                    isEmpty = false;
                }
            }
            long rowsInserted = 0;
            if (!isEmpty) {
                rowsInserted = context.getContentResolver().bulkInsert(AttachmentProvider.CONTENT_URI, contentValuesesAttachment);
                Log.i(TAG, String.valueOf(rowsInserted));
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }

    }
    //endregion

    //region Event read
    public static void getEventGsonReading(final Context context, final int id, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseListenerReading(context, id, TAG),
                errorListenerReading(TAG)) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" + Utils.getProfile(context).getId() +
                        ",\"section\":\"event\",\"action\":\"read\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerReading(final Context context, final int id, final String TAG) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ContentValues contentValues = new ContentValues();

                if (isBold(context, id)) {
                    contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, DashboardActivity.getNumber(context, "Events"));
                    context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                            contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Events"});
                    com.applab.wcircle_pro.Dashboard.HttpHelper.getIndicator(context, TAG, null);
                }

                contentValues = new ContentValues();
                contentValues.put(DBHelper.EVENT_COLUMN_NEW_CREATE, "false");
                context.getContentResolver().update(EventProvider.CONTENT_URI, contentValues,
                        DBHelper.EVENT_COLUMN_EVENT_ID + "=?", new String[]{String.valueOf(id)});
                Log.i(TAG, "Success send reading status");

            }
        };
    }

    public static boolean isBold(Context context, Object id) {
        boolean isBold = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(EventProvider.CONTENT_URI, null, DBHelper.EVENT_COLUMN_EVENT_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                if (!cursor.isNull(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_NEW_CREATE))) {
                    isBold = Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_NEW_CREATE)));
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isBold;
    }

    public static Response.ErrorListener errorListenerReading(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }
    //endregion

    //region Download
    public static void getGsonDownload(final String id, final Context context, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseListenerDownload(TAG),
                errorListenerDownload(TAG)) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" + Utils.getProfile(context).getId() +
                        ",\"section\":\"event\",\"action\":\"download\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerDownload(final String TAG) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Success send download status");
            }
        };
    }

    private static Response.ErrorListener errorListenerDownload(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }
    //endregion
}
