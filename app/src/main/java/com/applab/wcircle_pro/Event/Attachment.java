package com.applab.wcircle_pro.Event;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;

public class Attachment implements Parcelable {

    @Expose
    private Integer Id;
    @Expose
    private Integer EventId;
    @Expose
    private String FileSize;
    @Expose
    private String Title;
    @Expose
    private String Description;
    @Expose
    private String Attachment;
    @Expose
    private String UploadBy;
    @Expose
    private String UploadDate;
    private String path;

    private boolean canDownload = false;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean getCanDownload() {
        return canDownload;
    }

    public void setCanDownload(boolean canDownload) {
        this.canDownload = canDownload;
    }

    public String getUploadBy() {
        return UploadBy;
    }

    public void setUploadBy(String uploadBy) {
        UploadBy = uploadBy;
    }

    public String getUploadDate() {
        return UploadDate;
    }

    public void setUploadDate(String uploadDate) {
        UploadDate = uploadDate;
    }

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The EventId
     */
    public Integer getEventId() {
        return EventId;
    }

    /**
     * @param EventId The EventId
     */
    public void setEventId(Integer EventId) {
        this.EventId = EventId;
    }

    /**
     * @return The FileSize
     */
    public String getFileSize() {
        return FileSize;
    }

    /**
     * @param FileSize The FileSize
     */
    public void setFileSize(String FileSize) {
        this.FileSize = FileSize;
    }

    /**
     * @return The Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param Title The Title
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * @return The Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description The Description
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return The Attachment
     */
    public String getAttachment() {
        return Attachment;
    }

    /**
     * @param Attachment The Attachment
     */
    public void setAttachment(String Attachment) {
        this.Attachment = Attachment;
    }

    public static Attachment getAttachment(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        com.applab.wcircle_pro.Event.Attachment attachment = new Attachment();
        attachment.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_TITLE)));
        attachment.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_DESCRIPTION)));
        attachment.setFileSize(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_FILE_SIZE)));
        attachment.setUploadDate(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_UPLOAD_DATE)));
        attachment.setUploadBy(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_UPLOAD_BY)));
        attachment.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_ATTACHMENT_ID)));
        attachment.setEventId(cursor.getInt(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_EVENT_ID)));
        attachment.setAttachment(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_ATTACHMENT)));
        attachment.setPath(cursor.getString(cursor.getColumnIndex(DBHelper.ATTACHMENT_COLUMN_PATH)));
        return attachment;
    }

    public Attachment() {
    }

    public Attachment(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeInt(this.EventId);
        dest.writeString(this.FileSize);
        dest.writeString(this.Title);
        dest.writeString(this.Description);
        dest.writeString(this.FileSize);
        dest.writeString(this.Description);
        dest.writeString(this.Attachment);
        dest.writeString(this.UploadBy);
        dest.writeString(this.UploadDate);
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.Id = in.readInt();
        this.EventId = in.readInt();
        this.FileSize = in.readString();
        this.Title = in.readString();
        this.Description = in.readString();
        this.FileSize = in.readString();
        this.Description = in.readString();
        this.Attachment = in.readString();
        this.UploadBy = in.readString();
        this.UploadDate = in.readString();
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Attachment createFromParcel(Parcel in) {
            return new Attachment(in);
        }

        @Override
        public Attachment[] newArray(int size) {
            return new Attachment[size];
        }
    };


}