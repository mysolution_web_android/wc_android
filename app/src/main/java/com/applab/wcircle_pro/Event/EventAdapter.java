package com.applab.wcircle_pro.Event;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;


import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

/**
 * Created by user on 5/8/2015.
 */
public class EventAdapter extends RecyclerView.Adapter<EventViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;
    private String TAG = "Event";

    public EventAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("EventViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_event_row, parent, false);
        EventViewHolder holder = new EventViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Event current = getData(cursor, position);
        holder.title.setText(current.getTitle());
        holder.title.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        holder.date.setText(current.getStartDateTime());
        holder.date.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        holder.location.setText(current.getLocation());
        holder.location.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        holder.presenter.setText(current.getDescription());
        holder.presenter.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        final ProgressBar progressBar = holder.progress;
        Glide.with(context)
                .load(current.getFeatureImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new GlideDrawableImageViewTarget(holder.img) {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }

    public Event getData(Cursor cursor, int position) {
        Event event = new Event();
        cursor.moveToPosition(position);
        event.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_TITLE)));
        event.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_DESCRIPTION)));
        event.setStartDateTime(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_START_DATE)));
        event.setEndDateTime(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_END_DATE)));
        event.setFeatureImage(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_FEATURE_IMAGE)));
        event.setLocation(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_LOCATION)));
        event.setStartDateTime(Utils.setDate(Utils.DATE_FORMAT, "EEE, dd MMM yyyy 'at' h.mm a", event.getStartDateTime()));
        event.setNewCreate(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_NEW_CREATE))));
        return event;
    }

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            EventAdapter.this.notifyDataSetChanged();
        }
    };
}
