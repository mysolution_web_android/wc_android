package com.applab.wcircle_pro.Event;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

public class EventDetailsActivity extends AppCompatActivity implements
        SwipyRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar toolbar;
    private TextView txtToolbarTitle;
    private int id = 0;
    private Bundle bundle;
    private EventDetailsAdapter adapter;
    private RecyclerView recyclerView;
    private String TAG = "EVENT";
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private Cursor cursorEvent, cursorAttachment;
    private RelativeLayout fadeRL;
    private LinearLayoutManager linearLayoutManager;
    private PopupWindow popupWindow;
    private TextView txtError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_yellow));
        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_event_details));
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        bundle = getIntent().getExtras();
        txtError = (TextView) findViewById(R.id.txtError);
        id = bundle.getInt("id");
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new EventDetailsAdapter(EventDetailsActivity.this, cursorEvent, cursorAttachment, getSupportFragmentManager());
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        HttpHelper.getSingleEventGson(EventDetailsActivity.this, id, TAG, txtError);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        setOnTouchListener();
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        getSupportLoaderManager().initLoader(0, null, this);
        getSupportLoaderManager().initLoader(1, null, this);
    }

    private void setOnTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (position > 0) {
                    LinearLayout lvData = (LinearLayout) v.findViewById(R.id.lvData);
                    Attachment attachment = (Attachment) lvData.getTag();
                    if (attachment.getPath() != null) {
                        if (Utils.localFileExists(attachment.getPath())) {
                            Utils.openDocument(attachment.getPath(), EventDetailsActivity.this);
                        } else {
                            String message = Utils.DOWNLOAD_ALERT + EventDetailsActivity.this.getString(R.string.folder_name) + "/" + EventDetailsActivity.this.getString(R.string.attachment);
                            DownloadDialogFragment downloadDialogFragment = DownloadDialogFragment.newInstance(EventDetailsActivity.this, message,
                                    attachment.getAttachment(), attachment.getAttachment(), String.valueOf(attachment.getId()));
                            downloadDialogFragment.show(getSupportFragmentManager(), "");
                        }
                    } else {
                        String message = Utils.DOWNLOAD_ALERT + EventDetailsActivity.this.getString(R.string.folder_name) + "/" + EventDetailsActivity.this.getString(R.string.attachment);
                        DownloadDialogFragment downloadDialogFragment = DownloadDialogFragment.newInstance(EventDetailsActivity.this, message,
                                attachment.getAttachment(), attachment.getAttachment(), String.valueOf(attachment.getId()));
                        downloadDialogFragment.show(getSupportFragmentManager(), "");
                    }
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }
            }
        }
    };


    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, EventActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_event_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            startActivity(new Intent(this, EventActivity.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                getSupportLoaderManager().restartLoader(1, null, EventDetailsActivity.this);
            }
        }
    };


    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, iff);
        if (Utils.getStatus(Utils.EVENT_READ, EventDetailsActivity.this)) {
            HttpHelper.getEventGsonReading(EventDetailsActivity.this, id, TAG);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 1) {
            String selection = DBHelper.ATTACHMENT_COLUMN_EVENT_ID + "=?";
            String[] selectionArgs = {String.valueOf(this.id)};
            return new CursorLoader(getBaseContext(),
                    AttachmentProvider.CONTENT_URI, null,
                    selection, selectionArgs, DBHelper.ATTACHMENT_COLUMN_ID + " DESC ");
        } else if (id == 0) {
            return new CursorLoader(getBaseContext(), EventProvider.CONTENT_URI, null,
                    DBHelper.EVENT_COLUMN_EVENT_ID + "=?", new String[]{String.valueOf(this.id)}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 1) {
            cursorAttachment = data;
            adapter.swapCursorAttachment(data);
        } else if (loader.getId() == 0) {
            cursorEvent = data;
            adapter.swapCursorEvent(cursorEvent);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            HttpHelper.getSingleEventGson(EventDetailsActivity.this, id, TAG, txtError);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }
}
