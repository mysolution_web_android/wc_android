package com.applab.wcircle_pro.Event;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.applab.wcircle_pro.R;

/**
 * Created by user on 27/10/2015.
 */
public class EventDialogFragment extends DialogFragment {
    private Attachment mAttachment;
    private TextView mTxtTitle, mTxtDetails, mTxtSize,
            mTxtUploadedBy, mTxtUploadedDate;
    private ImageView mBtnCancel;
    private static AlertDialog.Builder builder;
    private static Context mContext;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static EventDialogFragment newInstance(Attachment attachment, Context context) {
        EventDialogFragment frag = new EventDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("Attachment", attachment);
        mContext = context;
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mAttachment = getArguments().getParcelable("Attachment");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_catalogue, null);
        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtTitle.setText(mAttachment.getTitle());
        mTxtDetails = (TextView) v.findViewById(R.id.txtDetails);
        mTxtDetails.setText(mAttachment.getDescription());
        mTxtSize = (TextView) v.findViewById(R.id.txtSize);
        mTxtSize.setText(mAttachment.getFileSize());
        mTxtUploadedBy = (TextView) v.findViewById(R.id.txtUploadedBy);
        mTxtUploadedBy.setText(mAttachment.getUploadBy());
        mTxtUploadedDate = (TextView) v.findViewById(R.id.txtUploadedDate);
        mTxtUploadedDate.setText(mAttachment.getUploadDate());
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            EventDialogFragment.this.getDialog().cancel();
        }
    };
}

