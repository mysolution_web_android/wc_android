package com.applab.wcircle_pro.Event;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/8/2015.
 */
public class EventViewHolder extends RecyclerView.ViewHolder {
    ProgressBar progress;
    TextView title;
    TextView presenter;
    TextView location;
    TextView date;
    ImageView img;
    RelativeLayout rlImage;

    public EventViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.txtName);
        presenter = (TextView) itemView.findViewById(R.id.txtPresenter);
        location = (TextView) itemView.findViewById(R.id.txtLocation);
        date = (TextView) itemView.findViewById(R.id.txtDate);
        img = (ImageView) itemView.findViewById(R.id.img);
        progress = (ProgressBar) itemView.findViewById(R.id.progress);
        rlImage = (RelativeLayout) itemView.findViewById(R.id.rlImage);
    }

}