package com.applab.wcircle_pro.Drive;

import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.FileUtils;
import com.owncloud.android.lib.resources.files.ReadRemoteFolderOperation;
import com.owncloud.android.lib.resources.files.RemoteFile;

import java.math.BigDecimal;

/**
 * Created by user on 8/10/2015.
 */

public class CheckQuotaOperation extends RemoteOperation {
    private BigDecimal mQuotaUsedBytes;
    private BigDecimal mQuotaAvailableBytes;
    private String mErrorMessage;

    public BigDecimal getQuotaAvailableBytes() {
        return mQuotaAvailableBytes;
    }

    public void setQuotaAvailableBytes(BigDecimal mQuotaAvailableBytes) {
        this.mQuotaAvailableBytes = mQuotaAvailableBytes;
    }

    public BigDecimal getQuotaUsedBytes() {
        return mQuotaUsedBytes;
    }

    public void setQuotaUsedBytes(BigDecimal mQuotaUsedBytes) {
        this.mQuotaUsedBytes = mQuotaUsedBytes;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

    public void setErrorMessage(String mErrorMessage) {
        this.mErrorMessage = mErrorMessage;
    }

    @Override
    protected RemoteOperationResult run(OwnCloudClient client) {
        ReadRemoteFolderOperation operation = new ReadRemoteFolderOperation(FileUtils.PATH_SEPARATOR);
        RemoteOperationResult result = operation.execute(client);
        if (result.isSuccess()) {
            RemoteFile current = (RemoteFile) result.getData().get(0);
            setQuotaUsedBytes(current.getQuotaUsedBytes());
            setQuotaAvailableBytes(current.getQuotaAvailableBytes());
        } else {
            if (result.getCode() == RemoteOperationResult.ResultCode.TIMEOUT) {
                setErrorMessage(Utils.OWNCLOUD_TIMEOUT);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.CANCELLED) {
                setErrorMessage(Utils.OWNCLOUD_TIMEOUT);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.WRONG_CONNECTION) {
                setErrorMessage(Utils.OWNCLOUD_TIMEOUT);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.INCORRECT_ADDRESS) {
                setErrorMessage(Utils.OWNCLOUD_TIMEOUT);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
                setErrorMessage(Utils.OWNCLOUD_TIMEOUT);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.ACCOUNT_NOT_FOUND) {
                setErrorMessage(Utils.OWNCLOUD_TIMEOUT);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.ACCOUNT_EXCEPTION) {
                setErrorMessage(Utils.OWNCLOUD_TIMEOUT);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.SSL_RECOVERABLE_PEER_UNVERIFIED) {
                setErrorMessage(Utils.OWNCLOUD_TIMEOUT);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.SSL_ERROR) {
                setErrorMessage(Utils.OWNCLOUD_TIMEOUT);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.UNKNOWN_ERROR) {
                setErrorMessage(Utils.OWNCLOUD_TIMEOUT);
            }else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
                setErrorMessage(Utils.UNKNOWN_MY_DRIVE_ACCOUNT);
            }else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
                setErrorMessage(Utils.UNKNOWN_MY_DRIVE_ACCOUNT);
            }
        }
        return result;
    }
}
