package com.applab.wcircle_pro.Drive;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.OwnCloudClientFactory;
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory;

import java.util.ArrayList;

/**
 * Created by admin on 13/8/2015.
 */
public class RenameDialogFragment extends DialogFragment {
    private OCFile mOCFile;
    private EditText mEdiName;
    private TextView mTxtTitle, mTxtError;
    private LinearLayout mLvError;
    private RenameFileOperation mRenameOperation;
    private ArrayList<String> mParentLevel = new ArrayList<>();
    private ArrayList<String> mRemotePathLevel = new ArrayList<>();
    private int mLevel;
    private OwnCloudClient mClient;
    private LinearLayout mBtnSubmit, mBtnExit;
    private ImageView mBtnCancel;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @param file File to rename.
     * @return Dialog ready to show.
     */
    public static RenameDialogFragment newInstance(OCFile file, int level, ArrayList<String> parentLevel, ArrayList<String> remotePathLevel) {
        RenameDialogFragment frag = new RenameDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("OCFile", file);
        args.putInt("level", level);
        args.putStringArrayList("parentLevel", parentLevel);
        args.putStringArrayList("remotePathLevel", remotePathLevel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Uri serverUri = Uri.parse(getString(R.string.server_base_url));
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, getActivity(), true);
        mClient.setCredentials(OwnCloudCredentialsFactory.newBasicCredentials(Utils.getProfile(getActivity()).getEmail(),
                DriveActivity.pref.getString("own_password", null)));

        mOCFile = getArguments().getParcelable("OCFile");
        mParentLevel = getArguments().getStringArrayList("parentLevel");
        mRemotePathLevel = getArguments().getStringArrayList("remotePathLevel");
        mLevel = getArguments().getInt("level");

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_folder, null);

        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.rename));

        mEdiName = (EditText) v.findViewById(R.id.ediName);
        mEdiName.requestFocus();
        mEdiName.setHint(getResources().getString(R.string.name1));
        mEdiName.requestFocus();

        mTxtError = (TextView) v.findViewById(R.id.txtError);
        mLvError = (LinearLayout) v.findViewById(R.id.lvError);

        mBtnSubmit = (LinearLayout) v.findViewById(R.id.btnSubmit);
        mBtnExit = (LinearLayout) v.findViewById(R.id.btnExit);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);

        mBtnSubmit.setOnClickListener(btnSubmitOnClickListener);
        mBtnExit.setOnClickListener(btnExitOnClickListener);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        // Build the dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            String message = DriveActivity.nameChecking(mEdiName.getText().toString(), mRemotePathLevel.get(mLevel) + "/"
                    + mEdiName.getText().toString(), mClient, !mOCFile.getMimeType().equals("DIR"), getActivity());
            if (!message.equals("")) {
                mLvError.setVisibility(View.VISIBLE);
                mTxtError.setText(message);
            } else {
                mLvError.setVisibility(View.GONE);
                if (!Utils.isConnectingToInternet(getActivity())) {
                    Utils.showNoConnection(getActivity());
                } else {
                    mRenameOperation = new RenameFileOperation(getActivity(), true, mParentLevel.get(mLevel),
                            mOCFile, mEdiName.getText().toString(), mRemotePathLevel.get(mLevel));
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                mRenameOperation.execute(mClient);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                    RenameDialogFragment.this.getDialog().cancel();
                }
            }
        }
    };

    private View.OnClickListener btnExitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            RenameDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            RenameDialogFragment.this.getDialog().cancel();
        }
    };
}
