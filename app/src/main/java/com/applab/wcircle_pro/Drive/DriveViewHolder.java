package com.applab.wcircle_pro.Drive;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/29/2015.
 */
public class DriveViewHolder extends RecyclerView.ViewHolder {
    TextView title;
    TextView capacity;
    TextView date;
    ImageView icon;
    ImageView img;
    LinearLayout lv;
    LinearLayout lvText;
    View divider;
    ImageView imgStatus;

    public DriveViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.txtTitle);
        capacity = (TextView) itemView.findViewById(R.id.txtCapacity);
        date = (TextView) itemView.findViewById(R.id.txtDate);
        img = (ImageView) itemView.findViewById(R.id.img);
        icon = (ImageView) itemView.findViewById(R.id.imgIcon);
        lv = (LinearLayout) itemView.findViewById(R.id.lv);
        lvText = (LinearLayout) itemView.findViewById(R.id.lvText);
        divider = (View) itemView.findViewById(R.id.divider);
        imgStatus = (ImageView) itemView.findViewById(R.id.imgStatus);
    }

}