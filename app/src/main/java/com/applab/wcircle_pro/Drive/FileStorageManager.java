package com.applab.wcircle_pro.Drive;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.owncloud.android.lib.resources.files.RemoteFile;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by user on 28/9/2015.
 */
public class FileStorageManager {
    private Context mContext;
    private ContentResolver mContentResolver;
    private static final String TAG = "FileStorageManager";

    public FileStorageManager(Context context, ContentResolver cr) {
        mContext = context;
        mContentResolver = cr;
    }

    public static final String getSavePath(Context context) {
        File direct = new File(Environment.getExternalStorageDirectory() + "/"
                + context.getResources().getString(R.string.folder_name) + "/"
                + context.getResources().getString(R.string.my_drive));
        if (!direct.exists()) {
            direct.mkdirs();
        }
        return direct.getAbsolutePath();
    }

/*    public static final String getDefaultSavePathFor(OCFile file,Context context) {
        return getSavePath(context) + file.getRemotePath();
    }*/


    public boolean removeFolder(boolean removeDBData) {
        boolean success = true;
        if (removeDBData) {
            success = removeFolderInDb();
        }
        return success;
    }

    private boolean removeFolderInDb() {
        Uri uri = FileProvider.CONTENT_URI;
        int deleted = getContentResolver().delete(uri, null, null);
        return deleted > 0;
    }

    public boolean fileDBRemoteExists(String id, String parentId) {
        Cursor cursor = null;
        boolean retval = false;
        try {
            cursor = getContentResolver().query(FileProvider.CONTENT_URI, null, DBHelper.FILE_REMOTE_ID + "=? AND " + DBHelper.FILE_PARENT_ID + "=?", new String[]{id, parentId}, null);
            if (cursor != null && cursor.moveToFirst()) {
                retval = cursor.moveToFirst();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return retval;
    }

    //if local files size is more than remote files size then remove local files
    private void deleteRemainingFileDB(ArrayList<RemoteFile> files, String parentId) {
        ArrayList<String> arrRemoteId = new ArrayList<>();
        if (files.size() > 0) {
            for (int i = 0; i < files.size(); i++) {
                arrRemoteId.add(files.get(i).getRemoteId());
            }
        }
        Cursor cursor = null;
        try {
            cursor = getCursorForValue(DBHelper.FILE_PARENT_ID, parentId);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    if (!cursor.isNull(cursor.getColumnIndex(DBHelper.FILE_REMOTE_ID))) {
                        String remoteId = cursor.getString(cursor.getColumnIndex(DBHelper.FILE_REMOTE_ID));
                        if (!arrRemoteId.contains(remoteId)) {
                            getContentResolver().delete(FileProvider.CONTENT_URI, DBHelper.FILE_REMOTE_ID + "=?", new String[]{remoteId});
                        }
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

    }

    public void insertFile(ArrayList<RemoteFile> files, String parentId, String remotePath, String remoteFilePath, String storagePath) {
        DBHelper helper = new DBHelper(mContext);
        deleteRemainingFileDB(files, parentId);
        ArrayList<ContentValues> contentValueses = new ArrayList<>();
        try {
            if (files.size() > 0) {
                for (int i = 0; i < files.size(); i++) {
                    if (!fileDBRemoteExists(files.get(i).getRemoteId(), parentId)) {
                        ContentValues contentValues = new ContentValues();
                        if (!files.get(i).getRemotePath().equals(remotePath)) {
                            if (!files.get(i).getRemotePath().equals("/")) {
                                contentValues.put(DBHelper.FILE_REMOTE_ID, files.get(i).getRemoteId());
                                contentValues.put(DBHelper.FILE_REMOTE_PATH, files.get(i).getRemotePath());
                                String[] result = files.get(i).getRemotePath().split("/");
                                contentValues.put(DBHelper.FILE_NAME, result[result.length - 1]);
                                contentValues.put(DBHelper.FILE_CREATION, files.get(i).getCreationTimestamp());
                                contentValues.put(DBHelper.FILE_PARENT_ID, parentId);
                                contentValues.put(DBHelper.FILE_ETAG, files.get(i).getEtag());
                                contentValues.put(DBHelper.FILE_CONTENT_LENGTH, files.get(i).getLength());
                                contentValues.put(DBHelper.FILE_CONTENT_TYPE, files.get(i).getMimeType());
                                contentValues.put(DBHelper.FILE_MODIFIED, files.get(i).getModifiedTimestamp());
                                contentValues.put(DBHelper.FILE_PERMISSIONS, files.get(i).getPermissions());
                                contentValues.put(DBHelper.FILE_SIZE, files.get(i).getSize());
                                if (remoteFilePath != null) {
                                    if (files.get(i).getRemotePath().equals(remoteFilePath)) {
                                        contentValues.put(DBHelper.FILE_STORAGE_PATH, storagePath);
                                    }
                                }
                                contentValueses.add(contentValues);
                                Log.i(TAG, "Path1 : " + files.get(i).getRemotePath());
                            }
                        }
                    } else {
                        if (!files.get(i).getRemotePath().equals("/")) {
                            ContentValues contentValuesUpdate = new ContentValues();
                            contentValuesUpdate.put(DBHelper.FILE_CREATION, files.get(i).getCreationTimestamp());
                            contentValuesUpdate.put(DBHelper.FILE_PARENT_ID, parentId);
                            String[] result = files.get(i).getRemotePath().split("/");
                            contentValuesUpdate.put(DBHelper.FILE_NAME, result[result.length - 1]);
                            contentValuesUpdate.put(DBHelper.FILE_ETAG, files.get(i).getEtag());
                            contentValuesUpdate.put(DBHelper.FILE_CONTENT_LENGTH, files.get(i).getLength());
                            contentValuesUpdate.put(DBHelper.FILE_REMOTE_PATH, files.get(i).getRemotePath());
                            contentValuesUpdate.put(DBHelper.FILE_CONTENT_TYPE, files.get(i).getMimeType());
                            contentValuesUpdate.put(DBHelper.FILE_PERMISSIONS, files.get(i).getPermissions());
                            contentValuesUpdate.put(DBHelper.FILE_MODIFIED, files.get(i).getModifiedTimestamp());
                            contentValuesUpdate.put(DBHelper.FILE_SIZE, files.get(i).getSize());
                            getContentResolver().update(FileProvider.CONTENT_URI, contentValuesUpdate,
                                    DBHelper.FILE_REMOTE_ID + "=?", new String[]{files.get(i).getRemoteId()});
                            Log.i(TAG, "Path2 : " + files.get(i).getRemotePath());
                        }
                    }
                }
                if (contentValueses.size() > 0) {
                    ContentValues[] arr = new ContentValues[contentValueses.size()];
                    arr = contentValueses.toArray(arr);
                    mContext.getContentResolver().bulkInsert(FileProvider.CONTENT_URI, arr);
                }
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Insert File Error :" + ex.toString());
        } finally {
            Intent intent = new Intent(DriveActivity.ACTION);
            intent.putExtra("error", false);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        }
    }

    public ContentResolver getContentResolver() {
        return mContentResolver;
    }

    public boolean fileExists(long id) {
        Cursor cursor = null;
        boolean retval = false;
        try {
            cursor = getCursorForValue(DBHelper.FILE_ID, String.valueOf(id));
            if (cursor != null && cursor.moveToFirst()) {
                retval = cursor.moveToFirst();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return retval;
    }

    public Cursor getCursorForValue(String key, String value) {
        Cursor c = null;
        String selection = key + "=?";
        String[] selectionArgs = new String[]{value};
        c = getContentResolver().query(FileProvider.CONTENT_URI, null, selection, selectionArgs, null);
        return c;
    }

    public void renameAll(OCFile file, File renamedFile) {
        Cursor cursor = null;
        try {
            String selection = DBHelper.FILE_STORAGE_PATH + " LIKE ?";
            String[] selectionArgs = {file.getStoragePath() + "%"};
            cursor = getContentResolver().query(FileProvider.CONTENT_URI, null, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    OCFile current = getOCFile(cursor);
                    String[] result = current.getStoragePath().split("/");
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.FILE_STORAGE_PATH, renamedFile.getAbsolutePath() + "/" + result[result.length - 1]);
                    getContentResolver().update(FileProvider.CONTENT_URI, contentValues, DBHelper.FILE_REMOTE_ID + "=?", new String[]{current.getRemoteId()});
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public OCFile getOCFile(Cursor cursor) {
        final OCFile file = new OCFile();
        file.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.FILE_ID)));
        file.setName(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_NAME)));
        file.setLength(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_CONTENT_LENGTH)));
        file.setRemoteId(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_REMOTE_ID)));
        file.setRemotePath(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_REMOTE_PATH)));
        file.setCreationTimestamp(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_CREATION)));
        file.setEtag(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_ETAG)));
        file.setMimeType(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_CONTENT_TYPE)));
        file.setParentId(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_PARENT_ID)));
        file.setModifiedTimestamp(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_MODIFIED)));
        file.setPermissions(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_PERMISSIONS)));
        file.setSize(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_SIZE)));
        if (!cursor.isNull(cursor.getColumnIndex(DBHelper.FILE_STORAGE_PATH))) {
            file.setStoragePath(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_STORAGE_PATH)));
        }
        return file;
    }
}
