package com.applab.wcircle_pro.Drive;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by user on 5/8/2015.
 */
public class DriveAdapter extends RecyclerView.Adapter<DriveViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private Cursor cursor;
    private String TAG = "OWNCLOUD";
    private FragmentManager fragmentManager;


    public DriveAdapter(Context context, Cursor cursor, FragmentManager fragmentManager) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.cursor = cursor;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public DriveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("DriveViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_drive_row, parent, false);
        DriveViewHolder holder = new DriveViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(DriveViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        OCFile current = getData(cursor, position);
        holder.title.setText(current.getName());
        if (!current.getMimeType().equals("DIR")) {
            //holder.date.setText(Utils.getRelativeDateTimeString(context, current.getModifiedTimestamp(), DateUtils.SECOND_IN_MILLIS, DateUtils.WEEK_IN_MILLIS, 0, false, context));
            holder.date.setText(Utils.setCalendarDate("dd MMMM yyyy", current.getModifiedTimestamp()));
            Log.i(TAG, String.valueOf(current.getModifiedTimestamp()));
            holder.img.setImageResource(R.mipmap.action_information);
            String[] result = current.getName().split("\\.");
            String type = result[result.length - 1] == null ? "" : result[result.length - 1];
            holder.icon.setImageResource(Utils.getFileType(type));
            if (Utils.localFileExists(current.getStoragePath())) {
                holder.imgStatus.setVisibility(View.VISIBLE);
            } else {
                holder.imgStatus.setVisibility(View.GONE);
            }
            holder.capacity.setVisibility(View.VISIBLE);
            holder.divider.setVisibility(View.VISIBLE);
            holder.date.setVisibility(View.VISIBLE);
            holder.capacity.setText(Utils.bytesIntoHumanReadable(current.getLength()));
            holder.img.setVisibility(View.INVISIBLE);
        } else {
            holder.imgStatus.setVisibility(View.GONE);
            holder.capacity.setVisibility(View.GONE);
            holder.date.setVisibility(View.GONE);
            holder.img.setVisibility(View.INVISIBLE);
            holder.divider.setVisibility(View.GONE);
            holder.icon.setImageResource(R.mipmap.info_folder);
        }
        setImgOnClickListener(cursor, position, holder.img);
    }

    private void setImgOnClickListener(final Cursor cursor, final int position, ImageView img) {
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OCFile file = getData(cursor, position);
                if (!file.getMimeType().equals("DIR")) {
                    DriveDetailsDialogFragment driveDetailsDialogFragment = DriveDetailsDialogFragment.newInstance(file, context);
                    driveDetailsDialogFragment.show(fragmentManager, "");
                }
            }
        });
    }

    public OCFile getData(Cursor cursor, int position) {
        OCFile current = new OCFile();
        cursor.moveToPosition(position);
        current.setLength(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_CONTENT_LENGTH)));
        current.setName(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_NAME)));
        current.setRemoteId(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_REMOTE_ID)));
        current.setRemotePath(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_REMOTE_PATH)));
        current.setCreationTimestamp(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_CREATION)));
        current.setEtag(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_ETAG)));
        current.setMimeType(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_CONTENT_TYPE)));
        current.setParentId(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_PARENT_ID)));
        current.setModifiedTimestamp(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_MODIFIED)));
        current.setPermissions(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_PERMISSIONS)));
        current.setSize(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_SIZE)));
        current.setStoragePath(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_STORAGE_PATH)));
        return current;
    }

    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            DriveAdapter.this.notifyDataSetChanged();
        }
    };
}
