package com.applab.wcircle_pro.Drive;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Settings.BackupActivity;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener;
import com.owncloud.android.lib.common.operations.OperationCancelledException;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.common.utils.Log_OC;
import com.owncloud.android.lib.resources.files.DownloadRemoteFileOperation;
import com.owncloud.android.lib.resources.files.FileUtils;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by user on 2/10/2015.
 */

public class DownloadFileOperation extends RemoteOperation {
    private Set<OnDatatransferProgressListener> mDataTransferListeners = new HashSet<OnDatatransferProgressListener>();
    private long mModificationTimestamp = 0;
    private final AtomicBoolean mCancellationRequested = new AtomicBoolean(false);
    private DownloadRemoteFileOperation mDownloadOperation;
    private Context mContext;
    private String mRemotePath;
    private String mParentRemotePath = "";
    private String TAG = "DOWNLOAD";
    private OCFile mRemoteFile;
    private Message msg = new Message();

    public DownloadFileOperation(Context context, String remotePath, String parentRemotePath, OCFile file) {
        mContext = context;
        mRemotePath = remotePath;
        mRemoteFile = file;
        if (!parentRemotePath.equals(FileUtils.PATH_SEPARATOR)) {
            mParentRemotePath = parentRemotePath;
        }
    }

    public String getTmpFolder() {
        File direct = new File(Environment.getExternalStorageDirectory() + "/"
                + mContext.getResources().getString(R.string.folder_name) + "/"
                + mContext.getResources().getString(R.string.my_drive) + "/"
                + mContext.getResources().getString(R.string.temp));
        if (!direct.exists()) {
            direct.mkdirs();
        }
        return direct.getAbsolutePath();
    }

    public String getTmpPath() {
        return getTmpFolder() + mRemotePath;
    }

    public String getSavedPath() {
        File file = new File(FileStorageManager.getSavePath(mContext));
        if (!file.exists()) {
            file.mkdirs();
        }
        Log.i(TAG, FileStorageManager.getSavePath(mContext));
        return FileStorageManager.getSavePath(mContext) + mRemotePath;
    }

    @Override
    protected RemoteOperationResult run(OwnCloudClient client) {
        RemoteOperationResult result = null;
        File newFile = null;
        boolean moved = true;

        /// download will be performed to a temporal file, then moved to the final location
        File tmpFile = new File(getTmpPath());

        String tmpFolder = getTmpFolder();

        /// perform the download
        synchronized (mCancellationRequested) {
            if (mCancellationRequested.get()) {
                return new RemoteOperationResult(new OperationCancelledException());
            }
        }

        mDownloadOperation = new DownloadRemoteFileOperation(mRemotePath, tmpFolder);
        Iterator<OnDatatransferProgressListener> listener = mDataTransferListeners.iterator();
        while (listener.hasNext()) {
            mDownloadOperation.addDatatransferProgressListener(listener.next());
        }
        result = mDownloadOperation.execute(client);

        if (result.isSuccess()) {
            mModificationTimestamp = mDownloadOperation.getModificationTimestamp();
            newFile = new File(getSavedPath());
            if (!newFile.getParentFile().exists()) {
                newFile.getParentFile().mkdirs();
            }
            moved = tmpFile.renameTo(newFile);
            if (!moved) {
                result = new RemoteOperationResult(
                        RemoteOperationResult.ResultCode.LOCAL_STORAGE_NOT_MOVED);
            } else {
                try {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.FILE_STORAGE_PATH, newFile.getAbsolutePath());
                    mContext.getContentResolver().update(FileProvider.CONTENT_URI, contentValues,
                            DBHelper.FILE_REMOTE_ID + "=?", new String[]{mRemoteFile.getRemoteId()});
                } catch (Exception ex) {
                    ex.fillInStackTrace();
                }
            }

        } else {
            if (result.getCode() == RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION) {
                handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.TIMEOUT) {
                handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
                Intent intent = new Intent(DriveActivity.ACTION);
                intent.putExtra("error", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
                Intent intent = new Intent(DriveActivity.ACTION);
                intent.putExtra("error", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            }
            if (result.isException()) {
                Log_OC.e(TAG, "Checked " + result.getLogMessage(), result.getException());
            } else {
                Log_OC.e(TAG, "Checked " + result.getLogMessage());
            }
        }
        Log_OC.i(TAG, "Download of " + mRemotePath + " to " + getSavedPath() + ": " +
                result.getLogMessage());
        return result;
    }

    public void cancel() {
        mCancellationRequested.set(true);   // atomic set; there is no need of synchronizing it
        if (mDownloadOperation != null) {
            mDownloadOperation.cancel();
        }
    }


    public void addDatatransferProgressListener(OnDatatransferProgressListener listener) {
        synchronized (mDataTransferListeners) {
            mDataTransferListeners.add(listener);
        }
    }

    public void removeDatatransferProgressListener(OnDatatransferProgressListener listener) {
        synchronized (mDataTransferListeners) {
            mDataTransferListeners.remove(listener);
        }
    }

    public void handlerOperation(final String code, final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.showError(mContext, code, message);
                Intent intent = new Intent(BackupActivity.ACTION);
                intent.putExtra("isFail", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            }
        }, 1000);
    }
}
