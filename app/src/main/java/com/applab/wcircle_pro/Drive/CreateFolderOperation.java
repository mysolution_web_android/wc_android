package com.applab.wcircle_pro.Drive;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.applab.wcircle_pro.Settings.BackupActivity;
import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.common.utils.Log_OC;
import com.owncloud.android.lib.resources.files.CreateRemoteFolderOperation;

/**
 * Access to remote operation performing the creation of a new folder in the ownCloud server.
 * Save the new folder in Database
 */

public class CreateFolderOperation extends RemoteOperation {
    private static final String TAG = CreateFolderOperation.class.getSimpleName();
    private String mRemotePath;
    private String mNewFolderPath;
    private boolean mCreateFullPath;
    private Context mContext;
    private boolean mIgnoreETag;
    private boolean mIsBackup;
    private String mParentId;
    private Message msg = new Message();

    /**
     * Constructor
     *
     * @param createFullPath 'True' means that all the ancestor folders should be created
     *                       if don't exist yet.
     */

    public CreateFolderOperation(Context context,
                                 boolean ignoreETag,
                                 String parentId,
                                 String remotePath,
                                 String newFolderPath,
                                 boolean createFullPath, boolean isBackup) {
        mContext = context;
        mIgnoreETag = ignoreETag;
        mParentId = parentId;
        mRemotePath = remotePath;
        mNewFolderPath = newFolderPath;
        mCreateFullPath = createFullPath;
        mIsBackup = isBackup;
        Log.i(TAG, "Parent ID: " + mParentId + " Remote Path: " + remotePath);
    }

    @Override
    protected RemoteOperationResult run(final OwnCloudClient client) {
        CreateRemoteFolderOperation operation = new CreateRemoteFolderOperation(mRemotePath + mNewFolderPath,
                mCreateFullPath);
        RemoteOperationResult result = operation.execute(client);
        if (result.isSuccess()) {
            Log.i(TAG, "Success");
            if (!mIsBackup) {
                RefreshOperation refreshOperation = new RefreshOperation(mContext, mIgnoreETag,
                        mParentId, mRemotePath, null, null, false);
                refreshOperation.execute(client);
            }
            Intent intent = new Intent(BackupActivity.ACTION);
            intent.putExtra("isCreated", true);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        } else {
            if (result.getCode() == RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION) {
                handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.TIMEOUT) {
                handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
                Intent intent = new Intent(DriveActivity.ACTION);
                intent.putExtra("error", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
                Intent intent = new Intent(DriveActivity.ACTION);
                intent.putExtra("error", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            }
            if (result.isException()) {
                Log_OC.e(TAG, "Checked 1" + result.getLogMessage(), result.getException());
            } else {
                if (result.getLogMessage().equals("Operation finished with HTTP status code 405 (fail)")) {
                    if (!mIsBackup) {
                        handlerOperation(Utils.CODE_CREATE_FOLDER_EXISTS, Utils.CREATE_FOLDER_EXISTS);
                    } else {
                        Intent intent = new Intent(BackupActivity.ACTION);
                        intent.putExtra("isCreated", true);
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                    }
                }
                Log_OC.e(TAG, "Checked 2" + result.getLogMessage());
            }
        }
        return result;
    }

    public void handlerOperation(final String code, final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.showError(mContext, code, message);
                Intent intent = new Intent(BackupActivity.ACTION);
                intent.putExtra("isFail", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            }
        }, 1000);
    }
}
