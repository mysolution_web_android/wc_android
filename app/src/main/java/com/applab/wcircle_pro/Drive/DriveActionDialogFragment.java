package com.applab.wcircle_pro.Drive;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;

import java.util.ArrayList;

/**
 * Created by user on 27/10/2015.
 */
public class DriveActionDialogFragment extends DialogFragment {
    private OCFile mFile;
    private TextView mTxtName, mTxtTitle;
    private LinearLayout mBtnDownload, mBtnShare, mBtnRename, mBtnMove, mBtnCopy, mBtnRemove;
    private View mDownloadDivider;
    private ImageView mBtnCancel;
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private static FragmentManager mFragmentManager;
    private ArrayList<String> mParentLevel = new ArrayList<>();
    private ArrayList<String> mRemotePathLevel = new ArrayList<>();
    private int mLevel = 0;
    private int mId = 0;
    public static OwnCloudClient mClient;
    private static RemoveOperation mRemoveOperation;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DriveActionDialogFragment newInstance(OCFile file, Context context, FragmentManager fragmentManager, ArrayList<String> parentLevel,
                                                        ArrayList<String> remotePathLevel,
                                                        int level, RemoveOperation removeOperation, OwnCloudClient client, int id) {
        DriveActionDialogFragment frag = new DriveActionDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("file", file);
        args.putStringArrayList("parentLevel", parentLevel);
        args.putStringArrayList("remotePathLevel", remotePathLevel);
        args.putInt("level", level);
        args.putInt("id", id);
        mContext = context;
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        mFragmentManager = fragmentManager;
        mRemoveOperation = removeOperation;
        mClient = client;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mFile = getArguments().getParcelable("file");
        mParentLevel = getArguments().getStringArrayList("parentLevel");
        mRemotePathLevel = getArguments().getStringArrayList("remotePathLevel");
        mLevel = getArguments().getInt("level");
        mId = getArguments().getInt("id");

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_action, null);
        mTxtName = (TextView) v.findViewById(R.id.txtName);
        String[] result = mFile.getRemotePath().split("/");
        mTxtName.setText(result[result.length - 1]);
        mBtnDownload = (LinearLayout) v.findViewById(R.id.btnDownload);
        mDownloadDivider = (View) v.findViewById(R.id.downloadDivider);

        if (!mFile.getMimeType().equals("DIR")) {
            mBtnDownload.setVisibility(View.VISIBLE);
            mDownloadDivider.setVisibility(View.VISIBLE);
        } else {
            mBtnDownload.setVisibility(View.GONE);
            mDownloadDivider.setVisibility(View.GONE);
        }

        if (Utils.localFileExists(mFile.getStoragePath())) {
            mBtnDownload.setVisibility(View.GONE);
            mDownloadDivider.setVisibility(View.GONE);
        } else {
            mBtnDownload.setVisibility(View.VISIBLE);
            mDownloadDivider.setVisibility(View.VISIBLE);
        }
        mBtnDownload.setOnClickListener(mBtnDownloadOnClickListener);
        mBtnShare = (LinearLayout) v.findViewById(R.id.btnShare);
        mBtnRename = (LinearLayout) v.findViewById(R.id.btnRename);
        mBtnMove = (LinearLayout) v.findViewById(R.id.btnMove);
        mBtnCopy = (LinearLayout) v.findViewById(R.id.btnCopy);
        mBtnRemove = (LinearLayout) v.findViewById(R.id.btnRemove);

        mBtnRename.setOnClickListener(mBtnRenamOnClickListener);

        mBtnRemove.setOnClickListener(mBtnRemoveOnClickListener);

        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener mBtnDownloadOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DriveActionDialogFragment.this.getDialog().cancel();
            if (!Utils.isConnectingToInternet(mContext)) {
                Utils.showNoConnection(mContext);
            } else {
                Intent intent = new Intent(mContext, DownloadIntentService.class);
                intent.putExtra("remoteId", mFile.getRemoteId());
                intent.putExtra("parentId", mParentLevel.get(mLevel));
                intent.putExtra("parentRemotePath", mRemotePathLevel.get(mLevel));
                intent.putExtra("OCFile", (Parcelable) mFile);
                intent.putExtra("id", mId);
                mContext.startService(intent);
            }
        }
    };

    private View.OnClickListener mBtnRemoveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DriveActionDialogFragment.this.getDialog().cancel();
            DriveRemoveDialogFragment driveRemoveDialogFragment = DriveRemoveDialogFragment.newInstance(mFile,
                    mContext, mRemoveOperation, mParentLevel, mRemotePathLevel, mLevel, mClient);
            driveRemoveDialogFragment.show(mFragmentManager, "");
        }
    };

    private View.OnClickListener mBtnRenamOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DriveActionDialogFragment.this.getDialog().cancel();
            RenameDialogFragment renameDialogFragment = RenameDialogFragment.newInstance(mFile, mLevel, mParentLevel,
                    mRemotePathLevel);
            renameDialogFragment.show(mFragmentManager, "");
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DriveActionDialogFragment.this.getDialog().cancel();
        }
    };
}

