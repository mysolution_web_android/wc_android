package com.applab.wcircle_pro.Drive;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;

import java.util.ArrayList;

/**
 * Created by user on 27/10/2015.
 */
public class DriveRemoveDialogFragment extends DialogFragment {
    private OCFile mFile;
    private LinearLayout mBtnRemoveAll, mBtnRemoveLocal, mBtnRemoveRemote, mBtnExit;
    private ImageView mBtnCancel;
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private static RemoveOperation mRemoveOperation;
    private ArrayList<String> mParentLevel = new ArrayList<>();
    private ArrayList<String> mRemotePathLevel = new ArrayList<>();
    private int mLevel = 0;
    public static OwnCloudClient mClient;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DriveRemoveDialogFragment newInstance(OCFile file, Context context, RemoveOperation removeOperation,
                                                        ArrayList<String> parentLevel, ArrayList<String> remotePathLevel,
                                                        int level, OwnCloudClient client) {
        DriveRemoveDialogFragment frag = new DriveRemoveDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("file", file);
        args.putStringArrayList("parentLevel", parentLevel);
        args.putStringArrayList("remotePathLevel", remotePathLevel);
        args.putInt("level", level);
        mContext = context;
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        mRemoveOperation = removeOperation;
        mClient = client;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mFile = getArguments().getParcelable("file");
        mParentLevel = getArguments().getStringArrayList("parentLevel");
        mRemotePathLevel = getArguments().getStringArrayList("remotePathLevel");
        mLevel = getArguments().getInt("level");

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_remove, null);
        mBtnRemoveAll = (LinearLayout) v.findViewById(R.id.btnRemoveAll);
        mBtnRemoveLocal = (LinearLayout) v.findViewById(R.id.btnRemoveLocal);
        mBtnRemoveRemote = (LinearLayout) v.findViewById(R.id.btnRemoveRemote);
        mBtnExit = (LinearLayout) v.findViewById(R.id.btnExit);
        if (!Utils.localFileExists(mFile.getStoragePath())) {
            mBtnRemoveAll.setVisibility(View.GONE);
            mBtnRemoveLocal.setVisibility(View.GONE);
        } else {
            mBtnRemoveRemote.setVisibility(View.GONE);
        }

        mBtnRemoveAll.setOnClickListener(mBtnRemoveAllOnClickListener);

        mBtnRemoveLocal.setOnClickListener(mBtnRemoveLocalOnClickListener);

        mBtnRemoveRemote.setOnClickListener(mBtnRemoveRemoteOnClickListener);

        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);
        mBtnExit.setOnClickListener(btnCancelOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener mBtnRemoveRemoteOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DriveRemoveDialogFragment.this.getDialog().cancel();
            if (!Utils.isConnectingToInternet(mContext)) {
                Utils.showNoConnection(mContext);
            } else {
                mRemoveOperation = new RemoveOperation(mContext, mFile.getRemoteId(), true, mParentLevel.get(mLevel), mRemotePathLevel.get(mLevel),
                        mFile.getRemotePath(), mFile, 3);
                new Thread() {
                    @Override
                    public void run() {
                        Looper.prepare();
                        mRemoveOperation.execute(mClient);
                        Looper.loop();
                    }
                }.start();
            }
        }
    };

    private View.OnClickListener mBtnRemoveLocalOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DriveRemoveDialogFragment.this.getDialog().cancel();
            if (!Utils.isConnectingToInternet(mContext)) {
                Utils.showNoConnection(mContext);
            } else {
                mRemoveOperation = new RemoveOperation(mContext, mFile.getRemoteId(), true, mParentLevel.get(mLevel), mRemotePathLevel.get(mLevel),
                        mFile.getRemotePath(), mFile, 2);
                new Thread() {
                    @Override
                    public void run() {
                        Looper.prepare();
                        mRemoveOperation.execute(mClient);
                        Looper.loop();
                    }
                }.start();
            }
        }
    };

    private View.OnClickListener mBtnRemoveAllOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DriveRemoveDialogFragment.this.getDialog().cancel();
            if (!Utils.isConnectingToInternet(mContext)) {
                Utils.showNoConnection(mContext);
            } else {
                mRemoveOperation = new RemoveOperation(mContext, mFile.getRemoteId(), true, mParentLevel.get(mLevel), mRemotePathLevel.get(mLevel),
                        mFile.getRemotePath(), mFile, 1);
                new Thread() {
                    @Override
                    public void run() {
                        Looper.prepare();
                        mRemoveOperation.execute(mClient);
                        Looper.loop();
                    }
                }.start();
            }
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DriveRemoveDialogFragment.this.getDialog().cancel();
        }
    };
}

