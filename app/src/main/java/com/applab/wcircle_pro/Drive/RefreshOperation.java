package com.applab.wcircle_pro.Drive;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.common.utils.Log_OC;
import com.owncloud.android.lib.resources.files.FileUtils;
import com.owncloud.android.lib.resources.files.ReadRemoteFileOperation;
import com.owncloud.android.lib.resources.files.ReadRemoteFolderOperation;
import com.owncloud.android.lib.resources.files.RemoteFile;

import java.util.ArrayList;

/**
 * Created by user on 25/9/2015.
 */

public class RefreshOperation extends RemoteOperation {
    private Context mContext;
    private boolean mIsShareSupported;
    private String TAG = RefreshOperation.class.getSimpleName();
    private boolean mIgnoreETag = false; //ignore version of file
    private boolean mRemoteFolderChanged = false;
    private FileStorageManager mStorageManager;
    private String parentId = "0";
    private String remotePath = FileUtils.PATH_SEPARATOR;
    private String mFileRemotePath;
    private String mStoragePath;
    private boolean mIsRefresh = false;

    public RefreshOperation(Context context,
                            boolean ignoreETag,
                            String parentId,
                            String remotePath,
                            String fileRemotePath,
                            String storagePath,
                            boolean isRefresh) {
        mContext = context;
        mStorageManager = new FileStorageManager(mContext, mContext.getContentResolver());
        mIgnoreETag = ignoreETag;
        mFileRemotePath = fileRemotePath;
        mStoragePath = storagePath;
        this.parentId = parentId;
        this.remotePath = remotePath;
        this.mIsRefresh = isRefresh;
    }

    private RemoteOperationResult updateOCVersion(OwnCloudClient client) {
        UpdateOCVersionOperation update = new UpdateOCVersionOperation(mContext);
        RemoteOperationResult result = update.execute(client);
        if (result.isSuccess()) {
            mIsShareSupported = update.getOCVersion().isSharedSupported();
        }
        return result;
    }

    @Override
    protected RemoteOperationResult run(OwnCloudClient client) {
        RemoteOperationResult result = null;
        result = updateOCVersion(client);
        if (result.isSuccess()) {
            result = checkForChanges(client);
            if (result.isSuccess()) {
                if (mRemoteFolderChanged) {
                    result = getReadiedData(client);
                } else {
                    // TODO Enable when "On Device" is recovered ?
//                mChildren = mStorageManager.getFolderContent(mLocalFolder/*, false*/);
                }
            } else {
                Intent mIntent = new Intent(DriveActivity.ACTION);
                if (result.getCode() == RemoteOperationResult.ResultCode.TIMEOUT) {
                    Intent intent = new Intent(DriveActivity.ACTION);
                    intent.putExtra("connection_not_available", true);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION) {
                    Intent intent = new Intent(DriveActivity.ACTION);
                    intent.putExtra("connection_not_available", true);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
                    mIntent.putExtra("error", true);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(mIntent);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
                    mIntent.putExtra("error", true);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(mIntent);
                }
            }
        } else {
            if (result.getCode() == RemoteOperationResult.ResultCode.TIMEOUT) {
                Intent intent = new Intent(DriveActivity.ACTION);
                intent.putExtra("connection_not_available", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION) {
                Intent intent = new Intent(DriveActivity.ACTION);
                intent.putExtra("connection_not_available", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
                Intent intent = new Intent(DriveActivity.ACTION);
                intent.putExtra("error", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            } else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
                Intent intent = new Intent(DriveActivity.ACTION);
                intent.putExtra("error", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            }
        }
        return result;
    }

    private RemoteOperationResult getReadiedData(OwnCloudClient client) {
        ReadRemoteFolderOperation operation = new ReadRemoteFolderOperation(remotePath);
        RemoteOperationResult result = operation.execute(client);
        if (result.isSuccess()) {
            ArrayList<RemoteFile> files = new ArrayList<>();
            for (Object obj : result.getData()) {
                files.add((RemoteFile) obj);
            }
            mStorageManager.insertFile(files, parentId, remotePath, mFileRemotePath, mStoragePath);
        } else {
            if (result.getCode() == RemoteOperationResult.ResultCode.FILE_NOT_FOUND)
                removeDB();
        }
        return result;
    }

    private void removeDB() {
        mStorageManager.removeFolder(true);
    }

    private RemoteOperationResult checkForChanges(OwnCloudClient client) {
        mRemoteFolderChanged = true;
        RemoteOperationResult result = null;
        // remote request
        ReadRemoteFileOperation operation = new ReadRemoteFileOperation(remotePath);
        result = operation.execute(client);
        if (result.isSuccess()) {
            if (!mIgnoreETag) {
                // check if remote and local folder are different
                String remoteFolderETag = ((RemoteFile) result.getData().get(0)).getEtag();
                if (remoteFolderETag != null) {
                    Cursor cursor = null;
                    try {
                        cursor = mStorageManager.getCursorForValue(DBHelper.FILE_REMOTE_ID, ((OCFile) result.getData().get(0)).getRemoteId());
                        if (cursor != null && cursor.moveToFirst()) {
                            mRemoteFolderChanged = !(remoteFolderETag.equalsIgnoreCase(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_ETAG))));
                            Log.i(TAG, String.valueOf(mRemoteFolderChanged));
                        }

                    } catch (Exception ex) {
                        ex.fillInStackTrace();
                    } finally {
                        if (cursor != null)
                            if (!cursor.isClosed()) {
                                cursor.close();
                            }
                    }
                } else {
                    Log_OC.e(TAG, "Checked, No ETag received from server");
                }
            }
            Log_OC.i(TAG, "Checked " + (mRemoteFolderChanged ? "changed" : "not changed"));
        } else {
            // check failed
            if (result.getCode() == RemoteOperationResult.ResultCode.FILE_NOT_FOUND) {
                removeDB();
            } else if (mIsRefresh) {
                if (result.getCode() == RemoteOperationResult.ResultCode.TIMEOUT) {
                    Intent intent = new Intent(DriveActivity.ACTION);
                    intent.putExtra("connection_not_available", true);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION) {
                    Intent intent = new Intent(DriveActivity.ACTION);
                    intent.putExtra("connection_not_available", true);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
                    Intent intent = new Intent(DriveActivity.ACTION);
                    intent.putExtra("error", true);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
                    Intent intent = new Intent(DriveActivity.ACTION);
                    intent.putExtra("error", true);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                }
            } else {
                if (result.getCode() != RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION) {
                    if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
                        Intent intent = new Intent(DriveActivity.ACTION);
                        intent.putExtra("error", true);
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                    } else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
                        Intent intent = new Intent(DriveActivity.ACTION);
                        intent.putExtra("error", true);
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                    }
                }
            }
            if (result.isException()) {
                Log_OC.e(TAG, "Checked " + result.getLogMessage(), result.getException());
            } else {
                Log_OC.e(TAG, "Checked " + result.getLogMessage());
            }
        }
        return result;
    }

    public void handlerOperation(final String code, final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.showError(mContext, code, message);
            }
        }, 1000);
    }

    /*else if (result.getCode() == RemoteOperationResult.ResultCode.TIMEOUT) {
        handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
    } else if (result.getCode() == RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION) {
        handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
    } else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
        handlerOperation(Utils.CODE_UNKNOWN_MY_DRIVE_ACCOUNT, Utils.UNKNOWN_MY_DRIVE_ACCOUNT);
    } else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
        handlerOperation(Utils.CODE_UNKNOWN_MY_DRIVE_ACCOUNT, Utils.UNKNOWN_MY_DRIVE_ACCOUNT);
    }*/
}
