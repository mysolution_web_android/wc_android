package com.applab.wcircle_pro.Drive;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by user on 7/10/2015.
 */
public class FileListAdapter extends RecyclerView.Adapter<FileListViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<File> mCurrent;
    private String TAG = "OWNCLOUD";

    public FileListAdapter(Context context, ArrayList<File> current) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.mCurrent = current;
    }

    @Override
    public FileListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("FileListViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_file_list_row, parent, false);
        FileListViewHolder holder = new FileListViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(FileListViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        File current = mCurrent.get(position);
        holder.title.setText(current.getName());
        holder.title.setTag(current.getAbsoluteFile());
        if (current.isFile()) {
            holder.date.setText(Utils.setCalendarDate("d MMMM yyyy", current.lastModified()));
            holder.icon.setImageResource(R.mipmap.file_doc);
            holder.capacity.setText(Utils.bytesIntoHumanReadable(current.length()));
            holder.capacity.setVisibility(View.VISIBLE);
            holder.date.setVisibility(View.VISIBLE);
            holder.divider.setVisibility(View.VISIBLE);
        } else {
            holder.capacity.setVisibility(View.GONE);
            holder.date.setVisibility(View.GONE);
            holder.divider.setVisibility(View.GONE);
            holder.icon.setImageResource(R.mipmap.info_folder);
        }
    }

    @Override
    public int getItemCount() {
        return mCurrent == null ? 0 : mCurrent.size();
    }

    public ArrayList<File> swapFile(ArrayList<File> current) {
        if (this.mCurrent == current) {
            return null;
        }
        ArrayList<File> oldFile = this.mCurrent;
        this.mCurrent = current;
        if (current != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldFile;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            FileListAdapter.this.notifyDataSetChanged();
        }
    };
}
