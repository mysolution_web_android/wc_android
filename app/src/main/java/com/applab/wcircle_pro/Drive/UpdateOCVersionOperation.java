package com.applab.wcircle_pro.Drive;

import android.content.Context;

import com.applab.wcircle_pro.R;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.accounts.AccountUtils;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.common.utils.Log_OC;
import com.owncloud.android.lib.resources.status.OwnCloudVersion;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user on 25/9/2015.
 */
public class UpdateOCVersionOperation extends RemoteOperation {
    private static final String TAG = UpdateOCVersionOperation.class.getSimpleName();
    private OwnCloudVersion mOwnCloudVersion;
    private Context mContext;
    public UpdateOCVersionOperation(Context context) {
        mContext = context;
        mOwnCloudVersion = null;
    }

    @Override
    protected RemoteOperationResult run(OwnCloudClient client) {
        String statUrl = mContext.getString(R.string.status_url);
        statUrl += AccountUtils.STATUS_PATH;
        RemoteOperationResult result = null;
        GetMethod get = null;
        try {
            get = new GetMethod(statUrl);
            int status = client.executeMethod(get);
            if (status != HttpStatus.SC_OK) {
                client.exhaustResponse(get.getResponseBodyAsStream());
                result = new RemoteOperationResult(false, status, get.getResponseHeaders());

            } else {
                String response = get.getResponseBodyAsString();
                if (response != null) {
                    JSONObject json = new JSONObject(response);
                    if (json != null && json.getString("version") != null) {

                        String version = json.getString("version");
                        mOwnCloudVersion = new OwnCloudVersion(version);
                        if (mOwnCloudVersion.isVersionValid()) {
                            result = new RemoteOperationResult(RemoteOperationResult.ResultCode.OK);
                        } else {
                            Log_OC.w(TAG, "Invalid version number received from server: " + json.getString("version"));
                            result = new RemoteOperationResult(RemoteOperationResult.ResultCode.BAD_OC_VERSION);
                        }
                    }
                }
                if (result == null) {
                    result = new RemoteOperationResult(RemoteOperationResult.ResultCode.INSTANCE_NOT_CONFIGURED);
                }
            }
            Log_OC.i(TAG, "Check for update of ownCloud server version at " + client.getWebdavUri() + ": " + result.getLogMessage());

        } catch (JSONException e) {
            result = new RemoteOperationResult(RemoteOperationResult.ResultCode.INSTANCE_NOT_CONFIGURED);
            Log_OC.e(TAG, "Check for update of ownCloud server version at " + client.getWebdavUri() + ": " + result.getLogMessage(), e);

        } catch (Exception e) {
            result = new RemoteOperationResult(e);
            Log_OC.e(TAG, "Check for update of ownCloud server version at " + client.getWebdavUri() + ": " + result.getLogMessage(), e);

        } finally {
            if (get != null)
                get.releaseConnection();
        }
        return result;
    }

    public OwnCloudVersion getOCVersion() {
        return mOwnCloudVersion;
    }
}
