package com.applab.wcircle_pro.Drive;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.applab.wcircle_pro.Settings.BackupActivity;
import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.common.utils.Log_OC;
import com.owncloud.android.lib.resources.files.UploadRemoteFileOperation;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by user on 2/10/2015.
 */
public class UploadFileOperation extends RemoteOperation {
    private Context mContext;
    private Set<OnDatatransferProgressListener> mDataTransferListeners = new HashSet<OnDatatransferProgressListener>();
    private long mModificationTimestamp = 0;
    private AtomicBoolean mCancellationRequested = new AtomicBoolean(false);
    private UploadRemoteFileOperation mUploadOperation;
    private OCFile mOCFile;
    private String mParentId;
    private String mStoragePath;
    private boolean mIgnoreTag;
    private boolean mIsBackup = false;
    private String TAG = "UPLOAD";
    private Message msg = new Message();

    public UploadFileOperation(Context context, boolean ignoreETag, OCFile remoteFile, String parentId, String storagePath, boolean isBackup) {
        mContext = context;
        mOCFile = remoteFile;
        mStoragePath = storagePath;
        mParentId = parentId;
        mIgnoreTag = ignoreETag;
        mIsBackup = isBackup;
    }

    @Override
    protected RemoteOperationResult run(OwnCloudClient client) {
        RemoteOperationResult result = null;
        try {
            int status = quotaStatus(client, new File(mStoragePath));
            if (status == 0) {
                String remotePath = getAvailableRemotePath(client, mOCFile.getRemotePath());
                mUploadOperation = new UploadRemoteFileOperation(mStoragePath,
                        remotePath, mOCFile.getMimeType());
                Iterator<OnDatatransferProgressListener> listener = mDataTransferListeners.iterator();
                while (listener.hasNext()) {
                    mUploadOperation.addDatatransferProgressListener(listener.next());
                }
                result = mUploadOperation.execute(client);

                if (result.isSuccess()) {
                    Log.i(TAG, "Success");
                    File source = new File(mStoragePath);
                    File target = new File(FileStorageManager.getSavePath(mContext) + remotePath);
                    Utils.copyDirectory(source, target);
                    String parent = new File(mOCFile.getRemotePath()).getParent() + "/";
                    if (!mIsBackup) {
                        RefreshOperation refreshOperation = new RefreshOperation(mContext, mIgnoreTag,
                                mParentId, parent, remotePath, target.getAbsolutePath(), false);
                        refreshOperation.execute(client);
                    }
                } else {
                    if (result.getCode() == RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION) {
                        handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
                    } else if (result.getCode() == RemoteOperationResult.ResultCode.TIMEOUT) {
                        handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
                    } else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
                        Intent intent = new Intent(DriveActivity.ACTION);
                        intent.putExtra("error", true);
                        DriveActivity.mgr.sendBroadcast(intent);
                    } else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
                        Intent intent = new Intent(DriveActivity.ACTION);
                        intent.putExtra("error", true);
                        DriveActivity.mgr.sendBroadcast(intent);
                    }
                    if (result.isException()) {
                        Log_OC.e(TAG, "Checked " + result.getLogMessage(), result.getException());
                    } else {
                        Log_OC.e(TAG, "Checked " + result.getLogMessage());
                    }
                }
            } else if (status == 1) {
                handlerOperation(Utils.CODE_QUOTA_EXCEED);
            } else if (status == 2) {
                handlerOperation(Utils.CODE_UPLOAD_FAILED);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void handlerOperation(final String code, final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.showError(mContext, code, message);
            }
        }, 1000);
    }

    public void handlerOperation(final String code) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (code.equals(Utils.CODE_QUOTA_EXCEED)) {
                    Utils.showError(mContext, code, Utils.QUOTA_EXCEED);
                } else if (code.equals(Utils.CODE_UPLOAD_FAILED)) {
                    Utils.showError(mContext, code, Utils.CODE_UPLOAD_FAILED);
                }
                Intent intent = new Intent(BackupActivity.ACTION);
                intent.putExtra("isFail", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            }
        }, 1000);
    }

    /**
     * Checks if remotePath does not exist in the server and returns it, or adds
     * a suffix to it in order to avoid the server file is overwritten.
     *
     * @param wc
     * @param remotePath
     * @return
     */
    private String getAvailableRemotePath(OwnCloudClient wc, String remotePath) throws Exception {
        boolean check = DriveActivity.existsFile(wc, remotePath);
        if (!check) {
            return remotePath;
        }

        int pos = remotePath.lastIndexOf(".");
        String suffix = "";
        String extension = "";
        if (pos >= 0) {
            extension = remotePath.substring(pos + 1);
            remotePath = remotePath.substring(0, pos);
        }
        int count = 2;
        do {
            suffix = " (" + count + ")";
            if (pos >= 0) {
                check = DriveActivity.existsFile(wc, remotePath + suffix + "." + extension);
            } else {
                check = DriveActivity.existsFile(wc, remotePath + suffix);
            }
            count++;
        } while (check);

        if (pos >= 0) {
            return remotePath + suffix + "." + extension;
        } else {
            return remotePath + suffix;
        }
    }

    public void cancel() {
        mCancellationRequested = new AtomicBoolean(true);
        if (mUploadOperation != null) {
            mUploadOperation.cancel();
        }
    }

    public Set<OnDatatransferProgressListener> getDataTransferListeners() {
        return mDataTransferListeners;
    }

    public void addDatatransferProgressListener(OnDatatransferProgressListener listener) {
        synchronized (mDataTransferListeners) {
            mDataTransferListeners.add(listener);
        }
    }

    public void removeDatatransferProgressListener(OnDatatransferProgressListener listener) {
        synchronized (mDataTransferListeners) {
            mDataTransferListeners.remove(listener);
        }
    }

    private int quotaStatus(OwnCloudClient client, File file) {
        CheckQuotaOperation operation = new CheckQuotaOperation();
        RemoteOperationResult result = operation.execute(client);
        if (result != null) {
            if (operation.getErrorMessage() == null) {
                if (operation.getQuotaUsedBytes().longValue() + file.length() >= operation.getQuotaAvailableBytes().longValue()) {
                    return 1; // Quata Exceed
                }
            } else {
                return 2; //Error in result
            }
        } else {
            return 2;// Error in result
        }
        return 0;
    }
}
