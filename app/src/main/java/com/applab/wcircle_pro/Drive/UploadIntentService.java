package com.applab.wcircle_pro.Drive;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.OwnCloudClientFactory;
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory;
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener;

import java.io.File;

/**
 * Created by user on 2/10/2015.
 */

public class UploadIntentService extends IntentService implements OnDatatransferProgressListener {
    private OwnCloudClient mClient;
    private OCFile mOCFile;
    private String mParentId;
    private String mStoragePath;
    private int id;

    public UploadIntentService() {
        super("UploadIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle bundle = intent.getExtras();

        Uri serverUri = Uri.parse(getString(R.string.server_base_url));
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, getBaseContext(), true);
        mClient.setCredentials(OwnCloudCredentialsFactory.newBasicCredentials(
                Utils.getProfile(getBaseContext()).getEmail(), DriveActivity.pref.getString("own_password", null)));

        mOCFile = bundle.getParcelable("OCFile");
        mParentId = bundle.getString("ParentId");
        mStoragePath = bundle.getString("StoragePath");

        UploadFileOperation uploadOperation = new UploadFileOperation(getBaseContext(), true, mOCFile, mParentId, mStoragePath, false);
        uploadOperation.addDatatransferProgressListener(this);

        if (Utils.getUsableSpace() < new File(mStoragePath).length()) {
            Utils.showError(getBaseContext(), Utils.CODE_LOCAL_SIZE_EXCEEDED, Utils.LOCAL_SIZE_EXCEED);
        } else {
            uploadOperation.execute(mClient);
        }
    }

    @Override
    public void onTransferProgress(long progressRate, long totalTransferredSoFar, long totalToTransfer, String fileAbsoluteName) {
        id = 0;

        final NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        String[] result = mStoragePath.split("/");
        mBuilder.setContentTitle(result[result.length - 1])
                .setContentText("Upload in progress")
                .setSmallIcon(R.mipmap.icon);

        // Start a lengthy operation in a background thread
        mBuilder.setProgress((int) totalToTransfer, (int) totalTransferredSoFar, false);
        // Displays the progress bar for the first time.
        mNotifyManager.notify(id, mBuilder.build());
        // When the loop is finished, updates the notification
        if ((int) totalToTransfer == (int) totalTransferredSoFar) {
            mBuilder.setContentText("Upload complete")
                    // Removes the progress bar
                    .setProgress(0, 0, false);
            mNotifyManager.notify(id, mBuilder.build());
        }
    }
}
