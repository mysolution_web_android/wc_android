package com.applab.wcircle_pro.Drive;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.common.utils.Log_OC;
import com.owncloud.android.lib.resources.files.RemoveRemoteFileOperation;

import java.io.File;

/**
 * Remote operation performing the removal of a remote file or folder in the ownCloud server.
 */

public class RemoveOperation extends RemoteOperation {
    private String mRemotePath;
    private String mRemovePath;
    private Context mContext;
    private String mParentId;
    private boolean mIgnoreETag;
    private int mStatus;
    private String mRemoteId;
    private String TAG = "REMOVE";
    private Message msg = new Message();
    private OCFile mCurrent;

    /**
     * Constructor
     *
     * @param remotePath RemotePath of the OCFile instance describing the remote file or
     *                   folder to remove from the server
     */
    public RemoveOperation(Context context,
                           String remoteId,
                           boolean ignoreETag,
                           String parentId,
                           String remotePath,
                           String removePath,
                           OCFile current,
                           int status) {
        mContext = context;
        mParentId = parentId;
        mRemotePath = remotePath;
        mIgnoreETag = ignoreETag;
        mRemovePath = removePath;
        mRemoteId = remoteId;
        mStatus = status;
        mCurrent = current;
    }


    /**
     * Performs the remove operation
     *
     * @param client Client object to communicate with the remote ownCloud server.
     */
    @Override
    protected RemoteOperationResult run(final OwnCloudClient client) {
        if (mStatus == 1) {
            RemoveRemoteFileOperation operation = new RemoveRemoteFileOperation(mRemovePath);
            RemoteOperationResult result = operation.execute(client);
            if (result.isSuccess()) {
                removeLocalFile();
                Log.i(TAG, "Success");
                new Thread(new Runnable() {
                    public void run() {
                        RefreshOperation refreshOperation = new RefreshOperation(mContext, mIgnoreETag,
                                mParentId, mRemotePath, null, null, false);
                        refreshOperation.execute(client);
                    }
                }).start();
            } else {
                Log.i(TAG, "Failed");
                if (result.getCode() == RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION) {
                    handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.TIMEOUT) {
                    handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
                    Intent intent = new Intent(DriveActivity.ACTION);
                    intent.putExtra("error", true);
                    DriveActivity.mgr.sendBroadcast(intent);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
                    Intent intent = new Intent(DriveActivity.ACTION);
                    intent.putExtra("error", true);
                    DriveActivity.mgr.sendBroadcast(intent);
                }
                if (result.isException()) {
                    Log_OC.e(TAG, "Checked " + result.getLogMessage(), result.getException());
                } else {
                    Log_OC.e(TAG, "Checked " + result.getLogMessage());
                }
            }
            return result;
        } else if (mStatus == 2) {
            removeLocalFile();
        } else if (mStatus == 3) {
            RemoveRemoteFileOperation operation = new RemoveRemoteFileOperation(mRemovePath);
            RemoteOperationResult result = operation.execute(client);
            if (result.isSuccess()) {
                Log.i(TAG, "Success");
                new Thread(new Runnable() {
                    public void run() {
                        RefreshOperation refreshOperation = new RefreshOperation(mContext, mIgnoreETag,
                                mParentId, mRemotePath, null, null, false);
                        refreshOperation.execute(client);
                    }
                }).start();
            } else {
                Log.i(TAG, "Failed");
                if (result.getCode() == RemoteOperationResult.ResultCode.TIMEOUT) {
                    handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION) {
                    handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
                    Intent intent = new Intent(DriveActivity.ACTION);
                    intent.putExtra("error", true);
                    DriveActivity.mgr.sendBroadcast(intent);
                } else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
                    Intent intent = new Intent(DriveActivity.ACTION);
                    intent.putExtra("error", true);
                    DriveActivity.mgr.sendBroadcast(intent);
                }
                if (result.isException()) {
                    Log_OC.e(TAG, "Checked " + result.getLogMessage(), result.getException());
                } else {
                    Log_OC.e(TAG, "Checked " + result.getLogMessage());
                }
            }
            return result;
        }
        return null;
    }

    private void removeLocalFile() {
        String path = Environment.getExternalStorageDirectory() + "/" +
                mContext.getResources().getString(R.string.folder_name) +
                "/" + mContext.getResources().getString(R.string.my_drive) + mRemovePath;

        if (path.contains("//")) {
            path = path.replace("//", "/");
        }

        Log.i(TAG, path);

        boolean success = false;
        if (mCurrent.getMimeType().equals("DIR")) {
            success = Utils.deleteFolderContent(new File(path));
        } else {
            success = Utils.deleteFile(path);
        }
        if (!success) {
            handlerOperation(Utils.CODE_REMOVE_FAILED, Utils.REMOVED_FAILED);
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.putNull(DBHelper.FILE_STORAGE_PATH);
            mContext.getContentResolver().update(FileProvider.CONTENT_URI, contentValues, DBHelper.FILE_STORAGE_PATH + " LIKE ? ", new String[]{path + "%"});
            handlerOperation(Utils.CODE_REMOVE_SUCCEED, Utils.REMOVED_SUCCEED);
        }
    }

    public void handlerOperation(final String code, final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.showError(mContext, code, message);
            }
        }, 1000);
    }
}
