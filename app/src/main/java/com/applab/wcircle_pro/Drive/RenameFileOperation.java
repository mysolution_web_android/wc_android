package com.applab.wcircle_pro.Drive;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.common.utils.Log_OC;
import com.owncloud.android.lib.resources.files.RenameRemoteFileOperation;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by user on 2/10/2015.
 */
public class RenameFileOperation extends RemoteOperation {
    private String mNewName;
    private String mParentRemotePath;
    private OCFile mOCFile;
    private boolean mIsFolder = false;
    private Context mContext;
    private boolean mIgnoreETag;
    private String mParentId;
    private String TAG = "RENAME";
    private Message msg = new Message();
    private OCFile mRemoteFile;

    public RenameFileOperation(Context context, boolean ignoreIag, String parentId, OCFile remoteFile, String newName, String parentRemotePath) {
        mNewName = newName;
        mParentRemotePath = parentRemotePath;
        mOCFile = remoteFile;
        mContext = context;
        mIgnoreETag = ignoreIag;
        mParentId = parentId;
        mRemoteFile = remoteFile;
        if (remoteFile.getMimeType().equals("DIR")) {
            mIsFolder = true;
        }
    }

    @Override
    protected RemoteOperationResult run(final OwnCloudClient client) {
        RemoteOperationResult result = null;
        // check if the new name is valid in the local file system
        RenameRemoteFileOperation operation = new RenameRemoteFileOperation(mOCFile.getRemotePath().replace("/", ""),
                mOCFile.getRemotePath(), mNewName + Utils.suffixOf(mOCFile.getRemotePath()), mIsFolder);
        result = operation.execute(client);

        if (result.isSuccess()) {
        } else if (result.getCode() == RemoteOperationResult.ResultCode.TIMEOUT) {
            handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
        } else if (result.getCode() == RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION) {
            handlerOperation(Utils.CODE_CONNECTION, Utils.CONNECTION);
        }else if (result.getCode() == RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE) {
            Intent intent = new Intent(DriveActivity.ACTION);
            intent.putExtra("error", true);
            DriveActivity.mgr.sendBroadcast(intent);
        }else if (result.getCode() == RemoteOperationResult.ResultCode.UNAUTHORIZED) {
            Intent intent = new Intent(DriveActivity.ACTION);
            intent.putExtra("error", true);
            DriveActivity.mgr.sendBroadcast(intent);
        }
        if (result.isException()) {
            Log_OC.e(TAG, "Checked 1" + result.getLogMessage(), result.getException());
        } else {
            renameLocalFile();
            Log_OC.e(TAG, "Checked 2" + result.getLogMessage());
            RefreshOperation refreshOperation = new RefreshOperation(mContext, mIgnoreETag,
                    mParentId, mParentRemotePath, null, null,false);
            refreshOperation.execute(client);
        }
        return result;
    }

    public void handlerOperation(final String code, final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.showError(mContext, code, message);
            }
        }, 1000);
    }

    private void renameLocalFile() {
        if (Utils.localFileExists(FileStorageManager.getSavePath(mContext) + mOCFile.getRemotePath())) {
            String storagePath = FileStorageManager.getSavePath(mContext) + mOCFile.getRemotePath();
            ArrayList<String> result = new ArrayList<>(Arrays.asList(storagePath.split("/")));
            File originalFile = new File(storagePath);
            File renamedFile = null;
            String newPath = "";
            result.remove(result.size() - 1);
            for (int j = 0; j < result.size(); j++) {
                newPath += "/" + result.get(j);
            }
            renamedFile = new File(newPath + "/" + mNewName + Utils.suffixOf(mOCFile.getRemotePath()));
            boolean success = originalFile.renameTo(renamedFile);
            if (success) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.FILE_STORAGE_PATH, renamedFile.getAbsolutePath());
                mContext.getContentResolver().update(FileProvider.CONTENT_URI, contentValues,
                        DBHelper.FILE_REMOTE_ID + "=?", new String[]{mRemoteFile.getRemoteId()});
                FileStorageManager fileStorageManager = new FileStorageManager(mContext, mContext.getContentResolver());
                fileStorageManager.renameAll(mOCFile, renamedFile);
                Log.i(TAG, "Success to rename");
            } else {
                Log.i(TAG, "Unsuccess to rename");
            }
        }
    }
}
