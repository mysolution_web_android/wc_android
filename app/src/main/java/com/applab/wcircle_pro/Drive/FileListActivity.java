package com.applab.wcircle_pro.Drive;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.applab.wcircle_pro.Notification.NotificationDialogFragment;
import com.applab.wcircle_pro.Notification.NotificationProvider;
import com.applab.wcircle_pro.Notification.Notifications;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;

import java.io.File;
import java.util.ArrayList;

public class FileListActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView txtToolbarTitle;
    private RecyclerView recyclerView;
    private FileListAdapter adapter;
    private File file = Environment.getExternalStorageDirectory();
    private ArrayList<File> mFile = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private Bundle bundle;
    private DialogFragment notificationDialogFragment;
    private int fileLevel = 0;
    private ArrayList<File> fileParentLevel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_list);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(getResources().getString(R.string.title_activity_file_list));
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        setFileParent();
        if (Utils.localFileExists(file.getAbsolutePath())) {
            mFile = rearrangeFile(file);
            adapter = new FileListAdapter(getBaseContext(), mFile);
            recyclerView.setAdapter(adapter);
            fileParentLevel.add(file);
        }
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        setTouchListener();
        bundle = getIntent().getExtras();
    }

    private void setFileParent() {
        while (file != null && file.getParentFile() != null) {
            //fileParentLevel.add(file);
            if (file.getParentFile().getName().equals("storage")) {
                file = file.getParentFile();
                break;
            } else {
                file = file.getParentFile();
            }
        }
       /* ArrayList<File> fileArrayList = new ArrayList<>();
        for (int i = fileParentLevel.size() - 1; i <= 0; i++) {
            fileArrayList.add(fileParentLevel.get(i));
        }
        fileParentLevel.clear();
        fileParentLevel = fileArrayList;*/
    }

    private ArrayList<File> rearrangeFile(File current) {
        File[] fileList = current.listFiles();
        ArrayList<File> folder = new ArrayList<>();
        ArrayList<File> file = new ArrayList<>();
        ArrayList<File> arrFile = new ArrayList<>();
        for (int i = 0; i < fileList.length; i++) {
            if (!fileList[i].isFile()) {
                folder.add(fileList[i]);
            } else {
                file.add(fileList[i]);
            }
        }
        for (int j = 0; j < folder.size(); j++) {
            arrFile.add(folder.get(j));
        }
        for (int k = 0; k < file.size(); k++) {
            arrFile.add(file.get(k));
        }
        return arrFile;
    }

    @Override
    public void onBackPressed() {
        if (fileLevel > 0) {
            fileParentLevel.remove(fileLevel);
            fileLevel--;
        } else {
            Intent intent = new Intent(getBaseContext(), DriveActivity.class);
            intent.putExtra("level", bundle.getInt("level"));
            intent.putExtra("remotePathLevel", bundle.getStringArrayList("remotePathLevel"));
            intent.putExtra("parentLevel", bundle.getStringArrayList("parentLevel"));
            startActivity(intent);
            finish();
        }
        mFile = adapter.swapFile(rearrangeFile(fileParentLevel.get(fileLevel)));
    }

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
                final File current = (File) txtTitle.getTag();
                if (current.isFile()) {
                    Intent i = new Intent(getBaseContext(), DriveActivity.class);
                    i.putExtra("level", bundle.getInt("level"));
                    i.putExtra("remotePathLevel", bundle.getStringArrayList("remotePathLevel"));
                    i.putExtra("parentLevel", bundle.getStringArrayList("parentLevel"));

                    String path = current.getAbsolutePath();
                    OCFile ocFile = new OCFile();
                    ocFile.setRemotePath(bundle.getStringArrayList("remotePathLevel").get(bundle.getInt("level")) + current.getName());
                    ocFile.setMimeType(Utils.getLocalFileMimiType(FileListActivity.this, current));
                    if (!Utils.isConnectingToInternet(getBaseContext())) {
                        Utils.showNoConnection(FileListActivity.this);
                    } else {
                        Intent intent = new Intent(getBaseContext(), UploadIntentService.class);
                        intent.putExtra("OCFile", (Parcelable) ocFile);
                        intent.putExtra("StoragePath", path);
                        intent.putExtra("ParentId", bundle.getStringArrayList("parentLevel").get(bundle.getInt("level")));
                        startService(intent);
                        startActivity(i);
                        finish();
                    }
                } else {
                    File[] list = current.listFiles();
                    if (list != null && list.length > 0) {
                        fileLevel++;
                        fileParentLevel.add(current);
                        mFile = adapter.swapFile(rearrangeFile(current));
                    } else {
                        Utils.showError(FileListActivity.this, Utils.CODE_NO_RESULT, Utils.NO_RESULT);
                    }
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(Utils.NOTIFICATION_ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_file_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            if (fileLevel > 0) {
                fileParentLevel.remove(fileLevel);
                fileLevel--;
            } else {
                File current = fileParentLevel.get(fileLevel);
                fileParentLevel.clear();
                fileParentLevel.add(current);
                fileLevel = 0;
                Intent intent = new Intent(getBaseContext(), DriveActivity.class);
                intent.putExtra("level", bundle.getInt("level"));
                intent.putExtra("remotePathLevel", bundle.getStringArrayList("remotePathLevel"));
                intent.putExtra("parentLevel", bundle.getStringArrayList("parentLevel"));
                startActivity(intent);
                finish();
            }
            mFile = adapter.swapFile(rearrangeFile(fileParentLevel.get(fileLevel)));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setPopUpWindow(Cursor cursor) {
        notificationDialogFragment = new NotificationDialogFragment();
        Bundle args = new Bundle();
        ArrayList<Notifications> arrayList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Notifications notifications = new Notifications();
                notifications.setId(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_ID)));
                notifications.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_CREATE_DATE)));
                notifications.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_TITLE)));
                notifications.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_DESCRIPTION)));
                notifications.setType(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_TYPE)));
                arrayList.add(notifications);
            }
            while (cursor.moveToNext());
            cursor.close();
        }
        args.putParcelableArrayList("Notifications", arrayList);
        notificationDialogFragment.setArguments(args);
        notificationDialogFragment.setCancelable(false);
        notificationDialogFragment.show(getSupportFragmentManager(), "");
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Utils.NOTIFICATION_ACTION)) {
                Uri uri = NotificationProvider.CONTENT_URI;
                String[] projection = {
                        DBHelper.NOTIFICATION_COLUMN_TYPE,
                        DBHelper.NOTIFICATION_COLUMN_TITLE,
                        DBHelper.NOTIFICATION_COLUMN_CREATE_DATE,
                        DBHelper.NOTIFICATION_COLUMN_DESCRIPTION,
                        DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY,
                        DBHelper.NOTIFICATION_COLUMN_ID
                };
                String selection = DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY + "=?";
                String[] selectionArgs = {"0"};
                Cursor cursor = getContentResolver().query(uri, projection, selection, selectionArgs, DBHelper.NOTIFICATION_COLUMN_ID + " DESC ");
                if (notificationDialogFragment != null) {
                    if (!notificationDialogFragment.isHidden()) {
                        notificationDialogFragment.dismiss();
                    }
                }
                setPopUpWindow(cursor);
            }
        }
    };
}
