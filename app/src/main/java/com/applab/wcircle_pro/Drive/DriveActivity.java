package com.applab.wcircle_pro.Drive;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.Menu.NavigationDrawerFragment;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.OwnCloudClientFactory;
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory;
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener;
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.ExistenceCheckRemoteOperation;
import com.owncloud.android.lib.resources.files.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DriveActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>,
        OnRemoteOperationListener, OnDatatransferProgressListener, SwipyRefreshLayout.OnRefreshListener {
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private RecyclerView recyclerView;
    private DriveAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private OwnCloudClient mClient;
    private Handler mHandler = new Handler();
    private ArrayList<String> parentLevel = new ArrayList<>();
    private ArrayList<String> remotePathLevel = new ArrayList<>();
    private int level = 0;
    private Bundle bundle;
    private Cursor cursor;
    private RefreshOperation operation;
    private CreateFolderOperation createOperation;
    private RenameFileOperation renameFileOperation;
    private RemoveOperation removeOperation;
    private String selection = DBHelper.FILE_PARENT_ID + "=?";
    private String parentId = "0";
    private String[] selectionArgs = {parentId};
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private String TAG = "DRIVE";
    public static LocalBroadcastManager mgr;
    public static final String ACTION = "DRIVE_REFRESH";
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    private TextView txtError, txtToolbarTitle;
    private ImageView space1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_purple_light));
        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_drive));
        space1 = (ImageView) toolbar.findViewById(R.id.space1);
        space1.setVisibility(View.INVISIBLE);
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setSelectedPosition(11);
        drawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new DriveAdapter(this, cursor, getSupportFragmentManager());
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        mHandler = new Handler();
        mgr = LocalBroadcastManager.getInstance(this);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            parentLevel = bundle.getStringArrayList("parentLevel");
            remotePathLevel = bundle.getStringArrayList("remotePathLevel");
            level = bundle.getInt("level");
            Log.i(TAG, "Remote Level: " + remotePathLevel.get(level));
        } else {
            level = 0;
            parentLevel.add("0");
            remotePathLevel.add(FileUtils.PATH_SEPARATOR);
        }
        setTouchListener();
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        pref = getApplicationContext().getSharedPreferences("OWNCLOUD PASSWORD", 0);
        editor = pref.edit();

        String password = pref.getString("own_password", null);
        String userId = Utils.getProfile(this).getEmail();

        Uri serverUri = Uri.parse(getString(R.string.server_base_url));
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true);
        mClient.setCredentials(OwnCloudCredentialsFactory.newBasicCredentials(userId, password));
        txtError = (TextView) findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);
        txtError.setTag(false);
        toolbar.setNavigationIcon(R.mipmap.action_arrow_prev_white);
        toolbar.setNavigationOnClickListener(toolbarOnClickListener);
    }

    private View.OnClickListener toolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (level > 0) {
                parentLevel.remove(level);
                remotePathLevel.remove(level);
                level--;
                if (level != 0) {
                    txtToolbarTitle.setText(remotePathLevel.get(level).split("/")[remotePathLevel.get(level).split("/").length - 1]);
                } else {
                    txtToolbarTitle.setText(getResources().getString(R.string.title_activity_drive));
                }
            } else {
                parentLevel.clear();
                remotePathLevel.clear();
                parentLevel.add("0");
                remotePathLevel.add(FileUtils.PATH_SEPARATOR);
                level = 0;
                Utils.clearPreviousActivity(DriveActivity.this);
            }
            selectionArgs = new String[]{parentLevel.get(level)};
            getSupportLoaderManager().restartLoader(0, null, DriveActivity.this);
        }
    };

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                cursor.moveToPosition(position);
                final OCFile file = getOCFile(cursor, position);
                if (file.getMimeType().equals("DIR")) {
                    parentId = file.getRemoteId();
                    parentLevel.add(parentId);
                    remotePathLevel.add(file.getRemotePath());
                    level++;
                    txtToolbarTitle.setText(remotePathLevel.get(level).split("/")[remotePathLevel.get(level).split("/").length - 1]);
                    operation = new RefreshOperation(DriveActivity.this, true, parentId, file.getRemotePath(), null, null, false);
                    operation.execute(mClient, DriveActivity.this, mHandler);
                    selectionArgs = new String[]{parentId};
                    getSupportLoaderManager().restartLoader(0, null, DriveActivity.this);
                } else if (!file.getMimeType().equals("DIR")) {
                    if (file.getStoragePath() != null) {
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        final String mimiType = mime.getExtensionFromMimeType(file.getMimeType());
                        if (Utils.localFileExists(file.getStoragePath())) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(new File(file.getStoragePath())),
                                    MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimiType));
                            startActivity(intent);
                        }
                    }
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                cursor.moveToPosition(position);
                final OCFile file = getOCFile(cursor, position);
                int id = cursor.getInt(cursor.getColumnIndex(DBHelper.FILE_ID));
                DriveActionDialogFragment driveActionDialogFragment = DriveActionDialogFragment.newInstance(file, DriveActivity.this,
                        getSupportFragmentManager(), parentLevel, remotePathLevel, level, removeOperation, mClient, id);
                driveActionDialogFragment.show(getSupportFragmentManager(), "");
                return true;
            }
        });
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        if (level > 0) {
            parentLevel.remove(level);
            remotePathLevel.remove(level);
            level--;
            if (level != 0) {
                txtToolbarTitle.setText(remotePathLevel.get(level).split("/")[remotePathLevel.get(level).split("/").length - 1]);
            } else {
                txtToolbarTitle.setText(getResources().getString(R.string.title_activity_drive));
            }
        } else {
            parentLevel.clear();
            remotePathLevel.clear();
            parentLevel.add("0");
            remotePathLevel.add(FileUtils.PATH_SEPARATOR);
            level = 0;
            Utils.clearPreviousActivity(DriveActivity.this);
        }
        selectionArgs = new String[]{parentLevel.get(level)};
        getSupportLoaderManager().restartLoader(0, null, DriveActivity.this);
    }

    public static String nameChecking(String name, String path, OwnCloudClient client, boolean isFile, Context context) {
        String message = "";
        if (name.equals("")) {
            if (!isFile) {
                message = Utils.getDialogMessage(context, Utils.CODE_FOLDER_NAME_FIELD, Utils.FOLDER_NAME_FIELD);
            } else {
                message = Utils.getDialogMessage(context, Utils.CODE_FILE_NAME_FIELD, Utils.FILE_NAME_FIELD);
            }
        } else if (!name.equals("")) {
            Pattern p = Pattern.compile("^[a-zA-Z0-9_\\s]*$");
            if (isFile) {
                p = Pattern.compile("^[a-zA-Z0-9_]*$");
            }
            Matcher m = p.matcher(name);
            boolean b = m.find();
            if (!b) {
                message = Utils.getDialogMessage(context, Utils.CODE_INVALID_NAME, Utils.INVALID_NAME);
            }
        }
        return message;
    }

    public static boolean existsFile(OwnCloudClient client, String remotePath) {
        ExistenceCheckRemoteOperation existsOperation =
                new ExistenceCheckRemoteOperation(remotePath, false);
        RemoteOperationResult result = existsOperation.execute(client);
        return result.isSuccess();
    }

    private void setPopUpCreateFolderWindow() {
        CreateFolderDialogFragment createFolderDialogFragment = CreateFolderDialogFragment.newInstance(level, parentLevel, remotePathLevel);
        createFolderDialogFragment.show(getSupportFragmentManager(), "");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_drive, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (drawerFragment.mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {

            return true;
        } else if (id == R.id.action_menu) {
            View v = findViewById(R.id.action_menu);
            PopupMenu popupMenu = new PopupMenu(toolbar.getContext(), v) {
                @Override
                public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_folder:
                            setPopUpCreateFolderWindow();
                            return true;
                        case R.id.action_upload:
                            Intent intent = new Intent(DriveActivity.this, FileListActivity.class);
                            intent.putExtra("level", level);
                            intent.putExtra("remotePathLevel", remotePathLevel);
                            intent.putExtra("parentLevel", parentLevel);
                            startActivity(intent);
                            finish();
                            return true;
                        default:
                            return super.onMenuItemSelected(menu, item);
                    }
                }
            };

            popupMenu.inflate(R.menu.menu_custom);
            popupMenu.getMenu().removeItem(R.id.action_new_chat);
            popupMenu.getMenu().removeItem(R.id.action_clear_employee);
            popupMenu.getMenu().removeItem(R.id.action_create_group);
            popupMenu.getMenu().removeItem(R.id.action_gallery);
            popupMenu.getMenu().removeItem(R.id.action_camera);
            popupMenu.getMenu().removeItem(R.id.action_close_group);
            popupMenu.getMenu().removeItem(R.id.action_group_info);
            popupMenu.getMenu().removeItem(R.id.action_add_people);
            popupMenu.getMenu().removeItem(R.id.action_exit_group);
            popupMenu.getMenu().removeItem(R.id.action_delete_group);
            popupMenu.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(DriveActivity.ACTION)) {
                if (intent.getBooleanExtra("error", false)) {
                    parentLevel.clear();
                    remotePathLevel.clear();
                    level = 0;
                    parentId = "0";
                    remotePathLevel.add(0, FileUtils.PATH_SEPARATOR);
                    parentLevel.add(0, parentId);
                    getContentResolver().delete(FileProvider.CONTENT_URI, null, null);
                    txtError.setText(Utils.getDialogMessage(DriveActivity.this, Utils.CODE_UNKNOWN_DRIVE_ACCOUNT, Utils.UNKNOWN_DRIVE_ACCOUNT));
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setTag(true);
                    adapter.swapCursor(null);
                } else if (!intent.getBooleanExtra("error", false)) {
                    txtError.setVisibility(View.GONE);
                    adapter.swapCursor(cursor);
                    txtError.setTag(false);
                } else if (intent.getBooleanExtra("connection_not_available", false)) {
                    txtError.setText(Utils.getDialogMessage(DriveActivity.this, Utils.CODE_CONNECTION, Utils.CONNECTION));
                    txtError.setVisibility(View.VISIBLE);
                    adapter.swapCursor(null);
                    txtError.setTag(true);
                }
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        getSupportLoaderManager().initLoader(0, null, this);
        selectionArgs = new String[]{parentLevel.get(level)};
        getSupportLoaderManager().restartLoader(0, null, DriveActivity.this);
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(Utils.NOTIFICATION_ACTION);
        iff.addAction(DriveActivity.ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
        if (Utils.isConnectingToInternet(DriveActivity.this)) {
            operation = new RefreshOperation(this, false, parentLevel.get(level), remotePathLevel.get(level), null, null, false);
            operation.execute(mClient, this, mHandler);
            txtError.setVisibility(View.GONE);
            txtError.setTag(false);
            adapter.swapCursor(cursor);
        } else {
            txtError.setVisibility(View.VISIBLE);
            txtError.setText(Utils.getDialogMessage(DriveActivity.this, Utils.CODE_CONNECTION, Utils.CONNECTION));
            txtError.setTag(true);
            adapter.swapCursor(null);
        }
    }

    public static OCFile getOCFile(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        final OCFile file = new OCFile();
        file.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.FILE_ID)));
        file.setName(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_NAME)));
        file.setLength(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_CONTENT_LENGTH)));
        file.setRemoteId(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_REMOTE_ID)));
        file.setRemotePath(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_REMOTE_PATH)));
        file.setCreationTimestamp(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_CREATION)));
        file.setEtag(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_ETAG)));
        file.setMimeType(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_CONTENT_TYPE)));
        file.setParentId(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_PARENT_ID)));
        file.setModifiedTimestamp(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_MODIFIED)));
        file.setPermissions(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_PERMISSIONS)));
        file.setSize(cursor.getLong(cursor.getColumnIndex(DBHelper.FILE_SIZE)));
        if (!cursor.isNull(cursor.getColumnIndex(DBHelper.FILE_STORAGE_PATH))) {
            file.setStoragePath(cursor.getString(cursor.getColumnIndex(DBHelper.FILE_STORAGE_PATH)));
        }
        return file;
    }

    @Override
    public void onTransferProgress(long progressRate, long totalTransferredSoFar, long totalToTransfer, String fileAbsoluteName) {

    }

    @Override
    public void onRemoteOperationFinish(RemoteOperation caller, RemoteOperationResult result) {

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getBaseContext(), FileProvider.CONTENT_URI, null, selection, selectionArgs,
                "CASE WHEN " + DBHelper.FILE_CONTENT_TYPE + " = 'DIR' then -1 else " + DBHelper.FILE_ID + " end");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            this.cursor = data;
            if (!(Boolean) (txtError.getTag())) {
                adapter.swapCursor(data);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at " + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            if (!Utils.isConnectingToInternet(getBaseContext())) {
                txtError.setVisibility(View.VISIBLE);
                txtError.setTag(true);
                adapter.swapCursor(null);
                txtError.setText(Utils.getDialogMessage(DriveActivity.this, Utils.CODE_CONNECTION, Utils.CONFIRM_PASSWORD_LENGTH));
            } else {
                txtError.setVisibility(View.GONE);
                txtError.setTag(false);
                adapter.swapCursor(cursor);
                operation = new RefreshOperation(DriveActivity.this, false, parentLevel.get(level), remotePathLevel.get(level), null, null, true);
                operation.execute(mClient, DriveActivity.this, mHandler);
                selectionArgs = new String[]{parentLevel.get(level)};
                getSupportLoaderManager().restartLoader(0, null, DriveActivity.this);
            }
            Utils.disableSwipyRefresh(mSwipyRefreshLayout);
        }
    }
}
