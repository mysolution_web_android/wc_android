package com.applab.wcircle_pro.Drive;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by user on 27/10/2015.
 */
public class DriveDetailsDialogFragment extends DialogFragment {
    private OCFile mFile;
    private TextView mTxtTitle,mTxtName, mTxtModifiedDate, mTxtSize,mTxtUrl;
    private ImageView mBtnCancel;
    private static AlertDialog.Builder builder;
    private static Context mContext;
    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DriveDetailsDialogFragment newInstance(OCFile file, Context context) {
        DriveDetailsDialogFragment frag = new DriveDetailsDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("file", file);
        mContext = context;
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mFile = getArguments().getParcelable("file");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_drive, null);
        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtUrl = (TextView) v.findViewById(R.id.txtUrl);
        mTxtUrl.setText(mFile.getRemotePath());
        mTxtTitle.setText(mFile.getName());
        mTxtSize = (TextView) v.findViewById(R.id.txtSize);
        mTxtSize.setText(Utils.bytesIntoHumanReadable(mFile.getLength()));
        mTxtModifiedDate = (TextView) v.findViewById(R.id.txtModifiedDate);
        mTxtName = (TextView) v.findViewById(R.id.txtName);
        mTxtName.setText(mFile.getName());
        Timestamp stamp = new Timestamp(mFile.getModifiedTimestamp());
        Date date = new Date(stamp.getTime());
        SimpleDateFormat format = new SimpleDateFormat("d MMMM yyyy", Locale.US);
        mTxtModifiedDate.setText(format.format(date));
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DriveDetailsDialogFragment.this.getDialog().cancel();
        }
    };
}

