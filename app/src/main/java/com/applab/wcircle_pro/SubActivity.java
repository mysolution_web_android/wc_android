package com.applab.wcircle_pro;

import android.support.v4.app.NavUtils;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class SubActivity extends AppCompatActivity {
    /*private HubConnection connection;
    private HubProxy hub;*/
    private TextView resultView;
    private Button sendButton;
    private EditText inputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        resultView = (TextView) findViewById(R.id.resultView);
        initialize();
        connectToServer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        prepareGetMessage();
    }

    private void initialize() {
        String serverUrl = "http://192.168.66.63:8080/signalr/";
        String hubName = "chathub";
        /*Platform.loadPlatformComponent(new AndroidPlatformComponent());
        connection = new HubConnection(serverUrl);
        hub = connection.createHubProxy(hubName);*/
        sendButton = (Button) findViewById(R.id.sendButton);
        inputText = (EditText) findViewById(R.id.inputText);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage(inputText.getText().toString());
            }
        });
    }

    private void prepareGetMessage() {
        /*connection.received(new MessageReceivedHandler() {
            @Override
            public void onMessageReceived(JsonElement json) {
                resultView.setText(json.toString() + " \n");
            }
        });*/
    }

    private void connectToServer() {
        try {
            /*SignalRFuture<Void> awaitConnection = connection.start();
            awaitConnection.get();*/
            Toast.makeText(this, "Connected", Toast.LENGTH_LONG).show();
        } catch (Exception E) {
            Log.i("SignalR", "Failed to Connect to Server");
        }
    }

    private void sendMessage(String msg) {
        try {
            /*hub.invoke("send","android", msg).get();*/
        } catch (Exception E) {
            Log.i("SignalR", "Fail to send message");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*connection.stop();*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sub, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
