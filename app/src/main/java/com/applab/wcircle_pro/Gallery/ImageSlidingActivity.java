package com.applab.wcircle_pro.Gallery;

import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import java.util.ArrayList;

public class ImageSlidingActivity extends AppCompatActivity {
    private ImageSlidingAdapter adapter;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private Bundle bundle;
    private String id;
    private TextView title;
    private LinearLayout btnSave;
    private String TAG = "Image Sliding";
    private int selectedPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_sliding);
        /*toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);*/
        title = (TextView) findViewById(R.id.imgTitle);
        btnSave = (LinearLayout) findViewById(R.id.btnSave);
        viewPager = (ViewPager) findViewById(R.id.pager);
        bundle = getIntent().getExtras();
        id = bundle.getString("id");
        selectedPage = bundle.getInt("position");
        adapter = new ImageSlidingAdapter(getSupportFragmentManager(), ImageSlidingActivity.this,
                getImage(id));
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(selectedPage);
        String[] result = getImage(id).get(viewPager.getCurrentItem()).split("/");
        title.setText(result[result.length - 1]);
        btnSave.setTag(getImage(id).get(selectedPage));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                String[] result = getImage(id).get(i).split("/");
                title.setText(result[result.length - 1]);
                btnSave.setTag(getImage(id).get(i));
                selectedPage = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isConnectingToInternet(getBaseContext())) {
                    Utils.downloadFile(btnSave.getTag().toString(), ImageSlidingActivity.this, btnSave.getTag().toString(),
                            getBaseContext().getString(R.string.gallery), "Gallery", getImageId(id).get(selectedPage));
                    if (Utils.getStatus(Utils.SINGLE_IMAGE_DOWNLOAD, ImageSlidingActivity.this)) {
                        HttpHelper.getGsonDownload(getImageId(id).get(selectedPage), ImageSlidingActivity.this, TAG);
                    }
                } else {
                    Utils.showNoConnection(ImageSlidingActivity.this);
                }
            }
        });
        //hideBar();
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        // enable status bar tint
        tintManager.setStatusBarTintEnabled(true);
        // enable navigation bar tint
        tintManager.setNavigationBarTintEnabled(true);
        // set a custom tint color for all system bars
        tintManager.setTintColor(Color.BLACK);
    }

    public void handleAction(View v) {
        if (title.getVisibility() == View.GONE) {
            Utils.expand(title);
        } else {
            Utils.collapse(title);
        }

        if (btnSave.getVisibility() == View.GONE) {
            Utils.expand(btnSave);
        } else {
            Utils.collapse(btnSave);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void hideBar() {
        if (Build.VERSION.SDK_INT < 16) { //ye olde method
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else { // Jellybean and up, new hotness
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            // Remember that you should never show the action bar if the
            // status bar is hidden, so hide that too if necessary.
            ActionBar actionBar = getSupportActionBar();
            actionBar.hide();
        }
    }

    private ArrayList<String> getImageId(String id) {
        Uri uri = ImageProvider.CONTENT_URI;
        String selection = DBHelper.IMAGE_COLUMN_ALBUM_ID + "=?";
        String[] selectionArgs = new String[]{id};
        Cursor cursor = null;
        ArrayList<String> arrString = new ArrayList<>();
        try {
            cursor = getContentResolver().query(uri, null, selection, selectionArgs, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String data = cursor.getString(cursor.getColumnIndex(DBHelper.IMAGE_COLUMN_IMAGE_ID));
                    arrString.add(data);
                }
                cursor.close();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrString;
    }

    private ArrayList<String> getImage(String id) {
        Uri uri = ImageProvider.CONTENT_URI;
        String selection = DBHelper.IMAGE_COLUMN_ALBUM_ID + "=?";
        String[] selectionArgs = new String[]{id};
        Cursor cursor = null;
        ArrayList<String> arrString = new ArrayList<>();
        try {
            cursor = getContentResolver().query(uri, null, selection, selectionArgs, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String data = cursor.getString(cursor.getColumnIndex(DBHelper.IMAGE_COLUMN_IMAGE_LARGE));
                    arrString.add(data);
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrString;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_sliding, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
    }
}
