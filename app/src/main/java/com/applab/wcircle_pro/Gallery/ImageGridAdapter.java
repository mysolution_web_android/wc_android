package com.applab.wcircle_pro.Gallery;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import java.util.ArrayList;

/**
 * Created by user on 14/9/2015.
 */
public class ImageGridAdapter extends BaseAdapter {
    private Context context;
    private Cursor cursor;
    private LayoutInflater inflater = null;
    private ArrayList<String> arrUrl = new ArrayList<>();
    private ArrayList<String> arrId = new ArrayList<>();
    private String TAG = "NEWS";
    private String url = Utils.API_URL + "api/Tracking";
    private boolean isSelected = false;
    private int selected = 0;
    private LinearLayout btn, btnCancel, btnSubmit;
    private ArrayList<ImageTag> arrTag = new ArrayList<>();
    private boolean isVisible = false;
    private boolean isSelect = false;
    private FragmentManager fragmentManager;
    private DownloadImageDialogFragment downloadImageDialogFragment;

    public ImageGridAdapter(Context context, Cursor cursor, LinearLayout btn,
                            LinearLayout btnCancel, LinearLayout btnSubmit, FragmentManager fragmentManager) {
        this.context = context;
        this.cursor = cursor;
        this.btn = btn;
        this.btnSubmit = btnSubmit;
        this.btnCancel = btnCancel;
        setCheckBoxStatus();
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.fragmentManager = fragmentManager;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public void setVisibilityCheckBox(boolean isVisible) {
        this.isVisible = isVisible;
        this.notifyDataSetChanged();
    }

    public void selectAll(boolean isSelect) {
        this.isSelect = isSelect;
        this.notifyDataSetChanged();
    }

    private View.OnClickListener btnCancelOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isSelected) {
                isSelected = false;
                setIsSelected(isSelected);
                selectAll(false);
                setVisibilityCheckBox(false);
                btn.setVisibility(View.GONE);
            }
            setCheckBoxStatus();
        }
    };

    private void setCheckBoxStatus() {
        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToFirst();
            arrTag.clear();
            do {
                ImageTag tag = new ImageTag();
                tag.status = false;
                arrTag.add(tag);
            } while (cursor.moveToNext());
        }
    }

    private View.OnClickListener btnSaveOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isSelected && selected > 0 && arrUrl.size() > 0) {
                String message = "Do you want to save the images?" +
                        " (Images will be saved at " + context.getString(R.string.folder_name) + "/" + context.getString(R.string.gallery) + " folder)";
                downloadImageDialogFragment = DownloadImageDialogFragment.newInstance(context, message, arrUrl, arrId, selected);
                downloadImageDialogFragment.show(fragmentManager, "");
            } else if (selected == 0 && arrUrl.size() == 0) {
                isSelected = false;
                setIsSelected(isSelected);
                selectAll(false);
                setVisibilityCheckBox(false);
                btn.setVisibility(View.GONE);
            }
            setCheckBoxStatus();
        }
    };


    @Override
    public int getCount() {
        return cursor == null ? 0 : cursor.getCount();
    }

    @Override
    public Object getItem(int position) {
        return cursor.moveToPosition(position);
    }

    @Override
    public long getItemId(int position) {
        cursor.moveToPosition(position);
        return cursor.getInt(cursor.getColumnIndex(DBHelper.NEWS_IMAGE_COLUMN_ID));
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final Holder holder;
        final Image current = getData(cursor, position);
        if (row == null) {
            holder = new Holder();
            row = inflater.inflate(R.layout.custom_image_grid, parent, false);
            holder.img = (ImageView) row.findViewById(R.id.img);
            holder.progressBar = (ProgressBar) row.findViewById(R.id.progressBar);
            holder.checkBox = (CheckBox) row.findViewById(R.id.ckImage);
            row.setTag(holder);
        } else {
            holder = (Holder) row.getTag();
        }
        ImageTag tag = new ImageTag();
        tag.url = current.getPhotoLarge();
        tag.id = String.valueOf(current.getId());
        holder.checkBox.setTag(tag);
        holder.checkBox.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
        holder.checkBox.setChecked(isSelect);
        final CheckBox checkBox = holder.checkBox;
        final ProgressBar progress = holder.progressBar;
        Glide.with(context)
                .load(current.getPhotoSmall())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new GlideDrawableImageViewTarget(holder.img) {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);
                        progress.setVisibility(View.GONE);
                    }
                });
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isSelected) {
                    Intent intent = new Intent(context, ImageSlidingActivity.class);
                    intent.putExtra("id", String.valueOf(current.getAlbumId()));
                    intent.putExtra("position", position);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    ImageTag imageTag = (ImageTag) checkBox.getTag();
                    if (checkBox.isChecked()) {
                        checkBox.setChecked(false);
                        arrTag.get(position).status = false;
                        if (selected > 0) {
                            selected -= 1;
                            setSelected(selected);
                        }
                        if (arrUrl.contains(imageTag.url)) {
                            arrUrl.remove(imageTag.url);
                        }

                        if (arrId.contains(imageTag.id)) {
                            arrUrl.remove(imageTag.id);
                        }
                    } else {
                        checkBox.setChecked(true);
                        arrTag.get(position).status = true;
                        selected += 1;
                        setSelected(selected);
                        arrUrl.add(imageTag.url);
                        arrId.add(imageTag.id);
                        Log.i(TAG, "Id size: " + String.valueOf(arrId.size()));
                        Log.i(TAG, "Url Size: " + String.valueOf(arrUrl.size()));
                    }
                }
            }
        });

        holder.img.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if (!isSelected) {
                    isSelected = true;
                    setIsSelected(isSelected);
                    selected++;
                    setSelected(selected);
                    btn.setVisibility(View.VISIBLE);
                    setVisibilityCheckBox(true);
                    checkBox.setChecked(true);
                    arrTag.get(position).status = true;
                    Utils.vibrate(250, context);
                    arrId.clear();
                    arrUrl.clear();
                    ImageTag imageTag = (ImageTag) checkBox.getTag();
                    arrUrl.add(imageTag.url);
                    arrId.add(imageTag.id);
                }
                return true;
            }
        });
        holder.checkBox.setChecked(arrTag.get(position).status);
        btnSubmit.setOnClickListener(btnSaveOnClick);
        btnCancel.setOnClickListener(btnCancelOnClick);
        return row;
    }

    public Image getData(Cursor cursor, int position) {
        Image image = new Image();
        cursor.moveToPosition(position);
        image.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.IMAGE_COLUMN_IMAGE_ID)));
        image.setAlbumId(cursor.getString(cursor.getColumnIndex(DBHelper.IMAGE_COLUMN_ALBUM_ID)));
        image.setPhotoSmall(cursor.getString(cursor.getColumnIndex(DBHelper.IMAGE_COLUMN_IMAGE_SMALL)));
        image.setPhotoLarge(cursor.getString(cursor.getColumnIndex(DBHelper.IMAGE_COLUMN_IMAGE_LARGE)));
        return image;
    }

    public class Holder {
        ImageView img;
        ProgressBar progressBar;
        CheckBox checkBox;
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            setCheckBoxStatus();
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            ImageGridAdapter.this.notifyDataSetChanged();
        }
    };
}