package com.applab.wcircle_pro.Gallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;

import com.applab.wcircle_pro.R;
import com.gesture_image_view.GestureImageView;

/**
 * Created by user on 6/10/2015.
 */
public class ImageSlidingImageAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private Context mContext;
    private Bitmap mBitmap;

    public ImageSlidingImageAdapter(Context context, Bitmap bitmap) {
        mContext = context;
        mBitmap = bitmap;
        mInflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_image_sliding_image, parent, false);
            holder.imgDisplay = (GestureImageView) convertView.findViewById(R.id.imgDisplay);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.progressBar.setVisibility(View.VISIBLE);
        if (mBitmap != null) {
            holder.imgDisplay.setImageBitmap(mBitmap);
            holder.progressBar.setVisibility(View.GONE);
        }
        return convertView;
    }

    public class Holder {
        GestureImageView imgDisplay;
        ProgressBar progressBar;
    }

    public Bitmap swapBitmap(Bitmap bitmap) {
        if (this.mBitmap == bitmap) {
            return null;
        }
        Bitmap oldBitmap = this.mBitmap;
        this.mBitmap = bitmap;
        if (bitmap != null) {
            this.notifyDataSetChanged();
        }
        return oldBitmap;
    }
}
