package com.applab.wcircle_pro.Gallery;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;


public class GalleryAdapter extends RecyclerView.Adapter<GalleryViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;
    private String TAG = "IMAGE";


    public GalleryAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("GalleryViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_gallery_grid, parent, false);
        GalleryViewHolder holder = new GalleryViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final GalleryViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Gallery current = getData(cursor, position);
        Glide.with(context)
                .load(current.getAlbumCover())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new GlideDrawableImageViewTarget(holder.imgAlbum) {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);
                        holder.progressBar.setVisibility(View.GONE);
                    }
                });
        holder.txtName.setText(current.getTitle());
        holder.txtNoPhoto.setText(String.valueOf(current.getTotalImage()));
        holder.rlNew.setVisibility(current.getNewCreate() ? View.VISIBLE : View.GONE);
        Log.i(TAG, "Url:" + current.getAlbumCover());
    }

    public Gallery getData(Cursor cursor, int position) {
        Gallery gallery = new Gallery();
        cursor.moveToPosition(position);
        gallery.setAlbumCover(cursor.getString(cursor.getColumnIndex(DBHelper.ALBUM_COLUMN_ALBUM_COVER)));
        gallery.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.ALBUM_COLUMN_TITLE)));
        gallery.setTotalImage(cursor.getInt(cursor.getColumnIndex(DBHelper.ALBUM_COLUMN_TOTAL_IMAGE)));
        boolean newCreate = false;
        if (!cursor.isNull(cursor.getColumnIndex(DBHelper.ALBUM_COLUMN_NEW_CREATE))) {
            newCreate = Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.ALBUM_COLUMN_NEW_CREATE)));
        }
        gallery.setNewCreate(newCreate);
        return gallery;
    }

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            GalleryAdapter.this.notifyDataSetChanged();
        }
    };

}