package com.applab.wcircle_pro.Gallery;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;

/**
 * Created by user on 2/12/2015.
 */
public class DownloadImageDialogFragment extends DialogFragment {
    private String mMessage;
    private ArrayList<String> mId, mUrl;
    private int mSelected = 0;
    private TextView mTxtTitle, mTxtMessage;
    private ImageView mBtnCancel;
    private LinearLayout mBtnYes, mBtnNo;
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private String TAG = "DOWNLOAD EVENT";

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DownloadImageDialogFragment newInstance(Context context, String message, ArrayList<String> url, ArrayList<String> id, int selected) {
        DownloadImageDialogFragment frag = new DownloadImageDialogFragment();
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putStringArrayList("url", url);
        args.putStringArrayList("id", id);
        args.putInt("selected", selected);
        mContext = context;
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mMessage = getArguments().getString("message");
        mUrl = getArguments().getStringArrayList("url");
        mId = getArguments().getStringArrayList("id");
        mSelected = getArguments().getInt("selected");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_dialog, null);
        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtMessage = (TextView) v.findViewById(R.id.txtMessage);
        mTxtTitle.setText(getString(R.string.download));
        mTxtMessage.setText(mMessage);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnYes = (LinearLayout) v.findViewById(R.id.btnYes);
        mBtnNo = (LinearLayout) v.findViewById(R.id.btnNo);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);
        mBtnYes.setOnClickListener(btnYesOnClickListener);
        mBtnNo.setOnClickListener(btnNoOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DownloadImageDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnYesOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
            Intent intent = new Intent(ImageActivity.ACTION);
            intent.putExtra("isYes", true);
            if (mSelected > 0) {
                if (Utils.isConnectingToInternet(mContext)) {
                    save(mUrl, mId);
                } else {
                    Utils.showNoConnection(mContext);
                }
                intent.putExtra("isSelectedMoreThanZero", true);
            } else {
                intent.putExtra("isSelectedMoreThanZero", false);
            }
            mgr.sendBroadcast(intent);
            DownloadImageDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnNoOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DownloadImageDialogFragment.this.getDialog().cancel();
        }
    };

    public void save(ArrayList<String> arrUrl, ArrayList<String> arrId) {
        String id = "";
        for (int i = 0; i < arrUrl.size(); i++) {
            if (id.equals("")) {
                id += arrId.get(i);
            } else {
                id += "," + arrId.get(i);
            }
            Utils.downloadFile(arrUrl.get(i), mContext, arrUrl.get(i), mContext.getString(R.string.gallery), "Gallery", arrId.get(i));
        }
        if (Utils.getStatus(Utils.IMAGE_DOWNLOAD, mContext)) {
            HttpHelper.getGsonDownload(id, mContext, TAG);
        }
    }
}
