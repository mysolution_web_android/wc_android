package com.applab.wcircle_pro.Gallery;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Dashboard.DashboardProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by user on 23/11/2015.
 */
public class HttpHelper {
    //region Album
    public static void getAlbumGson(Context context, final int pageNo, final String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        final Date lastUpdateDate = getAlbumLastUpdate(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url;
        if (Utils.getStatus(Utils.GALLERY_REFRESH, null)) {
            url = Utils.API_URL + "api/Gallery?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                    + Utils.encode(pageNo) + "LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty="
                    + Utils.encode(getGalleryRow(context, pageNo) > 0);
        } else {
            url = Utils.API_URL + "api/Gallery?LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty="
                    + Utils.encode(getGalleryRow(context, pageNo) > 0);
        }
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            if (Utils.getStatus(Utils.GALLERY_REFRESH, null)) {
                url = Utils.API_URL + "api/Gallery?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                        + Utils.encode(pageNo) + "LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty="
                        + Utils.encode(getGalleryRow(context, pageNo) > 0);
            } else {
                url = Utils.API_URL + "api/Gallery?LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty="
                        + Utils.encode(getGalleryRow(context, pageNo) > 0);
            }
        }
        final String finalUrl = url;
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<GalleryChecking> mGsonRequest = new GsonRequest<GalleryChecking>(
                Request.Method.GET,
                finalUrl,
                GalleryChecking.class,
                headers,
                responseAlbumListener(context, pageNo, txtError, TAG),
                errorAlbumListener(context, txtError)) {
            /*@Override
            public byte[] getBody() {
                String httpPostBody = "";
                if (Utils.getStatus(Utils.GALLERY_REFRESH, GalleryActivity.this)) {
                    httpPostBody = "employeeId=" + Utils.getProfile(GalleryActivity.this).id + "&NoPerPage=" + Utils.PAGING_NUMBER + "&PageNo=" + String.valueOf(pageNo);
                } else {
                    httpPostBody = "employeeId=" + Utils.getProfile(GalleryActivity.this).id;
                }

                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }*/
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<GalleryChecking> responseAlbumListener(final Context context, final int pageNo, final TextView txtError, final String TAG) {
        return new Response.Listener<GalleryChecking>() {
            @Override
            public void onResponse(GalleryChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getGallerys().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getAlbumLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(GalleryCheckingProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(GalleryProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(GalleryCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertGallery(response, context, pageNo, TAG);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(GalleryCheckingProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(GalleryProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(GalleryCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertGallery(response, context, pageNo, TAG);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(GalleryCheckingProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(GalleryProvider.CONTENT_URI, null, null);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(GalleryCheckingProvider.CONTENT_URI, null, null);
                                }
                                insertGallery(response, context, pageNo, TAG);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(GalleryCheckingProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(GalleryProvider.CONTENT_URI, null, null);
                                }
                            }
                        }
                    }

                }
            }
        };
    }

    public static boolean isExistAlbum(Context context, Object albumId) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(GalleryProvider.CONTENT_URI, null,
                    DBHelper.ALBUM_COLUMN_ALBUM_ID + "=?", new String[]{String.valueOf(albumId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void insertGallery(GalleryChecking result, Context context, int pageNo, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesGalleryChecking = new ContentValues();
            contentValuesGalleryChecking.put(DBHelper.ALBUM_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesGalleryChecking.put(DBHelper.ALBUM_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesGalleryChecking.put(DBHelper.ALBUM_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesGalleryChecking.put(DBHelper.ALBUM_CHECKING_COLUMN_LEVEL, pageNo);
            context.getContentResolver().insert(GalleryCheckingProvider.CONTENT_URI, contentValuesGalleryChecking);
            ContentValues[] contentValueses = new ContentValues[result.getGallerys().size()];
            for (int i = 0; i < result.getGallerys().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.ALBUM_COLUMN_ALBUM_ID, result.getGallerys().get(i).getId());
                contentValues.put(DBHelper.ALBUM_COLUMN_TITLE, result.getGallerys().get(i).getTitle());
                contentValues.put(DBHelper.ALBUM_COLUMN_LEVEL, pageNo);
                contentValues.put(DBHelper.ALBUM_COLUMN_TOTAL_IMAGE, result.getGallerys().get(i).getTotalImage());
                contentValues.put(DBHelper.ALBUM_COLUMN_ALBUM_COVER, result.getGallerys().get(i).getAlbumCover());
                contentValues.put(DBHelper.ALBUM_COLUMN_CREATED_DATE, result.getGallerys().get(i).getCreatedDate());
                contentValues.put(DBHelper.ALBUM_COLUMN_UPDATED_DATE, result.getGallerys().get(i).getUpdatedDate());
                contentValues.put(DBHelper.ALBUM_COLUMN_NEW_CREATE, String.valueOf(result.getGallerys().get(i).getNewCreate()));
                if (isExistAlbum(context, result.getGallerys().get(i).getId())) {
                    context.getContentResolver().delete(GalleryProvider.CONTENT_URI, DBHelper.ALBUM_COLUMN_ALBUM_ID + "=?",
                            new String[]{String.valueOf(result.getGallerys().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (Object ob : contentValueses) {
                if (ob != null) {
                    isEmpty = false;
                }
            }
            long rowsInserted = 0;
            if (!isEmpty) {
                rowsInserted = context.getContentResolver().bulkInsert(GalleryProvider.CONTENT_URI, contentValueses);
                Log.i(TAG, String.valueOf(rowsInserted));
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    public static Response.ErrorListener errorAlbumListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

    public static Date getAlbumLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.ALBUM_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(GalleryCheckingProvider.CONTENT_URI,
                    projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.ALBUM_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static int getGalleryRow(Context context, int pageNo) {
        int totalRow = 0;
        String[] projection = {DBHelper.ALBUM_COLUMN_ID};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(GalleryProvider.CONTENT_URI, projection,
                    DBHelper.ALBUM_COLUMN_LEVEL + "=?", new String[]{String.valueOf(pageNo)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
                cursor.close();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }
    //endregion

    //region Image
    public static int getImageRow(Context context, int pageNo) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ImageProvider.CONTENT_URI,
                    null, DBHelper.IMAGE_COLUMN_LEVEL + "=?", new String[]
                            {String.valueOf(String.valueOf(pageNo))}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getImageLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.IMAGE_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ImageCheckingProvider.CONTENT_URI, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.IMAGE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static void getImageGson(Context context, final int pageNo, int id, String TAG, final TextView txtError) {
        final String token = Utils.getToken(context);
        final Date lastUpdateDate = getImageLastUpdate(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Gallery" + "/" + Utils.encode(id) +
                "?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" +
                Utils.encode(pageNo) + "&LastUpdated=" + Utils.encode(lastUpdate) +
                "&ReturnEmpty=" + Utils.encode(getImageRow(context, pageNo) == 0) + "";
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Gallery" + "/" + Utils.encode(id) +
                    "?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" +
                    Utils.encode(pageNo) + "&LastUpdated=" + Utils.encode(lastUpdate) +
                    "&ReturnEmpty=" + Utils.encode(getImageRow(context, pageNo) > 0) + "";
        }
        final String finalUrl = url;
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<ImageChecking> mGsonRequest = new GsonRequest<ImageChecking>(
                Request.Method.GET,
                finalUrl,
                ImageChecking.class,
                headers,
                responseImageListener(context, pageNo, txtError, id, TAG),
                errorImageListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<ImageChecking> responseImageListener(final Context context, final int pageNo, final TextView txtError, final int id, final String TAG) {
        return new Response.Listener<ImageChecking>() {
            @Override
            public void onResponse(ImageChecking response) {
                Log.i(TAG, "Testing" + response.toString());
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else if (response.getImages().size() > 0) {
                        Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                        Date date2 = getImageLastUpdate(context);
                        if (date2 != null) {
                            if (date2.before(date1)) {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(ImageCheckingProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(ImageProvider.CONTENT_URI, null, null);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(ImageCheckingProvider.CONTENT_URI, null, null);
                                }
                                insertImage(response, context, pageNo, id, TAG);
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(ImageCheckingProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(ImageProvider.CONTENT_URI, null, null);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(ImageCheckingProvider.CONTENT_URI, null, null);
                                }
                                insertImage(response, context, pageNo, id, TAG);
                            }
                        } else {
                            if (pageNo == 1) {
                                context.getContentResolver().delete(ImageCheckingProvider.CONTENT_URI, null, null);
                                context.getContentResolver().delete(ImageProvider.CONTENT_URI, null, null);
                            } else if (pageNo > 1) {
                                context.getContentResolver().delete(ImageCheckingProvider.CONTENT_URI, null, null);
                            }
                            insertImage(response, context, pageNo, id, TAG);
                        }
                    } else {
                        if (pageNo == 1) {
                            if (response.getNoOfRecords() == 0) {
                                context.getContentResolver().delete(ImageCheckingProvider.CONTENT_URI, null, null);
                                context.getContentResolver().delete(ImageProvider.CONTENT_URI, null, null);
                            }
                        }
                    }
                }
            }
        };
    }

    public static Response.ErrorListener errorImageListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }

    public static void insertImage(ImageChecking result, Context context, int pageNo, int id, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" + context.getResources().getString(R.string.gallery));
            ContentValues contentValuesImageChecking = new ContentValues();
            contentValuesImageChecking.put(DBHelper.IMAGE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesImageChecking.put(DBHelper.IMAGE_CHECKING_COLUMN_LEVEL, pageNo);
            contentValuesImageChecking.put(DBHelper.IMAGE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesImageChecking.put(DBHelper.IMAGE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            context.getContentResolver().insert(ImageCheckingProvider.CONTENT_URI, contentValuesImageChecking);
            ContentValues[] contentValueses = new ContentValues[result.getImages().size()];
            for (int i = 0; i < result.getImages().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.IMAGE_COLUMN_ALBUM_ID, id);
                contentValues.put(DBHelper.IMAGE_COLUMN_IMAGE_ID, result.getImages().get(i).getId());
                contentValues.put(DBHelper.IMAGE_COLUMN_IMAGE_SMALL, result.getImages().get(i).getPhotoSmall());
                contentValues.put(DBHelper.IMAGE_COLUMN_LEVEL, pageNo);
                contentValues.put(DBHelper.IMAGE_COLUMN_IS_DEFAULT, String.valueOf(result.getImages().get(i).getIsDefault()));
                contentValues.put(DBHelper.IMAGE_COLUMN_IMAGE_LARGE, result.getImages().get(i).getPhotoLarge());
                contentValues.put(DBHelper.IMAGE_COLUMN_CREATED_DATE, result.getImages().get(i).getCreatedDate());
                contentValues.put(DBHelper.IMAGE_COLUMN_UPDATED_DATE, result.getImages().get(i).getUpdatedDate());
                String[] split = result.getImages().get(i).getPhotoLarge().split("/");
                contentValues.put(DBHelper.IMAGE_COLUMN_PATH, file.getAbsolutePath() + "/" + split[split.length - 1]);
                context.getContentResolver().delete(ImageProvider.CONTENT_URI, DBHelper.IMAGE_COLUMN_IMAGE_ID + "=?",
                        new String[]{String.valueOf(result.getImages().get(i).getId())});
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (Object ob : contentValueses) {
                if (ob != null) {
                    isEmpty = false;
                }
            }
            long rowsInserted = 0;
            if (!isEmpty) {
                rowsInserted = context.getContentResolver().bulkInsert(ImageProvider.CONTENT_URI, contentValueses);
                Log.i(TAG, String.valueOf(rowsInserted));
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    public static boolean isImageExists(Context context, Object imageId) {
        boolean isExist = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ImageProvider.CONTENT_URI, null, DBHelper.IMAGE_COLUMN_IMAGE_ID + "=?", new String[]{String.valueOf(imageId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExist = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExist;
    }
    //endregion

    //region Download
    public static void getGsonDownload(final String id, final Context context, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseListenerDownload(TAG),
                errorListenerDownload(TAG)) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" + Utils.getProfile(context).getId() +
                        ",\"section\":\"image\",\"action\":\"download\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return Utils.encode(httpPostBody).getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerDownload(final String TAG) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Success send download status");
            }
        };
    }

    public static Response.ErrorListener errorListenerDownload(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }

    //region Gallery read
    public static void getGsonReading(final Context context, final int id, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseListenerReading(context, id, TAG),
                errorListenerReading(TAG)) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" +
                        Utils.getProfile(context).getId() +
                        ",\"section\":\"gallery\",\"action\":\"read\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerReading(final
                                                                        Context context, final int id,
                                                                        final String TAG) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ContentValues contentValues = new ContentValues();

                if (isBold(context, id)) {
                    contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, DashboardActivity.getNumber(context, "Media Gallery"));
                    context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                            contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Media Gallery"});
                    com.applab.wcircle_pro.Dashboard.HttpHelper.getIndicator(context, TAG, null);
                }

                contentValues = new ContentValues();
                contentValues.put(DBHelper.ALBUM_COLUMN_NEW_CREATE, "false");
                context.getContentResolver().update(GalleryProvider.CONTENT_URI, contentValues,
                        DBHelper.ALBUM_COLUMN_ALBUM_ID + "=?", new String[]{String.valueOf(id)});


                Log.i(TAG, "Success Leave Id: " + id);

            }
        };
    }


    public static boolean isBold(Context context, Object id) {
        boolean isBold = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(GalleryProvider.CONTENT_URI, null, DBHelper.ALBUM_COLUMN_ALBUM_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                if (!cursor.isNull(cursor.getColumnIndex(DBHelper.ALBUM_COLUMN_NEW_CREATE))) {
                    isBold = Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.ALBUM_COLUMN_NEW_CREATE)));
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isBold;
    }

    public static Response.ErrorListener errorListenerReading(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }
    //endregion
}
