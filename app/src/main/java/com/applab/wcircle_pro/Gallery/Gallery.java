package com.applab.wcircle_pro.Gallery;

import com.google.gson.annotations.Expose;

public class Gallery {

    @Expose
    private Integer Id;
    @Expose
    private String Title;
    @Expose
    private String AlbumCover;
    @Expose
    private Integer TotalImage;
    @Expose
    private String CreatedDate;
    @Expose
    private String UpdatedDate;
    @Expose
    private Boolean NewCreate;

    public Boolean getNewCreate() {
        return NewCreate;
    }

    public void setNewCreate(Boolean newCreate) {
        NewCreate = newCreate;
    }

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param Title The Title
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * @return The AlbumCover
     */
    public String getAlbumCover() {
        return AlbumCover;
    }

    /**
     * @param AlbumCover The AlbumCover
     */
    public void setAlbumCover(String AlbumCover) {
        this.AlbumCover = AlbumCover;
    }

    /**
     * @return The TotalImage
     */
    public Integer getTotalImage() {
        return TotalImage;
    }

    /**
     * @param TotalImage The TotalImage
     */
    public void setTotalImage(Integer TotalImage) {
        this.TotalImage = TotalImage;
    }

    /**
     * @return The CreatedDate
     */
    public String getCreatedDate() {
        return CreatedDate;
    }

    /**
     * @param CreatedDate The CreatedDate
     */
    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    /**
     * @return The UpdatedDate
     */
    public String getUpdatedDate() {
        return UpdatedDate;
    }

    /**
     * @param UpdatedDate The UpdatedDate
     */
    public void setUpdatedDate(String UpdatedDate) {
        this.UpdatedDate = UpdatedDate;
    }

}