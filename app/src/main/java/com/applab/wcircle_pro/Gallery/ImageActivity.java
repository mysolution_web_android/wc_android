package com.applab.wcircle_pro.Gallery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

public class ImageActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar toolbar;
    private GridView gridView;
    private ImageGridAdapter adapter;
    private String TAG = "IMAGE";
    private Cursor cursor;
    private Bundle bundle;
    private int id;
    private String title;
    private LinearLayout btn, btnSave, btnCancel;
    private int pageNo = 1;
    private TextView txtError;
    public static String ACTION = "DOWNLOAD IMAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_blue));
        bundle = getIntent().getExtras();
        id = bundle.getInt("id");
        if (bundle != null) {
            if (bundle.getString("title") != null) {
                if (!bundle.getString("title").equals("")) {
                    title = bundle.getString("title");
                } else {
                    startActivity(new Intent(this, GalleryActivity.class));
                    finish();
                }
            } else {
                startActivity(new Intent(this, GalleryActivity.class));
                finish();
            }
        } else {
            startActivity(new Intent(this, GalleryActivity.class));
            finish();
        }
        TextView txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(title);
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        btn = (LinearLayout) findViewById(R.id.btn);
        btnSave = (LinearLayout) findViewById(R.id.btnSave);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        gridView = (GridView) findViewById(R.id.grid_view);
        adapter = new ImageGridAdapter(this, cursor, btn, btnCancel, btnSave, getSupportFragmentManager());
        gridView.setAdapter(adapter);
        txtError = (TextView) findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);
        HttpHelper.getImageGson(this, pageNo, id, TAG, txtError);
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.i(TAG, "Position: " + visibleItemCount);
                if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                    if (btn.getVisibility() == View.GONE) {
                        pageNo++;
                        HttpHelper.getImageGson(ImageActivity.this, pageNo, id, TAG, txtError);
                    }
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, GalleryActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            //NavUtils.navigateUpFromSameTask(this);
            startActivity(new Intent(this, GalleryActivity.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = ImageProvider.CONTENT_URI;
        if (id == 3) {
            return new CursorLoader(getBaseContext(), uri, null, DBHelper.IMAGE_COLUMN_ALBUM_ID + "=?",
                    new String[]{String.valueOf(this.id)}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 3 && data != null) {
            this.cursor = data;
            adapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                boolean isYes = intent.getBooleanExtra("isYes", false);
                boolean isSelectedMoreThanZero = intent.getBooleanExtra("isSelectedMoreThanZero", false);
                if (isYes) {
                    if (isSelectedMoreThanZero) {
                        adapter.selectAll(false);
                        adapter.setVisibilityCheckBox(false);
                        btn.setVisibility(View.GONE);
                    } else {
                        adapter.selectAll(false);
                        adapter.setVisibilityCheckBox(false);
                        btn.setVisibility(View.GONE);
                    }
                    adapter.setIsSelected(false);
                }
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
        getSupportLoaderManager().initLoader(3, null, this);
        if (Utils.getStatus(Utils.IMAGE_READ, this)) {
            HttpHelper.getGsonReading(this, id, TAG);
        }
    }
}
