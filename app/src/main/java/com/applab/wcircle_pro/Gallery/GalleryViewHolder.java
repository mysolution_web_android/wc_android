package com.applab.wcircle_pro.Gallery;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by admin on 11/6/2015.
 */
public class GalleryViewHolder extends RecyclerView.ViewHolder {
    TextView txtName;
    TextView txtNoPhoto;
    ImageView imgAlbum;
    ProgressBar progressBar;
    RelativeLayout rlNew;

    public GalleryViewHolder(View itemView) {
        super(itemView);
        imgAlbum = (ImageView) itemView.findViewById(R.id.imgAlbum);
        txtName = (TextView) itemView.findViewById(R.id.txtName);
        txtNoPhoto = (TextView) itemView.findViewById(R.id.txtNoPhoto);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        rlNew = (RelativeLayout) itemView.findViewById(R.id.rlNew);
    }
}
