package com.applab.wcircle_pro.Gallery;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.applab.wcircle_pro.Utils.Utils;

public class ImageSlidingAdapter extends FragmentStatePagerAdapter {

    private Activity mActivity;
    private ArrayList<String> mImagePaths;

    // constructor
    public ImageSlidingAdapter(FragmentManager fm, Activity activity, ArrayList<String> imagePaths) {
        super(fm);
        mActivity = activity;
        mImagePaths = imagePaths;
        android.os.Message msg = handler.obtainMessage();
        handler.handleMessage(msg);
    }

    @Override
    public Fragment getItem(int position) {
        return ImageSlidingListFragment.newInstance(mImagePaths.get(position));
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return this.mImagePaths.size();
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(mActivity.getBaseContext());
        }
    };
}

