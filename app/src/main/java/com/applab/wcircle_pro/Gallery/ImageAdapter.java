package com.applab.wcircle_pro.Gallery;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Login.TokenProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 5/8/2015.
 */
public class ImageAdapter extends RecyclerView.Adapter<ImageViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;
    private ArrayList<CheckBox> arrCheckBox;
    private String TAG = "IMAGE";
    private String url = Utils.API_URL + "api/Tracking";

    public ImageAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
        this.arrCheckBox = new ArrayList<>();
    }

    public void setVisibilityCheckBox(boolean isVisible) {
        if (arrCheckBox != null) {
            for (int i = 0; i < arrCheckBox.size(); i++) {
                if (isVisible) {
                    arrCheckBox.get(i).setVisibility(View.VISIBLE);
                } else {
                    arrCheckBox.get(i).setVisibility(View.GONE);
                }
            }
        }
    }

    public void selectAll(boolean isSelect) {
        if (arrCheckBox != null) {
            for (int i = 0; i < arrCheckBox.size(); i++) {
                if (isSelect) {
                    arrCheckBox.get(i).setChecked(true);
                } else {
                    arrCheckBox.get(i).setChecked(false);
                }
            }
        }
    }

    public void save(ArrayList<String> arrUrl, ArrayList<String> arrId) {
        String id = "";
        for (int i = 0; i < arrUrl.size(); i++) {
            if (id.equals("")) {
                id += arrId.get(i);
            } else {
                id += "," + arrId.get(i);
            }
            Utils.downloadFile(arrUrl.get(i), context, arrUrl.get(i), context.getString(R.string.gallery), "Gallery", arrId.get(i));
        }
        if (Utils.getStatus(Utils.IMAGE_DOWNLOAD, context)) {
            getGsonDownload(id);
        }
    }

    public void getGsonDownload(final String id) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + getToken());
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                url,
                JSONObject.class,
                headers,
                responseListenerDownload(),
                errorListenerDownload()) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" + Utils.getProfile(context).getId() +
                        ",\"section\":\"gallery\",\"action\":\"download\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<JSONObject> responseListenerDownload() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Success send download status");
            }
        };
    }

    private Response.ErrorListener errorListenerDownload() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }

    public String getToken() {
        String accessToken = "";
        Uri uri = TokenProvider.CONTENT_URI;
        String[] projection = {DBHelper.TOKEN_COLUMN_ID, DBHelper.TOKEN_COLUMN_ACCESS_TOKEN};
        Cursor cursor = context.getContentResolver().query(uri, projection,
                null, null, DBHelper.TOKEN_COLUMN_ID + " DESC Limit 1 ");
        cursor.moveToFirst();
        if (cursor != null && cursor.moveToFirst()) {
            accessToken = cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_ACCESS_TOKEN));
            cursor.close();
        }
        return accessToken;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("ImageViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_image_grid, parent, false);
        ImageViewHolder holder = new ImageViewHolder(view);
        holder.setIsRecyclable(false);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Image current = getData(cursor, position);
        ImageTag tag = new ImageTag();
        tag.id = String.valueOf(current.getId());
        tag.url = current.getPhotoLarge();
        holder.checkBox.setTag(tag);
        arrCheckBox.add(holder.checkBox);
        Log.i(TAG, "ARR: " + arrCheckBox.size());
        Glide.with(context)
                .load(current.getPhotoSmall())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new GlideDrawableImageViewTarget(holder.img) {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);
                        holder.progressBar.setVisibility(View.GONE);
                    }
                });
    }

    public Image getData(Cursor cursor, int position) {
        Image image = new Image();
        cursor.moveToPosition(position);
        image.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.IMAGE_COLUMN_IMAGE_ID)));
        image.setPhotoSmall(cursor.getString(cursor.getColumnIndex(DBHelper.IMAGE_COLUMN_IMAGE_SMALL)));
        image.setPhotoLarge(cursor.getString(cursor.getColumnIndex(DBHelper.IMAGE_COLUMN_IMAGE_LARGE)));
        return image;
    }

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            ImageAdapter.this.notifyDataSetChanged();
        }
    };
}
