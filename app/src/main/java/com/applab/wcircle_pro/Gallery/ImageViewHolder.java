package com.applab.wcircle_pro.Gallery;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 6/5/2015.
 */
public class ImageViewHolder extends RecyclerView.ViewHolder {
    ImageView img;
    ProgressBar progressBar;
    CheckBox checkBox;

    public ImageViewHolder(View itemView) {
        super(itemView);
        img = (ImageView) itemView.findViewById(R.id.img);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        checkBox = (CheckBox) itemView.findViewById(R.id.ckImage);
    }
}