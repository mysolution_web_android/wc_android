package com.applab.wcircle_pro.Gallery;

import com.google.gson.annotations.Expose;

public class Image {

    @Expose
    private Integer Id;
    @Expose
    private Boolean IsDefault;
    @Expose
    private String PhotoSmall;
    @Expose
    private String PhotoLarge;
    @Expose
    private String CreatedDate;
    @Expose
    private String UpdatedDate;
    @Expose
    private String AlbumId;

    public String getAlbumId() {
        return AlbumId;
    }

    public void setAlbumId(String albumId) {
        AlbumId = albumId;
    }

    /**
     *
     * @return
     * The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The IsDefault
     */
    public Boolean getIsDefault() {
        return IsDefault;
    }

    /**
     *
     * @param IsDefault
     * The IsDefault
     */
    public void setIsDefault(Boolean IsDefault) {
        this.IsDefault = IsDefault;
    }

    /**
     *
     * @return
     * The PhotoSmall
     */
    public String getPhotoSmall() {
        return PhotoSmall;
    }

    /**
     *
     * @param PhotoSmall
     * The PhotoSmall
     */
    public void setPhotoSmall(String PhotoSmall) {
        this.PhotoSmall = PhotoSmall;
    }

    /**
     *
     * @return
     * The PhotoLarge
     */
    public String getPhotoLarge() {
        return PhotoLarge;
    }

    /**
     *
     * @param PhotoLarge
     * The PhotoLarge
     */
    public void setPhotoLarge(String PhotoLarge) {
        this.PhotoLarge = PhotoLarge;
    }

    /**
     *
     * @return
     * The CreatedDate
     */
    public String getCreatedDate() {
        return CreatedDate;
    }

    /**
     *
     * @param CreatedDate
     * The CreatedDate
     */
    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    /**
     *
     * @return
     * The UpdatedDate
     */
    public String getUpdatedDate() {
        return UpdatedDate;
    }

    /**
     *
     * @param UpdatedDate
     * The UpdatedDate
     */
    public void setUpdatedDate(String UpdatedDate) {
        this.UpdatedDate = UpdatedDate;
    }

}