package com.applab.wcircle_pro.Profile;

/**
 * Created by user on 2/11/2015.
 */

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.MultiPartRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.HashMap;

/**
 * Created by user on 2/10/2015.
 */

public class ProfileUploadService extends IntentService implements MultiPartRequest.MultipartProgressListener {
    private String mFilePath;
    private String mContactNo;
    private int id;
    private String TAG = "EDIT PROFILE";
    private File mFile;
    public static LocalBroadcastManager mgr;

    public ProfileUploadService() {
        super("ProfileUploadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle bundle = intent.getExtras();
        mgr = LocalBroadcastManager.getInstance(this);
        mFilePath = bundle.getString("filePath");
        mContactNo = bundle.getString("contactNo");
        if (mFilePath != null) {
            mFile = new File(mFilePath);
        }

        if (mFile != null) {
            if (Utils.getUsableSpace() < new File(mFilePath).length()) {
                Utils.showError(getBaseContext(), Utils.CODE_LOCAL_SIZE_EXCEEDED, Utils.LOCAL_SIZE_EXCEED);
            } else {
                putProfileImage(mFile, mContactNo);
            }
        } else {
            putProfileImage(null, mContactNo);
        }

    }

    @Override
    public void transferred(long transferred, long progress) {
        id = 1511;
        final NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(mFile.getName())
                .setContentText("Upload in progress")
                .setSmallIcon(R.mipmap.icon);

        // Start a lengthy operation in a background thread
        mBuilder.setProgress(100, (int) progress, false);
        // Displays the progress bar for the first time.
        mNotifyManager.notify(id, mBuilder.build());
        // When the loop is finished, updates the notification
        if (100 == progress) {
            mBuilder.setContentText("Upload complete")
                    // Removes the progress bar
                    .setProgress(0, 0, false);
            mNotifyManager.notify(id, mBuilder.build());
        }
    }

    public void putProfileImage(File file, String contactNo) {
        final String token = Utils.getToken(getBaseContext());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        MultiPartRequest<JsonObject> mGsonRequest = new MultiPartRequest<JsonObject>(
                Request.Method.PUT,
                Utils.API_URL + "api/Account",
                JsonObject.class,
                headers,
                responseListener(),
                errorListener(),
                file,
                ProfileUploadService.this, contactNo) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }


    private Response.Listener<JsonObject> responseListener() {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                Toast.makeText(getBaseContext(), "Success Updated.", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ProfileActivity.ACTION);
                intent.putExtra("status", true);
                mgr.sendBroadcast(intent);
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(getBaseContext(), error);
            }
        };
    }
}
