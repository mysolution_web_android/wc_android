package com.applab.wcircle_pro.Profile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.GroupActivity;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;

import eu.janmuller.android.simplecropimage.CropImage;

public class EditProfileActivity extends AppCompatActivity {
    private LinearLayout btnSubmit, btnCancel;
    private Toolbar toolbar;
    private TextView txtToolbarTitle;
    private EditText ediMobile;
    private ImageView imgEdit, imgProfile, imgCamera;
    private static final int SELECT_PHOTO_REQUEST_CODE = 100;
    private static final int CROP_IMAGE_REQUEST_CODE = 56;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 101;
    private Uri fileUri;
    private File file;
    private RelativeLayout fadeRL;
    private ProgressBar progressBar;
    public static String ACTION = "EDIT PROFILE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_pink));
        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_edit_profile));
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        ediMobile = (EditText) findViewById(R.id.ediMobile);
        ediMobile.setText(Utils.getProfile(this).getContactNo());
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        btnSubmit = (LinearLayout) findViewById(R.id.btnSubmit);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        btnSubmit.setOnClickListener(btnSubmitOnClickListener);
        imgEdit = (ImageView) findViewById(R.id.imgEdit);
        imgCamera = (ImageView) findViewById(R.id.imgCamera);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        imgProfile.setOnClickListener(imgProfileOnClickListener);
        imgEdit.setOnClickListener(imgEditOnClickListener);
        imgCamera.setOnClickListener(imgCameraOnClickListener);
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        fadeRL.setOnClickListener(fadeRLOnClickListener);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        DisplayMetrics mDisplayMetrics = this.getResources().getDisplayMetrics();
        int mHeight = mDisplayMetrics.heightPixels;
        int mWidth = mDisplayMetrics.widthPixels;
        Glide.with(this)
                .load(Utils.getProfile(this).getProfileImage().replace("/thumb", ""))
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new SimpleTarget<Bitmap>(mHeight, mWidth) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        imgProfile.setImageBitmap(bitmap);
                    }
                });
        setVisibilityRlAndProgressBar(false);
    }

    private View.OnClickListener fadeRLOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private void setVisibilityRlAndProgressBar(boolean isVisible) {
        if (isVisible) {
            fadeRL.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            fadeRL.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener imgProfileOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getBaseContext(), ProfileImageSlidingActivity.class);
            intent.putExtra("id", String.valueOf(Utils.getProfile(EditProfileActivity.this).getId()));
            intent.putExtra("position", 0);
            if (file != null) {
                intent.putExtra("url", file);
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getBaseContext().startActivity(intent);
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibilityRlAndProgressBar(true);
            startActivity(new Intent(getBaseContext(), ProfileActivity.class));
            finish();
        }
    };

    private View.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibilityRlAndProgressBar(true);
            if (ediMobile.getText().toString().length() > 0) {
                Intent intent = new Intent(getBaseContext(), ProfileUploadService.class);
                if (file != null) {
                    if (file.length() < 3 * 1000000) {
                        String[] result = file.getName().split("\\.");
                        if (result[result.length - 1].equalsIgnoreCase("jpg") || result[result.length - 1].
                                equalsIgnoreCase("png") || result[result.length - 1].equalsIgnoreCase("jpeg")) {
                            intent.putExtra("filePath", file.getAbsolutePath());
                            intent.putExtra("contactNo", ediMobile.getText().toString());
                            startService(intent);
                            startActivity(new Intent(getBaseContext(), ProfileActivity.class));
                            finish();
                        } else {
                            setVisibilityRlAndProgressBar(false);
                            Utils.showError(EditProfileActivity.this, "", "The image must be jpg or png.");
                        }
                    } else {
                        setVisibilityRlAndProgressBar(false);
                        Utils.showError(EditProfileActivity.this, "", "The image is too large.");
                    }
                } else {
                    intent.putExtra("contactNo", ediMobile.getText().toString());
                    startService(intent);
                    startActivity(new Intent(getBaseContext(), ProfileActivity.class));
                    finish();
                }
            } else {
                setVisibilityRlAndProgressBar(false);
                Utils.showError(EditProfileActivity.this, "", "Mobile no cannot be empty.");
            }
        }
    };

    private View.OnClickListener imgCameraOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Utils.isDeviceSupportCamera(EditProfileActivity.this)) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = GroupActivity.getOutputMediaFileUri(EditProfileActivity.this, true, null);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            } else {
                Utils.showError(EditProfileActivity.this, "", "Sorry, you don't have supported camera.");
            }

        }
    };

    private View.OnClickListener imgEditOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO_REQUEST_CODE);
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap result = null;
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case SELECT_PHOTO_REQUEST_CODE:
                file = GroupActivity.previewSelectedImage(this, data.getData(), null, false, false, 0, true, null);
                GroupActivity.performCrop(Uri.fromFile(file), EditProfileActivity.this, CROP_IMAGE_REQUEST_CODE);
                break;
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                GroupActivity.performCrop(fileUri, EditProfileActivity.this, CROP_IMAGE_REQUEST_CODE);
                break;
            case CROP_IMAGE_REQUEST_CODE:
                if (data.getStringExtra(CropImage.IMAGE_PATH) == null) {
                    return;
                }
                result = BitmapFactory.decodeFile(data.getStringExtra(CropImage.IMAGE_PATH));
                file = Utils.getFile(this, result, true, null);
                file = GroupActivity.previewCapturedImage(this, Uri.parse(file.getAbsolutePath()), imgProfile, false, false, 0, true, null);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        setVisibilityRlAndProgressBar(false);
        IntentFilter iff = new IntentFilter(ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                if (intent.getBooleanExtra("isSaved", false)) {
                    file = (File) intent.getSerializableExtra("file");
                }
            }
        }
    };


    @Override
    public void onBackPressed() {
        setVisibilityRlAndProgressBar(true);
        startActivity(new Intent(this, ProfileActivity.class));
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            setVisibilityRlAndProgressBar(true);
            startActivity(new Intent(this, ProfileActivity.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
