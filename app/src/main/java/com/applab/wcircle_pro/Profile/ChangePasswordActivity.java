package com.applab.wcircle_pro.Profile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Employee.Employee;
import com.applab.wcircle_pro.ErrorDialogFragment;
import com.applab.wcircle_pro.Home.HttpHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Settings.SettingsActivity;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.HashMap;

public class ChangePasswordActivity extends AppCompatActivity {
    private LinearLayout btnSubmit, btnCancel;
    private Toolbar toolbar;
    private TextView txtToolbarTitle;
    private EditText ediCurrentPassword, ediNewPassword,
            ediConfirmNewPassword;
    private String TAG = "CHANGE_PASS";
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    private RelativeLayout fadeRL;
    private ProgressBar progressBar;
    public static String ACTION = "Change Password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_change_password));
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        ediCurrentPassword = (EditText) findViewById(R.id.ediCurrentPassword);
        ediNewPassword = (EditText) findViewById(R.id.ediNewPassword);
        ediConfirmNewPassword = (EditText) findViewById(R.id.ediConfirmNewPassword);
        pref = getApplicationContext().getSharedPreferences("OWNCLOUD PASSWORD", 0);
        editor = pref.edit();
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        fadeRL.setOnClickListener(fadeRLOnClickListener);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        btnSubmit = (LinearLayout) findViewById(R.id.btnSubmit);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibilityRlAndProgressBar(true);
                startActivity(new Intent(ChangePasswordActivity.this, SettingsActivity.class));
                finish();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postNewPassword();
            }
        });
        setVisibilityRlAndProgressBar(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
    }

    private View.OnClickListener fadeRLOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private void setVisibilityRlAndProgressBar(boolean isVisible) {
        if (isVisible) {
            fadeRL.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            fadeRL.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    private void postNewPassword() {
        if (isValid()) {
            postNewPassword(ediCurrentPassword.getText().toString(),
                    ediNewPassword.getText().toString(), ediConfirmNewPassword.getText().toString());
        }
    }

    private boolean isValid() {
        boolean valid = false;
        String password = pref.getString("own_password", null);
        String message = null;
        setVisibilityRlAndProgressBar(true);
        if (ediCurrentPassword.getText().toString().equals("")) {
            message = Utils.getDialogMessage(getBaseContext(), Utils.CODE_CURRENT_PASSWORD, Utils.CURRENT_PASSWORD);
        } else if (ediNewPassword.getText().toString().equals("")) {
            message = Utils.getDialogMessage(getBaseContext(), Utils.CODE_NEW_PASSWORD, Utils.NEW_PASSWORD);
        } else if (ediConfirmNewPassword.getText().toString().equals("")) {
            message = Utils.getDialogMessage(getBaseContext(), Utils.CODE_CONFIRM_NEW_PASSWORD, Utils.CONFIRM_NEW_PASSWORD);
        } else if (!ediNewPassword.getText().toString().equals(ediConfirmNewPassword.getText().toString())) {
            message = Utils.getDialogMessage(getBaseContext(), Utils.CODE_NEW_PASSWORD_AND_CONFIRM_PASS_NOT_MATCH, Utils.NEW_PASSWORD_AND_CONFIRM_PASS_NOT_MATCH);
        } else if (ediNewPassword.getText().toString().length() < 6) {
            message = Utils.getDialogMessage(getBaseContext(), Utils.CODE_PASSWORD_LENGTH, Utils.PASSWORD_LENGTH);
        } else {
            valid = true;
        }
        if (message != null) {
            setVisibilityRlAndProgressBar(false);
            ErrorDialogFragment leaveErrorDialogFragment = ErrorDialogFragment.newInstance(ChangePasswordActivity.this, message);
            leaveErrorDialogFragment.setCancelable(false);
            leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
        }
        return valid;
    }

    public void postNewPassword(final String currentPassword, final String newPassword, final String confirmNewPassword) {
        final String token = Utils.getToken(ChangePasswordActivity.this);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Employee> mGsonRequest = new GsonRequest<Employee>(
                Request.Method.POST,
                Utils.API_URL + "api/Account/ChangePassword",
                Employee.class,
                headers,
                responseListener(),
                errorListener()) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "OldPassword=" + currentPassword + "&NewPassword=" + newPassword + "&ConfirmPassword=" + confirmNewPassword;
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }

        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<Employee> responseListener() {
        return new Response.Listener<Employee>() {
            @Override
            public void onResponse(Employee response) {
                Utils.showError(ChangePasswordActivity.this, Utils.CODE_SUCCESS_CHANGE_PASSWORD, Utils.SUCCESS_CHANGE_PASSWORD);
                HttpHelper.setLogOut(ChangePasswordActivity.this);
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                setVisibilityRlAndProgressBar(false);
                Utils.serverHandlingError(ChangePasswordActivity.this, error);
            }
        };
    }

    @Override
    public void onBackPressed() {
        setVisibilityRlAndProgressBar(true);
        startActivity(new Intent(ChangePasswordActivity.this, SettingsActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            setVisibilityRlAndProgressBar(true);
            startActivity(new Intent(ChangePasswordActivity.this, SettingsActivity.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                if (intent.getBooleanExtra("isFail", false)) {
                    setVisibilityRlAndProgressBar(false);
                }
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, this, false);
        IntentFilter iff = new IntentFilter(ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, iff);
    }
}
