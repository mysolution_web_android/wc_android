package com.applab.wcircle_pro.Profile;

import android.database.Cursor;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;

public class Profile {

    @Expose
    private Integer Id;
    @Expose
    private Integer FavouriteId;
    @Expose
    private String ProfileImage;
    @Expose
    private Integer DefaultBranchId;
    @Expose
    private String DefaultBranchName;
    @Expose
    private String Name;
    @Expose
    private String Country;
    @Expose
    private String CountryImage;
    @Expose
    private String Gender;
    @Expose
    private String Department;
    @Expose
    private String Position;
    @Expose
    private String ContactNo;
    @Expose
    private String Email;
    @Expose
    private String AllBranch;
    @Expose
    private String ApiKey;
    @Expose
    private String LastUpdated;
    @Expose
    private String CountryName;
    @Expose
    private String EmployeeNo;
    @Expose
    private String DOB;
    @Expose
    private String OfficeNo;
    @Expose
    private String HODName;
    @Expose
    private String HODId;
    @Expose
    private String Message;
    @Expose
    private String OXUser;

    public String getOXUser() {
        return OXUser;
    }

    public void setOXUser(String OXUser) {
        this.OXUser = OXUser;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getEmployeeNo() {
        return EmployeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        EmployeeNo = employeeNo;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getOfficeNo() {
        return OfficeNo;
    }

    public void setOfficeNo(String officeNo) {
        OfficeNo = officeNo;
    }

    public String getHODName() {
        return HODName;
    }

    public void setHODName(String HODName) {
        this.HODName = HODName;
    }

    public String getHODId() {
        return HODId;
    }

    public void setHODId(String HODId) {
        this.HODId = HODId;
    }

    public String getApiKey() {
        return ApiKey;
    }

    public void setApiKey(String apiKey) {
        ApiKey = apiKey;
    }

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The FavouriteId
     */
    public Integer getFavouriteId() {
        return FavouriteId;
    }

    /**
     * @param FavouriteId The FavouriteId
     */
    public void setFavouriteId(Integer FavouriteId) {
        this.FavouriteId = FavouriteId;
    }

    /**
     * @return The ProfileImage
     */
    public String getProfileImage() {
        return ProfileImage;
    }

    /**
     * @param ProfileImage The ProfileImage
     */
    public void setProfileImage(String ProfileImage) {
        this.ProfileImage = ProfileImage;
    }

    /**
     * @return The DefaultBranchId
     */
    public Integer getDefaultBranchId() {
        return DefaultBranchId;
    }

    /**
     * @param DefaultBranchId The DefaultBranchId
     */
    public void setDefaultBranchId(Integer DefaultBranchId) {
        this.DefaultBranchId = DefaultBranchId;
    }

    /**
     * @return The DefaultBranchName
     */
    public String getDefaultBranchName() {
        return DefaultBranchName;
    }

    /**
     * @param DefaultBranchName The DefaultBranchName
     */
    public void setDefaultBranchName(String DefaultBranchName) {
        this.DefaultBranchName = DefaultBranchName;
    }

    /**
     * @return The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return The Country
     */
    public String getCountry() {
        return Country;
    }

    /**
     * @param Country The Country
     */
    public void setCountry(String Country) {
        this.Country = Country;
    }

    /**
     * @return The CountryImage
     */
    public String getCountryImage() {
        return CountryImage;
    }

    /**
     * @param CountryImage The CountryImage
     */
    public void setCountryImage(String CountryImage) {
        this.CountryImage = CountryImage;
    }

    /**
     * @return The Gender
     */
    public String getGender() {
        return Gender;
    }

    /**
     * @param Gender The Gender
     */
    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    /**
     * @return The Department
     */
    public String getDepartment() {
        return Department;
    }

    /**
     * @param Department The Department
     */
    public void setDepartment(String Department) {
        this.Department = Department;
    }

    /**
     * @return The Position
     */
    public String getPosition() {
        return Position;
    }

    /**
     * @param Position The Position
     */
    public void setPosition(String Position) {
        this.Position = Position;
    }

    /**
     * @return The ContactNo
     */
    public String getContactNo() {
        return ContactNo;
    }

    /**
     * @param ContactNo The ContactNo
     */
    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    /**
     * @return The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     * @param Email The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     * @return The AllBranch
     */
    public String getAllBranch() {
        return AllBranch;
    }

    /**
     * @param AllBranch The AllBranch
     */
    public void setAllBranch(String AllBranch) {
        this.AllBranch = AllBranch;
    }

    /**
     * @return The LastUpdated
     */
    public String getLastUpdated() {
        return LastUpdated;
    }

    /**
     * @param LastUpdated The LastUpdated
     */
    public void setLastUpdated(String LastUpdated) {
        this.LastUpdated = LastUpdated;
    }

    public static Profile getData(Cursor cursor, int position) {
        Profile profile = new Profile();
        cursor.moveToPosition(position);
        profile.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_ID)));
        profile.setName(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_NAME)));
        profile.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_ID)));
        profile.setDepartment(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_DEPARTMENT)));
        profile.setAllBranch(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_ALL_BRANCH)));
        profile.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_POSITION)));
        profile.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_CONTACT_NO)));
        profile.setEmail(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_EMAIL)));
        profile.setProfileImage(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_IMAGE)));
        profile.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_COUNTRY_IMAGE)));
        profile.setGender(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_GENDER)));
        profile.setLastUpdated(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_LAST_UPDATED)));
        profile.setMessage(null);
        profile.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_ID)));
        profile.setCountry(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_COUNTRY)));
        profile.setDefaultBranchName(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_BRANCH_NAME)));
        profile.setDefaultBranchId(cursor.getInt(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_BRANCH_ID)));
        profile.setFavouriteId(cursor.getInt(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_FAVORITE_ID)));
        profile.setCountryName(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_COUNTRY_NAME)));
        profile.setDOB(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_DOB)));
        profile.setEmployeeNo(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_EMPLOYEE_NO)));
        profile.setHODId(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_HOD_ID)));
        profile.setHODName(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_HOD_NAME)));
        profile.setOfficeNo(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_OFFICE_NO)));
        profile.setApiKey(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_API_KEY)));
        profile.setOXUser(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_OXUSER)));
        return profile;
    }

}