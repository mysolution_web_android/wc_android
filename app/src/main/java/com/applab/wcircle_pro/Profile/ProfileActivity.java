package com.applab.wcircle_pro.Profile;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Employee.Employee;
import com.applab.wcircle_pro.Notification.NotificationDialogFragment;
import com.applab.wcircle_pro.Notification.NotificationProvider;
import com.applab.wcircle_pro.Notification.Notifications;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Menu.NavigationDrawerFragment;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import java.util.ArrayList;
import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity implements SwipyRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {
    //region variable region
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private ImageView imgProfile, imgCountry;
    private Cursor cursor;
    private TextView txtName, txtDep, txtPosition, txtEmail, txtPhoneNo, txtEmployeeNo, txtHOD, txtError,txtOfficeNo,txtDOB;
    private String TAG = "PROFILE";
    private LinearLayout btnEdit, btnPassword;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private ScrollView scrollView;
    public static final String ACTION = "EDIT_PROFILE";
    private DialogFragment notificationDialogFragment;
    //endregion

    //region on create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportLoaderManager().initLoader(0, null, this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_pink));
        TextView txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_profile));
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setSelectedPosition(12);
        drawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        imgCountry = (ImageView) findViewById(R.id.imgCountry);
        txtError = (TextView) findViewById(R.id.txtError);
        txtName = (TextView) findViewById(R.id.txtName);
        txtDep = (TextView) findViewById(R.id.txtDep);
        txtPosition = (TextView) findViewById(R.id.txtPosition);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtPhoneNo = (TextView) findViewById(R.id.txtPhoneNo);
        txtEmployeeNo = (TextView) findViewById(R.id.txtEmployeeNo);
        txtHOD = (TextView) findViewById(R.id.txtHOD);
        txtOfficeNo = (TextView) findViewById(R.id.txtOffice);
        txtDOB = (TextView) findViewById(R.id.txtDOB);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollViewScrollChangedListener();
        getGson();
        btnEdit = (LinearLayout) findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(btnEditClickListener);
        btnPassword = (LinearLayout) findViewById(R.id.btnPassword);
        btnPassword.setOnClickListener(btnPasswordClickListener);
        imgProfile.setOnClickListener(imgProfileClickListener);
        toolbar.setNavigationIcon(R.mipmap.action_arrow_prev_white);
        toolbar.setNavigationOnClickListener(toolbarOnClickListener);
    }

    //endregion
    private View.OnClickListener toolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utils.clearPreviousActivity(ProfileActivity.this);
        }
    };

    private void scrollViewScrollChangedListener() {
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (!mSwipyRefreshLayout.isRefreshing()) {
                    mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                }
            }
        });
    }

    private LinearLayout.OnClickListener imgProfileClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Profile profile = Profile.getData(cursor, 0);
            Intent intent = new Intent(getBaseContext(), ProfileImageSlidingActivity.class);
            intent.putExtra("id", String.valueOf(profile.getId()));
            intent.putExtra("position", 0);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getBaseContext().startActivity(intent);
        }
    };

    private LinearLayout.OnClickListener btnEditClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(ProfileActivity.this, EditProfileActivity.class));
            finish();
        }
    };

    private LinearLayout.OnClickListener btnPasswordClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(ProfileActivity.this, ChangePasswordActivity.class));
            finish();
        }
    };

    public void getGson() {
        final String token = Utils.getToken(ProfileActivity.this);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Employee> mGsonRequest = new GsonRequest<Employee>(
                Request.Method.GET,
                Utils.API_URL + "api/Employee/" + Utils.encode(Utils.getProfile(ProfileActivity.this).getId()),
                Employee.class,
                headers,
                responseListener(),
                errorListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<Employee> responseListener() {
        return new Response.Listener<Employee>() {
            @Override
            public void onResponse(Employee response) {
                txtError.setVisibility(View.GONE);
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), ProfileActivity.this);
                    } else {
                        getBaseContext().getContentResolver().delete(ProfileProvider.CONTENT_URI, null, null);
                        insertProfile(response);
                    }
                }
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(ProfileActivity.this, error, txtError);
            }
        };
    }

    public void insertProfile(Employee result) {
        DBHelper helper = new DBHelper(getBaseContext());
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.PROFILE_COLUMN_EMAIL, result.getEmail());
            contentValues.put(DBHelper.PROFILE_COLUMN_ID, result.getId());
            contentValues.put(DBHelper.PROFILE_COLUMN_FAVORITE_ID, result.getFavouriteId());
            contentValues.put(DBHelper.PROFILE_COLUMN_IMAGE, result.getProfileImage());
            contentValues.put(DBHelper.PROFILE_COLUMN_BRANCH_ID, result.getDefaultBranchId());
            contentValues.put(DBHelper.PROFILE_COLUMN_BRANCH_NAME, result.getDefaultBranchName());
            contentValues.put(DBHelper.PROFILE_COLUMN_NAME, result.getName());
            contentValues.put(DBHelper.PROFILE_COLUMN_COUNTRY, result.getCountry());
            contentValues.put(DBHelper.PROFILE_COLUMN_COUNTRY_IMAGE, result.getCountryImage());
            contentValues.put(DBHelper.PROFILE_COLUMN_GENDER, result.getGender());
            contentValues.put(DBHelper.PROFILE_COLUMN_DEPARTMENT, result.getDepartment());
            contentValues.put(DBHelper.PROFILE_COLUMN_POSITION, result.getPosition());
            contentValues.put(DBHelper.PROFILE_COLUMN_CONTACT_NO, result.getContactNo());
            contentValues.put(DBHelper.PROFILE_COLUMN_EMAIL, result.getEmail());
            contentValues.put(DBHelper.PROFILE_COLUMN_ALL_BRANCH, result.getAllBranch());
            contentValues.put(DBHelper.PROFILE_COLUMN_LAST_UPDATED, result.getLastUpdated());
            contentValues.put(DBHelper.PROFILE_COLUMN_EMPLOYEE_NO, result.getEmployeeNo());
            contentValues.put(DBHelper.PROFILE_COLUMN_COUNTRY_NAME, result.getCountryName());
            contentValues.put(DBHelper.PROFILE_COLUMN_DOB, result.getDOB());
            contentValues.put(DBHelper.PROFILE_COLUMN_OFFICE_NO, result.getOfficeNo());
            contentValues.put(DBHelper.PROFILE_COLUMN_HOD_ID, result.getHODId());
            contentValues.put(DBHelper.PROFILE_COLUMN_HOD_NAME, result.getHODName());
            contentValues.put(DBHelper.PROFILE_COLUMN_API_KEY, Utils.getRegId(getBaseContext()));
            contentValues.put(DBHelper.PROFILE_COLUMN_OXUSER, result.getOXUser());
            getContentResolver().insert(ProfileProvider.CONTENT_URI, contentValues);
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    @Override
    public void onBackPressed() {
        Utils.clearPreviousActivity(ProfileActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (drawerFragment.mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else if (id == android.R.id.home) {
            Utils.clearPreviousActivity(ProfileActivity.this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = ProfileProvider.CONTENT_URI;
        return new CursorLoader(getBaseContext(), uri, null, null, null, DBHelper.PROFILE_COLUMN_ID + " DESC LIMIT 1 ");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.cursor = data;
        cursor.moveToFirst();
        if (cursor != null && cursor.moveToFirst()) {
            txtName.setText(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_NAME)));
            txtDep.setText(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_DEPARTMENT)));
            txtPosition.setText(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_POSITION)));
            txtEmail.setText(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_EMAIL)));
            txtPhoneNo.setText(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_CONTACT_NO)));
            txtEmployeeNo.setText(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_EMPLOYEE_NO)));
            txtHOD.setText(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_HOD_NAME)));
            txtDOB.setText(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_DOB)));
            txtOfficeNo.setText(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_OFFICE_NO)));
            Handler handler = new Handler();
            Runnable r = new Runnable() {
                public void run() {
                    Handler handler1 = new Handler();
                    Runnable r1 = new Runnable() {
                        public void run() {
                            Glide.with(ProfileActivity.this)
                                    .load(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_IMAGE)))
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .transform(new CircleTransform(ProfileActivity.this))
                                    .placeholder(R.mipmap.ic_action_person_dark)
                                    .into(imgProfile);
                            Glide.with(ProfileActivity.this)
                                    .load(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_COUNTRY_IMAGE)))
                                    .transform(new CircleTransform(ProfileActivity.this))
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(imgCountry);
                        }

                    };
                    handler1.postDelayed(r1, 10);
                }
            };
            handler.postDelayed(r, 10);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        cursor = null;
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            getGson();
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }

    private void setPopUpWindow(Cursor cursor) {
        notificationDialogFragment = new NotificationDialogFragment();
        Bundle args = new Bundle();
        ArrayList<Notifications> arrayList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Notifications notifications = new Notifications();
                notifications.setId(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_ID)));
                notifications.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_CREATE_DATE)));
                notifications.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_TITLE)));
                notifications.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_DESCRIPTION)));
                notifications.setType(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_TYPE)));
                arrayList.add(notifications);
            }
            while (cursor.moveToNext());
            cursor.close();
        }
        args.putParcelableArrayList("Notifications", arrayList);
        notificationDialogFragment.setArguments(args);
        notificationDialogFragment.setCancelable(false);
        notificationDialogFragment.show(getSupportFragmentManager(), "");
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Utils.NOTIFICATION_ACTION)) {
                Uri uri = NotificationProvider.CONTENT_URI;
                String[] projection = {
                        DBHelper.NOTIFICATION_COLUMN_TYPE,
                        DBHelper.NOTIFICATION_COLUMN_TITLE,
                        DBHelper.NOTIFICATION_COLUMN_CREATE_DATE,
                        DBHelper.NOTIFICATION_COLUMN_DESCRIPTION,
                        DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY,
                        DBHelper.NOTIFICATION_COLUMN_ID
                };
                String selection = DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY + "=?";
                String[] selectionArgs = {"0"};
                Cursor cursor = getContentResolver().query(uri, projection, selection, selectionArgs, DBHelper.NOTIFICATION_COLUMN_ID + " DESC ");
                if (notificationDialogFragment != null) {
                    if (!notificationDialogFragment.isHidden()) {
                        notificationDialogFragment.dismiss();
                    }
                }
                setPopUpWindow(cursor);
            } else if (action.equals(ACTION)) {
                if (intent.getBooleanExtra("status", false)) {
                    getGson();
                }
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(Utils.NOTIFICATION_ACTION);
        iff.addAction(ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }
}


