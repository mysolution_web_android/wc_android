package com.applab.wcircle_pro.Utils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class BootService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startid){
        Toast.makeText(this, "Service Running ...", Toast.LENGTH_LONG).show();
        Log.v("Service", "Service Running ...");

        return super.onStartCommand(intent, flags, startid);
    }
}