package com.applab.wcircle_pro.Utils;

import android.util.Log;

import com.applab.wcircle_pro.Chat.model.Msg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by admin on 25/6/2015.
 */
public class JSONParser {
    public JSONParser() {
        super();
    }

    public Msg parseMsg(JSONObject object) {
        Msg msg = new Msg();
        ArrayList<com.applab.wcircle_pro.Chat.model.array> arrArray = new ArrayList<>();
        try {
            JSONArray arr = object.getJSONArray("array");
            for (int i = 0; i < arr.length(); i++) {
                JSONObject jsonObj = arr.getJSONObject(i);
                com.applab.wcircle_pro.Chat.model.array array = new com.applab.wcircle_pro.Chat.model.array();
                array.setBody(jsonObj.getString("body"));
                array.setTo(jsonObj.getString("to"));
                array.setFrom(jsonObj.getString("from"));
                array.setId(jsonObj.getString("id"));
                array.setType(jsonObj.getString("type"));
                com.applab.wcircle_pro.Chat.model.subject subject = new com.applab.wcircle_pro.Chat.model.subject();
                JSONObject jsonSubject = jsonObj.getJSONObject("subject");
                subject.setIsimage(jsonSubject.getString("isimage"));
                subject.setFrom(jsonSubject.getString("from"));
                subject.setIsgroup(jsonSubject.getString("isgroup"));
                subject.setSenddate(jsonSubject.getString("senddate"));
                subject.setGroupname(jsonSubject.getString("groupname"));
                subject.setGroupid(jsonSubject.getString("groupid"));
                subject.setIsdelete(jsonSubject.getString("isdelete"));
                subject.setIsgroupupdate(jsonSubject.getString("isgroupupdate"));
                subject.setStatus(jsonSubject.getString("status"));
                array.setSubject(subject);
                arrArray.add(array);
            }
            if (arrArray.size() > 0) {
                msg.setArrays(arrArray);
            }
        } catch (JSONException e) {
            Log.i("parseNews", e.getMessage());
        }
        return msg;
    }
}
