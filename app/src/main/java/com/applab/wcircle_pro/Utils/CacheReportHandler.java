package com.applab.wcircle_pro.Utils;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Chat.xmpp.XMPPConn;
import com.applab.wcircle_pro.Login.LoginActivity;
import com.applab.wcircle_pro.Login.TokenProvider;
import com.applab.wcircle_pro.R;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;

public class CacheReportHandler implements Thread.UncaughtExceptionHandler {

    private Thread.UncaughtExceptionHandler defaultUEH;

    private String localPath;

    private String TAG = "CACHE_REPORT";

    private Context context;

    private final String LINE_SEPARATOR = "\n";

    /*
     * if any of the parameters is null, the respective functionality
     * will not be used
     */
    public CacheReportHandler(String localPath, Context context) {
        this.localPath = localPath;
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
        this.context = context;
    }

    public void deletePrevFiles() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/" + context.getResources().getString(R.string.folder_name) + "/" +
                context.getResources().getString(R.string.log));

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return;
            }
        } else {
            boolean isDirDeleted = Utils.deleteDir(mediaStorageDir);
            if (isDirDeleted) {
                mediaStorageDir.mkdirs();
            }
        }
    }

    public void uncaughtException(Thread t, Throwable ex) {
        String timestamp = String.valueOf(new Date().getTime());
        String filename = timestamp + ".txt";
        StringWriter stackTrace = new StringWriter();
        ex.printStackTrace(new PrintWriter(stackTrace));
        StringBuilder errorReport = new StringBuilder();
        errorReport.append("************ CAUSE OF ERROR ************\n\n");
        errorReport.append(stackTrace.toString());
        errorReport.append("\n************ DEVICE INFORMATION ***********\n");
        errorReport.append("Brand: ");
        errorReport.append(Build.BRAND);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Device: ");
        errorReport.append(Build.DEVICE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Model: ");
        errorReport.append(Build.MODEL);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Id: ");
        errorReport.append(Build.ID);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Product: ");
        errorReport.append(Build.PRODUCT);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("\n************ FIRMWARE ************\n");
        errorReport.append("SDK: ");
        errorReport.append(Build.VERSION.SDK);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Release: ");
        errorReport.append(Build.VERSION.RELEASE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Incremental: ");
        errorReport.append(Build.VERSION.INCREMENTAL);
        errorReport.append(LINE_SEPARATOR);

        if (localPath != null) {
            writeToFile(errorReport.toString(), filename);
        }

        if (!handleException(ex, errorReport.toString()) && defaultUEH != null) {
            defaultUEH.uncaughtException(t, ex);
        } else {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                Log.e(TAG, "error : ", e);
            }
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
    }

    private boolean handleException(Throwable ex, final String error) {
        if (ex == null) {
            return false;
        }
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                Toast.makeText(context, "Sorry, error occur, will now exit.", Toast.LENGTH_LONG).show();
                if (Utils.getProfile(context) != null) {
                    sendToServer(error);
                }
                Looper.loop();
            }
        }.start();
        return true;
    }

    private void writeToFile(String stacktrace, String filename) {
        deletePrevFiles();
        try {
            BufferedWriter bos = new BufferedWriter(new FileWriter(
                    localPath + "/" + filename));
            bos.write(stacktrace);
            bos.flush();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void sendToServer(final String stacktrace) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/System/Error",
                JSONObject.class,
                headers,
                responseListener(),
                errorListener()) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                String httpBody = "EmployeeId=" + Utils.getProfile(context).getId() + "&Info=" + stacktrace + "&IMEI=" + Utils.getIMEI(context);
                return httpBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<JSONObject> responseListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Success");
                AbstractXMPPConnection connection = XMPPConn.getInstance().getConn();
                if (connection != null) {
                    if (connection.isConnected()) {
                        connection.disconnect();
                    }
                }
                SharedPreferences pref = context.getSharedPreferences("OWNCLOUD PASSWORD", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();
                ContentValues contentValues = new ContentValues();
                contentValues.put("status", 0);
                context.getContentResolver().update(TokenProvider.CONTENT_URI, contentValues, null, null);

                Intent intent = new Intent(new Intent(context, LoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(context, error);
                AbstractXMPPConnection connection = XMPPConn.getInstance().getConn();
                if (connection != null) {
                    if (connection.isConnected()) {
                        connection.disconnect();
                    }
                }
                SharedPreferences pref = context.getSharedPreferences("OWNCLOUD PASSWORD", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();
                ContentValues contentValues = new ContentValues();
                contentValues.put("status", 0);
                context.getContentResolver().update(TokenProvider.CONTENT_URI, contentValues, null, null);

                Intent intent = new Intent(new Intent(context, LoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        };
    }
}