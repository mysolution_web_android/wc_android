package com.applab.wcircle_pro.Utils;

public interface ImageSlidingDataModel {
    public String buildUrl(String url,int width, int height);
}
