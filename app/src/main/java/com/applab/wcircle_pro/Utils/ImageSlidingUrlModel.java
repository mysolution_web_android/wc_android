package com.applab.wcircle_pro.Utils;

import android.content.Context;

import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImageSlidingUrlModel extends BaseGlideUrlLoader<String> {
    private static final Pattern PATTERN =
            Pattern.compile("__w-((?:-?\\d+)+)__");

    public ImageSlidingUrlModel(Context context) {
        super(context);
    }


    @Override
    protected String getUrl(String model, int width, int height) {
        Matcher m = PATTERN.matcher(model);
        int bestBucket = 0;
        if (m.find()) {
            String[] found = m.group(1).split("-");
            for (String bucketStr : found) {
                bestBucket = Integer.parseInt(bucketStr);
                if (bestBucket >= width) {
                    // the best bucket is the first immediately
                    // bigger than the requested width
                    break;
                }
            }
            if (bestBucket > 0) {
                model = m.replaceFirst("w"+bestBucket);
            }
        }
        return model;
    }
}