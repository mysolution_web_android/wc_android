package com.applab.wcircle_pro.Utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.opengl.GLES10;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Calendar.PlaceProvider;
import com.applab.wcircle_pro.Catalogue.CatalogueProvider;
import com.applab.wcircle_pro.Chat.GroupActivity;
import com.applab.wcircle_pro.Chat.db.ChatProvider;
import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.Chat.xmpp.XMPPConn;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Download.DownloadProvider;
import com.applab.wcircle_pro.Event.AttachmentProvider;
import com.applab.wcircle_pro.Gallery.ImageProvider;
import com.applab.wcircle_pro.Home.HomeActivity;
import com.applab.wcircle_pro.Home.MasterSettingProvider;
import com.applab.wcircle_pro.Home.MessageProvider;
import com.applab.wcircle_pro.Login.LoginActivity;
import com.applab.wcircle_pro.Login.TokenProvider;
import com.applab.wcircle_pro.News.NewsImageProvider;
import com.applab.wcircle_pro.Password.PasswordActivity;
import com.applab.wcircle_pro.Profile.Profile;
import com.applab.wcircle_pro.Profile.ProfileProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Register.RegisterActivity;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.joda.time.DateTimeZone;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by admin on 27/7/2015.
 */

public class Utils {
    public static String NOTIFICATION_ACTION = "CONTENT_CHANGE";
    private static String IS_OPEN_ACTION = "IS_OPEN_ACTION";
    private static String ACTIVE = "ACTIVE";
    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    /*public static final String API_URL = "http://app.wwrc.com/mintranet/";*/
    /*public static final String API_URL = "http://uat.applabweb.com/mintranet/";*/
    public static final String API_URL = "http://www.workcircle.com.my/wcapi/";
    //public static final String API_URL = "http://192.168.66.62:8088/";
    private static String REG_ID = "";
    public static int INTERNET_LOADING = 30000;
    public static final int DELAY = 2000;
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    public static int PAGING_NUMBER = 30;
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String DATE_FORMAT_PRINT = "yyyy-MM-dd'T'HH:mm:ss";
    private static DownloadManager mgr;
    private static String TAG = "UTILS";
    public static int glideSize = 25 * 10000000;
    private static final String[] sizeSuffixes = {"B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
    private static boolean isDashboad = false;
    private static final int SIZE_DEFAULT = 2048;
    private static final int SIZE_LIMIT = 4096;

    //Master Setting
    public static final String NEWS_READ = "news_read";
    public static final String NEWS_DOWNLOAD = "news_download";
    public static final String EVENT_READ = "event_read";
    public static final String EVENT_DOWNLOAD = "event_download";
    public static final String IMAGE_DOWNLOAD = "gallery_download";
    public static final String SINGLE_IMAGE_DOWNLOAD = "gallery_download";
    public static final String SINGLE_NEWS_IMAGE_DOWNLOAD = "news_download";
    public static final String DOWNLOAD_DOWNLOAD = "download_download";
    public static final String DOWNLOAD_READ = "download_read";
    public static final String CATALOGUE_DOWNLOAD = "product_download";
    public static final String CATALOGUE_READ = "product_read";
    public static final String GALLERY_REFRESH = "gallery_refresh";
    public static final String IMAGE_REFRESH = "image_refresh";
    public static final String CALENDAR_READ = "calendar_read";
    public static final String LEAVE_READ = "leave_read";
    public static final String IMAGE_READ = "gallery_read";

    //System Message (Not in Server)
    public static final String CONNECTION = "No connection, please try again.";
    public static final String INVALID_NAME = "Invalid name.";
    public static final String REMOVED_FAILED = "Remove failed.";
    public static final String REMOVED_SUCCEED = "Remove success.";
    public static final String CONNECTION_TIMEOUT = "Sorry, connection timeout";
    public static final String FIRST_NAME_FIELD = "First name is required.";
    public static final String LAST_NAME_FIELD = "Last name is required.";
    public static final String EMAIL_FIELD = "Email is required.";
    public static final String PASSWORD_FIELD = "Password is required.";
    public static final String CONFIRM_PASSWORD_FIELD = "Confirm password is required.";
    public static final String EMAIL_VALID = "Invalid email format.";
    public static final String PASSWORD_LENGTH = "The length of the password must more than 6 letters";
    public static final String CONFIRM_PASSWORD_LENGTH = "The length of the confirm password must more than 6 letters";
    public static final String PASSWORD_MATCH = "Password and confirm password are not match";
    public static final String LOADING = "Loading...";
    public static final String TITLE_FIELD = "Title is required";
    public static final String LOCATION_FIELD = "Location is required";
    public static final String START_DATE_FIELD = "Start date is required";
    public static final String END_DATE_FIELD = "End date is required";
    public static final String DESCRIPTION_FIELD = "Description is required";
    public static final String DOWNLOAD_ALERT = "Do you want to download this file? The file will be saved at ";
    public static final String RESET_PASSWORD = "We have sent you the Reset Password Link , Please check your email";
    public static final String AUTHENTICATION_ERROR = "You are not allowed to login difference account. Please try again!";
    public static final String CANCEL_LEAVE = "Do you want to cancel this leave?";
    public static final String CANCEL_CALENDAR = "Delete calendar with Attendees?";
    public static final String CHOOSE_CLIENT = "Choose an email client :";
    public static final String DELETE_SINGLE_RERENT = "Do you want to delete this item?";
    public static final String DELETE_MULTIPLE_RERENT = "Do you want to delete this item?";
    public static final String LOCAL_SIZE_EXCEED = "The file is exceeded the local storage size.";
    public static final String NO_RESULT = "No Result";
    public static final String QUOTA_EXCEED = "The file size is exceeded the server storage size.";
    public static final String UPLOAD_FAILED = "Unable to upload, please try again.";
    public static final String UNKNOWN_MY_DRIVE_ACCOUNT = "Unable to connect my drive, please contact the administrator.";
    public static final String REASON_FIELD = "Reason field is required.";
    public static final String NO_OF_DAYS_FIELD = "No of days field is required.";
    public static final String FROM_DATE_FIELD = "From date field is required.";
    public static final String TO_DATE_FIELD = "To date field is required.";
    public static final String NUMERIC_NO_OF_DAYS_FIELD = "Only numeric number is allowed in no of days field.";
    public static final String PREV_TO_DATE_FIELD = "To date field should be later than ";
    public static final String UNKNOWN_MESSENGER_ACCOUNT = "Messenger login error, please contact administrator.";
    public static final String CURRENT_PASSWORD = "Current password is required.";
    public static final String NEW_PASSWORD = "New password is required.";
    public static final String CONFIRM_NEW_PASSWORD = "Confirm new password is required.";
    public static final String NEW_PASSWORD_AND_CONFIRM_PASS_NOT_MATCH = "New password and confirm new password are not match.";
    public static final String PROCEED_FAILED = "Unable to proceed, please try again.";
    public static final String SEND_MESSAGE_FAILED = "Unable to send message, please try again.";
    public static final String WRONG_CURRENT_PASSWORD = "The current password is wrong, please try again.";
    public static final String SUCCESS_CHANGE_PASSWORD = "The password is successfully changed.";
    public static final String REMINDER_AFTER_CALENDAR_DATE = "The reminder field cannot be selected after the ";
    public static final String START_TIME_FIELD = "The start time field is required.";
    public static final String END_TIME_FIELD = "The end time field is required.";
    public static final String END_AFTER_START_DATE = "End date should be later than ";
    public static final String CREATE_FOLDER_EXISTS = "The folder is exists, please try again.";
    public static final String NO_SPACE = "Not allow to have spaces for the group name.";
    public static final String ROOM_EXISTS = "Room Name Exists. Please try other name.";
    public static final String ROOM_CLOSED = "You're no longer a member to this group.";
    public static final String EXIT = "Do you want to exit?";
    public static final String GROUP_NAME = "Group name is required.";
    public static final String ACCESS_MESSENGER = "Unable to access messenger server.";
    public static final String REMARKS_FIELD = "Remarks field is required";
    public static final String UNKNOWN_DRIVE_ACCOUNT = "Login error, please contact administrator.";
    public static final String FOLDER_NAME_FIELD = "Folder name is required.";
    public static final String FILE_NAME_FIELD = "File name is required.";
    public static final String BACKUP = "Do you want to upload this file to my drive?";
    public static final String NO_MESSAGE = "You don't have chat message.";
    public static final String NO_BACKUP = "You don't have back up file.";

    //System Message (In Server)
    public static final String CODE_CONNECTION = "101";
    public static final String CODE_CONNECTION_TIMEOUT = "122";
    public static final String CODE_EMAIL_FIELD = "105";
    public static final String CODE_FIRST_NAME_FIELD = "103";
    public static final String CODE_LAST_NAME_FIELD = "103";
    public static final String CODE_PASSWORD_FIELD = "106";
    public static final String CODE_CONFIRM_PASSWORD_FIELD = "107";
    public static final String CODE_EMAIL_VALID = "108";
    public static final String CODE_PASSWORD_LENGTH = "109";
    public static final String CODE_CONFIRM_PASSWORD_LENGTH = "110";
    public static final String CODE_PASSWORD_MATCH = "111";
    public static final String CODE_LOADING = "115";
    public static final String CODE_TITLE_FIELD = "117";
    public static final String CODE_LOCATION_FIELD = "118";
    public static final String CODE_START_DATE_FIELD = "119";
    public static final String CODE_END_DATE_FIELD = "120";
    public static final String CODE_DESCRIPTION_FIELD = "121";
    public static final String CODE_CHOOSE_CLIENT = "123";
    public static final String CODE_LOCAL_SIZE_EXCEEDED = "124";
    public static final String CODE_NO_RESULT = "125";
    public static final String CODE_REMOVE_SUCCEED = "126";
    public static final String CODE_REMOVE_FAILED = "127";
    public static final String CODE_QUOTA_EXCEED = "128";
    public static final String CODE_UPLOAD_FAILED = "129";
    public static final String CODE_AUTHENTICATION_ERROR = "130";
    public static final String CODE_RESET_PASSWORD = "131";
    public static final String CODE_CANCEL_LEAVE = "132";
    public static final String CODE_UNKNOWN_MY_DRIVE_ACCOUNT = "133";
    public static final String CODE_REASON_FIELD = "134";
    public static final String CODE_NO_OF_DAYS_FIELD = "135";
    public static final String CODE_FROM_DATE_FIELD = "136";
    public static final String CODE_TO_DATE_FIELD = "137";
    public static final String CODE_NUMERIC_NO_OF_DAYS_FIELD = "138";
    public static final String CODE_PREV_TO_DATE_FIELD = "139";
    public static final String CODE_UNKNOWN_MESSENGER_ACCOUNT = "140";
    public static final String CODE_CURRENT_PASSWORD = "141";
    public static final String CODE_NEW_PASSWORD = "142";
    public static final String CODE_CONFIRM_NEW_PASSWORD = "143";
    public static final String CODE_NEW_PASSWORD_AND_CONFIRM_PASS_NOT_MATCH = "144";
    public static final String CODE_PROCEED_FAILED = "145";
    public static final String CODE_SEND_MESSAGE_FAILED = "146";
    public static final String CODE_WRONG_CURRENT_PASSWORD = "147";
    public static final String CODE_SUCCESS_CHANGE_PASSWORD = "148";
    public static final String CODE_REMINDER_AFTER_CALENDAR_DATE = "149";
    public static final String CODE_START_TIME_FIELD = "150";
    public static final String CODE_END_TIME_FIELD = "151";
    public static final String CODE_END_AFTER_START_DATE = "152";
    public static final String CODE_CREATE_FOLDER_EXISTS = "153";
    public static final String CODE_NO_SPACE = "154";
    public static final String CODE_ROOM_EXISTS = "155";
    public static final String CODE_EXIT = "156";
    public static final String CODE_ROOM_CLOSED = "157";
    public static final String CODE_GROUP_NAME = "158";
    public static final String CODE_ACCESS_MESSENGER = "159";
    public static final String CODE_REMARKS_FIELD = "160";
    public static final String CODE_UNKNOWN_DRIVE_ACCOUNT = "161";
    public static final String CODE_FOLDER_NAME_FIELD = "162";
    public static final String CODE_FILE_NAME_FIELD = "163";
    public static final String CODE_INVALID_NAME = "164";
    public static final String CODE_BACKUP = "165";
    public static final String CODE_NO_MESSENGE = "166";
    public static final String CODE_NO_BACKUP = "167";
    public static final String CODE_CANCEL_CALENDAR = "168";

    //OwnCloud Error Message
    public static final String OWNCLOUD_TIMEOUT = "The connection is time out, please try again.";

    public static boolean getIsDashboad() {
        return isDashboad;
    }

    public static void setIsDashboad(boolean isDashboad) {
        Utils.isDashboad = isDashboad;
    }

    public static DownloadManager getMgr() {
        return mgr;
    }

    public static void setMgr(DownloadManager mgr) {
        Utils.mgr = mgr;
    }

    public static String convertToString(ArrayList<String> list) {
        StringBuilder sb = new StringBuilder();
        String delim = "";
        for (String s : list) {
            sb.append(delim);
            sb.append(s);
            delim = ",";
        }
        return sb.toString();
    }

    public static ArrayList<String> convertToArray(String string) {

        ArrayList<String> list = new ArrayList<String>(Arrays.asList(string.split(",")));
        return list;
    }

    public static boolean isMatch(ArrayList<String> args1, String args2) {
        boolean isMatch = false;
        for (int i = 0; i < args1.size(); i++) {
            if (args2.equals(args1.get(i))) {
                isMatch = true;
            }
        }
        return isMatch;
    }

    public void downloadImagesToSdCard(String downloadUrl, String imageName, Context context) {
        try

        {
            URL url = new URL("www.xxx.com" + downloadUrl);
                /* making a directory in sdcard */
            //  String sdCard=Environment.getExternalStorageDirectory().toString();
            ContextWrapper cw = new ContextWrapper(context);
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("files", Context.MODE_PRIVATE);

            File myDir = new File(directory, "folder");

                /*  if specified not exist create new */
            if (!myDir.exists()) {
                myDir.mkdir();
                Log.v("", "inside mkdir");
            }

                /* checks the file and if it already exist delete */
            String fname = imageName;
            File file = new File(myDir, fname);
            Log.d("file===========path", "" + file);
            if (file.exists())
                file.delete();

                /* Open a connection */
            URLConnection ucon = url.openConnection();
            InputStream inputStream = null;
            HttpURLConnection httpConn = (HttpURLConnection) ucon;
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            inputStream = httpConn.getInputStream();
                /*if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK)
                {
                    inputStream = httpConn.getInputStream();
                }*/

            FileOutputStream fos = new FileOutputStream(file);
            int totalSize = httpConn.getContentLength();
            int downloadedSize = 0;
            byte[] buffer = new byte[1024];
            int bufferLength = 0;
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fos.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize);
            }

            fos.close();
            Log.d("test", "Image Saved in sdcard..");
        } catch (IOException io) {
            io.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Boolean isFileExist(String filepath) {
        try {
            File file = new File(filepath);
            return file.exists();
        } catch (Exception ex) {
            ex.getStackTrace();
        }
        return false;
    }

    public static boolean isPlaceExits(Context context, String place) {
        Uri uri = PlaceProvider.CONTENT_URI;
        String[] projection = {DBHelper.PLACE_COLUMN_NAME, DBHelper.PLACE_COLUMN_ID};
        String selection = DBHelper.PLACE_COLUMN_NAME + "=?";
        String[] selecitonArgs = {place};
        Cursor cursor = context.getContentResolver().query(uri, projection, selection, selecitonArgs, DBHelper.PLACE_COLUMN_ID + " DESC LIMIT 1 ");
        if (cursor != null && cursor.moveToFirst()) {
            cursor.close();
            return true;
        }
        return false;
    }

    public static void downloadFile(String url, Context context, String fileName, String folderName, String type, Object id) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/" + context.getResources().getString(R.string.folder_name) + "/" + folderName);

        if (!direct.exists()) {
            direct.mkdirs();
        }

        if (!Utils.localFileExists(direct.getAbsolutePath() + "/" + fileName)) {
            String[] result = fileName.split("/");
            fileName = result[result.length - 1];
            if (getMgr() == null) {
                DownloadManager mgr = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                setMgr(mgr);
            }

            Uri downloadUri = Uri.parse(url);
            DownloadManager.Request request = new DownloadManager.Request(
                    downloadUri);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setAllowedNetworkTypes(
                    DownloadManager.Request.NETWORK_WIFI
                            | DownloadManager.Request.NETWORK_MOBILE)
                    .setAllowedOverRoaming(false)
                    .setDestinationInExternalPublicDir("/" + context.getResources().getString(R.string.folder_name) + "/" + folderName, fileName);
            getMgr().enqueue(request);
            Toast.makeText(context, "Downloading the file", Toast.LENGTH_SHORT).show();
            switch (type) {
                case "Download": {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.DOWNLOAD_COLUMN_PATH, direct.getAbsolutePath() + "/" + fileName);
                    long row = context.getContentResolver().update(DownloadProvider.CONTENT_URI, contentValues, DBHelper.DOWNLOAD_COLUMN_DOWNLOAD_ID + "=?", new String[]{String.valueOf(id)});
                    break;
                }
                case "Catalogue": {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.CATALOGUE_COLUMN_PATH, direct.getAbsolutePath() + "/" + fileName);
                    long row = context.getContentResolver().update(CatalogueProvider.CONTENT_URI, contentValues, DBHelper.CATALOGUE_COLUMN_CATALOGUE_ID + "=?", new String[]{String.valueOf(id)});
                    break;
                }
                case "Gallery": {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.IMAGE_COLUMN_PATH, direct.getAbsolutePath() + "/" + fileName);
                    long row = context.getContentResolver().update(ImageProvider.CONTENT_URI, contentValues, DBHelper.IMAGE_COLUMN_IMAGE_ID + "=?", new String[]{String.valueOf(id)});
                    break;
                }
                case "Attachment": {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.ATTACHMENT_COLUMN_PATH, direct.getAbsolutePath() + "/" + fileName);
                    long row = context.getContentResolver().update(AttachmentProvider.CONTENT_URI, contentValues, DBHelper.ATTACHMENT_COLUMN_ATTACHMENT_ID + "=?", new String[]{String.valueOf(id)});
                    break;
                }
                case "News": {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.NEWS_IMAGE_COLUMN_PATH, direct.getAbsolutePath() + "/" + fileName);
                    long row = context.getContentResolver().update(NewsImageProvider.CONTENT_URI, contentValues, DBHelper.NEWS_IMAGE_COLUMN_IMAGE_ID + "=?", new String[]{String.valueOf(id)});
                    break;
                }
                case "Messenger": {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_COLUMN_PATH, direct.getAbsolutePath() + "/" + fileName);
                    long row = context.getContentResolver().update(ChatProvider.CONTENT_URI, contentValues, DBHelper.CHAT_COLUMN_CHAT_ID + "=?", new String[]{String.valueOf(id)});
                    break;
                }
            }
        }
    }

    public static void checkDownloadStatus(long id, Context context) {
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(id);
        Cursor cursor = getMgr().query(query);
        if (cursor != null && cursor.moveToFirst()) {
            int columnIndex = cursor
                    .getColumnIndex(DownloadManager.COLUMN_STATUS);
            int status = cursor.getInt(columnIndex);
            int columnReason = cursor
                    .getColumnIndex(DownloadManager.COLUMN_REASON);
            int reason = cursor.getInt(columnReason);
            String title = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_TITLE));

            switch (status) {
                case DownloadManager.STATUS_FAILED:
                    String failedReason = "";
                    switch (reason) {
                        case DownloadManager.ERROR_CANNOT_RESUME:
                            failedReason = "Error cannot resume";
                            break;
                        case DownloadManager.ERROR_DEVICE_NOT_FOUND:
                            failedReason = "Error device not found";
                            break;
                        case DownloadManager.ERROR_FILE_ALREADY_EXISTS:
                            failedReason = "Error file already exists";
                            break;
                        case DownloadManager.ERROR_FILE_ERROR:
                            failedReason = "Error file error";
                            break;
                        case DownloadManager.ERROR_HTTP_DATA_ERROR:
                            failedReason = "Error HTTP data error";
                            break;
                        case DownloadManager.ERROR_INSUFFICIENT_SPACE:
                            failedReason = "Error insufficient space";
                            break;
                        case DownloadManager.ERROR_TOO_MANY_REDIRECTS:
                            failedReason = "Error too many redirects";
                            break;
                        case DownloadManager.ERROR_UNHANDLED_HTTP_CODE:
                            failedReason = "Error unhandled HTTP code";
                            break;
                        case DownloadManager.ERROR_UNKNOWN:
                            failedReason = "Error unknown";
                            break;
                    }

                    Toast.makeText(context, "Download " + title + " is failed: " + failedReason,
                            Toast.LENGTH_LONG).show();
                    break;
                case DownloadManager.STATUS_PAUSED:
                    String pausedReason = "";
                    switch (reason) {
                        case DownloadManager.PAUSED_QUEUED_FOR_WIFI:
                            pausedReason = "Paused queued for wifi";
                            break;
                        case DownloadManager.PAUSED_UNKNOWN:
                            pausedReason = "Paused unknown";
                            break;
                        case DownloadManager.PAUSED_WAITING_FOR_NETWORK:
                            pausedReason = "Paused waiting for network";
                            break;
                        case DownloadManager.PAUSED_WAITING_TO_RETRY:
                            pausedReason = "Paused waiting to retry";
                            break;
                    }
                    Toast.makeText(context, "Download " + title + " is paused: " + pausedReason,
                            Toast.LENGTH_LONG).show();
                    break;
                case DownloadManager.STATUS_PENDING:
                    Toast.makeText(context, "PENDING", Toast.LENGTH_LONG).show();
                    break;
                case DownloadManager.STATUS_RUNNING:
                    Toast.makeText(context, "RUNNING", Toast.LENGTH_LONG).show();
                    break;
                case DownloadManager.STATUS_SUCCESSFUL:
                    Toast.makeText(context, "Download " + title + " is completed", Toast.LENGTH_SHORT).show();
                    break;
            }
            cursor.close();
        }
    }

    public static void deleteFile(String message, final Context context, final String fileName) {
        String[] result = fileName.split("/");
        String file = result[result.length - 1];
        final File fileImage = new File(Environment.getExternalStorageDirectory()
                + "/" + context.getResources().getString(R.string.folder_name) + "/" + file);
        if (fileImage.exists()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setMessage(message);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    fileImage.delete();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public static File convertImageToSmall(Context context, File file, boolean isGropPicSmall, boolean isDelete, File direct) {
        if (file != null) {
            InputStream is = null;
            int sampleSize = 0;
            try {
                sampleSize = Utils.calculateBitmapSampleSize(Uri.fromFile(file), context, isGropPicSmall);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                is = context.getContentResolver().openInputStream(Uri.fromFile(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inSampleSize = sampleSize;
            Bitmap b = BitmapFactory.decodeStream(is, null, option);
            return getFile(context, b, isDelete, direct);
        } else return null;
    }

    public static File getFile(Context mContext, Bitmap result, boolean isDelete, File direct) {
        OutputStream fOut = null;
        File file = GroupActivity.getOutputMediaFile(mContext, isDelete, direct);
        assert file != null;
        try {
            fOut = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            assert result != null;
            result.compress(Bitmap.CompressFormat.JPEG, 85, fOut);// saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            try {
                assert fOut != null;
                fOut.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fOut.close(); // do not forget to close the stream
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
        return file;
    }

    public static int calculateBitmapSampleSize(Uri bitmapUri, Context context, boolean isGroupPicSmall) throws IOException {
        InputStream is = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            is = context.getContentResolver().openInputStream(bitmapUri);
            BitmapFactory.decodeStream(is, null, options); // Just get image size
        } finally {
            closeSilently(is);
        }

        int maxSize = getMaxImageSize();
        int sampleSize = 1;
        if (!isGroupPicSmall) {
            while (options.outHeight / sampleSize > maxSize || options.outWidth / sampleSize > maxSize) {
                sampleSize = sampleSize << 1;
            }
        } else {
            while (options.outHeight / sampleSize > 600 || options.outWidth / sampleSize > 600) {
                sampleSize = sampleSize << 1;
            }
        }
        return sampleSize;
    }

    public static void closeSilently(@Nullable Closeable c) {
        if (c == null) return;
        try {
            c.close();
        } catch (Throwable t) {
            // Do nothing
        }
    }

    public static int getMaxImageSize() {
        int textureLimit = getMaxTextureSize();
        if (textureLimit == 0) {
            return SIZE_DEFAULT;
        } else {
            return Math.min(textureLimit, SIZE_LIMIT);
        }
    }

    public static int getMaxTextureSize() {
        // The OpenGL texture size is the maximum size that can be drawn in an ImageView
        int[] maxSize = new int[1];
        GLES10.glGetIntegerv(GLES10.GL_MAX_TEXTURE_SIZE, maxSize, 0);
        return maxSize[0];
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targtetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        android.view.animation.Animation a = new android.view.animation.Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targtetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int) (targtetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        android.view.animation.Animation a = new android.view.animation.Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static boolean getIsActivityOpen(Context context) {
        SharedPreferences sp = context.getSharedPreferences("IS_OPEN_ACTION", 0);
        SharedPreferences.Editor ed = sp.edit();
        ed.apply();
        return sp.getBoolean(ACTIVE, false);
    }

    public static void setIsActivityOpen(boolean isActivityOpen, Context context, boolean isDashboad) {
        ShortcutBadger.removeCount(context);
        Utils.setIsDashboad(isDashboad);
        SharedPreferences sp = context.getSharedPreferences("IS_OPEN_ACTION", 0);
        SharedPreferences.Editor ed = sp.edit();
        ed.putBoolean(ACTIVE, isActivityOpen);
        ed.apply();

        sp = context.getSharedPreferences("PUSH_NOTIFICATION_NUMBER", 0);
        ed = sp.edit();
        ed.putInt("NOTIFICATION_NUMBER", 0);
        ed.apply();

        if (!isActivityOpen) {
            if (isBackgroundRunning(context)) {
                AbstractXMPPConnection connection = XMPPConn.getInstance().getConn();
                if (connection != null) {
                    if (connection.isConnected()) {
                        connection.disconnect();
                        Log.i(TAG, "Disconnected");
                    }
                }
            }
        } else {
            if (!context.getClass().getSimpleName().equals(LoginActivity.class.getSimpleName()) && !context.getClass().getSimpleName().equals(RegisterActivity.class.getSimpleName()) && !context.getClass().getSimpleName().equals(PasswordActivity.class.getSimpleName())) {
                initConnection(context);
            }
        }
    }

    public static void initConnection(Context context) {
        if (getStatus(context) == 1) {
            AbstractXMPPConnection connection = XMPPConn.getInstance().getConn();
            if (Utils.isConnectingToInternet(context)) {
                if (connection != null) {
                    if (!connection.isConnected()) {
                        connection.disconnect();
                        context.startService(new Intent(context, BackgroundXMPPService.class));
                    }
                } else {
                    context.startService(new Intent(context, BackgroundXMPPService.class));
                }
            }
        }
    }

    public static int getStatus(Context context) {
        Cursor cursor = null;
        int status = 0;
        try {
            cursor = context.getContentResolver().query(TokenProvider.CONTENT_URI, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                status = cursor.getInt(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_STATUS));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return status;
    }

    public static boolean isBackgroundRunning(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void vibrate(int duration, Context context) {
        Vibrator vibs = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibs.vibrate(duration);
    }

    public synchronized static String getAndroidId(Context context) {
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.apply();
            }
        }
        return uniqueID;
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                }
            }
        } else {
            if (connectivityManager != null) {
                //noinspection deprecation
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static String getDate() {
        Date date = new Date();
        return Utils.setCalendarDate("yyyy-MM-dd HH:mm:ss", date);
    }

    public static String getAndroidVersion() {
        return Build.VERSION.RELEASE;
    }

    public static String getModel() {
        return Build.MODEL;
    }

    public static String getIMEI(Context context) {
        String imei = null;
        if (Build.VERSION.SDK_INT >= 23) {
            if (Settings.canDrawOverlays(context)) {
                TelephonyManager telephonyManager = (TelephonyManager) context
                        .getSystemService(Context.TELEPHONY_SERVICE);
                imei = telephonyManager.getDeviceId() == null ? Settings.Secure.getString(context.getContentResolver(),
                        Settings.Secure.ANDROID_ID) : telephonyManager.getDeviceId();
                return imei;
            } else {
                imei = Settings.Secure.getString(context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                return imei;
            }
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            imei = telephonyManager.getDeviceId() == null ? Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID) : telephonyManager.getDeviceId();
            return imei;
        }
    }

    public static String getBrand() {
        return Build.BRAND;
    }

    public static String getInstalledDate(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Date installTime = new Date(packageInfo.firstInstallTime);
        return Utils.setToUTCDate("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss",
                Utils.setCalendarDate(Utils.DATE_FORMAT, installTime));
    }

    public static String getUpdatedDate(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Date updateTime = new Date(packageInfo.lastUpdateTime);
        return Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, updateTime));
    }

    public static String getAppVersion(Context context) {
        String versionName = "";
        try {
            versionName = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

    public static Integer getAppCode(Context context) {
        Integer versionCode = 0;
        try {
            versionCode = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    public static File getCacheLocation(Context context) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/" + context.getResources().getString(R.string.folder_name) + "/" + context.getResources().getString(R.string.cache_folder_name));

        if (!direct.exists()) {
            direct.mkdirs();
        }

        return direct;
    }

    public static String getRegId(Context context) {
        pref = context.getSharedPreferences("GET_REG_ID", 0);
        editor = pref.edit();
        if (REG_ID != null) {
            if (!REG_ID.equals("")) {
                return pref.getString("REG_ID", REG_ID);
            }
        }
        return REG_ID;
    }

    public static void setRegId(String regId, Context context) {
        if (regId != null) {
            if (!regId.equals("")) {
                pref = context.getSharedPreferences("GET_REG_ID", 0);
                editor = pref.edit();
                editor.clear();
                editor.apply();
                editor.putString("REG_ID", regId);
                editor.apply();
                REG_ID = regId;
            }
        }
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
            dir = context.getExternalCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            if (children.length > 0) {
                for (int i = 0; i < children.length - 1; i++) {
                    boolean success = deleteDir(new File(dir, children[i]));
                    if (!success) {
                        return false;
                    }
                }
            }
        }
        assert dir != null;
        return dir.delete();
    }


    public static boolean deleteDirAll(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            if (children.length > 0) {
                for (int i = 0; i < children.length; i++) {
                    File file = new File(dir, children[i]);
                    if (!file.getName().equals("journal.tml")) {
                        boolean success = deleteDir(new File(dir, children[i]));
                        if (!success) {
                            return false;
                        }
                    }
                }
            }
        }
        assert dir != null;
        return dir.delete();
    }

    public static long getTotalSize(Context context) {
        File internalCache = context.getCacheDir();
        File externalCache = context.getExternalCacheDir();
        long size = 0;
        File[] files = internalCache.listFiles();
        if (internalCache != null && internalCache.isDirectory()) {
            for (File f : files) {
                size = size + f.length();
            }
        }
        if (externalCache != null && externalCache.isDirectory()) {
            files = externalCache.listFiles();
            if (files != null) {
                for (File f : files) {
                    size += f.length();
                }
            }
        }
        return size;
    }

    public static long getTotalSizeGlide(Context context) {
        File cacheDir = getCacheLocation(context);
        long size = 0;
        File[] files = cacheDir.listFiles();
        if (cacheDir != null && cacheDir.isDirectory()) {
            for (File f : files) {
                size = size + f.length();
            }
        }
        return size;
    }

    public static boolean fileChecking(String fileName, Context context) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/" + context.getResources().getString(R.string.folder_name) + "/" + context.getResources().getString(R.string.attachment));
        Log.i("EVENT", direct.toString());
        if (!direct.exists()) {
            Log.i("EVENT", "Event Attachment is not created");
            return false;
        }
        String[] result = fileName.split("/");
        fileName = result[result.length - 1];

        File fileImage = new File(Environment.getExternalStorageDirectory()
                + "/" + context.getResources().getString(R.string.folder_name) + "/"
                + context.getResources().getString(R.string.attachment) + "/" + fileName);
        if (!fileImage.exists()) {
            Log.i("EVENT", "File not exist");
            return false;
        } else {
            Log.i("EVENT", "File is exist");
            return true;
        }
    }

    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static void alertDialog(String errorMessage, final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(errorMessage);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.getContentResolver().delete(TokenProvider.CONTENT_URI, null, null);
                Intent intent = new Intent(context, HomeActivity.class);
                context.startActivity(intent);
                ((Activity) context).finish();
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static String getMessage(Context context, String args) {
        String message = null;
        String[] projection = {DBHelper.MESSAGE_COLUMN_MESSAGE};
        String selection = DBHelper.MESSAGE_COLUMN_CODE + "=?";
        String[] selectionArgs = {args};
        String sorting = DBHelper.MESSAGE_COLUMN_ID + " DESC LIMIT 1";
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(MessageProvider.CONTENT_URI, projection, selection, selectionArgs, sorting);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                message = cursor.getString(cursor.getColumnIndex(DBHelper.MESSAGE_COLUMN_MESSAGE));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return message;
    }

    public static boolean getStatus(String args, Context context) {
        boolean status = false;
        Uri uri = MasterSettingProvider.CONTENT_URI;
        String[] projection = {DBHelper.MASTER_SETTING_COLUMN_STATUS};
        String selection = DBHelper.MASTER_SETTING_COLUMN_CODE + "=?";
        String[] selectionArgs = {args};
        String sorting = DBHelper.MASTER_SETTING_COLUMN_ID + " DESC LIMIT 1 ";
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, sorting);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    if (cursor.getString(cursor.getColumnIndex(DBHelper.MASTER_SETTING_COLUMN_STATUS)).equals("true")) {
                        status = true;
                    }
                }
                while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Log.i(TAG, "Status: " + String.valueOf(status));
        return status;
    }

    public static Profile getProfile(Context context) {
        Profile profile = null;
        Uri uri = ProfileProvider.CONTENT_URI;
        String sorting = DBHelper.PROFILE_COLUMN_ID + " DESC LIMIT 1 ";
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, null, null, null, sorting);
            if (cursor != null && cursor.moveToFirst()) {
                profile = new Profile();
                profile = Profile.getData(cursor, 0);
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return profile;
    }

    public static String getToken(Context context) {
        String accessToken = "";
        Uri uri = TokenProvider.CONTENT_URI;
        String[] projection = {DBHelper.TOKEN_COLUMN_ID, DBHelper.TOKEN_COLUMN_ACCESS_TOKEN};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, projection,
                    null, null, DBHelper.TOKEN_COLUMN_ID + " DESC Limit 1 ");

            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                accessToken = cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_ACCESS_TOKEN));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return accessToken;
    }

    public static void validateNetworkAndTimeOutError(Context context, VolleyError error, boolean refreshState) {
        if (refreshState) {
            if (error instanceof NoConnectionError) {
                String errorConnectionMessage = Utils.getMessage(context, Utils.CODE_CONNECTION);
                if (errorConnectionMessage != null) {
                    Toast.makeText(context, errorConnectionMessage, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, Utils.CONNECTION, Toast.LENGTH_SHORT).show();
                }
            } else if (error instanceof TimeoutError) {
                String errorTimeOutMessage = Utils.getMessage(context, Utils.CODE_CONNECTION_TIMEOUT);
                if (errorTimeOutMessage != null) {
                    Toast.makeText(context, errorTimeOutMessage, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, Utils.CONNECTION_TIMEOUT, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public static void serverHandlingError(Context context, VolleyError error, TextView txtError) {
        if (error instanceof AuthFailureError || error instanceof ServerError) {
            NetworkResponse response = error.networkResponse;
            String json = new String(response.data);
            Log.i("Error:", "Server Error: " + json);
            if (response.statusCode != 200) {
                if (json.contains("Authorization has been denied for this request.")) {
                    Utils.alertDialog("Authorization has been denied for this request.", context);
                } else {
                    txtError.setText(Utils.getDialogMessage(context, Utils.CODE_PROCEED_FAILED, Utils.PROCEED_FAILED));
                    txtError.setVisibility(View.VISIBLE);
                }
            }
        } else if (error instanceof NetworkError) {
            String errorConnectionMessage = Utils.getMessage(context, Utils.CODE_CONNECTION);
            if (errorConnectionMessage != null) {
                txtError.setText(errorConnectionMessage);
                txtError.setVisibility(View.VISIBLE);
            } else {
                txtError.setText(Utils.CONNECTION);
                txtError.setVisibility(View.VISIBLE);
            }
        } else if (error instanceof TimeoutError) {
            String errorTimeOutMessage = Utils.getMessage(context, Utils.CODE_CONNECTION_TIMEOUT);
            if (errorTimeOutMessage != null) {
                txtError.setText(errorTimeOutMessage);
            } else {
                txtError.setText(Utils.CONNECTION_TIMEOUT);
                txtError.setVisibility(View.VISIBLE);
            }
        }
    }


    public static void serverHandlingError(Context context, VolleyError error) {
        if (error instanceof AuthFailureError || error instanceof ServerError) {
            NetworkResponse response = error.networkResponse;
            String json = new String(response.data);
            Log.i("Error:", "Server Error: " + json);
            if (response.statusCode != 200) {
                if (json.contains("Authorization has been denied for this request.")) {
                    Utils.alertDialog("Authorization has been denied for this request.", context);
                } else if (json.contains("Incorrect password.")) {
                    Toast.makeText(context, "Incorrect Password", Toast.LENGTH_SHORT).show();
                } else {
                    Utils.showError(context, Utils.CODE_PROCEED_FAILED, Utils.PROCEED_FAILED);
                }
            }
        } else if (error instanceof NetworkError) {
            String errorConnectionMessage = Utils.getMessage(context, Utils.CODE_CONNECTION);
            if (errorConnectionMessage != null) {
                Toast.makeText(context, errorConnectionMessage, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, Utils.CONNECTION, Toast.LENGTH_SHORT).show();
            }
        } else if (error instanceof TimeoutError) {
            String errorTimeOutMessage = Utils.getMessage(context, Utils.CODE_CONNECTION_TIMEOUT);
            if (errorTimeOutMessage != null) {
                Toast.makeText(context, errorTimeOutMessage, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, Utils.CONNECTION_TIMEOUT, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void showNoConnection(Context context) {
        String errorMessage = Utils.getMessage(context, Utils.CODE_CONNECTION);
        if (errorMessage != null) {
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, Utils.CONNECTION, Toast.LENGTH_SHORT).show();
        }
    }

    public static void showError(Context context, String code, String error) {
        String errorMessage = Utils.getMessage(context, code);
        if (errorMessage != null) {
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
        }
    }

    public static String getDialogMessage(Context context, String code, String error) {
        String errorMessage = Utils.getMessage(context, code);
        if (errorMessage != null) {
            return errorMessage;
        } else {
            return error;
        }
    }

    public static void postRecentTrack(Context context, final String action, final String employeeId) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Employee/RecentContact",
                JSONObject.class,
                headers,
                responseListener(),
                errorListener()) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "Action=" + action + "&ContactEmployeeId=" + employeeId;
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private static Response.Listener<JSONObject> responseListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


            }
        };
    }

    private static Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }

    //convert utc to local date time
    public static String setDate(String prevFormat, String newFormat, String data) {
        String result = null;
        SimpleDateFormat format = new SimpleDateFormat(prevFormat, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat print = new SimpleDateFormat(newFormat, Locale.getDefault());
        print.setTimeZone(TimeZone.getDefault());
        if (data != null) {
            Date date = null;
            try {
                date = format.parse(data);
            } catch (ParseException e) {
                e.printStackTrace();
            } finally {
                if (date == null) {
                    try {
                        org.joda.time.DateTime dt = new org.joda.time.DateTime(data, DateTimeZone.UTC);
                        dt.toDateTime(DateTimeZone.getDefault());
                        result = dt.toString(newFormat);
                    } catch (Exception e) {
                        e.fillInStackTrace();
                    } finally {
                        if (data.contains("GMT")) {
                            org.joda.time.DateTime dt = new org.joda.time.DateTime();
                            dt.plusDays(3);
                            result = dt.toString(newFormat);
                        }
                    }
                } else {
                    String temp = print.format(date);
                    result = print.format(date);
                }
            }
        } else {
            result = null;
        }
        return result;
    }

    public static String setDate(String dateFormat, Date date) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(date);
    }

    public static Date setDate(String dateFormat, String data) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = format.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            if (date == null) {
                org.joda.time.DateTime dt = new org.joda.time.DateTime(data, DateTimeZone.UTC);
                dt.toDateTime(DateTimeZone.getDefault());
                date = Utils.setCalendarDate(Utils.DATE_FORMAT, dt.toString(Utils.DATE_FORMAT));
            }
        }
        return date;
    }

    public static String setDate(String dateFormat, long data) {
        String result = null;
        Timestamp stamp = new Timestamp(data);
        Date date = new Date(stamp.getTime());
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            result = format.format(date);
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (result == null) {
                org.joda.time.DateTime dt = new org.joda.time.DateTime(data, DateTimeZone.UTC);
                dt.toDateTime(DateTimeZone.getDefault());
                result = dt.toString(dateFormat);
            }
        }
        return result;
    }

    public static String setCalendarDate(String newFormat, long time) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(newFormat, Locale.getDefault());
        return sdf.format(d);
    }

    public static String setCalendarDate(String dateFormat, Date date) {
        String result = null;
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.getDefault());
        try {
            result = format.format(date);
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (result == null) {
                org.joda.time.DateTime dt = new org.joda.time.DateTime(date);
                result = dt.toString(dateFormat);
            }
        }
        return result;
    }

    public static String setCalendarDate(String prevFormat, String newFormat, String data) {
        String result = null;
        SimpleDateFormat format = new SimpleDateFormat(prevFormat, Locale.getDefault());
        SimpleDateFormat print = new SimpleDateFormat(newFormat, Locale.getDefault());
        Date date = null;
        try {
            date = format.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            if (data == null) {
                org.joda.time.DateTime dt = new org.joda.time.DateTime(date);
                result = dt.toString(newFormat);
            } else {
                result = print.format(date);
            }
        }
        return result;
    }

    public static Date setCalendarDate(String dateFormat, String data) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.getDefault());
        Date date = null;
        try {
            date = format.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            if (date == null) {
                org.joda.time.DateTime dt = new org.joda.time.DateTime(data);
                date = dt.toDate();
            }
        }
        return date;
    }

    public static String setToUTCDate(String prevFormat, String newFormat, String data) {
        SimpleDateFormat format = new SimpleDateFormat(prevFormat, Locale.getDefault());
        format.setTimeZone(TimeZone.getDefault());
        SimpleDateFormat print = new SimpleDateFormat(newFormat, Locale.getDefault());
        print.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = format.parse(data);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (date == null) {
                org.joda.time.DateTime dt = new org.joda.time.DateTime(data, DateTimeZone.getDefault());
                dt.toDateTime(DateTimeZone.UTC);
                date = Utils.setCalendarDate(Utils.DATE_FORMAT, dt.toString(Utils.DATE_FORMAT));
            }
        }
        String temp = print.format(date);
        return temp;
    }

    public static String bytesIntoHumanReadable(long bytes) {
        double result = bytes;
        int attachedsuff = 0;
        while (result > 1024 && attachedsuff < sizeSuffixes.length) {
            result /= 1024.;
            attachedsuff++;
        }
        result = ((int) (result * 100)) / 100.;
        return result + " " + sizeSuffixes[attachedsuff];
    }

    public static void copyDirectory(File sourceLocation, File targetLocation)
            throws IOException {

        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists() && !targetLocation.mkdirs()) {
                throw new IOException("Cannot create dir " + targetLocation.getAbsolutePath());
            }

            String[] children = sourceLocation.list();
            for (int i = 0; i < children.length; i++) {
                copyDirectory(new File(sourceLocation, children[i]),
                        new File(targetLocation, children[i]));
            }
        } else {

            // make sure the directory we plan to store the recording in exists
            File directory = targetLocation.getParentFile();
            if (directory != null && !directory.exists() && !directory.mkdirs()) {
                throw new IOException("Cannot create dir " + directory.getAbsolutePath());
            }

            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    }

    public static boolean localFileExists(String path) {
        if (path == null) {
            return false;
        }
        File direct = new File(path);
        return direct.exists();
    }


    public static boolean deleteFile(String path) {
        File direct = new File(path);
        return direct.delete();
    }

    public static boolean deleteFolderContent(File file) {
        if (file.isDirectory())
            for (File child : file.listFiles())
                deleteFolderContent(child);
        return file.delete();
    }

    public static Intent getIntent(String path, Context context) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
        File file = new File(path);
        String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
        String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        if (extension.equalsIgnoreCase("") || mimetype == null) {
            // if there is no extension or there is no definite mimetype, still try to open the file
            intent.setDataAndType(Uri.fromFile(file), "text/*");
        } else {
            intent.setDataAndType(Uri.fromFile(file), mimetype);
        }
        // custom message for the intent
        Intent.createChooser(intent, "Choose an Application:");
        return intent;
    }

    public static String getLocalFileMimiType(Context context, File file) {
        Uri uri = Uri.fromFile(file);
        ContentResolver cR = context.getContentResolver();
        return cR.getType(uri);
    }

    public static String suffixOf(String name) {
        if (name == null || name.equals("")) {
            return "";
        }
        String suffix = "";
        int index = name.lastIndexOf(".");
        if (index != -1) {
            suffix = name.substring(index + 1);
        }
        if (!suffix.equals("")) {
            suffix = "." + suffix;
        }
        return suffix;
    }

    public static long getUsableSpace() {
        File savePath = Environment.getExternalStorageDirectory();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD) {
            return savePath.getUsableSpace();

        } else {
            StatFs stats = new StatFs(savePath.getAbsolutePath());
            return stats.getAvailableBlocks() * stats.getBlockSize();
        }
    }

    public static void clearCache(Context context) {
        if (Utils.getTotalSizeGlide(context) > Utils.glideSize) {
            File dir = new File(Environment.getExternalStorageDirectory()+"/" + context.getString(R.string.folder_name)+"/"+context.getString(R.string.log));
            Utils.deleteDirAll(dir);
        }
    }

    //Disable refresh
    public static void disableSwipyRefresh(final SwipyRefreshLayout swipyRefreshLayout) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipyRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }

    public static void disableProgressBar(final ProgressBar swipyRefreshLayout) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipyRefreshLayout.setVisibility(View.GONE);
            }
        }, 4000);
    }

    public static String encode(Object obj) {
        try {
            return URLEncoder.encode(String.valueOf(obj), "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return String.valueOf(obj);
        }
    }

    public static String unixTimeToHumanReadable(long milliseconds) {
        try {
            Date date = new Date(milliseconds);
            DateFormat df = DateFormat.getDateTimeInstance();
            return df.format(date);
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
        return "few seconds ago";
    }

    public static CharSequence getRelativeDateTimeString(Context c, long time,
                                                         long minResolution, long transitionResolution,
                                                         int flags, boolean isChat, Context context) {

        CharSequence dateString = "";
        long diff = 0;
        if (isChat) {
            SharedPreferences pref;
            pref = context.getSharedPreferences("Server Date", 0);
            if (pref.getLong("server_date", 0) != 0) {
                if (pref.getBoolean("isAfter", false)) {// server date > current phone date
                    diff = pref.getLong("server_date", 0);
                } else {
                    diff = pref.getLong("server_date", 0) * -1;
                }
            }
        }

        // in Future
        if (time > System.currentTimeMillis() + diff) {
            return Utils.unixTimeToHumanReadable(time);
        }
        // < 60 seconds -> seconds ago
        else if ((System.currentTimeMillis() + diff - time) < 60 * 1000) {
            return "few seconds ago";
        } else {
            // Workaround 2.x bug (see https://github.com/owncloud/android/issues/716)
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB &&
                    (System.currentTimeMillis() - time) > 24 * 60 * 60 * 1000) {
                Date date = new Date(time);
                dateString = DateUtils.getRelativeDateTimeString(
                        c, date.getTime(), minResolution, transitionResolution, flags
                );
            } else {
                dateString = DateUtils.getRelativeDateTimeString(c, time, minResolution, transitionResolution, flags);
            }
        }

        return dateString.toString().split(",")[0];
    }

    public static String trimMessage(String json, String key) {
        String trimmedString = null;
        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }

    public static void clearPreviousActivity(Activity activity) {
        Intent i = new Intent(activity, DashboardActivity.class);
        activity.startActivity(i);
        activity.finish();
    }

    public static boolean isDeviceSupportCamera(Context context) {
        return context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA);
    }

    public static int getFileType(String type) {
        int fileType = R.mipmap.file_unknown;
        if (type.equalsIgnoreCase("doc") || type.equalsIgnoreCase("docx")) {
            fileType = R.mipmap.file_doc;
        } else if (type.equalsIgnoreCase("jpg") || type.equalsIgnoreCase("png") || type.equalsIgnoreCase("jpeg") || type.equalsIgnoreCase("gif")) {
            fileType = R.mipmap.file_img;
        } else if (type.equalsIgnoreCase("pdf")) {
            fileType = R.mipmap.file_pdf;
        } else if (type.equalsIgnoreCase("ppt") || type.equalsIgnoreCase("pptx")) {
            fileType = R.mipmap.file_ppt;
        } else if (type.equalsIgnoreCase("xls") || type.equalsIgnoreCase("xlsx")) {
            fileType = R.mipmap.file_xls;
        }
        return fileType;
    }

    public static void setupUI(final View view, final Activity activity) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(view, activity);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView, activity);
            }
        }
    }

    public static void hideSoftKeyboard(View view, Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void moveFile(File file, File dir) throws IOException {
        File newFile = new File(dir, file.getName());
        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
            outputChannel = new FileOutputStream(newFile).getChannel();
            inputChannel = new FileInputStream(file).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
            file.delete();
        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }
    }

    public static void openDocument(String path, Context context) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
        File file = new File(path);
        String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
        String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        if (extension.equalsIgnoreCase("") || mimetype == null) {
            // if there is no extension or there is no definite mimetype, still try to open the file
            intent.setDataAndType(Uri.fromFile(file), "text/*");
        } else {
            intent.setDataAndType(Uri.fromFile(file), mimetype);
        }
        // custom message for the intent
        context.startActivity(Intent.createChooser(intent, "Choose an Application:"));
    }
}
