package com.applab.wcircle_pro.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDexApplication;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Calendar.MonthFragment;
import com.applab.wcircle_pro.Calendar.ScheduleFragment;
import com.applab.wcircle_pro.Home.App;
import com.applab.wcircle_pro.Profile.ProfileProvider;
import com.applab.wcircle_pro.R;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.util.HashMap;

import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;
import com.urbanairship.push.notifications.DefaultNotificationFactory;

public class AppController extends MultiDexApplication {

    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static AppController mInstance;

    private static final String FIRST_RUN_KEY = "first_run";

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        sendCacheReport();
        UAirship.takeOff(this, new UAirship.OnReadyCallback() {
            @Override
            public void onAirshipReady(UAirship airship) {
                setChannelId(airship.getPushManager().getChannelId());
                airship.getPushManager().setUserNotificationsEnabled(true);
            }
        });
    }

    private void setChannelId(String channelId) {
        Utils.setRegId(channelId, getBaseContext());
        Log.i(TAG, "Channel registration updated. Channel Id:" + channelId + ".");
        if (Utils.getProfile(getBaseContext()) != null) {
            if (Utils.isConnectingToInternet(getBaseContext())) {
                String apiKey = UAirship.shared().getPushManager().getChannelId();
                if (apiKey != null) {
                    if (apiKey.length() > 0) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DBHelper.PROFILE_COLUMN_API_KEY, apiKey);
                        getBaseContext().getContentResolver().update(ProfileProvider.CONTENT_URI, contentValues, DBHelper.PROFILE_COLUMN_ID + "=?",
                                new String[]{String.valueOf(Utils.getProfile(getBaseContext()).getId())});
                        if(!Utils.getToken(getBaseContext()).equals("")){
                            pushRegId(apiKey, String.valueOf(Utils.getProfile(getBaseContext()).getId()), Utils.getToken(getBaseContext()), getBaseContext());
                        }
                    }
                }
            }
        }
    }

    private void pushRegId(final String regId, final String id, final String token, final Context context) {
        HashMap<String, String> headers = new HashMap<String, String>();
        Log.i(TAG, "Push Bot Api: " + regId);
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<App> mGsonRequest = new GsonRequest<App>(
                Request.Method.POST,
                Utils.API_URL + "api/Device",
                App.class,
                headers,
                responseListenerDevice(),
                errorListenerDevice()) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "EmployeeId=" + id + "&Platform=1&Token=" + regId +
                        "&OsVersion=" + Utils.getAndroidVersion() + "&UserAgent=" +
                        Utils.getBrand() + " (" + Utils.getModel() + ")" +
                        "&IMEI=" + Utils.getIMEI(context) + "&AppVersion=" +
                        Utils.getAppVersion(context) +
                        "&AppLastUpdateOn=" + Utils.getUpdatedDate(context,
                        context.getApplicationContext().getPackageName()) + "&NotificationChannel=" + AppController.this.getString(R.string.urbanairship);
                Log.i(TAG, "Http Body: " + httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<App> responseListenerDevice() {
        return new Response.Listener<App>() {
            @Override
            public void onResponse(App response) {
            }
        };
    }

    private Response.ErrorListener errorListenerDevice() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                } else if (error instanceof AuthFailureError) {
                } else if (error instanceof ServerError) {
                } else if (error instanceof NetworkError) {
                } else if (error instanceof ParseError) {
                }
                Log.i("ERROR", "Error");
            }
        };
    }

    private void sendCacheReport() {
        File file = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.folder_name) + "/" + getResources().getString(R.string.log));
        if (!file.exists()) {
            file.mkdirs();
        }
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof CacheReportHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new CacheReportHandler(
                    file.getPath(), this));
        }
    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new OkHttpStack());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        if (tag.equals(ScheduleFragment.class.getSimpleName()) || tag.equals(MonthFragment.class.getSimpleName())) {
            cancelPendingRequests(TextUtils.isEmpty(tag) ? TAG : tag);
        }
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}