package com.applab.wcircle_pro.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHelper extends SQLiteOpenHelper {

    //Database Applab
    public static final String DATABASE_NAME = "applab.db";

    //Table Calendar
    public static final String CALENDAR_TABLE_NAME = "applab_calendar";
    public static final String CALENDAR_COLUMN_ID = "id";
    public static final String CALENDAR_COLUMN_CALENDAR_ID = "calendar_id";
    public static final String CALENDAR_COLUMN_TYPE = "type";
    public static final String CALENDAR_COLUMN_ALL_DAY = "all_day";
    public static final String CALENDAR_COLUMN_TITLE = "title";
    public static final String CALENDAR_COLUMN_START_DATE = "start_date";
    public static final String CALENDAR_COLUMN_END_DATE = "end_date";
    public static final String CALENDAR_COLUMN_LOCATION = "location";
    public static final String CALENDAR_COLUMN_DESCRIPTION = "description";
    public static final String CALENDAR_COLUMN_REMINDER = "reminder";
    public static final String CALENDAR_COLUMN_STATUS = "status";
    public static final String CALENDAR_COLUMN_CREATE_BY_ID = "create_by_id";
    public static final String CALENDAR_COLUMN_CREATE_BY_NAME = "create_by_name";
    public static final String CALENDAR_COLUMN_UPDATE_BY_ID = "update_by_id";
    public static final String CALENDAR_COLUMN_UPDATE_BY_NAME = "update_by_name";
    public static final String CALENDAR_COLUMN_LEVEL = "level";
    public static final String CALENDAR_COLUMN_NEW_CREATE = "new_create";
    public static final String CALENDAR_COLUMN_SHOW_DATE = "show_date";
    public static final String CALENDAR_COLUMN_MONTH = "month";

    //Table Month
    public static final String MONTH_TABLE_NAME = "applab_month";
    public static final String MONTH_COLUMN_ID = "id";
    public static final String MONTH_COLUMN_SHOW_DATE = "show_date";
    public static final String MONTH_COLUMN_P = "p";
    public static final String MONTH_COLUMN_B = "b";
    public static final String MONTH_COLUMN_C = "c";
    public static final String MONTH_COLUMN_L = "l";
    public static final String MONTH_COLUMN_MONTH = "month";

    //Table Month Checking
    public static final String MONTH_CHECKING_TABLE_NAME = "applab_month_checking";
    public static final String MONTH_CHECKING_COLUMN_ID = "id";
    public static final String MONTH_CHECKING_COLUMN_LEVEL = "level";
    public static final String MONTH_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String MONTH_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String MONTH_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";
    public static final String MONTH_CHECKING_COLUMN_MONTH = "month";


    //Table Calendar Reminder
    public static final String CALENDAR_REMINDER_TABLE_NAME = "applab_calendar_reminder";
    public static final String CALENDAR_REMINDER_COLUMN_ID = "id";
    public static final String CALENDAR_REMINDER_CALENDAR_COLUMN_ID = "calendar_id";
    public static final String CALENDAR_REMINDER_COLUMN_DATE = "calendar_reminder";
    public static final String CALENDAR_REMINDER_COLUMN_IS_SEEN = "is_seen";

    //Table Calendar Checking
    public static final String CALENDAR_CHECKING_TABLE_NAME = "applab_calendar_checking";
    public static final String CALENDAR_CHECKING_COLUMN_ID = "id";
    public static final String CALENDAR_CHECKING_COLUMN_LEVEL = "level";
    public static final String CALENDAR_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String CALENDAR_CHECKING_COLUMN_MONTH = "month";
    public static final String CALENDAR_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String CALENDAR_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";

    //Table Attendees
    public static final String ATTENDEES_TABLE_NAME = "applab_attendees";
    public static final String ATTENDEES_COLUMN_ID = "id";
    public static final String ATTENDEES_COLUMN_LEVEL = "level";
    public static final String ATTENDEES_COLUMN_CALENDAR_ID = "calendar_id";
    public static final String ATTENDEES_COLUMN_EMPLOYEE_ID = "employee_id";
    public static final String ATTENDEES_COLUMN_NAME = "name";
    public static final String ATTENDEES_COLUMN_STATUS = "status";
    public static final String ATTENDEES_COLUMN_REMINDER = "reminder";
    public static final String ATTENDEES_COLUMN_IS_SELECT = "is_select";
    public static final String ATTENDEES_COLUMN_MONTH = "month";
    public static final String ATTENDEES_COLUMN_IS_SUBMIT = "is_submit";

    //Table Attendees Checking
    public static final String TEMP_ATTENDEES_TABLE_NAME = "applab_temp_attendees";
    public static final String TEMP_ATTENDEES_COLUMN_ID = "id";
    public static final String TEMP_ATTENDEES_COLUMN_CALENDAR_ID = "calendar_id";
    public static final String TEMP_ATTENDEES_COLUMN_EMPLOYEE_ID = "employee_id";
    public static final String TEMP_ATTENDEES_COLUMN_NAME = "name";
    public static final String TEMP_ATTENDEES_COLUMN_STATUS = "status";
    public static final String TEMP_ATTENDEES_COLUMN_REMINDER = "reminder";
    public static final String TEMP_ATTENDEES_COLUMN_IS_SELECT = "is_select";
    public static final String TEMP_ATTENDEES_COLUMN_IS_SUBMIT = "is_submit";

    //Table News
    public static final String NEWS_TABLE_NAME = "applab_news";
    public static final String NEWS_COLUMN_ID = "id";
    public static final String NEWS_COLUMN_NEWS_ID = "news_id";
    public static final String NEWS_COLUMN_TITLE = "title";
    public static final String NEWS_COLUMN_LEVEL = "level";
    public static final String NEWS_COLUMN_DESCRIPTION = "description";
    public static final String NEWS_COLUMN_AUTHOR = "author";
    public static final String NEWS_COLUMN_CREATE_DATE = "create_date";
    public static final String NEWS_COLUMN_NO_OF_IMAGE = "no_of_image";
    public static final String NEWS_COLUMN_NEW_CREATE = "new_create";

    //Table News Checking
    public static final String NEWS_CHECKING_TABLE_NAME = "applab_news_checking";
    public static final String NEWS_CHECKING_COLUMN_ID = "id";
    public static final String NEWS_CHECKING_COLUMN_LEVEL = "level";
    public static final String NEWS_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String NEWS_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String NEWS_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";

    //Table News Image
    public static final String NEWS_IMAGE_TABLE_NAME = "applab_news_image";
    public static final String NEWS_IMAGE_COLUMN_ID = "id";
    public static final String NEWS_IMAGE_COLUMN_IMAGE_ID = "image_id";
    public static final String NEWS_IMAGE_COLUMN_NEWS_ID = "news_id";
    public static final String NEWS_IMAGE_COLUMN_DESCRIPTION = "description";
    public static final String NEWS_IMAGE_COLUMN_IMAGE = "image";
    public static final String NEWS_IMAGE_COLUMN_IMAGE_THUMB = "image_thumb";
    public static final String NEWS_IMAGE_COLUMN_UPLOAD_BY = "upload_by";
    public static final String NEWS_IMAGE_COLUMN_UPLOAD_DATE = "upload_date";
    public static final String NEWS_IMAGE_COLUMN_PATH = "path";

    //Table Place
    public static final String PLACE_TABLE_NAME = "applab_place";
    public static final String PLACE_COLUMN_ID = "id";
    public static final String PLACE_COLUMN_NAME = "name";

    //Table Event
    public static final String EVENT_TABLE_NAME = "applab_event";
    public static final String EVENT_COLUMN_ID = "id";
    public static final String EVENT_COLUMN_EVENT_ID = "event_id";
    public static final String EVENT_COLUMN_LEVEL = "level";
    public static final String EVENT_COLUMN_TITLE = "title";
    public static final String EVENT_COLUMN_DESCRIPTION = "description";
    public static final String EVENT_COLUMN_LOCATION = "location";
    public static final String EVENT_COLUMN_START_DATE = "start_date";
    public static final String EVENT_COLUMN_END_DATE = "end_date";
    public static final String EVENT_COLUMN_TYPE_ID = "type_id";
    public static final String EVENT_COLUMN_TYPE = "type";
    public static final String EVENT_COLUMN_ALL_DAY = "all_day";
    public static final String EVENT_COLUMN_FEATURE_IMAGE = "feature_image";
    public static final String EVENT_COLUMN_BANNER = "banner";
    public static final String EVENT_COLUMN_CREATE_DATE = "create_date";
    public static final String EVENT_COLUMN_NEW_CREATE = "new_create";

    //Table Event Checking
    public static final String EVENT_CHECKING_TABLE_NAME = "applab_event_checking";
    public static final String EVENT_CHECKING_COLUMN_ID = "id";
    public static final String EVENT_CHECKING_COLUMN_LEVEL = "level";
    public static final String EVENT_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String EVENT_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String EVENT_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";

    //Table Dashboard
    public static final String DASHBOARD_TABLE_NAME = "applab_dashboard";
    public static final String DASHBOARD_COLUMN_ID = "id";
    public static final String DASHBOARD_COLUMN_TITLE = "title";
    public static final String DASHBOARD_COLUMN_IMG_DASHBOARD = "img_dashboard";
    public static final String DASHBOARD_COLUMN_IMG_MENU = "img_menu";
    public static final String DASHBOARD_COLUMN_NO = "no";

    //Table Favorite
    public static final String FAVORITE_CONTACT_TABLE_NAME = "applab_favorite_contact";
    public static final String FAVORITE_CONTACT_COLUMN_ID = "id";
    public static final String FAVORITE_CONTACT_COLUMN_LAST_UPDATE = "last_update";
    public static final String FAVORITE_CONTACT_COLUMN_PAGE_NO = "page_no";
    public static final String FAVORITE_CONTACT_COLUMN_NO_PER_PAGE = "no_per_page";
    public static final String FAVORITE_CONTACT_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String FAVORITE_CONTACT_COLUMN_NO_OF_PAGE = "no_of_page";

    //Table Contact
    public static final String CONTACT_TABLE_NAME = "applab_contact";
    public static final String CONTACT_COLUMN_ID = "id";
    public static final String CONTACT_COLUMN_FAVORITE_ID = "favorite_id";
    public static final String CONTACT_COLUMN_IMAGE = "image";
    public static final String CONTACT_COLUMN_BRANCH_ID = "branch_id";
    public static final String CONTACT_COLUMN_BRANCH_NAME = "branch_name";
    public static final String CONTACT_COLUMN_NAME = "name";
    public static final String CONTACT_COLUMN_COUNTRY = "country";
    public static final String CONTACT_COLUMN_COUNTRY_NAME = "country_name";
    public static final String CONTACT_COLUMN_PAGE_NO = "page_no";
    public static final String CONTACT_COLUMN_COUNTRY_IMAGE = "country_image";
    public static final String CONTACT_COLUMN_GENDER = "gender";
    public static final String CONTACT_COLUMN_OFFICE_NO = "office_no";
    public static final String CONTACT_COLUMN_DOB = "dob";
    public static final String CONTACT_COLUMN_HOD_NAME = "hod_name";
    public static final String CONTACT_COLUMN_EMPLOYEE_NO = "employee_no";
    public static final String CONTACT_COLUMN_HOD_ID = "hod_id";
    public static final String CONTACT_COLUMN_POSITION = "position";
    public static final String CONTACT_COLUMN_DEPARTMENT = "department";
    public static final String CONTACT_COLUMN_CONTACT_NO = "contact_no";
    public static final String CONTACT_COLUMN_EMAIL = "email";
    public static final String CONTACT_COLUMN_ALL_BRANCH = "all_branch";
    public static final String CONTACT_COLUMN_OXUSER = "ox_user";
    public static final String CONTACT_COLUMN_LAST_UPDATED = "last_updated";

    //Table Group
    public static final String GROUP_TABLE_NAME = "applab_group";
    public static final String GROUP_COLUMN_ID = "id";
    public static final String GROUP_COLUMN_GROUP_ID = "group_id";
    public static final String GROUP_COLUMN_NAME = "name";
    public static final String GROUP_COLUMN_IS_DELETE = "is_delete";
    public static final String GROUP_COLUMN_ICON = "icon";
    public static final String GROUP_COLUMN_CREATE_BY = "create_by";
    public static final String GROUP_COLUMN_CREATE_DATE = "create_date";
    public static final String GROUP_COLUMN_UPDATE_BY = "update_by";
    public static final String GROUP_COLUMN_UPDATE_DATE = "update_date";

    //Table Employee Checking
    public static final String EMPLOYEE_CHECKING_TABLE_NAME = "applab_employee_checking";
    public static final String EMPLOYEE_CHECKING_COLUMN_ID = "id";
    public static final String EMPLOYEE_CHECKING_COLUMN_PAGE_NO = "page_no";
    public static final String EMPLOYEE_CHECKING_COLUMN_TYPE = "type";
    public static final String EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String EMPLOYEE_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String EMPLOYEE_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";
    public static final String EMPLOYEE_CHECKING_COLUMN_NO_PER_PAGE = "no_per_page";
    public static final String EMPLOYEE_CHECKING_COLUMN_IS_RECENT = "is_recent";
    public static final String EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT = "is_department";
    public static final String EMPLOYEE_CHECKING_COLUMN_IS_BRANCH = "is_branch";

    //Table Recent Employee
    public static final String RECENT_EMPLOYEE_TABLE_NAME = "applab_recent_employee";
    public static final String RECENT_EMPLOYEE_COLUMN_ID = "id";
    public static final String RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID = "employee_id";
    public static final String RECENT_EMPLOYEE_COLUMN_ACTION_ID = "action_id";
    public static final String RECENT_EMPLOYEE_COLUMN_FAVORITE_ID = "favorite_id";
    public static final String RECENT_EMPLOYEE_COLUMN_IMAGE = "image";
    public static final String RECENT_EMPLOYEE_COLUMN_BRANCH_ID = "branch_id";
    public static final String RECENT_EMPLOYEE_COLUMN_BRANCH_NAME = "branch_name";
    public static final String RECENT_EMPLOYEE_COLUMN_NAME = "name";
    public static final String RECENT_EMPLOYEE_COLUMN_PAGE_NO = "page_no";
    public static final String RECENT_EMPLOYEE_COLUMN_DEPARTMENT = "department";
    public static final String RECENT_EMPLOYEE_COLUMN_CONTACT_NO = "contact_no";
    public static final String RECENT_EMPLOYEE_COLUMN_OFFICE_NO = "office_no";
    public static final String RECENT_EMPLOYEE_COLUMN_DOB = "dob";
    public static final String RECENT_EMPLOYEE_COLUMN_HOD_NAME = "hod_name";
    public static final String RECENT_EMPLOYEE_COLUMN_EMPLOYEE_NO = "employee_no";
    public static final String RECENT_EMPLOYEE_COLUMN_HOD_ID = "hod_id";
    public static final String RECENT_EMPLOYEE_COLUMN_GENDER = "gender";
    public static final String RECENT_EMPLOYEE_COLUMN_COUNTRY = "country";
    public static final String RECENT_EMPLOYEE_COLUMN_COUNTRY_NAME = "country_name";
    public static final String RECENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE = "country_image";
    public static final String RECENT_EMPLOYEE_COLUMN_POSITION = "position";
    public static final String RECENT_EMPLOYEE_COLUMN_EMAIL = "email";
    public static final String RECENT_EMPLOYEE_COLUMN_ALL_BRANCH = "all_branch";
    public static final String RECENT_EMPLOYEE_COLUMN_ACTION = "action";
    public static final String RECENT_EMPLOYEE_COLUMN_CREATE_DATE = "create_date";
    public static final String RECENT_EMPLOYEE_COLUMN_OXUSER = "ox_user";
    public static final String RECENT_EMPLOYEE_COLUMN_LAST_UPDATED = "last_updated";

    //Table Department Employee
    public static final String DEPARTMENT_EMPLOYEE_TABLE_NAME = "applab_department_employee";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_ID = "id";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID = "employee_id";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_FAVORITE_ID = "favorite_id";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_IMAGE = "image";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_ID = "branch_id";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_NAME = "branch_name";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_NAME = "name";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_PAGE_NO = "page_no";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_DEPARTMENT = "department";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_CONTACT_NO = "contact_no";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_OFFICE_NO = "office_no";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_DOB = "dob";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_HOD_NAME = "hod_name";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_NO = "employee_no";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_HOD_ID = "hod_id";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_GENDER = "gender";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY = "country";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_NAME = "country_name";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE = "country_image";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_POSITION = "position";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_EMAIL = "email";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_ALL_BRANCH = "all_branch";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_ACTION = "action";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_CREATE_DATE = "create_date";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_OXUSER = "ox_user";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_LAST_UPDATED = "last_updated";
    public static final String DEPARTMENT_EMPLOYEE_COLUMN_IS_SELECTED = "is_selected";

    //Table Branch Employee
    public static final String BRANCH_EMPLOYEE_TABLE_NAME = "applab_branch_employee";
    public static final String BRANCH_EMPLOYEE_COLUMN_ID = "id";
    public static final String BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID = "employee_id";
    public static final String BRANCH_EMPLOYEE_COLUMN_FAVORITE_ID = "favorite_id";
    public static final String BRANCH_EMPLOYEE_COLUMN_IMAGE = "image";
    public static final String BRANCH_EMPLOYEE_COLUMN_BRANCH_ID = "branch_id";
    public static final String BRANCH_EMPLOYEE_COLUMN_BRANCH_NAME = "branch_name";
    public static final String BRANCH_EMPLOYEE_COLUMN_NAME = "name";
    public static final String BRANCH_EMPLOYEE_COLUMN_PAGE_NO = "page_no";
    public static final String BRANCH_EMPLOYEE_COLUMN_DEPARTMENT = "department";
    public static final String BRANCH_EMPLOYEE_COLUMN_CONTACT_NO = "contact_no";
    public static final String BRANCH_EMPLOYEE_COLUMN_OFFICE_NO = "office_no";
    public static final String BRANCH_EMPLOYEE_COLUMN_DOB = "dob";
    public static final String BRANCH_EMPLOYEE_COLUMN_HOD_NAME = "hod_name";
    public static final String BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_NO = "employee_no";
    public static final String BRANCH_EMPLOYEE_COLUMN_HOD_ID = "hod_id";
    public static final String BRANCH_EMPLOYEE_COLUMN_GENDER = "gender";
    public static final String BRANCH_EMPLOYEE_COLUMN_COUNTRY = "country";
    public static final String BRANCH_EMPLOYEE_COLUMN_COUNTRY_NAME = "country_name";
    public static final String BRANCH_EMPLOYEE_COLUMN_COUNTRY_IMAGE = "country_image";
    public static final String BRANCH_EMPLOYEE_COLUMN_POSITION = "position";
    public static final String BRANCH_EMPLOYEE_COLUMN_EMAIL = "email";
    public static final String BRANCH_EMPLOYEE_COLUMN_ALL_BRANCH = "all_branch";
    public static final String BRANCH_EMPLOYEE_COLUMN_ACTION = "action";
    public static final String BRANCH_EMPLOYEE_COLUMN_CREATE_DATE = "create_date";
    public static final String BRANCH_EMPLOYEE_COLUMN_OXUSER = "ox_user";
    public static final String BRANCH_EMPLOYEE_COLUMN_LAST_UPDATED = "last_updated";
    public static final String BRANCH_EMPLOYEE_COLUMN_IS_SELECTED = "is_selected";

    public static final String LEAVE_CHECKING_TABLE_NAME = "applab_leave_checking";
    public static final String LEAVE_CHECKING_COLUMN_ID = "id";
    public static final String LEAVE_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String LEAVE_CHECKING_COLUMN_NO_PER_PAGE = "no_per_page";
    public static final String LEAVE_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String LEAVE_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";
    public static final String LEAVE_CHECKING_COLUMN_IS_CANCELLED = "is_cancelled";
    public static final String LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE = "is_employee_leave";
    public static final String LEAVE_CHECKING_COLUMN_PAGE_NO = "page_no";

    //Table Leave Reminder
    public static final String LEAVE_REMINDER_TABLE_NAME = "applab_leave_reminder";
    public static final String LEAVE_REMINDER_COLUMN_ID = "id";
    public static final String LEAVE_REMINDER_LEAVE_COLUMN_ID = "leave_id";
    public static final String LEAVE_REMINDER_COLUMN_DATE = "leave_reminder";
    public static final String LEAVE_REMINDER_COLUMN_IS_SEEN = "is_seen";

    //Table Leave
    public static final String LEAVE_TABLE_NAME = "applab_leave";
    public static final String LEAVE_COLUMN_ID = "id";
    public static final String LEAVE_COLUMN_LEAVE_ID = "leave_id";
    public static final String LEAVE_COLUMN_NAME = "name";
    public static final String LEAVE_COLUMN_EMPLOYEE_ID = "employee_id";
    public static final String LEAVE_COLUMN_DEPARTMENT = "department";
    public static final String LEAVE_COLUMN_POSITION = "position";
    public static final String LEAVE_COLUMN_SUPERIOR = "superior";
    public static final String LEAVE_COLUMN_SUPERIOR_ID = "superior_id";
    public static final String LEAVE_COLUMN_TYPE_CODE = "type_code";
    public static final String LEAVE_COLUMN_TYPE = "type";
    public static final String LEAVE_COLUMN_REMARKS = "remarks";
    public static final String LEAVE_COLUMN_LEAVE_DAYS = "leave_days";
    public static final String LEAVE_COLUMN_LEAVE_FROM = "leave_from";
    public static final String LEAVE_COLUMN_LEAVE_TO = "leave_to";
    public static final String LEAVE_COLUMN_IS_EMPLOYEE_LEAVE = "is_employee_leave";
    public static final String LEAVE_COLUMN_STATUS = "status";
    public static final String LEAVE_COLUMN_STATUS_REMARKS = "status_remarks";
    public static final String LEAVE_COLUMN_STATUS_UPDATE_BY = "status_update_by";
    public static final String LEAVE_COLUMN_STATUS_UPDATE_ON = "status_update_on";
    public static final String LEAVE_COLUMN_LAST_UPDATE = "last_update";
    public static final String LEAVE_COLUMN_CREATE_DATE = "create_date";
    public static final String LEAVE_COLUMN_PAGE_NO = "page_no";
    public static final String LEAVE_COLUMN_NEW_CREATE = "new_create";

    //Table Type
    public static final String TYPE_TABLE_NAME = "applab_type";
    public static final String TYPE_COLUMN_ID = "id";
    public static final String TYPE_COLUMN_LEAVE_CODE = "leave_code";
    public static final String TYPE_COLUMN_TITLE = "title";
    public static final String TYPE_COLUMN_DESCRIPTION = "description";
    public static final String TYPE_COLUMN_NO_AVAILABLE = "no_available";

    //Table Profile
    public static final String PROFILE_TABLE_NAME = "applab_profile";
    public static final String PROFILE_COLUMN_ID = "id";
    public static final String PROFILE_COLUMN_FAVORITE_ID = "favorite_id";
    public static final String PROFILE_COLUMN_API_KEY = "api_key";
    public static final String PROFILE_COLUMN_NAME = "name";
    public static final String PROFILE_COLUMN_IMAGE = "image";
    public static final String PROFILE_COLUMN_BRANCH_ID = "branch_id";
    public static final String PROFILE_COLUMN_BRANCH_NAME = "branch_name";
    public static final String PROFILE_COLUMN_COUNTRY = "country";
    public static final String PROFILE_COLUMN_COUNTRY_IMAGE = "country_image";
    public static final String PROFILE_COLUMN_GENDER = "gender";
    public static final String PROFILE_COLUMN_DEPARTMENT = "department";
    public static final String PROFILE_COLUMN_POSITION = "position";
    public static final String PROFILE_COLUMN_EMAIL = "email";
    public static final String PROFILE_COLUMN_CONTACT_NO = "contact_no";
    public static final String PROFILE_COLUMN_ALL_BRANCH = "all_branch";
    public static final String PROFILE_COLUMN_LAST_UPDATED = "last_updated";
    public static final String PROFILE_COLUMN_OFFICE_NO = "office_no";
    public static final String PROFILE_COLUMN_DOB = "dob";
    public static final String PROFILE_COLUMN_HOD_NAME = "hod_name";
    public static final String PROFILE_COLUMN_EMPLOYEE_NO = "employee_no";
    public static final String PROFILE_COLUMN_HOD_ID = "hod_id";
    public static final String PROFILE_COLUMN_OXUSER = "ox_user_id";
    public static final String PROFILE_COLUMN_COUNTRY_NAME = "country_name";

    //Table Token
    public static final String TOKEN_TABLE_NAME = "applab_token";
    public static final String TOKEN_COLUMN_ID = "id";
    public static final String TOKEN_COLUMN_ACCESS_TOKEN = "access_token";
    public static final String TOKEN_COLUMN_TOKEN_TYPE = "token_type";
    public static final String TOKEN_COLUMN_EXPIRED_IN = "expired_in";
    public static final String TOKEN_COLUMN_USER_NAME = "user_name";
    public static final String TOKEN_COLUMN_ISSUED_DATE = "issued_date";
    public static final String TOKEN_COLUMN_EXPIRED_DATE = "expired_date";
    public static final String TOKEN_COLUMN_STATUS = "status";
    public static final String TOKEN_COLUMN_CREATE_BY = "create_by";
    public static final String TOKEN_COLUMN_CREATE_DATE = "create_date";
    public static final String TOKEN_COLUMN_UPDATE_BY = "update_by";
    public static final String TOKEN_COLUMN_UPDATE_DATE = "update_date";

    //Table Album
    public static final String ALBUM_TABLE_NAME = "applab_album";
    public static final String ALBUM_COLUMN_ID = "id";
    public static final String ALBUM_COLUMN_ALBUM_ID = "album_id";
    public static final String ALBUM_COLUMN_TITLE = "title";
    public static final String ALBUM_COLUMN_LEVEL = "level";
    public static final String ALBUM_COLUMN_NEW_CREATE = "new_create";
    public static final String ALBUM_COLUMN_TOTAL_IMAGE = "total_image";
    public static final String ALBUM_COLUMN_ALBUM_COVER = "album_cover";
    public static final String ALBUM_COLUMN_CREATED_DATE = "created_date";
    public static final String ALBUM_COLUMN_UPDATED_DATE = "updated_date";

    //Table Album Checking
    public static final String ALBUM_CHECKING_TABLE_NAME = "applab_album_checking";
    public static final String ALBUM_CHECKING_COLUMN_ID = "id";
    public static final String ALBUM_CHECKING_COLUMN_LEVEL = "level";
    public static final String ALBUM_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String ALBUM_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String ALBUM_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";

    //Table Image Checking
    public static final String IMAGE_CHECKING_TABLE_NAME = "applab_image_checking";
    public static final String IMAGE_CHECKING_COLUMN_ID = "id";
    public static final String IMAGE_CHECKING_COLUMN_LEVEL = "level";
    public static final String IMAGE_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String IMAGE_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String IMAGE_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";

    //Table Image
    public static final String IMAGE_TABLE_NAME = "applab_image";
    public static final String IMAGE_COLUMN_ID = "id";
    public static final String IMAGE_COLUMN_IMAGE_ID = "image_id";
    public static final String IMAGE_COLUMN_ALBUM_ID = "album_id";
    public static final String IMAGE_COLUMN_LEVEL = "level";
    public static final String IMAGE_COLUMN_IS_DEFAULT = "is_default";
    public static final String IMAGE_COLUMN_IMAGE_SMALL = "image_small";
    public static final String IMAGE_COLUMN_IMAGE_LARGE = "image_large";
    public static final String IMAGE_COLUMN_CREATED_DATE = "created_date";
    public static final String IMAGE_COLUMN_UPDATED_DATE = "updated_date";
    public static final String IMAGE_COLUMN_PATH = "path";

    //Table Notification
    public static final String NOTIFICATION_TABLE_NAME = "applab_notification";
    public static final String NOTIFICATION_COLUMN_ID = "id";
    public static final String NOTIFICATION_COLUMN_TYPE = "type";
    public static final String NOTIFICATION_COLUMN_TITLE = "title";
    public static final String NOTIFICATION_COLUMN_DESCRIPTION = "description";
    public static final String NOTIFICATION_COLUMN_IS_NOTIFY = "is_notify";
    public static final String NOTIFICATION_COLUMN_CREATE_DATE = "create_date";

    //Table Attachment
    public static final String ATTACHMENT_TABLE_NAME = "applab_event_attachment";
    public static final String ATTACHMENT_COLUMN_ID = "id";
    public static final String ATTACHMENT_COLUMN_ATTACHMENT_ID = "attachment_id";
    public static final String ATTACHMENT_COLUMN_EVENT_ID = "event_id";
    public static final String ATTACHMENT_COLUMN_FILE_SIZE = "file_size";
    public static final String ATTACHMENT_COLUMN_TITLE = "title";
    public static final String ATTACHMENT_COLUMN_DESCRIPTION = "description";
    public static final String ATTACHMENT_COLUMN_ATTACHMENT = "attachment";
    public static final String ATTACHMENT_COLUMN_UPLOAD_BY = "upload_by";
    public static final String ATTACHMENT_COLUMN_UPLOAD_DATE = "upload_date";
    public static final String ATTACHMENT_COLUMN_PATH = "path";

    //Table Master Setting
    public static final String MASTER_SETTING_TABLE_NAME = "applab_master_setting";
    public static final String MASTER_SETTING_COLUMN_ID = "id";
    public static final String MASTER_SETTING_COLUMN_CODE = "code";
    public static final String MASTER_SETTING_COLUMN_STATUS = "status";
    public static final String MASTER_SETTING_COLUMN_CREATED_DATE = "created_date";
    public static final String MASTER_SETTING_COLUMN_UPDATED_DATE = "updated_date";

    //Table Message Setting
    public static final String MESSAGE_TABLE_NAME = "applab_message";
    public static final String MESSAGE_COLUMN_ID = "id";
    public static final String MESSAGE_COLUMN_CODE = "code";
    public static final String MESSAGE_COLUMN_MESSAGE = "message";
    public static final String MESSAGE_COLUMN_STATUS = "status";
    public static final String MESSAGE_COLUMN_CREATED_DATE = "created_date";
    public static final String MESSAGE_COLUMN_UPDATED_DATE = "updated_date";

    //Table System Setting
    public static final String SYSTEM_TABLE_NAME = "applab_system";
    public static final String SYSTEM_COLUMN_ID = "id";
    public static final String SYSTEM_COLUMN_MESSAGE_LAST_UPDATE = "message_last_update";
    public static final String SYSTEM_COLUMN_SETTING_LAST_UPDATE = "setting_last_update";

    //Table Download Checking
    public static final String DOWNLOAD_CHECKING_TABLE_NAME = "applab_download_checking";
    public static final String DOWNLOAD_CHECKING_COLUMN_ID = "id";
    public static final String DOWNLOAD_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String DOWNLOAD_CHECKING_COLUMN_LEVEL = "level";
    public static final String DOWNLOAD_CHECKING_COLUMN_PAGE_NO = "page_no";
    public static final String DOWNLOAD_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String DOWNLOAD_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";

    //Table Download
    public static final String DOWNLOAD_TABLE_NAME = "applab_download";
    public static final String DOWNLOAD_COLUMN_ID = "id";
    public static final String DOWNLOAD_COLUMN_DOWNLOAD_ID = "download_id";
    public static final String DOWNLOAD_COLUMN_TYPE = "type";
    public static final String DOWNLOAD_COLUMN_TITLE = "title";
    public static final String DOWNLOAD_COLUMN_FILE_PATH = "file_path";
    public static final String DOWNLOAD_COLUMN_FILE_SIZE = "file_size";
    public static final String DOWNLOAD_COLUMN_LEVEL = "level";
    public static final String DOWNLOAD_COLUMN_PAGE_NO = "page_no";
    public static final String DOWNLOAD_COLUMN_DESCRIPTION = "description";
    public static final String DOWNLOAD_COLUMN_CREATED_BY = "created_by";
    public static final String DOWNLOAD_COLUMN_CREATED_DATE = "created_date";
    public static final String DOWNLOAD_COLUMN_NEW_CREATE = "new_create";
    public static final String DOWNLOAD_COLUMN_FILE_SIZE_BYTES = "file_size_bytes";
    public static final String DOWNLOAD_COLUMN_PATH = "path";

    //Table Catalogue Checking
    public static final String CATALOGUE_CHECKING_TABLE_NAME = "applab_product_checking";
    public static final String CATALOGUE_CHECKING_COLUMN_ID = "id";
    public static final String CATALOGUE_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String CATALOGUE_CHECKING_COLUMN_LEVEL = "level";
    public static final String CATALOGUE_CHECKING_COLUMN_PAGE_NO = "page_no";
    public static final String CATALOGUE_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String CATALOGUE_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";

    //Table Catalogue
    public static final String CATALOGUE_TABLE_NAME = "applab_product";
    public static final String CATALOGUE_COLUMN_ID = "id";
    public static final String CATALOGUE_COLUMN_CATALOGUE_ID = "catalogue_id";
    public static final String CATALOGUE_COLUMN_TYPE = "type";
    public static final String CATALOGUE_COLUMN_TITLE = "title";
    public static final String CATALOGUE_COLUMN_FILE_PATH = "file_path";
    public static final String CATALOGUE_COLUMN_FILE_SIZE = "file_size";
    public static final String CATALOGUE_COLUMN_LEVEL = "level";
    public static final String CATALOGUE_COLUMN_PAGE_NO = "page_no";
    public static final String CATALOGUE_COLUMN_DESCRIPTION = "description";
    public static final String CATALOGUE_COLUMN_CREATED_BY = "created_by";
    public static final String CATALOGUE_COLUMN_CREATED_DATE = "created_date";
    public static final String CATALOGUE_COLUMN_NEW_CREATE = "new_create";
    public static final String CATALOGUE_COLUMN_PATH = "path";
    public static final String CATALOGUE_COLUMN_FILE_SIZE_BYTES = "file_size_bytes";

    //Table File
    public static final String FILE_TABLE_NAME = "applab_file";
    public static final String FILE_ID = "id";
    public static final String FILE_PARENT_ID = "parent_id";
    public static final String FILE_NAME = "filename";
    public static final String FILE_CREATION = "created";
    public static final String FILE_MODIFIED = "modified";
    public static final String FILE_CONTENT_LENGTH = "content_length";
    public static final String FILE_CONTENT_TYPE = "content_type";
    public static final String FILE_STORAGE_PATH = "media_path";
    public static final String FILE_REMOTE_PATH = "remote_path";
    public static final String FILE_ETAG = "etag";
    public static final String FILE_SIZE = "size";
    public static final String FILE_SHARE_BY_LINK = "share_by_link";
    public static final String FILE_PUBLIC_LINK = "public_link";
    public static final String FILE_PERMISSIONS = "permissions";
    public static final String FILE_REMOTE_ID = "remote_id";
    public static final String FILE_UPDATE_THUMBNAIL = "update_thumbnail";
    public static final String FILE_IS_DOWNLOADING = "is_downloading";

    //Table Country Checking
    public static final String COUNTRY_CHECKING_TABLE_NAME = "applab_country_checking";
    public static final String COUNTRY_CHECKING_COLUMN_ID = "id";
    public static final String COUNTRY_CHECKING_COLUMN_LEVEL = "level";
    public static final String COUNTRY_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String COUNTRY_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String COUNTRY_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";

    //Table Country
    public static final String COUNTRY_TABLE_NAME = "applab_country";
    public static final String COUNTRY_COLUMN_ID = "id";
    public static final String COUNTRY_COLUMN_COUNTRY_ID = "country_id";
    public static final String COUNTRY_COLUMN_COUNTRY_CODE = "country_code";
    public static final String COUNTRY_COLUMN_COUNTRY_NAME = "country_name";
    public static final String COUNTRY_COLUMN_COUNTRY_IMAGE = "country_image";
    public static final String COUNTRY_COLUMN_LAST_UPDATED = "last_updated";
    public static final String COUNTRY_COLUMN_LEVEL = "level";

    //Table Chat
    public static final String CHAT_TABLE_NAME = "applab_chat";
    public static final String CHAT_COLUMN_CHAT_ID = "chat_id";
    public static final String CHAT_COLUMN_THREAD_ID = "threadId";
    public static final String CHAT_COLUMN_FROM = "from_jid";
    public static final String CHAT_COLUMN_TO = "to_jid";
    public static final String CHAT_COLUMN_STATUS = "status";
    public static final String CHAT_COLUMN_TIME = "time";
    public static final String CHAT_COLUMN_MSG = "msg";
    public static final String CHAT_COLUMN_MSG_TYPE = "msg_type";
    public static final String CHAT_COLUMN_EXTRA = "msg_extra";
    public static final String CHAT_COLUMN_GROUP_ID = "group_id";
    public static final String CHAT_COLUMN_GROUP_NAME = "group_name";
    public static final String CHAT_COLUMN_IS_IMAGE = "is_image";
    public static final String CHAT_COLUMN_IS_GROUP = "is_group";
    public static final String CHAT_COLUMN_IS_DELETE = "is_delete";
    public static final String CHAT_COLUMN_IS_GROUP_UPDATE = "is_update";
    public static final String CHAT_COLUMN_PATH = "path";
    public static final String CHAT_COLUMN_IS_SUCCESS = "is_success";

    //Table Chat List
    public static final String CHAT_LIST_TABLE_NAME = "applab_chat_list";
    public static final String CHAT_LIST_COLUMN_LAST_THREAD_ID = "threadId";
    public static final String CHAT_LIST_COLUMN_USER_ID = "_id";
    public static final String CHAT_LIST_COLUMN_NAME = "name";
    public static final String CHAT_LIST_COLUMN_GROUP_ID = "group_id";
    public static final String CHAT_LIST_COLUMN_GROUP_NAME = "group_name";
    public static final String CHAT_LIST_COLUMN_JID = "jid";
    public static final String CHAT_LIST_COLUMN_AVATAR = "avatar";
    public static final String CHAT_LIST_COLUMN_LAST = "last_Seen";
    public static final String CHAT_LIST_COLUMN_LAST_MSG = "last_msg";
    public static final String CHAT_LIST_COLUMN_TOKEN = "token";
    public static final String CHAT_LIST_COLUMN_UNREAD = "unread";
    public static final String CHAT_LIST_COLUMN_EXTRA1 = "extra1";
    public static final String CHAT_LIST_COLUMN_EXTRA2 = "extra2";
    public static final String CHAT_LIST_COLUMN_IS_IMAGE = "is_image";
    public static final String CHAT_LIST_COLUMN_IS_GROUP = "is_group";
    public static final String CHAT_LIST_COLUMN_IS_DELETE = "is_delete";
    public static final String CHAT_LIST_COLUMN_IS_GROUP_UPDATE = "is_update";
    public static final String CHAT_LIST_COLUMN_IS_SUCCESS = "is_success";

    //Table User Chat Checking
    public static final String USER_CHAT_CHECKING_TABLE_NAME = "applab_user_chat_checking";
    public static final String USER_CHAT_CHECKING_COLUMN_ID = "id";
    public static final String USER_CHAT_CHECKING_COLUMN_LAST_UPDATE = "last_update";
    public static final String USER_CHAT_CHECKING_COLUMN_NO_OF_RECORDS = "no_of_records";
    public static final String USER_CHAT_CHECKING_COLUMN_NO_OF_PAGE = "no_of_page";

    //Table User Chat
    public static final String USER_CHAT_TABLE_NAME = "applab_user_chat";
    public static final String USER_CHAT_COLUMN_ID = "id";
    public static final String USER_CHAT_COLUMN_USER_ID = "user_id";
    public static final String USER_CHAT_COLUMN_IMAGE = "image";
    public static final String USER_CHAT_COLUMN_EMPLOYEE_ID = "employee_id";
    public static final String USER_CHAT_COLUMN_NAME = "employee_name";
    public static final String USER_CHAT_COLUMN_OX_USER = "ox_user";
    public static final String USER_CHAT_COLUMN_CONTACT_NO = "contact_no";
    public static final String USER_CHAT_COLUMN_OFFICE_NO = "office_no";
    public static final String USER_CHAT_COLUMN_POSITION = "position";
    public static final String USER_CHAT_COLUMN_COUNTRY_IMAGE = "country_image";

    //Table Group User Chat
    public static final String GROUP_CHAT_TABLE_NAME = "applab_group_chat";
    public static final String GROUP_CHAT_COLUMN_ID = "id";
    public static final String GROUP_CHAT_COLUMN_GROUP_ID = "group_id";
    public static final String GROUP_CHAT_COLUMN_IMAGE = "image";
    public static final String GROUP_CHAT_COLUMN_EMPLOYEE_ID = "employee_id";
    public static final String GROUP_CHAT_COLUMN_NAME = "employee_name";
    public static final String GROUP_CHAT_COLUMN_OX_USER = "ox_user";
    public static final String GROUP_CHAT_COLUMN_CONTACT_NO = "contact_no";
    public static final String GROUP_CHAT_COLUMN_OFFICE_NO = "office_no";
    public static final String GROUP_CHAT_COLUMN_POSITION = "position";
    public static final String GROUP_CHAT_COLUMN_COUNTRY_IMAGE = "country_image";
    public static final String GROUP_CHAT_COLUMN_OWNER = "owner";

    //Table View Group join User
    public static final String GROUP_USER_TABLE_VIEW_NAME = "applab_group_user_view";

    //Table View User Chat
    public static final String USER_TABLE_VIEW_NAME = "applab_user_view";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 426);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + PROFILE_TABLE_NAME +
                        "(" + PROFILE_COLUMN_ID + " integer primary key, " +
                        "" + PROFILE_COLUMN_FAVORITE_ID + " integer," +
                        "" + PROFILE_COLUMN_API_KEY + " text," +
                        "" + PROFILE_COLUMN_IMAGE + " text," +
                        "" + PROFILE_COLUMN_BRANCH_ID + " text," +
                        "" + PROFILE_COLUMN_BRANCH_NAME + " text," +
                        "" + PROFILE_COLUMN_COUNTRY + " text," +
                        "" + PROFILE_COLUMN_OFFICE_NO + " text," +
                        "" + PROFILE_COLUMN_DOB + " text," +
                        "" + PROFILE_COLUMN_HOD_NAME + " text," +
                        "" + PROFILE_COLUMN_EMPLOYEE_NO + " text," +
                        "" + PROFILE_COLUMN_HOD_ID + " text," +
                        "" + PROFILE_COLUMN_OXUSER + " text," +
                        "" + PROFILE_COLUMN_COUNTRY_NAME + " text," +
                        "" + PROFILE_COLUMN_COUNTRY_IMAGE + " text," +
                        "" + PROFILE_COLUMN_GENDER + " text," +
                        "" + PROFILE_COLUMN_ALL_BRANCH + " text," +
                        "" + PROFILE_COLUMN_NAME + " text," +
                        "" + PROFILE_COLUMN_DEPARTMENT + " text," +
                        "" + PROFILE_COLUMN_POSITION + " text," +
                        "" + PROFILE_COLUMN_EMAIL + " text," +
                        "" + PROFILE_COLUMN_LAST_UPDATED + " text," +
                        "" + PROFILE_COLUMN_CONTACT_NO + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + TOKEN_TABLE_NAME +
                        "(" + TOKEN_COLUMN_ID + " integer primary key, " +
                        "" + TOKEN_COLUMN_ACCESS_TOKEN + " text, " +
                        "" + TOKEN_COLUMN_TOKEN_TYPE + " text, " +
                        "" + TOKEN_COLUMN_EXPIRED_IN + " text, " +
                        "" + TOKEN_COLUMN_USER_NAME + " text, " +
                        "" + TOKEN_COLUMN_ISSUED_DATE + " text, " +
                        "" + TOKEN_COLUMN_EXPIRED_DATE + " text, " +
                        "" + TOKEN_COLUMN_STATUS + " integer," +
                        "" + TOKEN_COLUMN_CREATE_BY + " text," +
                        "" + TOKEN_COLUMN_CREATE_DATE + " text," +
                        "" + TOKEN_COLUMN_UPDATE_BY + " text," +
                        "" + TOKEN_COLUMN_UPDATE_DATE + " text) "
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + CALENDAR_TABLE_NAME +
                        "(" + CALENDAR_COLUMN_ID + " integer primary key, " +
                        "" + CALENDAR_COLUMN_TITLE + " text," +
                        "" + CALENDAR_COLUMN_CALENDAR_ID + " integer," +
                        "" + CALENDAR_COLUMN_TYPE + " text," +
                        "" + CALENDAR_COLUMN_REMINDER + " text," +
                        "" + CALENDAR_COLUMN_CREATE_BY_ID + " integer," +
                        "" + CALENDAR_COLUMN_CREATE_BY_NAME + " text," +
                        "" + CALENDAR_COLUMN_UPDATE_BY_ID + " text," +
                        "" + CALENDAR_COLUMN_UPDATE_BY_NAME + " text," +
                        "" + CALENDAR_COLUMN_LEVEL + " text," +
                        "" + CALENDAR_COLUMN_DESCRIPTION + " text," +
                        "" + CALENDAR_COLUMN_MONTH + " text," +// 12/1/2016 : Change from version 425 to 426
                        "" + CALENDAR_COLUMN_ALL_DAY + " text, " +
                        "" + CALENDAR_COLUMN_LOCATION + " text," +
                        "" + CALENDAR_COLUMN_NEW_CREATE + " text," +
                        "" + CALENDAR_COLUMN_START_DATE + " DATETIME," +
                        "" + CALENDAR_COLUMN_END_DATE + " DATETIME," +
                        "" + CALENDAR_COLUMN_SHOW_DATE + " DATETIME," +
                        "" + CALENDAR_COLUMN_STATUS + " integer)"
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + CALENDAR_CHECKING_TABLE_NAME +
                        "(" + CALENDAR_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + CALENDAR_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + CALENDAR_CHECKING_COLUMN_LEVEL + " integer," +
                        "" + CALENDAR_CHECKING_COLUMN_NO_OF_RECORDS + " text," +
                        "" + CALENDAR_CHECKING_COLUMN_MONTH + " text," +// 12/1/2016 : Change from version 425 to 426
                        "" + CALENDAR_CHECKING_COLUMN_NO_OF_PAGE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + ATTENDEES_TABLE_NAME +
                        "(" + ATTENDEES_COLUMN_ID + " integer primary key, " +
                        "" + ATTENDEES_COLUMN_CALENDAR_ID + " integer," +
                        "" + ATTENDEES_COLUMN_STATUS + " text," +
                        "" + ATTENDEES_COLUMN_REMINDER + " text," +
                        "" + ATTENDEES_COLUMN_LEVEL + " integer," +
                        "" + ATTENDEES_COLUMN_EMPLOYEE_ID + " integer," +
                        "" + ATTENDEES_COLUMN_IS_SELECT + " integer," +
                        "" + ATTENDEES_COLUMN_MONTH + " text," + // 12/1/2016 : Change from version 425 to 426
                        "" + ATTENDEES_COLUMN_IS_SUBMIT + " integer," +
                        "" + ATTENDEES_COLUMN_NAME + " integer) "
        );


        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + TEMP_ATTENDEES_TABLE_NAME +
                        "(" + TEMP_ATTENDEES_COLUMN_ID + " integer primary key, " +
                        "" + TEMP_ATTENDEES_COLUMN_CALENDAR_ID + " integer," +
                        "" + TEMP_ATTENDEES_COLUMN_STATUS + " text," +
                        "" + TEMP_ATTENDEES_COLUMN_REMINDER + " text," +
                        "" + TEMP_ATTENDEES_COLUMN_EMPLOYEE_ID + " integer," +
                        "" + TEMP_ATTENDEES_COLUMN_IS_SELECT + " integer," +
                        "" + TEMP_ATTENDEES_COLUMN_IS_SUBMIT + " integer," +
                        "" + TEMP_ATTENDEES_COLUMN_NAME + " integer) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + NEWS_TABLE_NAME +
                        "(" + NEWS_COLUMN_ID + " integer primary key, " +
                        "" + NEWS_COLUMN_NEWS_ID + " integer, " +
                        "" + NEWS_COLUMN_TITLE + " text," +
                        "" + NEWS_COLUMN_LEVEL + " integer," +
                        "" + NEWS_COLUMN_DESCRIPTION + " text," +
                        "" + NEWS_COLUMN_AUTHOR + " text," +
                        "" + NEWS_COLUMN_NO_OF_IMAGE + " text," +
                        "" + NEWS_COLUMN_NEW_CREATE + " text," +
                        "" + NEWS_COLUMN_CREATE_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + PLACE_TABLE_NAME +
                        "(" + PLACE_COLUMN_ID + " integer primary key, " +
                        "" + PLACE_COLUMN_NAME + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + EVENT_TABLE_NAME +
                        "(" + EVENT_COLUMN_ID + " integer primary key, " +
                        "" + EVENT_COLUMN_EVENT_ID + " integer," +
                        "" + EVENT_COLUMN_TITLE + " text," +
                        "" + EVENT_COLUMN_LEVEL + " integer," +
                        "" + EVENT_COLUMN_DESCRIPTION + " text," +
                        "" + EVENT_COLUMN_START_DATE + " text," +
                        "" + EVENT_COLUMN_END_DATE + " text," +
                        "" + EVENT_COLUMN_TYPE_ID + " text," +
                        "" + EVENT_COLUMN_TYPE + " text," +
                        "" + EVENT_COLUMN_ALL_DAY + " text," +
                        "" + EVENT_COLUMN_FEATURE_IMAGE + " text," +
                        "" + EVENT_COLUMN_BANNER + " text," +
                        "" + EVENT_COLUMN_CREATE_DATE + " text," +
                        "" + EVENT_COLUMN_NEW_CREATE + " text," +
                        "" + EVENT_COLUMN_LOCATION + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + DASHBOARD_TABLE_NAME +
                        "(" + DASHBOARD_COLUMN_ID + " integer primary key, " +
                        "" + DASHBOARD_COLUMN_TITLE + " text," +
                        "" + DASHBOARD_COLUMN_IMG_DASHBOARD + " integer," +
                        "" + DASHBOARD_COLUMN_IMG_MENU + " integer," +
                        "" + DASHBOARD_COLUMN_NO + " integer) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + RECENT_EMPLOYEE_TABLE_NAME +
                        "(" + RECENT_EMPLOYEE_COLUMN_ID + " integer primary key," +
                        "" + RECENT_EMPLOYEE_COLUMN_FAVORITE_ID + " integer," +
                        "" + RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + " integer," +
                        "" + RECENT_EMPLOYEE_COLUMN_ACTION_ID + " integer," +
                        "" + RECENT_EMPLOYEE_COLUMN_NAME + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_PAGE_NO + " integer," +
                        "" + RECENT_EMPLOYEE_COLUMN_DEPARTMENT + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_CONTACT_NO + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_OFFICE_NO + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_DOB + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_HOD_NAME + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_EMPLOYEE_NO + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_HOD_ID + " integer," +
                        "" + RECENT_EMPLOYEE_COLUMN_ALL_BRANCH + " integer," +
                        "" + RECENT_EMPLOYEE_COLUMN_ACTION + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_CREATE_DATE + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_OXUSER + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_GENDER + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_COUNTRY + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_COUNTRY_NAME + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_POSITION + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_LAST_UPDATED + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_IMAGE + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_BRANCH_NAME + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_BRANCH_ID + " text," +
                        "" + RECENT_EMPLOYEE_COLUMN_EMAIL + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + DEPARTMENT_EMPLOYEE_TABLE_NAME +
                        "(" + DEPARTMENT_EMPLOYEE_COLUMN_ID + " integer primary key," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_FAVORITE_ID + " integer," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + " integer," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_NAME + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_PAGE_NO + " integer," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_DEPARTMENT + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_CONTACT_NO + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_OFFICE_NO + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_DOB + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_HOD_NAME + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_NO + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_HOD_ID + " integer," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_ALL_BRANCH + " integer," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_ACTION + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_CREATE_DATE + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_OXUSER + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_GENDER + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_NAME + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_POSITION + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_LAST_UPDATED + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_IMAGE + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_NAME + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_ID + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_IS_SELECTED + " text," +
                        "" + DEPARTMENT_EMPLOYEE_COLUMN_EMAIL + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + BRANCH_EMPLOYEE_TABLE_NAME +
                        "(" + BRANCH_EMPLOYEE_COLUMN_ID + " integer primary key," +
                        "" + BRANCH_EMPLOYEE_COLUMN_FAVORITE_ID + " integer," +
                        "" + BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + " integer," +
                        "" + BRANCH_EMPLOYEE_COLUMN_NAME + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_PAGE_NO + " integer," +
                        "" + BRANCH_EMPLOYEE_COLUMN_DEPARTMENT + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_CONTACT_NO + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_OFFICE_NO + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_DOB + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_HOD_NAME + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_NO + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_HOD_ID + " integer," +
                        "" + BRANCH_EMPLOYEE_COLUMN_ALL_BRANCH + " integer," +
                        "" + BRANCH_EMPLOYEE_COLUMN_ACTION + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_CREATE_DATE + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_OXUSER + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_GENDER + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_COUNTRY + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_COUNTRY_NAME + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_COUNTRY_IMAGE + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_POSITION + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_LAST_UPDATED + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_IMAGE + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_BRANCH_NAME + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_BRANCH_ID + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_IS_SELECTED + " text," +
                        "" + BRANCH_EMPLOYEE_COLUMN_EMAIL + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + EMPLOYEE_CHECKING_TABLE_NAME +
                        "(" + EMPLOYEE_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + EMPLOYEE_CHECKING_COLUMN_PAGE_NO + " integer," +
                        "" + EMPLOYEE_CHECKING_COLUMN_TYPE + " text," +
                        "" + EMPLOYEE_CHECKING_COLUMN_IS_RECENT + " text," +
                        "" + EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + " text," +
                        "" + EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + " text," +
                        "" + EMPLOYEE_CHECKING_COLUMN_NO_PER_PAGE + " integer," +
                        "" + EMPLOYEE_CHECKING_COLUMN_NO_OF_RECORDS + " text," +
                        "" + EMPLOYEE_CHECKING_COLUMN_NO_OF_PAGE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + LEAVE_TABLE_NAME +
                        "(" + LEAVE_COLUMN_ID + " integer primary key," +
                        "" + LEAVE_COLUMN_LEAVE_ID + " integer," +
                        "" + LEAVE_COLUMN_REMARKS + " text," +
                        "" + LEAVE_COLUMN_NAME + " text," +
                        "" + LEAVE_COLUMN_EMPLOYEE_ID + " integer," +
                        "" + LEAVE_COLUMN_DEPARTMENT + " text," +
                        "" + LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + " text," +
                        "" + LEAVE_COLUMN_POSITION + " text," +
                        "" + LEAVE_COLUMN_SUPERIOR + " text," +
                        "" + LEAVE_COLUMN_SUPERIOR_ID + " integer," +
                        "" + LEAVE_COLUMN_TYPE_CODE + " text," +
                        "" + LEAVE_COLUMN_TYPE + " text," +
                        "" + LEAVE_COLUMN_LEAVE_DAYS + " text," +
                        "" + LEAVE_COLUMN_LEAVE_FROM + " text," +
                        "" + LEAVE_COLUMN_LEAVE_TO + " text," +
                        "" + LEAVE_COLUMN_STATUS_REMARKS + " text," +
                        "" + LEAVE_COLUMN_STATUS_UPDATE_BY + " text," +
                        "" + LEAVE_COLUMN_STATUS_UPDATE_ON + " text," +
                        "" + LEAVE_COLUMN_LAST_UPDATE + " text," +
                        "" + LEAVE_COLUMN_CREATE_DATE + " text," +
                        "" + LEAVE_COLUMN_PAGE_NO + " integer," +
                        "" + LEAVE_COLUMN_NEW_CREATE + " text," +
                        "" + LEAVE_COLUMN_STATUS + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + ALBUM_TABLE_NAME +
                        "(" + ALBUM_COLUMN_ID + " integer primary key, " +
                        "" + ALBUM_COLUMN_ALBUM_ID + " integer," +
                        "" + ALBUM_COLUMN_TITLE + " text," +
                        "" + ALBUM_COLUMN_LEVEL + " integer," +
                        "" + ALBUM_COLUMN_TOTAL_IMAGE + " integer," +
                        "" + ALBUM_COLUMN_ALBUM_COVER + " text," +
                        "" + ALBUM_COLUMN_NEW_CREATE + " text," +
                        "" + ALBUM_COLUMN_UPDATED_DATE + " text," +
                        "" + ALBUM_COLUMN_CREATED_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + IMAGE_TABLE_NAME +
                        "(" + IMAGE_COLUMN_ID + " integer primary key, " +
                        "" + IMAGE_COLUMN_IMAGE_ID + " integer," +
                        "" + IMAGE_COLUMN_ALBUM_ID + " integer," +
                        "" + IMAGE_COLUMN_LEVEL + " integer," +
                        "" + IMAGE_COLUMN_IMAGE_SMALL + " text," +
                        "" + IMAGE_COLUMN_IS_DEFAULT + " text," +
                        "" + IMAGE_COLUMN_IMAGE_LARGE + " text," +
                        "" + IMAGE_COLUMN_UPDATED_DATE + " text," +
                        "" + IMAGE_COLUMN_PATH + " text," +
                        "" + IMAGE_COLUMN_CREATED_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + FAVORITE_CONTACT_TABLE_NAME +
                        "(" + FAVORITE_CONTACT_COLUMN_ID + " integer primary key, " +
                        "" + FAVORITE_CONTACT_COLUMN_LAST_UPDATE + " text," +
                        "" + FAVORITE_CONTACT_COLUMN_PAGE_NO + " integer," +
                        "" + FAVORITE_CONTACT_COLUMN_NO_PER_PAGE + " integer," +
                        "" + FAVORITE_CONTACT_COLUMN_NO_OF_RECORDS + " integer," +
                        "" + FAVORITE_CONTACT_COLUMN_NO_OF_PAGE + " integer) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + CONTACT_TABLE_NAME +
                        "(" + CONTACT_COLUMN_ID + " integer primary key," +
                        "" + CONTACT_COLUMN_FAVORITE_ID + " integer," +
                        "" + CONTACT_COLUMN_PAGE_NO + " integer," +
                        "" + CONTACT_COLUMN_NAME + " text," +
                        "" + CONTACT_COLUMN_OFFICE_NO + " text," +
                        "" + CONTACT_COLUMN_DOB + " text," +
                        "" + CONTACT_COLUMN_HOD_NAME + " text," +
                        "" + CONTACT_COLUMN_EMPLOYEE_NO + " text," +
                        "" + CONTACT_COLUMN_HOD_ID + " integer," +
                        "" + CONTACT_COLUMN_DEPARTMENT + " text," +
                        "" + CONTACT_COLUMN_CONTACT_NO + " text," +
                        "" + CONTACT_COLUMN_IMAGE + " text," +
                        "" + CONTACT_COLUMN_BRANCH_ID + " integer," +
                        "" + CONTACT_COLUMN_BRANCH_NAME + " text," +
                        "" + CONTACT_COLUMN_COUNTRY + " text," +
                        "" + CONTACT_COLUMN_COUNTRY_NAME + " text," +
                        "" + CONTACT_COLUMN_COUNTRY_IMAGE + " text," +
                        "" + CONTACT_COLUMN_GENDER + " text," +
                        "" + CONTACT_COLUMN_POSITION + " text," +
                        "" + CONTACT_COLUMN_ALL_BRANCH + " text," +
                        "" + CONTACT_COLUMN_OXUSER + " text," +
                        "" + CONTACT_COLUMN_LAST_UPDATED + " text," +
                        "" + CONTACT_COLUMN_EMAIL + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + NOTIFICATION_TABLE_NAME +
                        "(" + NOTIFICATION_COLUMN_ID + " integer primary key," +
                        "" + NOTIFICATION_COLUMN_TYPE + " text," +
                        "" + NOTIFICATION_COLUMN_TITLE + " text," +
                        "" + NOTIFICATION_COLUMN_DESCRIPTION + " text," +
                        "" + NOTIFICATION_COLUMN_IS_NOTIFY + " integer," +
                        "" + NOTIFICATION_COLUMN_CREATE_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + ATTACHMENT_TABLE_NAME +
                        "(" + ATTACHMENT_COLUMN_ID + " integer primary key," +
                        "" + ATTACHMENT_COLUMN_ATTACHMENT_ID + " integer," +
                        "" + ATTACHMENT_COLUMN_EVENT_ID + " text," +
                        "" + ATTACHMENT_COLUMN_FILE_SIZE + " text," +
                        "" + ATTACHMENT_COLUMN_TITLE + " text," +
                        "" + ATTACHMENT_COLUMN_DESCRIPTION + " text," +
                        "" + ATTACHMENT_COLUMN_UPLOAD_BY + " text," +
                        "" + ATTACHMENT_COLUMN_UPLOAD_DATE + " text," +
                        "" + ATTACHMENT_COLUMN_PATH + " text," +
                        "" + ATTACHMENT_COLUMN_ATTACHMENT + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + MASTER_SETTING_TABLE_NAME +
                        "(" + MASTER_SETTING_COLUMN_ID + " integer primary key," +
                        "" + MASTER_SETTING_COLUMN_CODE + " text," +
                        "" + MASTER_SETTING_COLUMN_STATUS + " text," +
                        "" + MASTER_SETTING_COLUMN_CREATED_DATE + " text," +
                        "" + MASTER_SETTING_COLUMN_UPDATED_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + MESSAGE_TABLE_NAME +
                        "(" + MESSAGE_COLUMN_ID + " integer primary key," +
                        "" + MESSAGE_COLUMN_CODE + " text," +
                        "" + MESSAGE_COLUMN_MESSAGE + " text," +
                        "" + MESSAGE_COLUMN_STATUS + " text," +
                        "" + MESSAGE_COLUMN_CREATED_DATE + " text," +
                        "" + MESSAGE_COLUMN_UPDATED_DATE + " text) "
        );


        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + NEWS_CHECKING_TABLE_NAME +
                        "(" + NEWS_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + NEWS_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + NEWS_CHECKING_COLUMN_LEVEL + " integer," +
                        "" + NEWS_CHECKING_COLUMN_NO_OF_RECORDS + " text," +
                        "" + NEWS_CHECKING_COLUMN_NO_OF_PAGE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + EVENT_CHECKING_TABLE_NAME +
                        "(" + EVENT_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + EVENT_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + EVENT_CHECKING_COLUMN_LEVEL + " integer," +
                        "" + EVENT_CHECKING_COLUMN_NO_OF_RECORDS + " text," +
                        "" + EVENT_CHECKING_COLUMN_NO_OF_PAGE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + NEWS_IMAGE_TABLE_NAME +
                        "(" + NEWS_IMAGE_COLUMN_ID + " integer primary key," +
                        "" + NEWS_IMAGE_COLUMN_NEWS_ID + " integer," +
                        "" + NEWS_IMAGE_COLUMN_IMAGE_ID + " integer," +
                        "" + NEWS_IMAGE_COLUMN_DESCRIPTION + " text," +
                        "" + NEWS_IMAGE_COLUMN_IMAGE + " text," +
                        "" + NEWS_IMAGE_COLUMN_IMAGE_THUMB + " text," +
                        "" + NEWS_IMAGE_COLUMN_PATH + " text," +
                        "" + NEWS_IMAGE_COLUMN_UPLOAD_BY + " text," +
                        "" + NEWS_IMAGE_COLUMN_UPLOAD_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + ALBUM_CHECKING_TABLE_NAME +
                        "(" + ALBUM_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + ALBUM_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + ALBUM_CHECKING_COLUMN_LEVEL + " integer," +
                        "" + ALBUM_CHECKING_COLUMN_NO_OF_RECORDS + " text," +
                        "" + ALBUM_CHECKING_COLUMN_NO_OF_PAGE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + IMAGE_CHECKING_TABLE_NAME +
                        "(" + IMAGE_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + IMAGE_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + IMAGE_CHECKING_COLUMN_LEVEL + " integer," +
                        "" + IMAGE_CHECKING_COLUMN_NO_OF_RECORDS + " text," +
                        "" + IMAGE_CHECKING_COLUMN_NO_OF_PAGE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + SYSTEM_TABLE_NAME +
                        "(" + SYSTEM_COLUMN_ID + " integer primary key," +
                        "" + SYSTEM_COLUMN_MESSAGE_LAST_UPDATE + " integer," +
                        "" + SYSTEM_COLUMN_SETTING_LAST_UPDATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + DOWNLOAD_CHECKING_TABLE_NAME +
                        "(" + DOWNLOAD_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + DOWNLOAD_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + DOWNLOAD_CHECKING_COLUMN_LEVEL + " integer," +
                        "" + DOWNLOAD_CHECKING_COLUMN_PAGE_NO + " integer," +
                        "" + DOWNLOAD_CHECKING_COLUMN_NO_OF_RECORDS + " text," +
                        "" + DOWNLOAD_CHECKING_COLUMN_NO_OF_PAGE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + DOWNLOAD_TABLE_NAME +
                        "(" + DOWNLOAD_COLUMN_ID + " integer primary key," +
                        "" + DOWNLOAD_COLUMN_DOWNLOAD_ID + " integer," +
                        "" + DOWNLOAD_COLUMN_TYPE + " text," +
                        "" + DOWNLOAD_COLUMN_FILE_SIZE_BYTES + " integer," +
                        "" + DOWNLOAD_COLUMN_TITLE + " text," +
                        "" + DOWNLOAD_COLUMN_FILE_PATH + " text," +
                        "" + DOWNLOAD_COLUMN_LEVEL + " integer," +
                        "" + DOWNLOAD_COLUMN_PAGE_NO + " integer," +
                        "" + DOWNLOAD_COLUMN_FILE_SIZE + " text," +
                        "" + DOWNLOAD_COLUMN_DESCRIPTION + " text," +
                        "" + DOWNLOAD_COLUMN_CREATED_BY + " text," +
                        "" + DOWNLOAD_COLUMN_NEW_CREATE + " text," +
                        "" + DOWNLOAD_COLUMN_PATH + " text," +
                        "" + DOWNLOAD_COLUMN_CREATED_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + CATALOGUE_CHECKING_TABLE_NAME +
                        "(" + CATALOGUE_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + CATALOGUE_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + CATALOGUE_CHECKING_COLUMN_LEVEL + " integer," +
                        "" + CATALOGUE_CHECKING_COLUMN_PAGE_NO + " integer," +
                        "" + CATALOGUE_CHECKING_COLUMN_NO_OF_RECORDS + " text," +
                        "" + CATALOGUE_CHECKING_COLUMN_NO_OF_PAGE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + CATALOGUE_TABLE_NAME +
                        "(" + CATALOGUE_COLUMN_ID + " integer primary key," +
                        "" + CATALOGUE_COLUMN_CATALOGUE_ID + " integer," +
                        "" + CATALOGUE_COLUMN_TYPE + " text," +
                        "" + CATALOGUE_COLUMN_FILE_SIZE_BYTES + " integer," +
                        "" + CATALOGUE_COLUMN_TITLE + " text," +
                        "" + CATALOGUE_COLUMN_FILE_PATH + " text," +
                        "" + CATALOGUE_COLUMN_LEVEL + " integer," +
                        "" + CATALOGUE_COLUMN_PAGE_NO + " integer," +
                        "" + CATALOGUE_COLUMN_FILE_SIZE + " text," +
                        "" + CATALOGUE_COLUMN_PATH + " text," +
                        "" + CATALOGUE_COLUMN_DESCRIPTION + " text," +
                        "" + CATALOGUE_COLUMN_CREATED_BY + " text," +
                        "" + CATALOGUE_COLUMN_NEW_CREATE + " text," +
                        "" + CATALOGUE_COLUMN_CREATED_DATE + " text) "
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + FILE_TABLE_NAME +
                        "(" + FILE_ID + " integer primary key, " +
                        "" + FILE_PARENT_ID + " text," +
                        "" + FILE_NAME + " text," +
                        "" + FILE_CREATION + " integer," +
                        "" + FILE_MODIFIED + " integer, " +
                        "" + FILE_CONTENT_LENGTH + " integer," +
                        "" + FILE_CONTENT_TYPE + " text," +
                        "" + FILE_STORAGE_PATH + " text," +
                        "" + FILE_REMOTE_PATH + " text," +
                        "" + FILE_SIZE + " integer," +
                        "" + FILE_ETAG + " text," +
                        "" + FILE_SHARE_BY_LINK + " integer," +
                        "" + FILE_PUBLIC_LINK + " text," +
                        "" + FILE_PERMISSIONS + " text," +
                        "" + FILE_REMOTE_ID + " text," +
                        "" + FILE_UPDATE_THUMBNAIL + " text," +
                        "" + FILE_IS_DOWNLOADING + " text)"
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + COUNTRY_CHECKING_TABLE_NAME +
                        "(" + COUNTRY_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + COUNTRY_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + COUNTRY_CHECKING_COLUMN_LEVEL + " integer," +
                        "" + COUNTRY_CHECKING_COLUMN_NO_OF_RECORDS + " text," +
                        "" + COUNTRY_CHECKING_COLUMN_NO_OF_PAGE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + COUNTRY_TABLE_NAME +
                        "(" + COUNTRY_COLUMN_ID + " integer primary key," +
                        "" + COUNTRY_COLUMN_COUNTRY_ID + " integer," +
                        "" + COUNTRY_COLUMN_COUNTRY_CODE + " text," +
                        "" + COUNTRY_COLUMN_COUNTRY_NAME + " text," +
                        "" + COUNTRY_COLUMN_COUNTRY_IMAGE + " text," +
                        "" + COUNTRY_COLUMN_LEVEL + " integer," +
                        "" + COUNTRY_COLUMN_LAST_UPDATED + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + TYPE_TABLE_NAME +
                        "(" + TYPE_COLUMN_ID + " integer primary key," +
                        "" + TYPE_COLUMN_LEAVE_CODE + " text," +
                        "" + TYPE_COLUMN_NO_AVAILABLE + " integer," +
                        "" + TYPE_COLUMN_TITLE + " text," +
                        "" + TYPE_COLUMN_DESCRIPTION + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + LEAVE_CHECKING_TABLE_NAME +
                        "(" + LEAVE_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + LEAVE_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + LEAVE_CHECKING_COLUMN_NO_OF_PAGE + " integer," +
                        "" + LEAVE_CHECKING_COLUMN_IS_CANCELLED + " text," +
                        "" + LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + " text," +
                        "" + LEAVE_CHECKING_COLUMN_PAGE_NO + " integer," +
                        "" + LEAVE_CHECKING_COLUMN_NO_PER_PAGE + " integer," +
                        "" + LEAVE_CHECKING_COLUMN_NO_OF_RECORDS + " integer) "
        );
        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + CHAT_TABLE_NAME +
                        "(" + CHAT_COLUMN_CHAT_ID + " integer primary key autoincrement," +
                        "" + CHAT_COLUMN_FROM + " text," +
                        "" + CHAT_COLUMN_THREAD_ID + " text," +
                        "" + CHAT_COLUMN_TO + " text," +
                        "" + CHAT_COLUMN_STATUS + " text," +
                        "" + CHAT_COLUMN_TIME + " text," +
                        "" + CHAT_COLUMN_MSG + " text," +
                        "" + CHAT_COLUMN_IS_SUCCESS + " text," +// 12/1/2016 : Change from version 425 to 426
                        "" + CHAT_COLUMN_EXTRA + " text," +
                        "" + CHAT_COLUMN_GROUP_ID + " text," +
                        "" + CHAT_COLUMN_GROUP_NAME + " text," +
                        "" + CHAT_COLUMN_IS_IMAGE + " text," +
                        "" + CHAT_COLUMN_IS_GROUP + " text," +
                        "" + CHAT_COLUMN_IS_DELETE + " text," +
                        "" + CHAT_COLUMN_PATH + " text," +
                        "" + CHAT_COLUMN_IS_GROUP_UPDATE + " text," +
                        "" + CHAT_COLUMN_MSG_TYPE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + CHAT_LIST_TABLE_NAME +
                        "(" + CHAT_LIST_COLUMN_USER_ID + " integer primary key autoincrement," +
                        "" + CHAT_LIST_COLUMN_NAME + " text," +
                        "" + CHAT_LIST_COLUMN_GROUP_ID + " text," +
                        "" + CHAT_LIST_COLUMN_GROUP_NAME + " text," +
                        "" + CHAT_LIST_COLUMN_AVATAR + " text," +
                        "" + CHAT_LIST_COLUMN_JID + " text," +
                        "" + CHAT_LIST_COLUMN_LAST + " text," +
                        "" + CHAT_LIST_COLUMN_LAST_THREAD_ID + " text," +
                        "" + CHAT_LIST_COLUMN_LAST_MSG + " text," +
                        "" + CHAT_LIST_COLUMN_TOKEN + " text," +
                        "" + CHAT_LIST_COLUMN_UNREAD + " text," +
                        "" + CHAT_LIST_COLUMN_IS_SUCCESS + " text," +// 12/1/2016 : Change from version 425 to 426
                        "" + CHAT_LIST_COLUMN_IS_IMAGE + " text," +
                        "" + CHAT_LIST_COLUMN_IS_GROUP + " text," +
                        "" + CHAT_LIST_COLUMN_IS_DELETE + " text," +
                        "" + CHAT_LIST_COLUMN_IS_GROUP_UPDATE + " text," +
                        "" + CHAT_LIST_COLUMN_EXTRA1 + " text," +
                        "" + CHAT_LIST_COLUMN_EXTRA2 + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + USER_CHAT_TABLE_NAME +
                        "(" + USER_CHAT_COLUMN_ID + " integer primary key autoincrement," +
                        "" + USER_CHAT_COLUMN_USER_ID + " integer," +
                        "" + USER_CHAT_COLUMN_IMAGE + " text," +
                        "" + USER_CHAT_COLUMN_EMPLOYEE_ID + " integer," +
                        "" + USER_CHAT_COLUMN_COUNTRY_IMAGE + " text," +
                        "" + USER_CHAT_COLUMN_OX_USER + " text," +
                        "" + USER_CHAT_COLUMN_CONTACT_NO + " text," +
                        "" + USER_CHAT_COLUMN_POSITION + " text," +
                        "" + USER_CHAT_COLUMN_OFFICE_NO + " text," +
                        "" + USER_CHAT_COLUMN_NAME + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + USER_CHAT_CHECKING_TABLE_NAME +
                        "(" + USER_CHAT_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + USER_CHAT_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + USER_CHAT_CHECKING_COLUMN_NO_OF_RECORDS + " text," +
                        "" + USER_CHAT_CHECKING_COLUMN_NO_OF_PAGE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + CALENDAR_REMINDER_TABLE_NAME +
                        "(" + CALENDAR_REMINDER_COLUMN_ID + " integer primary key," +
                        "" + CALENDAR_REMINDER_CALENDAR_COLUMN_ID + " integer," +
                        "" + CALENDAR_REMINDER_COLUMN_IS_SEEN + " text," +
                        "" + CALENDAR_REMINDER_COLUMN_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + LEAVE_REMINDER_TABLE_NAME +
                        "(" + LEAVE_REMINDER_COLUMN_ID + " integer primary key," +
                        "" + LEAVE_REMINDER_LEAVE_COLUMN_ID + " integer," +
                        "" + LEAVE_REMINDER_COLUMN_IS_SEEN + " text," +
                        "" + LEAVE_REMINDER_COLUMN_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + GROUP_CHAT_TABLE_NAME +
                        "(" + GROUP_CHAT_COLUMN_ID + " integer primary key," +
                        "" + GROUP_CHAT_COLUMN_IMAGE + " text," +
                        "" + GROUP_CHAT_COLUMN_EMPLOYEE_ID + " text," +
                        "" + GROUP_CHAT_COLUMN_NAME + " text," +
                        "" + GROUP_CHAT_COLUMN_OX_USER + " text," +
                        "" + GROUP_CHAT_COLUMN_CONTACT_NO + " text," +
                        "" + GROUP_CHAT_COLUMN_OFFICE_NO + " text," +
                        "" + GROUP_CHAT_COLUMN_POSITION + " text," +
                        "" + GROUP_CHAT_COLUMN_COUNTRY_IMAGE + " text," +
                        "" + GROUP_CHAT_COLUMN_OWNER + " text," +
                        "" + GROUP_CHAT_COLUMN_GROUP_ID + " text) "
        );

        db.execSQL(
                " CREATE VIEW IF NOT EXISTS " + GROUP_USER_TABLE_VIEW_NAME + " AS " +
                        " SELECT * FROM " + DBHelper.CHAT_TABLE_NAME + " LEFT OUTER JOIN " +
                        DBHelper.USER_CHAT_TABLE_NAME + " ON " + DBHelper.CHAT_TABLE_NAME + "." +
                        DBHelper.CHAT_COLUMN_FROM + "=" + DBHelper.USER_CHAT_TABLE_NAME + "." +
                        DBHelper.USER_CHAT_COLUMN_OX_USER
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + GROUP_TABLE_NAME +
                        "(" + GROUP_COLUMN_ID + " integer primary key," +
                        "" + GROUP_COLUMN_GROUP_ID + " text," +
                        "" + GROUP_COLUMN_NAME + " text," +
                        "" + GROUP_COLUMN_IS_DELETE + " text," +
                        "" + GROUP_COLUMN_ICON + " text," +
                        "" + GROUP_COLUMN_CREATE_BY + " text," +
                        "" + GROUP_COLUMN_CREATE_DATE + " text," +
                        "" + GROUP_COLUMN_UPDATE_BY + " text," +
                        "" + GROUP_COLUMN_UPDATE_DATE + " text) "
        );

        db.execSQL(
                " CREATE VIEW IF NOT EXISTS " + USER_TABLE_VIEW_NAME + " AS " +
                        "SELECT * FROM " + DBHelper.CHAT_LIST_TABLE_NAME + " LEFT OUTER JOIN " +
                        DBHelper.USER_CHAT_TABLE_NAME + " ON " + DBHelper.CHAT_LIST_TABLE_NAME + "."
                        + DBHelper.CHAT_LIST_COLUMN_JID +
                        " = " + DBHelper.USER_CHAT_TABLE_NAME + "."
                        + DBHelper.USER_CHAT_COLUMN_OX_USER + " LEFT OUTER JOIN " + DBHelper.GROUP_TABLE_NAME + " ON " + DBHelper.CHAT_LIST_TABLE_NAME + "." +
                        DBHelper.CHAT_LIST_COLUMN_GROUP_ID + "=" + DBHelper.GROUP_TABLE_NAME + "." + DBHelper.GROUP_COLUMN_GROUP_ID +
                        " GROUP BY " + DBHelper.CHAT_LIST_TABLE_NAME + "." + DBHelper.CHAT_LIST_COLUMN_NAME
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + MONTH_TABLE_NAME +
                        "(" + MONTH_COLUMN_ID + " integer primary key," +
                        "" + MONTH_COLUMN_SHOW_DATE + " text," +
                        "" + MONTH_COLUMN_B + " text," +
                        "" + MONTH_COLUMN_C + " text," +
                        "" + MONTH_COLUMN_L + " text," +
                        "" + MONTH_COLUMN_MONTH + " text," +
                        "" + MONTH_COLUMN_P + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + MONTH_CHECKING_TABLE_NAME +
                        "(" + MONTH_CHECKING_COLUMN_ID + " integer primary key," +
                        "" + MONTH_CHECKING_COLUMN_LEVEL + " text," +
                        "" + MONTH_CHECKING_COLUMN_LAST_UPDATE + " text," +
                        "" + MONTH_CHECKING_COLUMN_NO_OF_RECORDS + " text," +
                        "" + MONTH_CHECKING_COLUMN_MONTH + " text," +
                        "" + MONTH_CHECKING_COLUMN_NO_OF_PAGE + " text) "
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS " + CALENDAR_TABLE_NAME + "");
        db.execSQL("DROP TABLE IF EXISTS " + ATTENDEES_TABLE_NAME + "");
        db.execSQL("DROP TABLE IF EXISTS " + CALENDAR_CHECKING_TABLE_NAME + "");
        onCreate(db);
        if (oldVersion < newVersion) {
            updateDB(db);
        }
    }

    private void updateDB(SQLiteDatabase db) {
        try {
            // 12/1/2016 : Change from version 425 to 426
            if (!isFieldExist(db, CHAT_TABLE_NAME, CHAT_COLUMN_IS_SUCCESS)) {
                db.execSQL("ALTER TABLE " + CHAT_TABLE_NAME + " ADD COLUMN " + CHAT_COLUMN_IS_SUCCESS + " TEXT");
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
        try {
            // 12/1/2016 : Change from version 425 to 426
            if (!isFieldExist(db, CHAT_LIST_TABLE_NAME, CHAT_LIST_TABLE_NAME)) {
                db.execSQL("ALTER TABLE " + CHAT_LIST_TABLE_NAME + " ADD COLUMN " + CHAT_LIST_COLUMN_IS_SUCCESS + " TEXT");
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
        try {
            // 12/1/2016 : Change from version 425 to 426
            if (!isFieldExist(db, CALENDAR_TABLE_NAME, CALENDAR_COLUMN_SHOW_DATE)) {
                db.execSQL("ALTER TABLE " + CALENDAR_TABLE_NAME + " ADD COLUMN " + CALENDAR_COLUMN_SHOW_DATE + " DATETIME");
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
        try {
            // 12/1/2016 : Change from version 425 to 426
            if (!isFieldExist(db, CALENDAR_CHECKING_TABLE_NAME, CALENDAR_CHECKING_COLUMN_MONTH)) {
                db.execSQL("ALTER TABLE " + CALENDAR_CHECKING_TABLE_NAME + " ADD COLUMN " + CALENDAR_CHECKING_COLUMN_MONTH + " TEXT");
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }

        try {
            // 12/1/2016 : Change from version 425 to 426
            if (!isFieldExist(db, CALENDAR_TABLE_NAME, CALENDAR_COLUMN_MONTH)) {
                db.execSQL("ALTER TABLE " + CALENDAR_TABLE_NAME + " ADD COLUMN " + CALENDAR_COLUMN_MONTH + " TEXT");
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }

        try {
            // 12/1/2016 : Change from version 425 to 426
            if (!isFieldExist(db, ATTENDEES_TABLE_NAME, ATTENDEES_COLUMN_MONTH)) {
                db.execSQL("ALTER TABLE " + ATTENDEES_TABLE_NAME + " ADD COLUMN " + ATTENDEES_COLUMN_MONTH + " TEXT");
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
    }

    public boolean isFieldExist(SQLiteDatabase db, String tableName, String fieldName) {
        boolean isExist = false;
        Cursor res = null;
        try {
            res = db.rawQuery("PRAGMA TABLE_INFO(" + tableName + ")", null);
            for (int i = 0; i < res.getCount(); i++) {
                res.moveToPosition(i);
                if (res.getString(res.getColumnIndex("name")).equals(fieldName)) {
                    isExist = true;
                    break;
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (res != null) {
                res.close();
            }
        }
        return isExist;
    }
}