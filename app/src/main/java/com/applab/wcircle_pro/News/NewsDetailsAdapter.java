package com.applab.wcircle_pro.News;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CustomGridView;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

public class NewsDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private Cursor cursorNews, cursorImage;
    private LinearLayout btn, btnCancel, btnSubmit;
    private NewsImageAdapter newsImageAdapter;
    private FragmentManager fragmentManager;

    public NewsDetailsAdapter(Context context, Cursor cursorNews, Cursor cursorImage,
                              LinearLayout btn, LinearLayout btnCancel, LinearLayout btnSubmit, FragmentManager fragmentManager) {
        this.inflater = LayoutInflater.from(context);
        this.cursorNews = cursorNews;
        this.cursorImage = cursorImage;
        this.context = context;
        this.btn = btn;
        this.btnSubmit = btnSubmit;
        this.btnCancel = btnCancel;
        this.fragmentManager = fragmentManager;
    }

    public boolean getIsSelected() {
        boolean isSelect = false;
        if (newsImageAdapter != null) {
            isSelect = newsImageAdapter.getIsSelected();
        }
        return isSelect;
    }

    class NewsDetails extends RecyclerView.ViewHolder {
        TextView txtDesc;

        public NewsDetails(View itemView) {
            super(itemView);
            txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);
        }
    }

    class NewsGridImage extends RecyclerView.ViewHolder {
        CustomGridView gridView;

        public NewsGridImage(View itemView) {
            super(itemView);
            gridView = (CustomGridView) itemView.findViewById(R.id.grid_view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 1;
        if (position == 0) {
            viewType = 0;
        }
        return viewType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("NewsDetailsViewHolder", "onCreateViewHolder called");
        if (viewType == 0) {
            View view = inflater.inflate(R.layout.custom_news_description, parent, false);
            return new NewsDetails(view);
        } else {
            View view = inflater.inflate(R.layout.custom_news_image_recycler_view, parent, false);
            return new NewsGridImage(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);

        switch (holder.getItemViewType()) {
            case 0:
                if (position == 0) {
                    News news = getNewsData(cursorNews, position);
                    NewsDetails newsDetailsHolder = (NewsDetails) holder;
                    newsDetailsHolder.txtDesc.setText(news.getDescription());
                }
                break;
            case 1:
                if (position > 0) {
                    NewsGridImage newsGridImage = (NewsGridImage) holder;
                    newsImageAdapter = new NewsImageAdapter(context, cursorImage, btn, btnCancel, btnSubmit, fragmentManager);
                    newsGridImage.gridView.setAdapter(newsImageAdapter);
                }
                break;
        }
    }

    public void setOnSave(boolean isMoreThanZero) {
        if (isMoreThanZero) {
            newsImageAdapter.selectAll(false);
            newsImageAdapter.setVisibilityCheckBox(false);
            btn.setVisibility(View.GONE);
        } else {
            newsImageAdapter.selectAll(false);
            newsImageAdapter.setVisibilityCheckBox(false);
            btn.setVisibility(View.GONE);
        }
        newsImageAdapter.setIsSelected(false);
    }

    @Override
    public boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
        return true;
    }

    public News getNewsData(Cursor cursor, int position) {
        News news = new News();
        cursor.moveToPosition(position);
        news.setAuthor("By " + cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_AUTHOR)));
        news.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_DESCRIPTION)));
        news.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_TITLE)));
        return news;
    }


    @Override
    public int getItemCount() {
        int total = 0;
        if (cursorNews != null) {
            total += cursorNews.getCount();
            if (cursorImage != null) {
                total += 1;
            }
        }
        return total > 2 ? 2 : total;
    }

    public Cursor swapNewsCursor(Cursor cursor) {
        if (this.cursorNews == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursorNews;
        this.cursorNews = cursor;
        if (cursor != null) {
            android.os.Message msg = handler1.obtainMessage();
            handler1.handleMessage(msg);
        }
        return oldCursor;
    }

    public Cursor swapNewsImageCursor(Cursor cursor) {
        if (this.cursorImage == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursorImage;
        this.cursorImage = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            NewsDetailsAdapter.this.notifyDataSetChanged();
        }
    };

    Handler handler1 = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            NewsDetailsAdapter.this.notifyDataSetChanged();
        }
    };
}
