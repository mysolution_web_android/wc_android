package com.applab.wcircle_pro.News;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

public class NewsDetailsActivity extends AppCompatActivity
        implements SwipyRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {
    private ImageView btnHome;
    private TextView txtTitle, txtAuthorDate, txtError;
    private RecyclerView recyclerView;
    private NewsDetailsAdapter adapter;
    private Cursor cursorNews, cursorImage;
    private LinearLayoutManager linearLayoutManager;
    private Bundle bundle;
    private int id = 0;
    private String TAG = "NEWS";
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private LinearLayout btn, btnCancel, btnSubmit;
    public static String ACTION = "NEWS DOWNLOAD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtAuthorDate = (TextView) findViewById(R.id.txtAuthorDate);
        bundle = getIntent().getExtras();
        if (bundle != null) id = bundle.getInt("id");
        btn = (LinearLayout) findViewById(R.id.btn);
        btnSubmit = (LinearLayout) findViewById(R.id.btnSubmit);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new NewsDetailsAdapter(NewsDetailsActivity.this, cursorNews, cursorImage, btn, btnCancel, btnSubmit, getSupportFragmentManager());
        recyclerView.setAdapter(adapter);
        txtError = (TextView) findViewById(R.id.txtError);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        btnHome = (ImageView) findViewById(R.id.btnHome);
        btnHome.setOnClickListener(btnHomeOnClickListener);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        HttpHelper.getSingleNewsGson(NewsDetailsActivity.this, TAG, id, txtError);
        getSupportLoaderManager().initLoader(0, null, this);
        getSupportLoaderManager().initLoader(1, null, this);
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (firstVisibleItem == 0) {
                        if (btn.getVisibility() == View.GONE) {
                            mSwipyRefreshLayout.setEnabled(true);
                            mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                        } else {
                            mSwipyRefreshLayout.setEnabled(false);
                        }
                    }
                }
            }
        }
    };

    private LinearLayout.OnClickListener btnHomeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(NewsDetailsActivity.this, NewsActivity.class));
            finish();
        }
    };

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, NewsActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_news_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);

    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
        if (Utils.getStatus(Utils.NEWS_READ, NewsDetailsActivity.this)) {
            HttpHelper.getNewsGsonReading(NewsDetailsActivity.this, TAG, id);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 0) {
            String selection = DBHelper.NEWS_COLUMN_NEWS_ID + "=?";
            String[] selecitonArgs = {String.valueOf(this.id)};
            return new CursorLoader(getBaseContext(),
                    NewsProvider.CONTENT_URI, null, selection, selecitonArgs, null);
        } else if (id == 1) {
            String selection = DBHelper.NEWS_IMAGE_COLUMN_NEWS_ID + "=?";
            String[] selecitonArgs = {String.valueOf(this.id)};
            return new CursorLoader(getBaseContext(),
                    NewsImageProvider.CONTENT_URI, null, selection, selecitonArgs, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 0) {
            this.cursorNews = data;
            adapter.swapNewsCursor(cursorNews);
            data.moveToFirst();
            txtTitle.setText(data.getString(data.getColumnIndex(DBHelper.NEWS_COLUMN_TITLE)));
            String uploadedDate = data.getString(data.getColumnIndex(DBHelper.NEWS_COLUMN_CREATE_DATE));
            uploadedDate = Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", uploadedDate);
            txtAuthorDate.setText(getResources().getString(R.string.by) + " " + data.getString(data.getColumnIndex(DBHelper.NEWS_COLUMN_AUTHOR)) + " " + getString(R.string.on) + " " + uploadedDate);
        } else if (loader.getId() == 1) {
            this.cursorImage = data;
            adapter.swapNewsImageCursor(cursorImage);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            HttpHelper.getSingleNewsGson(NewsDetailsActivity.this, TAG, id, txtError);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                boolean isYes = intent.getBooleanExtra("isYes", false);
                boolean isSelectedMoreThanZero = intent.getBooleanExtra("isSelectedMoreThanZero", false);
                if (isYes) {
                    adapter.setOnSave(isSelectedMoreThanZero);
                }
            }
        }
    };
}
