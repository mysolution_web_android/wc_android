package com.applab.wcircle_pro.News;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CustomGridView;
import com.applab.wcircle_pro.Utils.DBHelper;

public class CustomNewsListAdapter extends BaseAdapter {
    private Context context;
    private static LayoutInflater inflater = null;
    private Cursor cursorNews, cursorImage;

    public CustomNewsListAdapter(Context context, Cursor cursorNews, Cursor cursorImage) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.cursorImage = cursorImage;
        this.cursorNews = cursorNews;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        int total = 0;
        if (cursorNews != null) {
            total += cursorNews.getCount();
        }

        if (cursorImage != null) {
            total += 1;
        }
        return total;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        int type = getItemViewType(position);
        NewsDetailsHolder newsDetailsHolder = new NewsDetailsHolder();
        NewsGridImageHolder newsGridImageHolder = new NewsGridImageHolder();
        if (row == null) {
            if (type == 0) {
                row = inflater.inflate(R.layout.custom_news_description, parent, false);
            } else {
                row = inflater.inflate(R.layout.custom_news_image_recycler_view, parent, false);
            }
        }
        if (position == 0) {
            News news = getNewsData(cursorNews, position);
            newsDetailsHolder.txtDesc = (TextView) row.findViewById(R.id.txtDesc);
            newsDetailsHolder.txtDesc.setText(news.getDescription());
        }else{
            newsGridImageHolder.gridView = (CustomGridView) row.findViewById(R.id.grid_view);
//            newsGridImageHolder.gridView.setAdapter(new NewsImageAdapter(context, cursorImage, newsGridImageHolder.gridView));
        }
        return row;
    }

    public News getNewsData(Cursor cursor, int position) {
        News news = new News();
        cursor.moveToPosition(position);
        news.setAuthor("By " + cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_AUTHOR)));
        news.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_DESCRIPTION)));
        news.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_TITLE)));
        return news;
    }

    class NewsDetailsHolder {
        TextView txtDesc;
    }

    class NewsGridImageHolder {
        CustomGridView gridView;
    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 1;
        if (position == 0) {
            viewType = 0;
        }
        return viewType;
    }

    public Cursor swapNewsCursor(Cursor cursor) {
        if (this.cursorNews == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursorNews;
        this.cursorNews = cursor;
        if (cursor != null) {
            this.notifyDataSetChanged();
        }
        return oldCursor;
    }

    public Cursor swapNewsImageCursor(Cursor cursor) {
        if (this.cursorImage == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursorImage;
        this.cursorImage = cursor;
        if (cursor != null) {
            this.notifyDataSetChanged();
        }
        return oldCursor;
    }
}