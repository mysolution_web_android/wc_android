package com.applab.wcircle_pro.News;

import com.google.gson.annotations.Expose;

public class NewsImage {

    @Expose
    private Integer Id;
    @Expose
    private Integer NewsId;
    @Expose
    private String Description;
    @Expose
    private String Image;
    @Expose
    private String ImageThumb;
    @Expose
    private String UploadBy;
    @Expose
    private String UploadDate;

    /**
     *
     * @return
     * The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The NewsId
     */
    public Integer getNewsId() {
        return NewsId;
    }

    /**
     *
     * @param NewsId
     * The NewsId
     */
    public void setNewsId(Integer NewsId) {
        this.NewsId = NewsId;
    }

    /**
     *
     * @return
     * The Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     *
     * @param Description
     * The Description
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     *
     * @return
     * The ImageThumb
     */
    public String getImageThumb() {
        return ImageThumb;
    }

    /**
     *
     * @param ImageThumb
     * The Image
     */
    public void setImageThumb(String ImageThumb) {
        this.ImageThumb = ImageThumb;
    }

    /**
     *
     * @return
     * The Image
     */
    public String getImage() {
        return Image;
    }

    /**
     *
     * @param Image
     * The Image
     */
    public void setImage(String Image) {
        this.Image = Image;
    }

    /**
     *
     * @return
     * The UploadBy
     */
    public String getUploadBy() {
        return UploadBy;
    }

    /**
     *
     * @param UploadBy
     * The UploadBy
     */
    public void setUploadBy(String UploadBy) {
        this.UploadBy = UploadBy;
    }

    /**
     *
     * @return
     * The UploadDate
     */
    public String getUploadDate() {
        return UploadDate;
    }

    /**
     *
     * @param UploadDate
     * The UploadDate
     */
    public void setUploadDate(String UploadDate) {
        this.UploadDate = UploadDate;
    }

}