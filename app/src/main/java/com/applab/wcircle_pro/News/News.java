package com.applab.wcircle_pro.News;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class News {

    @Expose
    private String Id;
    @Expose
    private String Title;
    @Expose
    private String Description;
    @Expose
    private String Author;
    @Expose
    private String CreatedDate;
    @Expose
    private List<NewsImage> Images = new ArrayList<NewsImage>();
    @Expose
    private Integer NoOfImage;
    @Expose
    private String Message;
    @Expose
    private boolean NewCreate = false;

    public boolean getNewCreate() {
        return NewCreate;
    }

    public void setNewCreate(boolean newCreate) {
        NewCreate = newCreate;
    }

    /**
     * @return The Message
     */
    public String getMessage() {
        return Message;
    }

    /**
     * @param Message The Message
     */
    public void setMessage(String Message) {
        this.Message = Message;
    }

    /**
     * @return The NoOfImage
     */
    public Integer getNoOfImage() {
        return NoOfImage;
    }

    /**
     * @param NoOfImage The NoOfImage
     */
    public void setNoOfImage(Integer NoOfImage) {
        this.NoOfImage = NoOfImage;
    }

    /**
     * @return The Images
     */
    public List<NewsImage> getNewsImages() {
        return Images;
    }

    /**
     * @param Images The Images
     */
    public void setNewsImages(List<NewsImage> Images) {
        this.Images = Images;
    }

    /**
     * @return The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * @return The Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param Title The Title
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * @return The Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description The Description
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return The Author
     */
    public String getAuthor() {
        return Author;
    }

    /**
     * @param Author The Author
     */
    public void setAuthor(String Author) {
        this.Author = Author;
    }

    /**
     * @return The CreatedDate
     */
    public String getCreatedDate() {
        return CreatedDate;
    }

    /**
     * @param CreatedDate The CreatedDate
     */
    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

}
