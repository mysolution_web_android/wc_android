package com.applab.wcircle_pro.News;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class NewsChecking {

    @Expose
    private String LastUpdate;
    @Expose
    private Integer NoOfRecords;
    @Expose
    private Integer NoOfPage;
    @Expose
    private List<News> items = new ArrayList<News>();
    @Expose
    private String Message;

    /**
     * @return The Message
     */
    public String getMessage() {
        return Message;
    }

    /**
     * @param Message The Message
     */
    public void setMessage(String Message) {
        this.Message = Message;
    }

    /**
     *
     * @return
     * The LastUpdate
     */
    public String getLastUpdate() {
        return LastUpdate;
    }

    /**
     *
     * @param LastUpdate
     * The LastUpdate
     */
    public void setLastUpdate(String LastUpdate) {
        this.LastUpdate = LastUpdate;
    }

    /**
     *
     * @return
     * The NoOfRecords
     */
    public Integer getNoOfRecords() {
        return NoOfRecords;
    }

    /**
     *
     * @param NoOfRecords
     * The NoOfRecords
     */
    public void setNoOfRecords(Integer NoOfRecords) {
        this.NoOfRecords = NoOfRecords;
    }

    /**
     *
     * @return
     * The NoOfPage
     */
    public Integer getNoOfPage() {
        return NoOfPage;
    }

    /**
     *
     * @param NoOfPage
     * The NoOfPage
     */
    public void setNoOfPage(Integer NoOfPage) {
        this.NoOfPage = NoOfPage;
    }

    /**
     *
     * @return
     * The News
     */
    public List<News> getNews() {
        return items;
    }

    /**
     *
     * @param items
     * The News
     */
    public void setNews(List<News> items) {
        this.items = items;
    }

}
