package com.applab.wcircle_pro.News;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by user on 5/2/2015.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;

    public NewsAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        ;
        this.cursor = cursor;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("NewsViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_news_row, parent, false);
        NewsViewHolder holder = new NewsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        News news = getData(cursor, position);
        holder.title.setText(news.getTitle());
        holder.title.setTypeface(Typeface.defaultFromStyle(news.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        holder.desc.setText(news.getDescription());
        holder.desc.setTypeface(Typeface.defaultFromStyle(news.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        holder.date.setText(news.getCreatedDate());
        holder.date.setTypeface(Typeface.defaultFromStyle(news.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        holder.name.setText(news.getAuthor());
        holder.name.setTypeface(Typeface.defaultFromStyle(news.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
    }

    public News getData(Cursor cursor, int position) {
        News news = new News();
        cursor.moveToPosition(position);
        news.setAuthor("By " + cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_AUTHOR)));
        news.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_DESCRIPTION)));
        news.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_TITLE)));
        String createdDate = cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_CREATE_DATE));
        news.setCreatedDate(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", createdDate));
        news.setNewCreate(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_NEW_CREATE))));
        return news;
    }

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            NewsAdapter.this.notifyDataSetChanged();
        }
    };
}
