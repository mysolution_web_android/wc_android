package com.applab.wcircle_pro.News;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/2/2015.
 */
public class NewsViewHolder extends RecyclerView.ViewHolder {
    TextView title;
    TextView desc;
    TextView name;
    TextView date;

    public NewsViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.txtTitle);
        desc = (TextView) itemView.findViewById(R.id.txtDesc);
        name = (TextView) itemView.findViewById(R.id.txtName);
        date = (TextView) itemView.findViewById(R.id.txtDate);
    }

}
