package com.applab.wcircle_pro.News;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Dashboard.DashboardProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by user on 23/11/2015.
 */
public class HttpHelper {
    //region news region
    public static void getNewsGson(Context context, final int pageNo, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        final Date lastUpdateDate = getNewsLastUpdate(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/News?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                + Utils.encode(pageNo) + "&LastUpdated=" + Utils.encode(lastUpdate) +
                "&ReturnEmpty=" + Utils.encode(getNewsRow(context, pageNo) > 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/News?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                    + Utils.encode(pageNo) + "&LastUpdated=" + Utils.encode(lastUpdate)
                    + "&ReturnEmpty=" + String.valueOf(getNewsRow(context, pageNo) > 0);
        }
        final String finalUrl = url;
        Log.i(TAG, token);
        Log.i(TAG, url);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<NewsChecking> mGsonRequest = new GsonRequest<NewsChecking>(
                Request.Method.GET,
                finalUrl,
                NewsChecking.class,
                headers,
                responseNewsListener(context, pageNo, txtError, TAG),
                errorNewsListener(context, txtError)) {

        };
        Log.i(TAG, "URL: " + url);
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<NewsChecking> responseNewsListener(final Context context, final int pageNo, final TextView txtError, final String TAG) {
        return new Response.Listener<NewsChecking>() {
            @Override
            public void onResponse(NewsChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getNews().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getNewsLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(NewsProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(NewsCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(NewsCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertNews(response, context, pageNo, TAG);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(NewsProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(NewsCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(NewsCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertNews(response, context, pageNo, TAG);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(NewsProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(NewsCheckingProvider.CONTENT_URI, null, null);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(NewsCheckingProvider.CONTENT_URI, null, null);
                                }
                                insertNews(response, context, pageNo, TAG);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(NewsProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(NewsCheckingProvider.CONTENT_URI, null, null);
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    public static Response.ErrorListener errorNewsListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }

    public static int getNewsRow(Context context, int pageNo) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(NewsProvider.CONTENT_URI,
                    null, DBHelper.NEWS_COLUMN_LEVEL + "=?",
                    new String[]{String.valueOf(String.valueOf(pageNo))}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getNewsLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.NEWS_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(NewsCheckingProvider.CONTENT_URI,
                    projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static boolean isNewsExists(Context context, Object newsId) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(NewsProvider.CONTENT_URI, null,
                    DBHelper.NEWS_COLUMN_NEWS_ID + "=?", new String[]{String.valueOf(newsId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void insertNews(NewsChecking result, Context context, int pageNo, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesNewsChecking = new ContentValues();
            contentValuesNewsChecking.put(DBHelper.NEWS_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesNewsChecking.put(DBHelper.NEWS_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesNewsChecking.put(DBHelper.NEWS_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesNewsChecking.put(DBHelper.NEWS_CHECKING_COLUMN_LEVEL, pageNo);
            context.getContentResolver().insert(NewsCheckingProvider.CONTENT_URI, contentValuesNewsChecking);
            ContentValues[] contentValueses = new ContentValues[result.getNews().size()];
            for (int i = 0; i < result.getNews().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.NEWS_COLUMN_NEWS_ID, result.getNews().get(i).getId());
                contentValues.put(DBHelper.NEWS_COLUMN_TITLE, result.getNews().get(i).getTitle());
                contentValues.put(DBHelper.NEWS_COLUMN_LEVEL, pageNo);
                contentValues.put(DBHelper.NEWS_COLUMN_DESCRIPTION, result.getNews().get(i).getDescription());
                contentValues.put(DBHelper.NEWS_COLUMN_AUTHOR, result.getNews().get(i).getAuthor());
                contentValues.put(DBHelper.NEWS_COLUMN_CREATE_DATE, result.getNews().get(i).getCreatedDate());
                contentValues.put(DBHelper.NEWS_COLUMN_NO_OF_IMAGE, result.getNews().get(i).getNoOfImage());
                contentValues.put(DBHelper.NEWS_COLUMN_NEW_CREATE, String.valueOf(result.getNews().get(i).getNewCreate()));
                if (isNewsExists(context, result.getNews().get(i).getId())) {
                    context.getContentResolver().delete(NewsProvider.CONTENT_URI, DBHelper.NEWS_COLUMN_NEWS_ID + "=?",
                            new String[]{String.valueOf(result.getNews().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(NewsProvider.CONTENT_URI, contentValueses);
            }

        } catch (Exception ex) {
            Log.i(TAG, "Content Provider News Error :" + ex.toString());
        }
    }
    //endregion

    //region Single News
    public static void getSingleNewsGson(Context context, String TAG, int id, TextView txtError) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        Log.i(TAG, Utils.getToken(context));
        Log.i(TAG, Utils.API_URL + "api/News/" + Utils.encode(id));
        GsonRequest<News> mGsonRequest = new GsonRequest<News>(
                Request.Method.GET,
                Utils.API_URL + "api/News/" + Utils.encode(id),
                News.class,
                headers,
                responseNewsListener(context, txtError, id, TAG),
                errorSingleNewsListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<News> responseNewsListener(final Context context, final TextView txtError, final int id, final String TAG) {
        return new Response.Listener<News>() {
            @Override
            public void onResponse(News response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        context.getContentResolver().delete(NewsImageProvider.CONTENT_URI, DBHelper.NEWS_IMAGE_COLUMN_NEWS_ID + "=?", new String[]{String.valueOf(id)});
                        insertNews(response, context, TAG);
                    }
                }
            }
        };
    }

    public static void insertNews(News result, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" + context.getResources().getString(R.string.news_gallery));
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.NEWS_COLUMN_NEWS_ID, result.getId());
            contentValues.put(DBHelper.NEWS_COLUMN_TITLE, result.getTitle());
            contentValues.put(DBHelper.NEWS_COLUMN_DESCRIPTION, result.getDescription());
            contentValues.put(DBHelper.NEWS_COLUMN_AUTHOR, result.getAuthor());
            contentValues.put(DBHelper.NEWS_COLUMN_NO_OF_IMAGE, result.getNoOfImage());
            contentValues.put(DBHelper.NEWS_COLUMN_CREATE_DATE, result.getCreatedDate());
            contentValues.put(DBHelper.NEWS_COLUMN_NEW_CREATE, String.valueOf(result.getNewCreate()));
            if (!isNewsExists(context, result.getId())) {
                Uri uri = context.getContentResolver().insert(NewsProvider.CONTENT_URI, contentValues);
                long rowId = ContentUris.parseId(uri);
                Log.i(TAG, String.valueOf(rowId));
            } else {
                context.getContentResolver().update(NewsProvider.CONTENT_URI, contentValues,
                        DBHelper.NEWS_COLUMN_NEWS_ID + "=?", new String[]{String.valueOf(result.getId())});
            }

            ContentValues[] contentValuesesImage = new ContentValues[result.getNewsImages().size()];
            for (int i = 0; i < result.getNewsImages().size(); i++) {
                ContentValues contentValuesImage = new ContentValues();
                contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_IMAGE_ID, result.getNewsImages().get(i).getId());
                contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_DESCRIPTION, result.getNewsImages().get(i).getDescription());
                contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_IMAGE, result.getNewsImages().get(i).getImage());
                contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_IMAGE_THUMB, result.getNewsImages().get(i).getImageThumb());
                contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_NEWS_ID, result.getNewsImages().get(i).getNewsId());
                contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_UPLOAD_BY, result.getNewsImages().get(i).getUploadBy());
                contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_UPLOAD_DATE, result.getNewsImages().get(i).getUploadDate());
                String[] split = result.getNewsImages().get(i).getImage().split("/");
                contentValues.put(DBHelper.NEWS_IMAGE_COLUMN_PATH, file.getAbsolutePath() + "/" + split[split.length - 1]);
                contentValuesesImage[i] = contentValuesImage;
            }
            boolean isEmpty = true;
            for (ContentValues contentValuesImage : contentValuesesImage) {
                if (contentValuesImage != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(NewsImageProvider.CONTENT_URI, contentValuesesImage);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider News Error :" + ex.toString());
        }
    }

    public static Response.ErrorListener errorSingleNewsListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }
    //endregion

    //region News Reading
    public static void getNewsGsonReading(final Context context, final String TAG, final int id) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseNewsReadingListenerReading(context, id, TAG),
                errorNewsReadingListenerReading(TAG)) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" +
                        Utils.getProfile(context).getId() +
                        ",\"section\":\"news\",\"action\":\"read\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseNewsReadingListenerReading(final Context context, final int id, final String TAG) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ContentValues contentValues = new ContentValues();

                if (isBold(context, id)) {
                    contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, DashboardActivity.getNumber(context, "News"));
                    context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                            contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"News"});
                    com.applab.wcircle_pro.Dashboard.HttpHelper.getIndicator(context, TAG, null);
                }

                contentValues = new ContentValues();
                contentValues.put(DBHelper.NEWS_COLUMN_NEW_CREATE, "false");
                context.getContentResolver().update(NewsProvider.CONTENT_URI, contentValues, DBHelper.NEWS_COLUMN_NEWS_ID + "=?", new String[]{String.valueOf(id)});


                Log.i(TAG, "Success send reading status");

            }
        };
    }

    public static boolean isBold(Context context, Object id) {
        boolean isBold = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(NewsProvider.CONTENT_URI, null, DBHelper.NEWS_COLUMN_NEWS_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                if (!cursor.isNull(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_NEW_CREATE))) {
                    isBold = Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_NEW_CREATE)));
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isBold;
    }

    public static Response.ErrorListener errorNewsReadingListenerReading(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }
    //endregion

    //region Download
    public static void getGsonDownload(final String id, final String TAG, final Context context) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseListenerDownload(TAG),
                errorListenerDownload(TAG)) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" + Utils.getProfile(context).getId()
                        + ",\"section\":\"news\",\"action\":\"download\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerDownload(final String TAG) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Success send download status");
            }
        };
    }

    public static Response.ErrorListener errorListenerDownload(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }
}
