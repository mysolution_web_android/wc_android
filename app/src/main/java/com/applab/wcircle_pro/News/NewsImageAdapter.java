package com.applab.wcircle_pro.News;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.applab.wcircle_pro.Gallery.ImageTag;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import java.util.ArrayList;

/**
 * Created by user on 5/2/2015.
 */
import android.widget.BaseAdapter;

public class NewsImageAdapter extends BaseAdapter {
    private Context context;
    private Cursor cursor;
    private LayoutInflater inflater = null;
    private ArrayList<String> arrUrl = new ArrayList<>();
    private ArrayList<String> arrId = new ArrayList<>();
    private String TAG = "NEWS";
    private String url = Utils.API_URL + "api/Tracking";
    private boolean isSelected = false;
    private int selected = 0;
    private LinearLayout btn, btnCancel, btnSubmit;
    private ArrayList<ImageTag> arrTag = new ArrayList<>();
    private boolean isVisible = false;
    private boolean isSelect = false;
    private FragmentManager fragmentManager;

    public NewsImageAdapter(Context context, Cursor cursor,
                            LinearLayout btn,
                            LinearLayout btnCancel, LinearLayout btnSubmit, FragmentManager fragmentManager) {
        this.context = context;
        this.cursor = cursor;
        this.btn = btn;
        this.btnSubmit = btnSubmit;
        this.btnCancel = btnCancel;
        this.fragmentManager = fragmentManager;
        setCheckBoxStatus();
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setSelected(int selected) {
        this.selected = selected;
        this.notifyDataSetChanged();
    }

    public boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public void setVisibilityCheckBox(boolean isVisible) {
        this.isVisible = isVisible;
        this.notifyDataSetChanged();
    }

    public void selectAll(boolean isSelect) {
        this.isSelect = isSelect;
    }

    private void setCheckBoxStatus() {
        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToFirst();
            arrTag.clear();
            do {
                ImageTag tag = new ImageTag();
                tag.status = false;
                arrTag.add(tag);
            } while (cursor.moveToNext());
        }
    }

    private View.OnClickListener btnCancelOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isSelected) {
                isSelected = false;
                setIsSelected(isSelected);
                selectAll(false);
                setVisibilityCheckBox(false);
                btn.setVisibility(View.GONE);
            }
            setCheckBoxStatus();
        }
    };

    private View.OnClickListener btnSaveOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isSelected && selected > 0 && arrUrl.size() > 0) {
                String message = "Do you want to save the images?" +
                        " (News images will be saved at " + context.getString(R.string.folder_name) + "/" + context.getString(R.string.news_gallery) + " folder)";
                DownloadNewsDialogFragment newsDialogFragment = DownloadNewsDialogFragment.newInstance(context, message, arrUrl, arrId, selected);
                newsDialogFragment.show(fragmentManager, "");
            } else if (selected == 0 && arrUrl.size() == 0) {
                isSelected = false;
                setIsSelected(isSelected);
                selectAll(false);
                setVisibilityCheckBox(false);
                btn.setVisibility(View.GONE);
            }
            setCheckBoxStatus();
        }
    };


    @Override
    public int getCount() {
        return cursor == null ? 0 : cursor.getCount();
    }

    @Override
    public Object getItem(int position) {
        return cursor.moveToPosition(position);
    }

    @Override
    public long getItemId(int position) {
        cursor.moveToPosition(position);
        return cursor.getInt(cursor.getColumnIndex(DBHelper.NEWS_IMAGE_COLUMN_ID));
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        Holder holder = new Holder();
        final NewsImage current = getData(cursor, position);
        if (row == null) {
            row = inflater.inflate(R.layout.custom_news_image, parent, false);
            holder.imgNews = (ImageView) row.findViewById(R.id.imgNews);
            holder.checkBox = (CheckBox) row.findViewById(R.id.ckImage);
            holder.progressBar = (ProgressBar) row.findViewById(R.id.progressBar);
            row.setTag(holder);
        } else {
            holder = (Holder) row.getTag();
        }
        ImageTag tag = new ImageTag();
        tag.url = current.getImage();
        tag.id = String.valueOf(current.getId());
        holder.checkBox.setTag(tag);
        holder.checkBox.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
        holder.checkBox.setChecked(isSelect);
        final CheckBox checkBox = holder.checkBox;
        final ProgressBar progress = holder.progressBar;
        Glide.with(context)
                .load(current.getImageThumb())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new GlideDrawableImageViewTarget(holder.imgNews) {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);
                        progress.setVisibility(View.GONE);
                    }
                });
        holder.imgNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isSelected) {
                    Intent intent = new Intent(context, NewsImageSlidingActivity.class);
                    intent.putExtra("id", String.valueOf(current.getNewsId()));
                    intent.putExtra("position", position);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    ImageTag imageTag = (ImageTag) checkBox.getTag();
                    if (checkBox.isChecked()) {
                        checkBox.setChecked(false);
                        arrTag.get(position).status = false;
                        if (selected > 0) {
                            selected -= 1;
                            setSelected(selected);
                        }
                        if (arrUrl.contains(imageTag.url)) {
                            arrUrl.remove(imageTag.url);
                        }

                        if (arrId.contains(imageTag.id)) {
                            arrUrl.remove(imageTag.id);
                        }
                    } else {
                        checkBox.setChecked(true);
                        arrTag.get(position).status = true;
                        selected += 1;
                        setSelected(selected);
                        arrUrl.add(imageTag.url);
                        arrId.add(imageTag.id);
                        Log.i(TAG, "Id size: " + String.valueOf(arrId.size()));
                        Log.i(TAG, "Url Size: " + String.valueOf(arrUrl.size()));
                    }
                }
            }
        });

        holder.imgNews.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if (!isSelected) {
                    isSelected = true;
                    setIsSelected(isSelected);
                    selected++;
                    setSelected(selected);
                    btn.setVisibility(View.VISIBLE);
                    setVisibilityCheckBox(true);
                    checkBox.setChecked(true);
                    arrTag.get(position).status = true;
                    Utils.vibrate(250, context);
                    arrId.clear();
                    arrUrl.clear();
                    ImageTag imageTag = (ImageTag) checkBox.getTag();
                    arrUrl.add(imageTag.url);
                    arrId.add(imageTag.id);
                }
                return true;
            }
        });
        holder.checkBox.setChecked(arrTag.get(position).status);
        btnSubmit.setOnClickListener(btnSaveOnClick);
        btnCancel.setOnClickListener(btnCancelOnClick);
        return row;
    }


    public class Holder {
        ImageView imgNews;
        CheckBox checkBox;
        ProgressBar progressBar;
    }

    public NewsImage getData(Cursor cursor, int position) {
        NewsImage current = new NewsImage();
        cursor.moveToPosition(position);
        current.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.NEWS_IMAGE_COLUMN_IMAGE_ID)));
        current.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_IMAGE_COLUMN_DESCRIPTION)));
        current.setImage(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_IMAGE_COLUMN_IMAGE)));
        current.setImageThumb(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_IMAGE_COLUMN_IMAGE_THUMB)));
        current.setNewsId(cursor.getInt(cursor.getColumnIndex(DBHelper.NEWS_IMAGE_COLUMN_NEWS_ID)));
        current.setUploadBy(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_IMAGE_COLUMN_UPLOAD_BY)));
        current.setUploadDate(cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_IMAGE_COLUMN_UPLOAD_DATE)));
        return current;
    }
}