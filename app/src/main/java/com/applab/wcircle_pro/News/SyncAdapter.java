package com.applab.wcircle_pro.News;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.applab.wcircle_pro.R;

import java.util.ArrayList;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {
    private ArrayList<News> arrNews;

    // Interval at which to sync with the contact, in seconds
    // 60 seconds (1min) * 180 = 3 hours
    public static final int SYNC_INTERVAL = 60 * 180;
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 3;
    // Global variables
    // Define a variable to contain a content resolver instance
    ContentResolver mContentResolver;

    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
        arrNews = new ArrayList<>();
    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
        arrNews = new ArrayList<>();

    }

    /*
     * Specify the code you want to run in the sync adapter. The entire
     * sync adapter runs in a background thread, so you don't have to set
     * up your own background processing.
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider,
                              SyncResult syncResult) {
    /*
     * Put the data transfer code here.
     */
        String TAG = "SYNC";
        Log.i(TAG, "Beginning network syncronization");
//        insertNews(arrNews);
//        volleyData ();
    }

    public static void initializeSyncAdapter(Context context) {
        getSyncAccount(context);
    }


//    public void volleyData (){
//        final String TAG = "INSERT_NEWS";
//        String url = "http://192.168.72.50/api/News";
//        final String token = getToken();
//        Log.i(TAG,token);
//        HashMap<String, String> headers = new HashMap<String, String>();
//        headers.put("Authorization", "Bearer " + token);
//        GsonRequest<News[]> mGsonRequest = new GsonRequest<News[]>(
//                Request.Method.GET,
//                url,
//                News[].class,
//                headers,
//                responseListener(),
//                errorListener()){
//        };
//        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
//    }
//
//    private Response.Listener<News[]> responseListener() {
//        return new Response.Listener<News[]>() {
//            @Override
//            public void onResponse(News[] response) {
//                String TAG = "INSERT_NEWS";
//                insertNews(response);
//            };
//        };
//    }
//
//    private Response.ErrorListener errorListener() {
//        return new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                String TAG = "INSERT_NEWS";
//                String errorMsg = error.getMessage();
//                VolleyLog.d(TAG, errorMsg);
//            }
//        };
//    }

//    public void insertNews (News[] result ){
//        String TAG = "INSERT_NEWS";
//        DBHelper helper = new DBHelper(getContext());
//        ArrayList<News> arrNews = new ArrayList<News>(Arrays.asList(result));
//        try{
//            ContentValues[] contentValueses= new ContentValues[arrNews.size()];
//            String id ="";
//            if(getId()!=null){
//                if(!getId().equals("")){
//                    id = getId();
//                }
//            }
//            for (int i = 0; i < arrNews.size(); i++){
//                ContentValues contentValues = new ContentValues();
//                if(!id.equals("")) {
//                    if (Integer.valueOf(arrNews.get(i).Id) > Integer.valueOf(id)) {
//                        contentValues.put(helper.NEWS_COLUMN_ID, arrNews.get(i).Id);
//                        contentValues.put(helper.NEWS_COLUMN_TITLE, arrNews.get(i).Title);
//                        contentValues.put(helper.NEWS_COLUMN_DESCRIPTION, arrNews.get(i).Description);
//                        contentValues.put(helper.NEWS_COLUMN_AUTHOR, arrNews.get(i).Author);
//                        contentValues.put(helper.NEWS_COLUMN_CREATE_DATE, arrNews.get(i).CreatedDate);
//                        contentValueses[i] = contentValues;
//                    }
//                    else{
//                        contentValueses = null;
//                    }
//                }
//                else{
//                    contentValues.put(helper.NEWS_COLUMN_ID, arrNews.get(i).Id);
//                    contentValues.put(helper.NEWS_COLUMN_TITLE, arrNews.get(i).Title);
//                    contentValues.put(helper.NEWS_COLUMN_DESCRIPTION, arrNews.get(i).Description);
//                    contentValues.put(helper.NEWS_COLUMN_AUTHOR, arrNews.get(i).Author);
//                    contentValues.put(helper.NEWS_COLUMN_CREATE_DATE, arrNews.get(i).CreatedDate);
//                    contentValueses[i] = contentValues;
//                }
//                long rowsInserted=0;
//                if(contentValueses!=null){
//                    rowsInserted = getContext().getContentResolver().bulkInsert(NewsProvider.CONTENT_URI, contentValueses);
//                    Log.i(TAG,String.valueOf(rowsInserted));
//                }
//            }
//
//        }catch(Exception ex){
//            Log.i(TAG,"Content Provider Error :"+ex.toString());
//        }
//    }
//
//    public String getId(){
//        String id = "";
//        Uri uri = NewsProvider.CONTENT_URI;
//        String[] projection = { DBHelper.NEWS_COLUMN_ID,DBHelper.NEWS_COLUMN_TITLE,
//                DBHelper.NEWS_COLUMN_DESCRIPTION,DBHelper.NEWS_COLUMN_AUTHOR,
//                DBHelper.NEWS_COLUMN_CREATE_DATE};
//        Cursor cursor = getContext().getContentResolver().query(uri, projection,
//                null, null, DBHelper.NEWS_COLUMN_ID + " DESC Limit 1 ");
//        cursor.moveToFirst();
//        if( cursor != null && cursor.moveToFirst() ){
//            id= cursor.getString(cursor.getColumnIndex(DBHelper.NEWS_COLUMN_ID));
//            cursor.close();
//        }
//        return id;
//    }
//
//    public String getToken(){
//        String accessToken = "";
//        Uri uri = TokenProvider.CONTENT_URI;
//        String[] projection = {DBHelper.TOKEN_COLUMN_ID,DBHelper.TOKEN_COLUMN_ACCESS_TOKEN};
//        Cursor cursor = getContext().getContentResolver().query(uri, projection,
//                null, null, DBHelper.TOKEN_COLUMN_ID + " DESC Limit 1 ");
//        cursor.moveToFirst();
//        if( cursor != null && cursor.moveToFirst() ){
//            accessToken = cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_ACCESS_TOKEN));
//            cursor.close();
//        }
//        return accessToken;
//    }

    /**
     * Helper method to get the fake accounts to be used with SyncAdapter
     */
    public static Account getSyncAccount(Context context) {

        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        Account newAccount = new Account(context.getString(R.string.app_name),
                context.getString(R.string.sync_account_type));

        // If password doesn't exist, the account doesn't exist
        if (accountManager.getPassword(newAccount) == null) {
            // If not successful
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            // If you don't set android:syncable="true" in your <provider> element in the manifest
            // then call context.setIsSyncable(account, AUTHORITY, 1) here
            onAccountCreated(newAccount, context);
        }

        return newAccount;
    }

    private static void onAccountCreated(Account newAccount, Context context) {
        // Since we've created an account
        SyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);

        // Without calling setSyncAutomatically, our periodic sync will not be enabled
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);

        // Finally, do a sync to get things started
        syncImmediately(context);
    }

    /**
     * Helper method to schedule the sync adapter periodic execution
     */
    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {

        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder()
                    .syncPeriodic(syncInterval, flexTime)
                    .setSyncAdapter(account, authority)
                    .build();
        } else {
            ContentResolver.addPeriodicSync(account,
                    authority, new Bundle(), syncInterval);
        }
    }

    /**
     * Helper method to have the sync adapter sync immediately
     *
     * @param context An app context
     */
    public static void syncImmediately(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);

        Account account = getSyncAccount(context);
        ContentResolver.requestSync(account,
                context.getString(R.string.content_authority), bundle);
    }
}