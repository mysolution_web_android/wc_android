package com.applab.wcircle_pro.Employee;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.ChatRoomActivity;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.UserChatProvider;
import com.applab.wcircle_pro.Chat.xmpp.Config;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

public class BranchEmployeeFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {

    private RecyclerView recyclerView, slidingRecyclerView;
    private EmployeeAdapter employeeAdapter;
    private CountryAdapter countryAdapter;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private LinearLayoutManager linearLayoutManager, slidingLinearLayoutManager;
    private String TAG = "BRANCH EMPLOYEE";
    private EditText ediSlidingSearch;
    private SlidingUpPanelLayout slidingUpPanelLayout;
    private int pageNoCountry = 1;
    private int pageNoEmployee = 1;
    private Cursor cursorCountry, cursorEmployee;
    private boolean isCountryRefresh = true;
    private String countryCode = "";
    private RelativeLayout rlAutoComplete;
    private LinearLayout btnStaff;
    public static final String ACTION = "BRANCH EMPLOYEE";
    private TextView txtError;
    private String selectionEmployee = DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?";
    private String selectionEmployeeArgs[] = new String[]{countryCode};
    private LoaderManager.LoaderCallbacks callbacks;
    private TextView txtName, txtSlidingError;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.fragment_office_employee, container, false);
        layout.setOnKeyListener(backPressed);
        callbacks = this;
        ediSlidingSearch = (EditText) layout.findViewById(R.id.ediSlidingSearch);
        rlAutoComplete = (RelativeLayout) layout.findViewById(R.id.rlAutoComplete);
        rlAutoComplete.setVisibility(View.GONE);
        txtError = (TextView) layout.findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);

        mSwipyRefreshLayout = (SwipyRefreshLayout) layout.findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);

        slidingUpPanelLayout = (SlidingUpPanelLayout) layout.findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.setPanelSlideListener(panelSlideListener);
        txtName = (TextView) layout.findViewById(R.id.txtName);
        txtSlidingError = (TextView) layout.findViewById(R.id.txtSlidingError);
        txtSlidingError.setVisibility(View.GONE);
        btnStaff = (LinearLayout) layout.findViewById(R.id.btnStaff);
        btnStaff.setOnClickListener(btnStaffOnClick);

        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        countryAdapter = new CountryAdapter(getActivity(), cursorCountry);
        recyclerView.setAdapter(countryAdapter);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);

        slidingRecyclerView = (RecyclerView) layout.findViewById(R.id.slidingRecyclerView);
        employeeAdapter = new EmployeeAdapter(getActivity(), cursorEmployee, txtError, "Branch");
        slidingRecyclerView.setAdapter(employeeAdapter);
        slidingLinearLayoutManager = new LinearLayoutManager(getActivity());
        slidingRecyclerView.setLayoutManager(slidingLinearLayoutManager);
        slidingRecyclerView.addOnScrollListener(slidingRecyclerViewOnScrollListener);

        getActivity().getSupportLoaderManager().initLoader(1, null, this);
        getActivity().getSupportLoaderManager().initLoader(594, null, this);

        setSlidingTouchListener();
        setTouchListener();
        HttpHelper.getCountryGson(getActivity(), pageNoCountry, TAG, txtError);
        SearchSlidingWatcher();

        return layout;
    }

    //region back press region
    private View.OnKeyListener backPressed = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                return true;
            }
            return false;
        }
    };
    //endregion

    //region onclick region
    private View.OnClickListener btnStaffOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        }
    };
    //endregion

    //region broadcast region
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                if (!intent.getBooleanExtra("isSlide", false)) {
                    if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    }
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().restartLoader(564, null, this);
        IntentFilter iff = new IntentFilter(ACTION);
        iff.addAction(EmployeeActivity.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }
    //endregion

    //region panel slider region
    private SlidingUpPanelLayout.PanelSlideListener panelSlideListener = new SlidingUpPanelLayout.PanelSlideListener() {
        @Override
        public void onPanelSlide(View panel, float slideOffset) {
        }

        @Override
        public void onPanelCollapsed(View panel) {
            Intent intent = new Intent(EmployeeActivity.ACTION);
            intent.putExtra("isSlide", false);
            EmployeeActivity.mgr.sendBroadcast(intent);
        }

        @Override
        public void onPanelExpanded(View panel) {
            Intent intent = new Intent(EmployeeActivity.ACTION);
            intent.putExtra("isSlide", true);
            EmployeeActivity.mgr.sendBroadcast(intent);
        }

        @Override
        public void onPanelAnchored(View panel) {

        }

        @Override
        public void onPanelHidden(View view) {


        }
    };
    //endregion

    //region search watcher region
    private void SearchSlidingWatcher() {
        ediSlidingSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    pageNoEmployee = 1;
                    HttpHelper.getBranchEmployeeGson(getActivity(), pageNoEmployee, true, countryCode, ediSlidingSearch.getText().toString(), TAG, txtSlidingError);
                    if (ediSlidingSearch.getText().toString().length() > 0) {
                        selectionEmployee = "(" + DBHelper.BRANCH_EMPLOYEE_COLUMN_NAME + " LIKE ? OR " + DBHelper.BRANCH_EMPLOYEE_COLUMN_DEPARTMENT + " LIKE ? ) AND " + DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=? ";
                        selectionEmployeeArgs = new String[]{"%" + ediSlidingSearch.getText().toString() + "%", "%" + ediSlidingSearch.getText().toString() + "%", countryCode};
                        getActivity().getSupportLoaderManager().restartLoader(564, null, callbacks);
                    } else {
                        selectionEmployee = DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?";
                        selectionEmployeeArgs = new String[]{countryCode};
                        getActivity().getSupportLoaderManager().restartLoader(564, null, callbacks);
                    }
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }
    //endregion

    //region touch listener region

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                pageNoEmployee = 1;
                cursorCountry.moveToPosition(position);
                countryCode = cursorCountry.getString(cursorCountry.getColumnIndex(DBHelper.COUNTRY_COLUMN_COUNTRY_CODE));
                txtName.setText(cursorCountry.getString(cursorCountry.getColumnIndex(DBHelper.COUNTRY_COLUMN_COUNTRY_NAME)));
                HttpHelper.getBranchEmployeeGson(getActivity(), pageNoEmployee, false, countryCode, ediSlidingSearch.getText().toString(), TAG, txtSlidingError);
                if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                } else if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                selectionEmployee = DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?";
                selectionEmployeeArgs = new String[]{countryCode};
                getActivity().getSupportLoaderManager().restartLoader(564, null, callbacks);
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    private void setSlidingTouchListener() {
        ItemClickSupport.addTo(slidingRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                RelativeLayout rlImage = (RelativeLayout) v.findViewById(R.id.rlImage);
                Employee employee = (Employee) rlImage.getTag();
                if (getActivity().getIntent().getBooleanExtra("isMessenger", false)) {
                    Utils.postRecentTrack(getActivity(), "msg", employee.getId().toString());
                    if (!employee.getId().equals(Utils.getProfile(getActivity()).getId())) {
                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.SHARED_PREFERENCES_NAME, 0);
                        String selfId = sharedPreferences.getString("username", "");
                        Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
                        try {
                            if (!EmployeeActivity.isUserExist(employee.getId(), getActivity())) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID, employee.getId());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_USER_ID, EmployeeActivity.getLastUserId(getActivity()) + 1);
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_IMAGE, employee.getProfileImage());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_NAME, employee.getName());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_OX_USER, employee.getOXUser());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_CONTACT_NO, employee.getContactNo());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_OFFICE_NO, employee.getOfficeNo());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_POSITION, employee.getPosition());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE, employee.getCountryImage());
                                getActivity().getContentResolver().insert(UserChatProvider.CONTENT_URI, contentValues);
                            }
                        } catch (Exception ex) {
                            ex.fillInStackTrace();
                        } finally {
                            intent.putExtra("user", ChatDBHelper.getSingleUser(getActivity(), employee.getOXUser()));
                        }
                        intent.putExtra("friendId", employee.getOXUser());
                        intent.putExtra("employeeName", employee.getName());
                        intent.putExtra("selfId", selfId);
                        startActivity(intent);
                        getActivity().finish();
                    }
                } else {
                    Intent mIntent = new Intent(getActivity(), EmployeeDetailsActivity.class);
                    mIntent.putExtra("Employee", employee);
                    mIntent.putExtra("Type", "Branch");
                    mIntent.putExtra("isFavorite", employee.getFavouriteId() > 0);
                    startActivity(mIntent);
                }
            }
        });

        ItemClickSupport.addTo(slidingRecyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }


    //endregion

    //region scroll listener region

    private RecyclerView.OnScrollListener slidingRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = slidingLinearLayoutManager.getItemCount();
            int firstVisibleItem = slidingLinearLayoutManager.findLastVisibleItemPosition();
            int lastVisibleItem = slidingLinearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (lastVisibleItem >= totalItemCount - 1) {
                        pageNoEmployee++;
                        HttpHelper.getBranchEmployeeGson(getActivity(), pageNoEmployee, false,
                                countryCode, ediSlidingSearch.getText().toString(), TAG, txtSlidingError);
                    } else if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                        isCountryRefresh = false;
                    }
                }
            }
        }
    };

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (lastVisibleItem >= totalItemCount - 1) {
                        pageNoCountry++;
                        HttpHelper.getCountryGson(getActivity(), pageNoCountry, TAG, txtError);
                    } else if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                        isCountryRefresh = true;
                    }
                }
            }
        }
    };
    //endregion

    //region loader cursor region
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 1) {
            return new CursorLoader(getActivity(), CountryProvider.CONTENT_URI, null, null, null, null);
        } else if (id == 564) {
            return new CursorLoader(getActivity(), BranchEmployeeProvider.CONTENT_URI, null, selectionEmployee, selectionEmployeeArgs, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 1) {
            if (data != null) {
                this.cursorCountry = data;
                countryAdapter.swapCursor(cursorCountry);
            }
        } else if (loader.getId() == 564) {
            if (data != null) {
                this.cursorEmployee = data;
                employeeAdapter.swapCursor(cursorEmployee);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
    //endregion

    //region refresh region
    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at " + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (isCountryRefresh) {
            if (direction == SwipyRefreshLayoutDirection.TOP) {
                pageNoCountry = 1;
                HttpHelper.getCountryGson(getActivity(), pageNoCountry, TAG, txtError);
            }
        } else {
            if (direction == SwipyRefreshLayoutDirection.TOP) {
                pageNoEmployee = 1;
                HttpHelper.getBranchEmployeeGson(getActivity(), pageNoEmployee, false, countryCode, ediSlidingSearch.getText().toString(), TAG, txtSlidingError);
            }
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }
    //endregion
}
