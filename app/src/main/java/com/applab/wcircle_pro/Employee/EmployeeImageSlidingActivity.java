package com.applab.wcircle_pro.Employee;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Gallery.ImageSlidingAdapter;
import com.applab.wcircle_pro.Notification.NotificationDialogFragment;
import com.applab.wcircle_pro.Notification.NotificationProvider;
import com.applab.wcircle_pro.Notification.Notifications;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;

public class EmployeeImageSlidingActivity extends AppCompatActivity {
    private ImageSlidingAdapter adapter;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private Bundle bundle;
    private String id;
    private int position;
    private TextView title;
    private LinearLayout btnSave;
    private int i = 0;
    private DialogFragment notificationDialogFragment;
    private String url = Utils.API_URL + "api/Tracking";
    private String TAG = "NEWS_IMAGE_SLIDING";
    private int selectedPage;
    private String urlImage;
    private ArrayList<String> arrUrl = new ArrayList<>();
    private boolean isGallery = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_image_sliding);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        title = (TextView) findViewById(R.id.imgTitle);
        btnSave = (LinearLayout) findViewById(R.id.btnSave);
        viewPager = (ViewPager) findViewById(R.id.pager);
        bundle = getIntent().getExtras();
        id = bundle.getString("id");
        position = bundle.getInt("position");
        urlImage = bundle.getString("urlImage").replace("/thumb","");
        arrUrl.add(urlImage);
        adapter = new ImageSlidingAdapter(getSupportFragmentManager(), EmployeeImageSlidingActivity.this,
                arrUrl);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);
        selectedPage = position;
        String[] result = urlImage.split("/");
        title.setText(result[result.length - 1]);
        hideBar();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void handleAction(View v) {
        if (isGallery) {
            if (title.getVisibility() == View.GONE) {
                Utils.expand(title);
            } else {
                Utils.collapse(title);
            }

            if (btnSave.getVisibility() == View.GONE) {
                Utils.expand(btnSave);
            } else {
                Utils.collapse(btnSave);
            }
        }
    }

    public void hideBar() {
        if (Build.VERSION.SDK_INT < 16) { //ye olde method
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else { // Jellybean and up, new hotness
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            // Remember that you should never show the action bar if the
            // status bar is hidden, so hide that too if necessary.
            ActionBar actionBar = getSupportActionBar();
            actionBar.hide();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_news_image_sliding, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setPopUpWindow(Cursor cursor) {
        notificationDialogFragment = new NotificationDialogFragment();
        Bundle args = new Bundle();
        ArrayList<Notifications> arrayList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Notifications notifications = new Notifications();
                notifications.setId(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_ID)));
                notifications.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_CREATE_DATE)));
                notifications.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_TITLE)));
                notifications.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_DESCRIPTION)));
                notifications.setType(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_TYPE)));
                arrayList.add(notifications);
            }
            while (cursor.moveToNext());
            cursor.close();
        }
        args.putParcelableArrayList("Notifications", arrayList);
        notificationDialogFragment.setArguments(args);
        notificationDialogFragment.setCancelable(false);
        notificationDialogFragment.show(getSupportFragmentManager(), "");
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Utils.NOTIFICATION_ACTION)) {
                Uri uri = NotificationProvider.CONTENT_URI;
                String[] projection = {
                        DBHelper.NOTIFICATION_COLUMN_TYPE,
                        DBHelper.NOTIFICATION_COLUMN_TITLE,
                        DBHelper.NOTIFICATION_COLUMN_CREATE_DATE,
                        DBHelper.NOTIFICATION_COLUMN_DESCRIPTION,
                        DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY,
                        DBHelper.NOTIFICATION_COLUMN_ID
                };
                String selection = DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY + "=?";
                String[] selectionArgs = {"0"};
                Cursor cursor = getContentResolver().query(uri, projection, selection, selectionArgs, DBHelper.NOTIFICATION_COLUMN_ID + " DESC ");
                if (notificationDialogFragment != null) {
                    if (!notificationDialogFragment.isHidden()) {
                        notificationDialogFragment.dismiss();
                    }
                }
                setPopUpWindow(cursor);
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(),false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(),false);
        IntentFilter iff = new IntentFilter(Utils.NOTIFICATION_ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }
}
