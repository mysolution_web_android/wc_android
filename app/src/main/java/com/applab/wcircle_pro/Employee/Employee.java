package com.applab.wcircle_pro.Employee;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;

public class Employee implements Parcelable {

    @Expose
    private Integer Id;
    @Expose
    private Integer FavouriteId;
    @Expose
    private String ProfileImage;
    @Expose
    private Integer DefaultBranchId;
    @Expose
    private String DefaultBranchName;
    @Expose
    private String Country;
    @Expose
    private String CountryImage;
    @Expose
    private String Gender;
    @Expose
    private String AllBranch;
    @Expose
    private String Name;
    @Expose
    private String Department;
    @Expose
    private String Position;
    @Expose
    private String ContactNo;
    @Expose
    private String Email;
    @Expose
    private String LastUpdated;
    @Expose
    private String CountryName;
    @Expose
    private String EmployeeNo;
    @Expose
    private String DOB;
    @Expose
    private String OfficeNo;
    @Expose
    private String HODName;
    @Expose
    private String HODId;
    @Expose
    private boolean isSelect = false;
    @Expose
    private String Message;
    @Expose
    private String OXUser;
    private int actionId;
    private String createDate;
    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public String getOXUser() {
        return OXUser;
    }

    public void setOXUser(String OXUser) {
        this.OXUser = OXUser;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getEmployeeNo() {
        return EmployeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        EmployeeNo = employeeNo;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getOfficeNo() {
        return OfficeNo;
    }

    public void setOfficeNo(String officeNo) {
        OfficeNo = officeNo;
    }

    public String getHODName() {
        return HODName;
    }

    public void setHODName(String HODName) {
        this.HODName = HODName;
    }

    public String getHODId() {
        return HODId;
    }

    public void setHODId(String HODId) {
        this.HODId = HODId;
    }

    public Integer getFavouriteId() {
        return FavouriteId;
    }

    public void setFavouriteId(Integer favouriteId) {
        FavouriteId = favouriteId;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public Integer getDefaultBranchId() {
        return DefaultBranchId;
    }

    public void setDefaultBranchId(Integer defaultBranchId) {
        DefaultBranchId = defaultBranchId;
    }

    public String getDefaultBranchName() {
        return DefaultBranchName;
    }

    public void setDefaultBranchName(String defaultBranchName) {
        DefaultBranchName = defaultBranchName;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCountryImage() {
        return CountryImage;
    }

    public void setCountryImage(String countryImage) {
        CountryImage = countryImage;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public boolean isSelect() {
        return isSelect;
    }

    /**
     * @return The Message
     */
    public String getMessage() {
        return Message;
    }

    /**
     * @param Message The Message
     */
    public void setMessage(String Message) {
        this.Message = Message;
    }

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The Company
     */
    public String getAllBranch() {
        return AllBranch;
    }

    /**
     * @param AllBranch The Company
     */
    public void setAllBranch(String AllBranch) {
        this.AllBranch = AllBranch;
    }

    /**
     * @return The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return The Department
     */
    public String getDepartment() {
        return Department;
    }

    /**
     * @param Department The Department
     */
    public void setDepartment(String Department) {
        this.Department = Department;
    }

    /**
     * @return The Position
     */
    public String getPosition() {
        return Position;
    }

    /**
     * @param Position The Position
     */
    public void setPosition(String Position) {
        this.Position = Position;
    }

    /**
     * @return The LastUpdated
     */
    public String getLastUpdated() {
        return LastUpdated;
    }

    /**
     * @param LastUpdated The LastUpdated
     */
    public void setLastUpdated(String LastUpdated) {
        this.LastUpdated = LastUpdated;
    }

    /**
     * @return The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     * @param Email The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     * @return The ContactNo
     */
    public String getContactNo() {
        return ContactNo;
    }

    /**
     * @param ContactNo The ContactNo
     */
    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    /**
     * @return The isSelect
     */
    public boolean getIsSelect() {
        return isSelect;
    }

    /**
     * @param isSelect The isSelect
     */
    public void setIsSelect(boolean isSelect) {
        this.isSelect = isSelect;
    }

    public Employee() {
    }

    public Employee(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ProfileImage);
        dest.writeString(this.DefaultBranchName);
        dest.writeString(this.Country);
        dest.writeString(this.CountryImage);
        dest.writeString(this.Gender);
        dest.writeString(this.AllBranch);
        dest.writeString(this.Name);
        dest.writeString(this.Department);
        dest.writeString(this.Position);
        dest.writeString(this.ContactNo);
        dest.writeString(this.Email);
        dest.writeString(this.LastUpdated);
        dest.writeString(this.EmployeeNo);
        dest.writeString(this.CountryName);
        dest.writeString(this.DOB);
        dest.writeString(this.OfficeNo);
        dest.writeString(this.HODName);
        dest.writeString(this.HODId);
        dest.writeString(this.Message);
        dest.writeInt(this.Id);
        dest.writeInt(this.FavouriteId);
        dest.writeInt(this.DefaultBranchId);
        dest.writeString(this.OXUser);
        dest.writeByte((byte) (isSelect ? 1 : 0));
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.ProfileImage = in.readString();
        this.DefaultBranchName = in.readString();
        this.Country = in.readString();
        this.CountryImage = in.readString();
        this.Gender = in.readString();
        this.AllBranch = in.readString();
        this.Name = in.readString();
        this.Department = in.readString();
        this.Position = in.readString();
        this.ContactNo = in.readString();
        this.Email = in.readString();
        this.LastUpdated = in.readString();
        this.EmployeeNo = in.readString();
        this.CountryName = in.readString();
        this.DOB = in.readString();
        this.OfficeNo = in.readString();
        this.HODName = in.readString();
        this.HODId = in.readString();
        this.Message = in.readString();
        this.Id = in.readInt();
        this.FavouriteId = in.readInt();
        this.DefaultBranchId = in.readInt();
        this.OXUser = in.readString();
        this.isSelect = in.readByte() != 0;
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Employee createFromParcel(Parcel in) {
            return new Employee(in);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };

    public static Employee getDepartmentData(Cursor cursor, int position) {
        Employee employee = new Employee();
        cursor.moveToPosition(position);
        employee.setName(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_NAME)));
        employee.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID)));
        employee.setDepartment(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DEPARTMENT)));
        employee.setAllBranch(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_ALL_BRANCH)));
        employee.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_POSITION)));
        employee.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_CONTACT_NO)));
        employee.setOXUser(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_OXUSER)));
        employee.setEmail(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMAIL)));
        employee.setProfileImage(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IMAGE)));
        employee.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE)));
        employee.setGender(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_GENDER)));
        employee.setLastUpdated(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_LAST_UPDATED)));
        employee.setMessage(null);
        employee.setCountry(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY)));
        employee.setDefaultBranchName(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_NAME)));
        employee.setDefaultBranchId(cursor.getInt(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_ID)));
        employee.setFavouriteId(cursor.getInt(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_FAVORITE_ID)));
        employee.setCountryName(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_NAME)));
        employee.setDOB(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DOB)));
        employee.setEmployeeNo(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_NO)));
        employee.setHODId(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_HOD_ID)));
        employee.setHODName(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_HOD_NAME)));
        employee.setOfficeNo(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_OFFICE_NO)));
        employee.setIsSelect(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IS_SELECTED))));
        return employee;
    }

    public static Employee getBranchData(Cursor cursor, int position) {
        Employee employee = new Employee();
        cursor.moveToPosition(position);
        employee.setName(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_NAME)));
        employee.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID)));
        employee.setDepartment(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_DEPARTMENT)));
        employee.setAllBranch(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_ALL_BRANCH)));
        employee.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_POSITION)));
        employee.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_CONTACT_NO)));
        employee.setOXUser(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_OXUSER)));
        employee.setEmail(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMAIL)));
        employee.setProfileImage(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_IMAGE)));
        employee.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY_IMAGE)));
        employee.setGender(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_GENDER)));
        employee.setLastUpdated(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_LAST_UPDATED)));
        employee.setMessage(null);
        employee.setCountry(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY)));
        employee.setDefaultBranchName(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_BRANCH_NAME)));
        employee.setDefaultBranchId(cursor.getInt(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_BRANCH_ID)));
        employee.setFavouriteId(cursor.getInt(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_FAVORITE_ID)));
        employee.setCountryName(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY_NAME)));
        employee.setDOB(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_DOB)));
        employee.setEmployeeNo(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_NO)));
        employee.setHODId(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_HOD_ID)));
        employee.setHODName(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_HOD_NAME)));
        employee.setOfficeNo(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_OFFICE_NO)));
        employee.setIsSelect(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.BRANCH_EMPLOYEE_COLUMN_IS_SELECTED))));
        return employee;
    }

    public static Employee getRecentData(Cursor cursor, int position) {
        Employee employee = new Employee();
        cursor.moveToPosition(position);
        employee.setName(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_NAME)));
        employee.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID)));
        employee.setDepartment(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_DEPARTMENT)));
        employee.setAllBranch(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_ALL_BRANCH)));
        employee.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_POSITION)));
        employee.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_CONTACT_NO)));
        employee.setOXUser(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_OXUSER)));
        employee.setEmail(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_EMAIL)));
        employee.setProfileImage(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_IMAGE)));
        employee.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE)));
        employee.setGender(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_GENDER)));
        employee.setLastUpdated(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_LAST_UPDATED)));
        employee.setMessage(null);
        employee.setCountry(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_COUNTRY)));
        employee.setDefaultBranchName(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_BRANCH_NAME)));
        employee.setDefaultBranchId(cursor.getInt(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_BRANCH_ID)));
        employee.setFavouriteId(cursor.getInt(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_FAVORITE_ID)));
        employee.setCountryName(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_COUNTRY_NAME)));
        employee.setDOB(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_DOB)));
        employee.setEmployeeNo(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_NO)));
        employee.setHODId(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_HOD_ID)));
        employee.setHODName(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_HOD_NAME)));
        employee.setOfficeNo(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_OFFICE_NO)));
        employee.setOfficeNo(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_OFFICE_NO)));
        employee.setActionId(cursor.getInt(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_ACTION_ID)));
        employee.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_CREATE_DATE)));
        employee.setAction(cursor.getString(cursor.getColumnIndex(DBHelper.RECENT_EMPLOYEE_COLUMN_ACTION)));
        return employee;
    }
}