package com.applab.wcircle_pro.Employee;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/2/2015.
 */
public class EmployeeViewHolder extends RecyclerView.ViewHolder {
    TextView name;
    TextView position, dateTime;
    ImageView img, imgCountry;
    ImageView btnFavorite;
    RelativeLayout rlImage;

    public EmployeeViewHolder(View itemView) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.txtName);
        dateTime = (TextView) itemView.findViewById(R.id.txtDateTime);
        position = (TextView) itemView.findViewById(R.id.txtPosition);
        img = (ImageView) itemView.findViewById(R.id.img);
        btnFavorite = (ImageView) itemView.findViewById(R.id.btnFavorite);
        imgCountry = (ImageView) itemView.findViewById(R.id.imgCountry);
        rlImage = (RelativeLayout) itemView.findViewById(R.id.rlImage);
    }
}
