package com.applab.wcircle_pro.Employee;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by user on 5/2/2015.
 */

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private String TAG = "EMPLOYEE";
    private Cursor cursor;
    private TextView txtError;
    private String module;

    public EmployeeAdapter(Context context, Cursor cursor, TextView txtError, String module) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.cursor = cursor;
        this.txtError = txtError;
        this.module = module;
    }

    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("EmployeeViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_employee_row, parent, false);
        EmployeeViewHolder holder = new EmployeeViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(EmployeeViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Employee employee = module.equals("Branch") ? Employee.getBranchData(cursor, position) : Employee.getDepartmentData(cursor, position);
        holder.name.setText(employee.getName());
        holder.name.setTextColor(ContextCompat.getColor(context, R.color.color_green));
        holder.btnFavorite.setImageResource(employee.getIsSelect() ? R.mipmap.info_star_active : R.mipmap.info_star_inactive);
        employee.setIsSelect(employee.getIsSelect());
        if (employee.getId().equals(Utils.getProfile(context).getId())) {
            holder.btnFavorite.setVisibility(View.GONE);
        } else {
            holder.btnFavorite.setVisibility(View.VISIBLE);
        }
        holder.position.setText(employee.getPosition());
        Glide.with(context)
                .load(employee.getProfileImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleTransform(context))
                .placeholder(R.mipmap.ic_action_person_light)
                .into(holder.img);
        Glide.with(context)
                .load(employee.getCountryImage())
                .transform(new CircleTransform(context))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgCountry);
        holder.rlImage.setTag(employee);
        holder.btnFavorite.setTag(employee);
        setRlImageOnClickListener(holder.rlImage);
        setBtnFavoriteOnClickListener(holder.btnFavorite);
    }

    private void setRlImageOnClickListener(final RelativeLayout rlImage) {
        rlImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Employee employee = (Employee) rlImage.getTag();
                Intent intent = new Intent(context, EmployeeImageSlidingActivity.class);
                intent.putExtra("id", String.valueOf(employee.getId()));
                intent.putExtra("position", 0);
                String url = employee.getProfileImage();
                if (url == null) {
                    url = "";
                }
                intent.putExtra("urlImage", url);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    private void setBtnFavoriteOnClickListener(final ImageView btnFavorite) {
        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Employee employee = (Employee) btnFavorite.getTag();
                if (employee.getIsSelect()) {
                    if (Utils.isConnectingToInternet(context)) {
                        HttpHelper.deleteFavorite(employee.getId().toString(), context, TAG, txtError);
                        btnFavorite.setImageResource(R.mipmap.info_star_inactive);
                    } else {
                        Utils.showNoConnection(context);
                        btnFavorite.setImageResource(R.mipmap.info_star_active);
                    }
                } else {
                    if (Utils.isConnectingToInternet(context)) {
                        HttpHelper.postFavorite(context, String.valueOf(Utils.getProfile(context).getId()), employee.getId().toString(), "C", TAG, txtError);
                        btnFavorite.setImageResource(R.mipmap.info_star_active);
                    } else {
                        Utils.showNoConnection(context);
                        btnFavorite.setImageResource(R.mipmap.info_star_inactive);
                    }
                }
            }
        });
    }
    //endregion

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        android.os.Message msg = handler.obtainMessage();
        handler.handleMessage(msg);
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            EmployeeAdapter.this.notifyDataSetChanged();
        }
    };
}
