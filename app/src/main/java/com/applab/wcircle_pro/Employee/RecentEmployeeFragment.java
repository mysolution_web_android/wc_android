package com.applab.wcircle_pro.Employee;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.ChatRoomActivity;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.UserChatProvider;
import com.applab.wcircle_pro.Chat.xmpp.Config;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

public class RecentEmployeeFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipyRefreshLayout.OnRefreshListener {
    private RecyclerView recyclerView;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private String TAG = "RECENT EMPLOYEE";
    private RecentEmployeeAdapter adapter;
    private Cursor cursor;
    private EditText ediSearch;
    private int pageNo = 1;
    private TextView txtError;
    private String selection = DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "!=?";
    private String[] selectionArgs = null;
    private LoaderManager.LoaderCallbacks callbacks;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        selection = DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "!=?";
        selectionArgs = new String[]{Utils.getProfile(getActivity()).getId().toString()};
        final View layout = inflater.inflate(R.layout.fragment_recent_employee, container, false);
        mSwipyRefreshLayout = (SwipyRefreshLayout) layout.findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        ediSearch = (EditText) layout.findViewById(R.id.ediSearch);
        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        adapter = new RecentEmployeeAdapter(getActivity(), cursor, getFragmentManager());
        recyclerView.setAdapter(adapter);
        callbacks = this;
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        setTouchListener();
        txtError = (TextView) layout.findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);
        SearchWatcher();
        getActivity().getSupportLoaderManager().initLoader(0, null, this);
        return layout;
    }

    private void SearchWatcher() {
        ediSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    pageNo = 1;
                    HttpHelper.getOfficeGson(pageNo, true, getActivity(), ediSearch.getText().toString(), TAG, txtError);
                    if (ediSearch.getText().toString().length() > 0) {
                        selection = DBHelper.RECENT_EMPLOYEE_COLUMN_NAME + " LIKE ? OR " + DBHelper.RECENT_EMPLOYEE_COLUMN_DEPARTMENT + " LIKE ? AND " + DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "!=?";
                        selectionArgs = new String[]{"%" + ediSearch.getText().toString() + "%", "%" + ediSearch.getText().toString() + "%", Utils.getProfile(getActivity()).getId().toString()};
                        getActivity().getSupportLoaderManager().restartLoader(0, null, callbacks);
                    } else {
                        selection = DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "!=?";
                        selectionArgs = new String[]{Utils.getProfile(getActivity()).getId().toString()};
                        getActivity().getSupportLoaderManager().restartLoader(0, null, callbacks);
                    }
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                RelativeLayout rlImage = (RelativeLayout) v.findViewById(R.id.rlImage);
                final Employee current = (Employee) rlImage.getTag();
                if (getActivity().getIntent().getBooleanExtra("isMessenger", false)) {
                    Utils.postRecentTrack(getActivity(), "msg", current.getId().toString());
                    if (!current.getId().equals(Utils.getProfile(getActivity()).getId())) {
                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.SHARED_PREFERENCES_NAME, 0);
                        String selfId = sharedPreferences.getString("username", "");
                        Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
                        try {
                            if (!EmployeeActivity.isUserExist(current.getId(), getActivity())) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID, current.getId());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_USER_ID, EmployeeActivity.getLastUserId(getActivity()) + 1);
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_IMAGE, current.getProfileImage());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_NAME, current.getName());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_OX_USER, current.getOXUser());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_CONTACT_NO, current.getContactNo());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_OFFICE_NO, current.getOfficeNo());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_POSITION, current.getPosition());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE, current.getCountryImage());
                                getActivity().getContentResolver().insert(UserChatProvider.CONTENT_URI, contentValues);
                            }
                        } catch (Exception ex) {
                            ex.fillInStackTrace();
                        } finally {
                            intent.putExtra("user", ChatDBHelper.getSingleUser(getActivity(), current.getOXUser()));
                        }
                        intent.putExtra("friendId", current.getOXUser());
                        intent.putExtra("employeeName", current.getName());
                        intent.putExtra("selfId", selfId);
                        startActivity(intent);
                        getActivity().finish();
                    }
                } else {
                    Intent mIntent = new Intent(getActivity(), EmployeeDetailsActivity.class);
                    mIntent.putExtra("Employee", current);
                    mIntent.putExtra("Type", "Recent");
                    startActivity(mIntent);
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                final Employee current = Employee.getRecentData(cursor, position);
                RecentEmployeeDialogFragment recentEmployeeDialogFragment =
                        RecentEmployeeDialogFragment.newInstance(Utils.DELETE_SINGLE_RERENT, getActivity(),
                                current.getActionId());
                recentEmployeeDialogFragment.show(getFragmentManager(), "");
                return true;
            }
        });
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (lastVisibleItem >= totalItemCount - 1) {
                        pageNo++;
                        HttpHelper.getRecentGson(pageNo, getActivity(), TAG, txtError);
                    } else if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);

                    }
                }
            }
        }
    };

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            pageNo = 1;
            HttpHelper.getRecentGson(pageNo, getActivity(), TAG, txtError);
        } else {
            pageNo++;
            HttpHelper.getRecentGson(pageNo, getActivity(), TAG, txtError);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 0) {
            return new CursorLoader(getActivity(), RecentEmployeeProvider.CONTENT_URI, null, selection, selectionArgs, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 0) {
            if (data != null) {
                if (data.getCount() > 0) {
                    this.cursor = data;
                    adapter.swapCursor(cursor);
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onResume() {
        super.onResume();
        HttpHelper.getRecentGson(pageNo, getActivity(), TAG, txtError);
    }
}
