package com.applab.wcircle_pro.Employee;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 8/10/2015.
 */

public class CountryViewHolder extends RecyclerView.ViewHolder {
    ImageView imgCountry;
    TextView txtCountryName;

    public CountryViewHolder(View itemView) {
        super(itemView);
        txtCountryName = (TextView) itemView.findViewById(R.id.txtCountryName);
        imgCountry = (ImageView)itemView.findViewById(R.id.imgCountry);
    }

}