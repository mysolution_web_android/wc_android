package com.applab.wcircle_pro.Employee;

/**
 * Created by user on 18/9/2015.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class EmployeeChecking {

    @Expose
    private String LastUpdate;
    @Expose
    private Integer PageNo;
    @Expose
    private Integer NoPerPage;
    @Expose
    private Integer NoOfRecords;
    @Expose
    private Integer NoOfPage;
    @Expose
    private String Message;
    @Expose
    private List<Employee> items = new ArrayList<Employee>();

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    /**
     *
     * @return
     * The LastUpdate
     */
    public String getLastUpdate() {
        return LastUpdate;
    }

    /**
     *
     * @param LastUpdate
     * The LastUpdate
     */
    public void setLastUpdate(String LastUpdate) {
        this.LastUpdate = LastUpdate;
    }

    /**
     *
     * @return
     * The PageNo
     */
    public Integer getPageNo() {
        return PageNo;
    }

    /**
     *
     * @param PageNo
     * The PageNo
     */
    public void setPageNo(Integer PageNo) {
        this.PageNo = PageNo;
    }

    /**
     *
     * @return
     * The NoPerPage
     */
    public Integer getNoPerPage() {
        return NoPerPage;
    }

    /**
     *
     * @param NoPerPage
     * The NoPerPage
     */
    public void setNoPerPage(Integer NoPerPage) {
        this.NoPerPage = NoPerPage;
    }

    /**
     *
     * @return
     * The NoOfRecords
     */
    public Integer getNoOfRecords() {
        return NoOfRecords;
    }

    /**
     *
     * @param NoOfRecords
     * The NoOfRecords
     */
    public void setNoOfRecords(Integer NoOfRecords) {
        this.NoOfRecords = NoOfRecords;
    }

    /**
     *
     * @return
     * The NoOfPage
     */
    public Integer getNoOfPage() {
        return NoOfPage;
    }

    /**
     *
     * @param NoOfPage
     * The NoOfPage
     */
    public void setNoOfPage(Integer NoOfPage) {
        this.NoOfPage = NoOfPage;
    }

    /**
     *
     * @return
     * The items
     */
    public List<Employee> getEmployees() {
        return items;
    }

    /**
     *
     * @param items
     * The items
     */
    public void setEmployees(List<Employee> items) {
        this.items = items;
    }

}