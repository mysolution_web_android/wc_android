package com.applab.wcircle_pro.Employee;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by user on 5/2/2015.
 */

public class CountryAdapter extends RecyclerView.Adapter<CountryViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private String TAG = "EMPLOYEE";
    private Cursor mCursor;

    public CountryAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.mCursor = cursor;
    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("CountryViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_county_row, parent, false);
        CountryViewHolder holder = new CountryViewHolder(view);
        holder.setIsRecyclable(false);
        return holder;
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Country current = getCountry(mCursor, position);
        holder.txtCountryName.setText(current.getCountryName());
        holder.txtCountryName.setTextColor(ContextCompat.getColor(context, R.color.color_green));
        Glide.with(context)
                .load(current.getCountryImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.empty_photo)
                .into(holder.imgCountry);
        holder.txtCountryName.setTag(current);
    }

    private Country getCountry(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Country country = new Country();
        country.setCountryName(cursor.getString(cursor.getColumnIndex(DBHelper.COUNTRY_COLUMN_COUNTRY_NAME)));
        country.setCountryCode(cursor.getString(cursor.getColumnIndex(DBHelper.COUNTRY_COLUMN_COUNTRY_CODE)));
        country.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.COUNTRY_COLUMN_COUNTRY_IMAGE)));
        country.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.COUNTRY_COLUMN_COUNTRY_ID)));
        country.setLastUpdated(cursor.getString(cursor.getColumnIndex(DBHelper.COUNTRY_COLUMN_LAST_UPDATED)));
        return country;
    }

    @Override
    public int getItemCount() {
        return (mCursor == null) ? 0 : mCursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.mCursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.mCursor;
        this.mCursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);

        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            CountryAdapter.this.notifyDataSetChanged();
        }
    };
}
