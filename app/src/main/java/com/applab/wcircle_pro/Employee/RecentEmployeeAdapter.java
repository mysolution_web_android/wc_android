package com.applab.wcircle_pro.Employee;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.applab.wcircle_pro.Chat.ChatRoomActivity;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.UserChatProvider;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by user on 5/2/2015.
 */

public class RecentEmployeeAdapter extends RecyclerView.Adapter<EmployeeViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private String TAG = "EMPLOYEE";
    private Cursor cursor;
    private FragmentManager fragmentManager;

    public RecentEmployeeAdapter(Context context, Cursor cursor, FragmentManager fragmentManager) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.cursor = cursor;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("EmployeeViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_employee_row, parent, false);
        EmployeeViewHolder holder = new EmployeeViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(EmployeeViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Employee current = Employee.getRecentData(cursor, position);
        holder.name.setText(current.getName());
        holder.name.setTextColor(ContextCompat.getColor(context, R.color.color_green));
        switch (current.getAction()) {
            case "call":
                holder.btnFavorite.setImageResource(R.mipmap.action_call);
                break;
            case "msg":
                holder.btnFavorite.setImageResource(R.mipmap.action_messenger);
                break;
            case "email":
                holder.btnFavorite.setImageResource(R.mipmap.action_email);
                break;
        }
        holder.position.setText(current.getPosition());
        holder.dateTime.setVisibility(View.VISIBLE);
        holder.dateTime.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy h:mm a", current.getCreateDate()));
        Glide.with(context)
                .load(current.getProfileImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleTransform(context))
                .placeholder(R.mipmap.ic_action_person_light)
                .into(holder.img);
        Glide.with(context)
                .load(current.getCountryImage())
                .transform(new CircleTransform(context))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgCountry);
        Log.i(TAG, "Image:" + current.getProfileImage());
        holder.rlImage.setTag(current);
        holder.btnFavorite.setTag(current);
        setRlOnClickListener(holder.rlImage);
        setBtnFavoriteOnClickListener(holder.btnFavorite);
    }

    private void setBtnFavoriteOnClickListener(final ImageView btnFavorite) {
        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Employee current = (Employee) btnFavorite.getTag();
                switch (current.getAction()) {
                    case "email":
                        Utils.postRecentTrack(context, "email", current.getId().toString());
                        Intent email = new Intent(Intent.ACTION_SEND);
                        email.putExtra(Intent.EXTRA_EMAIL, new String[]{current.getEmail()});
                        email.putExtra(Intent.EXTRA_SUBJECT, "");
                        email.putExtra(Intent.EXTRA_TEXT, "");
                        email.setType("message/rfc822");
                        context.startActivity(Intent.createChooser(email, Utils.CHOOSE_CLIENT));
                        break;
                    case "msg":
                        Utils.postRecentTrack(context, "msg", current.getId().toString());
                        Intent intent = new Intent(context, ChatRoomActivity.class);
                        try {
                            if (!EmployeeActivity.isUserExist(current.getId(), context)) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID, current.getId());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_USER_ID, EmployeeActivity.getLastUserId(context) + 1);
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_IMAGE, current.getProfileImage());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_NAME, current.getName());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_OX_USER, current.getOXUser());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_CONTACT_NO, current.getContactNo());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_OFFICE_NO, current.getOfficeNo());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_POSITION, current.getPosition());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE, current.getCountryImage());
                                context.getContentResolver().insert(UserChatProvider.CONTENT_URI, contentValues);
                            }
                        } catch (Exception ex) {
                            ex.fillInStackTrace();
                        } finally {
                            intent.putExtra("user", ChatDBHelper.getSingleUser(context, current.getOXUser()));
                        }
                        intent.putExtra("friendId", current.getOXUser());
                        intent.putExtra("employeeName", current.getName());
                        intent.putExtra("selfId", Utils.getProfile(context).getOXUser());
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        break;
                    case "call":
                        EmployeeDialogFragment employeeDialogFragment = EmployeeDialogFragment.newInstance(current, context);
                        employeeDialogFragment.show(fragmentManager, "");
                        break;
                }
            }
        });
    }

    private void setRlOnClickListener(final RelativeLayout rlImage) {
        rlImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Employee current = (Employee) rlImage.getTag();
                Intent intent = new Intent(context, EmployeeImageSlidingActivity.class);
                intent.putExtra("id", String.valueOf(current.getId()));
                intent.putExtra("position", 0);
                String url = current.getProfileImage();
                if (url == null) {
                    url = "";
                }
                intent.putExtra("urlImage", url);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }


    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            RecentEmployeeAdapter.this.notifyDataSetChanged();
        }
    };
}
