package com.applab.wcircle_pro.Employee;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.ChatActivity;
import com.applab.wcircle_pro.Chat.db.ChatListProvider;
import com.applab.wcircle_pro.Chat.db.UserChatProvider;
import com.applab.wcircle_pro.Favorite.FavoriteActivity;
import com.applab.wcircle_pro.Menu.NavigationDrawerFragment;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Tabs.SlidingTabLayout;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

public class EmployeeActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private static RelativeLayout fadeRL;
    private Bundle bundle;
    private boolean isFavorite = false;
    private boolean isMessenger = false;
    private String TAG = "EMPLOYEE";
    private SlidingTabLayout mTabs;
    private ViewPager mPager;
    private String type = "";
    public static LocalBroadcastManager mgr;
    public static final String ACTION = "EMPLOYEE_VIEW_PAGER";
    private boolean isSlide = false;
    private boolean isRecent = true;
    private ImageView space1;
    private TextView txtToolbarTitle;
    private int mPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green));
        space1 = (ImageView) toolbar.findViewById(R.id.space1);
        space1.setVisibility(View.INVISIBLE);
        txtToolbarTitle = (TextView) findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_employee));
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        setFadeRL(fadeRL);
        mgr = LocalBroadcastManager.getInstance(this);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            isFavorite = bundle.getBoolean("isFavorite");
            isMessenger = bundle.getBoolean("isMessenger");
        }
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        drawerFragment.setSelectedPosition(6);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new EmployeePagerAdapter(getSupportFragmentManager(), this));
        mPager.setOffscreenPageLimit(3);
        mPager.addOnPageChangeListener(mPagerChangeListener);
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setDistributeEvenly(true);
        mTabs.setmColor(2);
        mTabs.setCustomTabView(R.layout.custom_tab_view, R.id.tabText, R.id.tabNumber);
        mTabs.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
        mTabs.setSelectedIndicatorColors(ContextCompat.getColor(this, R.color.color_green));
        mTabs.setViewPager(mPager);
        toolbar.setNavigationIcon(R.mipmap.action_arrow_prev_white);
        toolbar.setNavigationOnClickListener(toolbarOnClickListener);
    }

    private View.OnClickListener toolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isSlide) {
                if (isFavorite) {
                    startActivity(new Intent(EmployeeActivity.this, FavoriteActivity.class));
                    finish();
                } else if (isMessenger) {
                    startActivity(new Intent(EmployeeActivity.this, ChatActivity.class));
                    finish();
                } else {
                    Utils.clearPreviousActivity(EmployeeActivity.this);
                }
            } else {
                if (mPosition != 2) {
                    Utils.clearPreviousActivity(EmployeeActivity.this);
                } else {
                    Intent intent = new Intent(BranchEmployeeFragment.ACTION);
                    intent.putExtra("isSlide", false);
                    mgr.sendBroadcast(intent);
                }
            }
        }
    };

    public static boolean isUserExist(long id, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(UserChatProvider.CONTENT_URI, null, DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static int getLastUserId(Context context) {
        int id = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ChatListProvider.CONTENT_URI, null, null, null, null);
            if (cursor != null && cursor.moveToLast()) {
                id = cursor.getInt(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_USER_ID));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }


    private ViewPager.OnPageChangeListener mPagerChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            mPosition = position;
            if (position == 0) {
                isRecent = true;
                invalidateOptionsMenu();
            } else if (position == 1) {
                isRecent = false;
                invalidateOptionsMenu();
            } else {
                isRecent = false;
                invalidateOptionsMenu();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    public static RelativeLayout getFadeRL() {
        return fadeRL;
    }

    public static void setFadeRL(RelativeLayout fadeRL) {
        EmployeeActivity.fadeRL = fadeRL;
    }


    @Override
    public void onBackPressed() {
        if (!isSlide) {
            if (isFavorite) {
                startActivity(new Intent(EmployeeActivity.this, FavoriteActivity.class));
                finish();
            } else if (isMessenger) {
                startActivity(new Intent(EmployeeActivity.this, ChatActivity.class));
                finish();
            } else {
                Utils.clearPreviousActivity(EmployeeActivity.this);
            }
        } else {
            if (mPosition != 2) {
                Utils.clearPreviousActivity(EmployeeActivity.this);
            } else {
                Intent intent = new Intent(BranchEmployeeFragment.ACTION);
                intent.putExtra("isSlide", false);
                mgr.sendBroadcast(intent);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_employee, menu);
        if (isRecent) {
            for (int i = 0; i < menu.size(); i++) {
                menu.getItem(i).setVisible(true);
                space1.setVisibility(View.INVISIBLE);
            }
        } else {
            for (int i = 0; i < menu.size(); i++) {
                if (menu.getItem(i).getItemId() != R.id.menuRight) {
                    menu.getItem(i).setVisible(false);
                    space1.setVisibility(View.GONE);
                }
            }
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (drawerFragment.mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else if (id == R.id.action_menu) {
            View v = findViewById(R.id.action_menu);
            PopupMenu popupMenu = new PopupMenu(toolbar.getContext(), v) {
                @Override
                public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_clear_employee:
                            HttpHelper.deleteRecent(EmployeeActivity.this, TAG);
                            return true;
                        default:
                            return super.onMenuItemSelected(menu, item);
                    }
                }
            };
            popupMenu.inflate(R.menu.menu_custom);
            popupMenu.getMenu().removeItem(R.id.action_folder);
            popupMenu.getMenu().removeItem(R.id.action_upload);
            popupMenu.getMenu().removeItem(R.id.action_new_chat);
            popupMenu.getMenu().removeItem(R.id.action_create_group);
            popupMenu.getMenu().removeItem(R.id.action_camera);
            popupMenu.getMenu().removeItem(R.id.action_gallery);
            popupMenu.getMenu().removeItem(R.id.action_close_group);
            popupMenu.getMenu().removeItem(R.id.action_group_info);
            popupMenu.getMenu().removeItem(R.id.action_add_people);
            popupMenu.getMenu().removeItem(R.id.action_exit_group);
            popupMenu.getMenu().removeItem(R.id.action_delete_group);
            popupMenu.show();
        }
        return super.onOptionsItemSelected(item);
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(EmployeeActivity.ACTION)) {
                type = intent.getStringExtra("Type");
                if (type != null) {
                    if (type.equals("")) {
                        mPager.setCurrentItem(0);
                    } else if (type.equals("recent")) {
                        mPager.setCurrentItem(0);
                    } else if (type.equals("office")) {
                        mPager.setCurrentItem(1);
                    } else {
                        mPager.setCurrentItem(2);
                    }
                }
                isSlide = intent.getBooleanExtra("isSlide", false);
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(EmployeeActivity.ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }
}
