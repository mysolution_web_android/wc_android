package com.applab.wcircle_pro.Employee;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.ChatRoomActivity;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.UserChatProvider;
import com.applab.wcircle_pro.Chat.xmpp.Config;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

public class OfficeEmployeeFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView recyclerView;
    private EmployeeAdapter adapter;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private String TAG = "OFFICE EMPLOYEE";
    private EditText ediSearch;
    private int pageNo = 1;
    private TextView txtError;
    private Cursor cursor;
    private String selection = null;
    private String[] selectionArgs = null;
    private LoaderManager.LoaderCallbacks callbacks;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.fragment_office_employee, container, false);
        ediSearch = (EditText) layout.findViewById(R.id.ediSearch);
        callbacks = this;
        mSwipyRefreshLayout = (SwipyRefreshLayout) layout.findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        txtError = (TextView) layout.findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);
        adapter = new EmployeeAdapter(getActivity(), cursor, txtError, "Department");
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        setTouchListener();
        HttpHelper.getOfficeGson(pageNo, false, getActivity(), ediSearch.getText().toString(), TAG, txtError);
        SearchWatcher();
        getActivity().getSupportLoaderManager().initLoader(794, null, this);
        return layout;
    }


    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                RelativeLayout rlImage = (RelativeLayout) v.findViewById(R.id.rlImage);
                Employee employee = (Employee) rlImage.getTag();
                if (getActivity().getIntent().getBooleanExtra("isMessenger", false)) {
                    Utils.postRecentTrack(getActivity(), "msg", employee.getId().toString());
                    if (!employee.getId().equals(Utils.getProfile(getActivity()).getId())) {
                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.SHARED_PREFERENCES_NAME, 0);
                        String selfId = sharedPreferences.getString("username", "");
                        Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
                        try {
                            if (!EmployeeActivity.isUserExist(employee.getId(), getActivity())) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID, employee.getId());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_USER_ID, EmployeeActivity.getLastUserId(getActivity()) + 1);
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_IMAGE, employee.getProfileImage());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_NAME, employee.getName());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_OX_USER, employee.getOXUser());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_CONTACT_NO, employee.getContactNo());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_OFFICE_NO, employee.getOfficeNo());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_POSITION, employee.getPosition());
                                contentValues.put(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE, employee.getCountryImage());
                                getActivity().getContentResolver().insert(UserChatProvider.CONTENT_URI, contentValues);
                            }
                        } catch (Exception ex) {
                            ex.fillInStackTrace();
                        } finally {
                            intent.putExtra("user", ChatDBHelper.getSingleUser(getActivity(), employee.getOXUser()));
                        }
                        intent.putExtra("friendId", employee.getOXUser());
                        intent.putExtra("employeeName", employee.getName());
                        intent.putExtra("selfId", selfId);
                        startActivity(intent);
                        getActivity().finish();
                    }
                } else {
                    Intent mIntent = new Intent(getActivity(), EmployeeDetailsActivity.class);
                    mIntent.putExtra("Employee", employee);
                    mIntent.putExtra("Type", "Department");
                    mIntent.putExtra("isFavorite", employee.getFavouriteId() > 0);
                    startActivity(mIntent);
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    private void SearchWatcher() {
        ediSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    pageNo = 1;
                    HttpHelper.getOfficeGson(pageNo, true, getActivity(), ediSearch.getText().toString(), TAG, txtError);
                    if (ediSearch.getText().toString().length() > 0) {
                        selection = DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_NAME + " LIKE ? OR " + DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DEPARTMENT + " LIKE ? ";
                        selectionArgs = new String[]{"%" + ediSearch.getText().toString() + "%", "%" + ediSearch.getText().toString() + "%"};
                        getActivity().getSupportLoaderManager().restartLoader(794, null, callbacks);
                    } else {
                        selection = null;
                        selectionArgs = null;
                        getActivity().getSupportLoaderManager().restartLoader(794, null, callbacks);
                    }
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (lastVisibleItem >= totalItemCount - 1) {
                        pageNo++;
                        HttpHelper.getOfficeGson(pageNo, false, getActivity(), ediSearch.getText().toString(), TAG, txtError);
                    } else if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }
            }
        }
    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getActivity(), false);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getActivity(), false);
        getActivity().getSupportLoaderManager().restartLoader(794, null, this);
        IntentFilter iff = new IntentFilter(EmployeeActivity.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at " + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            pageNo = 1;
            HttpHelper.getOfficeGson(pageNo, false, getActivity(), ediSearch.getText().toString(), TAG, txtError);
        } else {
            pageNo++;
            HttpHelper.getOfficeGson(pageNo, false, getActivity(), ediSearch.getText().toString(), TAG, txtError);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 794) {
            return new CursorLoader(getActivity(), DepartmentEmployeeProvider.CONTENT_URI, null, selection, selectionArgs, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 794) {
            if (data != null) {
                this.cursor = data;
                adapter.swapCursor(cursor);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
