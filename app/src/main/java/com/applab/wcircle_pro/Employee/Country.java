package com.applab.wcircle_pro.Employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Country {

    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("CountryCode")
    @Expose
    private String CountryCode;
    @SerializedName("CountryName")
    @Expose
    private String CountryName;
    @SerializedName("CountryImage")
    @Expose
    private String CountryImage;
    @SerializedName("LastUpdated")
    @Expose
    private String LastUpdated;

    /**
     *
     * @return
     * The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The CountryCode
     */
    public String getCountryCode() {
        return CountryCode;
    }

    /**
     *
     * @param CountryCode
     * The CountryCode
     */
    public void setCountryCode(String CountryCode) {
        this.CountryCode = CountryCode;
    }

    /**
     *
     * @return
     * The CountryName
     */
    public String getCountryName() {
        return CountryName;
    }

    /**
     *
     * @param CountryName
     * The CountryName
     */
    public void setCountryName(String CountryName) {
        this.CountryName = CountryName;
    }

    /**
     *
     * @return
     * The CountryImage
     */
    public String getCountryImage() {
        return CountryImage;
    }

    /**
     *
     * @param CountryImage
     * The CountryImage
     */
    public void setCountryImage(String CountryImage) {
        this.CountryImage = CountryImage;
    }

    /**
     *
     * @return
     * The LastUpdated
     */
    public String getLastUpdated() {
        return LastUpdated;
    }

    /**
     *
     * @param LastUpdated
     * The LastUpdated
     */
    public void setLastUpdated(String LastUpdated) {
        this.LastUpdated = LastUpdated;
    }

}