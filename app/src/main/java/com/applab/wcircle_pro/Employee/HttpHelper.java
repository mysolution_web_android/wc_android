package com.applab.wcircle_pro.Employee;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Favorite.ContactProvider;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by user on 3/12/2015.
 */

public class HttpHelper {
    //region Country region
    public static void getCountryGson(Context context, int pageNoCountry, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String lastUpdate = "2000-09-09T10:16:25z";
        final Date lastUpdateDate = getCountryLastUpdate(context);
        String url = Utils.API_URL + "api/Country?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                + Utils.encode(pageNoCountry) + "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getCountryRow(context) > 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Country?&NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                    + Utils.encode(pageNoCountry) + "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getCountryRow(context) > 0);
        }
        final String finalUrl = url;
        GsonRequest<CountryChecking> mGsonRequest = new GsonRequest<CountryChecking>(
                Request.Method.GET,
                finalUrl,
                CountryChecking.class,
                headers,
                responseCountryListener(context, pageNoCountry, txtError, TAG),
                errorCountryListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<CountryChecking> responseCountryListener(final Context context, final int pageNoCountry, final TextView txtError, final String TAG) {
        return new Response.Listener<CountryChecking>() {
            @Override
            public void onResponse(CountryChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getCountry().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getCountryLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNoCountry == 1) {
                                        context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNoCountry > 1) {
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertCountry(context, response, pageNoCountry, TAG);
                                } else {
                                    if (pageNoCountry == 1) {
                                        context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNoCountry > 1) {
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertCountry(context, response, pageNoCountry, TAG);
                                }
                            } else {
                                if (pageNoCountry == 1) {
                                    context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                } else if (pageNoCountry > 1) {
                                    context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                }
                                insertCountry(context, response, pageNoCountry, TAG);
                            }
                        } else {
                            if (pageNoCountry == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    public static int getCountryRow(Context context) {
        int totalRow = 0;
        String[] projection = {DBHelper.COUNTRY_COLUMN_ID};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CountryProvider.CONTENT_URI, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getCountryLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.COUNTRY_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CountryCheckingProvider.CONTENT_URI, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.COUNTRY_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static boolean isCountryExist(Context context, Object id) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CountryProvider.CONTENT_URI, null,
                    DBHelper.COUNTRY_COLUMN_COUNTRY_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void insertCountry(Context context, CountryChecking result, int pageNoCountry, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesCountryChecking = new ContentValues();
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_LEVEL, pageNoCountry);
            context.getContentResolver().insert(CountryCheckingProvider.CONTENT_URI, contentValuesCountryChecking);
            ContentValues[] contentValueses = new ContentValues[result.getCountry().size()];
            for (int i = 0; i < result.getCountry().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_ID, result.getCountry().get(i).getId());
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_CODE, result.getCountry().get(i).getCountryCode());
                contentValues.put(DBHelper.COUNTRY_COLUMN_LEVEL, pageNoCountry);
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_NAME, result.getCountry().get(i).getCountryName());
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_IMAGE, result.getCountry().get(i).getCountryImage());
                contentValues.put(DBHelper.COUNTRY_COLUMN_LAST_UPDATED, result.getCountry().get(i).getLastUpdated());
                if (isCountryExist(context, result.getCountry().get(i).getId())) {
                    context.getContentResolver().delete(CountryProvider.CONTENT_URI, DBHelper.COUNTRY_COLUMN_COUNTRY_ID + "=?",
                            new String[]{String.valueOf(result.getCountry().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(CountryProvider.CONTENT_URI, contentValueses);
            }

        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Country Error :" + ex.toString());
        }
    }

    public static Response.ErrorListener errorCountryListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

    //endregion
    //region Branch region
    public static void getBranchEmployeeGson(Context context, int pageNoEmployee, boolean isSearch, String countryCode, String searchKey, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(countryCode) + "&NoPerPage=" +
                Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoEmployee) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getBranchEmployeeRow(context, pageNoEmployee, countryCode) > 0);
        final Date lastUpdateDate = getBranchEmployeeLastUpdate(context);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(countryCode) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoEmployee) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getBranchEmployeeRow(context, pageNoEmployee, countryCode) > 0);
        }

        if (isSearch) {
            url = Utils.API_URL + "api/Employee/FilterNameDepartment?SearchKey=" + Utils.encode(searchKey) + "&branchId=" +
                    Utils.encode(Utils.getProfile(context).getDefaultBranchId()) + "&exceptBranch=" + Utils.encode(true) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoEmployee);
        }
        final String finalUrl = url;
        Log.i(TAG, "Url: " + finalUrl);
        GsonRequest<EmployeeChecking> mGsonRequest = new GsonRequest<EmployeeChecking>(
                Request.Method.GET,
                finalUrl,
                EmployeeChecking.class,
                headers,
                responseBranchEmployeeListener(context, pageNoEmployee, isSearch, TAG, searchKey, countryCode, txtError),
                errorBranchEmployeeListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static int getBranchEmployeeRow(Context context, int pageNo, String countryCode) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(BranchEmployeeProvider.CONTENT_URI, null, DBHelper.BRANCH_EMPLOYEE_COLUMN_PAGE_NO + "=? AND " + DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{String.valueOf(pageNo), countryCode}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getBranchEmployeeLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(EmployeeCheckingProvider.CONTENT_URI, projection, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }


    public static Response.Listener<EmployeeChecking> responseBranchEmployeeListener(final Context context, final int pageNoEmployee, final boolean isSearch, final String TAG, final String searchKey, final String countryCode, final TextView txtError) {
        return new Response.Listener<EmployeeChecking>() {
            @Override
            public void onResponse(EmployeeChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getEmployees().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getBranchEmployeeLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNoEmployee == 1) {
                                        context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNoEmployee > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNoEmployee, isSearch, TAG, searchKey, countryCode);
                                } else {
                                    if (pageNoEmployee == 1) {
                                        context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNoEmployee > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNoEmployee, isSearch, TAG, searchKey, countryCode);
                                }
                            } else {
                                if (pageNoEmployee == 1) {
                                    context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                } else if (pageNoEmployee > 1) {
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                }
                                insertEmployee(context, response, pageNoEmployee, isSearch, TAG, searchKey, countryCode);
                            }
                        } else {
                            if (pageNoEmployee == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                }
                            }
                        }
                    }
                }
            }

        };
    }

    public static void insertEmployee(Context context, EmployeeChecking result, int pageNoBranch, boolean isSearch, String TAG, String searchKey, String countryCode) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesEmployeeChecking = new ContentValues();
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH, String.valueOf(true));
            context.getContentResolver().insert(EmployeeCheckingProvider.CONTENT_URI, contentValuesEmployeeChecking);
            ContentValues[] contentValueses = new ContentValues[result.getEmployees().size()];
            for (int i = 0; i < result.getEmployees().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID, result.getEmployees().get(i).getId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IMAGE, result.getEmployees().get(i).getProfileImage());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_BRANCH_ID, result.getEmployees().get(i).getDefaultBranchId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_PAGE_NO, pageNoBranch);
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_BRANCH_NAME, result.getEmployees().get(i).getDefaultBranchName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMAIL, result.getEmployees().get(i).getEmail());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_DEPARTMENT, result.getEmployees().get(i).getDepartment());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_NAME, result.getEmployees().get(i).getName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_CONTACT_NO, result.getEmployees().get(i).getContactNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_GENDER, result.getEmployees().get(i).getGender());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY, result.getEmployees().get(i).getCountry());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY_IMAGE, result.getEmployees().get(i).getCountryImage());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_OXUSER, result.getEmployees().get(i).getOXUser());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_POSITION, result.getEmployees().get(i).getPosition());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_ALL_BRANCH, result.getEmployees().get(i).getAllBranch());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_LAST_UPDATED, result.getEmployees().get(i).getLastUpdated());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_NO, result.getEmployees().get(i).getEmployeeNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY_NAME, result.getEmployees().get(i).getCountryName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_DOB, result.getEmployees().get(i).getDOB());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_OFFICE_NO, result.getEmployees().get(i).getOfficeNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_HOD_ID, result.getEmployees().get(i).getHODId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_HOD_NAME, result.getEmployees().get(i).getHODName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IS_SELECTED, result.getEmployees().get(i).getFavouriteId() > 0 ? String.valueOf(true) : String.valueOf(false));
                if (isBanchExists(result.getEmployees().get(i).getId(), context)) {
                    context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI,
                            DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(result.getEmployees().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(BranchEmployeeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    public static boolean isBanchExists(Object employeeId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(BranchEmployeeProvider.CONTENT_URI, null,
                    DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?",
                    new String[]{String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static Response.ErrorListener errorBranchEmployeeListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

    //endregion
    //region Office region
    public static void getOfficeGson(int pageNo, boolean isSearch, Context context, String searchKey, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(Utils.getProfile(context).getCountry()) + "&NoPerPage=" +
                Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getOfficeEmployeeRow(context, pageNo) > 0);
        final Date lastUpdateDate = getOfficeLastUpdate(context);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(Utils.getProfile(context).getCountry()) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getOfficeEmployeeRow(context, pageNo) > 0);
        }
        if (isSearch) {
            url = Utils.API_URL + "api/Employee/FilterNameDepartment?SearchKey=" + Utils.encode(searchKey) + "&branchId=" +
                    Utils.encode(Utils.getProfile(context).getDefaultBranchId()) + "&exceptBranch=" + Utils.encode(false) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo);
        }
        final String finalUrl = url;
        GsonRequest<EmployeeChecking> mGsonRequest = new GsonRequest<EmployeeChecking>(
                Request.Method.GET,
                finalUrl,
                EmployeeChecking.class,
                headers,
                responseOfficeListener(context, pageNo, txtError, TAG, isSearch, searchKey),
                errorOfficeListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static int getOfficeEmployeeRow(Context context, int pageNo) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DepartmentEmployeeProvider.CONTENT_URI, null, DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_PAGE_NO + "=?", new String[]{String.valueOf(pageNo)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getOfficeLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DepartmentEmployeeProvider.CONTENT_URI, projection, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static Response.ErrorListener errorOfficeListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }

    public static Response.Listener<EmployeeChecking> responseOfficeListener(final Context context, final int pageNo, final TextView txtError, final String TAG, final boolean isSearch, final String searchKey) {
        return new Response.Listener<EmployeeChecking>() {
            @Override
            public void onResponse(EmployeeChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getEmployees().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getOfficeLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNo, TAG, isSearch, searchKey);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNo, TAG, isSearch, searchKey);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                }
                                insertEmployee(context, response, pageNo, TAG, isSearch, searchKey);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                }
                            }
                        }
                    }
                }
            }

        };
    }

    public static void insertEmployee(Context context, EmployeeChecking result, int pageNo, String TAG, boolean isSearch, String searchKey) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesEmployeeChecking = new ContentValues();
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_PAGE_NO, pageNo);
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT, String.valueOf(true));
            context.getContentResolver().insert(EmployeeCheckingProvider.CONTENT_URI, contentValuesEmployeeChecking);
            ContentValues[] contentValueses = new ContentValues[result.getEmployees().size()];
            for (int i = 0; i < result.getEmployees().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID, result.getEmployees().get(i).getId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IMAGE, result.getEmployees().get(i).getProfileImage());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_ID, result.getEmployees().get(i).getDefaultBranchId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_PAGE_NO, pageNo);
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_NAME, result.getEmployees().get(i).getDefaultBranchName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMAIL, result.getEmployees().get(i).getEmail());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DEPARTMENT, result.getEmployees().get(i).getDepartment());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_NAME, result.getEmployees().get(i).getName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_CONTACT_NO, result.getEmployees().get(i).getContactNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_GENDER, result.getEmployees().get(i).getGender());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY, result.getEmployees().get(i).getCountry());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE, result.getEmployees().get(i).getCountryImage());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_OXUSER, result.getEmployees().get(i).getOXUser());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_POSITION, result.getEmployees().get(i).getPosition());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_ALL_BRANCH, result.getEmployees().get(i).getAllBranch());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_LAST_UPDATED, result.getEmployees().get(i).getLastUpdated());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_NO, result.getEmployees().get(i).getEmployeeNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_NAME, result.getEmployees().get(i).getCountryName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DOB, result.getEmployees().get(i).getDOB());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_OFFICE_NO, result.getEmployees().get(i).getOfficeNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_HOD_ID, result.getEmployees().get(i).getHODId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_HOD_NAME, result.getEmployees().get(i).getHODName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IS_SELECTED, result.getEmployees().get(i).getFavouriteId() > 0 ? String.valueOf(true) : String.valueOf(false));
                if (isOfficeExists(result.getEmployees().get(i).getId(), context)) {
                    context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(result.getEmployees().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(DepartmentEmployeeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    public static boolean isOfficeExists(Object employeeId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DepartmentEmployeeProvider.CONTENT_URI, null,
                    DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=? ", new String[]{String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    //endregion
    //region Recent region
    public static void getRecentGson(int pageNo, Context context, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String lastUpdate = "2000-09-09T10:16:25z";
        final Date lastUpdateDate = getRecentLastUpdate(context);
        String url = Utils.API_URL + "api/Employee/RecentContact?&NoPerPage=" + Utils.PAGING_NUMBER + "&PageNo=" + Utils.encode(pageNo) + "&LastUpdated=" +
                Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getActionsRow(context, pageNo) > 0) + "";
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Employee/RecentContact?&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo) +
                    "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getActionsRow(context, pageNo) > 0) + "";
        }
        final String finalUrl = url;
        GsonRequest<RecentEmployeeChecking> mGsonRequest = new GsonRequest<RecentEmployeeChecking>(
                Request.Method.GET,
                finalUrl,
                RecentEmployeeChecking.class,
                headers,
                responseRecentListener(context, pageNo, txtError, TAG),
                errorRecentListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.ErrorListener errorRecentListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

    public static Response.Listener<RecentEmployeeChecking> responseRecentListener(final Context context, final int pageNo, final TextView txtError, final String TAG) {
        return new Response.Listener<RecentEmployeeChecking>() {
            @Override
            public void onResponse(RecentEmployeeChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getActions().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getOfficeLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(RecentEmployeeProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_RECENT + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_RECENT + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(response, context, pageNo, TAG);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(RecentEmployeeProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_RECENT + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_RECENT + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(response, context, pageNo, TAG);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(RecentEmployeeProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_RECENT + "=?", new String[]{String.valueOf(true)});
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_RECENT + "=?", new String[]{String.valueOf(true)});
                                }
                                insertEmployee(response, context, pageNo, TAG);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(RecentEmployeeProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_RECENT + "=?", new String[]{String.valueOf(true)});
                                }
                            }
                        }
                    }
                }

            }
        };
    }

    public static boolean isRecentExists(Object actionId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(RecentEmployeeProvider.CONTENT_URI, null,
                    DBHelper.RECENT_EMPLOYEE_COLUMN_ACTION_ID + "=?",
                    new String[]{String.valueOf(actionId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void insertEmployee(RecentEmployeeChecking result, Context context, int pageNo, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesEmployeeChecking = new ContentValues();
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_RECENT, String.valueOf(true));
            context.getContentResolver().insert(EmployeeCheckingProvider.CONTENT_URI, contentValuesEmployeeChecking);
            ContentValues[] contentValueses = new ContentValues[result.getActions().size()];
            for (int i = 0; i < result.getActions().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID, result.getActions().get(i).getItem().getId());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_ACTION_ID, result.getActions().get(i).getId());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getActions().get(i).getItem().getFavouriteId());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_IMAGE, result.getActions().get(i).getItem().getProfileImage());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getActions().get(i).getItem().getFavouriteId());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_BRANCH_ID, result.getActions().get(i).getItem().getDefaultBranchId());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_PAGE_NO, pageNo);
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_BRANCH_NAME, result.getActions().get(i).getItem().getDefaultBranchName());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_EMAIL, result.getActions().get(i).getItem().getEmail());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_DEPARTMENT, result.getActions().get(i).getItem().getDepartment());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_NAME, result.getActions().get(i).getItem().getName());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_CONTACT_NO, result.getActions().get(i).getItem().getContactNo());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_GENDER, result.getActions().get(i).getItem().getGender());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_COUNTRY, result.getActions().get(i).getItem().getCountry());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE, result.getActions().get(i).getItem().getCountryImage());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_OXUSER, result.getActions().get(i).getItem().getOXUser());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_POSITION, result.getActions().get(i).getItem().getPosition());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_ALL_BRANCH, result.getActions().get(i).getItem().getAllBranch());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_LAST_UPDATED, result.getActions().get(i).getItem().getLastUpdated());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_ACTION, result.getActions().get(i).getAction());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_CREATE_DATE, result.getActions().get(i).getCreateDate());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_NO, result.getActions().get(i).getItem().getEmployeeNo());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_COUNTRY_NAME, result.getActions().get(i).getItem().getCountryName());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_DOB, result.getActions().get(i).getItem().getDOB());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_OFFICE_NO, result.getActions().get(i).getItem().getOfficeNo());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_HOD_ID, result.getActions().get(i).getItem().getHODId());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_HOD_NAME, result.getActions().get(i).getItem().getHODName());
                if (isRecentExists(result.getActions().get(i).getItem().getId(), context)) {
                    context.getContentResolver().delete(RecentEmployeeProvider.CONTENT_URI, DBHelper.RECENT_EMPLOYEE_COLUMN_ACTION_ID + "=?",
                            new String[]{String.valueOf(result.getActions().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(RecentEmployeeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    public static int getActionsRow(Context context, int pageNo) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(RecentEmployeeProvider.CONTENT_URI, null, DBHelper.RECENT_EMPLOYEE_COLUMN_PAGE_NO + "=?", new String[]{String.valueOf(pageNo)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getRecentLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(EmployeeCheckingProvider.CONTENT_URI, projection, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_RECENT + "=?", new String[]{String.valueOf(true)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    //endregion
    //region Single Employee region
    public static void getSingleEmployeeGson(Context context, Object id, TextView txtError, String TAG, String module) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Employee> mGsonRequest = new GsonRequest<Employee>(
                Request.Method.GET,
                Utils.API_URL + "api/Employee" + "/" + Utils.encode(id),
                Employee.class,
                headers,
                responseSingleEmployeeListener(context, txtError, module),
                errorSingleEmployeeListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Employee> responseSingleEmployeeListener(final Context context, final TextView txtError, final String module) {
        return new Response.Listener<Employee>() {
            @Override
            public void onResponse(Employee response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        insertEmployee(response, context, module);
                    }
                }
            }
        };
    }

    public static void insertEmployee(Employee result, Context context, String module) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValues = new ContentValues();
            if (module.equals("Branch")) {
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID, result.getId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_FAVORITE_ID, result.getFavouriteId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IMAGE, result.getProfileImage());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_FAVORITE_ID, result.getFavouriteId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_BRANCH_ID, result.getDefaultBranchId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_BRANCH_NAME, result.getDefaultBranchName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMAIL, result.getEmail());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_DEPARTMENT, result.getDepartment());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_NAME, result.getName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_CONTACT_NO, result.getContactNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_GENDER, result.getGender());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY, result.getCountry());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY_IMAGE, result.getCountryImage());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_OXUSER, result.getOXUser());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_POSITION, result.getPosition());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_ALL_BRANCH, result.getAllBranch());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_LAST_UPDATED, result.getLastUpdated());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_NO, result.getEmployeeNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY_NAME, result.getCountryName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_DOB, result.getDOB());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_OFFICE_NO, result.getOfficeNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_HOD_ID, result.getHODId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_HOD_NAME, result.getHODName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IS_SELECTED, result.getFavouriteId() > 0 ? String.valueOf(true) : String.valueOf(false));
            } else if (module.equals("Department")) {
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID, result.getId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getFavouriteId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IMAGE, result.getProfileImage());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getFavouriteId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_ID, result.getDefaultBranchId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_NAME, result.getDefaultBranchName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMAIL, result.getEmail());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DEPARTMENT, result.getDepartment());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_NAME, result.getName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_CONTACT_NO, result.getContactNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_GENDER, result.getGender());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY, result.getCountry());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE, result.getCountryImage());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_OXUSER, result.getOXUser());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_POSITION, result.getPosition());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_ALL_BRANCH, result.getAllBranch());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_LAST_UPDATED, result.getLastUpdated());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_NO, result.getEmployeeNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_NAME, result.getCountryName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DOB, result.getDOB());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_OFFICE_NO, result.getOfficeNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_HOD_ID, result.getHODId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_HOD_NAME, result.getHODName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IS_SELECTED, result.getFavouriteId() > 0 ? String.valueOf(true) : String.valueOf(false));
            } else if (module.equals("Recent")) {
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID, result.getId());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getFavouriteId());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_IMAGE, result.getProfileImage());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getFavouriteId());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_BRANCH_ID, result.getDefaultBranchId());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_BRANCH_NAME, result.getDefaultBranchName());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_EMAIL, result.getEmail());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_DEPARTMENT, result.getDepartment());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_NAME, result.getName());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_CONTACT_NO, result.getContactNo());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_GENDER, result.getGender());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_COUNTRY, result.getCountry());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE, result.getCountryImage());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_OXUSER, result.getOXUser());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_POSITION, result.getPosition());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_ALL_BRANCH, result.getAllBranch());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_LAST_UPDATED, result.getLastUpdated());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_NO, result.getEmployeeNo());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_COUNTRY_NAME, result.getCountryName());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_DOB, result.getDOB());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_OFFICE_NO, result.getOfficeNo());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_HOD_ID, result.getHODId());
                contentValues.put(DBHelper.RECENT_EMPLOYEE_COLUMN_HOD_NAME, result.getHODName());
            }
            if (isSingleEmployeeExists(result.getId(), context, module)) {
                if (module.equals("Recent")) {
                    context.getContentResolver().update(RecentEmployeeProvider.CONTENT_URI, contentValues,
                            DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(result.getId())});
                } else if (module.equals("Department")) {
                    context.getContentResolver().update(DepartmentEmployeeProvider.CONTENT_URI, contentValues,
                            DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(result.getId())});
                } else if (module.equals("Branch")) {
                    context.getContentResolver().update(BranchEmployeeProvider.CONTENT_URI, contentValues,
                            DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(result.getId())});
                }
            } else {
                if (module.equals("Recent")) {
                    context.getContentResolver().insert(RecentEmployeeProvider.CONTENT_URI, contentValues);
                } else if (module.equals("Department")) {
                    context.getContentResolver().insert(DepartmentEmployeeProvider.CONTENT_URI, contentValues);
                } else if (module.equals("Branch")) {
                    context.getContentResolver().insert(BranchEmployeeProvider.CONTENT_URI, contentValues);
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
    }

    public static boolean isSingleEmployeeExists(Object employeeId, Context context, String module) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            if (module.equals("Recent")) {
                cursor = context.getContentResolver().query(RecentEmployeeProvider.CONTENT_URI, null,
                        DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?",
                        new String[]{String.valueOf(employeeId)}, null);
            } else if (module.equals("Department")) {
                cursor = context.getContentResolver().query(DepartmentEmployeeProvider.CONTENT_URI, null,
                        DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?",
                        new String[]{String.valueOf(employeeId)}, null);
            } else if (module.equals("Branch")) {
                cursor = context.getContentResolver().query(BranchEmployeeProvider.CONTENT_URI, null,
                        DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?",
                        new String[]{String.valueOf(employeeId)}, null);
            }
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static Response.ErrorListener errorSingleEmployeeListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

    //endregion
    //region Post Favorite region
    public static void postFavorite(final Context context, final String employeeId, final String contactId, final String type, final String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Employee/FavouriteContact",
                JsonObject.class,
                headers,
                responseListenerFavorite(context, contactId, txtError, TAG),
                errorListenerFavorite(context)) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "EmployeeId=" + employeeId + "&ContactId=" + contactId + "&Type=" + type;
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JsonObject> responseListenerFavorite(final Context context, final String id, final TextView txtError, final String TAG) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IS_SELECTED, String.valueOf(true));
                context.getContentResolver().update(BranchEmployeeProvider.CONTENT_URI, contentValues, DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{id});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IS_SELECTED, String.valueOf(true));
                context.getContentResolver().update(DepartmentEmployeeProvider.CONTENT_URI, contentValues, DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{id});
            }
        };
    }

    public static Response.ErrorListener errorListenerFavorite(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }

    //endregion
    //region Delete Favorite region
    public static void deleteFavorite(final String employeeId, Context context, String TAG, final TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String url = Utils.API_URL + "api/Employee/FavouriteContact/" + Utils.encode(employeeId);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.DELETE,
                Utils.API_URL + "api/Employee/FavouriteContact/" + Utils.encode(employeeId),
                JsonObject.class,
                headers,
                responseListenerDeleteFavorite(employeeId, context, TAG, txtError),
                errorListenerDeleteFavorite(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JsonObject> responseListenerDeleteFavorite(final String id, final Context context, final String TAG, final TextView txtError) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                context.getContentResolver().delete(ContactProvider.CONTENT_URI, DBHelper.CONTACT_COLUMN_ID + "=?", new String[]{id});
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IS_SELECTED, String.valueOf(false));
                context.getContentResolver().update(BranchEmployeeProvider.CONTENT_URI, contentValues, DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{id});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IS_SELECTED, String.valueOf(false));
                context.getContentResolver().update(DepartmentEmployeeProvider.CONTENT_URI, contentValues, DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{id});
            }
        };
    }

    public static Response.ErrorListener errorListenerDeleteFavorite(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }

    //endregion
    //region Delete Single Recent region
    public static void deleteSingleRecent(String id, Context context, String TAG) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.DELETE,
                Utils.API_URL + "api/Employee/RecentContact/" + Utils.encode(id),
                JSONObject.class,
                headers,
                responseListenerDeleteSingleRecent(id, context),
                errorListenerDeleteSingleRecent(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerDeleteSingleRecent(final String id, final Context context) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                context.getContentResolver().delete(RecentEmployeeProvider.CONTENT_URI, DBHelper.RECENT_EMPLOYEE_COLUMN_ACTION_ID + "=?", new String[]{id});
            }
        };
    }

    public static Response.ErrorListener errorListenerDeleteSingleRecent(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }

    //endregion
    //region Delete All Recent region
    public static void deleteRecent(Context context, String TAG) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.DELETE,
                Utils.API_URL + "api/Employee/RecentContact",
                JSONObject.class,
                headers,
                responseDeleteAllRecentListener(context),
                errorDeleteAllRecentListener(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseDeleteAllRecentListener(final Context context) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                context.getContentResolver().delete(RecentEmployeeProvider.CONTENT_URI, null, null);
            }
        };
    }

    public static Response.ErrorListener errorDeleteAllRecentListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (error instanceof ServerError) {

                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }
    //endregion
}
