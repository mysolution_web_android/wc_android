package com.applab.wcircle_pro.Employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Action {

    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("Action")
    @Expose
    private String Action;
    @SerializedName("CreateDate")
    @Expose
    private String CreateDate;
    @SerializedName("Item")
    @Expose
    private Employee Item;

    /**
     *
     * @return
     * The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The Action
     */
    public String getAction() {
        return Action;
    }

    /**
     *
     * @param Action
     * The Action
     */
    public void setAction(String Action) {
        this.Action = Action;
    }

    /**
     *
     * @return
     * The CreateDate
     */
    public String getCreateDate() {
        return CreateDate;
    }

    /**
     *
     * @param CreateDate
     * The CreateDate
     */
    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    /**
     *
     * @return
     * The Item
     */
    public Employee getItem() {
        return Item;
    }

    /**
     *
     * @param Item
     * The Item
     */
    public void setItem(Employee Item) {
        this.Item = Item;
    }

}