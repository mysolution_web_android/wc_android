package com.applab.wcircle_pro.Employee;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.applab.wcircle_pro.R;

class EmployeePagerAdapter extends FragmentPagerAdapter {
    String[] tabs;

    public EmployeePagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        tabs = context.getResources().getStringArray(R.array.employee_tabs);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment item;
        RecentEmployeeFragment recentEmployeeFragment = new RecentEmployeeFragment();
        BranchEmployeeFragment branchEmployeeFragment = new BranchEmployeeFragment();
        OfficeEmployeeFragment officeEmployeeFragment = new OfficeEmployeeFragment();
        if (position == 0) {
            item = recentEmployeeFragment;
        } else if (position == 1) {
            item = officeEmployeeFragment;
        } else {
            item = branchEmployeeFragment;
        }
        return item;
    }

    @Override
    public int getCount() {
        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }
}