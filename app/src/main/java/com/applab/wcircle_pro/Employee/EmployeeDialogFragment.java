package com.applab.wcircle_pro.Employee;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

/**
 * Created by user on 31/10/2015.
 */
public class EmployeeDialogFragment extends DialogFragment {
    private ImageView mImgIcon, mImgCountry, mBtnMobile, mBtnOffice, mBtnCancel;
    private TextView mTxtName, mTxtPosition, mTxtMobile, mTxtOffice;
    private Employee mEmployee;
    private static AlertDialog.Builder builder;
    private static Context mContext;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static EmployeeDialogFragment newInstance(Employee employee, Context context) {
        EmployeeDialogFragment frag = new EmployeeDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("employee", employee);
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mEmployee = getArguments().getParcelable("employee");

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_call, null);

        mBtnMobile = (ImageView) v.findViewById(R.id.btnMobile);
        mBtnOffice = (ImageView) v.findViewById(R.id.btnOffice);

        mBtnMobile.setOnClickListener(btnMobileOnClickListener);
        mBtnOffice.setOnClickListener(btnOfficeOnClickListener);

        mTxtName = (TextView) v.findViewById(R.id.txtName);
        mTxtName.setText(mEmployee.getName());
        mTxtPosition = (TextView) v.findViewById(R.id.txtPosition);
        mTxtPosition.setText(mEmployee.getPosition());
        mTxtOffice = (TextView) v.findViewById(R.id.txtOffice);
        mTxtOffice.setText(mEmployee.getOfficeNo());
        mTxtMobile = (TextView) v.findViewById(R.id.txtMobile);
        mTxtMobile.setText(mEmployee.getContactNo());

        DisplayMetrics mDisplayMetrics = getActivity().getResources().getDisplayMetrics();
        int mHeight = mDisplayMetrics.heightPixels;
        int mWidth = mDisplayMetrics.widthPixels;

        mImgIcon = (ImageView) v.findViewById(R.id.imgIcon);
        mImgCountry = (ImageView) v.findViewById(R.id.imgCountry);

        Glide.with(getActivity())
                .load(mEmployee.getProfileImage())
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleTransform(mContext))
                .placeholder(R.mipmap.ic_action_person_light)
                .into(new SimpleTarget<Bitmap>(mWidth, mHeight) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        mImgIcon.setImageBitmap(bitmap);
                    }
                });

        Glide.with(getActivity())
                .load(mEmployee.getCountryImage())
                .asBitmap()
                .transform(new CircleTransform(mContext))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new SimpleTarget<Bitmap>(mWidth, mHeight) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        mImgCountry.setImageBitmap(bitmap);
                    }
                });

        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnMobileOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mEmployee.getContactNo() != null) {
                if (!mEmployee.getContactNo().equals("")) {
                    Utils.postRecentTrack(mContext, "call", mEmployee.getId().toString());
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mEmployee.getContactNo()));
                    startActivity(intent);
                }
            }
        }
    };

    private View.OnClickListener btnOfficeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mEmployee.getOfficeNo() != null) {
                if (!mEmployee.getOfficeNo().equals("")) {
                    Utils.postRecentTrack(mContext, "call", mEmployee.getId().toString());
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mEmployee.getOfficeNo()));
                    startActivity(intent);
                }
            }
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            EmployeeDialogFragment.this.getDialog().cancel();
        }
    };
}
