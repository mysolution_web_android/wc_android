package com.applab.wcircle_pro.Employee;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.ChatRoomActivity;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.UserChatProvider;
import com.applab.wcircle_pro.Chat.xmpp.Config;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.RoundImage;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

public class EmployeeDetailsActivity extends AppCompatActivity implements SwipyRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {
    private ImageView imgAvatar, imgCountry;
    private Toolbar toolbar;
    private TextView txtName, txtDep, txtPosition, txtEmail, txtMobile,
            txtOffice, txtDOB, txtHOD, txtEmployeeNo;
    private LinearLayout btnEmail, btnCall, btnMessage, lv;
    private ImageView btnFavorite;
    private String TAG = "EMPLOYEE";
    private ScrollView scrollView;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private Employee employee = new Employee();
    private Bundle bundle;
    private TextView txtError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_details);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green));
        TextView txtToolbarTitle = (TextView) findViewById(R.id.txtTitle);
        txtToolbarTitle.setText("Staff Directory");
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        bundle = getIntent().getExtras();
        txtError = (TextView) findViewById(R.id.txtError);
        employee = bundle.getParcelable("Employee");
        getSupportLoaderManager().initLoader(0, null, this);
        btnEmail = (LinearLayout) findViewById(R.id.btnEmail);
        btnCall = (LinearLayout) findViewById(R.id.btnCall);
        btnMessage = (LinearLayout) findViewById(R.id.btnMessage);
        imgAvatar = (ImageView) findViewById(R.id.imgAvatar);
        imgCountry = (ImageView) findViewById(R.id.imgCountry);
        btnEmail.setOnClickListener(btnEmailOnClickListener);
        btnMessage.setOnClickListener(btnMessageOnClickListener);
        btnCall.setOnClickListener(btnCallOnClickListener);
        Bitmap bm = BitmapFactory.decodeResource(this.getResources(), R.mipmap.neo);
        RoundImage roundImage = new RoundImage();
        imgAvatar.setImageBitmap(roundImage.getCircleBitmap(bm));
        imgAvatar.setOnClickListener(imgAvatarOnClickListener);
        txtName = (TextView) findViewById(R.id.txtName);
        txtDep = (TextView) findViewById(R.id.txtDep);
        txtPosition = (TextView) findViewById(R.id.txtPosition);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtMobile = (TextView) findViewById(R.id.txtMobile);
        txtOffice = (TextView) findViewById(R.id.txtOffice);
        txtDOB = (TextView) findViewById(R.id.txtDOB);
        txtHOD = (TextView) findViewById(R.id.txtHOD);
        txtEmployeeNo = (TextView) findViewById(R.id.txtEmployeeNo);
        btnFavorite = (ImageView) findViewById(R.id.btnFavorite);
        btnFavorite.setOnClickListener(btnFavoriteOnClickListener);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        lv = (LinearLayout) findViewById(R.id.lv);
        scrollViewScrollChangedListener();
        if (employee.getIsSelect() || bundle.getBoolean("isFavorite")) {
            btnFavorite.setImageResource(R.mipmap.info_star_active);
            btnFavorite.setTag("true");
        } else {
            btnFavorite.setImageResource(R.mipmap.info_star_inactive);
            btnFavorite.setTag("false");
        }
        if (Utils.getProfile(this).getId().equals(employee.getId())) {
            btnFavorite.setVisibility(View.GONE);
            lv.setVisibility(View.GONE);
        } else {
            btnFavorite.setVisibility(View.VISIBLE);
            lv.setVisibility(View.VISIBLE);
        }
        if (bundle.getString("Type") != null) {
            if (bundle.getString("Type").equals("Recent")) {
                btnFavorite.setVisibility(View.GONE);
            }
        }
    }

    private void scrollViewScrollChangedListener() {
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY();
                if (scrollY == 0) {
                    if (!mSwipyRefreshLayout.isRefreshing()) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }
            }
        });
    }

    private LinearLayout.OnClickListener imgAvatarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getBaseContext(), EmployeeImageSlidingActivity.class);
            intent.putExtra("id", String.valueOf(employee.getId()));
            intent.putExtra("position", 0);
            String url = employee.getProfileImage();
            if (url == null) {
                url = "";
            }
            intent.putExtra("urlImage", url);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getBaseContext().startActivity(intent);
        }
    };

    private LinearLayout.OnClickListener btnEmailOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent email = new Intent(Intent.ACTION_SEND);
            Utils.postRecentTrack(EmployeeDetailsActivity.this, "email", employee.getId().toString());
            email.putExtra(Intent.EXTRA_EMAIL, new String[]{employee.getEmail()});
            email.putExtra(Intent.EXTRA_SUBJECT, "");
            email.putExtra(Intent.EXTRA_TEXT, "");
            email.setType("message/rfc822");
            startActivity(Intent.createChooser(email, Utils.CHOOSE_CLIENT));
        }
    };

    private LinearLayout.OnClickListener btnMessageOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Utils.postRecentTrack(EmployeeDetailsActivity.this, "msg", employee.getId().toString());
            SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREFERENCES_NAME, 0);
            String selfId = sharedPreferences.getString("username", "");
            Intent intent = new Intent(EmployeeDetailsActivity.this, ChatRoomActivity.class);
            try {
                if (!EmployeeActivity.isUserExist(employee.getId(), EmployeeDetailsActivity.this)) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID, employee.getId());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_USER_ID, EmployeeActivity.getLastUserId(EmployeeDetailsActivity.this) + 1);
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_IMAGE, employee.getProfileImage());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_NAME, employee.getName());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_OX_USER, employee.getOXUser());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_CONTACT_NO, employee.getContactNo());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_OFFICE_NO, employee.getOfficeNo());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_POSITION, employee.getPosition());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE, employee.getCountryImage());
                    getContentResolver().insert(UserChatProvider.CONTENT_URI, contentValues);
                }
            } catch (Exception ex) {
                ex.fillInStackTrace();
            } finally {
                intent.putExtra("user", ChatDBHelper.getSingleUser(EmployeeDetailsActivity.this, employee.getOXUser()));
            }
            intent.putExtra("friendId", employee.getOXUser());
            intent.putExtra("employeeName", employee.getName());
            intent.putExtra("selfId", selfId);
            startActivity(intent);
            finish();
        }
    };

    private LinearLayout.OnClickListener btnCallOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            EmployeeDialogFragment employeeDialogFragment = EmployeeDialogFragment.newInstance(employee, EmployeeDetailsActivity.this);
            employeeDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    private LinearLayout.OnClickListener btnFavoriteOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (Utils.isConnectingToInternet(EmployeeDetailsActivity.this)) {
                if (btnFavorite.getTag().equals("true")) {
                    btnFavorite.setTag("false");
                    btnFavorite.setImageResource(R.mipmap.info_star_inactive);
                    HttpHelper.deleteFavorite(employee.getId().toString(), EmployeeDetailsActivity.this, TAG, txtError);
                } else {
                    btnFavorite.setTag("true");
                    btnFavorite.setImageResource(R.mipmap.info_star_active);
                    HttpHelper.postFavorite(EmployeeDetailsActivity.this, Utils.getProfile(EmployeeDetailsActivity.this).getId().toString(), employee.getId().toString(), "C", TAG, txtError);
                }
            } else {
                Utils.showNoConnection(EmployeeDetailsActivity.this);
            }
        }
    };

    @Override
    public void onBackPressed() {
        Intent intentBroadcast = new Intent(EmployeeActivity.ACTION);
        intentBroadcast.putExtra("Type", bundle.getString("Type"));
        EmployeeActivity.mgr.sendBroadcast(intentBroadcast);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_employee_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            Intent intentBroadcast = new Intent(EmployeeActivity.ACTION);
            intentBroadcast.putExtra("Type", bundle.getString("Type"));
            EmployeeActivity.mgr.sendBroadcast(intentBroadcast);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d("MainActivity", "Refresh triggered at " + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            HttpHelper.getSingleEmployeeGson(EmployeeDetailsActivity.this, employee.getId(), txtError, TAG, bundle.getString("Type"));
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (bundle.getString("Type").equals("Recent")) {
            return new CursorLoader(getBaseContext(), RecentEmployeeProvider.CONTENT_URI, null, DBHelper.RECENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?",
                    new String[]{String.valueOf(employee.getId())}, null);
        } else if (bundle.getString("Type").equals("Branch")) {
            return new CursorLoader(getBaseContext(), BranchEmployeeProvider.CONTENT_URI, null, DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?",
                    new String[]{String.valueOf(employee.getId())}, null);
        } else if (bundle.get("Type").equals("Department")) {
            return new CursorLoader(getBaseContext(), DepartmentEmployeeProvider.CONTENT_URI, null, DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?",
                    new String[]{String.valueOf(employee.getId())}, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            if (data.getCount() > 0) {
                if (bundle.getString("Type").equals("Branch")) {
                    employee = Employee.getBranchData(data, 0);
                } else if (bundle.getString("Type").equals("Department")) {
                    employee = Employee.getDepartmentData(data, 0);
                } else if (bundle.getString("Type").equals("Recent")) {
                    employee = Employee.getRecentData(data, 0);
                }
                txtName.setTag(employee.getId());
                txtName.setText(employee.getName());
                txtDep.setText(employee.getDepartment());
                txtPosition.setText(employee.getPosition());
                txtEmail.setText(employee.getEmail());
                txtMobile.setText(employee.getContactNo());
                txtOffice.setText(employee.getOfficeNo());
                txtDOB.setText(employee.getDOB());
                txtHOD.setText(employee.getHODName());
                txtEmployeeNo.setText(employee.getEmployeeNo());
                if (employee.getIsSelect()) {
                    btnFavorite.setImageResource(R.mipmap.info_star_active);
                    btnFavorite.setTag("true");
                } else {
                    btnFavorite.setImageResource(R.mipmap.info_star_inactive);
                    btnFavorite.setTag("false");
                }
                Glide.with(EmployeeDetailsActivity.this)
                        .load(employee.getProfileImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(new CircleTransform(EmployeeDetailsActivity.this))
                        .placeholder(R.mipmap.ic_action_person_dark)
                        .into(imgAvatar);
                Glide.with(EmployeeDetailsActivity.this)
                        .load(employee.getCountryImage())
                        .transform(new CircleTransform(EmployeeDetailsActivity.this))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgCountry);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
