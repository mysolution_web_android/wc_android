package com.applab.wcircle_pro.Leave;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Calendar.CalendarActivity;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.Date;

public class EditApplicationActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, SwipyRefreshLayout.OnRefreshListener {
    private Toolbar toolbar;
    private TextView txtName, txtDep, txtPosition, txtApprover,
            txtType, txtStatus, txtRemarks, txtNoDays,
            txtFromDate, txtToDate, txtReason, txtToolbarTitle, txtError, txtAppliedDate;
    private LinearLayout btnCancel, btnEdit, btnCancel2, btn, btnButton, btnButton1, btnApprove, btnReject;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private boolean isCancelled = false;
    private boolean isEmployeeLeave = false;
    private boolean isBoss = false;
    private String TAG = "LEAVE";
    private Cursor cursor;
    private int page = 0;
    private Leave leave;
    private int myType = 1;
    private int employeeType = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_application);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green_blue));
        txtToolbarTitle = (TextView) findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_edit_application));
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        txtError = (TextView) findViewById(R.id.txtError);
        txtName = (TextView) findViewById(R.id.txtName);
        txtDep = (TextView) findViewById(R.id.txtDep);
        txtPosition = (TextView) findViewById(R.id.txtPosition);
        txtApprover = (TextView) findViewById(R.id.txtApprover);
        txtAppliedDate = (TextView) findViewById(R.id.txtAppliedDate);
        txtType = (TextView) findViewById(R.id.txtType);
        txtStatus = (TextView) findViewById(R.id.txtStatus);
        txtRemarks = (TextView) findViewById(R.id.txtRemarks);
        txtReason = (TextView) findViewById(R.id.txtReason);
        txtNoDays = (TextView) findViewById(R.id.txtNoDays);
        txtFromDate = (TextView) findViewById(R.id.txtFromDate);
        txtToDate = (TextView) findViewById(R.id.txtToDate);
        leave = getIntent().getParcelableExtra("leave");
        isCancelled = getIntent().getBooleanExtra("isCancelled", false);
        isEmployeeLeave = getIntent().getBooleanExtra("isEmployeeLeave", false);
        isBoss = getIntent().getBooleanExtra("isBoss", false);
        myType = getIntent().getIntExtra("myType", 1);
        employeeType = getIntent().getIntExtra("employeeType", 1);
        page = getIntent().getIntExtra("page", 0);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        btnCancel2 = (LinearLayout) findViewById(R.id.btnCancel2);
        btnEdit = (LinearLayout) findViewById(R.id.btnEdit);
        btnButton = (LinearLayout) findViewById(R.id.btnButton);
        btn = (LinearLayout) findViewById(R.id.btn);
        btnButton1 = (LinearLayout) findViewById(R.id.btnButton1);
        btnApprove = (LinearLayout) findViewById(R.id.btnApprove);
        btnReject = (LinearLayout) findViewById(R.id.btnReject);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        btnCancel2.setOnClickListener(btnCancel2OnClickListener);
        btnEdit.setOnClickListener(btnEditOnClickListener);
        btnApprove.setOnClickListener(btnApproveOnClickListener);
        btnReject.setOnClickListener(btnRejectOnClickListener);
        btn.setOnClickListener(btnCancel2OnClickListener);
    }

    private LinearLayout.OnClickListener btnApproveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RemarksDialogFragment remarksDialogFragment = RemarksDialogFragment.newInstance(2,
                    leave.getId(), EditApplicationActivity.this, page);
            remarksDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    private LinearLayout.OnClickListener btnRejectOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RemarksDialogFragment remarksDialogFragment = RemarksDialogFragment.newInstance(3, leave.getId(), EditApplicationActivity.this, page);
            remarksDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    private LinearLayout.OnClickListener btnCancel2OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LeaveCancelDialogFragment leaveCancelDialogFragment = LeaveCancelDialogFragment.newInstance(leave, EditApplicationActivity.this,
                    Utils.getDialogMessage(EditApplicationActivity.this, Utils.CODE_CANCEL_LEAVE, Utils.CANCEL_LEAVE), page, getSupportFragmentManager());
            leaveCancelDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    private LinearLayout.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LeaveCancelDialogFragment leaveCancelDialogFragment = LeaveCancelDialogFragment.newInstance(leave, EditApplicationActivity.this,
                    Utils.getDialogMessage(EditApplicationActivity.this, Utils.CODE_CANCEL_LEAVE, Utils.CANCEL_LEAVE), page, getSupportFragmentManager());
            leaveCancelDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    private LinearLayout.OnClickListener btnEditOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent mIntent = new Intent(getBaseContext(), ApplicationActivity.class);
            mIntent.putExtra("id", leave.getId());
            mIntent.putExtra("page", page);
            mIntent.putExtra("employeeType", employeeType);
            mIntent.putExtra("myType", myType);
            startActivity(mIntent);
            finish();
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 89) {
            Uri uri = LeaveProvider.CONTENT_URI;
            return new CursorLoader(getBaseContext(), uri, null, DBHelper.LEAVE_COLUMN_LEAVE_ID
                    + "=?", new String[]{String.valueOf(this.leave.getId())}, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 89) {
            if (data != null) {
                if (data.getCount() > 0) {
                    this.cursor = data;
                    leave = Leave.getLeave(cursor, 0);
                    if (leave.getId() > 0) {
                        txtName.setText(leave.getName());
                        txtDep.setText(leave.getDepartment());
                        txtPosition.setText(leave.getPosition());
                        txtApprover.setText(leave.getSuperior());
                        txtType.setText(leave.getType());
                        txtStatus.setText(leave.getStatus());
                        txtRemarks.setText(leave.getRemarks());
                        txtNoDays.setText(String.format("%.1f", leave.getLeaveDays()));
                        txtFromDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", leave.getLeaveFrom()));
                        txtToDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", leave.getLeaveTo()));
                        txtReason.setText(leave.getStatusRemark());
                        txtAppliedDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", leave.getCreateDate()));

                        isCancelled = leave.getStatus().equals("cancelled");
                        if (leave.getSuperiorId().equals(Utils.getProfile(EditApplicationActivity.this).getId())) {
                            isEmployeeLeave = true;
                        } else if (!leave.getSuperiorId().equals(Utils.getProfile(EditApplicationActivity.this).getId()) &&
                                !leave.getEmployeeId().equals(Utils.getProfile(EditApplicationActivity.this).getId())) {
                            isEmployeeLeave = true;
                            isBoss = true;
                        } else if (!leave.getSuperiorId().equals(Utils.getProfile(EditApplicationActivity.this).getId()) &&
                                leave.getEmployeeId().equals(Utils.getProfile(EditApplicationActivity.this).getId())) {
                            isEmployeeLeave = false;
                            isBoss = false;
                        }

                        String status = leave.getStatus();
                        if (status.equals("pending")) {
                            if (isBoss) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else if (isEmployeeLeave) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.VISIBLE);
                            } else if (isCancelled) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.VISIBLE);
                                btnButton1.setVisibility(View.GONE);
                            }
                        } else if (status.equals("approved")) {
                            if (isBoss) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else if (isEmployeeLeave) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else if (isCancelled) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else {
                                if (Utils.setDate(Utils.DATE_FORMAT, "dd/MM/yyyy", leave.getLeaveFrom()).equals(Utils.setCalendarDate("dd/MM/yyyy", new Date()))) {
                                    btn.setVisibility(View.GONE);
                                } else if (Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, leave.getLeaveFrom())).after(new Date())) {
                                    btn.setVisibility(View.VISIBLE);
                                } else {
                                    btn.setVisibility(View.GONE);
                                }
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            }
                        } else if (status.equals("rejected")) {
                            if (isBoss) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else if (isEmployeeLeave) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else if (isCancelled) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            }
                        } else {
                            btn.setVisibility(View.GONE);
                            btnButton.setVisibility(View.GONE);
                            btnButton1.setVisibility(View.GONE);
                        }
                    }
                } else {
                    if (leave.getId() > 0) {
                        txtName.setText(leave.getName());
                        txtDep.setText(leave.getDepartment());
                        txtPosition.setText(leave.getPosition());
                        txtApprover.setText(leave.getSuperior());
                        txtType.setText(leave.getType());
                        txtStatus.setText(leave.getStatus());
                        txtRemarks.setText(leave.getRemarks());
                        txtNoDays.setText(String.format("%.1f", leave.getLeaveDays()));
                        txtFromDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", leave.getLeaveFrom()));
                        txtToDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", leave.getLeaveTo()));
                        txtReason.setText(leave.getStatusRemark());
                        txtAppliedDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", leave.getCreateDate()));

                        String status = leave.getStatus();

                        isCancelled = leave.getStatus().equals("cancelled");
                        if (leave.getSuperiorId().equals(Utils.getProfile(EditApplicationActivity.this).getId())) {
                            isEmployeeLeave = true;
                        } else if (!leave.getSuperiorId().equals(Utils.getProfile(EditApplicationActivity.this).getId()) &&
                                !leave.getEmployeeId().equals(Utils.getProfile(EditApplicationActivity.this).getId())) {
                            isEmployeeLeave = true;
                            isBoss = true;
                        } else if (!leave.getSuperiorId().equals(Utils.getProfile(EditApplicationActivity.this).getId()) &&
                                leave.getEmployeeId().equals(Utils.getProfile(EditApplicationActivity.this).getId())) {
                            isEmployeeLeave = false;
                            isBoss = false;
                        }

                        if (status.equals("pending")) {
                            if (isBoss) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else if (isEmployeeLeave) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.VISIBLE);
                            } else if (isCancelled) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.VISIBLE);
                                btnButton1.setVisibility(View.GONE);
                            }
                        } else if (status.equals("approved")) {
                            if (isBoss) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else if (isEmployeeLeave) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else if (isCancelled) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else {
                                if (Utils.setDate(Utils.DATE_FORMAT, "dd/MM/yyyy", leave.getLeaveFrom()).equals(Utils.setCalendarDate("dd/MM/yyyy", new Date()))) {
                                    btn.setVisibility(View.GONE);
                                } else if (Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, leave.getLeaveFrom())).after(new Date())) {
                                    btn.setVisibility(View.VISIBLE);
                                } else {
                                    btn.setVisibility(View.GONE);
                                }
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            }
                        } else if (status.equals("rejected")) {
                            if (isBoss) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else if (isEmployeeLeave) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else if (isCancelled) {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            } else {
                                btn.setVisibility(View.GONE);
                                btnButton.setVisibility(View.GONE);
                                btnButton1.setVisibility(View.GONE);
                            }
                        } else {
                            btn.setVisibility(View.GONE);
                            btnButton.setVisibility(View.GONE);
                            btnButton1.setVisibility(View.GONE);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        cursor = null;
    }

    @Override
    public void onBackPressed() {
        if (page == -1) {
            Intent intent = new Intent(this, CalendarActivity.class);
            intent.putExtra("page", 0);
            intent.putExtra("employeeType", employeeType);
            intent.putExtra("myType", myType);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, LeaveActivity.class);
            intent.putExtra("page", page);
            intent.putExtra("employeeType", employeeType);
            intent.putExtra("myType", myType);
            startActivity(intent);
            finish();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_application, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            //NavUtils.navigateUpFromSameTask(this);
            Intent intent = new Intent(this, LeaveActivity.class);
            intent.putExtra("page", page);
            intent.putExtra("employeeType", employeeType);
            intent.putExtra("myType", myType);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
        }
    };


    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(Utils.NOTIFICATION_ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
        getSupportLoaderManager().initLoader(89, null, this);
        if (Utils.getStatus(Utils.LEAVE_READ, EditApplicationActivity.this)) {
            HttpHelper.getGsonReading(this, leave.getId(), TAG);
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            HttpHelper.getSingleLeaveGson(EditApplicationActivity.this, leave, TAG, txtError);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }
}
