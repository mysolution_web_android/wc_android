package com.applab.wcircle_pro.Leave;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.Arrays;

public class EmployeeLeaveFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>, SwipyRefreshLayout.OnRefreshListener {
    private EmployeeLeaveAdapter adapter;
    private Cursor cursor;
    private String selection = DBHelper.LEAVE_COLUMN_STATUS + "=? AND " + DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?";
    private String[] selectionArgs = new String[]{"pending", String.valueOf(true)};
    private LoaderManager.LoaderCallbacks<Cursor> callBack;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private String TAG = "EMPLOYEE LEAVE";
    private int pageNo = 1;
    private TextView txtError;
    private LeaveListSpinnerAdapter spinnerAdapter;
    private Spinner spType;
    public static int employeeType = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_my_cancelled_leave, container, false);
        callBack = this;
        mSwipyRefreshLayout = (SwipyRefreshLayout) layout.findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        adapter = new EmployeeLeaveAdapter(getActivity(), cursor);
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        setTouchListener();
        txtError = (TextView) layout.findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        spType = (Spinner) layout.findViewById(R.id.spType);
        spinnerAdapter = new LeaveListSpinnerAdapter(new ArrayList<>(Arrays.asList(getActivity().getResources().getStringArray(R.array.leave_filter))), getActivity());
        spType.setAdapter(spinnerAdapter);
        spType.setOnItemSelectedListener(spTypeOnItemSelectedListener);
        employeeType = getActivity().getIntent().getIntExtra("employeeType", 0);
        spType.setSelection(employeeType);
        selectionArgs = new String[]{new ArrayList<>(Arrays.asList(getActivity().getResources().getStringArray(R.array.leave_filter))).get(employeeType).toLowerCase(), String.valueOf(false)};
        getActivity().getSupportLoaderManager().initLoader(1, null, this);
        HttpHelper.getEmployeeLeaveGson(getActivity(), pageNo, TAG, txtError);
        return layout;
    }

    private Spinner.OnItemSelectedListener spTypeOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            employeeType = position;
            selection = DBHelper.LEAVE_COLUMN_STATUS + "=? AND " + DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?";
            if (position == 0) {
                selection = "(" + DBHelper.LEAVE_COLUMN_NEW_CREATE + "=? OR " + DBHelper.LEAVE_COLUMN_STATUS + "=?) AND " + DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=? ";
                selectionArgs = new String[]{String.valueOf(true), String.valueOf("pending"), String.valueOf(true)};
            } else if (position == 2) {
                selectionArgs = new String[]{String.valueOf("pending"), String.valueOf(true)};
            } else if (position == 3) {
                selectionArgs = new String[]{String.valueOf("approved"), String.valueOf(true)};
            } else if (position == 4) {
                selectionArgs = new String[]{String.valueOf("rejected"), String.valueOf(true)};
            } else if (position == 5) {
                selectionArgs = new String[]{String.valueOf("cancelled"), String.valueOf(true)};
            } else if (position == 1) {
                selection = DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?";
                selectionArgs = new String[]{String.valueOf(true)};
            }
            getActivity().getSupportLoaderManager().restartLoader(1, null, callBack);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            return;
        }
    };

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                final Leave leave = Leave.getLeave(cursor, position);
                Intent i = new Intent(getActivity(), EditApplicationActivity.class);
                i.putExtra("leave", leave);
                i.putExtra("isCancelled", leave.getStatus().equals("cancelled"));
                if (leave.getSuperiorId().equals(Utils.getProfile(getActivity()).getId())) {
                    i.putExtra("isEmployeeLeave", true);
                } else if (!leave.getSuperiorId().equals(Utils.getProfile(getActivity()).getId()) && !leave.getEmployeeId().equals(Utils.getProfile(getActivity()).getId())) {
                    i.putExtra("isEmployeeLeave", true);
                    i.putExtra("isBoss", true);
                } else if (!leave.getSuperiorId().equals(Utils.getProfile(getActivity()).getId()) && leave.getEmployeeId().equals(Utils.getProfile(getActivity()).getId())) {
                    i.putExtra("isEmployeeLeave", false);
                    i.putExtra("isBoss", false);
                }
                i.putExtra("employeeType", employeeType);
                i.putExtra("myType", MyLeaveFragment.myType);
                i.putExtra("page", 1);
                startActivity(i);
                getActivity().finish();
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (lastVisibleItem >= totalItemCount - 1) {
                        pageNo++;
                        HttpHelper.getEmployeeLeaveGson(getActivity(), pageNo, TAG, txtError);
                    } else if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }
            }
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 1) {
            Uri uri = LeaveProvider.CONTENT_URI;
            return new CursorLoader(getActivity(), uri, null, selection, selectionArgs, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 1) {
            this.cursor = data;
            adapter.swapCursor(this.cursor);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            pageNo = 1;
            HttpHelper.getEmployeeLeaveGson(getActivity(), pageNo, TAG, txtError);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
