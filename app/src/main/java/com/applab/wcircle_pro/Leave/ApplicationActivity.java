package com.applab.wcircle_pro.Leave;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.ErrorDialogFragment;
import com.applab.wcircle_pro.Profile.Profile;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;

public class ApplicationActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar toolbar;
    private Spinner spType;
    private LeaveSpinnerAdapter adapter;
    private TextView txtName, txtDep, txtPosition, txtApprover, txtFromDate, txtToDate;
    private EditText ediRemarks, ediNoDays;
    private LinearLayout btnCancel;
    private boolean isStartDate = false;
    private boolean isEndDate = false;
    public static final String DATEPICKER_TAG = "DATEPICKER";
    private Bundle bundle;
    private int id = 0;
    private String TAG = "APPLICATION";
    private Cursor cursorType, cursorLeave;
    private Calendar calendar;
    private DatePickerDialog datePickerDialog;
    private int page = 0;
    private RelativeLayout fadeRL;
    private ProgressBar progressBar;
    private int myType = 1;
    private int employeeType = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green_blue));
        TextView txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_application));
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt("id");
            page = bundle.getInt("page");
            myType = bundle.getInt("myType");
            employeeType = bundle.getInt("employeeType");
        }
        spType = (Spinner) findViewById(R.id.spType);
        adapter = new LeaveSpinnerAdapter(cursorType, this);
        spType.setAdapter(adapter);
        txtName = (TextView) findViewById(R.id.txtName);
        txtDep = (TextView) findViewById(R.id.txtDep);
        txtPosition = (TextView) findViewById(R.id.txtPosition);
        txtApprover = (TextView) findViewById(R.id.txtApprover);
        ediRemarks = (EditText) findViewById(R.id.ediRemarks);
        ediNoDays = (EditText) findViewById(R.id.ediNoDays);
        txtFromDate = (TextView) findViewById(R.id.txtFromDate);
        txtToDate = (TextView) findViewById(R.id.txtToDate);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        calendar = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);
        findViewById(R.id.btnFromDate).setOnClickListener(btnFromDateOnClickListener);
        findViewById(R.id.btnToDate).setOnClickListener(btnToDateOnClickListener);
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        fadeRL.setOnClickListener(fadeRLOnClickListener);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        setVisibilityRlAndProgressBar(false);
        Profile profile = Utils.getProfile(ApplicationActivity.this);
        txtName.setText(profile.getName());
        txtDep.setText(profile.getDepartment());
        txtPosition.setText(profile.getPosition());
        txtApprover.setText(profile.getHODName());
    }

    private View.OnClickListener fadeRLOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private void setVisibilityRlAndProgressBar(boolean isVisible) {
        if (isVisible) {
            fadeRL.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            fadeRL.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    private ImageView.OnClickListener btnToDateOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            datePickerDialog.setVibrate(false);
            datePickerDialog.setYearRange(1985, 2028);
            datePickerDialog.setCloseOnSingleTapDay(false);
            datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
            isEndDate = true;
        }
    };

    private ImageView.OnClickListener btnFromDateOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            datePickerDialog.setVibrate(false);
            datePickerDialog.setYearRange(1985, 2028);
            datePickerDialog.setCloseOnSingleTapDay(false);
            datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
            isStartDate = true;
        }
    };

    private LinearLayout.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ApplicationActivity.this, LeaveActivity.class);
            intent.putExtra("page", page);
            intent.putExtra("myType", myType);
            intent.putExtra("employeeType", employeeType);
            startActivity(intent);
            finish();
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 2) {
            Uri uri = LeaveProvider.CONTENT_URI;
            return new CursorLoader(getBaseContext(), uri, null, DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?",
                    new String[]{String.valueOf(this.id)}, null);
        } else if (id == 3) {
            Uri uri = TypeProvider.CONTENT_URI;
            return new CursorLoader(getBaseContext(), uri, null, null, null, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            if (data.getCount() > 0) {
                if (loader.getId() == 2) {
                    if (id > 0) {
                        cursorLeave = data;
                        Leave leave = Leave.getLeave(cursorLeave, 0);
                        ediRemarks.setText(leave.getRemarks());
                        ediNoDays.setText(String.format("%.1f", leave.getLeaveDays()));
                        txtFromDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", leave.getLeaveFrom()));
                        txtToDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", leave.getLeaveTo()));
                        txtName.setText(leave.getName());
                        txtDep.setText(leave.getDepartment());
                        txtPosition.setText(leave.getPosition());
                        txtApprover.setText(leave.getSuperior());
                    }
                } else if (loader.getId() == 3) {
                    cursorType = data;
                    adapter.swapCursor(cursorType);
                    if (id > 0) {
                        cursorType.moveToFirst();
                        do {
                            if (cursorLeave != null && cursorLeave.getCount() > 0) {
                                Type type = Type.getType(cursorType, cursorType.getPosition());
                                Leave leave = Leave.getLeave(cursorLeave, 0);
                                if (leave.getTypeCode().equals(type.getLeaveCode())) {
                                    spType.setSelection(cursorType.getPosition());
                                }
                            }

                        } while (cursorType.moveToNext());
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LeaveActivity.class);
        intent.putExtra("page", page);
        intent.putExtra("myType", myType);
        intent.putExtra("employeeType", employeeType);
        startActivity(intent);
        finish();
    }

    public void btnSubmit(View view) throws ParseException {
        boolean isValid = true;
        setVisibilityRlAndProgressBar(true);
        isValid = getValidation();
        if (isValid) {
            String leaveFrom = txtFromDate.getText().toString();
            String leaveTo = txtToDate.getText().toString();

            leaveFrom = Utils.setToUTCDate("dd MMM yyyy", Utils.DATE_FORMAT, leaveFrom);
            leaveTo = Utils.setToUTCDate("dd MMM yyyy", Utils.DATE_FORMAT, leaveTo);

            TextView txtType = (TextView) spType.getSelectedView().findViewById(android.R.id.text1);
            Type type = (Type) txtType.getTag();
            String remarks = ediRemarks.getText().toString();
            String leaveDay = ediNoDays.getText().toString();
            if (leaveDay.contains(".")) {
                String[] result = leaveDay.split("\\.");
                if (result.length > 1) {
                    int i = Integer.valueOf(result[1]);
                    if (i > 0) {
                        i = 5;
                    } else {
                        i = 0;
                    }
                    leaveDay = result[0] + "." + i;
                }
            }

            if (id == 0) {
                postData(type, remarks, leaveDay, leaveFrom, leaveTo);
            } else {
                String id = String.valueOf(this.id);
                putData(id, type, remarks, leaveDay, leaveFrom, leaveTo);
            }
        } else {
            setVisibilityRlAndProgressBar(false);
        }
    }

    private void putData(final String id, final Type type, final String remarks, final String leaveDay, final String leaveFrom, final String leaveTo) {
        final String token = Utils.getToken(ApplicationActivity.this);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.PUT,
                Utils.API_URL + "api/Leave/My",
                JSONObject.class,
                headers,
                responseListener(),
                errorListener()) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "Id=" + Utils.encode(id) + "&TypeCode=" + Utils.encode(type.getLeaveCode()) +
                        "&Remark=" + Utils.encode(remarks) + "&NoOfDay=" + Utils.encode(leaveDay) + "&FromDate=" +
                        Utils.encode(leaveFrom) + "&ToDate=" + Utils.encode(leaveTo);
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                if (response.statusCode == 200) {
                    startActivity(new Intent(getBaseContext(), LeaveActivity.class));
                    finish();
                }
                return super.parseNetworkResponse(response);
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public boolean getValidation() {
        boolean isValid = true;
        String message = "";
        if (ediRemarks.getText().toString().equals("")) {
            message = Utils.getDialogMessage(ApplicationActivity.this, Utils.CODE_REASON_FIELD, Utils.REASON_FIELD);
            isValid = false;
        } else if (ediNoDays.getText().toString().equals("")) {
            message = Utils.getDialogMessage(ApplicationActivity.this, Utils.CODE_NO_OF_DAYS_FIELD, Utils.NO_OF_DAYS_FIELD);
            isValid = false;
        } else if (txtFromDate.getText().toString().equals("")) {
            message = Utils.getDialogMessage(ApplicationActivity.this, Utils.CODE_FROM_DATE_FIELD, Utils.FROM_DATE_FIELD);
            isValid = false;
        } else if (txtToDate.getText().toString().equals("")) {
            message = Utils.getDialogMessage(ApplicationActivity.this, Utils.CODE_TO_DATE_FIELD, Utils.TO_DATE_FIELD);
            isValid = false;
        } else if (!txtFromDate.getText().toString().equals("")) {
            if (!txtToDate.getText().toString().equals("")) {
                Date date1 = Utils.setCalendarDate("dd MMM yyyy", txtFromDate.getText().toString());
                Date date2 = Utils.setCalendarDate("dd MMM yyyy", txtToDate.getText().toString());
                if (date2.before(date1)) {
                    message = Utils.getDialogMessage(ApplicationActivity.this,
                            Utils.CODE_PREV_TO_DATE_FIELD, Utils.PREV_TO_DATE_FIELD) + txtFromDate.getText().toString() + ".";
                    isValid = false;
                }
            }
        }
        if (!ediNoDays.getText().toString().equals("")) {
            Scanner scanner = new Scanner(ediNoDays.getText().toString());
            if (!scanner.hasNextDouble()) {
                message = Utils.getDialogMessage(ApplicationActivity.this, Utils.CODE_NUMERIC_NO_OF_DAYS_FIELD, Utils.NUMERIC_NO_OF_DAYS_FIELD);
                isValid = false;
            }
        }

        if (!message.equals("")) {
            setVisibilityRlAndProgressBar(false);
            ErrorDialogFragment leaveErrorDialogFragment = ErrorDialogFragment.newInstance(ApplicationActivity.this, message);
            leaveErrorDialogFragment.setCancelable(false);
            leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
        }
        return isValid;
    }

    public void postData(final Type type, final String remarks, final String leaveDay, final String leaveFrom, final String leaveTo) {
        final String token = Utils.getToken(ApplicationActivity.this);
        Log.i(TAG, token);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Leave/My",
                JSONObject.class,
                headers,
                responseListener(),
                errorListener()) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "TypeCode=" + Utils.encode(type.getLeaveCode()) + "&Remark=" + Utils.encode(remarks) +
                        "&NoOfDay=" + Utils.encode(leaveDay) + "&FromDate=" + Utils.encode(leaveFrom) + "&ToDate=" + Utils.encode(leaveTo);
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                if (response.statusCode == 200) {
                    startActivity(new Intent(getBaseContext(), LeaveActivity.class));
                    finish();
                }
                return super.parseNetworkResponse(response);
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<JSONObject> responseListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.serverHandlingError(ApplicationActivity.this, error);
                setVisibilityRlAndProgressBar(false);
            }
        };
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        String[] m = getResources().getStringArray(R.array.month);
        String strMonth = String.valueOf(m[month]);
        String strDay = "";
        if (day < 10) {
            strDay = "0" + String.valueOf(day);
        } else {
            strDay = String.valueOf(day);
        }
        if (isStartDate) {
            txtFromDate.setText(strDay + " " + strMonth + " " + year);
            isStartDate = false;
        }
        if (isEndDate) {
            txtToDate.setText(strDay + " " + strMonth + " " + year);
            isEndDate = false;
        }
    }

    private void alertDialog(String errorMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ApplicationActivity.this);
        builder.setCancelable(false);
        builder.setMessage(errorMessage);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_application, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            Intent intent = new Intent(ApplicationActivity.this,
                    LeaveActivity.class);
            intent.putExtra("page", page);
            intent.putExtra("myType", myType);
            intent.putExtra("employeeType", employeeType);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        setVisibilityRlAndProgressBar(true);
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        setVisibilityRlAndProgressBar(false);
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(Utils.NOTIFICATION_ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
        getSupportLoaderManager().initLoader(2, null, this);
        getSupportLoaderManager().initLoader(3, null, this);
    }
}
