package com.applab.wcircle_pro.Leave;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Leave implements Parcelable {


    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("EmployeeId")
    @Expose
    private Integer EmployeeId;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Department")
    @Expose
    private String Department;
    @SerializedName("Position")
    @Expose
    private String Position;
    @SerializedName("SuperiorId")
    @Expose
    private Integer SuperiorId;
    @SerializedName("Superior")
    @Expose
    private String Superior;
    @SerializedName("TypeCode")
    @Expose
    private String TypeCode;
    @SerializedName("Type")
    @Expose
    private String Type;
    @SerializedName("Remarks")
    @Expose
    private String Remarks;
    @SerializedName("LeaveDays")
    @Expose
    private Double LeaveDays;
    @SerializedName("LeaveFrom")
    @Expose
    private String LeaveFrom;
    @SerializedName("LeaveTo")
    @Expose
    private String LeaveTo;
    @SerializedName("NewCreate")
    @Expose
    private Boolean NewCreate;
    @SerializedName("Status")
    @Expose
    private String Status;
    @SerializedName("StatusRemark")
    @Expose
    private String StatusRemark;
    @SerializedName("StatusUpdateBy")
    @Expose
    private String StatusUpdateBy;
    @SerializedName("StatusUpdateOn")
    @Expose
    private String StatusUpdateOn;
    @SerializedName("LastUpdate")
    @Expose
    private String LastUpdate;
    @SerializedName("CreateDate")
    @Expose
    private String CreateDate;

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The EmployeeId
     */
    public Integer getEmployeeId() {
        return EmployeeId;
    }

    /**
     * @param EmployeeId The EmployeeId
     */
    public void setEmployeeId(Integer EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    /**
     * @return The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return The Department
     */
    public String getDepartment() {
        return Department;
    }

    /**
     * @param Department The Department
     */
    public void setDepartment(String Department) {
        this.Department = Department;
    }

    /**
     * @return The Position
     */
    public String getPosition() {
        return Position;
    }

    /**
     * @param Position The Position
     */
    public void setPosition(String Position) {
        this.Position = Position;
    }

    /**
     * @return The SuperiorId
     */
    public Integer getSuperiorId() {
        return SuperiorId;
    }

    /**
     * @param SuperiorId The SuperiorId
     */
    public void setSuperiorId(Integer SuperiorId) {
        this.SuperiorId = SuperiorId;
    }

    /**
     * @return The Superior
     */
    public String getSuperior() {
        return Superior;
    }

    /**
     * @param Superior The Superior
     */
    public void setSuperior(String Superior) {
        this.Superior = Superior;
    }

    /**
     * @return The TypeCode
     */
    public String getTypeCode() {
        return TypeCode;
    }

    /**
     * @param TypeCode The TypeCode
     */
    public void setTypeCode(String TypeCode) {
        this.TypeCode = TypeCode;
    }

    /**
     * @return The Type
     */
    public String getType() {
        return Type;
    }

    /**
     * @param Type The Type
     */
    public void setType(String Type) {
        this.Type = Type;
    }

    /**
     * @return The Remarks
     */
    public String getRemarks() {
        return Remarks;
    }

    /**
     * @param Remarks The Remarks
     */
    public void setRemarks(String Remarks) {
        this.Remarks = Remarks;
    }

    /**
     * @return The LeaveDays
     */
    public Double getLeaveDays() {
        return LeaveDays;
    }

    /**
     * @param LeaveDays The LeaveDays
     */
    public void setLeaveDays(Double LeaveDays) {
        this.LeaveDays = LeaveDays;
    }

    /**
     * @return The LeaveFrom
     */
    public String getLeaveFrom() {
        return LeaveFrom;
    }

    /**
     * @param LeaveFrom The LeaveFrom
     */
    public void setLeaveFrom(String LeaveFrom) {
        this.LeaveFrom = LeaveFrom;
    }

    /**
     * @return The LeaveTo
     */
    public String getLeaveTo() {
        return LeaveTo;
    }

    /**
     * @param LeaveTo The LeaveTo
     */
    public void setLeaveTo(String LeaveTo) {
        this.LeaveTo = LeaveTo;
    }

    /**
     * @return The NewCreate
     */
    public Boolean getNewCreate() {
        return NewCreate;
    }

    /**
     * @param NewCreate The NewCreate
     */
    public void setNewCreate(Boolean NewCreate) {
        this.NewCreate = NewCreate;
    }

    /**
     * @return The Status
     */
    public String getStatus() {
        return Status;
    }

    /**
     * @param Status The Status
     */
    public void setStatus(String Status) {
        this.Status = Status;
    }

    /**
     * @return The StatusRemark
     */
    public String getStatusRemark() {
        return StatusRemark;
    }

    /**
     * @param StatusRemark The StatusRemark
     */
    public void setStatusRemark(String StatusRemark) {
        this.StatusRemark = StatusRemark;
    }

    /**
     * @return The StatusUpdateBy
     */
    public String getStatusUpdateBy() {
        return StatusUpdateBy;
    }

    /**
     * @param StatusUpdateBy The StatusUpdateBy
     */
    public void setStatusUpdateBy(String StatusUpdateBy) {
        this.StatusUpdateBy = StatusUpdateBy;
    }

    /**
     * @return The StatusUpdateOn
     */
    public String getStatusUpdateOn() {
        return StatusUpdateOn;
    }

    /**
     * @param StatusUpdateOn The StatusUpdateOn
     */
    public void setStatusUpdateOn(String StatusUpdateOn) {
        this.StatusUpdateOn = StatusUpdateOn;
    }

    /**
     * @return The LastUpdate
     */
    public String getLastUpdate() {
        return LastUpdate;
    }

    /**
     * @param LastUpdate The LastUpdate
     */
    public void setLastUpdate(String LastUpdate) {
        this.LastUpdate = LastUpdate;
    }

    /**
     * @return The CreateDate
     */
    public String getCreateDate() {
        return CreateDate;
    }

    /**
     * @param CreateDate The CreateDate
     */
    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    public Leave() {
    }

    public Leave(Parcel in) {
        readFromParcel(in);
    }


    public static Leave getLeave(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Leave leave = new Leave();
        leave.setDepartment(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_DEPARTMENT)));
        leave.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_CREATE_DATE)));
        leave.setEmployeeId(cursor.getInt(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_EMPLOYEE_ID)));
        leave.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_LEAVE_ID)));
        leave.setLastUpdate(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_LAST_UPDATE)));
        leave.setLeaveDays(cursor.getDouble(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_LEAVE_DAYS)));
        leave.setLeaveFrom(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_LEAVE_FROM)));
        leave.setLeaveTo(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_LEAVE_TO)));
        leave.setName(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_NAME)));
        leave.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_POSITION)));
        leave.setStatus(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_STATUS)));
        leave.setLastUpdate(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_LAST_UPDATE)));
        leave.setStatusRemark(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_STATUS_REMARKS)));
        leave.setStatusUpdateBy(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_BY)));
        leave.setStatusUpdateOn(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_ON)));
        leave.setSuperior(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_SUPERIOR)));
        leave.setSuperiorId(cursor.getInt(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_SUPERIOR_ID)));
        leave.setType(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_TYPE)));
        leave.setTypeCode(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_TYPE_CODE)));
        leave.setRemarks(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_REMARKS)));
        leave.setNewCreate(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_NEW_CREATE))));
        return leave;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeString(this.Name);
        dest.writeString(this.Department);
        dest.writeString(this.Position);
        dest.writeString(this.Superior);
        dest.writeString(this.TypeCode);
        dest.writeString(this.Type);
        dest.writeString(this.Remarks);
        dest.writeDouble(this.LeaveDays);
        dest.writeString(this.LeaveFrom);
        dest.writeString(this.LeaveTo);
        dest.writeString(this.Status);
        dest.writeString(this.StatusRemark);
        dest.writeString(this.StatusUpdateBy);
        dest.writeString(this.StatusUpdateOn);
        dest.writeString(this.LastUpdate);
        dest.writeInt(this.SuperiorId);
        dest.writeInt(this.EmployeeId);
        dest.writeString(this.CreateDate);
        dest.writeByte((byte) (NewCreate ? 1 : 0));
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.Id = in.readInt();
        this.Name = in.readString();
        this.Department = in.readString();
        this.Position = in.readString();
        this.Superior = in.readString();
        this.TypeCode = in.readString();
        this.Type = in.readString();
        this.Remarks = in.readString();
        this.LeaveDays = in.readDouble();
        this.LeaveFrom = in.readString();
        this.LeaveTo = in.readString();
        this.Status = in.readString();
        this.StatusRemark = in.readString();
        this.StatusUpdateBy = in.readString();
        this.StatusUpdateOn = in.readString();
        this.LastUpdate = in.readString();
        this.SuperiorId = in.readInt();
        this.EmployeeId = in.readInt();
        this.CreateDate = in.readString();
        this.NewCreate = in.readByte() != 0;
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Leave createFromParcel(Parcel in) {
            return new Leave(in);
        }

        @Override
        public Leave[] newArray(int size) {
            return new Leave[size];
        }
    };
}