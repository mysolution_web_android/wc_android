package com.applab.wcircle_pro.Leave;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by admin on 31/7/2015.
 */

public class LeaveExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader;
    private HashMap<String, ArrayList<Leave>> _listDataChild;
    private String TAG = "EXP LIST";

    public LeaveExpandableListAdapter(Context context, List<String> listDataHeader,
                                      HashMap<String, ArrayList<Leave>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final Leave leave = (Leave) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.custom_leave_row, null);
        }
        LinearLayout lv = (LinearLayout) convertView.findViewById(R.id.lv);
        TextView name = (TextView) convertView.findViewById(R.id.txtName);
        name.setVisibility(View.GONE);
        TextView type = (TextView) convertView.findViewById(R.id.txtType);
        TextView date = (TextView) convertView.findViewById(R.id.txtDate);
        TextView day = (TextView) convertView.findViewById(R.id.txtDay);
        TextView id = (TextView) convertView.findViewById(R.id.txtId);
        ImageView btnInfo = (ImageView) convertView.findViewById(R.id.btnInfo);
        type.setText(leave.getType());
        type.setTypeface(Typeface.defaultFromStyle(leave.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        date.setText(Utils.setDate(Utils.DATE_FORMAT, "d MMM", leave.getLeaveFrom()));
        day.setText(Utils.setDate(Utils.DATE_FORMAT, "EEEE", leave.getLeaveFrom()));
        id.setText(String.valueOf(leave.getId()));
        id.setTag(leave);
        switch (leave.getStatus()) {
            case "pending":
                btnInfo.setVisibility(View.VISIBLE);
                btnInfo.setImageResource(R.mipmap.info_p_2);
                break;
            case "rejected":
                btnInfo.setVisibility(View.VISIBLE);
                btnInfo.setImageResource(R.mipmap.info_r);
                break;
            case "approved":
                btnInfo.setVisibility(View.VISIBLE);
                btnInfo.setImageResource(R.mipmap.info_a);
                break;
            case "cancelled":
                btnInfo.setVisibility(View.GONE);
                break;
        }
        lv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(_context, EditApplicationActivity.class);
                mIntent.putExtra("leave", leave);
                mIntent.putExtra("isCancelled", leave.getStatus().equals("cancelled"));
                mIntent.putExtra("isEmployeeLeave", leave.getSuperiorId().equals(Utils.getProfile(_context).getId()));
                mIntent.putExtra("page", 0);
                _context.startActivity(mIntent);
                ((Activity) _context).finish();
            }
        });
        Log.i(TAG, "Group Position: " + groupPosition + " Child Position: " + childPosition);
        return convertView;
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)) == null ? 0 :
                this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.custom_favorite_group_row, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.imgIcon);
        imgIcon.setImageResource(isExpanded ? R.mipmap.ic_minus_light : R.mipmap.ic_action_new_light);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public Cursor swapCursor(Cursor cursor) {
        ArrayList<Leave> arrLeave = new ArrayList<>();
        ArrayList<Leave> arrLeave1 = new ArrayList<>();
        ArrayList<Leave> arrLeave2 = new ArrayList<>();
        ArrayList<Leave> arrLeave3 = new ArrayList<>();
        for (int i = 0; i < cursor.getCount(); i++) {
            Leave leave = Leave.getLeave(cursor, i);
            switch (leave.getStatus()) {
                case "pending":
                    arrLeave.add(leave);
                    break;
                case "approved":
                    arrLeave1.add(leave);
                    break;
                case "rejected":
                    arrLeave2.add(leave);
                    break;
                case "cancelled":
                    arrLeave3.add(leave);
                    break;
            }
        }
        this._listDataChild.clear();
        this._listDataChild.put("Pending", arrLeave);
        this._listDataChild.put("Approved", arrLeave1);
        this._listDataChild.put("Rejected", arrLeave2);
        this._listDataChild.put("Cancelled", arrLeave3);
        android.os.Message msg = handler.obtainMessage();
        handler.handleMessage(msg);
        return cursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            LeaveExpandableListAdapter.this.notifyDataSetChanged();
        }
    };
}
