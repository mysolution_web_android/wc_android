package com.applab.wcircle_pro.Leave;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/8/2015.
 */
public class LeaveViewHolder extends RecyclerView.ViewHolder {
    ImageView btnInfo;
    TextView type;
    TextView date;
    TextView day;

    public LeaveViewHolder(View itemView) {
        super(itemView);
        type = (TextView) itemView.findViewById(R.id.txtType);
        date = (TextView) itemView.findViewById(R.id.txtDate);
        day = (TextView) itemView.findViewById(R.id.txtDay);
        btnInfo = (ImageView) itemView.findViewById(R.id.btnInfo);
    }

}