package com.applab.wcircle_pro.Leave;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

public class LeaveSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private Cursor mCursor;

    public LeaveSpinnerAdapter(Cursor cursor, Context context) {
        this.mCursor = cursor;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mCursor == null ? 0 : mCursor.getCount();
    }

    @Override
    public Object getItem(int position) {
        mCursor.moveToPosition(position);
        return mCursor;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("DROPDOWN")) {
            LayoutInflater inflater = null;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_spinner_drop_down_row, parent, false);
            view.setTag("DROPDOWN");
        }
        Type type = Type.getType(mCursor,position);
        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(type.getTitle());
        textView.setTag(type);

        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
            LayoutInflater inflater = null;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.
                    custom_spinner_row, parent, false);
            view.setTag("NON_DROPDOWN");
        }
        Type type = Type.getType(mCursor,position);
        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(type.getTitle());
        textView.setTag(type);
        return view;
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.mCursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.mCursor;
        this.mCursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            LeaveSpinnerAdapter.this.notifyDataSetChanged();
        }
    };
}