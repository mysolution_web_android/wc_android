package com.applab.wcircle_pro.Leave;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.applab.wcircle_pro.R;

class LeavePagerAdapter extends FragmentPagerAdapter {
    String[] tabs;

    public LeavePagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        tabs = context.getResources().getStringArray(R.array.leave_tabs);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment item;
        MyLeaveFragment myLeaveFragment = new MyLeaveFragment();
        EmployeeLeaveFragment employeeLeaveFragment = new EmployeeLeaveFragment();
        if (position == 0) {
            item = myLeaveFragment;
        } else {
            item = employeeLeaveFragment;
        }
        return item;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }
}