package com.applab.wcircle_pro.Leave;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Dashboard.DashboardProvider;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by user on 23/11/2015.
 */
public class HttpHelper {

    //region My leave
    public static void getMyLeaveGson(Context context, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        final Date lastUpdateDate = getMyLeaveLastUpdate(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Leave/My?LastUpdated=" + Utils.encode(lastUpdate) +
                "&ReturnEmpty=" + Utils.encode(getMyLeaveRow(context) > 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Leave/My?LastUpdated=" + Utils.encode(lastUpdate) +
                    "&ReturnEmpty=" + Utils.encode(getMyLeaveRow(context) > 0);
        }
        final String finalUrl = url;
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<LeaveChecking> mGsonRequest = new GsonRequest<LeaveChecking>(
                Request.Method.GET,
                finalUrl,
                LeaveChecking.class,
                headers,
                responseMyLeaveListener(context, txtError, TAG),
                errorMyLeaveListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<LeaveChecking> responseMyLeaveListener(final Context context, final TextView txtError, final String TAG) {
        return new Response.Listener<LeaveChecking>() {
            @Override
            public void onResponse(LeaveChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getLeaves().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getMyLeaveLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    context.getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                    context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                    insertLeave(response, context, TAG);
                                } else {
                                    context.getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                    context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                    insertLeave(response, context, TAG);
                                }
                            } else {
                                context.getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                insertLeave(response, context, TAG);
                            }
                        } else {
                            if (response.getNoOfRecords() == 0) {
                                context.getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                            }
                        }
                    }
                }
                getTypeGson(context, TAG);
            }
        };
    }

    public static Response.ErrorListener errorMyLeaveListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

    public static int getMyLeaveRow(Context context) {
        int totalRow = 0;
        Cursor cursor = context.getContentResolver().query(LeaveProvider.CONTENT_URI, null, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                new String[]{String.valueOf(false)}, null);
        try {

            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return totalRow;
    }

    public static Date getMyLeaveLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        Cursor cursor = context.getContentResolver().query(LeaveCheckingProvider.CONTENT_URI,
                null, DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                new String[]{String.valueOf(false)}, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static boolean isLeaveExists(Context context, Object leaveId) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(LeaveProvider.CONTENT_URI, null,
                    DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(leaveId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void insertLeave(LeaveChecking result, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesLeaveChecking = new ContentValues();
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_PER_PAGE, result.getNoPerPage());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_PAGE_NO, 0);
            context.getContentResolver().insert(LeaveCheckingProvider.CONTENT_URI, contentValuesLeaveChecking);
            ContentValues[] contentValueses = new ContentValues[result.getLeaves().size()];
            for (int i = 0; i < result.getLeaves().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_ID, result.getLeaves().get(i).getId());
                contentValues.put(DBHelper.LEAVE_COLUMN_NAME, result.getLeaves().get(i).getName());
                contentValues.put(DBHelper.LEAVE_COLUMN_EMPLOYEE_ID, result.getLeaves().get(i).getEmployeeId());
                contentValues.put(DBHelper.LEAVE_COLUMN_DEPARTMENT, result.getLeaves().get(i).getDepartment());
                contentValues.put(DBHelper.LEAVE_COLUMN_POSITION, result.getLeaves().get(i).getPosition());
                contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR, result.getLeaves().get(i).getSuperior());
                contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR_ID, result.getLeaves().get(i).getSuperiorId());
                contentValues.put(DBHelper.LEAVE_COLUMN_TYPE_CODE, result.getLeaves().get(i).getTypeCode());
                contentValues.put(DBHelper.LEAVE_COLUMN_TYPE, result.getLeaves().get(i).getType());
                contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, result.getLeaves().get(i).getRemarks());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_DAYS, result.getLeaves().get(i).getLeaveDays());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_FROM, result.getLeaves().get(i).getLeaveFrom());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_TO, result.getLeaves().get(i).getLeaveTo());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, result.getLeaves().get(i).getStatus());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_REMARKS, result.getLeaves().get(i).getStatusRemark());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_BY, result.getLeaves().get(i).getStatusUpdateBy());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_ON, result.getLeaves().get(i).getStatusUpdateOn());
                contentValues.put(DBHelper.LEAVE_COLUMN_LAST_UPDATE, result.getLeaves().get(i).getLastUpdate());
                if (result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(context).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                } else if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(context).getId()) && !result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(context).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                } else if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(context).getId()) && result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(context).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
                }
                contentValues.put(DBHelper.LEAVE_COLUMN_NEW_CREATE, String.valueOf(result.getLeaves().get(i).getNewCreate()));
                contentValues.put(DBHelper.LEAVE_COLUMN_PAGE_NO, 0);
                contentValues.put(DBHelper.LEAVE_COLUMN_CREATE_DATE, result.getLeaves().get(i).getCreateDate());
                if (isLeaveExists(context, result.getLeaves().get(i).getId())) {
                    context.getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_LEAVE_ID +
                            "=?", new String[]{String.valueOf(result.getLeaves().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(LeaveProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider My Leave Error :" + ex.toString());
        }
    }
    //endregion

    //region Type
    public static void getTypeGson(Context context, String TAG) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Type[]> mGsonRequest = new GsonRequest<Type[]>(
                Request.Method.GET,
                Utils.API_URL + "api/Leave/Type",
                Type[].class,
                headers,
                responseTypeListener(context, TAG),
                errorTypeListener(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Type[]> responseTypeListener(final Context context, final String TAG) {
        return new Response.Listener<Type[]>() {
            @Override
            public void onResponse(Type[] response) {
                context.getContentResolver().delete(TypeProvider.CONTENT_URI, null, null);
                insertType(response, context, TAG);
            }
        };
    }

    public static Response.ErrorListener errorTypeListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }

    public static void insertType(Type[] result, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues[] contentValueses = new ContentValues[result.length];
            for (int i = 0; i < result.length; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.TYPE_COLUMN_DESCRIPTION, result[i].getDescription());
                contentValues.put(DBHelper.TYPE_COLUMN_ID, result[i].getId());
                contentValues.put(DBHelper.TYPE_COLUMN_LEAVE_CODE, result[i].getLeaveCode());
                contentValues.put(DBHelper.TYPE_COLUMN_TITLE, result[i].getTitle());
                contentValues.put(DBHelper.TYPE_COLUMN_NO_AVAILABLE,
                        result[i].getNoVailable() != null ? result[i].getNoVailable() : 0);
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(TypeProvider.CONTENT_URI, contentValueses);
            }

        } catch (Exception ex) {
            Log.i(TAG, "Content Provider My Leave Error :" + ex.toString());
        }
    }
    //endregion

    //region Employee Leave gson
    public static void getEmployeeLeaveGson(Context context, final int pageNo, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        final Date lastUpdateDate = getEmployeeLeaveLastUpdate(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Leave/Other?NoPerPage=" +
                Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                + Utils.encode(pageNo) + "&LastUpdated=" + Utils.encode(lastUpdate)
                + "&ReturnEmpty=" + Utils.encode(getEmployeeLeaveRow(context) > 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Leave/Other?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER)
                    + "&PageNo=" + Utils.encode(pageNo)
                    + "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" +
                    Utils.encode(getEmployeeLeaveRow(context) > 0);
        }
        final String finalUrl = url;
        Log.i(TAG, url);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<LeaveChecking> mGsonRequest = new GsonRequest<LeaveChecking>(
                Request.Method.GET,
                finalUrl,
                LeaveChecking.class,
                headers,
                responseEmployeeLeaveListener(context, pageNo, txtError, TAG),
                errorEmployeeLeaveListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<LeaveChecking> responseEmployeeLeaveListener(final Context context, final int pageNo, final TextView txtError, final String TAG) {
        return new Response.Listener<LeaveChecking>() {
            @Override
            public void onResponse(LeaveChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getLeaves().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getEmployeeLeaveLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(LeaveProvider.CONTENT_URI,
                                                DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                                new String[]{String.valueOf(true)});
                                        context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                                DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                                new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                                DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=? AND " +
                                                        DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_PAGE + "=?",
                                                new String[]{String.valueOf(true), String.valueOf(pageNo)});
                                    }
                                    insertLeave(response, context, pageNo, TAG);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(LeaveProvider.CONTENT_URI,
                                                DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                                new String[]{String.valueOf(true)});
                                        context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                                DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                                new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                                DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=? AND " +
                                                        DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_PAGE + "=?",
                                                new String[]{String.valueOf(true), String.valueOf(pageNo)});
                                    }
                                    insertLeave(response, context, pageNo, TAG);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(LeaveProvider.CONTENT_URI,
                                            DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                            new String[]{String.valueOf(true)});
                                    context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                            DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                            new String[]{String.valueOf(true)});
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                            DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=? AND " +
                                                    DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_PAGE + "=?",
                                            new String[]{String.valueOf(true), String.valueOf(pageNo)});
                                }
                                insertLeave(response, context, pageNo, TAG);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(LeaveProvider.CONTENT_URI,
                                            DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                            new String[]{String.valueOf(true)});
                                    context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                            DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                            new String[]{String.valueOf(true)});
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    public static Response.ErrorListener errorEmployeeLeaveListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

    public static int getEmployeeLeaveRow(Context context) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(LeaveProvider.CONTENT_URI, null,
                    DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(true)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getEmployeeLeaveLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(LeaveCheckingProvider.CONTENT_URI,
                    null, DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                    new String[]{String.valueOf(true)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static void insertLeave(LeaveChecking result, Context context, int pageNo, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesLeaveChecking = new ContentValues();
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_PER_PAGE, result.getNoPerPage());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_PAGE_NO, pageNo);
            context.getContentResolver().insert(LeaveCheckingProvider.CONTENT_URI, contentValuesLeaveChecking);
            ContentValues[] contentValueses = new ContentValues[result.getLeaves().size()];
            for (int i = 0; i < result.getLeaves().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_ID, result.getLeaves().get(i).getId());
                contentValues.put(DBHelper.LEAVE_COLUMN_NAME, result.getLeaves().get(i).getName());
                contentValues.put(DBHelper.LEAVE_COLUMN_DEPARTMENT, result.getLeaves().get(i).getDepartment());
                contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR_ID, result.getLeaves().get(i).getSuperiorId());
                contentValues.put(DBHelper.LEAVE_COLUMN_POSITION, result.getLeaves().get(i).getPosition());
                contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR, result.getLeaves().get(i).getSuperior());
                contentValues.put(DBHelper.LEAVE_COLUMN_TYPE_CODE, result.getLeaves().get(i).getTypeCode());
                contentValues.put(DBHelper.LEAVE_COLUMN_TYPE, result.getLeaves().get(i).getType());
                contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, result.getLeaves().get(i).getRemarks());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_DAYS, result.getLeaves().get(i).getLeaveDays());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_FROM, result.getLeaves().get(i).getLeaveFrom());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_TO, result.getLeaves().get(i).getLeaveTo());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, result.getLeaves().get(i).getStatus());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_REMARKS, result.getLeaves().get(i).getStatusRemark());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_BY, result.getLeaves().get(i).getStatusUpdateBy());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_ON, result.getLeaves().get(i).getStatusUpdateOn());
                contentValues.put(DBHelper.LEAVE_COLUMN_LAST_UPDATE, result.getLeaves().get(i).getLastUpdate());
                contentValues.put(DBHelper.LEAVE_COLUMN_NEW_CREATE, String.valueOf(result.getLeaves().get(i).getNewCreate()));
                if (result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(context).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                } else if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(context).getId()) && !result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(context).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                } else if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(context).getId()) && result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(context).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
                }
                contentValues.put(DBHelper.LEAVE_COLUMN_PAGE_NO, pageNo);
                contentValues.put(DBHelper.LEAVE_COLUMN_CREATE_DATE, result.getLeaves().get(i).getCreateDate());
                if (isLeaveExists(context, result.getLeaves().get(i).getId())) {
                    context.getContentResolver().delete(LeaveProvider.CONTENT_URI,
                            DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?",
                            new String[]{String.valueOf(result.getLeaves().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(LeaveProvider.CONTENT_URI, contentValueses);
            }

        } catch (Exception ex) {
            Log.i(TAG, "Content Provider My Leave Error :" + ex.toString());
        }
    }
    //endregion

    //region Cancelled Leave
    public static void getCancelledLeaveGson(Context context, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        final Date lastUpdateDate = getCancelledLeaveLastUpdate(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Leave/My/Cancelled?LastUpdated=" +
                Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getCancelledLeaveRow(context) > 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Leave/My/Cancelled?LastUpdated=" +
                    Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getCancelledLeaveRow(context) > 0);
        }
        final String finalUrl = url;
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<LeaveChecking> mGsonRequest = new GsonRequest<LeaveChecking>(
                Request.Method.GET,
                finalUrl,
                LeaveChecking.class,
                headers,
                responseCancelledLeaveListener(context, txtError, TAG),
                errorCancelledLeaveListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<LeaveChecking>
    responseCancelledLeaveListener(final Context context, final TextView txtError, final String TAG) {
        return new Response.Listener<LeaveChecking>() {
            @Override
            public void onResponse(LeaveChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }

                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getLeaves().size() > 0) {
                            context.getContentResolver().delete(LeaveProvider.CONTENT_URI,
                                    DBHelper.LEAVE_COLUMN_STATUS + "=? AND " +
                                            DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                    new String[]{"cancelled", String.valueOf(false)});
                            context.getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                    DBHelper.LEAVE_CHECKING_COLUMN_IS_CANCELLED + "=? AND " +
                                            DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                    new String[]{String.valueOf(true), String.valueOf(false)});
                            insertCancelledLeave(response, context, TAG);
                        }
                    }
                }
            }
        };
    }

    public static Response.ErrorListener errorCancelledLeaveListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }

    public static int getCancelledLeaveRow(Context context) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(LeaveProvider.CONTENT_URI, null,
                    DBHelper.LEAVE_COLUMN_STATUS + "=? AND " + DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE +
                            "=?", new String[]{"cancelled", String.valueOf(false)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getCancelledLeaveLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(LeaveCheckingProvider.CONTENT_URI,
                    null, DBHelper.LEAVE_CHECKING_COLUMN_IS_CANCELLED + "=? AND " +
                            DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                    new String[]{String.valueOf(true), String.valueOf(false)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static void insertCancelledLeave(LeaveChecking result, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesLeaveChecking = new ContentValues();
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_PER_PAGE, result.getNoPerPage());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_IS_CANCELLED, String.valueOf(true));
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_PAGE_NO, 0);
            context.getContentResolver().insert(LeaveCheckingProvider.CONTENT_URI, contentValuesLeaveChecking);
            ContentValues[] contentValueses = new ContentValues[result.getLeaves().size()];
            for (int i = 0; i < result.getLeaves().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_ID, result.getLeaves().get(i).getId());
                contentValues.put(DBHelper.LEAVE_COLUMN_EMPLOYEE_ID, result.getLeaves().get(i).getEmployeeId());
                contentValues.put(DBHelper.LEAVE_COLUMN_NAME, result.getLeaves().get(i).getName());
                contentValues.put(DBHelper.LEAVE_COLUMN_DEPARTMENT, result.getLeaves().get(i).getDepartment());
                contentValues.put(DBHelper.LEAVE_COLUMN_POSITION, result.getLeaves().get(i).getPosition());
                contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR, result.getLeaves().get(i).getSuperior());
                contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR_ID, result.getLeaves().get(i).getSuperiorId());
                contentValues.put(DBHelper.LEAVE_COLUMN_TYPE_CODE, result.getLeaves().get(i).getTypeCode());
                contentValues.put(DBHelper.LEAVE_COLUMN_TYPE, result.getLeaves().get(i).getType());
                contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, result.getLeaves().get(i).getRemarks());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_DAYS, result.getLeaves().get(i).getLeaveDays());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_FROM, result.getLeaves().get(i).getLeaveFrom());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_TO, result.getLeaves().get(i).getLeaveTo());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, result.getLeaves().get(i).getStatus());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_REMARKS, result.getLeaves().get(i).getStatusRemark());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_BY, result.getLeaves().get(i).getStatusUpdateBy());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_ON, result.getLeaves().get(i).getStatusUpdateOn());
                contentValues.put(DBHelper.LEAVE_COLUMN_LAST_UPDATE, result.getLeaves().get(i).getLastUpdate());
                contentValues.put(DBHelper.LEAVE_COLUMN_NEW_CREATE, String.valueOf(result.getLeaves().get(i).getNewCreate()));
                if (result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(context).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                } else if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(context).getId()) && !result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(context).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                } else if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(context).getId()) && result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(context).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
                }
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(context).getId())));
                contentValues.put(DBHelper.LEAVE_COLUMN_PAGE_NO, 0);
                contentValues.put(DBHelper.LEAVE_COLUMN_CREATE_DATE, result.getLeaves().get(i).getCreateDate());
                if (isLeaveExists(context, result.getLeaves().get(i).getId())) {
                    context.getContentResolver().delete(LeaveProvider.CONTENT_URI,
                            DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?",
                            new String[]{String.valueOf(result.getLeaves().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(LeaveProvider.CONTENT_URI, contentValueses);
            }

        } catch (Exception ex) {
            Log.i(TAG, "Content Provider My Leave Error :" + ex.toString());
        }
    }
    //endregion

    //region Single Leave
    public static void getSingleLeaveGson(Context context, Leave leave, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String url = Utils.API_URL + "api/Leave?id=" + Utils.encode(leave.getId());
        GsonRequest<Leave> mGsonRequest = new GsonRequest<Leave>(
                Request.Method.GET,
                Utils.API_URL + "api/Leave?id=" + Utils.encode(leave.getId()),
                Leave.class,
                headers,
                responseSingleLeaveListener(context, txtError, leave, TAG),
                errorSingleLeaveListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static boolean isSingleLeaveExists(Context context, Object id) {
        Cursor cursor = null;
        boolean isExists = false;
        try {
            cursor = context.getContentResolver().query(LeaveProvider.CONTENT_URI, null,
                    DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void insertLeave(Leave response, Context context, Leave leave, String TAG) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_ID, response.getId());
            contentValues.put(DBHelper.LEAVE_COLUMN_NAME, response.getName());
            contentValues.put(DBHelper.LEAVE_COLUMN_EMPLOYEE_ID, response.getEmployeeId());
            contentValues.put(DBHelper.LEAVE_COLUMN_DEPARTMENT, response.getDepartment());
            contentValues.put(DBHelper.LEAVE_COLUMN_POSITION, response.getPosition());
            contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR, response.getSuperior());
            contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR_ID, response.getSuperiorId());
            contentValues.put(DBHelper.LEAVE_COLUMN_TYPE_CODE, response.getTypeCode());
            contentValues.put(DBHelper.LEAVE_COLUMN_TYPE, response.getType());
            contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, response.getRemarks());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_DAYS, response.getLeaveDays());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_FROM, response.getLeaveFrom());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_TO, response.getLeaveTo());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, response.getStatus());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_REMARKS, response.getStatusRemark());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_BY, response.getStatusUpdateBy());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_ON, response.getStatusUpdateOn());
            contentValues.put(DBHelper.LEAVE_COLUMN_LAST_UPDATE, response.getLastUpdate());
            contentValues.put(DBHelper.LEAVE_COLUMN_NEW_CREATE, String.valueOf(response.getNewCreate()));
            contentValues.put(DBHelper.LEAVE_COLUMN_CREATE_DATE, response.getCreateDate());
            if (response.getSuperiorId().equals(Utils.getProfile(context).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
            } else if (!response.getSuperiorId().equals(Utils.getProfile(context).getId()) && !response.getEmployeeId().equals(Utils.getProfile(context).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
            } else if (!response.getSuperiorId().equals(Utils.getProfile(context).getId()) && response.getEmployeeId().equals(Utils.getProfile(context).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
            }
            if (isSingleLeaveExists(context, response.getId())) {
                context.getContentResolver().update(LeaveProvider.CONTENT_URI, contentValues,
                        DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(leave.getId())});
            } else {
                context.getContentResolver().insert(LeaveProvider.CONTENT_URI, contentValues);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Edit Application Error :" + ex.toString());
        }
    }

    public static Response.Listener<Leave> responseSingleLeaveListener(final Context context, final TextView txtError, final Leave leave, final String TAG) {
        return new Response.Listener<Leave>() {
            @Override
            public void onResponse(Leave response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response == null) {
                    context.getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(leave.getId())});
                    context.startActivity(new Intent(context, LeaveActivity.class));
                    ((Activity) context).finish();
                } else {
                    if (!response.getId().equals(leave.getId())) {
                        context.getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(leave.getId())});
                        context.startActivity(new Intent(context, LeaveActivity.class));
                        ((Activity) context).finish();
                    } else {
                        insertLeave(response, context, leave, TAG);
                    }
                }

            }
        };
    }

    public static Response.ErrorListener errorSingleLeaveListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

    //endregion
    //region leave read
    public static void getGsonReading(final Context context, final int id, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseListenerReading(context, id, TAG),
                errorListenerReading(TAG)) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" +
                        Utils.getProfile(context).getId() +
                        ",\"section\":\"leave\",\"action\":\"read\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerReading(final
                                                                        Context context, final int id,
                                                                        final String TAG) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ContentValues contentValues = new ContentValues();

                if (isBold(context, id)) {
                    contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, DashboardActivity.getNumber(context, "Leave"));
                    context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                            contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Leave"});
                    com.applab.wcircle_pro.Dashboard.HttpHelper.getIndicator(context, TAG, null);
                }

                contentValues = new ContentValues();
                contentValues.put(DBHelper.LEAVE_COLUMN_NEW_CREATE, "false");
                context.getContentResolver().update(LeaveProvider.CONTENT_URI, contentValues,
                        DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(id)});


                Log.i(TAG, "Success Leave Id: " + id);

            }
        };
    }


    public static boolean isBold(Context context, Object id) {
        boolean isBold = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(LeaveProvider.CONTENT_URI, null, DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                if (!cursor.isNull(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_NEW_CREATE))) {
                    isBold = Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_COLUMN_NEW_CREATE)));
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isBold;
    }

    public static Response.ErrorListener errorListenerReading(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }
    //endregion
}
