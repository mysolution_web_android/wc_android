package com.applab.wcircle_pro.Leave;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 13/10/2015.
 */
public class EmployeeLeaveViewHolder extends RecyclerView.ViewHolder {
    TextView txtDate;
    TextView txtDay;
    TextView txtType;
    TextView txtName,txtYear;
    ImageView btnInfo;

    public EmployeeLeaveViewHolder(View itemView) {
        super(itemView);
        txtDate = (TextView) itemView.findViewById(R.id.txtDate);
        txtDay = (TextView) itemView.findViewById(R.id.txtDay);
        txtType = (TextView) itemView.findViewById(R.id.txtType);
        txtName = (TextView) itemView.findViewById(R.id.txtName);
        txtYear = (TextView) itemView.findViewById(R.id.txtYear);
        btnInfo = (ImageView) itemView.findViewById(R.id.btnInfo);
    }
}