package com.applab.wcircle_pro.Leave;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by user on 5/8/2015.
 */
public class EmployeeLeaveAdapter extends RecyclerView.Adapter<EmployeeLeaveViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;
    private String TAG = "Event";

    public EmployeeLeaveAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public EmployeeLeaveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("EmployeeLeaveViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_leave_row, parent, false);
        view.setFocusable(true);
        view.setClickable(true);
        EmployeeLeaveViewHolder holder = new EmployeeLeaveViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(EmployeeLeaveViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Leave current = Leave.getLeave(cursor, position);
        holder.txtType.setText(current.getType());
        holder.txtName.setVisibility(View.VISIBLE);
        holder.txtName.setText(current.getName());
        holder.txtName.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        holder.txtType.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        holder.txtDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM", current.getLeaveFrom()));
        holder.txtDate.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        holder.txtDay.setText(Utils.setDate(Utils.DATE_FORMAT, "EEEE", current.getLeaveFrom()));
        holder.txtDay.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        holder.txtYear.setText(Utils.setDate(Utils.DATE_FORMAT, "yyyy", current.getLeaveFrom()));
        holder.txtYear.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        if (current.getStatus().equals("pending")) {
            holder.btnInfo.setImageResource(R.mipmap.info_p_2);
        } else if (current.getStatus().equals("rejected")) {
            holder.btnInfo.setImageResource(R.mipmap.info_r);
        } else if (current.getStatus().equals("approved")) {
            holder.btnInfo.setImageResource(R.mipmap.info_a);
        } else {
            holder.btnInfo.setImageResource(R.mipmap.info_c_2);
        }
    }

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            EmployeeLeaveAdapter.this.notifyDataSetChanged();
        }
    };
}
