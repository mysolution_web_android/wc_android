package com.applab.wcircle_pro.Leave;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.Menu.NavigationDrawerFragment;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Tabs.SlidingTabLayout;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

public class LeaveActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private String TAG = "LEAVE";
    private ViewPager mPager;
    private SlidingTabLayout mTabs;
    public static String ACTION = "LEAVE ACTION";
    private int page;
    private TextView txtDayNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green_blue));
        TextView txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_leave));
        ImageView space1 = (ImageView) findViewById(R.id.space1);
        space1.setVisibility(View.INVISIBLE);
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().
                findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout)
                findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setSelectedPosition(7);
        drawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new LeavePagerAdapter(getSupportFragmentManager(), this));
        mPager.setOffscreenPageLimit(3);
        mPager.addOnPageChangeListener(mPagerOnPageListener);
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setDistributeEvenly(true);
        mTabs.setmColor(3);
        mTabs.setCustomTabView(R.layout.custom_tab_view, R.id.tabText, R.id.tabNumber);
        mTabs.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
        mTabs.setSelectedIndicatorColors(ContextCompat.getColor(this, R.color.color_green_blue));
        mTabs.setViewPager(mPager);
        txtDayNo = (TextView) findViewById(R.id.txtDayNo);
        toolbar.setNavigationIcon(R.mipmap.action_arrow_prev_white);
        toolbar.setNavigationOnClickListener(toolbarOnClickListener);
        page = getIntent().getIntExtra("page", 0);
        mPager.setCurrentItem(page);
        getSupportLoaderManager().initLoader(324, null, this);
    }

    private ViewPager.OnPageChangeListener mPagerOnPageListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            page = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private View.OnClickListener toolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utils.clearPreviousActivity(LeaveActivity.this);
        }
    };


    @Override
    public void onBackPressed() {
        Utils.clearPreviousActivity(LeaveActivity.this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_leave, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (drawerFragment.mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else if (id == R.id.action_new) {
            Intent intent = new Intent(this, ApplicationActivity.class);
            intent.putExtra("page", page);
            intent.putExtra("myType", MyLeaveFragment.myType);
            intent.putExtra("employeeType", EmployeeLeaveFragment.employeeType);
            startActivity(intent);
            finish();
            return true;
        } else if (id == android.R.id.home) {
            Utils.clearPreviousActivity(LeaveActivity.this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 324) {
            return new CursorLoader(getBaseContext(), TypeProvider.CONTENT_URI, null, DBHelper.TYPE_COLUMN_LEAVE_CODE + "=?", new String[]{"AL"}, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 324) {
            if (data != null) {
                if (data.getCount() > 0) {
                    Type type = Type.getType(data, 0);
                    if (type != null) {
                        txtDayNo.setText(String.format("%.1f", type.getNoVailable()));
                    } else {
                        txtDayNo.setText("0");
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
