package com.applab.wcircle_pro.Leave;

/**
 * Created by user on 13/10/2015.
 */

import android.database.Cursor;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Type {

    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("LeaveCode")
    @Expose
    private String LeaveCode;
    @SerializedName("Title")
    @Expose
    private String Title;
    @SerializedName("Description")
    @Expose
    private String Description;
    @SerializedName("NoAvailable")
    @Expose
    private Double noVailable;

    public Double getNoVailable() {
        return noVailable;
    }

    public void setNoVailable(Double noVailable) {
        this.noVailable = noVailable;
    }

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The LeaveCode
     */
    public String getLeaveCode() {
        return LeaveCode;
    }

    /**
     * @param LeaveCode The LeaveCode
     */
    public void setLeaveCode(String LeaveCode) {
        this.LeaveCode = LeaveCode;
    }

    /**
     * @return The Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param Title The Title
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * @return The Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description The Description
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    public static Type getType(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Type type = new Type();
        type.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.TYPE_COLUMN_DESCRIPTION)));
        type.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.TYPE_COLUMN_ID)));
        type.setLeaveCode(cursor.getString(cursor.getColumnIndex(DBHelper.TYPE_COLUMN_LEAVE_CODE)));
        type.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.TYPE_COLUMN_TITLE)));
        if (!cursor.isNull(cursor.getColumnIndex(DBHelper.TYPE_COLUMN_NO_AVAILABLE))) {
            type.setNoVailable(cursor.getDouble(cursor.getColumnIndex(DBHelper.TYPE_COLUMN_NO_AVAILABLE)));
        } else {
            type.setNoVailable(0.0);
        }
        return type;
    }
}