package com.applab.wcircle_pro.Catalogue;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Environment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.applab.wcircle_pro.Menu.NavigationDrawerFragment;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.io.File;

public class CatalogueActivity extends AppCompatActivity implements
        SwipyRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private RecyclerView recyclerView;
    private CatalogueAdapter adapter;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private String TAG = "CATALOGUE";
    private Cursor cursor;
    private LinearLayoutManager linearLayoutManager;
    private int pageNo = 1;
    private int level = 0;
    private TextView txtError;
    public static String ACTION = "CATALOGUE";
    private String downloadPath;
    private File direct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_purple));
        TextView txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_catalogue));
        downloadPath = getBaseContext().getString(R.string.product_catalogue);
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().
                findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout)
                findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setSelectedPosition(10);
        drawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new CatalogueAdapter(this, cursor, getSupportFragmentManager());
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        setTouchListener();
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        getSupportLoaderManager().initLoader(0, null, this);
        txtError = (TextView) findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);
        txtError.setTag(false);
        direct = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.folder_name) + "/" + downloadPath);
        HttpHelper.getCatalogueGson(this, pageNo, level, TAG, txtError,direct);
        toolbar.setNavigationIcon(R.mipmap.action_arrow_prev_white);
        toolbar.setNavigationOnClickListener(toolbarOnClickListener);
    }

    private View.OnClickListener toolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utils.clearPreviousActivity(CatalogueActivity.this);
        }
    };

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
                final Catalogue current = (Catalogue) txtTitle.getTag();
                if (Utils.getStatus(Utils.CALENDAR_READ, CatalogueActivity.this)) {
                    HttpHelper.getCatalogueGsonReading(CatalogueActivity.this, current.getId(), TAG);
                }
                if (current.getType().equals("file")) {
                    if (current.getPath() != null) {
                        if (Utils.localFileExists(current.getPath())) {
                            Utils.openDocument(current.getPath(),CatalogueActivity.this);
                        } else {
                            String message = Utils.DOWNLOAD_ALERT + getBaseContext().getString(R.string.folder_name) + "/" + downloadPath;
                            DownloadCatalogueDialogFragment downloadCatalogueDialogFragment =
                                    DownloadCatalogueDialogFragment.newInstance(message,
                                            CatalogueActivity.this, current.getFilePath(), current.getFilePath(), downloadPath, current.getId());
                            downloadCatalogueDialogFragment.show(getSupportFragmentManager(), "");
                        }
                    } else {
                        String message = Utils.DOWNLOAD_ALERT + getBaseContext().getString(R.string.folder_name) + "/" + downloadPath;
                        DownloadCatalogueDialogFragment downloadCatalogueDialogFragment =
                                DownloadCatalogueDialogFragment.newInstance(message,
                                        CatalogueActivity.this, current.getFilePath(), current.getFilePath(), downloadPath, current.getId());
                        downloadCatalogueDialogFragment.show(getSupportFragmentManager(), "");
                    }
                } else {
                    Intent intent = new Intent(getBaseContext(), SubCatalogueOneActivity.class);
                    intent.putExtra("Title", current.getTitle());
                    intent.putExtra("level", current.getId());
                    intent.putExtra("downloadPath", downloadPath + "/" + current.getTitle());
                    startActivity(intent);
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }


    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (lastVisibleItem >= totalItemCount - 1) {
                        pageNo++;
                        HttpHelper.getCatalogueGson(CatalogueActivity.this, pageNo, level, TAG, txtError,direct);
                    } else if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }
            }
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getBaseContext(), CatalogueProvider.CONTENT_URI,
                null, DBHelper.CATALOGUE_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.cursor = data;
        if (!(Boolean) txtError.getTag()) {
            adapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d("CatalogueActivity", "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            pageNo = 1;
            HttpHelper.getCatalogueGson(CatalogueActivity.this, pageNo, level, TAG, txtError,direct);
        } else {
            pageNo++;
            HttpHelper.getCatalogueGson(CatalogueActivity.this, pageNo, level, TAG, txtError,direct);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }

    @Override
    public void onBackPressed() {
        Utils.clearPreviousActivity(CatalogueActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_catalogue, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (drawerFragment.mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            Utils.clearPreviousActivity(CatalogueActivity.this);
        }

        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                if (!intent.getBooleanExtra("isConnect", false)) {
                    adapter.swapCursor(null);
                } else {
                    adapter.swapCursor(cursor);
                }
            }else if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                getSupportLoaderManager().restartLoader(0, null, CatalogueActivity.this);
            }
        }
    };


    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        drawerFragment.setSelectedPosition(10);
        IntentFilter iff = new IntentFilter(ACTION);
        iff.addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, iff);
    }
}
