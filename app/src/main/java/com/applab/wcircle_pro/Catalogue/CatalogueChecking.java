package com.applab.wcircle_pro.Catalogue;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class CatalogueChecking {

    @Expose
    private String LastUpdate;
    @Expose
    private Integer NoOfRecords;
    @Expose
    private Integer NoOfPage;
    @Expose
    private List<Catalogue> items = new ArrayList<Catalogue>();
    @Expose
    private String Message;

    /**
     * @return The Message
     */
    public String getMessage() {
        return Message;
    }

    /**
     * @param Message The Message
     */
    public void setMessage(String Message) {
        this.Message = Message;
    }

    /**
     *
     * @return
     * The LastUpdate
     */
    public String getLastUpdate() {
        return LastUpdate;
    }

    /**
     *
     * @param LastUpdate
     * The LastUpdate
     */
    public void setLastUpdate(String LastUpdate) {
        this.LastUpdate = LastUpdate;
    }

    /**
     *
     * @return
     * The NoOfRecords
     */
    public Integer getNoOfRecords() {
        return NoOfRecords;
    }

    /**
     *
     * @param NoOfRecords
     * The NoOfRecords
     */
    public void setNoOfRecords(Integer NoOfRecords) {
        this.NoOfRecords = NoOfRecords;
    }

    /**
     *
     * @return
     * The NoOfPage
     */
    public Integer getNoOfPage() {
        return NoOfPage;
    }

    /**
     *
     * @param NoOfPage
     * The NoOfPage
     */
    public void setNoOfPage(Integer NoOfPage) {
        this.NoOfPage = NoOfPage;
    }

    /**
     *
     * @return
     * The items
     */
    public List<Catalogue> getCatalogues() {
        return items;
    }

    /**
     *
     * @param items
     * The items
     */
    public void setCatalogues(List<Catalogue> items) {
        this.items = items;
    }

}
