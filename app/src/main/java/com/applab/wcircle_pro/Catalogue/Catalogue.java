package com.applab.wcircle_pro.Catalogue;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

public class Catalogue implements Parcelable {

    @Expose
    private Integer Id;
    @Expose
    private String Type;
    @Expose
    private String Title;
    @Expose
    private String FilePath;
    @Expose
    private String FileSize;
    @Expose
    private String Description;
    @Expose
    private String CreatedBy;
    @Expose
    private Integer FileSizeBytes;
    @Expose
    private String CreatedDate;

    private String path;

    @Expose
    private boolean NewCreate = false;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean getNewCreate() {
        return NewCreate;
    }

    public void setNewCreate(boolean newCreate) {
        NewCreate = newCreate;
    }

    public Integer getFileSizeBytes() {
        return FileSizeBytes;
    }

    public void setFileSizeBytes(Integer fileSizeBytes) {
        FileSizeBytes = fileSizeBytes;
    }

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The Type
     */
    public String getType() {
        return Type;
    }

    /**
     * @param Type The Type
     */
    public void setType(String Type) {
        this.Type = Type;
    }

    /**
     * @return The Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param Title The Title
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * @return The FilePath
     */
    public String getFilePath() {
        return FilePath;
    }

    /**
     * @param FilePath The FilePath
     */
    public void setFilePath(String FilePath) {
        this.FilePath = FilePath;
    }

    /**
     * @return The FileSize
     */
    public String getFileSize() {
        return FileSize;
    }

    /**
     * @param FileSize The FileSize
     */
    public void setFileSize(String FileSize) {
        this.FileSize = FileSize;
    }

    /**
     * @return The Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description The Description
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return The CreatedBy
     */
    public String getCreatedBy() {
        return CreatedBy;
    }

    /**
     * @param CreatedBy The CreatedBy
     */
    public void setCreatedBy(String CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    /**
     * @return The CreatedDate
     */
    public String getCreatedDate() {
        return CreatedDate;
    }

    /**
     * @param CreatedDate The CreatedDate
     */
    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public Catalogue() {
    }

    public Catalogue(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeString(this.Type);
        dest.writeString(this.Title);
        dest.writeString(this.FilePath);
        dest.writeString(this.FileSize);
        dest.writeString(this.Description);
        dest.writeString(this.CreatedBy);
        dest.writeString(this.CreatedDate);
        dest.writeByte((byte) (NewCreate ? 1 : 0));
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.Id = in.readInt();
        this.Type = in.readString();
        this.Title = in.readString();
        this.FilePath = in.readString();
        this.FileSize = in.readString();
        this.Description = in.readString();
        this.CreatedBy = in.readString();
        this.CreatedDate = in.readString();
        this.NewCreate = in.readByte() != 0;
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Catalogue createFromParcel(Parcel in) {
            return new Catalogue(in);
        }

        @Override
        public Catalogue[] newArray(int size) {
            return new Catalogue[size];
        }
    };

}
