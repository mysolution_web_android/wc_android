package com.applab.wcircle_pro.Catalogue;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.R;

import android.widget.ImageView;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by user on 5/8/2015.
 */
public class SubCatalogueAdapter extends RecyclerView.Adapter<CatalogueViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private Cursor cursor;
    private FragmentManager fragmentManager;
    private String TAG = "DOWNLOAD CATALOGUE";

    public SubCatalogueAdapter(Context context, Cursor cursor, FragmentManager fragmentManager) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public CatalogueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("CatalogueViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_catalogue_row, parent, false);
        CatalogueViewHolder holder = new CatalogueViewHolder(view);
        return holder;
    }

    private void setImgClickOnListener(final Cursor cursor, final int position, final ImageView img) {
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Catalogue current = getData(cursor, position);
                if (Utils.getStatus(Utils.CALENDAR_READ, context)) {
                    HttpHelper.getCatalogueGsonReading(context, current.getId(), TAG);
                }
                if (current.getType().equals("file")) {
                    SubCatalogueDialogFragment subCatalogueDialogFragment =
                            SubCatalogueDialogFragment.newInstance(current, context);
                    subCatalogueDialogFragment.show(fragmentManager, "");
                } else {
                    if (!context.getClass().getSimpleName().equals("SubCatalogueFourActivity")) {
                        Intent intent = new Intent(context, SubCatalogueThreeActivity.class);
                        intent.putExtra("Title", current.getTitle());
                        intent.putExtra("level", current.getId());
                        context.startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public void onBindViewHolder(CatalogueViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Catalogue current = getData(cursor, position);
        if (current.getType().equals("folder")) {
            holder.title.setText(current.getTitle());
            holder.title.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
            holder.icon.setImageResource(R.mipmap.info_folder);
            holder.img.setVisibility(View.INVISIBLE);
            holder.lv.setVisibility(View.GONE);
            holder.title.setTag(current);
            holder.imgStatus.setVisibility(View.GONE);
        } else {
            if (current.getPath() != null) {
                if (Utils.localFileExists(current.getPath())) {
                    holder.imgStatus.setVisibility(View.VISIBLE);
                } else {
                    holder.imgStatus.setVisibility(View.GONE);
                }
            } else {
                holder.imgStatus.setVisibility(View.GONE);
            }
            holder.lv.setVisibility(View.VISIBLE);
            holder.img.setVisibility(View.VISIBLE);
            holder.title.setText(current.getTitle());
            holder.title.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
            holder.date.setText(current.getCreatedDate());
            String[] result = current.getFilePath().split("\\.");
            String type = result[result.length - 1];
            holder.icon.setImageResource(Utils.getFileType(type));
            holder.img.setImageResource(R.mipmap.action_information);
            holder.title.setTag(current);
        }
        setImgClickOnListener(cursor, position, holder.img);
    }

    public Catalogue getData(Cursor cursor, int position) {
        Catalogue current = new Catalogue();
        cursor.moveToPosition(position);
        current.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_CATALOGUE_ID)));
        current.setType(cursor.getString(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_TYPE)));
        current.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_TITLE)));
        current.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_DESCRIPTION)));
        current.setFilePath(cursor.getString(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_FILE_PATH)));
        if (current.getType().equals("file")) {
            current.setFilePath(current.getFilePath().replaceAll(" ", "%20"));
        }
        current.setFileSize(cursor.getString(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_FILE_SIZE)).toUpperCase());
        current.setCreatedBy(cursor.getString(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_CREATED_BY)));
        current.setCreatedDate(cursor.getString(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_CREATED_DATE)));
        current.setCreatedDate(Utils.setDate(Utils.DATE_FORMAT, "dd MMMM yyyy", current.getCreatedDate()));
        current.setNewCreate(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_NEW_CREATE))));
        current.setPath(cursor.getString(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_PATH)));
        return current;
    }

    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            SubCatalogueAdapter.this.notifyDataSetChanged();
        }
    };
}
