package com.applab.wcircle_pro.Catalogue;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 27/10/2015.
 */
public class SubCatalogueDialogFragment extends DialogFragment {
    private Catalogue mCatalogue;
    private TextView mTxtTitle, mTxtDetails, mTxtSize, mTxtUploadedBy, mTxtUploadedDate;
    private ImageView mBtnCancel;
    private static AlertDialog.Builder builder;
    private static Context mContext;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static SubCatalogueDialogFragment newInstance(Catalogue catalogue, Context context) {
        SubCatalogueDialogFragment frag = new SubCatalogueDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("Catalogue", catalogue);
        mContext = context;
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mCatalogue = getArguments().getParcelable("Catalogue");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_catalogue, null);
        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtTitle.setText(mCatalogue.getTitle());
        mTxtDetails = (TextView) v.findViewById(R.id.txtDetails);
        mTxtDetails.setText(mCatalogue.getDescription());
        mTxtSize = (TextView) v.findViewById(R.id.txtSize);
        mTxtSize.setText(mCatalogue.getFileSize());
        mTxtUploadedBy = (TextView) v.findViewById(R.id.txtUploadedBy);
        mTxtUploadedBy.setText(mCatalogue.getCreatedBy());
        mTxtUploadedDate = (TextView) v.findViewById(R.id.txtUploadedDate);
        mTxtUploadedDate.setText(mCatalogue.getCreatedDate());
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SubCatalogueDialogFragment.this.getDialog().cancel();
        }
    };
}

