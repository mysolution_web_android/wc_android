package com.applab.wcircle_pro.Catalogue;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by user on 27/10/2015.
 */
public class DownloadCatalogueDialogFragment extends DialogFragment {
    private String mMessage, mUrl, mFileName, mFolderName;
    private int mId;
    private TextView mTxtTitle, mTxtMessage;
    private ImageView mBtnCancel;
    private LinearLayout mBtnYes, mBtnNo;
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private String TAG = "DOWNLOAD CATALOGUE";

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */

    public static DownloadCatalogueDialogFragment newInstance(String message, final Context context, final String url,
                                                              final String fileName, final String folderName, final int id) {
        DownloadCatalogueDialogFragment frag = new DownloadCatalogueDialogFragment();
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putString("url", url);
        args.putString("fileName", fileName);
        args.putString("folderName", folderName);
        args.putInt("id", id);
        mContext = context;
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mMessage = getArguments().getString("message");
        mUrl = getArguments().getString("url");
        mFileName = getArguments().getString("fileName");
        mFolderName = getArguments().getString("folderName");
        mId = getArguments().getInt("id");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_dialog, null);
        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtMessage = (TextView) v.findViewById(R.id.txtMessage);
        mTxtTitle.setText(getResources().getString(R.string.download));
        mTxtMessage.setText(mMessage);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnYes = (LinearLayout) v.findViewById(R.id.btnYes);
        mBtnNo = (LinearLayout) v.findViewById(R.id.btnNo);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);
        mBtnYes.setOnClickListener(btnYesOnClickListener);
        mBtnNo.setOnClickListener(btnNoOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DownloadCatalogueDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnYesOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utils.downloadFile(mUrl, mContext, mFileName, mFolderName, "Catalogue", mId);
            if (Utils.getStatus(Utils.CATALOGUE_DOWNLOAD, mContext)) {
                HttpHelper.getCatalogueGsonDownload(mContext, mId, TAG);
            }
            DownloadCatalogueDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnNoOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DownloadCatalogueDialogFragment.this.getDialog().cancel();
        }
    };
}

