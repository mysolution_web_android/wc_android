package com.applab.wcircle_pro.Catalogue;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Dashboard.DashboardProvider;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by user on 25/11/2015.
 */
public class HttpHelper {
    //region Catalogue
    public static void getCatalogueGson(final Context context, final int pageNo,
                                        final int level, final String TAG, TextView txtError, final File direct) {
        final String token = Utils.getToken(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        final Date lastUpdateDate = getCatalogueLastUpdate(context);
        String url = Utils.API_URL + "api/ProductCatalog" + "/" + Utils.encode(level)
                + "?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo)
                + "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty="
                + Utils.encode(getCatalogueRow(context, level) > 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/ProductCatalog" + "/" + Utils.encode(level)
                    + "?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo)
                    + "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty="
                    + Utils.encode(getCatalogueRow(context, level) > 0);
        }
        final String finalUrl = url;
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<CatalogueChecking> mGsonRequest = new GsonRequest<CatalogueChecking>(
                Request.Method.GET,
                finalUrl,
                CatalogueChecking.class,
                headers,
                responseCatalogueListener(context, pageNo, txtError, level, TAG, direct),
                errorCatalogueListener(context, txtError)) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "employeeId=" + Utils.getProfile(context).getId() + "&NoPerPage="
                        + Utils.PAGING_NUMBER + "&PageNo=" + String.valueOf(pageNo);
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<CatalogueChecking> responseCatalogueListener(final Context context,
                                                                                 final int pageNo, final TextView txtError,
                                                                                 final int level, final String TAG, final File direct) {
        return new Response.Listener<CatalogueChecking>() {
            @Override
            public void onResponse(CatalogueChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                    txtError.setTag(false);
                }
                Intent intent = new Intent(CatalogueActivity.ACTION);
                intent.putExtra("isConnect", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getCatalogues().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getCatalogueLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(CatalogueProvider.CONTENT_URI, DBHelper.CATALOGUE_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)});
                                        context.getContentResolver().delete(CatalogueCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(CatalogueCheckingProvider.CONTENT_URI,
                                                null, null);
                                    }
                                    insertCatalogue(response, context, level, pageNo, TAG, direct);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(CatalogueProvider.CONTENT_URI, DBHelper.CATALOGUE_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)});
                                        context.getContentResolver().delete(CatalogueCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(CatalogueCheckingProvider.CONTENT_URI,
                                                null, null);
                                    }
                                    insertCatalogue(response, context, level, pageNo, TAG, direct);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(CatalogueProvider.CONTENT_URI, DBHelper.CATALOGUE_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)});
                                    context.getContentResolver().delete(CatalogueCheckingProvider.CONTENT_URI, null, null);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(CatalogueCheckingProvider.CONTENT_URI,
                                            null, null);
                                }
                                insertCatalogue(response, context, level, pageNo, TAG, direct);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(CatalogueProvider.CONTENT_URI, DBHelper.CATALOGUE_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)});
                                    context.getContentResolver().delete(CatalogueCheckingProvider.CONTENT_URI, null, null);
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    public static int getCatalogueRow(Context context, int level) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CatalogueProvider.CONTENT_URI, null,
                    DBHelper.CATALOGUE_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
                cursor.close();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return totalRow;
    }

    public static Response.ErrorListener errorCatalogueListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                    txtError.setTag(true);
                } else {
                    Utils.serverHandlingError(context, error);
                }
                Intent intent = new Intent(CatalogueActivity.ACTION);
                intent.putExtra("isConnect", false);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    public static Date getCatalogueLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.CATALOGUE_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CatalogueCheckingProvider.CONTENT_URI,
                    projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.
                            getColumnIndex(DBHelper.CATALOGUE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
                cursor.close();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static boolean isCatalogueExists(Context context, Object id) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CatalogueProvider.CONTENT_URI, null,
                    DBHelper.CATALOGUE_COLUMN_CATALOGUE_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void insertCatalogue(CatalogueChecking result, Context context,
                                       final int level, final int pageNo, final String TAG, final File direct) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesCatalogueChecking = new ContentValues();
            contentValuesCatalogueChecking.put(DBHelper.CATALOGUE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesCatalogueChecking.put(DBHelper.CATALOGUE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesCatalogueChecking.put(DBHelper.CATALOGUE_CHECKING_COLUMN_LEVEL, level);
            contentValuesCatalogueChecking.put(DBHelper.CATALOGUE_CHECKING_COLUMN_PAGE_NO, pageNo);
            contentValuesCatalogueChecking.put(DBHelper.CATALOGUE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            context.getContentResolver().insert(CatalogueCheckingProvider.CONTENT_URI, contentValuesCatalogueChecking);
            ContentValues[] contentValueses = new ContentValues[result.getCatalogues().size()];
            for (int i = 0; i < result.getCatalogues().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.CATALOGUE_COLUMN_CATALOGUE_ID, result.getCatalogues().get(i).getId());
                contentValues.put(DBHelper.CATALOGUE_COLUMN_TITLE, result.getCatalogues().get(i).getTitle());
                contentValues.put(DBHelper.CATALOGUE_COLUMN_DESCRIPTION, result.getCatalogues().get(i).getDescription());
                contentValues.put(DBHelper.CATALOGUE_COLUMN_FILE_PATH, result.getCatalogues().get(i).getFilePath());
                contentValues.put(DBHelper.CATALOGUE_COLUMN_TYPE, result.getCatalogues().get(i).getType());
                contentValues.put(DBHelper.CATALOGUE_COLUMN_FILE_SIZE, result.getCatalogues().get(i).getFileSize());
                contentValues.put(DBHelper.CATALOGUE_COLUMN_LEVEL, level);
                contentValues.put(DBHelper.CATALOGUE_COLUMN_FILE_SIZE_BYTES, result.getCatalogues().get(i).getFileSizeBytes());
                contentValues.put(DBHelper.CATALOGUE_COLUMN_PAGE_NO, pageNo);
                contentValues.put(DBHelper.CATALOGUE_COLUMN_CREATED_BY, result.getCatalogues().get(i).getCreatedBy());
                contentValues.put(DBHelper.CATALOGUE_COLUMN_NEW_CREATE, String.valueOf(result.getCatalogues().get(i).getNewCreate()));
                contentValues.put(DBHelper.CATALOGUE_COLUMN_CREATED_DATE, result.getCatalogues().get(i).getCreatedDate());
                contentValues.put(DBHelper.CATALOGUE_COLUMN_PATH, direct.getAbsolutePath() + "/" + result.getCatalogues().get(i).getTitle());
                if (isCatalogueExists(context, result.getCatalogues().get(i).getId())) {
                    context.getContentResolver().delete(CatalogueProvider.CONTENT_URI,
                            DBHelper.CATALOGUE_COLUMN_CATALOGUE_ID + "=?", new String[]{String.valueOf(result.getCatalogues().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(CatalogueProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Catalogue Error :" + ex.toString());
        }
    }
    //endregion

    //region Download Catalogue
    public static void getCatalogueGsonDownload(final Context context, final int id, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseListenerDownload(TAG),
                errorListenerDownload(TAG)) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" + Utils.getProfile(context).getId() +
                        ",\"section\":\"product\",\"action\":\"download\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerDownload(final String TAG) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Success send reading status");
            }
        };
    }

    public static Response.ErrorListener errorListenerDownload(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }
    //endregion

    //region Read Catalogue
    public static void getCatalogueGsonReading(final Context context, final int id, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseListenerReading(context, TAG, id),
                errorListenerReading(TAG)) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" + Utils.getProfile(context).getId() +
                        ",\"section\":\"product\",\"action\":\"read\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerReading(final Context context, final String TAG, final int id) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ContentValues contentValues = new ContentValues();

                if (isBold(context, id)) {
                    contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, DashboardActivity.getNumber(context, "Product Catalogue"));
                    context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                            contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Product Catalogue"});
                    com.applab.wcircle_pro.Dashboard.HttpHelper.getIndicator(context, TAG, null);
                }

                contentValues = new ContentValues();
                contentValues.put(DBHelper.CATALOGUE_COLUMN_NEW_CREATE, String.valueOf(false));
                context.getContentResolver().update(CatalogueProvider.CONTENT_URI, contentValues,
                        DBHelper.CATALOGUE_COLUMN_CATALOGUE_ID + "=?", new String[]{String.valueOf(id)});
                Log.i(TAG, "Success send reading status");
            }
        };
    }

    public static boolean isBold(Context context, Object id) {
        boolean isBold = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CatalogueProvider.CONTENT_URI, null, DBHelper.CATALOGUE_COLUMN_CATALOGUE_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_NEW_CREATE))) {
                    isBold = Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.CATALOGUE_COLUMN_NEW_CREATE)));
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isBold;
    }

    public static Response.ErrorListener errorListenerReading(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }
    //endregion
}
