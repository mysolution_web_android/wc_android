package com.applab.wcircle_pro.Register;

/**
 * Created by admin on 23/7/2015.
 */
import com.google.gson.annotations.Expose;

public class Register {

    @Expose
    private Integer employeeId;
    @Expose
    private String Email;

    /**
     *
     * @return
     * The employeeId
     */
    public Integer getemployeeId() {
        return employeeId;
    }

    /**
     *
     * @param employeeId
     * The employeeId
     */
    public void setemployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    /**
     *
     * @return
     * The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param Email
     * The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

}
