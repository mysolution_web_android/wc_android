package com.applab.wcircle_pro.Register;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.ErrorDialogFragment;
import com.applab.wcircle_pro.Login.LoginActivity;
import com.applab.wcircle_pro.Profile.ProfileProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private EditText ediEmail, ediPassword, ediConfirmPassword;
    private CheckBox ckEmail, ckPassword, ckConfirmPassword;
    private LinearLayout btnSubmit, btnCancel;
    private String TAG = "REGISTER";
    private String json = null;
    private RelativeLayout fadeRL;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        TextView txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText("Sign Up");
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        ediEmail = (EditText) findViewById(R.id.ediEmail);
        ediPassword = (EditText) findViewById(R.id.ediPassword);
        ediConfirmPassword = (EditText) findViewById(R.id.ediConfirmPassword);
        ckPassword = (CheckBox) findViewById(R.id.ckPassword);
        ckConfirmPassword = (CheckBox) findViewById(R.id.ckConfirmPassword);
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        fadeRL.setOnClickListener(fadeRLOnClickListener);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        btnSubmit = (LinearLayout) findViewById(R.id.btnSubmit);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        btnSubmit.setOnClickListener(btnSubmitOnClickListener);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        passwordWatcher();
        confirmPasswordWatcher();
    }

    private View.OnClickListener fadeRLOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private void setVisibilityRlAndProgressBar(boolean isVisible) {
        if (isVisible) {
            fadeRL.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            fadeRL.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    private LinearLayout.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            register(ediEmail.getText().toString(), ediPassword.getText().toString(), ediConfirmPassword.getText().toString());
        }
    };

    private LinearLayout.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibilityRlAndProgressBar(true);
            startActivity(new Intent(getBaseContext(), LoginActivity.class));
            finish();
        }
    };

    private void emailWatcher() {
        ediEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Utils.isValidEmail(ediEmail.getText().toString())) {
                    ckEmail.setChecked(true);
                } else {
                    ckEmail.setChecked(false);
                }
            }
        });
    }

    private void confirmPasswordWatcher() {
        ediConfirmPassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isValidPassword(ediConfirmPassword.getText().toString()) || isPasswordMatching(ediPassword.getText().toString(),
                        ediConfirmPassword.getText().toString())) {
                    ckConfirmPassword.setChecked(true);
                } else {
                    ckConfirmPassword.setChecked(false);
                }
            }
        });
    }

    private void passwordWatcher() {
        ediPassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isValidPassword(ediPassword.getText().toString())) {
                    ckPassword.setChecked(true);
                } else {
                    ckPassword.setChecked(false);
                }
            }
        });
    }

    private void focusPassword() {
        ediPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String msg = "";
                    msg += passwordValidation(0, ediPassword.getText().toString());
                    if (!msg.equals("")) {
                        ErrorDialogFragment leaveErrorDialogFragment = ErrorDialogFragment.newInstance(RegisterActivity.this, msg);
                        leaveErrorDialogFragment.setCancelable(false);
                        leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
                    }
                }
            }
        });
    }

    private void focusConfirmPassword() {
        ediConfirmPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String msg = "";
                    msg += passwordValidation(1, ediConfirmPassword.getText().toString());
                    if (!msg.equals("")) {
                        ErrorDialogFragment leaveErrorDialogFragment = ErrorDialogFragment.newInstance(RegisterActivity.this, msg);
                        leaveErrorDialogFragment.setCancelable(false);
                        leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
                    }
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        setVisibilityRlAndProgressBar(true);
        startActivity(new Intent(getBaseContext(), LoginActivity.class));
        finish();
    }

    private void register(final String email, final String password, final String confirmPassword) {
        String error = "";
        boolean isValid = true;
        if (email.equals("")) {
            error = Utils.EMAIL_FIELD;
            isValid = false;
        } else if (password.equals("")) {
            error = Utils.PASSWORD_FIELD;
            isValid = false;
        } else if (confirmPassword.equals("")) {
            error = Utils.CONFIRM_PASSWORD_FIELD;
            isValid = false;
        } else if (!Utils.isValidEmail(email)) {
            error = Utils.EMAIL_VALID;
            isValid = false;
        } else if (!isValidPassword(password)) {
            error = Utils.PASSWORD_LENGTH;
            isValid = false;
        } else if (!isValidPassword(confirmPassword)) {
            error = Utils.CONFIRM_PASSWORD_LENGTH;
            isValid = false;
        } else if (!isPasswordMatching(password, confirmPassword)) {
            error = Utils.PASSWORD_MATCH;
            isValid = false;
        }
        if (isValid) {
            setVisibilityRlAndProgressBar(true);
            GsonRequest<Register> mGsonRequest = new GsonRequest<Register>(
                    Request.Method.POST,
                    Utils.API_URL + "api/Account/Register",
                    Register.class,
                    null,
                    responseListener(),
                    errorListener()) {
                @Override
                public byte[] getBody() {
                    String httpPostBody = "email=" + email + "&password=" + password + "&confirmpassword=" + confirmPassword + "";
                    return httpPostBody.getBytes();
                }
            };
            mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
        } else {
            ErrorDialogFragment leaveErrorDialogFragment = ErrorDialogFragment.newInstance(RegisterActivity.this, error);
            leaveErrorDialogFragment.setCancelable(false);
            leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
        }
    }

    private Response.Listener<Register> responseListener() {
        return new Response.Listener<Register>() {
            @Override
            public void onResponse(Register response) {
                try {
                    try {
                        getContentResolver().delete(ProfileProvider.CONTENT_URI, null, null);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DBHelper.PROFILE_COLUMN_EMAIL, response.getEmail());
                        contentValues.put(DBHelper.PROFILE_COLUMN_ID, response.getemployeeId());
                        getContentResolver().insert(ProfileProvider.CONTENT_URI, contentValues);
                    } catch (Exception e) {
                        Log.i(TAG, e.getMessage());
                    }
                    Log.i(TAG, "Retrieve JSON : " + response);
                    Toast.makeText(getBaseContext(), "Success registered", Toast.LENGTH_LONG).show();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public String trimMessage(String json, String key) {
        String trimmedString = null;
        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.validateNetworkAndTimeOutError(RegisterActivity.this, error, true);
                if (error instanceof AuthFailureError) {
                } else if (error instanceof ServerError) {
                    NetworkResponse response = error.networkResponse;
                    if (response != null && response.data != null) {
                        switch (response.statusCode) {
                            case 400:
                                json = new String(response.data);
                                Log.i(TAG, json);
                                if (json.contains("model.ConfirmPassword")) {
                                    json = trimMessage(json, "ModelState", "model.ConfirmPassword");
                                } else if (json.contains("Please use a valid App Lab email address to register")) {
                                    json = trimMessage(json, "Message");
                                } else {
                                    json = trimMessage(json, "ModelState", "");
                                }
                                if (json != null) {
                                    ErrorDialogFragment leaveErrorDialogFragment = ErrorDialogFragment.newInstance(RegisterActivity.this, json);
                                    leaveErrorDialogFragment.setCancelable(false);
                                    leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
                                }
                                break;
                        }
                    }
                } else if (error instanceof NetworkError) {
                } else if (error instanceof ParseError) {
                }
                setVisibilityRlAndProgressBar(false);
            }
        };
    }

    private void pushRegId(final String token, final String id) {
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Device",
                JSONObject.class,
                null,
                responseListenerDevice(),
                errorListener()) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "EmployeeId=" + id + "&Platform=1&Token=" + token + "";
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<JSONObject> responseListenerDevice() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        };
    }

    public String trimMessage(String json, String key1, String key2) {
        ArrayList<String> arrString = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(json);
            JSONObject modelObj = obj.getJSONObject(key1);
            arrString = parseRegister(modelObj, key2);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return arrString.get(0);
    }

    public ArrayList<String> parseRegister(JSONObject object, String modelState) {
        ArrayList<String> arrString = new ArrayList<>();
        try {
            JSONArray arr = object.getJSONArray(modelState);
            for (int i = 0; i < arr.length(); i++) {
                String message = arr.getString(i);
                arrString.add(message);
            }
            return arrString;
        } catch (JSONException e) {
            Log.i("parseRegister", e.getMessage());
        }
        return null;
    }

    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() >= 6) {
            return true;
        }
        return false;
    }

    private String passwordValidation(int type, String pass) {
        String msg = "";
        String PASSWORD_PATTERN;
        Pattern pattern;
        Matcher matcher;
        if (pass != null) {
            PASSWORD_PATTERN = "((?=.*\\d))";
            pattern = Pattern.compile(PASSWORD_PATTERN);
            matcher = pattern.matcher(pass);
            if (!matcher.matches()) {
                msg += "Password must contains one digit from 0-9\n";
            }
            PASSWORD_PATTERN = "((?!.*\\\\s))";
            pattern = Pattern.compile(PASSWORD_PATTERN);
            matcher = pattern.matcher(pass);
            if (!matcher.matches()) {
                msg += "Password is disallowed to contain the spaces\n";
            }
            PASSWORD_PATTERN = "((?=.*[a-z]))";
            pattern = Pattern.compile(PASSWORD_PATTERN);
            matcher = pattern.matcher(pass);
            if (!matcher.matches()) {
                msg += "Password must contains one lowercase characters\n";
            }
            PASSWORD_PATTERN = "((?=.*[A-Z]))";
            pattern = Pattern.compile(PASSWORD_PATTERN);
            matcher = pattern.matcher(pass);
            if (!matcher.matches()) {
                msg += "Password must contains one uppercase characters\n";
            }
        }

        if (type == 1) {
            if (msg.contains("Password")) {
                msg = msg.replace("Password", "Confirm password");
            }
        }
        return msg;
    }

    public boolean isPasswordMatching(String password, String confirmPassword) {
        if (password.equals(confirmPassword)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            setVisibilityRlAndProgressBar(true);
            NavUtils.navigateUpFromSameTask(this);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
