package com.applab.wcircle_pro.Favorite;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/2/2015.
 */
public class FavoriteViewHolder extends RecyclerView.ViewHolder {
    TextView title;
    ImageView icon;

    public FavoriteViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.listText);
        icon = (ImageView) itemView.findViewById(R.id.listIcon);
    }

}
