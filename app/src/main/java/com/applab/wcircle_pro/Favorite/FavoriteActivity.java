package com.applab.wcircle_pro.Favorite;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.Employee.EmployeeActivity;
import com.applab.wcircle_pro.Menu.NavigationDrawerFragment;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

public class FavoriteActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, SwipyRefreshLayout.OnRefreshListener {
    //region variable
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private TextView txtToolbarTitle;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private EditText ediSearch;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private Cursor cursor;
    private ContactAdapter adapter;
    private String selection;
    private String[] selectionArgs;
    private String TAG = "FAVORITE";
    private int pageNo = 1;
    private TextView txtError;
    //endregion

    //region create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_red));
        ImageView space1 = (ImageView) toolbar.findViewById(R.id.space1);
        space1.setVisibility(View.INVISIBLE);
        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_my_favorite));
        ediSearch = (EditText) findViewById(R.id.ediSearch);
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        drawerFragment.setSelectedPosition(1);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new ContactAdapter(this, cursor);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        setTouchListener();
        getSupportLoaderManager().initLoader(0, null, this);
        txtError = (TextView) findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);
        HttpHelper.getFavoriteGson(pageNo, FavoriteActivity.this, TAG, txtError);
        ediSearch.setOnEditorActionListener(ediSearchOnEditActionListener);
        toolbar.setNavigationIcon(R.mipmap.action_arrow_prev_white);
        toolbar.setNavigationOnClickListener(toolbarOnClickListener);
    }
    //endregion

    private View.OnClickListener toolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utils.clearPreviousActivity(FavoriteActivity.this);
        }
    };

    private TextView.OnEditorActionListener ediSearchOnEditActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                if (ediSearch.getText().length() > 0) {
                    selection = DBHelper.CONTACT_COLUMN_NAME + " LIKE ?";
                    selectionArgs = new String[]{"%" + ediSearch.getText().toString() + "%"};
                } else {
                    selection = null;
                    selectionArgs = null;
                }
                getSupportLoaderManager().restartLoader(0, null, FavoriteActivity.this);
                return true;
            }
            return false;
        }
    };

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                cursor.moveToPosition(position);
                Intent intent = new Intent(FavoriteActivity.this, FavoriteDetailsActivity.class);
                intent.putExtra("id", cursor.getInt(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_ID)));
                startActivity(intent);
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (lastVisibleItem >= totalItemCount - 1) {
                        pageNo++;
                        HttpHelper.getFavoriteGson(pageNo, FavoriteActivity.this, TAG, txtError);
                    } else if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }
            }
        }
    };


    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d("NewsActivity", "Refresh triggered at " + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            pageNo = 1;
            HttpHelper.getFavoriteGson(pageNo, FavoriteActivity.this, TAG, txtError);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }

    @Override
    public void onBackPressed() {
        Utils.clearPreviousActivity(FavoriteActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_favorite, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.action_add_people:
                Intent intent = new Intent(getBaseContext(), EmployeeActivity.class);
                intent.putExtra("isFavorite", true);
                startActivity(intent);
                finish();
                return true;
            default:
                return drawerFragment.mDrawerToggle.onOptionsItemSelected(item);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = ContactProvider.CONTENT_URI;
        return new CursorLoader(getBaseContext(), uri, null, selection, selectionArgs, DBHelper.CONTACT_COLUMN_ID + " DESC ");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            this.cursor = data;
            adapter.swapCursor(cursor);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
    }
}
