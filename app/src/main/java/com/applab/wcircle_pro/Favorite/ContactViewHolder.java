package com.applab.wcircle_pro.Favorite;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/2/2015.
 */
public class ContactViewHolder extends RecyclerView.ViewHolder {
    TextView name;
    TextView department;
    ImageView imgIcon,imgCountry;
    ImageView btnFavorite;
    RelativeLayout rlImage;

    public ContactViewHolder(View itemView) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.txtName);
        department = (TextView) itemView.findViewById(R.id.txtDepartment);
        imgIcon = (ImageView) itemView.findViewById(R.id.imgIcon);
        btnFavorite = (ImageView) itemView.findViewById(R.id.btnFavorite);
        imgCountry = (ImageView)itemView.findViewById(R.id.imgCountry);
        rlImage = (RelativeLayout)itemView.findViewById(R.id.rlImage);
    }
}
