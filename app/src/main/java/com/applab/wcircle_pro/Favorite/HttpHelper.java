package com.applab.wcircle_pro.Favorite;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Employee.BranchEmployeeProvider;
import com.applab.wcircle_pro.Employee.DepartmentEmployeeProvider;
import com.applab.wcircle_pro.Employee.Employee;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by user on 4/12/2015.
 */
public class HttpHelper {
    //region Favorite region
    public static void getFavoriteGson(int pageNo, Context context, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        Log.i(TAG, token);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        final Date lastUpdateDate = getFavoriteLastUpdate(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
        }
        final String date = lastUpdate;
        GsonRequest<FavoriteContact> mGsonRequest = new GsonRequest<FavoriteContact>(
                Request.Method.GET,
                Utils.API_URL + "api/Employee/FavouriteContact?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) +
                        "&PageNo=" + Utils.encode(pageNo) + "&LastUpdated=" + Utils.encode(date) +
                        "&ReturnEmpty=" + Utils.encode(getContactRow(context, pageNo) > 0) + "",
                FavoriteContact.class,
                headers,
                responseFavoriteListener(pageNo, context, TAG, txtError),
                errorFavoriteListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<FavoriteContact> responseFavoriteListener(final int pageNo, final Context context, final String TAG, final TextView txtError) {
        return new Response.Listener<FavoriteContact>() {
            @Override
            public void onResponse(FavoriteContact response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getContacts().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getFavoriteLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                    }
                                    insertFavoriteContact(response, context, pageNo, TAG);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                    }
                                    insertFavoriteContact(response, context, pageNo, TAG);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                }
                                insertFavoriteContact(response, context, pageNo, TAG);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    public static Date getFavoriteLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.FAVORITE_CONTACT_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(FavoriteContactProvider.CONTENT_URI, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.FAVORITE_CONTACT_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static int getContactRow(Context context, int pageNo) {
        int totalRow = 0;
        String[] projection = {DBHelper.CONTACT_COLUMN_ID};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ContactProvider.CONTENT_URI, projection,
                    DBHelper.CONTACT_COLUMN_PAGE_NO + "=?", new String[]{String.valueOf(pageNo)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Response.ErrorListener errorFavoriteListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }

    public static void insertFavoriteContact(FavoriteContact result, Context context, int pageNo, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesFavorite = new ContentValues();
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_NO_PER_PAGE, result.getNoPerPage());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_PAGE_NO, pageNo);
            context.getContentResolver().insert(FavoriteContactProvider.CONTENT_URI, contentValuesFavorite);
            ContentValues[] contentValueses = new ContentValues[result.getContacts().size()];
            for (int i = 0; i < result.getContacts().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.CONTACT_COLUMN_ALL_BRANCH, result.getContacts().get(i).getAllBranch());
                contentValues.put(DBHelper.CONTACT_COLUMN_BRANCH_ID, result.getContacts().get(i).getDefaultBranchId());
                contentValues.put(DBHelper.CONTACT_COLUMN_BRANCH_NAME, result.getContacts().get(i).getDefaultBranchName());
                contentValues.put(DBHelper.CONTACT_COLUMN_CONTACT_NO, result.getContacts().get(i).getContactNo());
                contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY, result.getContacts().get(i).getCountry());
                contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY_IMAGE, result.getContacts().get(i).getCountryImage());
                contentValues.put(DBHelper.CONTACT_COLUMN_DEPARTMENT, result.getContacts().get(i).getDepartment());
                contentValues.put(DBHelper.CONTACT_COLUMN_PAGE_NO, pageNo);
                contentValues.put(DBHelper.CONTACT_COLUMN_EMAIL, result.getContacts().get(i).getEmail());
                contentValues.put(DBHelper.CONTACT_COLUMN_FAVORITE_ID, result.getContacts().get(i).getFavouriteId());
                contentValues.put(DBHelper.CONTACT_COLUMN_GENDER, result.getContacts().get(i).getGender());
                contentValues.put(DBHelper.CONTACT_COLUMN_NAME, result.getContacts().get(i).getName());
                contentValues.put(DBHelper.CONTACT_COLUMN_LAST_UPDATED, result.getContacts().get(i).getLastUpdated());
                contentValues.put(DBHelper.CONTACT_COLUMN_IMAGE, result.getContacts().get(i).getProfileImage());
                contentValues.put(DBHelper.CONTACT_COLUMN_POSITION, result.getContacts().get(i).getPosition());
                contentValues.put(DBHelper.CONTACT_COLUMN_EMPLOYEE_NO, result.getContacts().get(i).getEmployeeNo());
                contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY_NAME, result.getContacts().get(i).getCountryName());
                contentValues.put(DBHelper.CONTACT_COLUMN_DOB, result.getContacts().get(i).getDOB());
                contentValues.put(DBHelper.CONTACT_COLUMN_OFFICE_NO, result.getContacts().get(i).getOfficeNo());
                contentValues.put(DBHelper.CONTACT_COLUMN_HOD_ID, result.getContacts().get(i).getHODId());
                contentValues.put(DBHelper.CONTACT_COLUMN_HOD_NAME, result.getContacts().get(i).getHODName());
                contentValues.put(DBHelper.CONTACT_COLUMN_OXUSER, result.getContacts().get(i).getOXUser());
                contentValues.put(DBHelper.CONTACT_COLUMN_ID, result.getContacts().get(i).getId());
                if (isFavoriteExists(context, result.getContacts().get(i).getId())) {
                    context.getContentResolver().delete(ContactProvider.CONTENT_URI, DBHelper.CONTACT_COLUMN_ID + "=?", new String[]{String.valueOf(result.getContacts().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(ContactProvider.CONTENT_URI, contentValueses);
            }

        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    public static boolean isFavoriteExists(Context context, Object id) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ContactProvider.CONTENT_URI, null, DBHelper.CONTACT_COLUMN_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    //endregion
    //region Single Favorite region
    public static void getSingleFavoriteGson(Context context, Contact contact, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Employee> mGsonRequest = new GsonRequest<Employee>(
                Request.Method.GET,
                Utils.API_URL + "api/Employee" + "/" + Utils.encode(contact.getId().toString()),
                Employee.class,
                headers,
                responseSingleFavoriteListener(context, txtError, TAG, contact),
                errorListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Employee> responseSingleFavoriteListener(final Context context, final TextView txtError, final String TAG, final Contact contact) {
        return new Response.Listener<Employee>() {
            @Override
            public void onResponse(Employee response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        insertContact(response, context, TAG, contact);
                    }
                }
            }
        };
    }

    public static void insertContact(Employee employee, Context context, String TAG, Contact contact) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.CONTACT_COLUMN_ALL_BRANCH, employee.getAllBranch());
            contentValues.put(DBHelper.CONTACT_COLUMN_GENDER, employee.getGender());
            contentValues.put(DBHelper.CONTACT_COLUMN_FAVORITE_ID, employee.getFavouriteId());
            contentValues.put(DBHelper.CONTACT_COLUMN_EMAIL, employee.getEmail());
            contentValues.put(DBHelper.CONTACT_COLUMN_POSITION, employee.getPosition());
            contentValues.put(DBHelper.CONTACT_COLUMN_BRANCH_ID, employee.getDefaultBranchId());
            contentValues.put(DBHelper.CONTACT_COLUMN_BRANCH_NAME, employee.getDefaultBranchName());
            contentValues.put(DBHelper.CONTACT_COLUMN_CONTACT_NO, employee.getContactNo());
            contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY, employee.getCountry());
            contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY_IMAGE, employee.getCountryImage());
            contentValues.put(DBHelper.CONTACT_COLUMN_DEPARTMENT, employee.getDepartment());
            contentValues.put(DBHelper.CONTACT_COLUMN_IMAGE, employee.getProfileImage());
            contentValues.put(DBHelper.CONTACT_COLUMN_NAME, employee.getName());
            contentValues.put(DBHelper.CONTACT_COLUMN_EMPLOYEE_NO, employee.getEmployeeNo());
            contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY_NAME, employee.getCountryName());
            contentValues.put(DBHelper.CONTACT_COLUMN_OXUSER, employee.getOXUser());
            contentValues.put(DBHelper.CONTACT_COLUMN_DOB, employee.getDOB());
            contentValues.put(DBHelper.CONTACT_COLUMN_OFFICE_NO, employee.getOfficeNo());
            contentValues.put(DBHelper.CONTACT_COLUMN_HOD_ID, employee.getHODId());
            contentValues.put(DBHelper.CONTACT_COLUMN_HOD_NAME, employee.getHODName());
            contentValues.put(DBHelper.CONTACT_COLUMN_LAST_UPDATED, employee.getLastUpdated());
            context.getContentResolver().update(ContactProvider.CONTENT_URI, contentValues, DBHelper.CONTACT_COLUMN_ID + "=?",
                    new String[]{String.valueOf(contact.getId())});
        } catch (Exception ex) {
            Log.i(TAG, "Insert Contact Error in Favorite Details : " + ex.getMessage());
        }

    }

    public static Response.ErrorListener errorListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

    //endregion
    //region Delete Favorite
    public static void deleteFavorite(final String employeeId, Context context, Contact contact, String TAG, boolean isBack) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.DELETE,
                Utils.API_URL + "api/Employee/FavouriteContact/" + Utils.encode(employeeId),
                JsonObject.class,
                headers,
                responseListenerDeleteFavorite(employeeId, context, isBack),
                errorListenerDeleteFavorite(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.ErrorListener errorListenerDeleteFavorite(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }

    public static Response.Listener<JsonObject> responseListenerDeleteFavorite(final String id, final Context context, final boolean isBack) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                context.getContentResolver().delete(ContactProvider.CONTENT_URI, DBHelper.CONTACT_COLUMN_ID + "=?", new String[]{id});
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IS_SELECTED, String.valueOf(false));
                context.getContentResolver().update(BranchEmployeeProvider.CONTENT_URI, contentValues, DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{id});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IS_SELECTED, String.valueOf(false));
                context.getContentResolver().update(DepartmentEmployeeProvider.CONTENT_URI, contentValues, DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{id});
                if (isBack) {
                    ((Activity) context).finish();
                }
            }
        };
    }

    public static void postFavorite(final String employeeId, final String contactId, final String type, Context context, final String TAG) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Employee/FavouriteContact",
                JsonObject.class,
                headers,
                responseListenerFavorite(contactId, context),
                errorListenerFavorite(context)) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "EmployeeId=" + employeeId + "&ContactId=" + contactId + "&Type=" + type;
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JsonObject> responseListenerFavorite(final String id, final Context context) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IS_SELECTED, String.valueOf(true));
                context.getContentResolver().update(BranchEmployeeProvider.CONTENT_URI, contentValues, DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{id});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IS_SELECTED, String.valueOf(true));
                context.getContentResolver().update(DepartmentEmployeeProvider.CONTENT_URI, contentValues, DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{id});
            }
        };
    }

    public static Response.ErrorListener errorListenerFavorite(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }
    //endregion
}
