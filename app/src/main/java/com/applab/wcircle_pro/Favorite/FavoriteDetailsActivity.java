package com.applab.wcircle_pro.Favorite;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.ChatRoomActivity;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.ChatListProvider;
import com.applab.wcircle_pro.Chat.db.UserChatProvider;
import com.applab.wcircle_pro.Chat.xmpp.Config;
import com.applab.wcircle_pro.Employee.EmployeeImageSlidingActivity;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

public class FavoriteDetailsActivity extends AppCompatActivity implements SwipyRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {
    private ImageView imgAvatar, imgCountry;
    private Toolbar toolbar;
    private TextView txtName, txtDep, txtPosition, txtEmail, txtMobile, txtOffice, txtDOB, txtHOD, txtEmployeeNo, txtError;
    private LinearLayout btnEmail, btnCall, btnMessage;
    private ImageView btnFavorite;
    private String TAG = "EMPLOYEE";
    private ScrollView scrollView;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private Contact contact = new Contact();
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_details);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_red));
        TextView txtToolbarTitle = (TextView) findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_my_favorite));
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        txtError = (TextView) findViewById(R.id.txtError);
        bundle = getIntent().getExtras();
        contact.setId(bundle.getInt("id"));
        btnEmail = (LinearLayout) findViewById(R.id.btnEmail);
        btnMessage = (LinearLayout) findViewById(R.id.btnMessage);
        btnCall = (LinearLayout) findViewById(R.id.btnCall);
        imgAvatar = (ImageView) findViewById(R.id.imgAvatar);
        imgAvatar.setOnClickListener(imgAvatarOnClickListener);
        imgCountry = (ImageView) findViewById(R.id.imgCountry);
        btnEmail.setOnClickListener(btnEmailOnClickListener);
        btnMessage.setOnClickListener(btnMessageOnClickListener);
        btnCall.setOnClickListener(btnCallOnClickListener);
        txtName = (TextView) findViewById(R.id.txtName);
        txtDep = (TextView) findViewById(R.id.txtDep);
        txtPosition = (TextView) findViewById(R.id.txtPosition);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtMobile = (TextView) findViewById(R.id.txtMobile);
        txtOffice = (TextView) findViewById(R.id.txtOffice);
        txtDOB = (TextView) findViewById(R.id.txtDOB);
        txtHOD = (TextView) findViewById(R.id.txtHOD);
        txtEmployeeNo = (TextView) findViewById(R.id.txtEmployeeNo);
        btnFavorite = (ImageView) findViewById(R.id.btnFavorite);
        btnFavorite.setOnClickListener(btnFavoriteOnClickListener);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollViewScrollChangedListener();
    }

    private void scrollViewScrollChangedListener() {
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY(); //for vertical ScrollView
                if (scrollY == 0) {
                    if (!mSwipyRefreshLayout.isRefreshing()) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }
            }
        });
    }

    private LinearLayout.OnClickListener imgAvatarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getBaseContext(), EmployeeImageSlidingActivity.class);
            intent.putExtra("id", String.valueOf(contact.getId()));
            intent.putExtra("position", 0);
            String url = contact.getProfileImage();
            if (url == null) {
                url = "";
            }
            intent.putExtra("urlImage", url);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getBaseContext().startActivity(intent);
        }
    };

    private LinearLayout.OnClickListener btnEmailOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent email = new Intent(Intent.ACTION_SEND);
            Utils.postRecentTrack(FavoriteDetailsActivity.this, "email", contact.getId().toString());
            email.putExtra(Intent.EXTRA_EMAIL, new String[]{contact.getEmail()});
            email.putExtra(Intent.EXTRA_SUBJECT, "");
            email.putExtra(Intent.EXTRA_TEXT, "");
            email.setType("message/rfc822");
            startActivity(Intent.createChooser(email, Utils.CHOOSE_CLIENT));
        }
    };

    private LinearLayout.OnClickListener btnMessageOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Utils.postRecentTrack(FavoriteDetailsActivity.this, "msg", contact.getId().toString());
            SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREFERENCES_NAME, 0);
            String selfId = sharedPreferences.getString("username", "");
            Intent intent = new Intent(FavoriteDetailsActivity.this, ChatRoomActivity.class);
            try {
                if (!isUserExist(contact.getId())) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID, contact.getId());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_USER_ID, getLastUserId() + 1);
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_IMAGE, contact.getProfileImage());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_NAME, contact.getName());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_OX_USER, contact.getOXUser());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_CONTACT_NO, contact.getContactNo());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_OFFICE_NO, contact.getOfficeNo());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_POSITION, contact.getPosition());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE, contact.getCountryImage());
                    getContentResolver().insert(UserChatProvider.CONTENT_URI, contentValues);
                }
            } catch (Exception ex) {
                ex.fillInStackTrace();
            } finally {
                intent.putExtra("user", ChatDBHelper.getSingleUser(FavoriteDetailsActivity.this, contact.getOXUser()));
            }
            intent.putExtra("friendId", contact.getOXUser());
            intent.putExtra("employeeName", contact.getName());
            intent.putExtra("selfId", selfId);

            startActivity(intent);
            finish();
        }
    };

    private int getLastUserId() {
        int id = 0;
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(ChatListProvider.CONTENT_URI, null, null, null, null);
            if (cursor != null && cursor.moveToLast()) {
                id = cursor.getInt(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_USER_ID));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

    private boolean isUserExist(long id) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(UserChatProvider.CONTENT_URI, null,
                    DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    private LinearLayout.OnClickListener btnCallOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            FavoriteDialogFragment favoriteDialogFragment = FavoriteDialogFragment.newInstance(contact, FavoriteDetailsActivity.this);
            favoriteDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    private LinearLayout.OnClickListener btnFavoriteOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (Utils.isConnectingToInternet(FavoriteDetailsActivity.this)) {
                if (btnFavorite.getTag().equals("true")) {
                    btnFavorite.setTag("false");
                    btnFavorite.setImageResource(R.mipmap.info_star_inactive);
                    HttpHelper.deleteFavorite(contact.getId().toString(), FavoriteDetailsActivity.this, contact, TAG, true);
                } else {
                    btnFavorite.setTag("true");
                    btnFavorite.setImageResource(R.mipmap.info_star_active);
                    HttpHelper.postFavorite(Utils.getProfile(FavoriteDetailsActivity.this).getId().toString(), contact.getId().toString(), "C", FavoriteDetailsActivity.this, TAG);
                }
            } else {
                Utils.showNoConnection(FavoriteDetailsActivity.this);
            }
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_favorite_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            startActivity(new Intent(this, FavoriteActivity.class));
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        getSupportLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d("MainActivity", "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            HttpHelper.getSingleFavoriteGson(FavoriteDetailsActivity.this, contact, TAG, txtError);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getBaseContext(), ContactProvider.CONTENT_URI, null,
                DBHelper.CONTACT_COLUMN_ID + "=?", new String[]{String.valueOf(contact.getId())}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            if (data.getCount() > 0) {
                contact = Contact.getContact(data, 0);
                txtName.setTag(contact.getId());
                txtName.setText(contact.getName());
                txtDep.setText(contact.getDepartment());
                txtPosition.setText(contact.getPosition());
                txtEmail.setText(contact.getEmail());
                txtMobile.setText(contact.getContactNo());
                txtDOB.setText(contact.getDOB());
                txtHOD.setText(contact.getHODName());
                txtEmployeeNo.setText(contact.getEmployeeNo());
                txtOffice.setText(contact.getOfficeNo());
                if (contact.getFavouriteId() > 0) {
                    btnFavorite.setImageResource(R.mipmap.info_star_active);
                    btnFavorite.setTag("true");
                } else {
                    btnFavorite.setImageResource(R.mipmap.info_star_inactive);
                    btnFavorite.setTag("false");
                }

                Glide.with(FavoriteDetailsActivity.this)
                        .load(contact.getProfileImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(new CircleTransform(FavoriteDetailsActivity.this))
                        .placeholder(R.mipmap.ic_action_person_dark)
                        .into(imgAvatar);
                Glide.with(FavoriteDetailsActivity.this)
                        .load(contact.getCountryImage())
                        .transform(new CircleTransform(FavoriteDetailsActivity.this))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgCountry);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
