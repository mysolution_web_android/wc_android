package com.applab.wcircle_pro.Favorite;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contact implements Parcelable{

    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("FavouriteId")
    @Expose
    private Integer FavouriteId;
    @SerializedName("ProfileImage")
    @Expose
    private String ProfileImage;
    @SerializedName("DefaultBranchId")
    @Expose
    private Integer DefaultBranchId;
    @SerializedName("DefaultBranchName")
    @Expose
    private String DefaultBranchName;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Country")
    @Expose
    private String Country;
    @SerializedName("CountryImage")
    @Expose
    private String CountryImage;
    @SerializedName("Gender")
    @Expose
    private String Gender;
    @SerializedName("Department")
    @Expose
    private String Department;
    @SerializedName("Position")
    @Expose
    private String Position;
    @SerializedName("ContactNo")
    @Expose
    private String ContactNo;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("AllBranch")
    @Expose
    private String AllBranch;
    @SerializedName("LastUpdated")
    @Expose
    private String LastUpdated;
    @Expose
    private String CountryName;
    @Expose
    private String EmployeeNo;
    @Expose
    private String DOB;
    @Expose
    private String OfficeNo;
    @Expose
    private String HODName;
    @Expose
    private String HODId;
    @Expose
    private String OXUser;

    public String getOXUser() {
        return OXUser;
    }

    public void setOXUser(String OXUser) {
        this.OXUser = OXUser;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getEmployeeNo() {
        return EmployeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        EmployeeNo = employeeNo;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getOfficeNo() {
        return OfficeNo;
    }

    public void setOfficeNo(String officeNo) {
        OfficeNo = officeNo;
    }

    public String getHODName() {
        return HODName;
    }

    public void setHODName(String HODName) {
        this.HODName = HODName;
    }

    public String getHODId() {
        return HODId;
    }

    public void setHODId(String HODId) {
        this.HODId = HODId;
    }

    private boolean isSelect = false;

    public boolean getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(boolean isSelect) {
        this.isSelect = isSelect;
    }

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The FavouriteId
     */
    public Integer getFavouriteId() {
        return FavouriteId;
    }

    /**
     * @param FavouriteId The FavouriteId
     */
    public void setFavouriteId(Integer FavouriteId) {
        this.FavouriteId = FavouriteId;
    }

    /**
     * @return The ProfileImage
     */
    public String getProfileImage() {
        return ProfileImage;
    }

    /**
     * @param ProfileImage The ProfileImage
     */
    public void setProfileImage(String ProfileImage) {
        this.ProfileImage = ProfileImage;
    }

    /**
     * @return The DefaultBranchId
     */
    public Integer getDefaultBranchId() {
        return DefaultBranchId;
    }

    /**
     * @param DefaultBranchId The DefaultBranchId
     */
    public void setDefaultBranchId(Integer DefaultBranchId) {
        this.DefaultBranchId = DefaultBranchId;
    }

    /**
     * @return The DefaultBranchName
     */
    public String getDefaultBranchName() {
        return DefaultBranchName;
    }

    /**
     * @param DefaultBranchName The DefaultBranchName
     */
    public void setDefaultBranchName(String DefaultBranchName) {
        this.DefaultBranchName = DefaultBranchName;
    }

    /**
     * @return The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return The Country
     */
    public String getCountry() {
        return Country;
    }

    /**
     * @param Country The Country
     */
    public void setCountry(String Country) {
        this.Country = Country;
    }

    /**
     * @return The CountryImage
     */
    public String getCountryImage() {
        return CountryImage;
    }

    /**
     * @param CountryImage The CountryImage
     */
    public void setCountryImage(String CountryImage) {
        this.CountryImage = CountryImage;
    }

    /**
     * @return The Gender
     */
    public String getGender() {
        return Gender;
    }

    /**
     * @param Gender The Gender
     */
    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    /**
     * @return The Department
     */
    public String getDepartment() {
        return Department;
    }

    /**
     * @param Department The Department
     */
    public void setDepartment(String Department) {
        this.Department = Department;
    }

    /**
     * @return The Position
     */
    public String getPosition() {
        return Position;
    }

    /**
     * @param Position The Position
     */
    public void setPosition(String Position) {
        this.Position = Position;
    }

    /**
     * @return The ContactNo
     */
    public String getContactNo() {
        return ContactNo;
    }

    /**
     * @param ContactNo The ContactNo
     */
    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    /**
     * @return The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     * @param Email The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     * @return The AllBranch
     */
    public String getAllBranch() {
        return AllBranch;
    }

    /**
     * @param AllBranch The AllBranch
     */
    public void setAllBranch(String AllBranch) {
        this.AllBranch = AllBranch;
    }

    /**
     * @return The LastUpdated
     */
    public String getLastUpdated() {
        return LastUpdated;
    }

    /**
     * @param LastUpdated The LastUpdated
     */
    public void setLastUpdated(String LastUpdated) {
        this.LastUpdated = LastUpdated;
    }

    public Contact() {
    }

    public Contact(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeInt(this.FavouriteId);
        dest.writeString(this.ProfileImage);
        dest.writeInt(this.DefaultBranchId);
        dest.writeString(this.DefaultBranchName);
        dest.writeString(this.Name);
        dest.writeString(this.Country);
        dest.writeString(this.CountryImage);
        dest.writeString(this.Gender);
        dest.writeString(this.Department);
        dest.writeString(this.Position);
        dest.writeString(this.ContactNo);
        dest.writeString(this.Email);
        dest.writeString(this.AllBranch);
        dest.writeString(this.LastUpdated);
        dest.writeString(this.CountryName);
        dest.writeString(this.EmployeeNo);
        dest.writeString(this.DOB);
        dest.writeString(this.OfficeNo);
        dest.writeString(this.HODName);
        dest.writeString(this.HODId);
        dest.writeString(this.OXUser);
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.Id = in.readInt();
        this.FavouriteId = in.readInt();
        this.ProfileImage = in.readString();
        this.DefaultBranchId = in.readInt();
        this.DefaultBranchName = in.readString();
        this.Name = in.readString();
        this.Country = in.readString();
        this.CountryImage = in.readString();
        this.Gender = in.readString();
        this.Department = in.readString();
        this.Position = in.readString();
        this.ContactNo = in.readString();
        this.Email = in.readString();
        this.AllBranch = in.readString();
        this.LastUpdated = in.readString();
        this.CountryName = in.readString();
        this.EmployeeNo = in.readString();
        this.DOB = in.readString();
        this.OfficeNo = in.readString();
        this.HODName = in.readString();
        this.HODId = in.readString();
        this.OXUser = in.readString();
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public static Contact getContact(Cursor cursor, int position) {
        if (!cursor.isClosed()) {
            cursor.moveToPosition(position);
            Contact contact = new Contact();
            contact.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_ID)));
            contact.setAllBranch(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_ALL_BRANCH)));
            contact.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_CONTACT_NO)));
            contact.setCountry(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_COUNTRY)));
            contact.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_COUNTRY_IMAGE)));
            contact.setDefaultBranchId(cursor.getInt(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_BRANCH_ID)));
            contact.setDefaultBranchName(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_BRANCH_NAME)));
            contact.setDepartment(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_DEPARTMENT)));
            contact.setEmail(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_EMAIL)));
            contact.setFavouriteId(cursor.getInt(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_FAVORITE_ID)));
            contact.setGender(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_GENDER)));
            contact.setLastUpdated(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_LAST_UPDATED)));
            contact.setName(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_NAME)));
            contact.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_POSITION)));
            contact.setProfileImage(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_IMAGE)));
            contact.setCountryName(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_COUNTRY_NAME)));
            contact.setDOB(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_DOB)));
            contact.setEmployeeNo(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_EMPLOYEE_NO)));
            contact.setHODId(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_HOD_ID)));
            contact.setHODName(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_HOD_NAME)));
            contact.setOfficeNo(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_OFFICE_NO)));
            contact.setOXUser(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_OXUSER)));
            return contact;
        } else {
            return null;
        }
    }



}