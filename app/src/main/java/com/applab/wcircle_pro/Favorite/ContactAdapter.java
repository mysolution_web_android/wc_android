package com.applab.wcircle_pro.Favorite;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.applab.wcircle_pro.Employee.EmployeeImageSlidingActivity;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by user on 5/2/2015.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;
    private String TAG = "CONTACT";

    public ContactAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.cursor = cursor;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("ContactViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_favorite_row, parent, false);
        ContactViewHolder holder = new ContactViewHolder(view);
        holder.setIsRecyclable(false);
        return holder;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Contact current = getData(cursor, position);
        holder.name.setText(current.getName());
        holder.btnFavorite.setImageResource(current.getIsSelect() ? R.mipmap.info_star_active : R.mipmap.info_star_inactive);
        holder.btnFavorite.setTag(current);
        holder.department.setText(current.getDepartment());
        Glide.with(context)
                .load(current.getProfileImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleTransform(context))
                .placeholder(R.mipmap.ic_action_person_light)
                .into(holder.imgIcon);
        Glide.with(context)
                .load(current.getCountryImage())
                .transform(new CircleTransform(context))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgCountry);
        setBtnFavoriteOnClickListener(holder.btnFavorite);
        setRlImageOnClickListener(cursor, position, holder.rlImage);
    }

    private void setBtnFavoriteOnClickListener(final ImageView btnFavorite) {
        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Contact contact = (Contact) btnFavorite.getTag();
                if (contact.getIsSelect()) {
                    if (Utils.isConnectingToInternet(context)) {
                        HttpHelper.deleteFavorite(String.valueOf(contact.getId()), context, contact, TAG, false);
                        btnFavorite.setImageResource(R.mipmap.info_star_inactive);
                        contact.setIsSelect(false);
                    } else {
                        Utils.showNoConnection(context);
                        btnFavorite.setImageResource(R.mipmap.info_star_active);
                    }
                }
            }
        });
    }

    private void setRlImageOnClickListener(final Cursor cursor, final int position, final RelativeLayout rlImage) {
        rlImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Contact contact = getData(cursor, position);
                Intent intent = new Intent(context, EmployeeImageSlidingActivity.class);
                intent.putExtra("id", String.valueOf(contact.getId()));
                intent.putExtra("position", 0);
                String url = contact.getProfileImage();
                if (url == null) {
                    url = contact.getProfileImage();
                }
                intent.putExtra("urlImage", url);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }


    public Contact getData(Cursor cursor, int position) {
        Contact current = new Contact();
        cursor.moveToPosition(position);
        current.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_ID)));
        current.setName(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_NAME)));
        current.setDepartment(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_DEPARTMENT)));
        current.setAllBranch(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_ALL_BRANCH)));
        current.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_POSITION)));
        current.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_CONTACT_NO)));
        current.setEmail(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_EMAIL)));
        current.setProfileImage(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_IMAGE)));
        current.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_COUNTRY_IMAGE)));
        if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_FAVORITE_ID))) {
            if (cursor.getInt(cursor.getColumnIndex(DBHelper.CONTACT_COLUMN_FAVORITE_ID)) > 0) {
                current.setIsSelect(true);
            }
        }
        return current;
    }

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            ContactAdapter.this.notifyDataSetChanged();
        }
    };
}
