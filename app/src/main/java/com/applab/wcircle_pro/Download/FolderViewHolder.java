package com.applab.wcircle_pro.Download;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/8/2015.
 */
public class FolderViewHolder extends RecyclerView.ViewHolder {
    TextView title;
    TextView capacity;
    TextView date;
    ImageView icon;
    ImageView img,imgStatus;
    LinearLayout lv;
    LinearLayout lvText;

    public FolderViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.txtTitle);
        capacity = (TextView) itemView.findViewById(R.id.txtCapacity);
        date = (TextView) itemView.findViewById(R.id.txtDate);
        img = (ImageView) itemView.findViewById(R.id.img);
        icon = (ImageView) itemView.findViewById(R.id.imgIcon);
        imgStatus = (ImageView) itemView.findViewById(R.id.imgStatus);
        lv = (LinearLayout) itemView.findViewById(R.id.lv);
        lvText = (LinearLayout) itemView.findViewById(R.id.lvText);
    }

}