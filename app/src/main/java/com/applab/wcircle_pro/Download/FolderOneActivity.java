package com.applab.wcircle_pro.Download;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Home.MasterSettingProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.io.File;

public class FolderOneActivity extends AppCompatActivity implements SwipyRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private FolderAdapter adapter;
    private RelativeLayout fadeRL;
    private DialogFragment notificationDialogFragment;
    private Bundle bundle;
    private String title;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private String TAG = "DOWNLOAD";
    private Cursor cursor;
    private LinearLayoutManager linearLayoutManager;
    private int pageNo = 1;
    private int level = 1;
    private TextView txtError;
    private String downloadPath;
    private File direct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_blue_light));
        bundle = getIntent().getExtras();
        if (bundle != null) title = bundle.getString("Title");
        if (bundle != null) level = bundle.getInt("level");
        if (bundle != null) downloadPath = bundle.getString("downloadPath");
        TextView txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(title != null ? title : "");
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new FolderAdapter(this, cursor, getSupportFragmentManager());
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        setTouchListener();
        txtError = (TextView) findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        txtError.setTag(false);
        getSupportLoaderManager().initLoader(0, null, this);
        direct = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.folder_name) + "/" + downloadPath);
        HttpHelper.getDownloadGson(this, pageNo, level, TAG, txtError, direct);
    }

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
                final Download current = (Download) txtTitle.getTag();
                if (Utils.getStatus(Utils.DOWNLOAD_READ, FolderOneActivity.this)) {
                    HttpHelper.getDownloadGsonReading(current.getId(), FolderOneActivity.this, TAG);
                }
                if (current.getType().equals("file")) {
                    if (current.getPath() != null) {
                        if (Utils.localFileExists(current.getPath())) {
                            Utils.openDocument(current.getPath(), FolderOneActivity.this);
                        } else {
                            String message = Utils.DOWNLOAD_ALERT + getBaseContext().getString(R.string.folder_name) + "/" + downloadPath;
                            DownloadDialogFragment downloadDialogFragment = DownloadDialogFragment.newInstance(message, FolderOneActivity.this, current.getFilePath(), current.getFilePath(), downloadPath, current.getId());
                            downloadDialogFragment.show(getSupportFragmentManager(), "");
                        }
                    }else{
                        String message = Utils.DOWNLOAD_ALERT + getBaseContext().getString(R.string.folder_name) + "/" + downloadPath;
                        DownloadDialogFragment downloadDialogFragment = DownloadDialogFragment.newInstance(message, FolderOneActivity.this, current.getFilePath(), current.getFilePath(), downloadPath, current.getId());
                        downloadDialogFragment.show(getSupportFragmentManager(), "");
                    }
                } else {
                    Intent intent = new Intent(getBaseContext(), FolderTwoActivity.class);
                    intent.putExtra("Title", current.getTitle());
                    intent.putExtra("level", current.getId());
                    intent.putExtra("downloadPath", downloadPath + "/" + current.getTitle());
                    startActivity(intent);
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (lastVisibleItem >= totalItemCount - 1) {
                        pageNo++;
                        HttpHelper.getDownloadGson(FolderOneActivity.this, pageNo, level, TAG, txtError,direct);
                    } else if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }
            }
        }
    };

    private boolean getStatus(String args) {
        boolean status = false;
        Uri uri = MasterSettingProvider.CONTENT_URI;
        String[] projection = {DBHelper.MASTER_SETTING_COLUMN_STATUS};
        String selection = DBHelper.MASTER_SETTING_COLUMN_CODE + "=?";
        String[] selectionArgs = {args};
        String sorting = DBHelper.MASTER_SETTING_COLUMN_ID + " DESC LIMIT 1 ";
        Cursor cursor = getContentResolver().query(uri, projection, selection, selectionArgs, sorting);
        if (cursor.moveToFirst()) {
            do {
                if (cursor.getString(cursor.getColumnIndex(DBHelper.MASTER_SETTING_COLUMN_STATUS)).equals("true")) {
                    status = true;
                }
            }
            while (cursor.moveToNext());
            cursor.close();
        }
        Log.i(TAG, "Status: " + String.valueOf(status));
        return status;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getBaseContext(),
                DownloadProvider.CONTENT_URI, null, DBHelper.DOWNLOAD_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)}, " LENGTH(" + DBHelper.DOWNLOAD_COLUMN_TYPE + ") DESC ");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.cursor = data;
        if (!(Boolean) txtError.getTag()) {
            adapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at " + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            pageNo = 1;
            HttpHelper.getDownloadGson(FolderOneActivity.this, pageNo, level, TAG, txtError,direct);
        } else {
            pageNo++;
            HttpHelper.getDownloadGson(FolderOneActivity.this, pageNo, level, TAG, txtError,direct);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_folder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(DownloadActivity.ACTION)) {
                if (!intent.getBooleanExtra("isConnect", false)) {
                    adapter.swapCursor(null);
                } else {
                    adapter.swapCursor(cursor);
                }
            }else if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                getSupportLoaderManager().restartLoader(0, null, FolderOneActivity.this);
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(DownloadActivity.ACTION);
        iff.addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, iff);
    }
}
