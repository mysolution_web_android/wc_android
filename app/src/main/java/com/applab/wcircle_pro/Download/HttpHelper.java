package com.applab.wcircle_pro.Download;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Dashboard.DashboardProvider;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by user on 26/11/2015.
 */

public class HttpHelper {
    //region Download
    public static void getDownloadGson(final Context context, final int pageNo, final int level, final String TAG, TextView txtError, final File direct) {
        final String token = Utils.getToken(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        final Date lastUpdateDate = getDownloadLastUpdate(context);
        String url = Utils.API_URL + "api/DownloadCentre" + "/" + Utils.encode(level)
                + "?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                + Utils.encode(pageNo) + "&LastUpdated=" + Utils.encode(lastUpdate)
                + "&ReturnEmpty=" + Utils.encode(getDownloadRow(context, level) > 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/DownloadCentre" + "/" + Utils.encode(level)
                    + "?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                    + Utils.encode(pageNo) + "&LastUpdated=" + Utils.encode(lastUpdate)
                    + "&ReturnEmpty=" + Utils.encode(getDownloadRow(context, level) > 0);
        }
        final String finalUrl = url;
        Log.i(TAG, url);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<DownloadChecking> mGsonRequest = new GsonRequest<DownloadChecking>(
                Request.Method.GET,
                finalUrl,
                DownloadChecking.class,
                headers,
                responseDownloadListener(context, txtError, pageNo, level, TAG, direct),
                errorDownloadListener(context, txtError)) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "employeeId=" + Utils.getProfile(context).getId() + "&NoPerPage=" + Utils.PAGING_NUMBER + "&PageNo=" + String.valueOf(pageNo);
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<DownloadChecking> responseDownloadListener(final Context context, final TextView txtError, final int pageNo, final int level, final String TAG, final File direct) {
        return new Response.Listener<DownloadChecking>() {
            @Override
            public void onResponse(DownloadChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                    txtError.setTag(false);
                }
                Intent intent = new Intent(DownloadActivity.ACTION);
                intent.putExtra("isConnect", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getDownloads().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getDownloadLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(DownloadProvider.CONTENT_URI, DBHelper.DOWNLOAD_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)});
                                        context.getContentResolver().delete(DownloadCheckingProvider.CONTENT_URI, null, null);
                                        insertDownload(response, context, pageNo, level, TAG, direct);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(DownloadCheckingProvider.CONTENT_URI, null, null);
                                        insertDownload(response, context, pageNo, level, TAG, direct);
                                    }

                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(DownloadProvider.CONTENT_URI, DBHelper.DOWNLOAD_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)});
                                        context.getContentResolver().delete(DownloadCheckingProvider.CONTENT_URI, null, null);
                                        insertDownload(response, context, pageNo, level, TAG, direct);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(DownloadCheckingProvider.CONTENT_URI, null, null);
                                        insertDownload(response, context, pageNo, level, TAG, direct);
                                    }

                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(DownloadProvider.CONTENT_URI, DBHelper.DOWNLOAD_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)});
                                    context.getContentResolver().delete(DownloadCheckingProvider.CONTENT_URI, null, null);
                                    insertDownload(response, context, pageNo, level, TAG, direct);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(DownloadCheckingProvider.CONTENT_URI, null, null);
                                    insertDownload(response, context, pageNo, level, TAG, direct);
                                }
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(DownloadProvider.CONTENT_URI, DBHelper.DOWNLOAD_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)});
                                    context.getContentResolver().delete(DownloadCheckingProvider.CONTENT_URI, null, null);
                                }
                            }
                        }

                    }
                }
            }
        };
    }

    public static int getDownloadRow(Context context, int level) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DownloadProvider.CONTENT_URI, null,
                    DBHelper.DOWNLOAD_COLUMN_LEVEL + "=?", new String[]{String.valueOf(level)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return totalRow;
    }

    public static Response.ErrorListener errorDownloadListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                    txtError.setTag(true);
                } else {
                    Utils.serverHandlingError(context, error);
                }
                Intent intent = new Intent(DownloadActivity.ACTION);
                intent.putExtra("isConnect", false);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    public static Date getDownloadLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.DOWNLOAD_CHECKING_COLUMN_LAST_UPDATE;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DownloadCheckingProvider.CONTENT_URI, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.DOWNLOAD_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
                cursor.close();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static boolean isDownloadExists(Context context, Object id) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DownloadProvider.CONTENT_URI, null, DBHelper.DOWNLOAD_COLUMN_DOWNLOAD_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void insertDownload(DownloadChecking result, Context context, int pageNo, int level, String TAG, final File direct) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesDownloadChecking = new ContentValues();
            contentValuesDownloadChecking.put(DBHelper.DOWNLOAD_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesDownloadChecking.put(DBHelper.DOWNLOAD_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesDownloadChecking.put(DBHelper.DOWNLOAD_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesDownloadChecking.put(DBHelper.DOWNLOAD_CHECKING_COLUMN_PAGE_NO, pageNo);
            contentValuesDownloadChecking.put(DBHelper.DOWNLOAD_CHECKING_COLUMN_LEVEL, level);
            context.getContentResolver().insert(DownloadCheckingProvider.CONTENT_URI, contentValuesDownloadChecking);
            ContentValues[] contentValueses = new ContentValues[result.getDownloads().size()];
            for (int i = 0; i < result.getDownloads().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_DOWNLOAD_ID, result.getDownloads().get(i).getId());
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_TITLE, result.getDownloads().get(i).getTitle());
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_DESCRIPTION, result.getDownloads().get(i).getDescription());
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_FILE_PATH, result.getDownloads().get(i).getFilePath());
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_TYPE, result.getDownloads().get(i).getType());
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_FILE_SIZE, result.getDownloads().get(i).getFileSize());
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_LEVEL, level);
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_FILE_SIZE_BYTES, result.getDownloads().get(i).getFileSizeBytes());
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_PAGE_NO, pageNo);
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_CREATED_BY, result.getDownloads().get(i).getCreatedBy());
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_CREATED_DATE, result.getDownloads().get(i).getCreatedDate());
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_NEW_CREATE, String.valueOf(result.getDownloads().get(i).getNewCreate()));
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_PATH, direct.getAbsolutePath() + "/" + result.getDownloads().get(i).getTitle());
                if (isDownloadExists(context, result.getDownloads().get(i).getId())) {
                    context.getContentResolver().delete(DownloadProvider.CONTENT_URI, DBHelper.DOWNLOAD_COLUMN_ID + "=?",
                            new String[]{String.valueOf(result.getDownloads().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(DownloadProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Download Error :" + ex.toString());
        }
    }
    //endregion

    //region Download Download
    public static void getDownloadGsonDownload(final int id, final Context context, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseListenerDownload(TAG),
                errorListenerDownload(TAG)) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" + Utils.getProfile(context).getId() + ",\"section\":\"download\",\"action\":\"download\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerDownload(final String TAG) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Success send reading status");
            }
        };
    }

    public static Response.ErrorListener errorListenerDownload(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }

    //region Download Reading
    public static void getDownloadGsonReading(final int id, final Context context, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseListenerReading(context, TAG, id),
                errorListenerReading(TAG)) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" + Utils.getProfile(context).getId() + ",\"section\":\"download\",\"action\":\"read\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerReading(final Context context, final String TAG, final int id) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ContentValues contentValues = new ContentValues();
                if (isBold(context, id)) {
                    contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, DashboardActivity.getNumber(context, "Download Centre"));
                    context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                            contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Download Centre"});
                    com.applab.wcircle_pro.Dashboard.HttpHelper.getIndicator(context, TAG, null);
                }
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DOWNLOAD_COLUMN_NEW_CREATE, String.valueOf(false));
                context.getContentResolver().update(DownloadProvider.CONTENT_URI, contentValues,
                        DBHelper.DOWNLOAD_COLUMN_DOWNLOAD_ID + "=?", new String[]{String.valueOf(id)});

                Log.i(TAG, "Success send reading status");
            }
        };
    }

    public static boolean isBold(Context context, Object id) {
        boolean isBold = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DownloadProvider.CONTENT_URI, null, DBHelper.DOWNLOAD_COLUMN_DOWNLOAD_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                if (!cursor.isNull(cursor.getColumnIndex(DBHelper.DOWNLOAD_COLUMN_NEW_CREATE))) {
                    isBold = Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.DOWNLOAD_COLUMN_NEW_CREATE)));
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isBold;
    }

    public static Response.ErrorListener errorListenerReading(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }
    //endregion
}
