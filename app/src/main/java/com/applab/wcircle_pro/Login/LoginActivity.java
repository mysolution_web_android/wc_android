package com.applab.wcircle_pro.Login;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Chat.db.HttpHelper;
import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.Chat.xmpp.XMPPConn;
import com.applab.wcircle_pro.Home.App;
import com.applab.wcircle_pro.Home.HomeActivity;
import com.applab.wcircle_pro.ErrorDialogFragment;
import com.applab.wcircle_pro.Password.PasswordActivity;
import com.applab.wcircle_pro.Profile.Profile;
import com.applab.wcircle_pro.Profile.ProfileProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Register.Register;
import com.applab.wcircle_pro.Register.RegisterActivity;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.urbanairship.UAirship;

import org.jivesoftware.smack.AbstractXMPPConnection;

import java.util.Date;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    //region variable region
    private LinearLayout btnLogin, btnSignUp;
    private TextView btnForgetPassword;
    private EditText ediEmail, ediPassword;
    private String TAG = "AUTH";
    private String json = null;
    private PopupWindow popupWindow = null;
    private Profile profile;
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    private ProgressBar progressBar;
    private RelativeLayout fadeProgressBar;
    //endregion

    //region create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin = (LinearLayout) findViewById(R.id.btnLogin);
        btnForgetPassword = (TextView) findViewById(R.id.btnForgetPassword);
        btnSignUp = (LinearLayout) findViewById(R.id.btnSignUp);
        ediEmail = (EditText) findViewById(R.id.ediEmail);
        profile = Utils.getProfile(LoginActivity.this);
        ediPassword = (EditText) findViewById(R.id.ediPassword);
        btnLogin.setOnClickListener(btnLoginOnClickListener);
        btnForgetPassword.setOnClickListener(btnForgetPasswordOnClickListener);
        btnSignUp.setOnClickListener(btnSignUpOnClickListener);
        pref = getApplicationContext().getSharedPreferences("OWNCLOUD PASSWORD", 0);
        fadeProgressBar = (RelativeLayout) findViewById(R.id.fadeProgressBar);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        fadeProgressBar.setOnClickListener(fadeProgressBarOnClickListener);
        progressBar.setVisibility(View.GONE);
        fadeProgressBar.setVisibility(View.GONE);
        if (profile != null) {
            ediEmail.setFocusable(false);
            btnSignUp.setVisibility(View.INVISIBLE);
            ediEmail.setText(profile.getEmail());
        } else {
            ediEmail.setFocusable(true);
            btnSignUp.setVisibility(View.VISIBLE);
        }
        editor = pref.edit();
    }
    //endregion

    private View.OnClickListener fadeProgressBarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private void setVisibility(boolean isVisible) {
        if (isVisible) {
            progressBar.setVisibility(View.VISIBLE);
            fadeProgressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            fadeProgressBar.setVisibility(View.GONE);
        }
    }

    //region on click region
    private LinearLayout.OnClickListener btnSignUpOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(getBaseContext(), RegisterActivity.class));
        }
    };

    private LinearLayout.OnClickListener btnForgetPasswordOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(getBaseContext(), PasswordActivity.class));
        }
    };

    private LinearLayout.OnClickListener btnLoginOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ediEmail.getText().toString().length() > 0 && ediPassword.getText().toString().length() > 0) {
                if (profile != null) {
                    if (profile.getEmail().equals(ediEmail.getText().toString())) {
                        authentication(ediEmail.getText().toString(), ediPassword.getText().toString());
                    } else {
                        Utils.showError(LoginActivity.this, Utils.CODE_AUTHENTICATION_ERROR, Utils.AUTHENTICATION_ERROR);
                    }
                } else {
                    authentication(ediEmail.getText().toString(), ediPassword.getText().toString());
                }
            } else if (ediEmail.getText().toString().length() == 0) {
                ErrorDialogFragment leaveErrorDialogFragment =
                        ErrorDialogFragment.newInstance(LoginActivity.this, Utils.getDialogMessage(LoginActivity.this, Utils.CODE_EMAIL_FIELD, Utils.CODE_EMAIL_FIELD));
                leaveErrorDialogFragment.setCancelable(false);
                leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
            } else if (ediPassword.getText().toString().length() == 0) {
                ErrorDialogFragment leaveErrorDialogFragment =
                        ErrorDialogFragment.newInstance(LoginActivity.this, Utils.getDialogMessage(LoginActivity.this, Utils.CODE_PASSWORD_FIELD, Utils.CODE_PASSWORD_FIELD));
                leaveErrorDialogFragment.setCancelable(false);
                leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
            } else if (!Utils.isValidEmail(ediEmail.getText().toString())) {
                ErrorDialogFragment leaveErrorDialogFragment =
                        ErrorDialogFragment.newInstance(LoginActivity.this, Utils.getDialogMessage(LoginActivity.this, Utils.CODE_EMAIL_VALID, Utils.CODE_EMAIL_FIELD));
                leaveErrorDialogFragment.setCancelable(false);
                leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
            }
        }
    };
    //endregion

    //region backpressed
    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
        android.os.Process.killProcess(android.os.Process.myPid());
    }
    //endregion

    //region authentication
    private void authentication(final String email, final String password) {
        setVisibility(true);
        GsonRequest<Login> mGsonRequest = new GsonRequest<Login>(
                Request.Method.POST,
                Utils.API_URL + "Token",
                Login.class,
                null,
                responseListener(),
                errorListener()) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "grant_type=password&username=" + email
                        + "&password=" + password;
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<Login> responseListener() {
        return new Response.Listener<Login>() {
            @Override
            public void onResponse(Login response) {
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), LoginActivity.this);
                    } else {
                        DBHelper dbHelper = new DBHelper(LoginActivity.this);
                        getBaseContext().getContentResolver().delete(TokenProvider.CONTENT_URI, null, null);
                        insertToken(response);
                        Log.i(TAG, "Retrieve JSON : " + response.getAccessToken());
                    }
                }
            }
        };
    }

    private void insertToken(Login login) {
        DBHelper helper = new DBHelper(getBaseContext());
        String accessToken = login.getAccessToken();
        String tokenType = login.getTokenType();
        int expiredIn = login.getExpiresIn();
        String userName = login.getUserName();
        String issuedDate = login.getIssued();
        String expiredDate = login.getExpires();
        Date date = new Date();
        String currDate = Utils.setCalendarDate(Utils.DATE_FORMAT, date);
        expiredDate = Utils.setDate("EEE, d MMM yyyy HH:mm:ss z", Utils.DATE_FORMAT, expiredDate);
        issuedDate = Utils.setDate("EEE, d MMM yyyy HH:mm:ss z", Utils.DATE_FORMAT, issuedDate);
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.TOKEN_COLUMN_ACCESS_TOKEN, accessToken);
        Log.i("ACCESS TOKEN", accessToken);
        contentValues.put(DBHelper.TOKEN_COLUMN_TOKEN_TYPE, tokenType);
        contentValues.put(DBHelper.TOKEN_COLUMN_EXPIRED_IN, String.valueOf(expiredIn));
        contentValues.put(DBHelper.TOKEN_COLUMN_USER_NAME, userName);
        contentValues.put(DBHelper.TOKEN_COLUMN_ISSUED_DATE, issuedDate);
        contentValues.put(DBHelper.TOKEN_COLUMN_EXPIRED_DATE, expiredDate);
        contentValues.put(DBHelper.TOKEN_COLUMN_STATUS, 1);
        contentValues.put(DBHelper.TOKEN_COLUMN_CREATE_BY, userName);
        contentValues.put(DBHelper.TOKEN_COLUMN_CREATE_DATE, currDate);
        contentValues.put(DBHelper.TOKEN_COLUMN_UPDATE_BY, userName);
        contentValues.put(DBHelper.TOKEN_COLUMN_UPDATE_DATE, currDate);
        try {
            getBaseContext().getContentResolver().insert(TokenProvider.CONTENT_URI, contentValues);
        } catch (Exception e) {
            Log.i(TAG, e.getMessage());
        } finally {
            getEmployee(ediEmail.getText().toString());
        }
    }
    //endregion

    //region get employee
    private void getEmployee(final String email) {
        HashMap<String, String> headers = new HashMap<String, String>();
        String token = Utils.getToken(LoginActivity.this);
        headers.put("Authorization", "Bearer " + token);
        Log.i(TAG, "Authorization: Bearer " + token);
        GsonRequest<Register> mGsonRequest = new GsonRequest<Register>(
                Request.Method.GET,
                Utils.API_URL + "api/Employee/EmployeeId?email=" + Utils.encode(email),
                Register.class,
                headers,
                responseListenerEmployee(),
                errorListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<Register> responseListenerEmployee() {
        return new Response.Listener<Register>() {
            @Override
            public void onResponse(Register response) {
                getProfile(String.valueOf(response.getemployeeId()));
            }
        };
    }
    //endregion

    //region get profile
    private void getProfile(final String id) {
        HashMap<String, String> headers = new HashMap<String, String>();
        String token = Utils.getToken(LoginActivity.this);
        headers.put("Authorization", "Bearer " + token);
        Log.i(TAG, "Authorization: Bearer " + token);
        GsonRequest<Profile> mGsonRequest = new GsonRequest<Profile>(
                Request.Method.GET,
                Utils.API_URL + "api/Employee/" + Utils.encode(id),
                Profile.class,
                headers,
                responseListenerProfile(),
                errorListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<Profile> responseListenerProfile() {
        return new Response.Listener<Profile>() {
            @Override
            public void onResponse(Profile response) {
                try {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.PROFILE_COLUMN_EMAIL, response.getEmail());
                    contentValues.put(DBHelper.PROFILE_COLUMN_ID, response.getId());
                    contentValues.put(DBHelper.PROFILE_COLUMN_FAVORITE_ID, response.getFavouriteId());
                    contentValues.put(DBHelper.PROFILE_COLUMN_IMAGE, response.getProfileImage());
                    contentValues.put(DBHelper.PROFILE_COLUMN_BRANCH_ID, response.getDefaultBranchId());
                    contentValues.put(DBHelper.PROFILE_COLUMN_BRANCH_NAME, response.getDefaultBranchName());
                    contentValues.put(DBHelper.PROFILE_COLUMN_NAME, response.getName());
                    contentValues.put(DBHelper.PROFILE_COLUMN_COUNTRY, response.getCountry());
                    contentValues.put(DBHelper.PROFILE_COLUMN_COUNTRY_IMAGE, response.getCountryImage());
                    contentValues.put(DBHelper.PROFILE_COLUMN_GENDER, response.getGender());
                    contentValues.put(DBHelper.PROFILE_COLUMN_DEPARTMENT, response.getDepartment());
                    contentValues.put(DBHelper.PROFILE_COLUMN_POSITION, response.getPosition());
                    contentValues.put(DBHelper.PROFILE_COLUMN_CONTACT_NO, response.getContactNo());
                    contentValues.put(DBHelper.PROFILE_COLUMN_EMAIL, response.getEmail());
                    contentValues.put(DBHelper.PROFILE_COLUMN_ALL_BRANCH, response.getAllBranch());
                    contentValues.put(DBHelper.PROFILE_COLUMN_LAST_UPDATED, response.getLastUpdated());
                    String apiKey = UAirship.shared().getPushManager().getChannelId();
                    contentValues.put(DBHelper.PROFILE_COLUMN_API_KEY, apiKey);
                    contentValues.put(DBHelper.PROFILE_COLUMN_EMPLOYEE_NO, response.getEmployeeNo());
                    contentValues.put(DBHelper.PROFILE_COLUMN_COUNTRY_NAME, response.getCountryName());
                    contentValues.put(DBHelper.PROFILE_COLUMN_DOB, response.getDOB());
                    contentValues.put(DBHelper.PROFILE_COLUMN_OFFICE_NO, response.getOfficeNo());
                    contentValues.put(DBHelper.PROFILE_COLUMN_HOD_ID, response.getHODId());
                    contentValues.put(DBHelper.PROFILE_COLUMN_HOD_NAME, response.getHODName());
                    contentValues.put(DBHelper.PROFILE_COLUMN_OXUSER, response.getOXUser());
                    if (Utils.getProfile(LoginActivity.this) == null) {
                        getContentResolver().insert(ProfileProvider.CONTENT_URI, contentValues);
                    } else {
                        getContentResolver().update(ProfileProvider.CONTENT_URI, contentValues, DBHelper.PROFILE_COLUMN_ID + "=?", new String[]{String.valueOf(response.getId())});
                    }
                    editor.clear();
                    editor.commit();
                    editor.putString("own_password", ediPassword.getText().toString());
                    editor.commit();
                    initConnection();
                } catch (Exception ex) {
                    ex.fillInStackTrace();
                } finally {
                    com.applab.wcircle_pro.Home.HttpHelper.setSystemSetting(LoginActivity.this, TAG);
                    HttpHelper.GetServerDate(LoginActivity.this, TAG);
                    String apiKey = UAirship.shared().getPushManager().getChannelId();
                    if (Utils.isConnectingToInternet(LoginActivity.this)) {
                        if (apiKey != null) {
                            if (apiKey.length() > 0) {
                                pushRegId(apiKey, String.valueOf(Utils.getProfile(LoginActivity.this).getId()));
                            } else {
                                Utils.clearPreviousActivity(LoginActivity.this);
                            }
                        } else {
                            Utils.clearPreviousActivity(LoginActivity.this);
                        }
                    } else {
                        Utils.clearPreviousActivity(LoginActivity.this);
                    }
                }
            }
        };
    }
    //endregion

    private void initConnection() {
        AbstractXMPPConnection connection = XMPPConn.getInstance().getConn();
        if (Utils.isConnectingToInternet(LoginActivity.this)) {
            if (connection != null) {
                if (!connection.isConnected()) {
                    connection.disconnect();
                    startService(new Intent(LoginActivity.this, BackgroundXMPPService.class));
                } else {
                    connection.disconnect();
                    startService(new Intent(LoginActivity.this, BackgroundXMPPService.class));
                }
            } else {
                startService(new Intent(LoginActivity.this, BackgroundXMPPService.class));
            }
        }
    }

    //region push reg id
    private void pushRegId(final String regId, final String id) {
        String token = Utils.getToken(LoginActivity.this);
        Log.i(TAG, "Push Bot Api: " + regId);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<App> mGsonRequest = new GsonRequest<App>(
                Request.Method.POST,
                Utils.API_URL + "api/Device",
                App.class,
                headers,
                responseListenerDevice(),
                errorListenerDevice()) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "EmployeeId=" + id + "&Platform=1&Token=" + regId +
                        "&OsVersion=" + Utils.getAndroidVersion() + "&UserAgent=" +
                        Utils.getBrand() + " (" + Utils.getModel() + ")" +
                        "&IMEI=" + Utils.getIMEI(getBaseContext()) + "&AppVersion=" +
                        Utils.getAppVersion(LoginActivity.this) +
                        "&AppLastUpdateOn=" + Utils.getUpdatedDate(getBaseContext(),
                        getApplicationContext().getPackageName()) + "&NotificationChannel=" +
                        LoginActivity.this.getString(R.string.urbanairship);
                Log.i(TAG, "Http Body: " + httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<App> responseListenerDevice() {
        return new Response.Listener<App>() {
            @Override
            public void onResponse(App response) {
                Utils.clearPreviousActivity(LoginActivity.this);
            }
        };
    }

    private Response.ErrorListener errorListenerDevice() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                } else if (error instanceof AuthFailureError) {
                } else if (error instanceof ServerError) {
                } else if (error instanceof NetworkError) {
                } else if (error instanceof ParseError) {
                }
                setVisibility(false);
                Log.i("ERROR", "Error");
                Utils.clearPreviousActivity(LoginActivity.this);
            }
        };
    }
    //endregion

    //region stop and destroy
    @Override
    protected void onStop() {
        super.onStop();
        setVisibility(false);
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setVisibility(false);
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }
    //endregion

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.validateNetworkAndTimeOutError(LoginActivity.this, error, true);
                if (error instanceof AuthFailureError) {
                } else if (error instanceof ServerError) {
                    NetworkResponse response = error.networkResponse;
                    if (response != null && response.data != null) {
                        switch (response.statusCode) {
                            case 400:
                                json = new String(response.data);
                                Log.i(TAG, json);
                                json = Utils.trimMessage(json, "error_description");
                                if (json != null) {
                                    ErrorDialogFragment leaveErrorDialogFragment = ErrorDialogFragment.newInstance(LoginActivity.this, json);
                                    leaveErrorDialogFragment.setCancelable(false);
                                    leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
                                }
                                break;
                        }
                    }
                } else if (error instanceof NetworkError) {
                } else if (error instanceof ParseError) {
                }
                setVisibility(false);
            }
        };
    }

    public void alertDialog(String errorMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setCancelable(false);
        builder.setMessage(errorMessage);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    //region menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //endregion

    //region broadcast
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(HomeActivity.ACTION)) {
                if (Utils.getRegId(getBaseContext()).equals("")) {
                    if (intent.getStringExtra("reg_id") != null) {
                        Utils.setRegId(intent.getStringExtra("reg_id").
                                replace("Device registered, registration ID=", ""), getBaseContext());
                    }
                }
            }
        }
    };
    //endregion

    //region resume and pause
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(HomeActivity.ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, intentFilter);
        Utils.setIsActivityOpen(true, getBaseContext(), true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }
    //endregion
}
