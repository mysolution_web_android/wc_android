package com.applab.wcircle_pro.Login;

import android.database.Cursor;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("token_type")
    @Expose
    private String tokenType;
    @SerializedName("expires_in")
    @Expose
    private Integer expiresIn;
    @Expose
    private String userName;
    @SerializedName(".issued")
    @Expose
    private String Issued;
    @SerializedName(".expires")
    @Expose
    private String Expires;
    @Expose
    private String Message;

    /**
     * @return The Message
     */
    public String getMessage() {
        return Message;
    }

    /**
     * @param Message The Message
     */
    public void setMessage(String Message) {
        this.Message = Message;
    }

    /**
     * @return The accessToken
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * @param accessToken The access_token
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * @return The tokenType
     */
    public String getTokenType() {
        return tokenType;
    }

    /**
     * @param tokenType The token_type
     */
    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    /**
     * @return The expiresIn
     */
    public Integer getExpiresIn() {
        return expiresIn;
    }

    /**
     * @param expiresIn The expires_in
     */
    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    /**
     * @return The userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName The userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return The Issued
     */
    public String getIssued() {
        return Issued;
    }

    /**
     * @param Issued The .issued
     */
    public void setIssued(String Issued) {
        this.Issued = Issued;
    }

    /**
     * @return The Expires
     */
    public String getExpires() {
        return Expires;
    }

    /**
     * @param Expires The .expires
     */
    public void setExpires(String Expires) {
        this.Expires = Expires;
    }

    public Login getLogin (Cursor cursor,int position){
        cursor.moveToPosition(position);
        Login login = new Login();
        login.setAccessToken(cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_ACCESS_TOKEN)));
        login.setExpires(cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_EXPIRED_DATE)));
        login.setExpiresIn(cursor.getInt(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_EXPIRED_IN)));
        login.setIssued(cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_ISSUED_DATE)));
        login.setMessage(null);
        login.setTokenType(cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_TOKEN_TYPE)));
        login.setUserName(cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_USER_NAME)));
        return login;
    }
}