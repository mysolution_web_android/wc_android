package com.applab.wcircle_pro;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by user on 27/10/2015.
 */
public class ErrorDialogFragment extends DialogFragment {
    private String mMessage;
    private TextView mTxtTitle, mTxtMessage;
    private ImageView mBtnCancel;
    private LinearLayout mBtnOk;
    private static AlertDialog.Builder builder;
    private static Context mContext;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static ErrorDialogFragment newInstance(Context context, String message) {
        ErrorDialogFragment frag = new ErrorDialogFragment();
        Bundle args = new Bundle();
        args.putString("message", message);
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mMessage = getArguments().getString("message");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_error, null);
        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtMessage = (TextView) v.findViewById(R.id.txtMessage);
        mTxtTitle.setText(getString(R.string.error));
        mTxtMessage.setText(mMessage);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnOk = (LinearLayout) v.findViewById(R.id.btnOK);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);
        mBtnOk.setOnClickListener(mBtnOkOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ErrorDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener mBtnOkOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ErrorDialogFragment.this.getDialog().cancel();
        }
    };
}

