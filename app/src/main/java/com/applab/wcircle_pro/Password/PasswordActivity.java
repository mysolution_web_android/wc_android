package com.applab.wcircle_pro.Password;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.ErrorDialogFragment;
import com.applab.wcircle_pro.Login.LoginActivity;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;

import org.json.JSONObject;

public class PasswordActivity extends AppCompatActivity {
    private LinearLayout btnCancel, btnSubmit;
    private EditText ediEmail;
    private String TAG = "PASSWORD";
    private GsonAsyncTask gsonAsyncTask;
    private RelativeLayout fadeRL;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        ediEmail = (EditText) findViewById(R.id.ediEmail);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        btnSubmit = (LinearLayout) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(btnSubmitOnClickListener);
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        fadeRL.setOnClickListener(fadeRLOnClickListener);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        setVisibilityRlAndProgressBar(false);
    }

    private void setVisibilityRlAndProgressBar(boolean isVisible) {
        if (isVisible) {
            fadeRL.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            fadeRL.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener fadeRLOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private LinearLayout.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setVisibilityRlAndProgressBar(true);
            if (ediEmail.getText().toString().length() > 0) {
                gsonAsyncTask = new GsonAsyncTask();
                gsonAsyncTask.execute();
            } else if (ediEmail.getText().toString().length() == 0) {
                ErrorDialogFragment leaveErrorDialogFragment =
                        ErrorDialogFragment.newInstance(PasswordActivity.this, Utils.getDialogMessage(PasswordActivity.this,
                                Utils.CODE_EMAIL_FIELD, Utils.EMAIL_FIELD));
                leaveErrorDialogFragment.setCancelable(false);
                leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
                setVisibilityRlAndProgressBar(false);
            } else if (!Utils.isValidEmail(ediEmail.getText().toString())) {
                ErrorDialogFragment leaveErrorDialogFragment = ErrorDialogFragment.newInstance(PasswordActivity.this,
                        Utils.getDialogMessage(PasswordActivity.this, Utils.CODE_EMAIL_VALID, Utils.EMAIL_VALID));
                leaveErrorDialogFragment.setCancelable(false);
                leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
                setVisibilityRlAndProgressBar(false);
            }
        }
    };

    private LinearLayout.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setVisibilityRlAndProgressBar(true);
            startActivity(new Intent(getBaseContext(), LoginActivity.class));
            finish();
        }
    };

    public void alertDialog(String errorMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(PasswordActivity.this);
        builder.setCancelable(false);
        builder.setMessage(errorMessage);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void getGson() {
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Employee/ForgetPassword",
                JSONObject.class,
                null,
                responseListener(),
                errorListener()) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "Email=" + ediEmail.getText().toString();
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<JSONObject> responseListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Utils.showError(PasswordActivity.this, Utils.CODE_RESET_PASSWORD, Utils.RESET_PASSWORD);
                startActivity(new Intent(getBaseContext(), LoginActivity.class));
                finish();
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.serverHandlingError(PasswordActivity.this, error);
            }
        };
    }

    public class GsonAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            getGson();
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void result) {
        }
    }

    @Override
    public void onBackPressed() {
        setVisibilityRlAndProgressBar(true);
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
