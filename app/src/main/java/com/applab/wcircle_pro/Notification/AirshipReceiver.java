package com.applab.wcircle_pro.Notification;

/**
 * Created by user on 31/3/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.applab.wcircle_pro.Chat.ChatActivity;
import com.applab.wcircle_pro.Dashboard.HttpHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.urbanairship.push.BaseIntentReceiver;
import com.urbanairship.push.PushMessage;

import me.leolin.shortcutbadger.ShortcutBadger;

public class AirshipReceiver extends BaseIntentReceiver {
    private static final String TAG = "AirshipReceiver";

    /**
     * Intent action sent as a local broadcast to update the channel.
     */
    public static final String ACTION_UPDATE_CHANNEL = "ACTION_UPDATE_CHANNEL";

    @Override
    protected void onChannelRegistrationSucceeded(Context context, String channelId) {
        // Broadcast that the channel updated. Used to refresh the channel ID on the home fragment
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(ACTION_UPDATE_CHANNEL));
    }

    @Override
    protected void onChannelRegistrationFailed(Context context) {
        Log.i(TAG, "Channel registration failed.");
    }

    @Override
    protected void onPushReceived(Context context, PushMessage message, int notificationId) {
        SharedPreferences sp = context.getSharedPreferences("PUSH_NOTIFICATION_NUMBER", 0);
        int no = sp.getInt("NOTIFICATION_NUMBER", 0);
        SharedPreferences.Editor ed = sp.edit();
        no += 1;
        ed.putInt("NOTIFICATION_NUMBER", no);
        ed.apply();
        ShortcutBadger.applyCount(context, no);
        //HttpHelper.getIndicator(context, TAG, null);
        Log.i(TAG, "Received push notification. Alert: " + message.getAlert() + ". Notification ID: " + notificationId);
    }

    @Override
    protected void onBackgroundPushReceived(Context context, PushMessage message) {
        SharedPreferences sp = context.getSharedPreferences("PUSH_NOTIFICATION_NUMBER", 0);
        int no = sp.getInt("NOTIFICATION_NUMBER", 0);
        SharedPreferences.Editor ed = sp.edit();
        no += 1;
        ed.putInt("NOTIFICATION_NUMBER", no);
        ed.apply();
        ShortcutBadger.applyCount(context, no);
        //HttpHelper.getIndicator(context, TAG, null);
        Log.i(TAG, "Received background push message: " + message);
    }

    @Override
    protected boolean onNotificationOpened(Context context, PushMessage message, int notificationId) {
        Bundle bundle = message.getPushBundle();
        String id = bundle.getString("id");
        String module = bundle.getString("module");
        Log.i(TAG, "User clicked notification. Alert: " + message.getAlert());

        // Return false here to allow Urban Airship to auto launch the launcher activity
        return setPush(context, id, module);
    }

    @Override
    protected boolean onNotificationActionOpened(Context context, PushMessage message, int notificationId, String buttonId, boolean isForeground) {
        Log.i(TAG, "User clicked notification button. Button ID: " + buttonId + " Alert: " + message.getAlert());

        // Return false here to allow Urban Airship to auto launch the launcher
        // activity for foreground notification action buttons
        return false;
    }

    @Override
    protected void onNotificationDismissed(Context context, PushMessage message, int notificationId) {
        Log.i(TAG, "Notification dismissed. Alert: " + message.getAlert() + ". Notification ID: " + notificationId);
    }

    public boolean setPush(Context context, String id, String module) {
        boolean isValid = true;
        if (Utils.getProfile(context) != null) {
            if (id != null) {
                if (module != null) {
                    if (module.equals("Messenger")) {
                        Intent intent = new Intent(context, ChatActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    } else {
                        setPushNotification(context, module, id, TAG);
                    }
                } else {
                    isValid = false;
                }
            } else {
                isValid = false;
            }
        } else {
            isValid = false;
        }
        return isValid;
    }

    private void setPushNotification(Context context, String module, String id, String TAG) {
        switch (module) {
            case "Calendar":
                com.applab.wcircle_pro.Home.HttpHelper.getCalendarGson(context, id, TAG);
                break;
            case "Media":
                com.applab.wcircle_pro.Home.HttpHelper.getImageGson(context, 1, Integer.valueOf(id), TAG);
                break;
            case "Leave":
                com.applab.wcircle_pro.Home.HttpHelper.getLeaveGson(context, id, TAG);
                break;
            case "News":
                com.applab.wcircle_pro.Home.HttpHelper.getNewsGson(context, id, TAG);
                break;
            case "Event":
                com.applab.wcircle_pro.Home.HttpHelper.getEventGson(context, id, TAG);
                break;
        }
    }
}