package com.applab.wcircle_pro.Notification;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.applab.wcircle_pro.Dashboard.Dashboard;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GcmIntentService extends IntentService {
    private static final int UNIQUEID = 45612;
    NotificationCompat.Builder notification;
    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (extras != null && !extras.isEmpty()) {  // has effect of unparcelling Bundle
            // Since we're not using two way messaging, this is all we really to check for
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                Logger.getLogger("GCM_RECEIVED").log(Level.INFO, extras.toString());

                showToast(extras.getString("message"));
                intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                sendBroadcast(intent, null);
                sendNotification(extras.getString("message"));
//                CustomNotification(extras.getString("message"));
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    protected void showToast(final String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void sendNotification(final String msg) {
        if(!Utils.getIsActivityOpen(getBaseContext())){
            Log.i("ALIVE",String.valueOf(Utils.getIsActivityOpen(getBaseContext())));
//            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
            notification = new NotificationCompat.Builder(this);
            notification.setAutoCancel(true);
            notification.setSmallIcon(R.mipmap.icon);
            notification.setTicker("This is the ticker");
            notification.setWhen(System.currentTimeMillis());
            notification.setContentTitle("Here is the title");
            notification.setSound(alarmSound);
            notification.setContentText(msg);
            notification.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
            Intent intentNotification = new Intent(this,DashboardActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intentNotification,PendingIntent.FLAG_UPDATE_CURRENT);
            notification.setContentIntent(pendingIntent);
            //Builds notification and send it
            NotificationManager me =(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            me.notify(UNIQUEID, notification.build());
        }
        else{
            Log.i("ALIVE", String.valueOf(Utils.getIsActivityOpen(getBaseContext())));
            Handler mHandler = new Handler(getMainLooper());
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    final MediaPlayer mp = MediaPlayer.create(getApplication(), R.raw.notification);
                    mp.start();
                }
            });
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(this);
            Intent intent = new Intent(Utils.NOTIFICATION_ACTION);
            mgr.sendBroadcast(intent);
            Utils.vibrate(1000,getBaseContext());
        }
        InputStream stream = new ByteArrayInputStream(msg.getBytes());
        Reader reader = new InputStreamReader(stream);
        Gson gson = new Gson();
        Notifications notifications = gson.fromJson(reader,Notifications.class);
        Uri uri = NotificationProvider.CONTENT_URI;
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.NOTIFICATION_COLUMN_TITLE,notifications.getTitle());
        contentValues.put(DBHelper.NOTIFICATION_COLUMN_DESCRIPTION,notifications.getDescription());
        contentValues.put(DBHelper.NOTIFICATION_COLUMN_TYPE,notifications.getType());
        String createDate = notifications.getCreateDate();
        createDate = Utils.setDate(Utils.DATE_FORMAT,"yyyy-MM-dd HH:mm:ss",createDate);
        contentValues.put(DBHelper.NOTIFICATION_COLUMN_CREATE_DATE, createDate);
        contentValues.put(DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY, 0);
        getApplication().getContentResolver().insert(uri,contentValues);
    }

    private void CustomNotification(final String msg) {
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification);

        // Set Notification Title
        String strtitle = "Here is the Title";
        // Set Notification Text
        String strtext = msg;

        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(this, Dashboard.class);
        // Send data to NotificationView Class
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                // Set Icon
                .setSmallIcon(R.mipmap.icon)
                        // Set Ticker Message
                .setTicker(msg)
                        // Dismiss Notification
                .setAutoCancel(true)
                        // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                        // Set RemoteViews into Notification
                .setContent(remoteViews);

        // Locate and set the Text into customnotificationtext.xml TextViews
        remoteViews.setTextViewText(R.id.txtTitle, strtitle);
        remoteViews.setTextViewText(R.id.txtDesc, msg);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());
    }
}