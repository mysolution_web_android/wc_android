package com.applab.wcircle_pro.Notification;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;

import java.util.ArrayList;

/**
 * Created by admin on 13/8/2015.
 */
public class NotificationDialogFragment extends DialogFragment {
    private ArrayList<Notifications> arrayList;
    private TextView txtTitle, txtNo;
    private ImageView imgLeft, imgRight, btnCancel;
    private LinearLayout btnDismiss;
    private ViewPager viewPager;
    private NotificationPagerAdapter adapter;

    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.arrayList = getArguments().getParcelableArrayList("Notifications");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.window_notification, container, false);
        btnCancel = (ImageView) rootView.findViewById(R.id.btnCancel);
        imgLeft = (ImageView) rootView.findViewById(R.id.imgLeft);
        imgRight = (ImageView) rootView.findViewById(R.id.imgRight);
        txtNo = (TextView) rootView.findViewById(R.id.txtNo);
        btnDismiss = (LinearLayout) rootView.findViewById(R.id.btnDismiss);
        txtTitle = (TextView) rootView.findViewById(R.id.txtTitle);
        viewPager = (ViewPager) rootView.findViewById(R.id.pager);
        adapter = new NotificationPagerAdapter(getChildFragmentManager(), arrayList);
        viewPager.setAdapter(adapter);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        viewPager.setCurrentItem(0);
        imgLeft.setVisibility(View.INVISIBLE);
        if (arrayList.size() == 1) {
            imgRight.setVisibility(View.INVISIBLE);
            imgLeft.setVisibility(View.INVISIBLE);
        }
        txtTitle.setText(arrayList.get(viewPager.getCurrentItem()).getTitle());
        if (arrayList.size() >= 9) {
            txtNo.setText("+9");
        } else {
            txtNo.setText(String.valueOf(arrayList.size()));
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY, 1);
        getActivity().getContentResolver().update(NotificationProvider.CONTENT_URI, contentValues,
                DBHelper.NOTIFICATION_COLUMN_ID + "=?", new String[]{arrayList.get(0).getId()});
        viewPager.addOnPageChangeListener(pageChangeListener);
        return rootView;
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            txtTitle.setText(arrayList.get(position).getTitle());
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY, 1);
            int id = getActivity().getContentResolver().update(NotificationProvider.CONTENT_URI, contentValues,
                    DBHelper.NOTIFICATION_COLUMN_ID + "=?", new String[]{arrayList.get(position).getId()});
            Log.i("Notification", String.valueOf(id));
            if (position > 0 && position < arrayList.size() - 1) {
                imgLeft.setVisibility(View.VISIBLE);
                imgRight.setVisibility(View.VISIBLE);
            } else if (position == arrayList.size() - 1) {
                imgRight.setVisibility(View.INVISIBLE);
                imgLeft.setVisibility(View.VISIBLE);
            } else if (position == 0) {
                imgRight.setVisibility(View.VISIBLE);
                imgLeft.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
