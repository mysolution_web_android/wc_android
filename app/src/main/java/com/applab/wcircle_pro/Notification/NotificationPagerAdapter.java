package com.applab.wcircle_pro.Notification;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class NotificationPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Notifications> arrayList;

    // constructor
    public NotificationPagerAdapter(FragmentManager fm, ArrayList<Notifications> arrayList) {
        super(fm);
        this.arrayList = arrayList;
    }

    @Override
    public Fragment getItem(int position) {
        return NotificationListFragment.newInstance(arrayList,position);
    }


    @Override
    public int getCount() {
        return this.arrayList == null ? 0 : this.arrayList.size();
    }
}