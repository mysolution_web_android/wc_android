package com.applab.wcircle_pro.Notification;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.applab.wcircle_pro.R;

import java.util.ArrayList;

public class NotificationListFragment extends ListFragment {
    private String title;
    private String desc;
    private String type;

    static NotificationListFragment newInstance(ArrayList<Notifications> arrayList,int position) {
        NotificationListFragment f = new NotificationListFragment();
        Bundle args = new Bundle();
        args.putString("title", arrayList.get(position).getTitle());
        args.putString("desc", arrayList.get(position).getDescription());
        args.putString("type", arrayList.get(position).getType());
        f.setArguments(args);
        return f;
    }

    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = getArguments() != null ? getArguments().getString("title") : null;
        desc = getArguments() != null ? getArguments().getString("desc") : null;
        type = getArguments() != null ? getArguments().getString("type") : null;
    }

    /**
     * The Fragment's UI is just a simple text view showing its
     * instance number.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View viewLayout = inflater.inflate(R.layout.custom_notification_leave, container, false);
        if(type.equals("calendar status")){
            viewLayout = inflater.inflate(R.layout.custom_notification_calendar_status, container, false);
        }
        else if(type.equals("calendar request")){
            viewLayout = inflater.inflate(R.layout.custom_notification_calendar_request, container, false);
        }
        else if(type.equals("leave")){
            viewLayout = inflater.inflate(R.layout.custom_notification_leave, container, false);
        }
        return viewLayout;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.i("FragmentList", "Item clicked: " + id);
    }


}
