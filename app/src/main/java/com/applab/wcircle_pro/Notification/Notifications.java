package com.applab.wcircle_pro.Notification;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notifications implements Parcelable {

    @Expose
    private String title;
    @Expose
    private String description;
    @Expose
    private String type;
    @SerializedName("create_date")
    @Expose
    private String createDate;

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The createDate
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate The create_date
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(type);
        dest.writeString(createDate);
        dest.writeString(id);
    }

    public static final Parcelable.Creator<Notifications> CREATOR = new
            Parcelable.Creator<Notifications>() {
                public Notifications createFromParcel(Parcel in) {
                    return new Notifications(in);
                }

                public Notifications[] newArray(int size) {
                    return new Notifications[size];
                }
            };

    private Notifications(Parcel parcel) {
        title = parcel.readString();
        description = parcel.readString();
        type = parcel.readString();
        createDate = parcel.readString();
        id = parcel.readString();
    }

    public Notifications() {
    }
}