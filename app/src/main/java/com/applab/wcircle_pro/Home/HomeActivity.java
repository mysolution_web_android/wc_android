package com.applab.wcircle_pro.Home;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Dashboard.DashboardProvider;
import com.applab.wcircle_pro.Login.LoginActivity;
import com.applab.wcircle_pro.Login.TokenProvider;
import com.applab.wcircle_pro.Notification.GcmIntentService;
import com.applab.wcircle_pro.Notification.GcmRegistrationAsyncTask;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.gcm.backend.registration.Registration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity {
    //region variable
    private ProgressBar progressBar;
    private RelativeLayout fadeRL;
    private PopupWindow popupWindow = null;
    private String androidVersion, model, brand, imei,
            androidId, packageName, installedDate,
            updatedDate, cacheSize, glideCacheSize;
    private String TAG = "HOME";
    public static String ACTION = "PUSH_REG_ID";
    public static LocalBroadcastManager mgrs;
    //endregion

    //region create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
    //endregion

    //region Insert Dashboard
    private int getTotalDashboardRow() {
        Cursor cursor = null;
        int total = 0;
        try {
            cursor = getContentResolver().query(DashboardProvider.CONTENT_URI,
                    null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                total = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return total;
    }

    private void insertDashboard() {
        if (getTotalDashboardRow() == 0) {
            ContentValues[] contentValueses = new ContentValues[15];
            String[] title = {"My Dashboard", "My Favourite", "Calendar", "News",
                    "Events", "Messenger", "Staff Directory",
                    "Leave", "Download Centre", "Media Gallery", "Product Catalogue",
                    "My Drive", "My Profile", "Settings", "Log Out"};
            int[] img = {0, R.drawable.dashboard_my_favourite, R.drawable.dashboard_calendar,
                    R.drawable.dashboard_news, R.drawable.dashboard_events,
                    R.drawable.dashboard_messenger, R.drawable.dashboard_staff_directory,
                    R.drawable.dashboard_leave, R.drawable.dashboard_download_centre,
                    R.drawable.dashboard_media_gallery, R.drawable.dashboard_product_catalog,
                    R.drawable.dashboard_my_drive, R.drawable.dashboard_my_profile, 0, 0};
            int[] icon = {R.mipmap.menu_home, R.mipmap.menu_my_favourite,
                    R.mipmap.menu_calendar, R.mipmap.menu_news,
                    R.mipmap.menu_events, R.mipmap.menu_messenger,
                    R.mipmap.menu_staff_directory, R.mipmap.menu_leave,
                    R.mipmap.menu_download, R.mipmap.menu_media,
                    R.mipmap.menu_product_catalog, R.mipmap.menu_my_drive,
                    R.mipmap.menu_my_profile, R.mipmap.menu_setting, R.mipmap.menu_logout};
            for (int i = 0; i < 15; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_TITLE, title[i]);
                contentValues.put(DBHelper.DASHBOARD_COLUMN_IMG_DASHBOARD, img[i]);
                contentValues.put(DBHelper.DASHBOARD_COLUMN_IMG_MENU, icon[i]);
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, 0);
                contentValueses[i] = contentValues;
            }
            getContentResolver().bulkInsert(DashboardProvider.CONTENT_URI, contentValueses);
        }
    }
    //endregion

    //region System Check
    private void setSystemSetting() {
        GsonRequest<App> mGsonRequest = new GsonRequest<App>(
                Request.Method.GET,
                Utils.API_URL + "api/System/Check/1",
                App.class,
                null,
                responseListenerSystem(),
                errorListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<App> responseListenerSystem() {
        return new Response.Listener<App>() {
            @Override
            public void onResponse(App response) {
                insertSystem(response);
            }
        };
    }

    private void insertSystem(App response) {
        if (response != null) {
            if (HttpHelper.getSystemCheck(HomeActivity.this) != null) {
                Date d1 = Utils.setDate(Utils.DATE_FORMAT, response.getMessageLastUpdate());
                Date d2 = Utils.setDate(Utils.DATE_FORMAT, response.getSettingLastUpdate());
                Date d3 = Utils.setDate(Utils.DATE_FORMAT, HttpHelper.getSystemCheck(HomeActivity.this).get(0));
                Date d4 = Utils.setDate(Utils.DATE_FORMAT, HttpHelper.getSystemCheck(HomeActivity.this).get(1));
                if (d3.before(d1)) {
                    HttpHelper.setMessageSetting(HomeActivity.this, TAG);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.SYSTEM_COLUMN_MESSAGE_LAST_UPDATE, response.getMessageLastUpdate());
                    getContentResolver().update(SystemProvider.CONTENT_URI, contentValues, DBHelper.SYSTEM_COLUMN_ID + "=?", new String[]{"1"});
                }
                if (d4.before(d2)) {
                    HttpHelper.setMasterSetting(HomeActivity.this, TAG);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.SYSTEM_COLUMN_SETTING_LAST_UPDATE, response.getSettingLastUpdate());
                    getContentResolver().update(SystemProvider.CONTENT_URI, contentValues, DBHelper.SYSTEM_COLUMN_ID + "=?", new String[]{"1"});
                }
            } else {
                HttpHelper.setMessageSetting(HomeActivity.this, TAG);
                HttpHelper.setMasterSetting(HomeActivity.this, TAG);
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.SYSTEM_COLUMN_ID, 1);
                contentValues.put(DBHelper.SYSTEM_COLUMN_MESSAGE_LAST_UPDATE, response.getMessageLastUpdate());
                contentValues.put(DBHelper.SYSTEM_COLUMN_SETTING_LAST_UPDATE, response.getSettingLastUpdate());
                getContentResolver().insert(SystemProvider.CONTENT_URI, contentValues);
            }

            if (response.getReleaseNo() != null) {
                if (response.getReleaseNo() > Utils.getAppCode(HomeActivity.this) && !response.getMaintenanceMode()) {
                    showPopupProgress(response.getMaintenanceMode(), getString(R.string.version_msg));
                } else if (response.getMaintenanceMode()) {
                    showPopupProgress(response.getMaintenanceMode(), getString(R.string.maintenance_msg));
                } else {
                    try {
                        getData();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    getData();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } else {
            HttpHelper.setMessageSetting(HomeActivity.this, TAG);
            HttpHelper.setMasterSetting(HomeActivity.this, TAG);
            try {
                getData();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    getData();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        };
    }
    //endregion

    public void getData() throws ParseException {
        Uri uri = TokenProvider.CONTENT_URI;
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(uri, null, null, null, DBHelper.TOKEN_COLUMN_ID + " DESC ");
            if (cursor != null && cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    String expiryDate = cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_EXPIRED_DATE));
                    Date date = new Date();
                    Date nextDate = new Date();
                    SimpleDateFormat format = new SimpleDateFormat(Utils.DATE_FORMAT, Locale.getDefault());
                    nextDate = format.parse(expiryDate);
                    if (date.after(nextDate)) {
                        startActivity(new Intent(getBaseContext(), LoginActivity.class));
                        finish();
                    } else {
                        Utils.clearPreviousActivity(HomeActivity.this);
                    }
                }
            } else {
                startActivity(new Intent(getBaseContext(), LoginActivity.class));
                finish();
            }
        } catch (Exception e) {
            e.fillInStackTrace();
            startActivity(new Intent(getBaseContext(), LoginActivity.class));
            finish();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void popUpUpdate(final boolean isMaintenance, String msg) {
        fadeRL.setVisibility(View.VISIBLE);
        LayoutInflater inflater = (LayoutInflater) getBaseContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.window_new_update, (ViewGroup) findViewById(R.id.lv), false);
        popupWindow = new PopupWindow(
                popupView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        TextView txtStatus = (TextView) popupView.findViewById(R.id.txtStatus);
        ImageView btnCancel = (ImageView) popupView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                Utils.clearPreviousActivity(HomeActivity.this);
            }
        });
        TextView txtDesc = (TextView) popupView.findViewById(R.id.txtDesc);
        txtDesc.setText(msg);
        if (isMaintenance) {
            btnCancel.setVisibility(View.INVISIBLE);
            txtStatus.setText(getResources().getString(R.string.maintenance));
        } else {
            txtStatus.setText(getResources().getString(R.string.update));
            btnCancel.setVisibility(View.VISIBLE);

        }
        LinearLayout btnOK = (LinearLayout) popupView.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMaintenance) {
                    Intent a = new Intent(Intent.ACTION_MAIN);
                    a.addCategory(Intent.CATEGORY_HOME);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(a);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    popupWindow.dismiss();
                } else {
                    popupWindow.dismiss();
                    try {
                        getData();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                fadeRL.setVisibility(View.GONE);
            }
        });
    }

    public void showPopupProgress(final boolean isMaintaince, final String msg) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (getWindow().getDecorView().getWindowVisibility() == View.GONE) {
                    showPopupProgress(isMaintaince, msg);
                    return;
                }
                popUpUpdate(isMaintaince, msg);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), true);
        setUpHome();
    }

    private void setUpHome() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        DBHelper helper = new DBHelper(this);
        mgrs = LocalBroadcastManager.getInstance(this);
        androidVersion = Utils.getAndroidVersion();
        model = Utils.getModel();
        brand = Utils.getBrand();
        imei = Utils.getIMEI(this);
        androidId = Utils.getAndroidId(this);
        packageName = getApplicationContext().getPackageName();
        installedDate = Utils.getInstalledDate(this, packageName);
        updatedDate = Utils.getUpdatedDate(this, packageName);
        cacheSize = String.valueOf(Utils.getTotalSize(this));
        glideCacheSize = String.valueOf(Utils.getTotalSizeGlide(this));
        insertDashboard();
        Log.i("HOME", "\nVersion: " + androidVersion + "\nModel: "
                + model + "\nBrand: " + brand + "\nImei: " + imei
                + "\nAndroid id: " + androidId + "\nPackage Name: " + packageName + "\nInstalled Date: "
                + installedDate + "\nUpdated Date: " + updatedDate + "\nTotal Cache Size: " + cacheSize
                + "\nGlide Total Cache Size: " + glideCacheSize + " App Version: " + Utils.getAppVersion(getBaseContext()));
        setSystemSetting();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
