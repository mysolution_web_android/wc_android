package com.applab.wcircle_pro.Home;

import com.google.gson.annotations.Expose;

public class Message {

    @Expose
    private Integer Id;
    @Expose
    private String Code;
    @Expose
    private String Message;
    @Expose
    private String CreatedDate;
    @Expose
    private String UpdatedDate;

    /**
     *
     * @return
     * The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The Code
     */
    public String getCode() {
        return Code;
    }

    /**
     *
     * @param Code
     * The Code
     */
    public void setCode(String Code) {
        this.Code = Code;
    }

    /**
     *
     * @return
     * The Message
     */
    public String getMessage() {
        return Message;
    }

    /**
     *
     * @param Message
     * The Message
     */
    public void setMessage(String Message) {
        this.Message = Message;
    }

    /**
     *
     * @return
     * The CreatedDate
     */
    public String getCreatedDate() {
        return CreatedDate;
    }

    /**
     *
     * @param CreatedDate
     * The CreatedDate
     */
    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    /**
     *
     * @return
     * The UpdatedDate
     */
    public String getUpdatedDate() {
        return UpdatedDate;
    }

    /**
     *
     * @param UpdatedDate
     * The UpdatedDate
     */
    public void setUpdatedDate(String UpdatedDate) {
        this.UpdatedDate = UpdatedDate;
    }

}