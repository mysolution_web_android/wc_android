package com.applab.wcircle_pro.Home;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Calendar.AttendeesProvider;
import com.applab.wcircle_pro.Calendar.Calendar;
import com.applab.wcircle_pro.Calendar.CalendarActivity;
import com.applab.wcircle_pro.Calendar.CalendarEventDetailsActivity;
import com.applab.wcircle_pro.Calendar.CalendarProvider;
import com.applab.wcircle_pro.Calendar.MonthFragment;
import com.applab.wcircle_pro.Chat.xmpp.XMPPConn;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Event.Attachment;
import com.applab.wcircle_pro.Event.AttachmentProvider;
import com.applab.wcircle_pro.Event.Event;
import com.applab.wcircle_pro.Event.EventActivity;
import com.applab.wcircle_pro.Event.EventDetailsActivity;
import com.applab.wcircle_pro.Event.EventProvider;
import com.applab.wcircle_pro.Gallery.GalleryActivity;
import com.applab.wcircle_pro.Gallery.GalleryCheckingProvider;
import com.applab.wcircle_pro.Gallery.ImageActivity;
import com.applab.wcircle_pro.Gallery.ImageChecking;
import com.applab.wcircle_pro.Gallery.ImageCheckingProvider;
import com.applab.wcircle_pro.Gallery.ImageProvider;
import com.applab.wcircle_pro.Leave.EditApplicationActivity;
import com.applab.wcircle_pro.Leave.Leave;
import com.applab.wcircle_pro.Leave.LeaveActivity;
import com.applab.wcircle_pro.Leave.LeaveProvider;
import com.applab.wcircle_pro.Login.LoginActivity;
import com.applab.wcircle_pro.Login.TokenProvider;
import com.applab.wcircle_pro.News.News;
import com.applab.wcircle_pro.News.NewsActivity;
import com.applab.wcircle_pro.News.NewsDetailsActivity;
import com.applab.wcircle_pro.News.NewsImageProvider;
import com.applab.wcircle_pro.News.NewsProvider;
import com.applab.wcircle_pro.Profile.ChangePasswordActivity;
import com.applab.wcircle_pro.Profile.ProfileProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.urbanairship.UAirship;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 15/11/2015.
 */

public class HttpHelper {

    //region Log Out
    public static void setLogOut(Context context) {
        if (Utils.isConnectingToInternet(context)) {
            String apiKey = UAirship.shared().getPushManager().getChannelId();
            if (apiKey != null) {
                if (apiKey.length() > 0) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.PROFILE_COLUMN_API_KEY, apiKey);
                    context.getContentResolver().update(ProfileProvider.CONTENT_URI, contentValues, DBHelper.PROFILE_COLUMN_ID + "=?",
                            new String[]{String.valueOf(Utils.getProfile(context).getId())});
                    logOutGson(context, apiKey);
                }
            } else {
                SharedPreferences pref = context.getSharedPreferences("OWNCLOUD PASSWORD", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();
                ContentValues contentValues = new ContentValues();
                contentValues.put("status", 0);
                context.getContentResolver().update(TokenProvider.CONTENT_URI, contentValues, null, null);
                Intent intent = new Intent(new Intent(context, LoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        } else {
            Utils.showNoConnection(context);
        }
    }

    public static void logOutGson(final Context context, final String apiKey) {
        final String TAG = "Log Out";
        Log.i(TAG, "Push Bot Api: " + apiKey);
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Account/Logout",
                JSONObject.class,
                headers,
                responseListener(context),
                errorListener(context)) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String httpPostBody = "Token=" + apiKey;
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListener(final Context context) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                AbstractXMPPConnection connection = XMPPConn.getInstance().getConn();
                if (connection != null) {
                    if (connection.isConnected()) {
                        connection.disconnect();
                    }
                }
                SharedPreferences pref = context.getSharedPreferences("OWNCLOUD PASSWORD", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();
                context.getContentResolver().delete(TokenProvider.CONTENT_URI, null, null);
                Intent intent = new Intent(new Intent(context, LoginActivity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        };
    }

    public static Response.ErrorListener errorListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Intent intent = new Intent(ChangePasswordActivity.ACTION);
                intent.putExtra("isFail", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utils.serverHandlingError(context, error);
            }
        };
    }
    //endregion


    //region Image
    public static int getImageRow(Context context, int pageNo) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ImageProvider.CONTENT_URI,
                    null, DBHelper.IMAGE_COLUMN_LEVEL + "=?", new String[]
                            {String.valueOf(String.valueOf(pageNo))}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getImageLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.IMAGE_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(GalleryCheckingProvider.CONTENT_URI, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.IMAGE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static void getImageGson(Context context, final int pageNo, int id, String TAG) {
        final String token = Utils.getToken(context);
        final Date lastUpdateDate = getImageLastUpdate(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Gallery" + "/" + Utils.encode(id) +
                "?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" +
                Utils.encode(pageNo) + "&LastUpdated=" + Utils.encode(lastUpdate) +
                "&ReturnEmpty=" + Utils.encode(getImageRow(context, pageNo) > 0) + "";
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Gallery" + "/" + Utils.encode(id) +
                    "?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" +
                    Utils.encode(pageNo) + "&LastUpdated=" + Utils.encode(lastUpdate) +
                    "&ReturnEmpty=" + Utils.encode(getImageRow(context, pageNo) > 0) + "";
        }
        final String finalUrl = url;
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<ImageChecking> mGsonRequest = new GsonRequest<ImageChecking>(
                Request.Method.GET,
                finalUrl,
                ImageChecking.class,
                headers,
                responseImageListener(context, pageNo, id, TAG),
                errorListener(context, "Media")) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<ImageChecking> responseImageListener(final Context context, final int pageNo, final int id, final String TAG) {
        return new Response.Listener<ImageChecking>() {
            @Override
            public void onResponse(ImageChecking response) {
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    }
                    if (response.getImages().size() > 0) {
                        Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                        Date date2 = getImageLastUpdate(context);
                        if (date2 != null) {
                            if (date2.before(date1)) {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(ImageCheckingProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(ImageProvider.CONTENT_URI, null, null);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(ImageProvider.CONTENT_URI, null, null);
                                }
                                insertImage(response, context, pageNo, id, TAG);
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(ImageCheckingProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(ImageProvider.CONTENT_URI, null, null);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(ImageProvider.CONTENT_URI, null, null);
                                }
                                insertImage(response, context, pageNo, id, TAG);
                            }
                        } else {
                            if (pageNo == 1) {
                                context.getContentResolver().delete(ImageCheckingProvider.CONTENT_URI, null, null);
                                context.getContentResolver().delete(ImageProvider.CONTENT_URI, null, null);
                            } else if (pageNo > 1) {
                                context.getContentResolver().delete(ImageProvider.CONTENT_URI, null, null);
                            }
                            insertImage(response, context, pageNo, id, TAG);
                        }
                    } else {
                        if (pageNo == 1) {
                            if (response.getNoOfRecords() == 0) {
                                context.getContentResolver().delete(ImageCheckingProvider.CONTENT_URI, null, null);
                                context.getContentResolver().delete(ImageProvider.CONTENT_URI, null, null);
                            }
                        }
                        Intent intent = new Intent(context, GalleryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("id", id);
                        context.startActivity(intent);
                    }
                }
            }
        };
    }

    public static void insertImage(ImageChecking result, Context context, int pageNo, int id, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesImageChecking = new ContentValues();
            contentValuesImageChecking.put(DBHelper.IMAGE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesImageChecking.put(DBHelper.IMAGE_CHECKING_COLUMN_LEVEL, pageNo);
            contentValuesImageChecking.put(DBHelper.IMAGE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesImageChecking.put(DBHelper.IMAGE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            context.getContentResolver().insert(ImageCheckingProvider.CONTENT_URI, contentValuesImageChecking);
            ContentValues[] contentValueses = new ContentValues[result.getImages().size()];
            for (int i = 0; i < result.getImages().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.IMAGE_COLUMN_ALBUM_ID, id);
                contentValues.put(DBHelper.IMAGE_COLUMN_IMAGE_ID, result.getImages().get(i).getId());
                contentValues.put(DBHelper.IMAGE_COLUMN_IMAGE_SMALL, result.getImages().get(i).getPhotoSmall());
                contentValues.put(DBHelper.IMAGE_COLUMN_LEVEL, pageNo);
                contentValues.put(DBHelper.IMAGE_COLUMN_IS_DEFAULT, String.valueOf(result.getImages().get(i).getIsDefault()));
                contentValues.put(DBHelper.IMAGE_COLUMN_IMAGE_LARGE, result.getImages().get(i).getPhotoLarge());
                contentValues.put(DBHelper.IMAGE_COLUMN_CREATED_DATE, result.getImages().get(i).getCreatedDate());
                contentValues.put(DBHelper.IMAGE_COLUMN_UPDATED_DATE, result.getImages().get(i).getUpdatedDate());
                if (isImageExists(context, result.getImages().get(i).getId())) {
                    context.getContentResolver().delete(ImageProvider.CONTENT_URI, DBHelper.IMAGE_COLUMN_IMAGE_ID + "=?",
                            new String[]{String.valueOf(result.getImages().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (Object ob : contentValueses) {
                if (ob != null) {
                    isEmpty = false;
                }
            }
            long rowsInserted = 0;
            if (!isEmpty) {
                rowsInserted = context.getContentResolver().bulkInsert(ImageProvider.CONTENT_URI, contentValueses);
                Log.i(TAG, String.valueOf(rowsInserted));
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        } finally {
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("id", id);
            context.startActivity(intent);
        }
    }

    public static boolean isImageExists(Context context, Object imageId) {
        boolean isExist = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ImageProvider.CONTENT_URI, null, DBHelper.IMAGE_COLUMN_IMAGE_ID + "=?", new String[]{String.valueOf(imageId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExist = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExist;
    }
    //endregion

    //region news
    public static void getNewsGson(final Context context, String id, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        Log.i(TAG, Utils.getToken(context));
        String url = Utils.API_URL + "api/News/" + Utils.encode(id);
        GsonRequest<News> mGsonRequest = new GsonRequest<News>(
                Request.Method.GET,
                url,
                News.class,
                headers,
                responseNewsListener(context, id, TAG),
                errorListener(context, "News")) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<News> responseNewsListener(final Context context, final String id, final String TAG) {
        return new Response.Listener<News>() {
            @Override
            public void onResponse(News response) {
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        context.getContentResolver().delete(NewsImageProvider.CONTENT_URI, DBHelper.NEWS_IMAGE_COLUMN_NEWS_ID + "=?", new String[]{id});
                        insertNews(response, context, TAG);
                    }
                }
            }
        };
    }

    public static void insertNews(News result, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" + context.getResources().getString(R.string.news_gallery));
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.NEWS_COLUMN_NEWS_ID, result.getId());
            contentValues.put(DBHelper.NEWS_COLUMN_TITLE, result.getTitle());
            contentValues.put(DBHelper.NEWS_COLUMN_DESCRIPTION, result.getDescription());
            contentValues.put(DBHelper.NEWS_COLUMN_AUTHOR, result.getAuthor());
            contentValues.put(DBHelper.NEWS_COLUMN_NO_OF_IMAGE, result.getNoOfImage());
            contentValues.put(DBHelper.NEWS_COLUMN_CREATE_DATE, result.getCreatedDate());
            contentValues.put(DBHelper.NEWS_COLUMN_NEW_CREATE, String.valueOf(result.getNewCreate()));
            if (getNewsData(String.valueOf(result.getId()), context) == 0) {
                Uri uri = context.getContentResolver().insert(NewsProvider.CONTENT_URI, contentValues);
                long rowId = ContentUris.parseId(uri);
                Log.i(TAG, String.valueOf(rowId));
            } else {
                context.getContentResolver().update(NewsProvider.CONTENT_URI, contentValues,
                        DBHelper.NEWS_COLUMN_NEWS_ID + "=?", new String[]{String.valueOf(result.getId())});
            }
            ContentValues[] contentValuesesImage = new ContentValues[result.getNewsImages().size()];
            if (result.getNewsImages() != null) {
                for (int i = 0; i < result.getNewsImages().size(); i++) {
                    ContentValues contentValuesImage = new ContentValues();
                    contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_ID, result.getNewsImages().get(i).getId());
                    contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_DESCRIPTION, result.getNewsImages().get(i).getDescription());
                    contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_IMAGE, result.getNewsImages().get(i).getImage());
                    contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_IMAGE_THUMB, result.getNewsImages().get(i).getImageThumb());
                    contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_NEWS_ID, result.getNewsImages().get(i).getNewsId());
                    contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_UPLOAD_BY, result.getNewsImages().get(i).getUploadBy());
                    contentValuesImage.put(DBHelper.NEWS_IMAGE_COLUMN_UPLOAD_DATE, result.getNewsImages().get(i).getUploadDate());
                    String[] split = result.getNewsImages().get(i).getImage().split("/");
                    contentValues.put(DBHelper.NEWS_IMAGE_COLUMN_PATH, file.getAbsolutePath() + "/" + split[split.length - 1]);
                    contentValuesesImage[i] = contentValuesImage;
                }
                boolean isEmpty = true;
                for (ContentValues contentValuesImage : contentValuesesImage) {
                    if (contentValuesImage != null) {
                        isEmpty = false;
                    }
                }
                if (!isEmpty) {
                    context.getContentResolver().bulkInsert(NewsImageProvider.CONTENT_URI, contentValuesesImage);
                }
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider News Error :" + ex.toString());
        } finally {
            if (Utils.getStatus(Utils.NEWS_READ, context)) {
                com.applab.wcircle_pro.News.HttpHelper.getNewsGsonReading(context, TAG, Integer.valueOf(result.getId()));
            }

            Intent intent = new Intent(context, NewsDetailsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("id", Integer.valueOf(result.getId()));
            context.startActivity(intent);
        }
    }

    public static int getNewsData(String id, Context context) {
        int totalRow = 0;
        Uri uri = NewsProvider.CONTENT_URI;
        String selection = DBHelper.NEWS_COLUMN_NEWS_ID + "=?";
        String[] selectionArgs = {id};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, null,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
                cursor.close();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static int getImageData(String id, Context context) {
        int totalRow = 0;
        Uri uri = NewsImageProvider.CONTENT_URI;
        String[] projection = {DBHelper.NEWS_IMAGE_COLUMN_ID};
        String selection = DBHelper.NEWS_IMAGE_COLUMN_NEWS_ID + "=?";
        String[] selectionArgs = {id};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return totalRow;
    }

    //endregion
    //region event region
    public static void getEventGson(Context context, String id, String TAG) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String url = Utils.API_URL + "api/Event" + "/" + Utils.encode(id);
        GsonRequest<Event> mGsonRequest = new GsonRequest<Event>(
                Request.Method.GET,
                url,
                Event.class,
                headers,
                responseEventListener(context, id, TAG),
                errorListener(context, "Event")) {
        };
        Log.i(TAG, url);
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private static Response.Listener<Event> responseEventListener(final Context context, final String id, final String TAG) {
        return new Response.Listener<Event>() {
            @Override
            public void onResponse(Event response) {
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (getAttachmentRow(context, id) > 0) {
                            context.getContentResolver().delete(AttachmentProvider.CONTENT_URI,
                                    DBHelper.ATTACHMENT_COLUMN_EVENT_ID + "=?", new String[]{String.valueOf(id)});
                        }
                        insertAttachment(response, context, TAG);
                    }
                }
            }
        };
    }

    public static int getEventData(String id, Context context) {
        int totalRow = 0;
        Uri uri = EventProvider.CONTENT_URI;
        String selection = DBHelper.EVENT_COLUMN_EVENT_ID + "=?";
        String[] selectionArgs = {id};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, null,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
                cursor.close();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static int getAttachmentRow(Context context, String id) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(AttachmentProvider.CONTENT_URI, null,
                    DBHelper.ATTACHMENT_COLUMN_EVENT_ID + "=?",
                    new String[]{String.valueOf(id)}, DBHelper.ATTACHMENT_COLUMN_ID + " DESC ");
            if (cursor != null) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return totalRow;
    }

    public static void insertAttachment(Event result, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" + context.getResources().getString(R.string.attachment));
            String startDate = result.getStartDateTime();
            String endDate = result.getEndDateTime();
            String createDate = result.getCreatedDate();
            ContentValues contentValuesEvent = new ContentValues();
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_EVENT_ID, result.getId());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_TITLE, result.getTitle());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_DESCRIPTION, result.getDescription());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_LOCATION, result.getLocation());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_START_DATE, startDate);
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_END_DATE, endDate);
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_TYPE_ID, result.getTypeId());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_TYPE, result.getType());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_ALL_DAY, result.getAllDay());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_FEATURE_IMAGE, result.getFeatureImage());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_BANNER, result.getBanner());
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_CREATE_DATE, createDate);
            contentValuesEvent.put(DBHelper.EVENT_COLUMN_NEW_CREATE, String.valueOf(result.getNewCreate()));
            if (getEventData(result.getId(), context) == 0) {
                context.getContentResolver().insert(EventProvider.CONTENT_URI, contentValuesEvent);
            } else {
                context.getContentResolver().update(EventProvider.CONTENT_URI, contentValuesEvent,
                        DBHelper.EVENT_COLUMN_EVENT_ID + "=?", new String[]{result.getId()});
            }
            ContentValues[] contentValuesesAttachment = new ContentValues[result.getAttachments().size()];
            List<Attachment> arrayList = result.getAttachments();
            for (int j = 0; j < arrayList.size(); j++) {
                String uploadDate = arrayList.get(j).getUploadDate();
                ContentValues contentValues1 = new ContentValues();
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_ATTACHMENT_ID, arrayList.get(j).getId());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_EVENT_ID, arrayList.get(j).getEventId());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_FILE_SIZE, arrayList.get(j).getFileSize());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_TITLE, arrayList.get(j).getTitle());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_DESCRIPTION, arrayList.get(j).getDescription());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_ATTACHMENT, arrayList.get(j).getAttachment());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_UPLOAD_BY, arrayList.get(j).getUploadBy());
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_UPLOAD_DATE, uploadDate);
                String[] split = arrayList.get(j).getAttachment().split("/");
                contentValues1.put(DBHelper.ATTACHMENT_COLUMN_PATH, file.getAbsolutePath() + "/" + split[split.length - 1]);
                contentValuesesAttachment[j] = contentValues1;
            }
            boolean isEmpty = true;
            for (Object ob : contentValuesesAttachment) {
                if (ob != null) {
                    isEmpty = false;
                }
            }
            long rowsInserted = 0;
            if (!isEmpty) {
                rowsInserted = context.getContentResolver().bulkInsert(AttachmentProvider.CONTENT_URI, contentValuesesAttachment);
                Log.i(TAG, String.valueOf(rowsInserted));
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        } finally {
            if (Utils.getStatus(Utils.EVENT_READ, context)) {
                com.applab.wcircle_pro.Event.HttpHelper.getEventGsonReading(context, Integer.valueOf(result.getId()), TAG);
            }
            Intent intent = new Intent(context, EventDetailsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("id", Integer.valueOf(result.getId()));
            context.startActivity(intent);
        }

    }

    //endregion
    //region calendar region
    public static boolean isAttendeesExists(Context context, Object calendarId, Object employeeId) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(AttendeesProvider.CONTENT_URI, null,
                    DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                    new String[]{String.valueOf(calendarId), String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void getCalendarGson(Context context, String id, String TAG) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Calendar> mGsonRequest = new GsonRequest<Calendar>(
                Request.Method.GET,
                Utils.API_URL + "api/Calendar/" + id,
                Calendar.class,
                headers,
                responseCalendarListener(context, TAG, id),
                errorListener(context, "Calendar")) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static void insertCalendar(Calendar response, Context context, String TAG) {
        try {
            String calendarTo = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, response.getCalendarTo());
            String calendarFrom = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, response.getCalendarFrom());
            String reminderDate = response.getReminder() != null ? Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, response.getReminder()) : null;
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.CALENDAR_COLUMN_CALENDAR_ID, response.getId());
            contentValues.put(DBHelper.CALENDAR_COLUMN_TYPE, response.getType());
            contentValues.put(DBHelper.CALENDAR_COLUMN_TITLE, response.getTitle());
            contentValues.put(DBHelper.CALENDAR_COLUMN_ALL_DAY, String.valueOf(response.getAllDay()));
            contentValues.put(DBHelper.CALENDAR_COLUMN_START_DATE, calendarFrom);
            contentValues.put(DBHelper.CALENDAR_COLUMN_END_DATE, calendarTo);
            contentValues.put(DBHelper.CALENDAR_COLUMN_LOCATION, response.getLocation());
            contentValues.put(DBHelper.CALENDAR_COLUMN_DESCRIPTION, response.getDescription());
            contentValues.put(DBHelper.CALENDAR_COLUMN_REMINDER, reminderDate);
            contentValues.put(DBHelper.CALENDAR_COLUMN_STATUS, response.getStatus());
            contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_ID, response.getCreateById());
            contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_NAME, response.getCreateByName());
            contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_ID, response.getUpdateById());
            contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_NAME, response.getUpdateByName());
            contentValues.put(DBHelper.CALENDAR_COLUMN_NEW_CREATE, String.valueOf(response.getNewCreate()));
            if (MonthFragment.isCalendarExists(response.getId(), context)) {
                context.getContentResolver().update(CalendarProvider.CONTENT_URI, contentValues,
                        DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(response.getId())});
            } else {
                context.getContentResolver().insert(CalendarProvider.CONTENT_URI, contentValues);
            }
            if (response.getAttendees() != null) {
                for (int j = 0; j < response.getAttendees().size(); j++) {
                    String reminder = response.getAttendees().get(j).getReminder() != null ?
                            Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, response.getAttendees().get(j).getReminder()) : null;
                    ContentValues contentValuesAttendees = new ContentValues();
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SUBMIT, 1);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SELECT, 1);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_NAME, response.getAttendees().get(j).getEmployeyName());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID, response.getAttendees().get(j).getEmployeeId());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_CALENDAR_ID, response.getId());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_REMINDER, reminder);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_STATUS, response.getAttendees().get(j).getStatus());
                    if (isAttendeesExists(context, response.getId(), response.getAttendees().get(j).getEmployeeId())) {
                        context.getContentResolver().update(AttendeesProvider.CONTENT_URI, contentValuesAttendees,
                                DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                                new String[]{String.valueOf(response.getId()), String.valueOf(response.getAttendees().get(j).getEmployeeId())});
                    } else {
                        context.getContentResolver().insert(AttendeesProvider.CONTENT_URI, contentValuesAttendees);
                    }
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (Utils.getStatus(Utils.CALENDAR_READ, context)) {
                com.applab.wcircle_pro.Calendar.db.HttpHelper.getCalendarGsonReading(context, response.getId(), TAG);
            }
            Intent intent = new Intent(context, CalendarEventDetailsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("calendar", response);
            if (response.getAttendees() != null) {
                intent.putParcelableArrayListExtra("attendees", new ArrayList<Parcelable>(response.getAttendees()));
            } else {
                intent.putParcelableArrayListExtra("attendees", new ArrayList<Parcelable>());
            }
            intent.putExtra("page", 0);
            context.startActivity(intent);
        }
    }

    public static Response.Listener<Calendar> responseCalendarListener(final Context context, final String TAG, final String id) {
        return new Response.Listener<Calendar>() {
            @Override
            public void onResponse(Calendar response) {
                if (!String.valueOf(response.getId()).equals(id)) {
                    context.getContentResolver().delete(CalendarProvider.CONTENT_URI, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(id)});
                    context.getContentResolver().delete(AttendeesProvider.CONTENT_URI, DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(id)});
                    Intent intent = new Intent(context, CalendarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    insertCalendar(response, context, TAG);
                }
            }
        };
    }

    //endregion
    //region leave region
    public static void getLeaveGson(Context context, String id, String TAG) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String url = Utils.API_URL + "api/Leave/?id=" + Utils.encode(id);
        GsonRequest<Leave> mGsonRequest = new GsonRequest<Leave>(
                Request.Method.GET,
                url,
                Leave.class,
                headers,
                responseListener(context, TAG, id),
                errorListener(context, "Leave")) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static boolean isLeaveExists(Object id, Context context) {
        Cursor cursor = null;
        boolean isExists = false;
        try {
            cursor = context.getContentResolver().query(LeaveProvider.CONTENT_URI,
                    null, DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void insertLeave(Leave response, Context context, String TAG) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_ID, response.getId());
            contentValues.put(DBHelper.LEAVE_COLUMN_NAME, response.getName());
            contentValues.put(DBHelper.LEAVE_COLUMN_DEPARTMENT, response.getDepartment());
            contentValues.put(DBHelper.LEAVE_COLUMN_EMPLOYEE_ID, response.getEmployeeId());
            contentValues.put(DBHelper.LEAVE_COLUMN_POSITION, response.getPosition());
            contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR, response.getSuperior());
            contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR_ID, response.getSuperiorId());
            contentValues.put(DBHelper.LEAVE_COLUMN_TYPE_CODE, response.getTypeCode());
            contentValues.put(DBHelper.LEAVE_COLUMN_TYPE, response.getType());
            contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, response.getRemarks());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_DAYS, response.getLeaveDays());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_FROM, response.getLeaveFrom());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_TO, response.getLeaveTo());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, response.getStatus());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_REMARKS, response.getStatusRemark());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_BY, response.getStatusUpdateBy());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_ON, response.getStatusUpdateOn());
            contentValues.put(DBHelper.LEAVE_COLUMN_LAST_UPDATE, response.getLastUpdate());
            contentValues.put(DBHelper.LEAVE_COLUMN_NEW_CREATE, String.valueOf(response.getNewCreate()));
            contentValues.put(DBHelper.LEAVE_COLUMN_CREATE_DATE, response.getCreateDate());
            if (response.getSuperiorId().equals(Utils.getProfile(context).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
            } else if (!response.getSuperiorId().equals(Utils.getProfile(context).getId()) && !response.getEmployeeId().equals(Utils.getProfile(context).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
            } else if (!response.getSuperiorId().equals(Utils.getProfile(context).getId()) && response.getEmployeeId().equals(Utils.getProfile(context).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
            }
            if (isLeaveExists(response.getId(), context)) {
                context.getContentResolver().update(LeaveProvider.CONTENT_URI, contentValues, DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?",
                        new String[]{String.valueOf(response.getId())});
            } else {
                context.getContentResolver().insert(LeaveProvider.CONTENT_URI, contentValues);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Edit Application Error :" + ex.toString());
        } finally {
            if (Utils.getStatus(Utils.LEAVE_READ, context)) {
                com.applab.wcircle_pro.Leave.HttpHelper.getGsonReading(context, response.getId(), TAG);
            }
            Intent intent = new Intent(context, EditApplicationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("leave", response);
            intent.putExtra("isCancelled", response.getStatus().equals("cancelled"));
            if (response.getSuperiorId().equals(Utils.getProfile(context).getId())) {
                intent.putExtra("isEmployeeLeave", true);
            } else if (!response.getSuperiorId().equals(Utils.getProfile(context).getId()) && !response.getEmployeeId().equals(Utils.getProfile(context).getId())) {
                intent.putExtra("isEmployeeLeave", true);
                intent.putExtra("isBoss", true);
            } else if (!response.getSuperiorId().equals(Utils.getProfile(context).getId()) && response.getEmployeeId().equals(Utils.getProfile(context).getId())) {
                intent.putExtra("isEmployeeLeave", false);
                intent.putExtra("isBoss", false);
            }
            intent.putExtra("page", 0);
            context.startActivity(intent);
        }
    }

    public static Response.Listener<Leave> responseListener(final Context context, final String TAG, final String id) {
        return new Response.Listener<Leave>() {
            @Override
            public void onResponse(Leave response) {
                if (response == null) {
                    context.getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(id)});
                    Intent intent = new Intent(context, LeaveActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    if (!response.getId().equals(Integer.valueOf(id))) {
                        context.getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(id)});
                        Intent intent = new Intent(context, LeaveActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(new Intent(context, LeaveActivity.class));
                    } else {
                        insertLeave(response, context, TAG);
                    }
                }
            }
        };
    }
    //endregion

    public static Response.ErrorListener errorListener(final Context context, final String module) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(context, error);
                if (module.equals("Calendar")) {
                    Intent intent = new Intent(context, CalendarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(new Intent(context, CalendarActivity.class));
                } else if (module.equals("Leave")) {
                    Intent intent = new Intent(context, LeaveActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(new Intent(context, LeaveActivity.class));
                } else if (module.equals("News")) {
                    Intent intent = new Intent(context, NewsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(new Intent(context, NewsActivity.class));
                } else if (module.equals("Event")) {
                    Intent intent = new Intent(context, EventActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(new Intent(context, EventActivity.class));
                } else if (module.equals("Media")) {
                    Intent intent = new Intent(context, GalleryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(new Intent(context, GalleryActivity.class));
                } else {
                    Intent intent = new Intent(context, DashboardActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(new Intent(context, DashboardActivity.class));
                }
            }
        };
    }

    //region Error Setting
    public static void setMessageSetting(Context context, String TAG) {
        GsonRequest<Message[]> mGsonRequest = new GsonRequest<Message[]>(
                Request.Method.GET,
                Utils.API_URL + "api/System/Message",
                Message[].class,
                null,
                responseListenerMessage(context, TAG),
                errorListenerMessage(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Message[]> responseListenerMessage(final Context context, final String TAG) {
        return new Response.Listener<Message[]>() {
            @Override
            public void onResponse(Message[] response) {
                context.getContentResolver().delete(MessageProvider.CONTENT_URI, null, null);
                insertMessageSetting(context, response, TAG);
            }
        };
    }

    public static void insertMessageSetting(Context contexts, Message[] result, String TAG) {
        DBHelper helper = new DBHelper(contexts);
        ArrayList<Message> arrayList = new ArrayList<>(Arrays.asList(result));
        try {
            ContentValues[] contentValueses = new ContentValues[arrayList.size()];
            for (int i = 0; i < arrayList.size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.MESSAGE_COLUMN_ID, arrayList.get(i).getId());
                contentValues.put(DBHelper.MESSAGE_COLUMN_CODE, arrayList.get(i).getCode());
                contentValues.put(DBHelper.MESSAGE_COLUMN_MESSAGE, arrayList.get(i).getMessage());
                String createDate = arrayList.get(i).getCreatedDate();
                createDate = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT_PRINT, createDate);
                contentValues.put(DBHelper.MESSAGE_COLUMN_CREATED_DATE, createDate);
                String updateDate = arrayList.get(i).getUpdatedDate();
                updateDate = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT_PRINT, updateDate);
                contentValues.put(DBHelper.MESSAGE_COLUMN_UPDATED_DATE, updateDate);
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                contexts.getContentResolver().bulkInsert(MessageProvider.CONTENT_URI, contentValueses);
            }

        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Message Error :" + ex.toString());
        }
    }

    public static Response.ErrorListener errorListenerMessage(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Utils.clearPreviousActivity((Activity) context);
            }
        };
    }
    //endregion

    //region Master Setting
    public static void setMasterSetting(Context context, String TAG) {
        GsonRequest<MasterSetting[]> mGsonRequest = new GsonRequest<MasterSetting[]>(
                Request.Method.GET,
                Utils.API_URL + "api/System/Setting",
                MasterSetting[].class,
                null,
                responseListenerMasterSystem(context, TAG),
                errorListenerMasterSystem(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<MasterSetting[]> responseListenerMasterSystem(final Context context, final String TAG) {
        return new Response.Listener<MasterSetting[]>() {
            @Override
            public void onResponse(MasterSetting[] response) {
                context.getContentResolver().delete(MasterSettingProvider.CONTENT_URI, null, null);
                insertMasterSetting(context, response, TAG);
            }
        };
    }

    public static void insertMasterSetting(Context context, MasterSetting[] result, final String TAG) {
        DBHelper helper = new DBHelper(context);
        ArrayList<MasterSetting> arrayList = new ArrayList<>(Arrays.asList(result));
        try {
            ContentValues[] contentValueses = new ContentValues[arrayList.size()];
            for (int i = 0; i < arrayList.size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.MASTER_SETTING_COLUMN_ID, arrayList.get(i).getId());
                contentValues.put(DBHelper.MASTER_SETTING_COLUMN_CODE, arrayList.get(i).getCode());
                contentValues.put(DBHelper.MASTER_SETTING_COLUMN_STATUS, String.valueOf(arrayList.get(i).getStatus()));
                contentValues.put(DBHelper.MASTER_SETTING_COLUMN_CREATED_DATE, arrayList.get(i).getCreatedDate());
                contentValues.put(DBHelper.MASTER_SETTING_COLUMN_UPDATED_DATE, arrayList.get(i).getUpdatedDate());
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(MasterSettingProvider.CONTENT_URI, contentValueses);
            }

        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Message Error :" + ex.toString());
        }
    }

    public static Response.ErrorListener errorListenerMasterSystem(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Utils.clearPreviousActivity((Activity) context);
            }
        };
    }
    //endregion

    //region System Check
    public static void setSystemSetting(Context context, String TAG) {
        GsonRequest<App> mGsonRequest = new GsonRequest<App>(
                Request.Method.GET,
                Utils.API_URL + "api/System/Check/1",
                App.class,
                null,
                responseListenerSystem(context, TAG),
                errorListenerSetting(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<App> responseListenerSystem(final Context context, final String TAG) {
        return new Response.Listener<App>() {
            @Override
            public void onResponse(App response) {
                insertSystem(context, response, TAG);
            }
        };
    }

    public static void insertSystem(Context context, App response, String TAG) {
        try {
            if (response != null) {
                if (getSystemCheck(context) != null) {
                    Date d1 = Utils.setDate(Utils.DATE_FORMAT, response.getMessageLastUpdate());
                    Date d2 = Utils.setDate(Utils.DATE_FORMAT, response.getSettingLastUpdate());
                    Date d3 = Utils.setDate(Utils.DATE_FORMAT, getSystemCheck(context).get(0));
                    Date d4 = Utils.setDate(Utils.DATE_FORMAT, getSystemCheck(context).get(1));
                    if (d3.before(d1)) {
                        setMessageSetting(context, TAG);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DBHelper.SYSTEM_COLUMN_MESSAGE_LAST_UPDATE, response.getMessageLastUpdate());
                        context.getContentResolver().update(SystemProvider.CONTENT_URI, contentValues, DBHelper.SYSTEM_COLUMN_ID + "=?", new String[]{"1"});
                    }
                    if (d4.before(d2)) {
                        setMasterSetting(context, TAG);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DBHelper.SYSTEM_COLUMN_SETTING_LAST_UPDATE, response.getSettingLastUpdate());
                        context.getContentResolver().update(SystemProvider.CONTENT_URI, contentValues, DBHelper.SYSTEM_COLUMN_ID + "=?", new String[]{"1"});
                    }
                } else {
                    setMessageSetting(context, TAG);
                    setMasterSetting(context, TAG);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.SYSTEM_COLUMN_ID, 1);
                    contentValues.put(DBHelper.SYSTEM_COLUMN_MESSAGE_LAST_UPDATE, response.getMessageLastUpdate());
                    contentValues.put(DBHelper.SYSTEM_COLUMN_SETTING_LAST_UPDATE, response.getSettingLastUpdate());
                    context.getContentResolver().insert(SystemProvider.CONTENT_URI, contentValues);
                }
            } else {
                setMessageSetting(context, TAG);
                setMasterSetting(context, TAG);
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }

    }

    public static ArrayList<String> getSystemCheck(Context context) {
        ArrayList<String> arrayList = new ArrayList<>();
        Cursor cursor = null;
        try {
            String[] projection = {DBHelper.SYSTEM_COLUMN_MESSAGE_LAST_UPDATE, DBHelper.SYSTEM_COLUMN_SETTING_LAST_UPDATE};
            cursor = context.getContentResolver().query(SystemProvider.CONTENT_URI, projection, null, null, DBHelper.SYSTEM_COLUMN_ID + " DESC LIMIT 1 ");
            if (cursor != null && cursor.moveToFirst()) {
                arrayList.add(cursor.getString(cursor.getColumnIndex(DBHelper.SYSTEM_COLUMN_MESSAGE_LAST_UPDATE)));
                arrayList.add(cursor.getString(cursor.getColumnIndex(DBHelper.SYSTEM_COLUMN_SETTING_LAST_UPDATE)));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
            arrayList = null;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (arrayList != null) {
                if (arrayList.size() == 0) {
                    arrayList = null;
                }
            }
        }
        return arrayList;
    }

    public static Response.ErrorListener errorListenerSetting(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Utils.clearPreviousActivity((Activity) context);
            }
        };
    }
    //endregion
}
