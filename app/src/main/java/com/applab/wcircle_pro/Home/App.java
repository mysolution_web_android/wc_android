package com.applab.wcircle_pro.Home;

import com.google.gson.annotations.Expose;

public class App {

    @Expose
    private String MessageLastUpdate;
    @Expose
    private String SettingLastUpdate;
    @Expose
    private String Version;
    @Expose
    private String DownloadPath;
    @Expose
    private String CreateDate;
    @Expose
    private Integer ReleaseNo;
    @Expose
    private Boolean MaintenanceMode;

    public Boolean getMaintenanceMode() {
        return MaintenanceMode;
    }

    public void setMaintenanceMode(Boolean maintenanceMode) {
        MaintenanceMode = maintenanceMode;
    }

    public Integer getReleaseNo() {
        return ReleaseNo;
    }

    public void setReleaseNo(Integer releaseNo) {
        ReleaseNo = releaseNo;
    }

    /**
     * @return The MessageLastUpdate
     */
    public String getMessageLastUpdate() {
        return MessageLastUpdate;
    }

    /**
     * @param MessageLastUpdate The MessageLastUpdate
     */
    public void setMessageLastUpdate(String MessageLastUpdate) {
        this.MessageLastUpdate = MessageLastUpdate;
    }

    /**
     * @return The SettingLastUpdate
     */
    public String getSettingLastUpdate() {
        return SettingLastUpdate;
    }

    /**
     * @param SettingLastUpdate The SettingLastUpdate
     */
    public void setSettingLastUpdate(String SettingLastUpdate) {
        this.SettingLastUpdate = SettingLastUpdate;
    }

    /**
     * @return The Version
     */
    public String getVersion() {
        return Version;
    }

    /**
     * @param Version The Version
     */
    public void setVersion(String Version) {
        this.Version = Version;
    }

    /**
     * @return The DownloadPath
     */
    public String getDownloadPath() {
        return DownloadPath;
    }

    /**
     * @param DownloadPath The DownloadPath
     */
    public void setDownloadPath(String DownloadPath) {
        this.DownloadPath = DownloadPath;
    }

    /**
     * @return The CreateDate
     */
    public String getCreateDate() {
        return CreateDate;
    }

    /**
     * @param CreateDate The CreateDate
     */
    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

}
