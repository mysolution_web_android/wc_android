package com.applab.wcircle_pro.Calendar.decorater;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.DisplayMetrics;

import com.applab.wcircle_pro.Calendar.NewCalendar;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.spans.ImageDotSpan;

/**
 * Created by user on 22-Mar-16.
 */
public class CalenderDecorator implements DayViewDecorator {
    private NewCalendar mCalendar = null;
    private String mDateFormat = "yyyy-MM-dd";
    private Context mContext;

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        if (mCalendar != null) {
            return Utils.setCalendarDate(mDateFormat, day.getDate()).equals(Utils.setCalendarDate(mDateFormat, Utils.setCalendarDate(mDateFormat, mCalendar.getCalendarFrom())));
        } else {
            return false;
        }
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.addSpan(new ImageDotSpan(getBitmap(mCalendar.getGreen(), mCalendar.getPurple(), mCalendar.getRed(), mCalendar.getYellow()), 4.7f));
    }

    public void setCalendar(NewCalendar calendar, Context context) {
        mContext = context;
        this.mCalendar = calendar;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    private Bitmap getBitmap(boolean isGreen, boolean isPurple, boolean isRed, boolean isYellow) {
        Bitmap bitmap = null;
        DisplayMetrics displayMetrics;
        displayMetrics = mContext.getResources().getDisplayMetrics();
        int width = ((displayMetrics.widthPixels) / 7) / 4 - 7;
        int height = width;
        if (isGreen && isPurple && isRed && isYellow) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_rypg), height, width * 4);
        } else if (isGreen && isPurple && isRed) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_rgp), height, width * 3);
        } else if (isGreen && isPurple && isYellow) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_ygp), height, width * 3);
        } else if (isGreen && isYellow && isRed) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_ryg), height, width * 3);
        } else if (isYellow && isPurple && isRed) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_ryp), height, width * 3);
        } else if (isGreen && isPurple) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_pg), height, width * 2);
        } else if (isGreen && isRed) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_rg), height, width * 2);
        } else if (isGreen && isYellow) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_yg), height, width * 2);
        } else if (isPurple && isYellow) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_yp), height, width * 2);
        } else if (isPurple && isRed) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_rp), height, width * 2);
        } else if (isRed && isYellow) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_ry), height, width * 2);
        } else if (isRed) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_r), height, width);
        } else if (isGreen) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_g), height, width);
        } else if (isPurple) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_p), height, width);
        } else if (isYellow) {
            bitmap = getResizedBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_y), height, width);
        }
        return bitmap;
    }
}
