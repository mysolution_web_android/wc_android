package com.applab.wcircle_pro.Calendar;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.*;
import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.Employee.DepartmentEmployeeProvider;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;

public class DepartmentFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView recyclerView;
    private DepartmentAdapter adapter;
    private EditText ediSearch;
    private Intent mIntent;
    private LinearLayout btnCancel, btnSubmit;
    private ArrayList<Integer> addedId = new ArrayList<>();
    private ArrayList<NewEmployee> addedEmployee = new ArrayList<>();
    private String TAG = "DEPARTMENT FRAGMENT";
    private int pageNo = 1;
    private LinearLayoutManager linearLayoutManager;
    private String selection = null;
    private String[] selectionArgs = null;
    private LoaderManager.LoaderCallbacks callbacks;
    private TextView txtError;
    private Cursor cursor;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        selection = DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "!=?";
        selectionArgs = new String[]{String.valueOf(Utils.getProfile(getActivity()).getId())};
        View layout = inflater.inflate(R.layout.fragment_department, container, false);
        mIntent = getActivity().getIntent();
        if (mIntent.getParcelableArrayListExtra("addedEmployee") != null) {
            addedEmployee = mIntent.getParcelableArrayListExtra("addedEmployee");
            for (int i = 0; i < addedEmployee.size(); i++) {
                NewEmployee employee = addedEmployee.get(i);
                addedId.add(employee.getId());
            }
        }
        ediSearch = (EditText) layout.findViewById(R.id.ediSearch);
        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        adapter = new DepartmentAdapter(getActivity(), cursor, addedEmployee, addedId);
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        setTouchListener();

        btnCancel = (LinearLayout) layout.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        btnSubmit = (LinearLayout) layout.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(btnSubmitOnClickListener);
        txtError = (TextView) layout.findViewById(R.id.txtError);
        searchWatcher();
        HttpHelper.getDepartmentGson(pageNo, false, getActivity(), null, TAG, txtError);
        callbacks = this;
        getActivity().getSupportLoaderManager().initLoader(856, null, this);
        return layout;
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (totalItemCount > 1) {
                if (lastVisibleItem >= totalItemCount - 1) {
                    pageNo++;
                    HttpHelper.getDepartmentGson(pageNo, false, getActivity(), null, TAG, txtError);
                }
            }
        }
    };

    //region on click region
    private LinearLayout.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                ArrayList<ContentValues> contentValueses = new ArrayList<ContentValues>(addedId.size());
                Uri uri = TempAttendeesProvider.CONTENT_URI;
                getActivity().getContentResolver().delete(uri, DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(mIntent.getIntExtra("calendarId", 0))});
                for (int i = 0; i < addedEmployee.size(); i++) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID, mIntent.getIntExtra("calendarId", 0));
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_EMPLOYEE_ID, addedEmployee.get(i).getId());
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_NAME, addedEmployee.get(i).getName());
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_IS_SELECT, 1);
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_IS_SUBMIT, 0);
                    contentValueses.add(contentValues);
                }
                boolean isEmpty = true;
                for (Object ob : contentValueses) {
                    if (ob != null) {
                        isEmpty = false;
                    }
                }
                if (!isEmpty) {
                    ContentValues[] arr = new ContentValues[contentValueses.size()];
                    arr = contentValueses.toArray(arr);
                    getActivity().getContentResolver().bulkInsert(TempAttendeesProvider.CONTENT_URI, arr);
                }
            } catch (Exception ex) {
                Log.i("ERROR", "Content Provider Error :" + ex.toString());
            } finally {
                Intent intent = new Intent(AddContactActivity.ACTION);
                intent.putExtra("isSubmit", true);
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getActivity());
                mgr.sendBroadcast(intent);
                getActivity().finish();
            }
        }
    };

    private LinearLayout.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(AddContactActivity.ACTION);
            intent.putExtra("isCancel", true);
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getActivity());
            mgr.sendBroadcast(intent);
            getActivity().finish();
        }
    };
    //endregion

    //region touch listener
    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    //endregion

    //region search watcher
    private void searchWatcher() {
        ediSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    pageNo = 1;
                    HttpHelper.getDepartmentGson(pageNo, true, getActivity(), ediSearch.getText().toString(), TAG, txtError);
                    if (ediSearch.getText().toString().length() > 0) {
                        selection = DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_NAME + " LIKE ? OR " + DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DEPARTMENT + " LIKE ? AND " + DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "!=?";
                        selectionArgs = new String[]{"%" + ediSearch.getText().toString() + "%", "%" + ediSearch.getText().toString() + "%", Utils.getProfile(getActivity()).getId().toString()};
                        getActivity().getSupportLoaderManager().restartLoader(856, null, callbacks);
                    } else {
                        selection = DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "!=?";
                        selectionArgs = new String[]{Utils.getProfile(getActivity()).getId().toString()};
                        getActivity().getSupportLoaderManager().restartLoader(856, null, callbacks);
                    }
                    InputMethodManager imm = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }
    //endregion

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == getActivity().RESULT_OK && requestCode == 123) {
            if (intent.getBooleanExtra("isDepartment", false)) {
                addedEmployee = new ArrayList<>();
                addedId = new ArrayList<>();
                addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                for (int i = 0; i < addedEmployee.size(); i++) {
                    addedId.add(addedEmployee.get(i).getId());
                }
                adapter.swapAddedEmployee(addedId, addedEmployee);
            }
        }
    }

    //region broadcast region
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(AddContactActivity.ACTION)) {
                if (intent.getBooleanExtra("isDepartment", false)) {
                    addedEmployee = new ArrayList<>();
                    addedId = new ArrayList<>();
                    addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                    for (int i = 0; i < addedEmployee.size(); i++) {
                        addedId.add(addedEmployee.get(i).getId());
                    }
                    adapter.swapAddedEmployee(addedId, addedEmployee);
                } else if (intent.getBooleanExtra("isDepartmentChange", false)) {
                    addedEmployee = new ArrayList<>();
                    addedId = new ArrayList<>();
                    addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                    for (int i = 0; i < addedEmployee.size(); i++) {
                        addedId.add(addedEmployee.get(i).getId());
                    }
                }
            }
        }
    };
    //endregion

    //region pause and resume
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(AddContactActivity.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 856) {
            return new CursorLoader(getActivity(), DepartmentEmployeeProvider.CONTENT_URI, null, selection, selectionArgs, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 856) {
            if (data != null) {
                cursor = data;
                adapter.swapCursor(cursor);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
    //endregion
}