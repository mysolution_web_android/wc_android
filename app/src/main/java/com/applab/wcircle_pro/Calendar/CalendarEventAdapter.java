package com.applab.wcircle_pro.Calendar;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by admin on 26/8/2015.
 */

public class CalendarEventAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursorCalendar, cursorAttendee;
    private Context context;
    private String TAG = "REINVITE";
    private FragmentManager fragmentManager;

    public CalendarEventAdapter(Context context, Cursor cursorCalendar, Cursor cursorAttendee, FragmentManager fragmentManager) {
        this.inflater = LayoutInflater.from(context);
        this.cursorCalendar = cursorCalendar;
        this.cursorAttendee = cursorAttendee;
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    class CalendarDetails extends RecyclerView.ViewHolder {
        TextView txtLocation, txtFromDate, txtToDate, txtTitle, txtAllDay, txtDesc, txtStatus, txtCreator;
        LinearLayout lvStatus, lvAttendees;

        public CalendarDetails(View itemView) {
            super(itemView);
            txtLocation = (TextView) itemView.findViewById(R.id.txtLocation);
            txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);
            txtFromDate = (TextView) itemView.findViewById(R.id.txtFromDate);
            txtToDate = (TextView) itemView.findViewById(R.id.txtToDate);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtAllDay = (TextView) itemView.findViewById(R.id.txtAllDay);
            txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            txtCreator = (TextView) itemView.findViewById(R.id.txtCreator);
            lvStatus = (LinearLayout) itemView.findViewById(R.id.lvStatus);
            lvAttendees = (LinearLayout) itemView.findViewById(R.id.lvAttendees);
        }
    }

    class AttendeesList extends RecyclerView.ViewHolder {
        ImageView img, imgDelete;
        TextView txtName, txtReinvite;
        LinearLayout lv;

        public AttendeesList(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            imgDelete = (ImageView) itemView.findViewById(R.id.delete);
            txtReinvite = (TextView) itemView.findViewById(R.id.txtReinvite);
            lv = (LinearLayout) itemView.findViewById(R.id.lv);
        }
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 1;
        if (position == 0) {
            viewType = 0;
        }
        return viewType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = inflater.inflate(R.layout.custom_calendar_event_details, parent, false);
            return new CalendarDetails(view);
        } else {
            View view = inflater.inflate(R.layout.custom_edit_calendar_row, parent, false);
            return new AttendeesList(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                if (position == 0) {
                    if (cursorCalendar != null) {
                        if (cursorCalendar.getCount() > 0) {
                            if (!cursorCalendar.isClosed()) {
                                Calendar data = Calendar.getCalendar(cursorCalendar, position);
                                if (data.getStatus() != null) {
                                    CalendarDetails calendarDetails = (CalendarDetails) holder;
                                    calendarDetails.txtTitle.setText(data.getTitle());
                                    if (isAttendeesExists(data.getId(), context)) {
                                        calendarDetails.lvAttendees.setVisibility(View.VISIBLE);
                                    } else {
                                        calendarDetails.lvAttendees.setVisibility(View.GONE);
                                    }
                                    if (data.getAllDay()) {
                                        calendarDetails.txtAllDay.setText(context.getResources().getString(R.string.yes));
                                        calendarDetails.txtFromDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy h:mm a", data.getCalendarFrom()));
                                        calendarDetails.txtToDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", data.getCalendarTo()));

                                    } else {
                                        calendarDetails.txtAllDay.setText(context.getResources().getString(R.string.no));
                                        calendarDetails.txtFromDate.setText(Utils.setDate(Utils.DATE_FORMAT,
                                                "dd MMM yyyy  h:mm a", data.getCalendarFrom()));
                                        calendarDetails.txtToDate.setText(Utils.setDate(Utils.DATE_FORMAT,
                                                "dd MMM yyyy  h:mm a", data.getCalendarTo()));
                                    }
                                    if (data.getStatus().equals("inactive")) {
                                        calendarDetails.lvStatus.setVisibility(View.VISIBLE);
                                        calendarDetails.txtStatus.setText(context.getString(R.string.cancelled));
                                    } else {
                                        calendarDetails.lvStatus.setVisibility(View.GONE);
                                    }
                                    calendarDetails.txtCreator.setText(data.getCreateByName());
                                    calendarDetails.txtDesc.setText(data.getDescription());
                                    calendarDetails.txtLocation.setText(data.getLocation());
                                }
                            }

                        }
                    }
                }
                break;
            case 1:
                if (position > 0) {
                    if (cursorAttendee != null) {
                        if (cursorAttendee.getCount() > 0) {
                            if (!cursorAttendee.isClosed()) {
                                if (cursorCalendar != null) {
                                    if (cursorCalendar.getCount() > 0) {
                                        if (!cursorCalendar.isClosed()) {
                                            AttendeesList attendeesList = (AttendeesList) holder;
                                            Attendee current = Attendee.getAttendee(cursorAttendee, position - 1);
                                            Calendar data = Calendar.getCalendar(cursorCalendar, 0);
                                            if (current != null) {
                                                attendeesList.lv.setTag(data);
                                                attendeesList.lv.setVisibility(View.VISIBLE);
                                                ((AttendeesList) holder).imgDelete.setVisibility(View.GONE);
                                                attendeesList.img.setVisibility(View.VISIBLE);
                                                attendeesList.txtReinvite.setVisibility(View.GONE);
                                                if (current.getStatus() != null) {
                                                    switch (current.getStatus()) {
                                                        case "attend":
                                                            attendeesList.img.setImageResource(R.mipmap.info_a);
                                                            break;
                                                        case "reject":
                                                            if (data.getCreateById().equals(String.valueOf(Utils.getProfile(context).getId()))) {
                                                                current.setCalendarId(data.getId());
                                                                attendeesList.txtReinvite.setTag(current);
                                                                attendeesList.txtReinvite.setVisibility(View.VISIBLE);
                                                            }
                                                            setReinviteOnClickListener(attendeesList.txtReinvite);
                                                            attendeesList.img.setImageResource(R.mipmap.info_r);
                                                            break;
                                                        case "pending":
                                                            attendeesList.img.setImageResource(R.mipmap.info_p_2);
                                                            break;
                                                        default:
                                                            attendeesList.img.setImageResource(R.mipmap.info_p_2);
                                                            break;
                                                    }
                                                } else {
                                                    attendeesList.img.setImageResource(R.mipmap.info_p_2);
                                                }
                                                if (data.getType().equals("B") || data.getType().equals("C")) {
                                                    attendeesList.img.setVisibility(View.INVISIBLE);
                                                }
                                                attendeesList.txtName.setText(current.getEmployeyName());
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                    break;
                }
        }
    }

    private static boolean isAttendeesExists(Object calendarId, Context context) {
        boolean isExist = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(AttendeesProvider.CONTENT_URI, null,
                    DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(calendarId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExist = cursor.getCount() > 0;
            }

        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExist;
    }

    private void setReinviteOnClickListener(final TextView invite) {
        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Attendee attendee = (Attendee) invite.getTag();
                CalendarRejectDialogFragment calendarRejectDialogFragment =
                        CalendarRejectDialogFragment.newInstance(attendee, context);
                calendarRejectDialogFragment.show(fragmentManager, "");
            }
        });
    }

    public void swapCalendar(Cursor cursor) {
        if (this.cursorCalendar == cursor) {
            return;
        }
        this.cursorCalendar = cursor;
        if (cursor != null) {
            android.os.Message msg = handler1.obtainMessage();
            handler1.handleMessage(msg);
        }
    }

    public void swapAttendee(Cursor cursor) {
        if (this.cursorAttendee == cursor) {
            return;
        }
        this.cursorAttendee = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            CalendarEventAdapter.this.notifyDataSetChanged();
        }
    };

    Handler handler1 = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            CalendarEventAdapter.this.notifyDataSetChanged();
        }
    };

    @Override
    public int getItemCount() {
        int total = 0;
        if (cursorCalendar != null) {
            if (cursorCalendar.getCount() > 0) {
                total += cursorCalendar.getCount();
                if (cursorAttendee != null) {
                    if (cursorAttendee.getCount() > 0) {
                        total += cursorAttendee.getCount();
                    }
                }
            }
        }
        if (cursorCalendar != null && cursorAttendee != null) {
            if (total > cursorCalendar.getCount() + cursorAttendee.getCount()) {
                total = cursorCalendar.getCount() + cursorAttendee.getCount();
            }
        }
        return total;
    }
}

