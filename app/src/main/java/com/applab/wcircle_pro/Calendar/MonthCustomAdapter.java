package com.applab.wcircle_pro.Calendar;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidGridAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import hirondelle.date4j.DateTime;

public class MonthCustomAdapter extends CaldroidGridAdapter {
    private ArrayList<NewCalendar> arrayList;
    private int dayHeight;

    public MonthCustomAdapter(Context context, int month, int year,
                              HashMap<String, Object> caldroidData,
                              HashMap<String, Object> extraData, int id, ArrayList<NewCalendar> arrayList) {
        super(context, month, year, caldroidData, extraData);
        this.arrayList = arrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cellView = convertView;
        MonthCustomViewHolder holder;

        // For reuse
        if (convertView == null) {
            cellView = inflater.inflate(R.layout.cardroid_custom_cell, null);
            holder = new MonthCustomViewHolder();
            holder.txtDay = (TextView) cellView.findViewById(R.id.txtDay);
            holder.redView = (View) cellView.findViewById(R.id.redView);
            holder.rl = (RelativeLayout) cellView.findViewById(R.id.rl);
            holder.custom_cell = (RelativeLayout) cellView.findViewById(R.id.custom_cell);
            cellView.setTag(holder);
        } else {
            holder = (MonthCustomViewHolder) convertView.getTag();
        }


        int topPadding = cellView.getPaddingTop();
        int leftPadding = cellView.getPaddingLeft();
        int bottomPadding = cellView.getPaddingBottom();
        int rightPadding = cellView.getPaddingRight();

        holder.txtDay.setTextColor(Color.BLACK);
        // Get dateTime of this cell
        DateTime dateTime = this.datetimeList.get(position);
        String date = dateTime.getYear().toString() + "-" + dateTime.getMonth().toString() + "-" + dateTime.getDay().toString();
        if (extraData.get("DATA") != null) {
            ArrayList<NewCalendar> arrayList = (ArrayList<NewCalendar>) extraData.get("DATA");
            ArrayList<String> arrCalendarFrom = (ArrayList<String>) extraData.get("DATA1");
            if (arrCalendarFrom.contains(date)) {
                int row = arrCalendarFrom.indexOf(date);
                if (row < arrayList.size()) {
                    holder.redView.setVisibility(View.VISIBLE);
                    setColor(arrayList.get(row).getGreen(), arrayList.get(row).getPurple(),
                            arrayList.get(row).getRed(), arrayList.get(row).getYellow(), holder.redView);
                }
            } else {
                holder.redView.setVisibility(View.GONE);
            }
        }
        if (extraData.get("SELECTED_DATE") != null) {
            if (extraData.get("SELECTED_DATE").equals(date)) {
                cellView.setBackgroundResource(R.color.blue);
            } else {
                cellView.setBackgroundResource(R.drawable.cell_bg);
            }
        }

        if (extraData.get("HEIGHT") != null) {
            int value = (Integer) extraData.get("HEIGHT");
            holder.custom_cell.setMinimumHeight(value / 6);
        } else {
            holder.custom_cell.setMinimumHeight(CaldroidFragment.getCalendarHeights() / 6);
        }

        Resources resources = context.getResources();

        // Set color of the dates in previous / next month
        if (dateTime.getMonth() != month) {
            holder.txtDay.setTextColor(ContextCompat.getColor(context, R.color.caldroid_darker_gray));
        }

        boolean shouldResetDiabledView = false;
        boolean shouldResetSelectedView = false;

        // Customize for disabled dates and date outside min/max dates
        if ((minDateTime != null && dateTime.lt(minDateTime))
                || (maxDateTime != null && dateTime.gt(maxDateTime))
                || (disableDates != null && disableDates.indexOf(dateTime) != -1)) {

            holder.txtDay.setTextColor(CaldroidFragment.disabledTextColor);
            if (CaldroidFragment.disabledBackgroundDrawable == -1) {
                cellView.setBackgroundResource(com.applab.wcircle_pro.R.drawable.disable_cell);
            } else {
                cellView.setBackgroundResource(CaldroidFragment.disabledBackgroundDrawable);
            }

            if (dateTime.equals(getToday())) {
                cellView.setBackgroundResource(com.applab.wcircle_pro.R.drawable.red_border_gray_bg);
            }

        } else {
            shouldResetDiabledView = true;
        }

        // Customize for selected dates
        if (selectedDates != null && selectedDates.indexOf(dateTime) != -1) {
            cellView.setBackgroundColor(ContextCompat.getColor(context, R.color.caldroid_sky_blue));
            holder.txtDay.setTextColor(Color.BLACK);
        } else {
            shouldResetSelectedView = true;
        }
        if (shouldResetDiabledView && shouldResetSelectedView) {
            // Customize for today
            if (dateTime.equals(getToday())) {
                holder.rl.setBackgroundResource(R.mipmap.info_circle);
            } else {
                holder.rl.setBackground(null);
            }
        }
        holder.txtDay.setText("" + dateTime.getDay());

        // Somehow after setBackgroundResource, the padding collapse.
        // This is to recover the padding
        cellView.setPadding(leftPadding, topPadding, rightPadding,
                bottomPadding);

        // Set custom color if required
        setCustomResources(dateTime, cellView, holder.txtDay);

        return cellView;
    }

    private void setColor(boolean isGreen, boolean isPurple, boolean isRed, boolean isYellow, View view) {
        DisplayMetrics displayMetrics;
        displayMetrics = context.getResources().getDisplayMetrics();
        int width = ((displayMetrics.widthPixels) / 7) / 4 - 7;
        view.getLayoutParams().height = width;
        if (isGreen && isPurple && isRed && isYellow) {
            view.setBackgroundResource(R.drawable.icon_rypg);
            view.getLayoutParams().width = width * 4;
        } else if (isGreen && isPurple && isRed) {
            view.setBackgroundResource(R.drawable.icon_rgp);
            view.getLayoutParams().width = width * 3;
        } else if (isGreen && isPurple && isYellow) {
            view.setBackgroundResource(R.drawable.icon_ygp);
            view.getLayoutParams().width = width * 3;
        } else if (isGreen && isYellow && isRed) {
            view.setBackgroundResource(R.drawable.icon_ryg);
            view.getLayoutParams().width = width * 3;
        } else if (isYellow && isPurple && isRed) {
            view.setBackgroundResource(R.drawable.icon_ryp);
            view.getLayoutParams().width = width * 3;
        } else if (isGreen && isPurple) {
            view.setBackgroundResource(R.drawable.icon_pg);
            view.getLayoutParams().width = width * 2;
        } else if (isGreen && isRed) {
            view.setBackgroundResource(R.drawable.icon_rg);
            view.getLayoutParams().width = width * 2;
        } else if (isGreen && isYellow) {
            view.setBackgroundResource(R.drawable.icon_yg);
            view.getLayoutParams().width = width * 2;
        } else if (isPurple && isYellow) {
            view.setBackgroundResource(R.drawable.icon_yp);
            view.getLayoutParams().width = width * 2;
        } else if (isPurple && isRed) {
            view.setBackgroundResource(R.drawable.icon_rp);
            view.getLayoutParams().width = width * 2;
        } else if (isRed && isYellow) {
            view.setBackgroundResource(R.drawable.icon_ry);
            view.getLayoutParams().width = width * 2;
        } else if (isRed) {
            view.setBackgroundResource(R.drawable.icon_r);
            view.getLayoutParams().width = width;
        } else if (isGreen) {
            view.setBackgroundResource(R.drawable.icon_g);
            view.getLayoutParams().width = width;
        } else if (isPurple) {
            view.setBackgroundResource(R.drawable.icon_p);
            view.getLayoutParams().width = width;
        } else if (isYellow) {
            view.setBackgroundResource(R.drawable.icon_y);
            view.getLayoutParams().width = width;
        }
    }

    static class MonthCustomViewHolder {
        TextView txtDay;
        View redView;
        RelativeLayout rl, custom_cell;
    }
}