package com.applab.wcircle_pro.Calendar;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.JsonObject;

import java.util.HashMap;

/**
 * Created by user on 27/10/2015.
 */
public class CalendarRejectDeleteDialogFragment extends DialogFragment {
    private int mId;
    private int mCondition = 1;
    private int mPage = 1;
    private TextView mTxtTitle, mTxtDetails;
    private LinearLayout mBtnYes, mBtnNo, mBtnExit;
    private ImageView mBtnCancel;
    private String TAG = "REJECT_DELETE";
    private static AlertDialog.Builder builder;
    private static Context mContext;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static CalendarRejectDeleteDialogFragment newInstance(int id, Context context, int condition, int page) {
        CalendarRejectDeleteDialogFragment frag = new CalendarRejectDeleteDialogFragment();
        Bundle args = new Bundle();
        args.putInt("id", id);
        args.putInt("page", page);
        args.putInt("condition", condition);
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mId = getArguments().getInt("id");
        mCondition = getArguments().getInt("condition");
        mPage = getArguments().getInt("page");

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_calendar_event_details, null);

        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtDetails = (TextView) v.findViewById(R.id.txtDetails);

        mBtnYes = (LinearLayout) v.findViewById(R.id.btnYes);
        mBtnNo = (LinearLayout) v.findViewById(R.id.btnNo);
        mBtnExit = (LinearLayout) v.findViewById(R.id.btnExit);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);

        mBtnYes.setOnClickListener(btnYesOnClickListener);
        mBtnNo.setOnClickListener(btnNoOnClickListener);
        mBtnExit.setOnClickListener(btnExitOnClickListener);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        if (mCondition == 1) {
            mTxtTitle.setText(getResources().getString(R.string.confirmation_delete));
            mTxtDetails.setText(Utils.getDialogMessage(mContext, Utils.CODE_CANCEL_CALENDAR, Utils.CANCEL_CALENDAR));
            mTxtDetails.setVisibility(View.GONE);
        } else if (mCondition == 2) {
            mTxtTitle.setText(getResources().getString(R.string.reject));
            mTxtDetails.setText(getResources().getString(R.string.reject_msg));
            mTxtDetails.setVisibility(View.VISIBLE);
        }

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnYesOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mCondition == 1) {
                deleteCalendar(true);
            } else if (mCondition == 2) {
                HttpHelper.rejectEvent(true, mContext, mId, TAG);
            }
            CalendarRejectDeleteDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnNoOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mCondition == 1) {
                //HttpHelper.deleteCalendar(mContext, mId, TAG);
            } else if (mCondition == 2) {
                HttpHelper.rejectEvent(false, mContext, mId, TAG);
            }
            Intent intent = new Intent(CalendarEventDetailsActivity.ACTION);
            intent.putExtra("isFail", true);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            CalendarRejectDeleteDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnExitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(CalendarEventDetailsActivity.ACTION);
            intent.putExtra("isFail",true);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            CalendarRejectDeleteDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(CalendarEventDetailsActivity.ACTION);
            intent.putExtra("isFail",true);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            CalendarRejectDeleteDialogFragment.this.getDialog().cancel();
        }
    };

    //region delete calendar region
    public void deleteCalendar(boolean remove) {
        final String token = Utils.getToken(mContext);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.DELETE,
                Utils.API_URL + "api/Calendar/" + mId + "?Remove=" + String.valueOf(remove),
                JsonObject.class,
                headers,
                responseDeleteListener(remove),
                errorDeleteListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<JsonObject> responseDeleteListener(final boolean remove) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                if (remove) {
                    mContext.getContentResolver().delete(CalendarProvider.CONTENT_URI, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(mId)});
                } else {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.CALENDAR_COLUMN_STATUS, "inactive");
                    mContext.getContentResolver().update(CalendarProvider.CONTENT_URI, contentValues, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(mId)});
                }
                Intent intent = new Intent(CalendarEventDetailsActivity.ACTION);
                intent.putExtra("isSuccess", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            }
        };
    }

    private Response.ErrorListener errorDeleteListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(mContext, error);
                Intent intent = new Intent(CalendarEventDetailsActivity.ACTION);
                intent.putExtra("isFail", true);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            }
        };
    }
    //endregion
}

