package com.applab.wcircle_pro.Calendar;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Calendar implements Parcelable {

    @SerializedName("Id")
    @Expose
    private int Id;
    @SerializedName("ShowDate")
    @Expose
    private String ShowDate;
    @SerializedName("Type")
    @Expose
    private String Type;
    @SerializedName("Title")
    @Expose
    private String Title;
    @SerializedName("AllDay")
    @Expose
    private Boolean AllDay;
    @SerializedName("CalendarFrom")
    @Expose
    private String CalendarFrom;
    @SerializedName("CalendarTo")
    @Expose
    private String CalendarTo;
    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("Description")
    @Expose
    private String Description;
    @SerializedName("Reminder")
    @Expose
    private String Reminder;
    @SerializedName("Status")
    @Expose
    private String Status;
    @SerializedName("CreateById")
    @Expose
    private String CreateById;
    @SerializedName("CreateByName")
    @Expose
    private String CreateByName;
    @SerializedName("UpdateById")
    @Expose
    private String UpdateById;
    @SerializedName("UpdateByName")
    @Expose
    private String UpdateByName;
    @SerializedName("Attendees")
    @Expose
    private List<Attendee> Attendees = new ArrayList<Attendee>();
    @Expose
    private boolean NewCreate = false;

    public String getShowDate() {
        return ShowDate;
    }

    public void setShowDate(String showDate) {
        ShowDate = showDate;
    }

    public boolean getNewCreate() {
        return NewCreate;
    }

    public void setNewCreate(boolean newCreate) {
        NewCreate = newCreate;
    }

    public Calendar() {
    }

    public Calendar(Parcel in) {
        readFromParcel(in);
    }

    /**
     * @return The Id
     */
    public int getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(int Id) {
        this.Id = Id;
    }

    /**
     * @return The Type
     */
    public String getType() {
        return Type;
    }

    /**
     * @param Type The Type
     */
    public void setType(String Type) {
        this.Type = Type;
    }

    /**
     * @return The Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param Title The Title
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * @return The AllDay
     */
    public Boolean getAllDay() {
        return AllDay;
    }

    /**
     * @param AllDay The AllDay
     */
    public void setAllDay(Boolean AllDay) {
        this.AllDay = AllDay;
    }

    /**
     * @return The CalendarFrom
     */
    public String getCalendarFrom() {
        return CalendarFrom;
    }

    /**
     * @param CalendarFrom The CalendarFrom
     */
    public void setCalendarFrom(String CalendarFrom) {
        this.CalendarFrom = CalendarFrom;
    }

    /**
     * @return The CalendarTo
     */
    public String getCalendarTo() {
        return CalendarTo;
    }

    /**
     * @param CalendarTo The CalendarTo
     */
    public void setCalendarTo(String CalendarTo) {
        this.CalendarTo = CalendarTo;
    }

    /**
     * @return The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     * @param Location The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     * @return The Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description The Description
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return The Reminder
     */
    public String getReminder() {
        return Reminder;
    }

    /**
     * @param Reminder The Reminder
     */
    public void setReminder(String Reminder) {
        this.Reminder = Reminder;
    }

    /**
     * @return The Status
     */
    public String getStatus() {
        return Status;
    }

    /**
     * @param Status The Status
     */
    public void setStatus(String Status) {
        this.Status = Status;
    }

    /**
     * @return The CreateById
     */
    public String getCreateById() {
        return CreateById;
    }

    /**
     * @param CreateById The CreateById
     */
    public void setCreateById(String CreateById) {
        this.CreateById = CreateById;
    }

    /**
     * @return The CreateByName
     */
    public String getCreateByName() {
        return CreateByName;
    }

    /**
     * @param CreateByName The CreateByName
     */
    public void setCreateByName(String CreateByName) {
        this.CreateByName = CreateByName;
    }

    /**
     * @return The UpdateById
     */
    public String getUpdateById() {
        return UpdateById;
    }

    /**
     * @param UpdateById The UpdateById
     */
    public void setUpdateById(String UpdateById) {
        this.UpdateById = UpdateById;
    }

    /**
     * @return The UpdateByName
     */
    public String getUpdateByName() {
        return UpdateByName;
    }

    /**
     * @param UpdateByName The UpdateByName
     */
    public void setUpdateByName(String UpdateByName) {
        this.UpdateByName = UpdateByName;
    }

    /**
     * @return The Attendees
     */
    public List<Attendee> getAttendees() {
        return Attendees;
    }

    /**
     * @param Attendees The Attendees
     */
    public void setAttendees(List<Attendee> Attendees) {
        this.Attendees = Attendees;
    }

    public static Calendar getCalendar(Cursor cursor, int position) {
        Calendar calendar = new Calendar();
        if (cursor != null) {
            cursor.moveToPosition(position);
            calendar.setReminder(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_REMINDER)));
            calendar.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_CALENDAR_ID)));
            calendar.setStatus(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_STATUS)));
            calendar.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_DESCRIPTION)));
            calendar.setAllDay(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_ALL_DAY))));
            calendar.setCalendarFrom(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_START_DATE)));
            calendar.setCalendarTo(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_END_DATE)));
            calendar.setCreateById(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_CREATE_BY_ID)));
            calendar.setCreateByName(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_CREATE_BY_NAME)));
            calendar.setUpdateById(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_UPDATE_BY_ID)));
            calendar.setUpdateByName(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_UPDATE_BY_NAME)));
            calendar.setLocation(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_LOCATION)));
            calendar.setShowDate(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_SHOW_DATE)));
            calendar.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_TITLE)));
            calendar.setType(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_TYPE)));
            calendar.setNewCreate(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_NEW_CREATE))));
        }
        return calendar;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.CalendarFrom);
        dest.writeString(this.CalendarTo);
        dest.writeString(this.Description);
        dest.writeString(this.Type);
        dest.writeString(this.Title);
        dest.writeString(this.Location);
        dest.writeString(this.Reminder);
        dest.writeString(this.Status);
        dest.writeString(this.CreateById);
        dest.writeString(this.CreateByName);
        dest.writeString(this.UpdateById);
        dest.writeString(this.UpdateByName);
        dest.writeInt(this.Id);
        dest.writeByte((byte) (this.AllDay ? 1 : 0));
        dest.writeByte((byte) (this.NewCreate ? 1 : 0));
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.CalendarFrom = in.readString();
        this.CalendarTo = in.readString();
        this.Description = in.readString();
        this.Type = in.readString();
        this.Title = in.readString();
        this.Location = in.readString();
        this.Reminder = in.readString();
        this.Status = in.readString();
        this.CreateById = in.readString();
        this.CreateByName = in.readString();
        this.UpdateById = in.readString();
        this.UpdateByName = in.readString();
        this.Id = in.readInt();
        this.AllDay = in.readByte() != 0;
        this.NewCreate = in.readByte() != 0;
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Calendar createFromParcel(Parcel in) {
            return new Calendar(in);
        }

        @Override
        public Calendar[] newArray(int size) {
            return new Calendar[size];
        }
    };

}

