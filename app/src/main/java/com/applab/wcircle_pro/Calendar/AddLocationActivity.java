package com.applab.wcircle_pro.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Notification.NotificationDialogFragment;
import com.applab.wcircle_pro.Notification.NotificationProvider;
import com.applab.wcircle_pro.Notification.Notifications;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;


public class AddLocationActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar toolbar;
    private LinearLayout btnCancel;
    private RecyclerView recyclerView;
    private LocationAdapter adapter;
    private String selection = null;
    private String[] selectionArgs = null;
    private Cursor cursor;
    private EditText ediSearch;
    private String place = "";
    private DialogFragment notificationDialogFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportLoaderManager().initLoader(0, null, this);
        setContentView(R.layout.activity_add_location);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        TextView txtToolbarTitle = (TextView) findViewById(R.id.txtTitle);
        txtToolbarTitle.setText("Add Location");
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new LocationAdapter(this, cursor);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        setTouchListener();
        ediSearch = (EditText) findViewById(R.id.ediSearch);
        selectionArgs = null;
        selection = null;
        searchWatcher();
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        ediSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                cursor.moveToPosition(position);
                SharedPreferences pref;
                SharedPreferences.Editor editor;
                pref = getApplicationContext().getSharedPreferences("AddLocation", 0);
                editor = pref.edit();
                editor.clear();
                editor.apply(); // commit changes
                editor.putString("place", cursor.getString(cursor.getColumnIndex(DBHelper.PLACE_COLUMN_NAME)));
                editor.apply();
                finish();
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    private void searchWatcher() {
        ediSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (ediSearch.getText().length() > 0) {
                    selection = DBHelper.PLACE_COLUMN_NAME + " LIKE ? ";
                    selectionArgs = new String[]{"%" + ediSearch.getText().toString() + "%"};
                } else {
                    selection = null;
                    selectionArgs = null;
                }
                getSupportLoaderManager().restartLoader(0, null, AddLocationActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_location, menu);
        // Get the SearchView and set the searchable configuration

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                break;
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = PlaceProvider.CONTENT_URI;
        return new CursorLoader(getBaseContext(), uri, null, selection, selectionArgs, DBHelper.PLACE_COLUMN_ID + " DESC ");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.cursor = data;
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    private void setPopUpWindow(Cursor cursor) {
        notificationDialogFragment = new NotificationDialogFragment();
        Bundle args = new Bundle();
        ArrayList<Notifications> arrayList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Notifications notifications = new Notifications();
                notifications.setId(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_ID)));
                notifications.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_CREATE_DATE)));
                notifications.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_TITLE)));
                notifications.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_DESCRIPTION)));
                notifications.setType(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_TYPE)));
                arrayList.add(notifications);
            }
            while (cursor.moveToNext());
            cursor.close();
        }
        args.putParcelableArrayList("Notifications", arrayList);
        notificationDialogFragment.setArguments(args);
        notificationDialogFragment.setCancelable(false);
        notificationDialogFragment.show(getSupportFragmentManager(), "");
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Utils.NOTIFICATION_ACTION)) {
                Uri uri = NotificationProvider.CONTENT_URI;
                String[] projection = {
                        DBHelper.NOTIFICATION_COLUMN_TYPE,
                        DBHelper.NOTIFICATION_COLUMN_TITLE,
                        DBHelper.NOTIFICATION_COLUMN_CREATE_DATE,
                        DBHelper.NOTIFICATION_COLUMN_DESCRIPTION,
                        DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY,
                        DBHelper.NOTIFICATION_COLUMN_ID
                };
                String selection = DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY + "=?";
                String[] selectionArgs = {"0"};
                Cursor cursor = getContentResolver().query(uri, projection, selection, selectionArgs, DBHelper.NOTIFICATION_COLUMN_ID + " DESC ");
                if (notificationDialogFragment != null) {
                    if (!notificationDialogFragment.isHidden()) {
                        notificationDialogFragment.dismiss();
                    }
                }
                setPopUpWindow(cursor);
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(),false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(),false);
        IntentFilter iff = new IntentFilter(Utils.NOTIFICATION_ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }
}
