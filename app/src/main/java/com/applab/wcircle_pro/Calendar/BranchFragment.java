package com.applab.wcircle_pro.Calendar;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.*;
import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.Employee.BranchEmployeeProvider;
import com.applab.wcircle_pro.Employee.CountryProvider;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

public class BranchFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    //region variable
    private RecyclerView recyclerView, slidingRecyclerView;
    private BranchAdapter branchAdapter;
    private CountryAdapter countryAdapter;
    private LinearLayoutManager linearLayoutManager, slidingLinearLayoutManager;
    private String TAG = "EMPLOYEE";
    private EditText ediSlidingSearch;
    private SlidingUpPanelLayout slidingUpPanelLayout;
    private int pageNoCountry = 1;
    private int pageNoEmployee = 1;
    private Cursor cursorCountry, cursorEmployee;
    private ArrayList<NewEmployee> addedEmployee = new ArrayList<>();
    private String countryCode = "";
    private RelativeLayout rlAutoComplete;
    private Intent mIntent;
    private LinearLayout btnCancel, btnSubmit, btnStaff;
    private ArrayList<Integer> addedId = new ArrayList<>();
    public static final String ACTION = "BRANCH_CALENDAR_EMPLOYEE";
    private String selection;
    private String[] selectionArgs = null;
    private TextView txtName, txtError, txtSlidingError;
    //endregion

    //region on create
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        selection = DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "!=? AND " + DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?";
        selectionArgs = new String[]{String.valueOf(Utils.getProfile(getActivity()).getId()), countryCode};
        View layout = inflater.inflate(R.layout.fragment_branch, container, false);
        layout.setOnKeyListener(backPressed);
        mIntent = getActivity().getIntent();

        if (mIntent.getParcelableArrayListExtra("addedEmployee") != null) {
            addedEmployee = mIntent.getParcelableArrayListExtra("addedEmployee");
            for (int i = 0; i < addedEmployee.size(); i++) {
                NewEmployee employee = addedEmployee.get(i);
                addedId.add(employee.getId());
            }
        }
        ediSlidingSearch = (EditText) layout.findViewById(R.id.ediSlidingSearch);
        rlAutoComplete = (RelativeLayout) layout.findViewById(R.id.rlAutoComplete);
        rlAutoComplete.setVisibility(View.GONE);

        slidingUpPanelLayout = (SlidingUpPanelLayout) layout.findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.setPanelSlideListener(panelSlideListener);

        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        countryAdapter = new CountryAdapter(getActivity(), cursorCountry);
        recyclerView.setAdapter(countryAdapter);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);

        slidingRecyclerView = (RecyclerView) layout.findViewById(R.id.slidingRecyclerView);
        branchAdapter = new BranchAdapter(getActivity(), cursorEmployee, addedEmployee, addedId);
        slidingRecyclerView.setAdapter(branchAdapter);
        slidingLinearLayoutManager = new LinearLayoutManager(getActivity());
        slidingRecyclerView.setLayoutManager(slidingLinearLayoutManager);
        slidingRecyclerView.addOnScrollListener(slidingRecyclerViewOnScrollListener);

        setSlidingTouchListener();
        setTouchListener();


        SearchSlidingWatcher();
        txtName = (TextView) layout.findViewById(R.id.txtName);
        txtError = (TextView) layout.findViewById(R.id.txtError);
        txtSlidingError = (TextView) layout.findViewById(R.id.txtSlidingError);

        HttpHelper.getCountryGson(getActivity(), pageNoCountry, TAG, txtError);

        btnCancel = (LinearLayout) layout.findViewById(R.id.btnCancel);
        btnStaff = (LinearLayout) layout.findViewById(R.id.btnStaff);
        btnStaff.setOnClickListener(btnStaffOnClickListener);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        btnSubmit = (LinearLayout) layout.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(btnSubmitOnClickListener);
        getActivity().getSupportLoaderManager().initLoader(2, null, this);
        getActivity().getSupportLoaderManager().initLoader(365, null, this);
        return layout;
    }
    //endregion

    private View.OnClickListener btnStaffOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        }
    };

    //region back press region
    private View.OnKeyListener backPressed = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                return true;
            }
            return false;
        }
    };
    //endregion

    //region onclick region
    private LinearLayout.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                ArrayList<ContentValues> contentValueses = new ArrayList<ContentValues>(addedId.size());
                Uri uri = TempAttendeesProvider.CONTENT_URI;
                getActivity().getContentResolver().delete(uri, DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(mIntent.getIntExtra("calendarId", 0))});
                for (int i = 0; i < addedEmployee.size(); i++) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID, mIntent.getIntExtra("calendarId", 0));
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_EMPLOYEE_ID, addedEmployee.get(i).getId());
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_NAME, addedEmployee.get(i).getName());
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_IS_SELECT, 1);
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_IS_SUBMIT, 0);
                    contentValueses.add(contentValues);
                }
                boolean isEmpty = true;
                for (Object ob : contentValueses) {
                    if (ob != null) {
                        isEmpty = false;
                    }
                }
                if (!isEmpty) {
                    ContentValues[] arr = new ContentValues[contentValueses.size()];
                    arr = contentValueses.toArray(arr);
                    getActivity().getContentResolver().bulkInsert(TempAttendeesProvider.CONTENT_URI, arr);
                }
            } catch (Exception ex) {
                Log.i("ERROR", "Content Provider Error :" + ex.toString());
            } finally {
                Intent intent = new Intent(AddContactActivity.ACTION);
                intent.putExtra("isSubmit", true);
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getActivity());
                mgr.sendBroadcast(intent);
                getActivity().finish();
            }

        }
    };

    private LinearLayout.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(AddContactActivity.ACTION);
            intent.putExtra("isCancel", true);
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getActivity());
            mgr.sendBroadcast(intent);
            getActivity().finish();
        }
    };

    //endregion

    //region scroll listener
    private RecyclerView.OnScrollListener slidingRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = slidingLinearLayoutManager.getItemCount();
            int lastVisibleItem = slidingLinearLayoutManager.findLastVisibleItemPosition();
            if (totalItemCount > 1) {
                if (lastVisibleItem >= totalItemCount - 1) {
                    pageNoEmployee++;
                    HttpHelper.getCountryEmployeeGson(getActivity(), pageNoEmployee, false, countryCode, TAG, null, txtSlidingError);
                }
            }
        }
    };

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (totalItemCount > 1) {
                if (lastVisibleItem >= totalItemCount - 1) {
                    pageNoCountry++;
                    HttpHelper.getCountryGson(getActivity(), pageNoCountry, TAG, txtError);
                }
            }
        }
    };
    //endregion

    //region touch listener
    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                pageNoEmployee = 1;
                cursorCountry.moveToPosition(position);
                countryCode = cursorCountry.getString(cursorCountry.getColumnIndex(DBHelper.COUNTRY_COLUMN_COUNTRY_CODE));
                txtName.setText(cursorCountry.getString(cursorCountry.getColumnIndex(DBHelper.COUNTRY_COLUMN_COUNTRY_NAME)));
                HttpHelper.getCountryEmployeeGson(getActivity(), pageNoEmployee, false, countryCode, TAG, null, txtSlidingError);
                if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                } else if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                selection = DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "!=? AND " + DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?";
                selectionArgs = new String[]{String.valueOf(Utils.getProfile(getActivity()).getId()), countryCode};
                getActivity().getSupportLoaderManager().restartLoader(365, null, BranchFragment.this);

            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    private void setSlidingTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {

            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    //endregion

    //region search watacher
    private void SearchSlidingWatcher() {
        ediSlidingSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    pageNoEmployee = 1;
                    HttpHelper.getCountryEmployeeGson(getActivity(), pageNoEmployee, true, countryCode, TAG, ediSlidingSearch.getText().toString(), txtSlidingError);
                    if (ediSlidingSearch.getText().toString().length() > 0) {
                        selection = "(" + DBHelper.BRANCH_EMPLOYEE_COLUMN_NAME + " LIKE ? OR " + DBHelper.BRANCH_EMPLOYEE_COLUMN_DEPARTMENT + " LIKE ?) AND "
                                + DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=? AND " + DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "!=?";
                        selectionArgs = new String[]{"%" + ediSlidingSearch.getText().toString() + "%", "%" + ediSlidingSearch.getText().toString() + "%", countryCode, Utils.getProfile(getActivity()).getId().toString()};
                        getActivity().getSupportLoaderManager().restartLoader(365, null, BranchFragment.this);
                    } else {
                        selection = DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=? AND " + DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "!=?";
                        selectionArgs = new String[]{countryCode, Utils.getProfile(getActivity()).getId().toString()};
                        getActivity().getSupportLoaderManager().restartLoader(365, null, BranchFragment.this);
                    }
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }
    //endregion

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == getActivity().RESULT_OK && requestCode == 123) {
            if (intent.getBooleanExtra("isBranch", false)) {
                addedEmployee = new ArrayList<>();
                addedId = new ArrayList<>();
                addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                for (int i = 0; i < addedEmployee.size(); i++) {
                    addedId.add(addedEmployee.get(i).getId());
                }
                branchAdapter.swapAddedEmployee(addedId, addedEmployee);
            }
        }
    }

    //region broadcast
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(AddContactActivity.ACTION)) {
                if (intent.getBooleanExtra("isBranch", false)) {
                    addedEmployee = new ArrayList<>();
                    addedId = new ArrayList<>();
                    addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                    for (int i = 0; i < addedEmployee.size(); i++) {
                        addedId.add(addedEmployee.get(i).getId());
                    }
                    branchAdapter.swapAddedEmployee(addedId, addedEmployee);
                } else if (intent.getBooleanExtra("isBranchChange", false)) {
                    addedEmployee = new ArrayList<>();
                    addedId = new ArrayList<>();
                    addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                    for (int i = 0; i < addedEmployee.size(); i++) {
                        addedId.add(addedEmployee.get(i).getId());
                    }
                }
            } else if (action.equals(ACTION)) {
                if (!intent.getBooleanExtra("isSlide", false)) {
                    if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    }
                }
            }
        }
    };
    //endregion

    //region resume and on pause
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(AddContactActivity.ACTION);
        iff.addAction(ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    public void onStop() {
        super.onStop();
        getLoaderManager().destroyLoader(2);
    }
    //endregion

    //region panel slider region
    private SlidingUpPanelLayout.PanelSlideListener panelSlideListener = new SlidingUpPanelLayout.PanelSlideListener() {
        @Override
        public void onPanelSlide(View panel, float slideOffset) {

        }

        @Override
        public void onPanelCollapsed(View panel) {
            Intent intent = new Intent(AddContactActivity.ACTION);
            intent.putExtra("isSlide", false);
            AddContactActivity.mgr.sendBroadcast(intent);
        }

        @Override
        public void onPanelExpanded(View panel) {
            Intent intent = new Intent(AddContactActivity.ACTION);
            intent.putExtra("isSlide", true);
            AddContactActivity.mgr.sendBroadcast(intent);
        }

        @Override
        public void onPanelAnchored(View panel) {

        }

        @Override
        public void onPanelHidden(View view) {


        }
    };
    //endregion

    //region loader cursor region

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 2) {
            return new CursorLoader(getActivity(), CountryProvider.CONTENT_URI, null, null, null, null);
        } else if (id == 365) {
            return new CursorLoader(getActivity(), BranchEmployeeProvider.CONTENT_URI, null, selection, selectionArgs, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 2) {
            if (data != null) {
                this.cursorCountry = data;
                countryAdapter.swapCursor(cursorCountry);
            }
        } else if (loader.getId() == 365) {
            if (data != null) {
                this.cursorEmployee = data;
                branchAdapter.swapCursor(cursorEmployee);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
    //endregion
}