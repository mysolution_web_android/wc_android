package com.applab.wcircle_pro.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Tabs.SlidingTabLayout;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

public class AddContactActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ViewPager mPager;
    private SlidingTabLayout mTabs;
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    public static String ACTION = "ADD_OR_REMOVE_CONTACT_ACTION";
    public static LocalBroadcastManager mgr;
    private boolean isSlide = false;
    private SlidingUpPanelLayout slidingUpPanelLayout;
    private boolean isMainSlide = false;
    private ArrayList<Integer> addedId = new ArrayList<>();
    private ArrayList<NewEmployee> addedEmployee = new ArrayList<>();
    private Intent mIntent;
    private RecyclerView slidingRecyclerView;
    private LinearLayoutManager slidingLinearLayoutManager;
    private AddedEmployeeAdapter addedAdapter;
    private RelativeLayout fadeRL;
    private ProgressBar progressBar;
    private TextView txtToolbarView;
    private LinearLayout btnStaff;
    private int mPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        txtToolbarView = (TextView) findViewById(R.id.txtTitle);
        txtToolbarView.setText(this.getResources().getString(R.string.title_activity_add_contact));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        mIntent = getIntent();
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        setVisibilityRlAndProgressBar(false);
        fadeRL.setOnClickListener(fadeRLOnClickListener);
        if (mIntent.getParcelableArrayListExtra("addedEmployee") != null) {
            addedEmployee = mIntent.getParcelableArrayListExtra("addedEmployee");
            for (int i = 0; i < addedEmployee.size(); i++) {
                NewEmployee employee = addedEmployee.get(i);
                addedId.add(employee.getId());
            }
        }

        btnStaff = (LinearLayout) findViewById(R.id.btnStaff);
        btnStaff.setOnClickListener(btnStaffOnClickListener);

        slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.setPanelSlideListener(panelSlideListener);

        slidingRecyclerView = (RecyclerView) findViewById(R.id.slidingRecyclerView);
        addedAdapter = new AddedEmployeeAdapter(this, addedEmployee);
        slidingRecyclerView.setAdapter(addedAdapter);
        slidingLinearLayoutManager = new LinearLayoutManager(this);
        slidingRecyclerView.setLayoutManager(slidingLinearLayoutManager);

        pref = getApplicationContext().getSharedPreferences("AddContact", 0);
        editor = pref.edit();
        mgr = LocalBroadcastManager.getInstance(this);
        getIntent().getExtras();
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(3);
        mPager.addOnPageChangeListener(mPagerChangeListener);
        mPager.setAdapter(new AddContactPagerAdapter(getSupportFragmentManager(), this));
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setDistributeEvenly(true);
        mTabs.setCustomTabView(R.layout.custom_tab_view, R.id.tabText, R.id.tabNumber);
        mTabs.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
        mTabs.setSelectedIndicatorColors(ContextCompat.getColor(this, android.R.color.holo_red_light));
        mTabs.setViewPager(mPager);
    }

    private ViewPager.OnPageChangeListener mPagerChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            mPosition = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private View.OnClickListener btnStaffOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        }
    };

    private View.OnClickListener fadeRLOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private void setVisibilityRlAndProgressBar(boolean isVisible) {
        if (isVisible) {
            fadeRL.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            fadeRL.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    //region panel slider region
    private SlidingUpPanelLayout.PanelSlideListener panelSlideListener = new SlidingUpPanelLayout.PanelSlideListener() {
        @Override
        public void onPanelSlide(View panel, float slideOffset) {
        }

        @Override
        public void onPanelCollapsed(View panel) {
            isMainSlide = false;
        }

        @Override
        public void onPanelExpanded(View panel) {
            isMainSlide = true;
        }

        @Override
        public void onPanelAnchored(View panel) {

        }

        @Override
        public void onPanelHidden(View view) {


        }
    };
    //endregion

    @Override
    public void onBackPressed() {
        if (isMainSlide) {
            if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            } else if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        } else if (!isSlide) {
            setVisibilityRlAndProgressBar(true);
            editor.clear();
            editor.apply();
            finish();
        } else {
            if (mPosition != 2) {
                setVisibilityRlAndProgressBar(true);
                editor.clear();
                editor.apply();
                finish();
            } else {
                Intent intent = new Intent(BranchFragment.ACTION);
                intent.putExtra("isSlide", false);
                mgr.sendBroadcast(intent);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            if (isMainSlide) {
                if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                } else if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
            } else if (!isSlide) {
                setVisibilityRlAndProgressBar(true);
                editor.clear();
                editor.apply();
                finish();
            } else {
                if (mPosition != 2) {
                    setVisibilityRlAndProgressBar(true);
                    editor.clear();
                    editor.apply();
                    finish();
                } else {
                    Intent intent = new Intent(BranchFragment.ACTION);
                    intent.putExtra("isSlide", false);
                    mgr.sendBroadcast(intent);
                }
            }
            return true;
        } else if (id == R.id.action_member) {
            if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            } else if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                isSlide = intent.getBooleanExtra("isSlide", false);
                if (intent.getBooleanExtra("isMainChange", false)) {
                    if (intent.getParcelableArrayListExtra("addedEmployee") != null) {
                        addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                        for (int i = 0; i < addedEmployee.size(); i++) {
                            NewEmployee employee = addedEmployee.get(i);
                            addedId.add(employee.getId());
                        }
                    }
                } else if (intent.getBooleanExtra("isMain", false)) {
                    if (intent.getParcelableArrayListExtra("addedEmployee") != null) {
                        addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                        for (int i = 0; i < addedEmployee.size(); i++) {
                            NewEmployee employee = addedEmployee.get(i);
                            addedId.add(employee.getId());
                        }
                    }
                    addedAdapter.swapEmployee(addedEmployee);
                } else if (intent.getBooleanExtra("isSubmit", false)) {
                    setVisibilityRlAndProgressBar(true);
                } else if (intent.getBooleanExtra("isCancel", false)) {
                    setVisibilityRlAndProgressBar(true);
                } else if (intent.getBooleanExtra("isFail", false)) {
                    setVisibilityRlAndProgressBar(false);
                }
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

}
