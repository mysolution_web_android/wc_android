package com.applab.wcircle_pro.Calendar;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by admin on 4/5/2015.
 */
public class ScheduleViewHolder extends RecyclerView.ViewHolder {
    ImageView imgColor;
    TextView txtDayMonth;
    TextView txtDayWeek;
    TextView txtTitle;
    TextView txtTime;
    TextView txtId;

    public ScheduleViewHolder(View itemView) {
        super(itemView);
        txtDayMonth = (TextView) itemView.findViewById(R.id.txtDayMonth);
        txtDayWeek = (TextView) itemView.findViewById(R.id.txtDayWeek);
        txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
        txtTime = (TextView) itemView.findViewById(R.id.txtTime);
        imgColor = (ImageView) itemView.findViewById(R.id.imgColor);
        txtId = (TextView)itemView.findViewById(R.id.txtId);
    }
}
