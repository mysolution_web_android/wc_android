package com.applab.wcircle_pro.Calendar.db;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Calendar.Attendee;
import com.applab.wcircle_pro.Calendar.AttendeesProvider;
import com.applab.wcircle_pro.Calendar.CalendarActivity;
import com.applab.wcircle_pro.Calendar.CalendarChecking;
import com.applab.wcircle_pro.Calendar.CalendarCheckingProvider;
import com.applab.wcircle_pro.Calendar.CalendarDetailsActivity;
import com.applab.wcircle_pro.Calendar.CalendarEventDetailsActivity;
import com.applab.wcircle_pro.Calendar.CalendarProvider;
import com.applab.wcircle_pro.Calendar.Month;
import com.applab.wcircle_pro.Calendar.MonthChecking;
import com.applab.wcircle_pro.Calendar.MonthCheckingProvider;
import com.applab.wcircle_pro.Calendar.MonthFragment;
import com.applab.wcircle_pro.Calendar.MonthProvider;
import com.applab.wcircle_pro.Calendar.PlaceProvider;
import com.applab.wcircle_pro.Calendar.SpecialMonthFragment;
import com.applab.wcircle_pro.Calendar.TempAttendeesProvider;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Dashboard.DashboardProvider;
import com.applab.wcircle_pro.Employee.BranchEmployeeProvider;
import com.applab.wcircle_pro.Employee.CountryChecking;
import com.applab.wcircle_pro.Employee.CountryCheckingProvider;
import com.applab.wcircle_pro.Employee.CountryProvider;
import com.applab.wcircle_pro.Employee.DepartmentEmployeeProvider;
import com.applab.wcircle_pro.Employee.EmployeeChecking;
import com.applab.wcircle_pro.Employee.EmployeeCheckingProvider;
import com.applab.wcircle_pro.Favorite.ContactProvider;
import com.applab.wcircle_pro.Favorite.FavoriteContact;
import com.applab.wcircle_pro.Favorite.FavoriteContactProvider;
import com.applab.wcircle_pro.Leave.EditApplicationActivity;
import com.applab.wcircle_pro.Leave.Leave;
import com.applab.wcircle_pro.Leave.LeaveActivity;
import com.applab.wcircle_pro.Leave.LeaveProvider;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by user on 23/11/2015.
 */
public class HttpHelper {
    //region Schedule
    public static void getScheduleGson(Context context, String TAG, TextView txtError, Date first, Date last) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        final int calendarRow = getCalendarRow(context, Utils.setCalendarDate("MMMMyyyy", first));
        final Date lastUpdateDate = getCalendarLastUpdate(context, first);
        String lastUpdate = "2000-09-09T10:16:25z";
        String firstDate = Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, first)));
        String lastDate = Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, last)));
        String url = Utils.API_URL + "api/Calendar/Month/List?FromDate=" + firstDate + "&ToDate=" + lastDate +
                "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(calendarRow == 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Calendar/Month/List?FromDate=" + firstDate + "&ToDate=" + lastDate +
                    "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(calendarRow == 0);
        }
        final String finalUrl = url;
        Log.i(TAG, finalUrl);
        GsonRequest<CalendarChecking> mGsonRequest = new GsonRequest<CalendarChecking>(
                Request.Method.GET,
                finalUrl,
                CalendarChecking.class,
                headers,
                responseCalendarListener(context, txtError, TAG, first, last),
                errorCalendarListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<CalendarChecking> responseCalendarListener(final Context context, final TextView txtError, final String TAG, final Date first, final Date last) {
        return new Response.Listener<CalendarChecking>() {
            @Override
            public void onResponse(CalendarChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                String month = Utils.setCalendarDate("MMMMyyyy", first);
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getCalendar().size() > 0) {
                            if (first != last) {
                                context.getContentResolver().delete(CalendarProvider.CONTENT_URI, DBHelper.CALENDAR_COLUMN_MONTH + "=? OR " + DBHelper.CALENDAR_COLUMN_MONTH + " IS NULL ", new String[]{month});
                                context.getContentResolver().delete(AttendeesProvider.CONTENT_URI, DBHelper.ATTENDEES_COLUMN_MONTH + "=? OR " + DBHelper.ATTENDEES_COLUMN_MONTH + " IS NULL ", new String[]{month});
                                context.getContentResolver().delete(CalendarCheckingProvider.CONTENT_URI, DBHelper.CALENDAR_CHECKING_COLUMN_MONTH + "=? OR " + DBHelper.CALENDAR_CHECKING_COLUMN_MONTH + " IS NULL ", new String[]{month});
                            }
                            insertCalendar(response, context, first, last, TAG);
                        }
                    }
                }
            }
        };
    }

    public static Date getCalendarLastUpdate(Context context, Date firstDate) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.CALENDAR_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CalendarCheckingProvider.CONTENT_URI,
                    projection, DBHelper.CALENDAR_CHECKING_COLUMN_MONTH + " =? ",
                    new String[]{Utils.setCalendarDate("MMMMyyyy", firstDate)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static int getCalendarRow(Context context, Object month) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CalendarProvider.CONTENT_URI,
                    null, DBHelper.CALENDAR_COLUMN_MONTH + "=?", new String[]{String.valueOf(month)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return totalRow;
    }

    public static Response.ErrorListener errorCalendarListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }

    public static void insertCalendar(CalendarChecking result, Context context, Date first, Date last, String TAG) {
        DBHelper helper = new DBHelper(context);
        String strMonth = Utils.setCalendarDate("MMMMyyyy", first);
        try {
            ContentValues contentValuesCalendarChecking = new ContentValues();
            contentValuesCalendarChecking.put(DBHelper.CALENDAR_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesCalendarChecking.put(DBHelper.CALENDAR_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesCalendarChecking.put(DBHelper.CALENDAR_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesCalendarChecking.put(DBHelper.CALENDAR_CHECKING_COLUMN_LEVEL, 1);
            if (first != last) {
                contentValuesCalendarChecking.put(DBHelper.CALENDAR_CHECKING_COLUMN_MONTH, strMonth);
                context.getContentResolver().insert(CalendarCheckingProvider.CONTENT_URI, contentValuesCalendarChecking);
            }
            ArrayList<ContentValues> contentValuesArrayList = new ArrayList<>();
            ArrayList<ContentValues> contentValuesArrayListAttendees = new ArrayList<>();
            for (int i = 0; i < result.getCalendar().size(); i++) {
                ContentValues contentValues = new ContentValues();
                String calendarTo = result.getCalendar().get(i).getCalendarTo();
                String calendarFrom = result.getCalendar().get(i).getCalendarFrom();
                String showDate = result.getCalendar().get(i).getShowDate();
                String reminderDate = result.getCalendar().get(i).getReminder() != null ? result.getCalendar().get(i).getReminder() : null;
                contentValues.put(DBHelper.CALENDAR_COLUMN_CALENDAR_ID, result.getCalendar().get(i).getId());
                contentValues.put(DBHelper.CALENDAR_COLUMN_TYPE, result.getCalendar().get(i).getType());
                contentValues.put(DBHelper.CALENDAR_COLUMN_TITLE, result.getCalendar().get(i).getTitle());
                contentValues.put(DBHelper.CALENDAR_COLUMN_ALL_DAY, String.valueOf(result.getCalendar().get(i).getAllDay()));
                contentValues.put(DBHelper.CALENDAR_COLUMN_START_DATE, calendarFrom);
                contentValues.put(DBHelper.CALENDAR_COLUMN_END_DATE, calendarTo);
                contentValues.put(DBHelper.CALENDAR_COLUMN_LOCATION, result.getCalendar().get(i).getLocation());
                contentValues.put(DBHelper.CALENDAR_COLUMN_DESCRIPTION, result.getCalendar().get(i).getDescription());
                contentValues.put(DBHelper.CALENDAR_COLUMN_REMINDER, reminderDate);
                contentValues.put(DBHelper.CALENDAR_COLUMN_STATUS, result.getCalendar().get(i).getStatus());
                contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_ID, result.getCalendar().get(i).getCreateById());
                contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_NAME, result.getCalendar().get(i).getCreateByName());
                contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_ID, result.getCalendar().get(i).getUpdateById());
                contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_NAME, result.getCalendar().get(i).getUpdateByName());
                contentValues.put(DBHelper.CALENDAR_COLUMN_NEW_CREATE, String.valueOf(result.getCalendar().get(i).getNewCreate()));
                if (first != last) {
                    contentValues.put(DBHelper.CALENDAR_COLUMN_MONTH, strMonth);
                }
                contentValues.put(DBHelper.CALENDAR_COLUMN_LEVEL, 1);
                if (first != last) {
                    context.getContentResolver().delete(CalendarProvider.CONTENT_URI, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?",
                            new String[]{String.valueOf(result.getCalendar().get(i).getId())});
                    if (!contentValuesArrayList.contains(contentValues)) {
                        contentValuesArrayList.add(contentValues);
                    }
                } else {
                    int row = context.getContentResolver().update(CalendarProvider.CONTENT_URI, contentValues, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.CALENDAR_COLUMN_SHOW_DATE + "=?",
                            new String[]{String.valueOf(result.getCalendar().get(i).getId()), showDate});
                    if (row == 0) {
                        if (!contentValuesArrayList.contains(contentValues)) {
                            contentValuesArrayList.add(contentValues);
                        }
                    }
                }
                context.getContentResolver().delete(AttendeesProvider.CONTENT_URI, DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=?",
                        new String[]{String.valueOf(result.getCalendar().get(i).getId())});
                if (result.getCalendar().get(i).getAttendees() != null) {
                    if (result.getCalendar().get(i).getAttendees().size() > 0) {
                        for (int j = 0; j < result.getCalendar().get(i).getAttendees().size(); j++) {
                            String remind = result.getCalendar().get(i).getAttendees().get(j).getReminder() != null ? result.getCalendar().get(i).getAttendees().get(j).getReminder() : null;
                            ContentValues contentValuesAttendees = new ContentValues();
                            contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SUBMIT, 1);
                            contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SELECT, 1);
                            contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_NAME, result.getCalendar().get(i).getAttendees().get(j).getEmployeyName());
                            if (first != last) {
                                contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_MONTH, strMonth);
                            }
                            contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_LEVEL, 1);
                            contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID, result.getCalendar().get(i).getAttendees().get(j).getEmployeeId());
                            contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_CALENDAR_ID, result.getCalendar().get(i).getId());
                            contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_REMINDER, remind);
                            contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_STATUS, result.getCalendar().get(i).getAttendees().get(j).getStatus());

                            if (!contentValuesArrayListAttendees.contains(contentValuesAttendees)) {
                                contentValuesArrayListAttendees.add(contentValuesAttendees);
                                if (result.getCalendar().get(i).getAttendees().get(j).getStatus().equals("pending") &&
                                        result.getCalendar().get(i).getAttendees().get(j).getEmployeeId().equals(Utils.getProfile(context).getId())) {
                                    if (remind != null && result.getCalendar().get(i).getStatus().equals("active")) {
                                        remind = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, remind);
                                        calendarFrom = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, calendarFrom);
                                        Date date = Utils.setCalendarDate(Utils.DATE_FORMAT, remind);
                                        if (date.after(new Date())) {
                                            int year = CalendarActivity.getYear(remind);
                                            int month = CalendarActivity.getMonth(remind);
                                            int day = CalendarActivity.getDay(remind);
                                            int hour = CalendarActivity.getHour(remind, 0);
                                            int minutes = CalendarActivity.getMinutes(remind, 0);
                                            String msg = "";
                                            if (result.getCalendar().get(i).getAllDay()) {
                                                msg += "On " + Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy 'at' h:mm a", calendarFrom) + "\n";
                                            } else {
                                                msg += "On " + Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy 'at' h:mm a", calendarFrom) + "\n";
                                            }
                                            if (result.getCalendar().get(i).getTitle().equals("testing")) {
                                                Log.i(TAG, "Testing: " + msg);
                                            }
                                            CalendarActivity.setAlarm(year, month, day, hour, minutes,
                                                    msg, result.getCalendar().get(i),
                                                    new ArrayList<Attendee>(result.getCalendar().get(i).getAttendees()), context);
                                        }
                                    }
                                }
                                context.getContentResolver().delete(AttendeesProvider.CONTENT_URI, DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                                        new String[]{String.valueOf(result.getCalendar().get(i).getId()),
                                                String.valueOf(result.getCalendar().get(i).getAttendees().get(j).getEmployeeId())});
                            }
                        }
                    }
                }
                if (result.getCalendar().get(i).getCreateById().equals(String.valueOf(Utils.getProfile(context).getId()))
                        || result.getCalendar().get(i).getType().equals("C") || result.getCalendar().get(i).getType().equals("B")) {
                    if (reminderDate != null && result.getCalendar().get(i).getStatus().equals("active")) {
                        reminderDate = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, reminderDate);
                        calendarFrom = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, calendarFrom);
                        Date date = Utils.setCalendarDate(Utils.DATE_FORMAT, reminderDate);
                        if (date.after(new Date())) {
                            int year = CalendarActivity.getYear(reminderDate);
                            int month = CalendarActivity.getMonth(reminderDate);
                            int day = CalendarActivity.getDay(reminderDate);
                            int hour = CalendarActivity.getHour(reminderDate, 0);
                            int minutes = CalendarActivity.getMinutes(reminderDate, 0);
                            String msg = "";
                            if (!result.getCalendar().get(i).getAllDay()) {
                                msg += "On " + Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy 'at' h:mm a", calendarFrom) + "\n";
                            } else {
                                msg += "On " + Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy 'at' h:mm a", calendarFrom) + "\n";
                            }
                            if (result.getCalendar().get(i).getTitle().equals("testing")) {
                                Log.i(TAG, "Testing: " + msg);
                            }
                            CalendarActivity.setAlarm(year, month, day, hour, minutes, msg, result.getCalendar().get(i),
                                    new ArrayList<Attendee>(result.getCalendar().get(i).getAttendees()), context);
                        }
                    }
                }
            }
            ArrayList<ContentValues> completeContentValuesArrayList = new ArrayList<>();
            for (int k = 0; k < contentValuesArrayList.size(); k++) {
                ContentValues contentValues = new ContentValues();
                contentValues.putAll(contentValuesArrayList.get(k));
                Date calendarFrom = Utils.setDate(Utils.DATE_FORMAT, contentValues.getAsString(DBHelper.CALENDAR_COLUMN_START_DATE));
                Date calendarTo = Utils.setDate(Utils.DATE_FORMAT, contentValues.getAsString(DBHelper.CALENDAR_COLUMN_END_DATE));
                contentValues.put(DBHelper.CALENDAR_COLUMN_SHOW_DATE, contentValues.getAsString(DBHelper.CALENDAR_COLUMN_START_DATE));
                completeContentValuesArrayList.add(contentValues);
                while (calendarFrom.before(calendarTo)) {
                    calendarFrom = addDay(calendarFrom, 1);
                    Date d1 = Utils.setCalendarDate("yyyy-MM-dd", Utils.setCalendarDate("yyyy-MM-dd", calendarFrom));
                    Date d2 = Utils.setCalendarDate("yyyy-MM-dd", Utils.setCalendarDate("yyyy-MM-dd", calendarTo));
                    if (!d1.after(d2)) {
                        contentValues = new ContentValues();
                        contentValues.putAll(contentValuesArrayList.get(k));
                        contentValues.put(DBHelper.CALENDAR_COLUMN_SHOW_DATE, Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, calendarFrom)));
                        completeContentValuesArrayList.add(contentValues);
                    }
                }
            }
            ArrayList<ContentValues> completeMonthContentValuesArrayList = new ArrayList<>();
            ArrayList<Month> monthsPrev = new ArrayList<>();
            ArrayList<Month> monthsNext = new ArrayList<>();
            if (first != last) {
                for (int k = 0; k < completeContentValuesArrayList.size(); k++) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.putAll(completeContentValuesArrayList.get(k));
                    String showDate = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, contentValues.getAsString(DBHelper.CALENDAR_COLUMN_SHOW_DATE));
                    showDate = Utils.setCalendarDate(Utils.DATE_FORMAT, "yyyy-MM-dd", showDate) + "T00:00:00Z";
                    boolean isP = contentValues.getAsString(DBHelper.CALENDAR_COLUMN_TYPE).equals("P");
                    boolean isB = contentValues.getAsString(DBHelper.CALENDAR_COLUMN_TYPE).equals("B");
                    boolean isC = contentValues.getAsString(DBHelper.CALENDAR_COLUMN_TYPE).equals("C");
                    Month month = new Month();
                    month.setB(isB);
                    month.setP(isP);
                    month.setC(isC);
                    month.setShowDate(Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, showDate));
                    monthsPrev.add(month);
                    monthsNext.add(month);
                }

                for (int k = 0; k < monthsPrev.size(); k++) {
                    String showDatePrev = monthsPrev.get(k).getShowDate();
                    Month monthPrev = monthsPrev.get(k);
                    for (int j = 0; j < monthsNext.size(); j++) {
                        String showDateNext = monthsNext.get(j).getShowDate();
                        Month monthNext = monthsNext.get(j);
                        if (showDateNext.equals(showDatePrev)) {
                            if (monthPrev.getB()) {
                                monthNext.setB(true);
                            }

                            if (monthPrev.getP()) {
                                monthNext.setP(true);
                            }

                            if (monthPrev.getC()) {
                                monthNext.setC(true);
                            }
                        }
                    }
                }
                context.getContentResolver().delete(MonthProvider.CONTENT_URI, null, null);
                for (int k = 0; k < monthsNext.size(); k++) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.MONTH_COLUMN_SHOW_DATE, monthsNext.get(k).getShowDate());
                    contentValues.put(DBHelper.MONTH_COLUMN_B, monthsNext.get(k).getB().toString());
                    contentValues.put(DBHelper.MONTH_COLUMN_P, monthsNext.get(k).getP().toString());
                    contentValues.put(DBHelper.MONTH_COLUMN_C, monthsNext.get(k).getC().toString());
                    completeMonthContentValuesArrayList.add(contentValues);
                }
            }


            ContentValues[] contentValueses = new ContentValues[completeContentValuesArrayList.size()];
            contentValueses = completeContentValuesArrayList.toArray(contentValueses);
            boolean isEmpty = true;
            for (Object ob : contentValueses) {
                if (ob != null) {
                    isEmpty = false;
                }
            }
            long rowsInserted = 0;
            if (!isEmpty) {
                if (first != last) {
                    rowsInserted = context.getContentResolver().bulkInsert(CalendarProvider.CONTENT_URI, contentValueses);
                    Log.i(TAG, String.valueOf(rowsInserted));
                }
            }
            isEmpty = true;
            if (contentValuesArrayListAttendees.size() > 0) {
                contentValueses = new ContentValues[contentValuesArrayListAttendees.size()];
                contentValueses = contentValuesArrayListAttendees.toArray(contentValueses);
                for (Object ob : contentValueses) {
                    if (ob != null) {
                        isEmpty = false;
                    }
                }
                rowsInserted = 0;
                if (!isEmpty) {
                    rowsInserted = context.getContentResolver().bulkInsert(AttendeesProvider.CONTENT_URI, contentValueses);
                    Log.i(TAG, String.valueOf(rowsInserted));
                }
            }

            isEmpty = true;
            if (completeMonthContentValuesArrayList.size() > 0) {
                contentValueses = new ContentValues[completeMonthContentValuesArrayList.size()];
                contentValueses = completeMonthContentValuesArrayList.toArray(contentValueses);
                for (Object ob : contentValueses) {
                    if (ob != null) {
                        isEmpty = false;
                    }
                }
                rowsInserted = 0;
                if (!isEmpty) {
                    rowsInserted = context.getContentResolver().bulkInsert(MonthProvider.CONTENT_URI, contentValueses);
                    Log.i(TAG, String.valueOf(rowsInserted));
                    if(rowsInserted>0){
                        Intent intent = new Intent(SpecialMonthFragment.class.getSimpleName());
                        intent.putExtra("isRefresh", true);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                    }
                }
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    public static Date addDay(Date date, int day) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, day);
        return c.getTime();
    }

    public static int getAlarmPosition(String startDate, String reminder) {
        int position = 0;
        Date sDate = Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, startDate));
        Date rDate = Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, reminder));
        long minutes = TimeUnit.MILLISECONDS.toMinutes(sDate.getTime() - rDate.getTime());
        if (minutes == 30) {
            position = 1;
        } else if (minutes == 60) {
            position = 2;
        } else if (minutes == 120) {
            position = 3;
        } else if (minutes == 180) {
            position = 4;
        } else if (minutes == 240) {
            position = 5;
        } else if (minutes == 300) {
            position = 6;
        } else if (minutes == 360) {
            position = 7;
        } else if (minutes == 420) {
            position = 8;
        } else if (minutes == 480) {
            position = 9;
        }
        return position;
    }
    //endregion

    //region Month
    public static Date getFirstDate(Object month, Object year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.valueOf(String.valueOf(year)));
        calendar.set(Calendar.MONTH, Integer.valueOf(String.valueOf(month)));
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    public static Date getLastDate(Object month, Object year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.valueOf(String.valueOf(year)));
        calendar.set(Calendar.MONTH, Integer.valueOf(String.valueOf(month)));
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }

    public static void getMonthGson(Context context, String TAG, Date first, Date last) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        final int calendarRow = getCalendarRow(context);
        final Date lastUpdateDate = getMonthLastUpdate(context, first);
        String lastUpdate = "2000-09-09T10:16:25z";
        String firstDate = Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, first)));
        String lastDate = Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, last)));
        String url = Utils.API_URL + "api/Calendar/Month?FromDate=" + firstDate
                + "&ToDate=" + lastDate
                + "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(calendarRow > 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Calendar/Month?FromDate=" + firstDate +
                    "&ToDate=" + lastDate
                    + "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(calendarRow > 0);
        }
        final String finalUrl = url;
        Log.i(TAG, finalUrl);
        GsonRequest<MonthChecking> mGsonRequest = new GsonRequest<MonthChecking>(
                Request.Method.GET,
                finalUrl,
                MonthChecking.class,
                headers,
                responseMonthListener(context, TAG, first),
                errorMonthListener(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<MonthChecking> responseMonthListener(final Context context, final String TAG, final Date first) {
        return new Response.Listener<MonthChecking>() {
            @Override
            public void onResponse(MonthChecking response) {
                String month = Utils.setCalendarDate("MMMMyyyy", first);
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getMonths().size() > 0) {
                            context.getContentResolver().delete(MonthProvider.CONTENT_URI, DBHelper.MONTH_CHECKING_COLUMN_MONTH + "=?", new String[]{month});
                            context.getContentResolver().delete(MonthCheckingProvider.CONTENT_URI, DBHelper.MONTH_COLUMN_MONTH + "=?", new String[]{month});
                            insertMonth(response, context, TAG, first);
                        } else {
                            if (response.getNoOfRecords() == 0) {
                                context.getContentResolver().delete(MonthProvider.CONTENT_URI, DBHelper.MONTH_CHECKING_COLUMN_MONTH + "=?", new String[]{month});
                                context.getContentResolver().delete(MonthCheckingProvider.CONTENT_URI, DBHelper.MONTH_COLUMN_MONTH + "=?", new String[]{month});
                            }
                        }
                    }
                }
            }
        };
    }

    public static Date getMonthLastUpdate(Context context, Date first) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.MONTH_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(MonthCheckingProvider.CONTENT_URI,
                    projection, DBHelper.MONTH_CHECKING_COLUMN_MONTH + "=?", new String[]{Utils.setCalendarDate("MMMMyyyy", first)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static int getCalendarRow(Context context) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CalendarProvider.CONTENT_URI, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return totalRow;
    }

    public static boolean isAttendeesExists(Context context, Object calendarId, Object employeeId) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(AttendeesProvider.CONTENT_URI, null,
                    DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                    new String[]{String.valueOf(calendarId), String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static Response.ErrorListener errorMonthListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }

    public static void insertMonth(MonthChecking result, Context context, String TAG, Date first) {
        DBHelper helper = new DBHelper(context);
        String strMonth = Utils.setCalendarDate("MMMMyyyy", first);
        try {
            ContentValues contentValuesCalendarChecking = new ContentValues();
            contentValuesCalendarChecking.put(DBHelper.MONTH_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesCalendarChecking.put(DBHelper.MONTH_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesCalendarChecking.put(DBHelper.MONTH_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesCalendarChecking.put(DBHelper.MONTH_CHECKING_COLUMN_MONTH, strMonth);
            contentValuesCalendarChecking.put(DBHelper.MONTH_CHECKING_COLUMN_LEVEL, 1);
            context.getContentResolver().insert(MonthCheckingProvider.CONTENT_URI, contentValuesCalendarChecking);
            ContentValues[] contentValueses = new ContentValues[result.getMonths().size()];
            for (int i = 0; i < result.getMonths().size(); i++) {
                ContentValues contentValues = new ContentValues();
                String showDate = result.getMonths().get(i).getShowDate();
                contentValues.put(DBHelper.MONTH_COLUMN_SHOW_DATE, showDate);
                contentValues.put(DBHelper.MONTH_COLUMN_P, String.valueOf(result.getMonths().get(i).getP()));
                contentValues.put(DBHelper.MONTH_COLUMN_B, String.valueOf(result.getMonths().get(i).getB()));
                contentValues.put(DBHelper.MONTH_COLUMN_C, String.valueOf(result.getMonths().get(i).getC()));
                contentValues.put(DBHelper.MONTH_COLUMN_L, String.valueOf(false));
                contentValues.put(DBHelper.MONTH_COLUMN_MONTH, strMonth);
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (Object ob : contentValueses) {
                if (ob != null) {
                    isEmpty = false;
                }
            }
            long rowsInserted = 0;
            if (!isEmpty) {
                rowsInserted = context.getContentResolver().bulkInsert(MonthProvider.CONTENT_URI, contentValueses);
                Log.i(TAG, String.valueOf(rowsInserted));
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        } finally {
            Intent intent = new Intent(CalendarActivity.ACTION);
            intent.putExtra("refresh", true);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }
    }

    public static boolean isMonthExists(Context context, Object showDate) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(MonthProvider.CONTENT_URI, null, DBHelper.MONTH_COLUMN_SHOW_DATE + "=?", new String[]{String.valueOf(showDate)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return isExists;
    }
    //endregion

    //region Leave
    public static Leave getLeave(int id, Context context) {
        Leave leave = null;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(LeaveProvider.CONTENT_URI,
                    null, DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                leave = new Leave();
                leave = Leave.getLeave(cursor, 0);
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return leave;
    }

    public static void getLeaveGson(int id, Context context) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Leave> mGsonRequest = new GsonRequest<Leave>(
                Request.Method.GET,
                Utils.API_URL + "api/Leave/" + Utils.encode(id),
                Leave.class,
                headers,
                responseLeaveListener(context, id),
                errorLeaveListener(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, "MONTH");
    }

    public static Response.Listener<Leave> responseLeaveListener(final Context context, final int id) {
        return new Response.Listener<Leave>() {
            @Override
            public void onResponse(Leave response) {
                if (response == null) {
                    context.startActivity(new Intent(context, LeaveActivity.class));
                    ((Activity) context).finish();
                } else {
                    if (response.getId() != id) {
                        context.startActivity(new Intent(context, LeaveActivity.class));
                        ((Activity) context).finish();
                    } else {
                        try {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_ID, response.getId());
                            contentValues.put(DBHelper.LEAVE_COLUMN_NAME, response.getName());
                            contentValues.put(DBHelper.LEAVE_COLUMN_EMPLOYEE_ID, response.getEmployeeId());
                            contentValues.put(DBHelper.LEAVE_COLUMN_DEPARTMENT, response.getDepartment());
                            contentValues.put(DBHelper.LEAVE_COLUMN_POSITION, response.getPosition());
                            contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR, response.getSuperior());
                            contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR_ID, response.getSuperiorId());
                            contentValues.put(DBHelper.LEAVE_COLUMN_TYPE_CODE, response.getTypeCode());
                            contentValues.put(DBHelper.LEAVE_COLUMN_TYPE, response.getType());
                            contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, response.getRemarks());
                            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_DAYS, response.getLeaveDays());
                            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_FROM, response.getLeaveFrom());
                            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_TO, response.getLeaveTo());
                            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, response.getStatus());
                            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_REMARKS, response.getStatusRemark());
                            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_BY, response.getStatusUpdateBy());
                            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_ON, response.getStatusUpdateOn());
                            contentValues.put(DBHelper.LEAVE_COLUMN_LAST_UPDATE, response.getLastUpdate());
                            if (response.getSuperiorId().equals(Utils.getProfile(context).getId())) {
                                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                            } else if (!response.getSuperiorId().equals(Utils.getProfile(context).getId()) && !response.getEmployeeId().equals(Utils.getProfile(context).getId())) {
                                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                            } else if (!response.getSuperiorId().equals(Utils.getProfile(context).getId()) && response.getEmployeeId().equals(Utils.getProfile(context).getId())) {
                                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
                            }
                            contentValues.put(DBHelper.LEAVE_COLUMN_CREATE_DATE, response.getCreateDate());
                            context.getContentResolver().insert(LeaveProvider.CONTENT_URI, contentValues);

                            Intent intent = new Intent(context, EditApplicationActivity.class);
                            intent.putExtra("leave", response);
                            intent.putExtra("isCancelled", response.getType().equals("cancelled"));
                            if (response.getSuperiorId().equals(Utils.getProfile(context).getId())) {
                                intent.putExtra("isEmployeeLeave", true);
                            } else if (!response.getSuperiorId().equals(Utils.getProfile(context).getId()) && !response.getEmployeeId().equals(Utils.getProfile(context).getId())) {
                                intent.putExtra("isEmployeeLeave", true);
                                intent.putExtra("isBoss", true);
                            } else if (!response.getSuperiorId().equals(Utils.getProfile(context).getId()) && response.getEmployeeId().equals(Utils.getProfile(context).getId())) {
                                intent.putExtra("isEmployeeLeave", false);
                                intent.putExtra("isBoss", false);
                            }
                            intent.putExtra("page", -1);
                            context.startActivity(intent);
                        } catch (Exception ex) {
                            Log.i("MONTH", "Content Provider Edit Application Error :" + ex.toString());
                        }
                    }
                }

            }
        };
    }

    public static Response.ErrorListener errorLeaveListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }
//endregion

    //region Delete Calendar
    public static void deleteCalendar(Context context, int id, String TAG) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.DELETE,
                Utils.API_URL + "api/Calendar/" + id + "?Remove=" + String.valueOf(true),
                JsonObject.class,
                headers,
                responseDeleteListener(context, true, id),
                errorDeleteListener(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JsonObject> responseDeleteListener(final Context context, final boolean remove, final int id) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                if (remove) {
                    context.getContentResolver().delete(CalendarProvider.CONTENT_URI, DBHelper.CALENDAR_COLUMN_CALENDAR_ID
                            + "=?", new String[]{String.valueOf(id)});
                } else {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.CALENDAR_COLUMN_STATUS, "inactive");
                    context.getContentResolver().update(CalendarProvider.CONTENT_URI, contentValues,
                            DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(id)});
                }
                Utils.showError(context, "", "Success deleted");
            }
        };
    }

    public static Response.ErrorListener errorDeleteListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }
//endregion

    //region Reject Calendar
    public static void rejectEvent(boolean removeFromList, final Context context, int id, String TAG) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        final String finalUrl = Utils.API_URL + "api/Calendar/Attendees/Rejected?id="
                + id + "&RemoveFromList=" + removeFromList;
        Log.i(TAG, finalUrl);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.PUT,
                finalUrl,
                JsonObject.class,
                headers,
                responseRejectListener(context, id),
                errorRejectListener(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JsonObject> responseRejectListener(final Context context, final int id) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
          /*ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.ATTENDEES_COLUMN_STATUS, "reject");
                context.getContentResolver().update(AttendeesProvider.CONTENT_URI, contentValues,
                        DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " +
                                DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                        new String[]{String.valueOf(id),
                                String.valueOf(Utils.getProfile(context).getId())});*/

                context.getContentResolver().delete(CalendarProvider.CONTENT_URI,
                        DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(id)});

                context.getContentResolver().delete(AttendeesProvider.CONTENT_URI,
                        DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(id)});

                Intent intent = new Intent(CalendarEventDetailsActivity.ACTION);
                intent.putExtra("isSuccess", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    public static Response.ErrorListener errorRejectListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(context, error);
                Intent intent = new Intent(CalendarEventDetailsActivity.ACTION);
                intent.putExtra("isFail", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }
//endregion

    //region Favorite
    public static void getFavoriteGson(int pageNo, Context context, String TAG, final TextView txtError) {
        final String token = Utils.getToken(context);
        Log.i(TAG, token);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        final Date lastUpdateDate = getFavoriteLastUpdate(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
        }
        final String date = lastUpdate;
        GsonRequest<FavoriteContact> mGsonRequest = new GsonRequest<FavoriteContact>(
                Request.Method.GET,
                Utils.API_URL + "api/Employee/FavouriteContact?NoPerPage=" +
                        Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo) +
                        "&LastUpdated=" + Utils.encode(date) + "&ReturnEmpty=" +
                        Utils.encode(getContactRow(context, pageNo) > 0) + "",
                FavoriteContact.class,
                headers,
                responseFavoriteListener(context, pageNo, TAG, txtError),
                errorListener(context, TAG, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<FavoriteContact> responseFavoriteListener(final Context context, final int pageNo, final String TAG, final TextView txtError) {
        return new Response.Listener<FavoriteContact>() {
            @Override
            public void onResponse(FavoriteContact response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getContacts().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getFavoriteLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI,
                                                null, null);
                                    }
                                    insertFavoriteContact(context, response, pageNo, TAG);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI,
                                                null, null);
                                    }
                                    insertFavoriteContact(context, response, pageNo, TAG);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI,
                                            null, null);
                                }
                                insertFavoriteContact(context, response, pageNo, TAG);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    public static Date getFavoriteLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.FAVORITE_CONTACT_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(FavoriteContactProvider.CONTENT_URI,
                    projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.FAVORITE_CONTACT_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static int getContactRow(Context context, int pageNo) {
        int totalRow = 0;
        String[] projection = {DBHelper.CONTACT_COLUMN_ID};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ContactProvider.CONTENT_URI,
                    projection, DBHelper.CONTACT_COLUMN_PAGE_NO + "=?",
                    new String[]{String.valueOf(pageNo)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Response.ErrorListener errorListener(final Context context, final String TAG, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

    public static void insertFavoriteContact(Context context, FavoriteContact result, int pageNo, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesFavorite = new ContentValues();
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_NO_PER_PAGE, result.getNoPerPage());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_PAGE_NO, pageNo);
            context.getContentResolver().insert(FavoriteContactProvider.CONTENT_URI, contentValuesFavorite);
            ContentValues[] contentValueses = new ContentValues[result.getContacts().size()];
            for (int i = 0; i < result.getContacts().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.CONTACT_COLUMN_ALL_BRANCH, result.getContacts().get(i).getAllBranch());
                contentValues.put(DBHelper.CONTACT_COLUMN_BRANCH_ID, result.getContacts().get(i).getDefaultBranchId());
                contentValues.put(DBHelper.CONTACT_COLUMN_BRANCH_NAME, result.getContacts().get(i).getDefaultBranchName());
                contentValues.put(DBHelper.CONTACT_COLUMN_CONTACT_NO, result.getContacts().get(i).getContactNo());
                contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY, result.getContacts().get(i).getCountry());
                contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY_IMAGE, result.getContacts().get(i).getCountryImage());
                contentValues.put(DBHelper.CONTACT_COLUMN_DEPARTMENT, result.getContacts().get(i).getDepartment());
                contentValues.put(DBHelper.CONTACT_COLUMN_PAGE_NO, pageNo);
                contentValues.put(DBHelper.CONTACT_COLUMN_EMAIL, result.getContacts().get(i).getEmail());
                contentValues.put(DBHelper.CONTACT_COLUMN_FAVORITE_ID, result.getContacts().get(i).getFavouriteId());
                contentValues.put(DBHelper.CONTACT_COLUMN_GENDER, result.getContacts().get(i).getGender());
                contentValues.put(DBHelper.CONTACT_COLUMN_NAME, result.getContacts().get(i).getName());
                contentValues.put(DBHelper.CONTACT_COLUMN_LAST_UPDATED, result.getContacts().get(i).getLastUpdated());
                contentValues.put(DBHelper.CONTACT_COLUMN_IMAGE, result.getContacts().get(i).getProfileImage());
                contentValues.put(DBHelper.CONTACT_COLUMN_POSITION, result.getContacts().get(i).getPosition());
                contentValues.put(DBHelper.CONTACT_COLUMN_EMPLOYEE_NO, result.getContacts().get(i).getEmployeeNo());
                contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY_NAME, result.getContacts().get(i).getCountryName());
                contentValues.put(DBHelper.CONTACT_COLUMN_DOB, result.getContacts().get(i).getDOB());
                contentValues.put(DBHelper.CONTACT_COLUMN_OFFICE_NO, result.getContacts().get(i).getOfficeNo());
                contentValues.put(DBHelper.CONTACT_COLUMN_HOD_ID, result.getContacts().get(i).getHODId());
                contentValues.put(DBHelper.CONTACT_COLUMN_HOD_NAME, result.getContacts().get(i).getHODName());
                contentValues.put(DBHelper.CONTACT_COLUMN_OXUSER, result.getContacts().get(i).getOXUser());
                contentValues.put(DBHelper.CONTACT_COLUMN_ID, result.getContacts().get(i).getId());
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(ContactProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }
//endregion

    //region Employee
    public static void getDepartmentGson(int pageNo, boolean isSearch, Context context, String searchKey, final String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(Utils.getProfile(context).getCountry()) + "&NoPerPage=" +
                Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getDepartmentEmployeeRow(context, pageNo) > 0);
        final Date lastUpdateDate = getDepartmentLastUpdate(context);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(Utils.getProfile(context).getCountry()) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getDepartmentEmployeeRow(context, pageNo) > 0);
        }
        if (isSearch) {
            url = Utils.API_URL + "api/Employee/FilterNameDepartment?SearchKey=" + Utils.encode(searchKey) + "&branchId=" +
                    Utils.encode(Utils.getProfile(context).getDefaultBranchId()) + "&exceptBranch=" + Utils.encode(false) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo);
        }
        final String finalUrl = url;
        GsonRequest<EmployeeChecking> mGsonRequest = new GsonRequest<EmployeeChecking>(
                Request.Method.GET,
                finalUrl,
                EmployeeChecking.class,
                headers,
                responseDepartmentListener(context, pageNo, txtError, TAG, isSearch, searchKey),
                errorDepartmentListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static int getDepartmentEmployeeRow(Context context, int pageNo) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DepartmentEmployeeProvider.CONTENT_URI, null, DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_PAGE_NO + "=?", new String[]{String.valueOf(pageNo)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getDepartmentLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DepartmentEmployeeProvider.CONTENT_URI, projection, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static Response.ErrorListener errorDepartmentListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }

    public static Response.Listener<EmployeeChecking> responseDepartmentListener(final Context context, final int pageNo, final TextView txtError, final String TAG, final boolean isSearch, final String searchKey) {
        return new Response.Listener<EmployeeChecking>() {
            @Override
            public void onResponse(EmployeeChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getEmployees().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getDepartmentLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNo, TAG, isSearch, searchKey);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNo, TAG, isSearch, searchKey);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                }
                                insertEmployee(context, response, pageNo, TAG, isSearch, searchKey);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                }
                            }
                        }
                    }
                }
            }

        };
    }

    public static void insertEmployee(Context context, EmployeeChecking result, int pageNo, String TAG, boolean isSearch, String searchKey) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesEmployeeChecking = new ContentValues();
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_PAGE_NO, pageNo);
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT, String.valueOf(true));
            context.getContentResolver().insert(EmployeeCheckingProvider.CONTENT_URI, contentValuesEmployeeChecking);
            ContentValues[] contentValueses = new ContentValues[result.getEmployees().size()];
            for (int i = 0; i < result.getEmployees().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID, result.getEmployees().get(i).getId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IMAGE, result.getEmployees().get(i).getProfileImage());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_ID, result.getEmployees().get(i).getDefaultBranchId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_PAGE_NO, pageNo);
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_NAME, result.getEmployees().get(i).getDefaultBranchName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMAIL, result.getEmployees().get(i).getEmail());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DEPARTMENT, result.getEmployees().get(i).getDepartment());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_NAME, result.getEmployees().get(i).getName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_CONTACT_NO, result.getEmployees().get(i).getContactNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_GENDER, result.getEmployees().get(i).getGender());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY, result.getEmployees().get(i).getCountry());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE, result.getEmployees().get(i).getCountryImage());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_OXUSER, result.getEmployees().get(i).getOXUser());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_POSITION, result.getEmployees().get(i).getPosition());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_ALL_BRANCH, result.getEmployees().get(i).getAllBranch());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_LAST_UPDATED, result.getEmployees().get(i).getLastUpdated());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_NO, result.getEmployees().get(i).getEmployeeNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_NAME, result.getEmployees().get(i).getCountryName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DOB, result.getEmployees().get(i).getDOB());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_OFFICE_NO, result.getEmployees().get(i).getOfficeNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_HOD_ID, result.getEmployees().get(i).getHODId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_HOD_NAME, result.getEmployees().get(i).getHODName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IS_SELECTED, result.getEmployees().get(i).getFavouriteId() > 0 ? String.valueOf(true) : String.valueOf(false));
                if (isDepartmentExists(result.getEmployees().get(i).getId(), context)) {
                    context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(result.getEmployees().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(DepartmentEmployeeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    public static boolean isDepartmentExists(Object employeeId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DepartmentEmployeeProvider.CONTENT_URI, null,
                    DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=? ", new String[]{String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }
//endregion

    //region Country
    public static void getCountryGson(Context context, int pageNoCountry, final String TAG, final TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String lastUpdate = "2000-09-09T10:16:25z";
        final Date lastUpdateDate = getCountryLastUpdate(context);
        String url = Utils.API_URL + "api/Country?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                + Utils.encode(pageNoCountry) + "&LastUpdated=" +
                Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getCountryRow(context) == 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Country?&NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                    + Utils.encode(pageNoCountry) + "&LastUpdated=" +
                    Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getCountryRow(context) == 0);
        }
        final String finalUrl = url;
        GsonRequest<CountryChecking> mGsonRequest = new GsonRequest<CountryChecking>(
                Request.Method.GET,
                finalUrl,
                CountryChecking.class,
                headers,
                responseListener(context, pageNoCountry, TAG, txtError),
                errorListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<CountryChecking> responseListener(final Context context,
                                                                      final int pageNoCountry,
                                                                      final String TAG, final TextView txtError) {
        return new Response.Listener<CountryChecking>() {
            @Override
            public void onResponse(CountryChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getCountry().size() > 0) {
                            Date date1 = Utils.setDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getCountryLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNoCountry == 1) {
                                        context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNoCountry > 1) {
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertCountry(response, context, pageNoCountry, TAG);
                                } else {
                                    if (pageNoCountry == 1) {
                                        context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNoCountry > 1) {
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertCountry(response, context, pageNoCountry, TAG);
                                }
                            } else {
                                if (pageNoCountry == 1) {
                                    context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                } else if (pageNoCountry > 1) {
                                    context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                }
                                insertCountry(response, context, pageNoCountry, TAG);
                            }
                        } else {
                            if (pageNoCountry == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    public static int getCountryRow(Context context) {
        int totalRow = 0;
        String[] projection = {DBHelper.COUNTRY_COLUMN_ID};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CountryProvider.CONTENT_URI,
                    projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getCountryLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.COUNTRY_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CountryCheckingProvider.CONTENT_URI,
                    projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.
                                    COUNTRY_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static boolean isCountryExist(Context context, Object id) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CountryProvider.CONTENT_URI, null,
                    DBHelper.COUNTRY_COLUMN_COUNTRY_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void insertCountry(CountryChecking result, Context context, int pageNoCountry, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesCountryChecking = new ContentValues();
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_LEVEL, pageNoCountry);
            context.getContentResolver().insert(CountryCheckingProvider.CONTENT_URI, contentValuesCountryChecking);
            ContentValues[] contentValueses = new ContentValues[result.getCountry().size()];
            for (int i = 0; i < result.getCountry().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_ID, result.getCountry().get(i).getId());
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_CODE, result.getCountry().get(i).getCountryCode());
                contentValues.put(DBHelper.COUNTRY_COLUMN_LEVEL, pageNoCountry);
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_NAME, result.getCountry().get(i).getCountryName());
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_IMAGE, result.getCountry().get(i).getCountryImage());
                contentValues.put(DBHelper.COUNTRY_COLUMN_LAST_UPDATED, result.getCountry().get(i).getLastUpdated());
                if (isCountryExist(context, result.getCountry().get(i).getId())) {
                    context.getContentResolver().delete(CountryProvider.CONTENT_URI, DBHelper.COUNTRY_COLUMN_COUNTRY_ID + "=?",
                            new String[]{String.valueOf(result.getCountry().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(CountryProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Country Error :" + ex.toString());
        }
    }

    public static Response.ErrorListener errorListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }
//endregion

    //region Country Employee region
    public static void getCountryEmployeeGson(Context context, int pageNoEmployee, boolean isSearch, String countryCode, String TAG, String searchKey, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(countryCode) + "&NoPerPage=" +
                Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoEmployee) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getBranchEmployeeRow(context, pageNoEmployee, countryCode) > 0);
        final Date lastUpdateDate = getBranchEmployeeLastUpdate(context);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(countryCode) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoEmployee) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getBranchEmployeeRow(context, pageNoEmployee, countryCode) > 0);
        }

        if (isSearch) {
            url = Utils.API_URL + "api/Employee/FilterNameDepartment?SearchKey=" + Utils.encode(searchKey) + "&branchId=" +
                    Utils.encode(Utils.getProfile(context).getDefaultBranchId()) + "&exceptBranch=" + Utils.encode(true) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoEmployee);
        }
        final String finalUrl = url;
        Log.i(TAG, "Url: " + finalUrl);
        GsonRequest<EmployeeChecking> mGsonRequest = new GsonRequest<EmployeeChecking>(
                Request.Method.GET,
                finalUrl,
                EmployeeChecking.class,
                headers,
                responseBranchEmployeeListener(context, pageNoEmployee, isSearch, TAG, searchKey, countryCode, txtError),
                errorBranchEmployeeListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static int getBranchEmployeeRow(Context context, int pageNo, String countryCode) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(BranchEmployeeProvider.CONTENT_URI, null,
                    DBHelper.BRANCH_EMPLOYEE_COLUMN_PAGE_NO + "=? AND " + DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{String.valueOf(pageNo), countryCode}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getBranchEmployeeLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(EmployeeCheckingProvider.CONTENT_URI, projection, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }


    public static Response.Listener<EmployeeChecking> responseBranchEmployeeListener(final Context context, final int pageNoEmployee, final boolean isSearch, final String TAG, final String searchKey, final String countryCode, final TextView txtError) {
        return new Response.Listener<EmployeeChecking>() {
            @Override
            public void onResponse(EmployeeChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getEmployees().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getBranchEmployeeLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNoEmployee == 1) {
                                        context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNoEmployee > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNoEmployee, isSearch, TAG, searchKey, countryCode);
                                } else {
                                    if (pageNoEmployee == 1) {
                                        context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNoEmployee > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNoEmployee, isSearch, TAG, searchKey, countryCode);
                                }
                            } else {
                                if (pageNoEmployee == 1) {
                                    context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                } else if (pageNoEmployee > 1) {
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                }
                                insertEmployee(context, response, pageNoEmployee, isSearch, TAG, searchKey, countryCode);
                            }
                        } else {
                            if (pageNoEmployee == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                }
                            }
                        }
                    }
                }
            }

        };
    }

    public static void insertEmployee(Context context, EmployeeChecking result, int pageNoBranch, boolean isSearch, String TAG, String searchKey, String countryCode) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesEmployeeChecking = new ContentValues();
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH, String.valueOf(true));
            context.getContentResolver().insert(EmployeeCheckingProvider.CONTENT_URI, contentValuesEmployeeChecking);
            ContentValues[] contentValueses = new ContentValues[result.getEmployees().size()];
            for (int i = 0; i < result.getEmployees().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID, result.getEmployees().get(i).getId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IMAGE, result.getEmployees().get(i).getProfileImage());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_BRANCH_ID, result.getEmployees().get(i).getDefaultBranchId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_PAGE_NO, pageNoBranch);
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_BRANCH_NAME, result.getEmployees().get(i).getDefaultBranchName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMAIL, result.getEmployees().get(i).getEmail());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_DEPARTMENT, result.getEmployees().get(i).getDepartment());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_NAME, result.getEmployees().get(i).getName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_CONTACT_NO, result.getEmployees().get(i).getContactNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_GENDER, result.getEmployees().get(i).getGender());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY, result.getEmployees().get(i).getCountry());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY_IMAGE, result.getEmployees().get(i).getCountryImage());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_OXUSER, result.getEmployees().get(i).getOXUser());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_POSITION, result.getEmployees().get(i).getPosition());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_ALL_BRANCH, result.getEmployees().get(i).getAllBranch());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_LAST_UPDATED, result.getEmployees().get(i).getLastUpdated());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_NO, result.getEmployees().get(i).getEmployeeNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY_NAME, result.getEmployees().get(i).getCountryName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_DOB, result.getEmployees().get(i).getDOB());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_OFFICE_NO, result.getEmployees().get(i).getOfficeNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_HOD_ID, result.getEmployees().get(i).getHODId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_HOD_NAME, result.getEmployees().get(i).getHODName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IS_SELECTED, result.getEmployees().get(i).getFavouriteId() > 0 ? String.valueOf(true) : String.valueOf(false));
                if (isBanchExists(result.getEmployees().get(i).getId(), context)) {
                    context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(result.getEmployees().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(BranchEmployeeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    public static boolean isBanchExists(Object employeeId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(BranchEmployeeProvider.CONTENT_URI, null,
                    DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?",
                    new String[]{String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static Response.ErrorListener errorBranchEmployeeListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

//endregion

    //region Put Calendar
    public static void putCalendarData(final Context context, final String id, final String title, final String startDate,
                                       final String endDate, final boolean allDay, final String location,
                                       final String description, final String attendees, final String reminder, final String TAG,
                                       final int value, final int page, final SharedPreferences.Editor editor, final ProgressBar progressBar,
                                       final RelativeLayout fadeRL) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String url = Utils.API_URL + "api/Calendar";
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.PUT,
                Utils.API_URL + "api/Calendar",
                JsonObject.class,
                headers,
                responsePutCalendarListener(id, title, startDate, endDate, allDay,
                        location, description, attendees, reminder, value, context, TAG, page, editor),
                errorPutCalendarListener(context, fadeRL, progressBar)) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "Id=" + Utils.encode(id)
                        + "&Title=" + Utils.encode(title)
                        + "&Description=" + Utils.encode(description)
                        + "&AllDay=" + Utils.encode(String.valueOf(allDay).toLowerCase())
                        + "&CalendarFrom=" + Utils.encode(startDate)
                        + "&CalendarTo=" + Utils.encode(endDate)
                        + "&Location=" + Utils.encode(location)
                        + reminder
                        + attendees
                        + "&AlertAttendees=" + true;
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JsonObject>
    responsePutCalendarListener(final String id, final String title, final String startDate,
                                final String endDate, final boolean allDay, final String location,
                                final String description, final String attendees, final String reminder,
                                final int value, final Context context, final String TAG, final int page,
                                final SharedPreferences.Editor editor) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                try {
                    editor.clear();
                    editor.apply(); // commit changes
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.CALENDAR_COLUMN_TITLE, title);
                    contentValues.put(DBHelper.CALENDAR_COLUMN_CALENDAR_ID, id);
                    contentValues.put(DBHelper.CALENDAR_COLUMN_DESCRIPTION, description);
                    contentValues.put(DBHelper.CALENDAR_COLUMN_ALL_DAY, String.valueOf(allDay));
                    contentValues.put(DBHelper.CALENDAR_COLUMN_LOCATION, location);
                    contentValues.put(DBHelper.CALENDAR_COLUMN_START_DATE, startDate);
                    contentValues.put(DBHelper.CALENDAR_COLUMN_REMINDER, reminder);
                    contentValues.put(DBHelper.CALENDAR_COLUMN_END_DATE, endDate);
                    if (value > 0) {
                        com.applab.wcircle_pro.Calendar.Calendar calendar = new com.applab.wcircle_pro.Calendar.Calendar();
                        calendar.setId(Integer.valueOf(id));
                        HttpHelper.getSingleCalendarGson(context, calendar, TAG, null);
                        Toast.makeText(context, "Success updated", Toast.LENGTH_LONG).show();
                        /*context.getContentResolver().update(CalendarProvider.CONTENT_URI, contentValues, "id = ?", new String[]{Integer.toString(value)});
                        context.getContentResolver().delete(AttendeesProvider.CONTENT_URI, DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(value)});
                        insertBackOriAttendees(String.valueOf(value), value, context);*/
                        context.getContentResolver().delete(TempAttendeesProvider.CONTENT_URI, null, null);
                    }
                    if (location != null) {
                        if (!location.equals("")) {
                            if (!Utils.isPlaceExits(context, location)) {
                                contentValues = new ContentValues();
                                contentValues.put(DBHelper.PLACE_COLUMN_NAME, location);
                                Long id = Long.valueOf(context.getContentResolver().insert(PlaceProvider.CONTENT_URI, contentValues).getLastPathSegment());
                                Log.i(TAG, String.valueOf(id));
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.fillInStackTrace();
                } finally {
                    ((Activity) context).finish();
                }

            }
        };
    }

    public static void insertBackOriAttendees(String calendarId, int value, Context context) {
        Uri uri = TempAttendeesProvider.CONTENT_URI;
        String selection = DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID + "=?";
        String[] selectionArgs = {String.valueOf(value)};
        Cursor cursor = context.getContentResolver().query(uri, null, selection, selectionArgs, DBHelper.TEMP_ATTENDEES_COLUMN_ID + " ASC ");
        if (cursor != null && cursor.moveToFirst()) {
            ContentValues[] contentValueses = new ContentValues[cursor.getCount()];
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.ATTENDEES_COLUMN_IS_SUBMIT, "1");
                contentValues.put(DBHelper.ATTENDEES_COLUMN_IS_SELECT, cursor.getString(cursor.getColumnIndex(DBHelper.TEMP_ATTENDEES_COLUMN_IS_SELECT)));
                contentValues.put(DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID, cursor.getString(cursor.getColumnIndex(DBHelper.TEMP_ATTENDEES_COLUMN_EMPLOYEE_ID)));
                contentValues.put(DBHelper.ATTENDEES_COLUMN_CALENDAR_ID, calendarId);
                contentValues.put(DBHelper.ATTENDEES_COLUMN_NAME, cursor.getString(cursor.getColumnIndex(DBHelper.TEMP_ATTENDEES_COLUMN_NAME)));
                contentValues.put(DBHelper.ATTENDEES_COLUMN_STATUS, "pending");
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (Object ob : contentValueses) {
                if (ob != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(AttendeesProvider.CONTENT_URI, contentValueses);
            }
            cursor.close();
        }
    }

    public static Response.ErrorListener errorPutCalendarListener(final Context context, final RelativeLayout fadeRL, final ProgressBar progressBar) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Utils.serverHandlingError(context, error);
                } catch (Exception ex) {
                    ex.fillInStackTrace();
                } finally {
                    fadeRL.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }
            }
        };
    }
//endregion

    //region Post Data
    public static void postCalendarData(final Context context, final String employeeId, final String title, final String startDate,
                                        final String endDate, final boolean allDay, final String location,
                                        final String description, final String attendees, final String reminder, final String TAG,
                                        final int value, final int page, final SharedPreferences.Editor editor, final ProgressBar progressBar,
                                        final RelativeLayout fadeRL) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        Log.i(TAG, "Authorization: Bearer " + token);
        String url = Utils.API_URL + "api/Calendar";
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.POST,
                url,
                JsonObject.class,
                headers,
                responsePutCalendarListener(employeeId, title, startDate, endDate, allDay,
                        location, description, attendees, reminder, value, context, TAG, page, editor),
                errorPutCalendarListener(context, fadeRL, progressBar)) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "Title=" + Utils.encode(title)
                        + "&Description=" + Utils.encode(description)
                        + "&AllDay=" + Utils.encode(String.valueOf(allDay).toLowerCase())
                        + "&CalendarFrom=" + Utils.encode(startDate)
                        + "&CalendarTo=" + Utils.encode(endDate)
                        + "&Location=" + Utils.encode(location)
                        + reminder
                        + attendees;
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }
//endregion

    //region Get Single Calendar
    public static void getSingleCalendarGson(Context context,
                                             com.applab.wcircle_pro.Calendar.Calendar calendar, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<com.applab.wcircle_pro.Calendar.Calendar> mGsonRequest = new GsonRequest<com.applab.wcircle_pro.Calendar.Calendar>(
                Request.Method.GET,
                Utils.API_URL + "api/Calendar/" + calendar.getId(),
                com.applab.wcircle_pro.Calendar.Calendar.class,
                headers,
                responseSingleCalendarListener(context, txtError, calendar),
                errorSingleCalendarListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static void insertCalendar(com.applab.wcircle_pro.Calendar.Calendar response, Context context, com.applab.wcircle_pro.Calendar.Calendar calendar) {
        try {
            String calendarTo = response.getCalendarTo();
            String calendarFrom = response.getCalendarFrom();
            String reminderDate = response.getReminder() != null ? response.getReminder() : null;
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.CALENDAR_COLUMN_CALENDAR_ID, response.getId());
            contentValues.put(DBHelper.CALENDAR_COLUMN_TYPE, response.getType());
            contentValues.put(DBHelper.CALENDAR_COLUMN_TITLE, response.getTitle());
            contentValues.put(DBHelper.CALENDAR_COLUMN_ALL_DAY, String.valueOf(response.getAllDay()));
            contentValues.put(DBHelper.CALENDAR_COLUMN_START_DATE, calendarFrom);
            contentValues.put(DBHelper.CALENDAR_COLUMN_END_DATE, calendarTo);
            contentValues.put(DBHelper.CALENDAR_COLUMN_LOCATION, response.getLocation());
            contentValues.put(DBHelper.CALENDAR_COLUMN_DESCRIPTION, response.getDescription());
            contentValues.put(DBHelper.CALENDAR_COLUMN_REMINDER, reminderDate);
            contentValues.put(DBHelper.CALENDAR_COLUMN_STATUS, response.getStatus());
            contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_ID, response.getCreateById());
            contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_NAME, response.getCreateByName());
            contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_ID, response.getUpdateById());
            contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_NAME, response.getUpdateByName());
            contentValues.put(DBHelper.CALENDAR_COLUMN_NEW_CREATE, String.valueOf(response.getNewCreate()));
            if (MonthFragment.isCalendarExists(response.getId(), context)) {
                context.getContentResolver().update(CalendarProvider.CONTENT_URI, contentValues, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?",
                        new String[]{String.valueOf(calendar.getId())});
            } else {
                context.getContentResolver().insert(CalendarProvider.CONTENT_URI, contentValues);
            }
            if (response.getAttendees() != null) {
                for (int j = 0; j < response.getAttendees().size(); j++) {
                    String reminder = response.getAttendees().get(j).getReminder() != null ? response.getAttendees().get(j).getReminder() : null;
                    ContentValues contentValuesAttendees = new ContentValues();
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SUBMIT, 1);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SELECT, 1);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_NAME, response.getAttendees().get(j).getEmployeyName());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID, response.getAttendees().get(j).getEmployeeId());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_CALENDAR_ID, response.getId());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_REMINDER, reminder);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_STATUS, response.getAttendees().get(j).getStatus());
                    if (isAttendeesExists(context, calendar.getId(), response.getAttendees().get(j).getEmployeeId())) {
                        context.getContentResolver().update(AttendeesProvider.CONTENT_URI, contentValuesAttendees,
                                DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                                new String[]{String.valueOf(response.getId()), String.valueOf(response.getAttendees().get(j).getEmployeeId())});
                    } else {
                        context.getContentResolver().insert(AttendeesProvider.CONTENT_URI, contentValuesAttendees);
                    }
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
    }

    public static Response.Listener<com.applab.wcircle_pro.Calendar.Calendar> responseSingleCalendarListener(final Context context, final TextView txtError, final com.applab.wcircle_pro.Calendar.Calendar calendar) {
        return new Response.Listener<com.applab.wcircle_pro.Calendar.Calendar>() {
            @Override
            public void onResponse(com.applab.wcircle_pro.Calendar.Calendar response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                insertCalendar(response, context, calendar);
            }
        };
    }

    public static Response.ErrorListener errorSingleCalendarListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }
//endregion

    //region Reinvite
    public static void reinviteAttendee(final Context context, final int id, final int employeeId, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.PUT,
                Utils.API_URL + "api/Calendar/Reinvite",
                JSONObject.class,
                headers,
                responseListenerReinvite(context, id, employeeId),
                errorListenerReinvite(context, TAG)) {

            @Override
            public byte[] getBody() {
                String httpPostBody = "Id=" + id + "&Attendees=" + employeeId;
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerReinvite(final Context context, final int id, final int employeeId) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.ATTENDEES_COLUMN_STATUS, "pending");
                context.getContentResolver().update(AttendeesProvider.CONTENT_URI, contentValues,
                        DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(id), String.valueOf(employeeId)});

                contentValues = new ContentValues();
                contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_STATUS, "pending");
                context.getContentResolver().update(TempAttendeesProvider.CONTENT_URI, contentValues,
                        DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.TEMP_ATTENDEES_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(id), String.valueOf(employeeId)});

                Intent intent = new Intent(CalendarDetailsActivity.ACTION);
                intent.putExtra("isSuccess", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    public static Response.ErrorListener errorListenerReinvite(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.serverHandlingError(context, error);
                Intent intent = new Intent(CalendarDetailsActivity.ACTION);
                intent.putExtra("isFail", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }
    //endregion

    //region Calendar Read
    public static void getCalendarGsonReading(final Context context, final int id, final String TAG) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + Utils.getToken(context));
        GsonRequest<JSONObject> mGsonRequest = new GsonRequest<JSONObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Tracking",
                JSONObject.class,
                headers,
                responseListenerReading(context, id, TAG),
                errorListenerReading(TAG)) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String httpPostBody = "{\"employeeId\":" +
                        Utils.getProfile(context).getId() +
                        ",\"section\":\"calendar\",\"action\":\"read\",\"id\":[" + id + "]}";
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JSONObject> responseListenerReading(final Context context, final int id, final String TAG) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ContentValues contentValues = new ContentValues();

                if (isBold(context, id)) {
                    contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, DashboardActivity.getNumber(context, "Calendar"));
                    context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                            contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Calendar"});
                    com.applab.wcircle_pro.Dashboard.HttpHelper.getIndicator(context, TAG, null);
                }

                contentValues = new ContentValues();
                contentValues.put(DBHelper.CALENDAR_COLUMN_NEW_CREATE, "false");
                context.getContentResolver().update(CalendarProvider.CONTENT_URI, contentValues,
                        DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(id)});


                Log.i(TAG, "Success send reading status");
            }
        };
    }

    public static boolean isBold(Context context, Object id) {
        boolean isBold = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CalendarProvider.CONTENT_URI, null,
                    DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_NEW_CREATE))) {
                    isBold = Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_NEW_CREATE)));
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isBold;
    }

    public static Response.ErrorListener errorListenerReading(final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i(TAG, "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.i(TAG, "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.i(TAG, "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.i(TAG, "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.i(TAG, "ParseError");
                }
            }
        };
    }
    //endregion
}
