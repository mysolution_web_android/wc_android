package com.applab.wcircle_pro.Calendar;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

public class EditCalendarViewHolder extends RecyclerView.ViewHolder {
    TextView title, txtReinvite;
    ImageView icon, delete;

    public EditCalendarViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.txtName);
        icon = (ImageView) itemView.findViewById(R.id.img);
        delete = (ImageView) itemView.findViewById(R.id.delete);
        txtReinvite = (TextView) itemView.findViewById(R.id.txtReinvite);
    }
}