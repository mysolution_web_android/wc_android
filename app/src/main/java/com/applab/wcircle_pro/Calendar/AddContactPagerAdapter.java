package com.applab.wcircle_pro.Calendar;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.applab.wcircle_pro.R;

class AddContactPagerAdapter extends FragmentPagerAdapter {
    private String[] tabs;

    public AddContactPagerAdapter(FragmentManager fm,Context context) {
        super(fm);
        tabs=context.getResources().getStringArray(R.array.add_contact_tabs);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment item;
        BranchFragment branchFragment = new BranchFragment();
        MyFavoriteFragment myFavorite = new MyFavoriteFragment();
        DepartmentFragment departmentFragment = new DepartmentFragment();
        if (position == 0 ){
            item = myFavorite;
        }
        else if (position==1){
            item = departmentFragment;
        }
        else
            item = branchFragment;
        return item;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }
}