package com.applab.wcircle_pro.Calendar;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.applab.wcircle_pro.Favorite.Contact;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

/**
 * Created by user on 5/7/2015.
 */

public class MyFavoriteAdapter extends RecyclerView.Adapter<MyFavoriteViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;
    private ArrayList<NewEmployee> addedEmployee;
    private ArrayList<Integer> addedId;

    public MyFavoriteAdapter(Context context, Cursor cursor, ArrayList<NewEmployee> addedEmployee, ArrayList<Integer> addedId) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
        this.addedEmployee = addedEmployee;
        this.addedId = addedId;
    }

    @Override
    public MyFavoriteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("ScheduleViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_my_favorite_row, parent, false);
        MyFavoriteViewHolder holder = new MyFavoriteViewHolder(view);
        holder.setIsRecyclable(true);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyFavoriteViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Contact current = Contact.getContact(cursor, position);
        holder.btnAddId.setImageResource(addedId.contains(current.getId()) ? R.mipmap.info_checked : R.mipmap.info_uncheck);
        current.setIsSelect(addedId.contains(current.getId()));
        holder.btnAddId.setTag(current);
        holder.department.setText(current.getDepartment());
        holder.title.setText(current.getName());
        holder.title.setTextColor(ContextCompat.getColor(context, R.color.color_red));
        Glide.with(context)
                .load(current.getProfileImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleTransform(context))
                .placeholder(R.mipmap.ic_action_person_light)
                .into(holder.icon);
        Glide.with(context)
                .load(current.getCountryImage())
                .transform(new CircleTransform(context))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgCountry);
        setCheckBoxOnClickListener(holder.btnAddId);
    }

    private void setCheckBoxOnClickListener(final ImageView btnAddId) {
        btnAddId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Contact contact = (Contact) btnAddId.getTag();
                if (contact.getIsSelect()) {
                    if (addedId.contains(contact.getId())) {
                        btnAddId.setImageResource(R.mipmap.info_uncheck);
                        Intent intent = new Intent(AddContactActivity.ACTION);
                        int row = addedId.indexOf(contact.getId());
                        addedId.remove(row);
                        addedEmployee.remove(row);
                        intent.putParcelableArrayListExtra("addedEmployee", addedEmployee);
                        intent.putExtra("isDepartment", true);
                        intent.putExtra("isMain", true);
                        intent.putExtra("isBranch", true);
                        intent.putExtra("isFavoriteChange", true);;
                        AddContactActivity.mgr.sendBroadcast(intent);
                        contact.setIsSelect(false);
                        btnAddId.setTag(contact);
                    }
                } else {
                    if (!addedId.contains(contact.getId())) {
                        btnAddId.setImageResource(R.mipmap.info_checked);
                        Intent intent = new Intent(AddContactActivity.ACTION);
                        NewEmployee newEmployee = new NewEmployee();
                        newEmployee.setId(contact.getId());
                        newEmployee.setName(contact.getName());
                        newEmployee.setPosition(contact.getPosition());
                        newEmployee.setCountryImage(contact.getCountryImage());
                        newEmployee.setImage(contact.getProfileImage());
                        newEmployee.setOxUser(contact.getOXUser());
                        addedEmployee.add(newEmployee);
                        addedId.add(contact.getId());
                        intent.putParcelableArrayListExtra("addedEmployee", addedEmployee);
                        intent.putExtra("isDepartment", true);
                        intent.putExtra("isBranch", true);
                        intent.putExtra("isMain", true);
                        intent.putExtra("isFavoriteChange", true);
                        AddContactActivity.mgr.sendBroadcast(intent);
                        contact.setIsSelect(true);
                        btnAddId.setTag(contact);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);

        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            MyFavoriteAdapter.this.notifyDataSetChanged();
        }
    };
}