package com.applab.wcircle_pro.Calendar;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by user on 5/8/2015.
 */
public class CalendarAdapter  extends RecyclerView.Adapter<CalendarViewHolder>{
    private LayoutInflater inflater;
    private Context context;

    private static final String tag = "CalendarAdapter";
    private final List<String> list;
    private final String[] weekdays = new String[] { "SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT" };
    private final String[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
    private final int[] daysOfMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    private int month, year;
    int daysInMonth, prevMonthDays;
    private int currentDayOfMonth,currentMonth,currentYear;
    private ArrayList<Schedule> arrSchedule;

    public CalendarAdapter( Context context,int month, int year) {
        this.inflater = LayoutInflater.from(context);;
        this.context = context;
        this.list = new ArrayList<String>();
        this.month = month;
        this.year = year;
        Log.d(tag, "Month: " + month + " " + "Year: " + year);
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        currentDayOfMonth = calendar.get(java.util.Calendar.DAY_OF_MONTH);
        currentMonth = calendar.get(java.util.Calendar.MONTH);
        currentYear = calendar.get(java.util.Calendar.YEAR);
        try {
            arrSchedule = (ArrayList<Schedule>) getData();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        printMonth(month, year);
    }

    @Override
    public CalendarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("CalendarViewHolder", "onCreateViewHolder called");
        View view=inflater.inflate(R.layout.custom_calendar,parent,false);
        CalendarViewHolder holder = new CalendarViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CalendarViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        String[] day_color = list.get(position).split("-");
        holder.txtDayNo.setText(day_color[0]);
        holder.rl.setTag(day_color[0] + "-" + day_color[2] + "-" + day_color[3]);
        if (day_color[1].equals("GREY"))
        {
            holder.txtDayNo.setTextColor(Color.LTGRAY);
        }
        if (day_color[1].equals("WHITE"))
        {
            holder.txtDayNo.setTextColor(Color.BLACK);
        }
        if (day_color[0].equals(String.valueOf(currentDayOfMonth)) &&
                day_color[2].equals(months[currentMonth]) &&
                day_color[3].equals(String.valueOf(currentYear)))
        {
            holder.rl.setBackgroundColor(Color.RED);
            holder.txtDayNo.setTextColor(Color.WHITE);
        }
        setVisibilityImageView(Integer.valueOf(day_color[4]),holder);
        setVisibilityImageView(Integer.valueOf(day_color[5]),holder);
        setVisibilityImageView(Integer.valueOf(day_color[6]),holder);
        setVisibilityImageView(Integer.valueOf(day_color[7]),holder);
        setVisibilityImageView(Integer.valueOf(day_color[8]),holder);
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    private void printMonth(int mm, int yy)
    {
        // The number of days to leave blank at
        // the start of this month.
        int trailingSpaces = 0;
        int leadSpaces = 0;
        int daysInPrevMonth = 0;
        int prevMonth = 0;
        int prevYear = 0;
        int nextMonth = 0;
        int nextYear = 0;

        GregorianCalendar cal = new GregorianCalendar(yy, mm, currentDayOfMonth);

        // Days in Current Month
        daysInMonth = daysOfMonth[mm];
        int currentMonth = mm;
        if (currentMonth == 11)
        {
            prevMonth = 10;
            daysInPrevMonth = daysOfMonth[prevMonth];
            nextMonth = 0;
            prevYear = yy;
            nextYear = yy + 1;
        } else if (currentMonth == 0)
        {
            prevMonth = 11;
            prevYear = yy - 1;
            nextYear = yy;
            daysInPrevMonth = daysOfMonth[prevMonth];
            nextMonth = 1;
        } else
        {
            prevMonth = currentMonth - 1;
            nextMonth = currentMonth + 1;
            nextYear = yy;
            prevYear = yy;
            daysInPrevMonth = daysOfMonth[prevMonth];
        }

        // Compute how much to leave before before the first day of the
        // month.
        // getDay() returns 0 for Sunday.
        trailingSpaces = cal.get(java.util.Calendar.DAY_OF_WEEK) - 1;

        if (cal.isLeapYear(cal.get(java.util.Calendar.YEAR)) && mm == 1)
        {
            ++daysInMonth;
        }

        // Trailing Month days
        for (int i = 0; i < trailingSpaces; i++)
        {
            String day = String.valueOf((daysInPrevMonth - trailingSpaces + 1) + i);
            String month = months[prevMonth];
            String year = String.valueOf(prevYear);
            int red=0;
            int green = 0;
            int orange = 0;
            int purple = 0;
            int blue = 0;
            for (int j = 0; j<arrSchedule.size(); j++) {
                String[] strDate = arrSchedule.get(j).time.split("-");
                String monthEvent = months[Integer.valueOf(strDate[1])-1];
                if(day.equals(strDate[0])&&month.equals(monthEvent)&&year.equals(strDate[2])){
                    int color = arrSchedule.get(j).color;
                    if(color ==1 ){
                        red = 1;
                    }
                    else if(color == 2){
                        green = 2;
                    }
                    else if(color == 3){
                        orange = 3;
                    }
                    else if(color == 4){
                        purple = 4;
                    }
                    else if(color == 5){
                        blue = 5;
                    }
                }
            }
            list.add(day + "-GREY" + "-" + month + "-" + year+"-"
                    +red+"-"+green+"-"+orange+"-"+purple+"-"+blue);

        }

        // Current Month Days
        for (int i = 1; i <= daysInMonth; i++)
        {
            String day = String.valueOf(i);
            String month = months[mm];
            String year = String.valueOf(yy);
            int red=0;
            int green = 0;
            int orange = 0;
            int purple = 0;
            int blue = 0;
            for (int j = 0; j<arrSchedule.size(); j++) {
                String[] strDate = arrSchedule.get(j).time.split("-");
                String monthEvent = months[Integer.valueOf(strDate[1])-1];
                if(day.equals(strDate[0])&&month.equals(monthEvent)&&year.equals(strDate[2])){
                    int color = arrSchedule.get(j).color;
                    if(color ==1 ){
                        red = 1;
                    }
                    else if(color == 2){
                        green = 2;
                    }
                    else if(color == 3){
                        orange = 3;
                    }
                    else if(color == 4){
                        purple = 4;
                    }
                    else if(color == 5){
                        blue = 5;
                    }
                }
            }
            list.add(day + "-WHITE" + "-" + month + "-" + year
                    +"-"+red+"-"+green+"-"+orange+"-"+purple+"-"+blue);
        }

        // Leading Month days
        for (int i = 0; i < list.size() % 7; i++)
        {
            String day = String.valueOf(i + 1);
            String month = months[nextMonth];
            String year = String.valueOf(nextYear);
            int red=0;
            int green = 0;
            int orange = 0;
            int purple = 0;
            int blue = 0;
            for (int j = 0; j<arrSchedule.size(); j++) {
                String[] strDate = arrSchedule.get(j).time.split("-");
                String monthEvent = months[Integer.valueOf(strDate[1])-1];
                if(day.equals(strDate[0])&&month.equals(monthEvent)&&year.equals(strDate[2])){
                    int color = arrSchedule.get(j).color;
                    if(color ==1 ){
                        red = 1;
                    }
                    else if(color == 2){
                        green = 2;
                    }
                    else if(color == 3){
                        orange = 3;
                    }
                    else if(color == 4){
                        purple = 4;
                    }
                    else if(color == 5){
                        blue = 5;
                    }
                }
            }
            list.add(day + "-GREY" + "-" + month + "-" + year
                    +"-"+red+"-"+green+"-"+orange+"-"+purple+"-"+blue);
        }
    }

    public List<Schedule> getData() throws ParseException {
        ArrayList<Schedule> arrSchedule = new ArrayList();
        Uri uri = CalendarProvider.CONTENT_URI;
        Cursor cursor =context.getContentResolver().query(uri, null,
                null, null, DBHelper.CALENDAR_COLUMN_TITLE);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Schedule schedule = new Schedule();
            schedule.id = cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_CALENDAR_ID));
//            schedule.color=cursor.getInt(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_COLOR));
            String startDate = cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_START_DATE));
            String year="";
            String month="";
            String day="";
            year = Utils.setDate(Utils.DATE_FORMAT,"yyyy",startDate);
            month = Utils.setDate(Utils.DATE_FORMAT, "M", startDate);
            day = Utils.setDate(Utils.DATE_FORMAT, "d", startDate);
            schedule.time = day+"-"+month+"-"+year;
            arrSchedule.add(schedule);
            cursor.moveToNext();
        }
        cursor.close();
        return arrSchedule;
    }

    private void setVisibilityImageView(int i,CalendarViewHolder holder){
        int color=i;
        switch ( color ) {
            case 1:
                holder.imgRed.setVisibility(View.VISIBLE);
                break;
            case 2:
                holder.imgGreen.setVisibility(View.VISIBLE);
                break;
            case 3:
                holder.imgOrange.setVisibility(View.VISIBLE);
                break;
            case 4:
                holder.imgPurple.setVisibility(View.VISIBLE);
                break;
            case 5:
                holder.imgBlue.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

}
