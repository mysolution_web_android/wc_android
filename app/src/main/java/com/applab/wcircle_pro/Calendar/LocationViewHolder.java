package com.applab.wcircle_pro.Calendar;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 6/19/2015.
 */

public class LocationViewHolder extends RecyclerView.ViewHolder {
    TextView name;

    public LocationViewHolder(View itemView) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.txtName);
    }
}