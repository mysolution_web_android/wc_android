package com.applab.wcircle_pro.Calendar;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.*;
import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.Favorite.ContactProvider;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;

public class MyFavoriteFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    //region variable
    private RecyclerView recyclerView;
    private MyFavoriteAdapter adapter;
    private Cursor cursor;
    private EditText ediSearch;
    private LoaderManager.LoaderCallbacks callbacks;
    private Intent mIntent;
    private LinearLayout btnCancel, btnSubmit;
    private ArrayList<NewEmployee> addedEmployee = new ArrayList<>();
    private ArrayList<Integer> addedId = new ArrayList<>();
    private int id;
    private String selection = DBHelper.CONTACT_COLUMN_ID + "!=?";
    private String[] selectionArgs = new String[]{String.valueOf(id)};
    private String TAG = "MY FAVORITE FRAGMENT";
    private int pageNo = 1;
    private LinearLayoutManager linearLayoutManager;
    private TextView txtError;
    //endregion

    //region create
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_my_favorite, container, false);
        id = Utils.getProfile(getActivity()).getId();
        callbacks = this;
        mIntent = getActivity().getIntent();

        if (mIntent.getParcelableArrayListExtra("addedEmployee") != null) {
            addedEmployee = mIntent.getParcelableArrayListExtra("addedEmployee");
            for (int i = 0; i < addedEmployee.size(); i++) {
                NewEmployee employee = addedEmployee.get(i);
                addedId.add(employee.getId());
            }
        }

        ediSearch = (EditText) layout.findViewById(R.id.ediSearch);
        txtError = (TextView) layout.findViewById(R.id.txtError);
        ediSearch.setOnEditorActionListener(ediSearchOnEditActionListener);

        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        adapter = new MyFavoriteAdapter(getActivity(), cursor, addedEmployee, addedId);
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);

        btnCancel = (LinearLayout) layout.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        btnSubmit = (LinearLayout) layout.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(btnSubmitOnClickListener);
        setOnTouchListener();
        HttpHelper.getFavoriteGson(pageNo, getActivity(), TAG, txtError);
        return layout;
    }
    //endregion

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (totalItemCount > 1) {
                if (lastVisibleItem >= totalItemCount - 1) {
                    pageNo++;
                    HttpHelper.getFavoriteGson(pageNo, getActivity(), TAG, txtError);
                }
            }
        }
    };

    //region on click region
    private LinearLayout.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                ArrayList<ContentValues> contentValueses = new ArrayList<ContentValues>(addedId.size());
                Uri uri = TempAttendeesProvider.CONTENT_URI;
                getActivity().getContentResolver().delete(uri, DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID +
                        "=?", new String[]{String.valueOf(mIntent.getIntExtra("calendarId", 0))});
                for (int i = 0; i < addedEmployee.size(); i++) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID,
                            mIntent.getIntExtra("calendarId", 0));
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_EMPLOYEE_ID, addedEmployee.get(i).getId());
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_NAME, addedEmployee.get(i).getName());
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_IS_SELECT, 1);
                    contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_IS_SUBMIT, 0);
                    contentValueses.add(contentValues);
                }
                boolean isEmpty = true;
                for (Object ob : contentValueses) {
                    if (ob != null) {
                        isEmpty = false;
                    }
                }
                if (!isEmpty) {
                    ContentValues[] arr = new ContentValues[contentValueses.size()];
                    arr = contentValueses.toArray(arr);
                    getActivity().getContentResolver().bulkInsert(TempAttendeesProvider.CONTENT_URI, arr);
                }
            } catch (Exception ex) {
                Log.i("ERROR", "Content Provider Error :" + ex.toString());
            } finally {
                Intent intent = new Intent(AddContactActivity.ACTION);
                intent.putExtra("isSubmit", true);
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getActivity());
                mgr.sendBroadcast(intent);
                getActivity().finish();
            }
        }
    };

    private LinearLayout.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(AddContactActivity.ACTION);
            intent.putExtra("isCancel", true);
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getActivity());
            mgr.sendBroadcast(intent);
            getActivity().finish();
        }
    };
    //endregion

    //region touch region
    private void setOnTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    //endregion


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == getActivity().RESULT_OK && requestCode == 123) {
            if (intent.getBooleanExtra("isFavorite", false)) {
                addedEmployee = new ArrayList<>();
                addedId = new ArrayList<>();
                addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                for (int i = 0; i < addedEmployee.size(); i++) {
                    addedId.add(addedEmployee.get(i).getId());
                }
                adapter = new MyFavoriteAdapter(getActivity(), cursor, addedEmployee, addedId);
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);
            }
        }
    }

    //region broadcast
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(AddContactActivity.ACTION)) {
                if (intent.getBooleanExtra("isFavorite", false)) {
                    addedEmployee = new ArrayList<>();
                    addedId = new ArrayList<>();
                    addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                    for (int i = 0; i < addedEmployee.size(); i++) {
                        addedId.add(addedEmployee.get(i).getId());
                    }
                    adapter = new MyFavoriteAdapter(getActivity(), cursor, addedEmployee, addedId);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                } else if (intent.getBooleanExtra("isFavoriteChange", false)) {
                    addedEmployee = new ArrayList<>();
                    addedId = new ArrayList<>();
                    addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                    for (int i = 0; i < addedEmployee.size(); i++) {
                        addedId.add(addedEmployee.get(i).getId());
                    }
                }
            }
        }
    };
    //endregion

    //region pause and resume
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().initLoader(0, null, this);
        IntentFilter iff = new IntentFilter(AddContactActivity.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
        getActivity().getSupportLoaderManager().restartLoader(0, null, callbacks);
    }
    //endregion

    //region search
    private TextView.OnEditorActionListener ediSearchOnEditActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                InputMethodManager imm = (InputMethodManager) getActivity().
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                if (ediSearch.getText().length() > 0) {
                    selection = DBHelper.CONTACT_COLUMN_NAME + " LIKE ? OR " +
                            DBHelper.CONTACT_COLUMN_DEPARTMENT + " LIKE ? AND " +
                            DBHelper.CONTACT_COLUMN_ID + "!=?";
                    selectionArgs = new String[]{"%" + ediSearch.getText().toString() +
                            "%", "%" + ediSearch.getText().toString() + "%", String.valueOf(id)};
                } else {
                    selection = DBHelper.CONTACT_COLUMN_ID + "!=?";
                    selectionArgs = new String[]{String.valueOf(id)};
                }
                getActivity().getSupportLoaderManager().restartLoader(0, null, callbacks);
                return true;
            }
            return false;
        }
    };
    //endregion

    //region loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 0) {
            Uri uri = ContactProvider.CONTENT_URI;
            return new CursorLoader(getActivity(), uri, null, selection, selectionArgs, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 0) {
            if (data != null) {
                this.cursor = data;
                adapter.swapCursor(data);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
    //endregion
}