package com.applab.wcircle_pro.Calendar;

import android.database.Cursor;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Month {

    @SerializedName("ShowDate")
    @Expose
    private String ShowDate;
    @SerializedName("P")
    @Expose
    private Boolean P;
    @SerializedName("B")
    @Expose
    private Boolean B;
    @SerializedName("C")
    @Expose
    private Boolean C;

    /**
     * @return The ShowDate
     */
    public String getShowDate() {
        return ShowDate;
    }

    /**
     * @param ShowDate The ShowDate
     */
    public void setShowDate(String ShowDate) {
        this.ShowDate = ShowDate;
    }

    /**
     * @return The P
     */
    public Boolean getP() {
        return P;
    }

    /**
     * @param P The P
     */
    public void setP(Boolean P) {
        this.P = P;
    }

    /**
     * @return The B
     */
    public Boolean getB() {
        return B;
    }

    /**
     * @param B The B
     */
    public void setB(Boolean B) {
        this.B = B;
    }

    /**
     * @return The C
     */
    public Boolean getC() {
        return C;
    }

    /**
     * @param C The C
     */
    public void setC(Boolean C) {
        this.C = C;
    }

    public static Month getCalendar(Cursor cursor, int position) {
        Month month = new Month();
        if (cursor != null) {
            cursor.moveToPosition(position);
            month.setB(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_B))));
            month.setP(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_P))));
            month.setC(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_C))));
            month.setShowDate(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_SHOW_DATE)));
        }
        return month;
    }

}