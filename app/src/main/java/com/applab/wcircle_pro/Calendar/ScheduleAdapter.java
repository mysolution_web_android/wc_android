package com.applab.wcircle_pro.Calendar;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by admin on 4/5/2015.
 */

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;

    public ScheduleAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public ScheduleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("ScheduleViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_schedule_row, parent, false);
        ScheduleViewHolder holder = new ScheduleViewHolder(view);
        return new ScheduleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ScheduleViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Calendar calendar = Calendar.getCalendar(cursor, position);
        holder.txtTitle.setText(calendar.getTitle());
        holder.txtTitle.setTag(calendar);
        holder.txtTitle.setTypeface(Typeface.defaultFromStyle(calendar.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        String range;
        if (Utils.setDate(Utils.DATE_FORMAT, "d/M/yyyy", calendar.getCalendarFrom()).equals(Utils.setDate(Utils.DATE_FORMAT, "d/M/yyyy", calendar.getCalendarTo()))) {
            if (!calendar.getAllDay()) {
                range = Utils.setDate(Utils.DATE_FORMAT, "h:mm a", calendar.getCalendarFrom()) + " - " + Utils.setDate(Utils.DATE_FORMAT, "h:mm a", calendar.getCalendarTo());
            } else {
                range = Utils.setDate(Utils.DATE_FORMAT, "h.mm a", calendar.getCalendarFrom()) + " " + context.getString(R.string.all_day);
            }
        } else {
            if (!calendar.getAllDay()) {
                range = Utils.setDate(Utils.DATE_FORMAT, "MMM dd, h.mm a", calendar.getCalendarFrom()) + " - " + Utils.setDate(Utils.DATE_FORMAT, "MMM dd, h.mm a", calendar.getCalendarTo());
            } else {
                range = Utils.setDate(Utils.DATE_FORMAT, "MMM dd, h.mm a", calendar.getCalendarFrom()) + " - " + Utils.setDate(Utils.DATE_FORMAT, "MMM dd ", calendar.getCalendarTo()) + context.getString(R.string.all_day);
            }
        }
        holder.txtTime.setText(range);
        holder.txtTime.setTypeface(Typeface.defaultFromStyle(calendar.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
        if (calendar.getShowDate() != null) {
            holder.txtDayMonth.setText(Utils.setDate(Utils.DATE_FORMAT, "dd", calendar.getShowDate()));
            holder.txtDayWeek.setText(Utils.setDate(Utils.DATE_FORMAT, "EEE", calendar.getShowDate()));
        }
        holder.txtDayMonth.setTypeface(Typeface.defaultFromStyle(calendar.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));

        holder.txtDayWeek.setTypeface(Typeface.defaultFromStyle(calendar.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));

        holder.txtId.setText(String.valueOf(calendar.getId()));
        switch (calendar.getType()) {
            case "P":
                holder.imgColor.setImageResource(R.mipmap.info_p);
                break;
            case "B":
                holder.imgColor.setImageResource(R.mipmap.info_b);
                break;
            case "C":
                holder.imgColor.setImageResource(R.mipmap.info_c);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    public void swapCursor(Cursor cursor) {
        this.cursor = cursor;
        android.os.Message msg = handler.obtainMessage();
        handler.handleMessage(msg);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            ScheduleAdapter.this.notifyDataSetChanged();
        }
    };
}
