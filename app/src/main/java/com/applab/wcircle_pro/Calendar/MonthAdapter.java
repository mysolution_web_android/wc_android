package com.applab.wcircle_pro.Calendar;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

public class MonthAdapter extends RecyclerView.Adapter<MonthViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;

    public MonthAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public MonthViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("MonthViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_month_row, parent, false);
        MonthViewHolder holder = new MonthViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MonthViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Calendar current = Calendar.getCalendar(cursor, position);
        if (current.getStatus() != null) {
            holder.txtTitle.setText(current.getTitle());
            holder.txtTitle.setTag(current);
            holder.txtTitle.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
            holder.txtId.setText(String.valueOf(current.getId()));
            String range;
            if (Utils.setDate(Utils.DATE_FORMAT, "d/M/yyyy", current.getCalendarFrom()).equals(Utils.setDate(Utils.DATE_FORMAT, "d/M/yyyy", current.getCalendarTo()))) {
                if (!current.getAllDay()) {
                    range = Utils.setDate(Utils.DATE_FORMAT, "h:mm a", current.getCalendarFrom()) + " - " + Utils.setDate(Utils.DATE_FORMAT, "h:mm a", current.getCalendarTo());
                } else {
                    range = Utils.setDate(Utils.DATE_FORMAT, "h.mm a", current.getCalendarFrom()) + " " + context.getString(R.string.all_day);
                }
            } else {
                if (!current.getAllDay()) {
                    range = Utils.setDate(Utils.DATE_FORMAT, "MMM dd, h.mm a", current.getCalendarFrom()) + " - " + Utils.setDate(Utils.DATE_FORMAT, "MMM dd, h.mm a", current.getCalendarTo());
                } else {
                    range = Utils.setDate(Utils.DATE_FORMAT, "MMM dd", current.getCalendarFrom()) + " - " + Utils.setDate(Utils.DATE_FORMAT, "MMM dd ", current.getCalendarTo()) + context.getString(R.string.all_day);
                }
            }
            holder.txtTime.setText(range);
            holder.txtTime.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
            if(current.getShowDate()!=null){
                holder.txtDayMonth.setText(Utils.setDate(Utils.DATE_FORMAT, "dd", current.getShowDate()));
                holder.txtDayMonth.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
                holder.txtDayWeek.setText(Utils.setDate(Utils.DATE_FORMAT, "EEE", current.getShowDate()));
            }
            holder.txtDayWeek.setTypeface(Typeface.defaultFromStyle(current.getNewCreate() ? Typeface.BOLD : Typeface.NORMAL));
            switch (current.getType()) {
                case "P":
                    holder.imgColor.setImageResource(R.mipmap.info_p);
                    break;
                case "B":
                    holder.imgColor.setImageResource(R.mipmap.info_b);
                    break;
                case "C":
                    holder.imgColor.setImageResource(R.mipmap.info_c);
                    break;
                case "L":
                    holder.imgColor.setImageResource(R.drawable.icon_y);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            MonthAdapter.this.notifyDataSetChanged();
        }
    };
}
