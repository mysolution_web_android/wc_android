package com.applab.wcircle_pro.Calendar;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.Calendar.decorater.CalenderDecorator;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Employee.EmployeeActivity;
import com.applab.wcircle_pro.Leave.EditApplicationActivity;
import com.applab.wcircle_pro.Leave.Leave;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.Date;

public class SpecialMonthFragment extends Fragment implements OnDateSelectedListener, OnMonthChangedListener, LoaderManager.LoaderCallbacks<Cursor> {
    private MaterialCalendarView calendarView;
    private String TAG = SpecialMonthFragment.class.getSimpleName();
    public static int mYear = -1;
    public static int mMonth = -1;
    private SlidingUpPanelLayout mSlidingUpPanel;
    private RecyclerView mRecyclerView;
    private MonthAdapter mAdapter;
    private String mDateFormat = "yyyy-MM-dd";
    private String mTimeStart = "T00:00:00Z";
    private String mTimeEnd = "T23:59:59Z";
    private Cursor cursor;
    private String selection;
    private String selectedDate = "";
    public static String ACTION = "MONTH_SLIDE_UP";
    public boolean mIsStart = false;
    private TextView mTxtDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_special_month, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mAdapter = new MonthAdapter(getActivity(), null);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        String month = Utils.setCalendarDate("M", new Date());
        String d = Utils.setCalendarDate("d", new Date());
        String year = Utils.setCalendarDate("yyyy", new Date());
        mMonth = getMonth(month);
        mYear = Integer.valueOf(year);
        if (getActivity().getIntent().getIntExtra("monthMonth", -1) == -1) {

        } else {
            mMonth = getActivity().getIntent().getIntExtra("monthMonth", -1);
        }

        if (getActivity().getIntent().getIntExtra("yearMonth", -1) == -1) {

        } else {
            mMonth = getActivity().getIntent().getIntExtra("yearMonth", -1);
        }

        calendarView = (MaterialCalendarView) view.findViewById(R.id.calenderView);
        calendarView.setDateSelected(new Date(), true);
        calendarView.setOnDateChangedListener(this);
        calendarView.setOnMonthChangedListener(this);
        mTxtDate = (TextView) view.findViewById(R.id.txtDate);

        mSlidingUpPanel = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
        mSlidingUpPanel.setPanelSlideListener(panelSlideListener);

        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(mItemClickListener);
        return view;
    }

    private SlidingUpPanelLayout.PanelSlideListener panelSlideListener = new SlidingUpPanelLayout.PanelSlideListener() {
        @Override
        public void onPanelSlide(View panel, float slideOffset) {
        }

        @Override
        public void onPanelCollapsed(View panel) {
            Intent intent = new Intent(EmployeeActivity.ACTION);
            intent.putExtra("isSlide", false);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        }

        @Override
        public void onPanelExpanded(View panel) {
            Intent intent = new Intent(EmployeeActivity.ACTION);
            intent.putExtra("isSlide", true);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        }

        @Override
        public void onPanelAnchored(View panel) {

        }

        @Override
        public void onPanelHidden(View view) {


        }
    };

    @Override
    public void onResume() {
        super.onResume();
        setMonth(mMonth, mYear);
        getActivity().getSupportLoaderManager().initLoader(187, null, SpecialMonthFragment.this);
        getActivity().getSupportLoaderManager().initLoader(194, null, SpecialMonthFragment.this);
        IntentFilter iff = new IntentFilter(ACTION);
        iff.addAction(TAG);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                if (!intent.getBooleanExtra("isSlide", false)) {
                    if (mSlidingUpPanel.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                        mSlidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    }
                }
            } else if (action.equals(TAG)) {
                if (intent.getBooleanExtra("isRefresh", false)) {
                    getActivity().getSupportLoaderManager().restartLoader(194, null, SpecialMonthFragment.this);
                }
            }
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 187) {
            Uri uri = CalendarProvider.CONTENT_URI;
            return new CursorLoader(getActivity(), uri, null, selection, null, null);
        } else if (id == 194) {
            return new CursorLoader(getActivity(), MonthProvider.CONTENT_URI, null, " 1 GROUP BY " + DBHelper.MONTH_COLUMN_SHOW_DATE, null, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 187) {
            if (data != null) {
                if (data.getCount() > 0) {
                    this.cursor = data;
                    mAdapter.swapCursor(data);
                    if (mIsStart) {
                        mSlidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                        mIsStart = false;
                    } else {
                        mIsStart = false;
                    }
                } else {
                    mIsStart = false;
                }
            } else {
                mIsStart = false;
            }
        } else if (loader.getId() == 194) {
            ArrayList<NewCalendar> newCalendars = getEventDate(data);
            ArrayList<CalenderDecorator> calenderDecorators = new ArrayList<>();
            for (int i = 0; i < newCalendars.size(); i++) {
                CalenderDecorator calenderDecorator = new CalenderDecorator();
                calenderDecorator.setCalendar(newCalendars.get(i), getActivity());
                calenderDecorators.add(calenderDecorator);
            }
            calendarView.addDecorators(calenderDecorators);
        }
    }

    public ArrayList<NewCalendar> getEventDate(Cursor cursor) {
        ArrayList<NewCalendar> arrayList = new ArrayList<>();
        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        NewCalendar newCalendar = new NewCalendar();
                        newCalendar.setCalendarFrom(com.applab.wcircle_pro.Utils.Utils.setDate(
                                Utils.DATE_FORMAT, Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_SHOW_DATE))));
                        newCalendar.setIsRed(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_P))));
                        newCalendar.setIsGreen(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_B))));
                        newCalendar.setIsPurple(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_C))));
                        newCalendar.setIsYellow(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_L))));
                        arrayList.add(newCalendar);
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        mIsStart = true;
        mTxtDate.setText(Utils.setCalendarDate("MMMM yyyy", date.getDate()));
        selectedDate = Utils.setCalendarDate("yyyy-MM-dd", date.getDate());
        selection = "strftime('%Y-%m-%d','" + selectedDate + "') = strftime('%Y-%m-%d',DATETIME(" + DBHelper.CALENDAR_COLUMN_SHOW_DATE + ",'localtime'))";
        HttpHelper.getScheduleGson(getActivity(), TAG, null, date.getDate(), date.getDate());
        getActivity().getSupportLoaderManager().restartLoader(187, null, SpecialMonthFragment.this);
    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
        mMonth = date.getMonth();
        mYear = date.getYear();
        setMonth(mMonth, mYear);
    }

    private void setMonth(int month, int year) {
        mMonth = month;
        mYear = year;
    }

    private ItemClickSupport.OnItemClickListener mItemClickListener = new ItemClickSupport.OnItemClickListener() {
        @Override
        public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
            final com.applab.wcircle_pro.Calendar.Calendar calendar = com.applab.wcircle_pro.Calendar.Calendar.getCalendar(cursor, position);
            if (calendar.getType().equals("L")) {
                Leave leave = HttpHelper.getLeave(calendar.getId(), getActivity());
                if (leave != null) {
                    Intent intent = new Intent(getActivity(), EditApplicationActivity.class);
                    intent.putExtra("leave", leave);
                    intent.putExtra("isCancelled", leave.getType().equals("cancelled"));
                    if (leave.getSuperiorId().equals(Utils.getProfile(getActivity()).getId())) {
                        intent.putExtra("isEmployeeLeave", true);
                    } else if (!leave.getSuperiorId().equals(Utils.getProfile(getActivity()).getId()) && !leave.getEmployeeId().equals(Utils.getProfile(getActivity()).getId())) {
                        intent.putExtra("isEmployeeLeave", true);
                        intent.putExtra("isBoss", true);
                    } else if (!leave.getSuperiorId().equals(Utils.getProfile(getActivity()).getId()) && leave.getEmployeeId().equals(Utils.getProfile(getActivity()).getId())) {
                        intent.putExtra("isEmployeeLeave", false);
                        intent.putExtra("isBoss", false);
                    }
                    intent.putExtra("page", 0);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    HttpHelper.getLeaveGson(calendar.getId(), getActivity());
                }
            } else {
                Intent mIntent = new Intent(getActivity(), CalendarEventDetailsActivity.class);
                TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
                com.applab.wcircle_pro.Calendar.Calendar current = (com.applab.wcircle_pro.Calendar.Calendar) txtTitle.getTag();
                mIntent.putExtra("calendar", current);
                mIntent.putExtra("attendees", DashboardActivity.getAttendee(current.getId(), getActivity()));
                startActivity(mIntent);
            }
        }
    };

    public static int getMonth(Object month) {
        int m = Integer.valueOf(String.valueOf(month)) - 1;
        if (m < 0) {
            m = 11;
        }
        return m;
    }
}
