package com.applab.wcircle_pro.Calendar;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;

/**
 * Created by user on 6/19/2015.
 */
public class LocationAdapter extends RecyclerView.Adapter<LocationViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;

    public LocationAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("ScheduleViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_add_location_row, parent, false);
        LocationViewHolder holder = new LocationViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Location current = getLocation(cursor, position);
        holder.name.setText(current.name);
    }

    public Location getLocation(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Location location = new Location();
        location.id = cursor.getString(cursor.getColumnIndex(DBHelper.PLACE_COLUMN_ID));
        location.name = cursor.getString(cursor.getColumnIndex(DBHelper.PLACE_COLUMN_NAME));
        return location;
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        if(oldCursor!=null){
            oldCursor.close();
        }
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);

        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            LocationAdapter.this.notifyDataSetChanged();
        }
    };

    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }
}