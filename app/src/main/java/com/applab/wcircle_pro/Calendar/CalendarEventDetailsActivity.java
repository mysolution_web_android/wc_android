package com.applab.wcircle_pro.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Download.Download;
import com.applab.wcircle_pro.Download.DownloadDialogFragment;
import com.applab.wcircle_pro.Download.FolderOneActivity;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.Date;

public class CalendarEventDetailsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, SwipyRefreshLayout.OnRefreshListener {
    //region variable
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private TextView txtToolbarTitle, txtError;
    private CalendarEventAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private Calendar calendar;
    private ArrayList<Attendee> arrAttendee = new ArrayList<>();
    private Bundle bundle;
    private LinearLayout btnButton1, btnButton2, btnButton3, btnEdit, btnCancel, btnReject, btnAttend, btnDelete;
    private String TAG = "CALENDAR";
    private Cursor cursor, cursorAttendee;
    private ProgressBar progressBar;
    private RelativeLayout fadeProgressBar;
    public static String ACTION = "EVENT DETAILS";
    //endregion

    //region on create region
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_event_details);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        txtToolbarTitle = (TextView) findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(getResources().getString(R.string.title_activity_calendar_event_details));
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        bundle = getIntent().getExtras();
        arrAttendee = bundle.getParcelableArrayList("attendees");
        calendar = bundle.getParcelable("calendar");
        txtError = (TextView) findViewById(R.id.txtError);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        btnAttend = (LinearLayout) findViewById(R.id.btnAttend);
        btnReject = (LinearLayout) findViewById(R.id.btnReject);
        btnButton1 = (LinearLayout) findViewById(R.id.btnButton1);
        btnButton2 = (LinearLayout) findViewById(R.id.btnButton2);
        btnButton3 = (LinearLayout) findViewById(R.id.btnButton3);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        btnDelete = (LinearLayout) findViewById(R.id.btnDelete);
        btnEdit = (LinearLayout) findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(btnEditOnClickListener);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        btnAttend.setOnClickListener(btnAttendOnClickListener);
        btnReject.setOnClickListener(btnRejectOnClickListener);
        btnDelete.setOnClickListener(btnDeleteOnClickListener);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new CalendarEventAdapter(this, cursor, cursorAttendee, getSupportFragmentManager());
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        fadeProgressBar = (RelativeLayout) findViewById(R.id.fadeProgressBar);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        fadeProgressBar.setOnClickListener(fadeProgressBarOnClickListener);
        setTouchListener();
    }
    //endregion

    private View.OnClickListener fadeProgressBarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private void setVisibility(boolean isVisible) {
        if (isVisible) {
            progressBar.setVisibility(View.VISIBLE);
            fadeProgressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            fadeProgressBar.setVisibility(View.GONE);
        }
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
                    }
                }
            }
        }
    };

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                /*try {
                    TextView txtReinvite = (TextView) v.findViewById(R.id.txtReinvite);
                    LinearLayout lv = (LinearLayout) v.findViewById(R.id.lv);
                    if (txtReinvite != null) {
                        if (lv != null) {
                            if (txtReinvite.getTag() != null) {
                                Attendee attendee = (Attendee) txtReinvite.getTag();
                                if (lv.getTag() != null) {
                                    Calendar calendar = (Calendar) lv.getTag();
                                    if (calendar.getCreateById().equals(String.valueOf(Utils.getProfile(CalendarEventDetailsActivity.this).getId()))) {
                                        if (attendee.getStatus().equals("reject")) {
                                            Intent intent = new Intent(CalendarDetailsActivity.ACTION);
                                            intent.putExtra("isPending", true);
                                            LocalBroadcastManager.getInstance(CalendarEventDetailsActivity.this).sendBroadcast(intent);
                                            HttpHelper.reinviteAttendee(CalendarEventDetailsActivity.this, attendee.getCalendarId(), attendee.getEmployeeId(), TAG);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.fillInStackTrace();
                } catch (Exception ex) {
                    ex.fillInStackTrace();
                }*/
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        setVisibility(true);
        /*Intent intent = new Intent(this, CalendarActivity.class);
        intent.putExtra("page", page);
        intent.putExtra("monthSchedule", monthSchedule);
        intent.putExtra("yearSchedule", yearSchedule);
        intent.putExtra("monthMonth", monthMonth);
        intent.putExtra("yearMonth", yearMonth);
        startActivity(intent);*/
        finish();
    }

    //region gson region
    private boolean isAttendeesExists(Object calendarId, Object employeeId) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(AttendeesProvider.CONTENT_URI, null,
                    DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                    new String[]{String.valueOf(calendarId), String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }
    //endregion

    //region on click listener
    private LinearLayout.OnClickListener btnAttendOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibility(true);
            Calendar calendar = Calendar.getCalendar(cursor, 0);
            CalendarDialogFragment calendarDialogFragment = CalendarDialogFragment.newInstance(Calendar.getCalendar(cursor, 0),
                    CalendarEventDetailsActivity.this,
                    Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT_PRINT, calendar.getCalendarFrom()),
                    0, DashboardActivity.getAttendee(calendar.getId(), CalendarEventDetailsActivity.this));
            calendarDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    private LinearLayout.OnClickListener btnDeleteOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibility(true);
            CalendarRejectDeleteDialogFragment calendarRejectDeleteDialogFragment =
                    CalendarRejectDeleteDialogFragment.newInstance(calendar.getId(), CalendarEventDetailsActivity.this, 1, 0);
            calendarRejectDeleteDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    private LinearLayout.OnClickListener btnRejectOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            /*CalendarRejectDeleteDialogFragment calendarRejectDeleteDialogFragment =
                    CalendarRejectDeleteDialogFragment.newInstance(calendar.getId(),
                            CalendarEventDetailsActivity.this, 2, page);
            calendarRejectDeleteDialogFragment.show(getSupportFragmentManager(), "");*/
            setVisibility(true);
            HttpHelper.rejectEvent(true, CalendarEventDetailsActivity.this, calendar.getId(), TAG);
        }
    };

    private LinearLayout.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibility(true);
            CalendarRejectDeleteDialogFragment calendarRejectDeleteDialogFragment =
                    CalendarRejectDeleteDialogFragment.newInstance(calendar.getId(), CalendarEventDetailsActivity.this, 1, 0);
            calendarRejectDeleteDialogFragment.show(getSupportFragmentManager(), "");
            /*HttpHelper.deleteCalendar(CalendarEventDetailsActivity.this, calendar.getId(), TAG);
            Intent intent = new Intent(CalendarEventDetailsActivity.this, CalendarActivity.class);
            intent.putExtra("page", page);
            intent.putExtra("monthSchedule", monthSchedule);
            intent.putExtra("yearSchedule", yearSchedule);
            intent.putExtra("monthMonth", monthMonth);
            intent.putExtra("yearMonth", yearMonth);
            startActivity(intent);
            finish();*/
        }
    };

    private LinearLayout.OnClickListener btnEditOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getBaseContext(), CalendarDetailsActivity.class);
            intent.putExtra("id", calendar.getId());
            startActivity(intent);
            finish();
        }
    };
    //endregion


    //region menu region
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calendar_event_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            setVisibility(true);
            /*Intent intent = new Intent(new Intent(getBaseContext(), CalendarActivity.class));
            intent.putExtra("page", page);
            intent.putExtra("monthSchedule", monthSchedule);
            intent.putExtra("yearSchedule", yearSchedule);
            intent.putExtra("monthMonth", monthMonth);
            intent.putExtra("yearMonth", yearMonth);
            startActivity(intent);*/
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
    //endregion

    //region loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 89) {
            return new CursorLoader(CalendarEventDetailsActivity.this,
                    CalendarProvider.CONTENT_URI, null, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?",
                    new String[]{String.valueOf(this.calendar.getId())}, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + " LIMIT 1 ");
        } else if (id == 90) {
            return new CursorLoader(CalendarEventDetailsActivity.this,
                    AttendeesProvider.CONTENT_URI, null, DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? ",
                    new String[]{String.valueOf(this.calendar.getId())}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 89) {
            if (data != null) {
                if (data.getCount() > 0) {
                    this.cursor = data;
                    calendar = Calendar.getCalendar(cursor, 0);

                    if (calendar.getType().equals("C") || calendar.getType().equals("B")) {
                        btnButton1.setVisibility(View.GONE);
                        btnButton2.setVisibility(View.GONE);
                        btnButton3.setVisibility(View.GONE);
                    } else {
                        if (Utils.setDate(Utils.DATE_FORMAT, calendar.getCalendarTo()).after(new Date())) {
                            if (calendar.getCreateById().equals(String.valueOf(Utils.getProfile(CalendarEventDetailsActivity.this).getId()))) {
                                btnButton1.setVisibility(View.GONE);
                                btnButton2.setVisibility(View.VISIBLE);
                                btnButton3.setVisibility(View.GONE);
                            }
                        } else {
                            if (calendar.getStatus().equals("active")) {
                                if (calendar.getCreateById().equals(String.valueOf(Utils.getProfile(CalendarEventDetailsActivity.this).getId()))) {
                                    btnButton1.setVisibility(View.GONE);
                                    btnButton2.setVisibility(View.VISIBLE);
                                    btnButton3.setVisibility(View.GONE);
                                }
                            } else {
                                if (calendar.getCreateById().equals(String.valueOf(Utils.getProfile(CalendarEventDetailsActivity.this).getId()))) {
                                    btnButton1.setVisibility(View.GONE);
                                    btnButton2.setVisibility(View.VISIBLE);
                                    btnButton3.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                    adapter.swapCalendar(cursor);
                }
            }
        } else if (loader.getId() == 90) {
            if (data != null) {
                if (data.getCount() > 0) {
                    this.cursorAttendee = data;
                    adapter.swapAttendee(cursorAttendee);
                    ArrayList<Attendee> attendees = new ArrayList<>();
                    if (cursorAttendee != null) {
                        if (cursorAttendee.getCount() > 0) {
                            for (int i = 0; i < cursorAttendee.getCount(); i++) {
                                Attendee attendee = Attendee.getAttendee(cursorAttendee, i);
                                attendees.add(attendee);
                            }
                            if (calendar.getType().equals("C") || calendar.getType().equals("B")) {
                                btnButton1.setVisibility(View.GONE);
                                btnButton2.setVisibility(View.GONE);
                                btnButton3.setVisibility(View.GONE);
                            } else if (Utils.setDate(Utils.DATE_FORMAT, calendar.getCalendarTo()).after(new Date())) {
                                if (attendees.size() > 0) {
                                    for (int i = 0; i < attendees.size(); i++) {
                                        if (attendees.get(i).getEmployeeId().equals(Utils.getProfile(this).getId())) {
                                            if (attendees.get(i).getStatus().equals("attend")) {
                                                btnButton1.setVisibility(View.GONE);
                                                btnButton2.setVisibility(View.GONE);
                                                btnButton3.setVisibility(View.GONE);
                                            } else if (attendees.get(i).getStatus().equals("reject")) {
                                                btnButton1.setVisibility(View.GONE);
                                                btnButton2.setVisibility(View.GONE);
                                                btnButton3.setVisibility(View.GONE);
                                            } else {
                                                btnButton1.setVisibility(View.VISIBLE);
                                                btnButton2.setVisibility(View.GONE);
                                                btnButton3.setVisibility(View.GONE);
                                            }
                                            break;
                                        } else {
                                            btnButton1.setVisibility(View.GONE);
                                            btnButton2.setVisibility(View.GONE);
                                            btnButton3.setVisibility(View.GONE);
                                        }
                                    }
                                } else {
                                    btnButton1.setVisibility(View.GONE);
                                    btnButton2.setVisibility(View.GONE);
                                    btnButton3.setVisibility(View.GONE);
                                }
                            } else {
                                if (calendar.getStatus().equals("active")) {
                                    if (attendees.size() > 0) {
                                        for (int i = 0; i < attendees.size(); i++) {
                                            if (attendees.get(i).getEmployeeId().equals(Utils.getProfile(this).getId())) {
                                                if (attendees.get(i).getStatus().equals("attend")) {
                                                    btnButton1.setVisibility(View.GONE);
                                                    btnButton2.setVisibility(View.GONE);
                                                    btnButton3.setVisibility(View.GONE);
                                                } else if (attendees.get(i).getStatus().equals("reject")) {
                                                    btnButton1.setVisibility(View.GONE);
                                                    btnButton2.setVisibility(View.GONE);
                                                    btnButton3.setVisibility(View.GONE);
                                                } else {
                                                    btnButton1.setVisibility(View.GONE);
                                                    btnButton2.setVisibility(View.GONE);
                                                    btnButton3.setVisibility(View.GONE);
                                                }
                                                break;
                                            } else {
                                                btnButton1.setVisibility(View.GONE);
                                                btnButton2.setVisibility(View.GONE);
                                                btnButton3.setVisibility(View.GONE);
                                            }
                                        }
                                    } else {
                                        btnButton1.setVisibility(View.GONE);
                                        btnButton2.setVisibility(View.GONE);
                                        btnButton3.setVisibility(View.GONE);
                                    }
                                } else {
                                    if (attendees.size() > 0) {
                                        for (int i = 0; i < attendees.size(); i++) {
                                            if (attendees.get(i).getEmployeeId().equals(Utils.getProfile(this).getId())) {
                                                if (attendees.get(i).getStatus().equals("attend")) {
                                                    btnButton1.setVisibility(View.GONE);
                                                    btnButton2.setVisibility(View.GONE);
                                                    btnButton3.setVisibility(View.GONE);
                                                } else if (attendees.get(i).getStatus().equals("reject")) {
                                                    btnButton1.setVisibility(View.GONE);
                                                    btnButton2.setVisibility(View.GONE);
                                                    btnButton3.setVisibility(View.GONE);
                                                } else {
                                                    btnButton1.setVisibility(View.GONE);
                                                    btnButton2.setVisibility(View.GONE);
                                                    btnButton3.setVisibility(View.GONE);
                                                }
                                                break;
                                            } else {
                                                btnButton1.setVisibility(View.GONE);
                                                btnButton2.setVisibility(View.GONE);
                                                btnButton3.setVisibility(View.GONE);
                                            }
                                        }
                                    } else {
                                        btnButton1.setVisibility(View.GONE);
                                        btnButton2.setVisibility(View.GONE);
                                        btnButton3.setVisibility(View.GONE);
                                    }
                                }
                            }
                            if (Utils.setDate(Utils.DATE_FORMAT, calendar.getCalendarTo()).after(new Date())) {
                                if (calendar.getCreateById().equals(String.valueOf(Utils.getProfile(CalendarEventDetailsActivity.this).getId()))) {
                                    btnButton1.setVisibility(View.GONE);
                                    btnButton2.setVisibility(View.VISIBLE);
                                    btnButton3.setVisibility(View.GONE);
                                }
                            } else {
                                if (calendar.getStatus().equals("active")) {
                                    if (calendar.getCreateById().equals(String.valueOf(Utils.getProfile(CalendarEventDetailsActivity.this).getId()))) {
                                        btnButton1.setVisibility(View.GONE);
                                        btnButton2.setVisibility(View.VISIBLE);
                                        btnButton3.setVisibility(View.GONE);
                                    }
                                } else {
                                    if (calendar.getCreateById().equals(String.valueOf(Utils.getProfile(CalendarEventDetailsActivity.this).getId()))) {
                                        btnButton1.setVisibility(View.GONE);
                                        btnButton2.setVisibility(View.VISIBLE);
                                        btnButton3.setVisibility(View.GONE);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
    //endregion

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                if (intent.getBooleanExtra("isSuccess", false)) {
                    Intent mIntent = new Intent(CalendarEventDetailsActivity.this, CalendarActivity.class);
                    startActivity(mIntent);
                    finish();
                } else if (intent.getBooleanExtra("isFail", false)) {
                    setVisibility(false);
                }
            } else if (action.equals(CalendarDetailsActivity.ACTION)) {
                /*if (intent.getBooleanExtra("isPending", false)) {
                    //setVisibility(true);
                } else if (intent.getBooleanExtra("isSuccess", false)) {
                    //setVisibility(false);
                } else if (intent.getBooleanExtra("isFail", false)) {
                    //setVisibility(false);
                }*/
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(ACTION);
        iff.addAction(CalendarDetailsActivity.ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        getSupportLoaderManager().initLoader(89, null, this);
        getSupportLoaderManager().initLoader(90, null, this);
        if (Utils.getStatus(Utils.CALENDAR_READ, CalendarEventDetailsActivity.this)) {
            HttpHelper.getCalendarGsonReading(this, calendar.getId(), TAG);
        }
        HttpHelper.getSingleCalendarGson(CalendarEventDetailsActivity.this, calendar, TAG, txtError);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
        Utils.setIsActivityOpen(false, getBaseContext(), false);
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            HttpHelper.getSingleCalendarGson(CalendarEventDetailsActivity.this, calendar, TAG, txtError);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }
}
