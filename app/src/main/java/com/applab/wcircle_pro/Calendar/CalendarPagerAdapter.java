package com.applab.wcircle_pro.Calendar;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.applab.wcircle_pro.R;

class CalendarPagerAdapter extends FragmentPagerAdapter {
    String[] tabs;

    public CalendarPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        tabs = context.getResources().getStringArray(R.array.calendar_tabs);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment item = null;
        ScheduleFragment scheduleFragment = new ScheduleFragment();
        SpecialMonthFragment monthFragment = new SpecialMonthFragment();
        if (position == 0) {
            item = scheduleFragment;
        } else if (position == 1) {
            item = monthFragment;
        }
        return item;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }
}