package com.applab.wcircle_pro.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Leave.EditApplicationActivity;
import com.applab.wcircle_pro.Leave.Leave;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.api.client.auth.oauth.OAuthGetAccessToken;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class MonthFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    //region variable
    private RecyclerView recyclerView;
    private MonthAdapter monthAdapter;
    private String selectedDate = "";
    private Cursor cursor;
    private LoaderManager.LoaderCallbacks<Cursor> callBack;
    private String selection;
    private EditText ediSearch;
    private RelativeLayout rlAutoComplete;
    private CaldroidFragment caldroidFragment;
    private CaldroidFragment dialogCaldroidFragment;
    private SlidingUpPanelLayout slidingUpPanelLayout;
    public static String ACTION = "MONTH_SLIDE_UP";
    private View layout;
    private String TAG = MonthFragment.class.getSimpleName();
    public static int mYear = -1;
    public static int mMonth = -1;
    private LocalBroadcastManager mgr;
    private TextView txtDate;
    //endregion

    //region on create region
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getSupportLoaderManager().initLoader(187, null, this);
        layout = inflater.inflate(R.layout.fragment_month, container, false);
        layout.setOnKeyListener(backPressed);
        rlAutoComplete = (RelativeLayout) layout.findViewById(R.id.rlAutoComplete);
        rlAutoComplete.setVisibility(View.GONE);
        ediSearch = (EditText) layout.findViewById(R.id.ediSearch);
        callBack = this;
        mgr = LocalBroadcastManager.getInstance(getActivity());
        slidingUpPanelLayout = (SlidingUpPanelLayout) layout.findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.setPanelSlideListener(panelSlideListener);
        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        monthAdapter = new MonthAdapter(getActivity(), cursor);
        recyclerView.setAdapter(monthAdapter);
        txtDate = (TextView) layout.findViewById(R.id.txtDate);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setTouchListener();
        layout.addOnLayoutChangeListener(layoutChangeListener);
        setCaldroid(savedInstanceState);
        return layout;
    }
    //endregion

    //region layout change listener region
    private View.OnLayoutChangeListener layoutChangeListener = new View.OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        }
    };
    //endregion

    //region onclick region
    //endregion

    //region back pressed region
    private View.OnKeyListener backPressed = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                return true;
            }
            return false;
        }
    };
    //endregion

    //region calroid region
    private void setCustomResourceForDates() {
        Calendar cal = Calendar.getInstance();

        // Min date is last 7 days
        cal.add(Calendar.DATE, -7);
        Date blueDate = cal.getTime();

        // Max date is next 7 days
        cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        Date greenDate = cal.getTime();
    }

    private void setCaldroid(Bundle savedInstanceState) {
        caldroidFragment = new MonthCustomFragment();
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState, "CALDROID_SAVED_STATE");
        } else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, mMonth == -1 ? cal.get(Calendar.MONTH) + 1 : mMonth + 1);
            args.putInt(CaldroidFragment.YEAR, mYear == -1 ? cal.get(Calendar.YEAR) : mYear);
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
            args.putBoolean(CaldroidFragment.SHOW_NAVIGATION_ARROWS, true);
            args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, false);
            caldroidFragment.setArguments(args);
        }
        setCustomResourceForDates();

        FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar, caldroidFragment);
        t.commit();
        final CaldroidListener listener = new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {
                HashMap<String, Object> extraData = caldroidFragment.getExtraData();
                extraData.put("SELECTED_DATE", Utils.setCalendarDate("yyyy-M-d", date));
                caldroidFragment.refreshView();
                View redView = view.findViewById(R.id.redView);
                txtDate.setText(Utils.setCalendarDate("MMMM yyyy", date));
                if (redView.getVisibility() == View.VISIBLE) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                    selectedDate = Utils.setCalendarDate("yyyy-MM-dd", date);
                    selection = "strftime('%Y-%m-%d','" + selectedDate + "') = strftime('%Y-%m-%d',DATETIME(" + DBHelper.CALENDAR_COLUMN_SHOW_DATE + ",'localtime'))";
                    getActivity().getSupportLoaderManager().restartLoader(187, null, MonthFragment.this);
                    HttpHelper.getScheduleGson(getActivity(), TAG, null, date, date);
                }
            }

            @Override
            public void onChangeMonth(int month, int year) {
                mMonth = month - 1 < 0 ? 0 : month - 1;
                mYear = year;
                HttpHelper.getScheduleGson(getActivity(), TAG, null, HttpHelper.getFirstDate(ScheduleFragment.getSelectedMonthPosition(mMonth - 3),
                        ScheduleFragment.getYear(mMonth - 3, year)), HttpHelper.getLastDate(ScheduleFragment.getSelectedMonthPosition(mMonth + 3), ScheduleFragment.getYear(mMonth + 3, year)));
            }

            @Override
            public void onLongClickDate(Date date, View view) {
            }

            @Override
            public void onCaldroidViewCreated() {

            }
        };
        caldroidFragment.setCaldroidListener(listener);
    }
    //endregion

    //region sliding up panel listener region
    private SlidingUpPanelLayout.PanelSlideListener panelSlideListener = new SlidingUpPanelLayout.PanelSlideListener() {
        @Override
        public void onPanelSlide(View panel, float slideOffset) {

        }

        @Override
        public void onPanelCollapsed(View panel) {
            /*Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.reverse_rotate);
            animation.setFillAfter(true);
            animation.setDuration((int) (panel.getHeight() / panel.getContext().getResources().getDisplayMetrics().density));
            btnArrow.startAnimation(animation);*/

            Intent intent = new Intent(CalendarActivity.ACTION);
            intent.putExtra("isSlide", false);
            mgr.sendBroadcast(intent);
        }

        @Override
        public void onPanelExpanded(View panel) {
            /*Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
            animation.setFillAfter(true);
            animation.setDuration((int) (panel.getHeight() / panel.getContext().getResources().getDisplayMetrics().density));
            btnArrow.startAnimation(animation);*/

            Intent intent = new Intent(CalendarActivity.ACTION);
            intent.putExtra("isSlide", true);
            mgr.sendBroadcast(intent);
        }

        @Override
        public void onPanelAnchored(View panel) {

        }

        @Override
        public void onPanelHidden(View view) {


        }
    };

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, final View v) {
                final com.applab.wcircle_pro.Calendar.Calendar calendar = com.applab.wcircle_pro.Calendar.Calendar.getCalendar(cursor, position);
                if (calendar.getType().equals("L")) {
                    Leave leave = HttpHelper.getLeave(calendar.getId(), getActivity());
                    if (leave != null) {
                        Intent intent = new Intent(getActivity(), EditApplicationActivity.class);
                        intent.putExtra("leave", leave);
                        intent.putExtra("isCancelled", leave.getType().equals("cancelled"));
                        if (leave.getSuperiorId().equals(Utils.getProfile(getActivity()).getId())) {
                            intent.putExtra("isEmployeeLeave", true);
                        } else if (!leave.getSuperiorId().equals(Utils.getProfile(getActivity()).getId()) && !leave.getEmployeeId().equals(Utils.getProfile(getActivity()).getId())) {
                            intent.putExtra("isEmployeeLeave", true);
                            intent.putExtra("isBoss", true);
                        } else if (!leave.getSuperiorId().equals(Utils.getProfile(getActivity()).getId()) && leave.getEmployeeId().equals(Utils.getProfile(getActivity()).getId())) {
                            intent.putExtra("isEmployeeLeave", false);
                            intent.putExtra("isBoss", false);
                        }
                        intent.putExtra("page", 0);
                        startActivity(intent);
                        getActivity().finish();
                    } else {
                        HttpHelper.getLeaveGson(calendar.getId(), getActivity());
                    }
                } else {
                    Intent mIntent = new Intent(getActivity(), CalendarEventDetailsActivity.class);
                    TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
                    com.applab.wcircle_pro.Calendar.Calendar current = (com.applab.wcircle_pro.Calendar.Calendar) txtTitle.getTag();
                    mIntent.putExtra("calendar", current);
                    mIntent.putExtra("attendees", DashboardActivity.getAttendee(current.getId(), getActivity()));
                    mIntent.putExtra("page", 1);
                    mIntent.putExtra("monthSchedule", ScheduleFragment.selectedMonthPosition);
                    mIntent.putExtra("yearSchedule", Integer.valueOf(ScheduleFragment.year));
                    mIntent.putExtra("monthMonth", MonthFragment.mMonth);
                    mIntent.putExtra("yearMonth", MonthFragment.mYear);
                    startActivity(mIntent);
                    getActivity().finish();
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }
    //endregion

    //region loader region
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 187) {
            Uri uri = CalendarProvider.CONTENT_URI;
            return new CursorLoader(getActivity(), uri, null, selection, null, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 187) {
            this.cursor = data;
            monthAdapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        monthAdapter.swapCursor(null);
    }
    //endregion

    //region on save state region
    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);

        if (caldroidFragment != null) {
            caldroidFragment.saveStatesToKey(outState, "CALDROID_SAVED_STATE");
        }
        if (dialogCaldroidFragment != null) {
            dialogCaldroidFragment.saveStatesToKey(outState,
                    "DIALOG_CALDROID_SAVED_STATE");
        }
    }
    //endregion

    //region broadcast region
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                if (!intent.getBooleanExtra("isSlide", false)) {
                    if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    }
                }
            }
        }
    };
    //endregion

    //region on pause and resume region
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
    }
    //endregion

    //region gson region
    public static boolean isCalendarExists(Object calendarId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CalendarProvider.CONTENT_URI, null,
                    DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(calendarId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }
    //endregion
}

