package com.applab.wcircle_pro.Calendar;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalendarChecking {

    @SerializedName("LastUpdate")
    @Expose
    private String LastUpdate;
    @SerializedName("PageNo")
    @Expose
    private Integer PageNo;
    @SerializedName("NoPerPage")
    @Expose
    private Integer NoPerPage;
    @SerializedName("NoOfRecords")
    @Expose
    private Integer NoOfRecords;
    @SerializedName("NoOfPage")
    @Expose
    private Integer NoOfPage;
    @SerializedName("items")
    @Expose
    private List<Calendar> items = new ArrayList<Calendar>();
    @Expose
    private String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    /**
     * @return The LastUpdate
     */
    public String getLastUpdate() {
        return LastUpdate;
    }

    /**
     * @param LastUpdate The LastUpdate
     */
    public void setLastUpdate(String LastUpdate) {
        this.LastUpdate = LastUpdate;
    }

    /**
     * @return The PageNo
     */
    public Integer getPageNo() {
        return PageNo;
    }

    /**
     * @param PageNo The PageNo
     */
    public void setPageNo(Integer PageNo) {
        this.PageNo = PageNo;
    }

    /**
     * @return The NoPerPage
     */
    public Integer getNoPerPage() {
        return NoPerPage;
    }

    /**
     * @param NoPerPage The NoPerPage
     */
    public void setNoPerPage(Integer NoPerPage) {
        this.NoPerPage = NoPerPage;
    }

    /**
     * @return The NoOfRecords
     */
    public Integer getNoOfRecords() {
        return NoOfRecords;
    }

    /**
     * @param NoOfRecords The NoOfRecords
     */
    public void setNoOfRecords(Integer NoOfRecords) {
        this.NoOfRecords = NoOfRecords;
    }

    /**
     * @return The NoOfPage
     */
    public Integer getNoOfPage() {
        return NoOfPage;
    }

    /**
     * @param NoOfPage The NoOfPage
     */
    public void setNoOfPage(Integer NoOfPage) {
        this.NoOfPage = NoOfPage;
    }

    /**
     * @return The items
     */
    public List<Calendar> getCalendar() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setCalendar(List<Calendar> items) {
        this.items = items;
    }

}