package com.applab.wcircle_pro.Calendar;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

public class CalendarViewHolder extends RecyclerView.ViewHolder{
    TextView txtDayNo;
    ImageView imgRed,imgBlue,imgOrange,imgPurple,imgGreen;
    RelativeLayout rl;
    public CalendarViewHolder(View itemView) {
        super(itemView);
        txtDayNo  = (TextView)itemView.findViewById(R.id.textView1);
        imgRed = (ImageView)itemView.findViewById(R.id.imgRed);
        imgBlue = (ImageView)itemView.findViewById(R.id.imgBlue);
        imgOrange =(ImageView)itemView.findViewById(R.id.imgOrange);
        imgGreen = (ImageView)itemView.findViewById(R.id.imgGreen);
        rl = (RelativeLayout)itemView.findViewById(R.id.rl);
    }
}
