package com.applab.wcircle_pro.Calendar;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user on 5/11/2015.
 */
public class NewEmployee implements Parcelable {
    private int id;
    private String oxUser;
    private String name;
    private String image;
    private String position;
    private String countryImage;
    private int selectedPos = -1;

    public String getOxUser() {
        return oxUser;
    }

    public void setOxUser(String oxUser) {
        this.oxUser = oxUser;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCountryImage() {
        return countryImage;
    }

    public void setCountryImage(String countryImage) {
        this.countryImage = countryImage;
    }

    public int getSelectedPos() {
        return selectedPos;
    }

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NewEmployee() {
    }

    public NewEmployee(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.oxUser);
        dest.writeString(this.name);
        dest.writeString(this.image);
        dest.writeString(this.countryImage);
        dest.writeString(this.position);
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.id = in.readInt();
        this.oxUser = in.readString();
        this.name = in.readString();
        this.image = in.readString();
        this.countryImage = in.readString();
        this.position = in.readString();
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public NewEmployee createFromParcel(Parcel in) {
            return new NewEmployee(in);
        }

        @Override
        public NewEmployee[] newArray(int size) {
            return new NewEmployee[size];
        }
    };
}
