package com.applab.wcircle_pro.Calendar;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.Dashboard.CalendarReminderProvider;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Dashboard.DashboardCalendarReminderDialogFragment;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.joda.time.DateTimeZone;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ScheduleFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipyRefreshLayout.OnRefreshListener {
    //region variable region
    private RecyclerView recyclerView;
    private ScheduleAdapter adapter;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private Cursor cursor;
    private String TAG = ScheduleFragment.class.getSimpleName();
    private TextView txtError, calendar_month_year_textview;
    private boolean isScroll = false;
    private ImageView calendar_left_arrow, calendar_right_arrow;
    public static int selectedMonthPosition = -2;
    private String month;
    public static String year = "2016";
    private String selection = null;
    private String[] selectionArgs = null;
    //endregion

    //region on create region
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_schedule, container, false);
        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        adapter = new ScheduleAdapter(getActivity(), cursor);
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        mSwipyRefreshLayout = (SwipyRefreshLayout) layout.findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        txtError = (TextView) layout.findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);
        calendar_left_arrow = (ImageView) layout.findViewById(R.id.calendar_left_arrow);
        calendar_right_arrow = (ImageView) layout.findViewById(R.id.calendar_right_arrow);
        calendar_month_year_textview = (TextView) layout.findViewById(R.id.calendar_month_year_textview);
        setSelectedMonthPosition(selectedMonthPosition);
        calendar_left_arrow.setOnClickListener(leftArrowOnClickListener);
        calendar_right_arrow.setOnClickListener(rightArrowOnClickListener);
        setTouchListener();
        return layout;
    }

    private String getMonth(int position) {
        position += 1;
        String month = "";
        if (position > 12) {
            position = 12;
        }
        if (position < 10) {
            month = "0" + position;
        } else {
            month = String.valueOf(position);
        }
        return month;
    }

    private View.OnClickListener leftArrowOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setSelectedMonthPosition(selectedMonthPosition - 1);
        }
    };

    private View.OnClickListener rightArrowOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setSelectedMonthPosition(selectedMonthPosition + 1);
        }
    };

    private void setSelectedMonthPosition(int position) {
        if (position == -2) {
            selectedMonthPosition = Integer.valueOf(Utils.setCalendarDate("M", new Date())) - 1;
            month = Utils.setCalendarDate("MMMM", new Date()) + " ";
            year = Utils.setCalendarDate("yyyy", new Date());
            selection = DBHelper.CALENDAR_COLUMN_TYPE + "!='L' AND strftime('%m-%Y','" + year + "-" + getMonth(selectedMonthPosition) + "-01" + "') = strftime('%m-%Y',DATETIME(" + DBHelper.CALENDAR_COLUMN_SHOW_DATE + ",'LOCALTIME'))";
            String result = month + year;
            /*HttpHelper.getScheduleGson(getActivity(), TAG, txtError,
                    HttpHelper.getFirstDate(getSelectedMonthPosition(selectedMonthPosition), getYear(selectedMonthPosition, Integer.valueOf(year) - 1)),
                    HttpHelper.getLastDate(getSelectedMonthPosition(selectedMonthPosition), getYear(selectedMonthPosition, Integer.valueOf(year) + 1)));*/
            calendar_month_year_textview.setText(result);
            getActivity().getSupportLoaderManager().initLoader(0, null, ScheduleFragment.this);
        } else {
            if (position > 11) {
                selectedMonthPosition = 0;
                month = Utils.setCalendarDate("M", "MMMM", String.valueOf(selectedMonthPosition + 1)) + " ";
                int monthPosition = selectedMonthPosition + 1;
                year = getYear(year + " " + monthPosition + " 01", 1);
            } else if (position < 0) {
                selectedMonthPosition = 11;
                month = Utils.setCalendarDate("M", "MMMM", String.valueOf(selectedMonthPosition + 1)) + " ";
                int monthPosition = selectedMonthPosition + 1;
                year = getYear(year + " " + monthPosition + " 01", -1);
            } else {
                selectedMonthPosition = position;
                month = Utils.setCalendarDate("M", "MMMM", String.valueOf(selectedMonthPosition + 1)) + " ";
            }
            selection = DBHelper.CALENDAR_COLUMN_TYPE + "!='L' AND strftime('%m-%Y','" + year + "-" + getMonth(selectedMonthPosition) + "-01" + "') = strftime('%m-%Y',DATETIME(" + DBHelper.CALENDAR_COLUMN_SHOW_DATE + ",'LOCALTIME'))";
            String result = month + year;
            /*HttpHelper.getScheduleGson(getActivity(), TAG, txtError,
                    HttpHelper.getFirstDate(getSelectedMonthPosition(selectedMonthPosition - 3), getYear(selectedMonthPosition - 3, Integer.valueOf(year))),
                    HttpHelper.getLastDate(getSelectedMonthPosition(selectedMonthPosition + 3), getYear(selectedMonthPosition + 3, Integer.valueOf(year))));*/
            calendar_month_year_textview.setText(result);
            getActivity().getSupportLoaderManager().restartLoader(0, null, ScheduleFragment.this);
        }
    }

    public static int getSelectedMonthPosition(int position) {
        if (position > 11) {
            position = 0;
        } else if (position < 0) {
            position = 11;
        }
        return position;
    }

    public static int getYear(int month, int year) {
        if (month > 11) {
            year += 1;
        } else if (month < 0) {
            year -= 1;
        }
        return year;
    }

    public static String getYear(String dateString, int minusPlus) {
        Date currentDate = Utils.setCalendarDate("yyyy M dd", dateString);
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(java.util.Calendar.YEAR, minusPlus);
        return Utils.setCalendarDate("yyyy", calendar.getTime());
    }

    //endregion
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        HttpHelper.getScheduleGson(getActivity(), TAG, txtError,
                HttpHelper.getFirstDate(getSelectedMonthPosition(Integer.valueOf(Utils.setCalendarDate("M", new Date())) - 1), getYear(Integer.valueOf(Utils.setCalendarDate("M", new Date())) - 1, Integer.valueOf(year) - 1)),
                HttpHelper.getLastDate(getSelectedMonthPosition(Integer.valueOf(Utils.setCalendarDate("M", new Date())) - 1), getYear(Integer.valueOf(Utils.setCalendarDate("M", new Date())) - 1, Integer.valueOf(year) + 1)));
        getActivity().getSupportLoaderManager().restartLoader(0, null, this);
        IntentFilter iff = new IntentFilter(CalendarActivity.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
    }

    //region on touch region
    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, final View v) {
                Intent mIntent = new Intent(getActivity(), CalendarEventDetailsActivity.class);
                TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
                com.applab.wcircle_pro.Calendar.Calendar calendar = (com.applab.wcircle_pro.Calendar.Calendar)
                        txtTitle.getTag();
                mIntent.putExtra("calendar", calendar);
                mIntent.putExtra("attendees", DashboardActivity.getAttendee(calendar.getId(), getActivity()));
                startActivity(mIntent);
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    //endregion

    //region loader region
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 0) {
            Uri uri = CalendarProvider.CONTENT_URI;
            return new CursorLoader(getActivity(), uri, null, selection, selectionArgs, DBHelper.CALENDAR_COLUMN_SHOW_DATE + " DESC ");
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 0) {
            this.cursor = data;
            adapter.swapCursor(data);
            if (this.cursor != null) {
                if (!isScroll && this.cursor.getCount() > 0) {
                    int position = 0;
                    for (int i = 0; i < this.cursor.getCount(); i++) {
                        com.applab.wcircle_pro.Calendar.Calendar calendar = com.applab.wcircle_pro.Calendar.Calendar.getCalendar(cursor, i);
                        if (Utils.setCalendarDate(Utils.DATE_FORMAT, calendar.getCalendarFrom()).equals(new Date())) {
                            position = i;
                            break;
                        } else if (Utils.setCalendarDate(Utils.DATE_FORMAT, calendar.getCalendarFrom()).before(new Date())) {
                            position = i;
                        }
                    }
                    if (position > 0) {
                        //linearLayoutManager.scrollToPosition(position);
                        isScroll = true;
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    //endregion
    public static boolean isAttendeesExists(Object calendarId, Object employeeId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(AttendeesProvider.CONTENT_URI, null,
                    DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                    new String[]{String.valueOf(calendarId), String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void setCalendarReminder(com.applab.wcircle_pro.Calendar.Calendar calendar, Context context) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.CALENDAR_REMINDER_CALENDAR_COLUMN_ID, calendar.getId());
        contentValues.put(DBHelper.CALENDAR_REMINDER_COLUMN_IS_SEEN, false);
        if (DashboardCalendarReminderDialogFragment.isCalendarReminderExists(calendar.getId(), context)) {
            context.getContentResolver().update(CalendarReminderProvider.CONTENT_URI, contentValues,
                    DBHelper.CALENDAR_REMINDER_COLUMN_ID + "=?", new String[]{String.valueOf(calendar.getId())});
        } else {
            context.getContentResolver().insert(CalendarReminderProvider.CONTENT_URI, contentValues);
        }
    }

    //region refresh region
    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            HttpHelper.getScheduleGson(getActivity(), TAG, txtError,
                    HttpHelper.getFirstDate(getSelectedMonthPosition(Integer.valueOf(Utils.setCalendarDate("M", new Date())) - 1), getYear(Integer.valueOf(Utils.setCalendarDate("M", new Date())) - 1, Integer.valueOf(year) - 1)),
                    HttpHelper.getLastDate(getSelectedMonthPosition(Integer.valueOf(Utils.setCalendarDate("M", new Date())) - 1), getYear(Integer.valueOf(Utils.setCalendarDate("M", new Date())) - 1, Integer.valueOf(year) + 1)));
            getActivity().getSupportLoaderManager().restartLoader(0, null, this);
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }
    //endregion
}