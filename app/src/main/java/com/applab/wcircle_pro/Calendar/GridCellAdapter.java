package com.applab.wcircle_pro.Calendar;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.RoundImage;
import com.applab.wcircle_pro.Utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

// Inner Class
public class GridCellAdapter extends BaseAdapter {
    private static final String tag = "GridCellAdapter";
    private final Context _context;
    private final List<String> list;
    private final String[] weekdays = new String[]{"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
    private final String[] months = {"January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December"};
    private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private final int month, year;
    int daysInMonth, prevMonthDays;
    private final int currentDayOfMonth, currentMonth, currentYear;
    private TextView gridcell;
    private RelativeLayout rl;
    private ImageView imgRed, imgPurple, imgGreen;
    private Cursor cursor;

    // Days in Current Month
    public GridCellAdapter(Context context, Cursor cursor, int month, int year) throws ParseException {
        super();
        this._context = context;
        this.list = new ArrayList<String>();
        this.month = month;
        this.year = year;
        Log.d(tag, "Month: " + month + " " + "Year: " + year);
        Calendar calendar = Calendar.getInstance();
        currentDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        currentMonth = calendar.get(Calendar.MONTH);
        currentYear = calendar.get(Calendar.YEAR);
        printMonth(month, year);
        this.cursor = cursor;
        if (cursor != null) {
            cursor.close();
        }
    }

    public String getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    private void printMonth(int mm, int yy) {
        int trailingSpaces = 0;
        int daysInPrevMonth = 0;
        int prevMonth = 0;
        int prevYear = 0;
        int nextMonth = 0;
        int nextYear = 0;

        GregorianCalendar cal = new GregorianCalendar(yy, mm, currentDayOfMonth);
        // Days in Current Month
        daysInMonth = daysOfMonth[mm];
        int currentMonth = mm;
        if (currentMonth == 11) {
            prevMonth = 10;
            daysInPrevMonth = daysOfMonth[prevMonth];
            nextMonth = 0;
            prevYear = yy;
            nextYear = yy + 1;
        } else if (currentMonth == 0) {
            prevMonth = 11;
            prevYear = yy - 1;
            nextYear = yy;
            daysInPrevMonth = daysOfMonth[prevMonth];
            nextMonth = 1;
        } else {
            prevMonth = currentMonth - 1;
            nextMonth = currentMonth + 1;
            nextYear = yy;
            prevYear = yy;
            daysInPrevMonth = daysOfMonth[prevMonth];
        }

        // Compute how much to leave before before the first day of the
        // month.
        // getDay() returns 0 for Sunday.
        trailingSpaces = cal.get(Calendar.DAY_OF_WEEK) - 1;

        if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 1) {
            ++daysInMonth;
        }

        // Trailing Month days
        for (int i = 0; i < trailingSpaces; i++) {
            String day = String.valueOf((daysInPrevMonth - trailingSpaces + 1) + i);
            String month = months[prevMonth];
            String year = String.valueOf(prevYear);
            int red = 0;
            int green = 0;
            int purple = 0;
            Schedule schedule = getData(year + "-" + String.valueOf(mm + 1) + "-" + day);
            if (schedule != null) {
                String[] strDate = schedule.time.split("-");
                String monthEvent = months[Integer.valueOf(strDate[1]) - 1];
                if (day.equals(strDate[0]) && month.equals(monthEvent) && year.equals(strDate[2])) {
                    int color = schedule.color;
                    if (color == 1) {
                        red = 1;
                    } else if (color == 2) {
                        green = 2;
                    } else if (color == 4) {
                        purple = 4;
                    }
                }
            }
            list.add(day + "-GREY" + "-" + month + "-" + year + "-"
                    + red + "-" + green + "-" + purple);
        }

        // Current Month Days
        for (int i = 1; i <= daysInMonth; i++) {
            String day = String.valueOf(i);
            String month = months[mm];
            String year = String.valueOf(yy);
            int red = 0;
            int green = 0;
            int purple = 0;
            Schedule schedule = getData(year + "-" + String.valueOf(mm + 1) + "-" + day);
            if (schedule != null) {
                String[] strDate = schedule.time.split("-");
                String monthEvent = months[Integer.valueOf(strDate[1]) - 1];
                if (day.equals(strDate[0]) && month.equals(monthEvent) && year.equals(strDate[2])) {
                    int color = schedule.color;
                    if (color == 1) {
                        red = 1;
                    } else if (color == 2) {
                        green = 2;
                    } else if (color == 4) {
                        purple = 4;
                    }
                }
            }
            list.add(day + "-WHITE" + "-" + month + "-" + year
                    + "-" + red + "-" + green + "-" + purple);

        }

        // Leading Month days
        for (int i = 0; i < list.size() % 7; i++) {
            String day = String.valueOf(i + 1);
            String month = months[nextMonth];
            String year = String.valueOf(nextYear);
            int red = 0;
            int green = 0;
            int purple = 0;
            Schedule schedule = getData(year + "-" + String.valueOf(mm + 1) + "-" + day);
            if (schedule != null) {
                String[] strDate = schedule.time.split("-");
                String monthEvent = months[Integer.valueOf(strDate[1]) - 1];
                if (day.equals(strDate[0]) && month.equals(monthEvent) && year.equals(strDate[2])) {
                    int color = schedule.color;
                    if (color == 1) {
                        red = 1;
                    } else if (color == 2) {
                        green = 2;
                    } else if (color == 4) {
                        purple = 4;
                    }
                }
            }
            list.add(day + "-GREY" + "-" + month + "-" + year
                    + "-" + red + "-" + green + "-" + purple);
        }
    }

    public Schedule getData(String strDate) {
        strDate = convertDateFormat(strDate);
        Uri uri = CalendarProvider.CONTENT_URI;
        String[] projection = {DBHelper.CALENDAR_COLUMN_START_DATE, DBHelper.CALENDAR_COLUMN_CALENDAR_ID};
        String selection = DBHelper.CALENDAR_COLUMN_START_DATE + " LIKE ? ";
        String[] selectionArgs = {"%" + strDate + "%"};
        if (_context == null) {
            return null;
        }
        Cursor cursor = _context.getContentResolver().query(uri, projection, selection, selectionArgs, DBHelper.CALENDAR_COLUMN_START_DATE + " DESC LIMIT 1 ");
        try {
            if (cursor != null && cursor.moveToFirst()) {
                Schedule schedule = new Schedule();
                schedule.id = cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_CALENDAR_ID));
                schedule.color = 1;
                String startDate = cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_START_DATE));
                String year = "";
                String month = "";
                String day = "";
                year =Utils.setDate(Utils.DATE_FORMAT,"yyyy",startDate);
                month = Utils.setDate(Utils.DATE_FORMAT, "M", startDate);
                day = Utils.setDate(Utils.DATE_FORMAT, "d", startDate);
                schedule.time = day + "-" + month + "-" + year;
                cursor.close();
                return schedule;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    public String convertDateFormat(String strDate) {
        return Utils.setDate("yyyy-M-d","yyyy-MM-dd",strDate);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater;
        Log.d(tag, "getView ...");
        View row = convertView;
        Log.d(tag, "Starting XML Row Inflation ... ");
        inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Log.d(tag, "Successfully completed XML Row Inflation!");
        Log.d(tag, "Current Day: " + currentDayOfMonth);
        String[] day_color = list.get(position).split("-");
        if(row == null){
            row = inflater.inflate(R.layout.custom_calendar, parent, false);
        }
        else{
            return row;
        }
        gridcell = (TextView) row.findViewById(R.id.textView1);
        rl = (RelativeLayout) row.findViewById(R.id.rl);
        FrameLayout circleFrame = (FrameLayout) row.findViewById(R.id.circleFrame);

        gridcell.setText(day_color[0]);
        rl.setTag(day_color[0] + "-" + day_color[2] + "-" + day_color[3]);
        if (day_color[1].equals("GREY")) {
            gridcell.setTextColor(Color.LTGRAY);
        }
        if (day_color[1].equals("WHITE")) {
            gridcell.setTextColor(Color.BLACK);
        }
        if (day_color[0].equals(String.valueOf(currentDayOfMonth)) &&
                day_color[2].equals(months[currentMonth]) &&
                day_color[3].equals(String.valueOf(currentYear))) {
            circleFrame.setVisibility(View.VISIBLE);
        }
        Bitmap bm = BitmapFactory.decodeResource(_context.getResources(), R.mipmap.red_round);
        RoundImage roundImage = new RoundImage();
        if (day_color[4].equals("1")) {
            imgRed = (ImageView) row.findViewById(R.id.imgRed);
            imgRed.setImageBitmap(roundImage.getCircleBitmap(bm));
            setVisibilityImageView(Integer.valueOf("1"));
        }
        if (day_color[5].equals("2")) {
            imgGreen = (ImageView) row.findViewById(R.id.imgGreen);
            bm = BitmapFactory.decodeResource(_context.getResources(), R.mipmap.green_round);
            imgGreen.setImageBitmap(roundImage.getCircleBitmap(bm));
            setVisibilityImageView(Integer.valueOf("2"));
        }
        if (day_color[6].equals("4")) {
            imgPurple = (ImageView) row.findViewById(R.id.imgPurple);
            bm = BitmapFactory.decodeResource(_context.getResources(), R.mipmap.purple_round);
            imgPurple.setImageBitmap(roundImage.getCircleBitmap(bm));
            setVisibilityImageView(Integer.valueOf("4"));
        }
        return row;
    }

    private void setVisibilityImageView(int i) {
        switch (i) {
            case 1:
                imgRed.setVisibility(View.VISIBLE);
                break;
            case 2:
                imgGreen.setVisibility(View.VISIBLE);
                break;
            case 4:
                imgPurple.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }
}