package com.applab.wcircle_pro.Calendar;

/**
 * Created by user on 30/10/2015.
 */
public class NewCalendar {
    boolean isGreen = false;
    boolean isPurple = false;
    boolean isRed = false;
    boolean isYellow = false;
    String calendarFrom;

    public boolean getGreen() {
        return isGreen;
    }

    public void setIsGreen(boolean isGreen) {
        this.isGreen = isGreen;
    }

    public boolean getPurple() {
        return isPurple;
    }

    public void setIsPurple(boolean isPurple) {
        this.isPurple = isPurple;
    }

    public boolean getRed() {
        return isRed;
    }

    public void setIsRed(boolean isRed) {
        this.isRed = isRed;
    }

    public boolean getYellow() {
        return isYellow;
    }

    public void setIsYellow(boolean isYellow) {
        this.isYellow = isYellow;
    }

    public String getCalendarFrom() {
        return calendarFrom;
    }

    public void setCalendarFrom(String calendarFrom) {
        this.calendarFrom = calendarFrom;
    }
}
