package com.applab.wcircle_pro.Calendar;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by user on 5/7/2015.
 */
public class EditCalendarAdapter extends RecyclerView.Adapter<EditCalendarViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private Cursor cursor;
    private String TAG = "REINVITE ATTENDEE";

    public EditCalendarAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public EditCalendarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("ScheduleViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_edit_calendar_row, parent, false);
        EditCalendarViewHolder holder = new EditCalendarViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(EditCalendarViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Attendee current = Attendee.getAttendee(cursor, position);
        holder.title.setTag(current);
        holder.title.setText(current.getEmployeyName());
        holder.txtReinvite.setVisibility(View.GONE);
        if (current.getStatus() != null) {
            switch (current.getStatus()) {
                case "attend":
                    holder.icon.setImageResource(R.mipmap.info_a);
                    break;
                case "reject":
                    holder.icon.setImageResource(R.mipmap.info_r);
                    break;
                default:
                    holder.icon.setImageResource(R.mipmap.info_p_2);
                    break;
            }
        } else {
            holder.icon.setImageResource(R.mipmap.info_p_2);
        }
        holder.delete.setTag(current);
        holder.txtReinvite.setTag(current);
        setDeleteOnClickListener(holder.delete);
        setReinviteOnClickListener(holder.txtReinvite);
    }

    private void setDeleteOnClickListener(final ImageView delete) {
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Attendee attendee = (Attendee) delete.getTag();
                context.getContentResolver().delete(TempAttendeesProvider.CONTENT_URI, DBHelper.TEMP_ATTENDEES_COLUMN_EMPLOYEE_ID + "=? AND "
                                + DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID + "=?",
                        new String[]{String.valueOf(attendee.getEmployeeId()), String.valueOf(attendee.getCalendarId())});
            }
        });
    }

    private void setReinviteOnClickListener(final TextView invite) {
        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Attendee attendee = (Attendee) invite.getTag();
                HttpHelper.reinviteAttendee(context, attendee.getCalendarId(), attendee.getEmployeeId(), TAG);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cursor != null ? cursor.getCount() : 0;
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            EditCalendarAdapter.this.notifyDataSetChanged();
        }
    };

}