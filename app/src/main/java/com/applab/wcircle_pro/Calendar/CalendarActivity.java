package com.applab.wcircle_pro.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.Dashboard.DashboardCalendarDialogFragment;
import com.applab.wcircle_pro.Dashboard.DashboardEmployeeLeaveDialogFragment;
import com.applab.wcircle_pro.Menu.NavigationDrawerFragment;
import com.applab.wcircle_pro.Notification.NotificationDialogFragment;
import com.applab.wcircle_pro.Notification.NotificationProvider;
import com.applab.wcircle_pro.Notification.Notifications;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Tabs.SlidingTabLayout;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;


public class CalendarActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ViewPager mPager;
    private SlidingTabLayout mTabs;
    private TextView txtTitle;
    private DialogFragment notificationDialogFragment;
    private DashboardCalendarDialogFragment calendarDialogFragment;
    private DashboardEmployeeLeaveDialogFragment employeeLeaveDialogFragment;
    private NavigationDrawerFragment drawerFragment;
    public static String ACTION = "SLIDE_DOWN_PANEL";
    private boolean isSlide = false;
    public static LocalBroadcastManager mgr;
    public static AlarmManager manager;
    private Bundle bundle;
    private int page = 0;
    private int monthMonth = -1;
    private int yearMonth = -1;
    private int yearSchedule = 2016;
    private int monthSchedule = -2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        ImageView space1 = (ImageView) toolbar.findViewById(R.id.space1);
        space1.setVisibility(View.INVISIBLE);
        txtTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtTitle.setText(getBaseContext().getResources().getString(R.string.title_activity_calendar));
        bundle = getIntent().getExtras();
        mgr = LocalBroadcastManager.getInstance(this);
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        drawerFragment.setSelectedPosition(2);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(1);
        mPager.setAdapter(new CalendarPagerAdapter(getSupportFragmentManager(), this));
        mPager.addOnPageChangeListener(mPagerOnPageChangeListener);
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setDistributeEvenly(true);
        mTabs.setCustomTabView(R.layout.custom_tab_view, R.id.tabText, R.id.tabNumber);
        mTabs.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
        mTabs.setSelectedIndicatorColors(ContextCompat.getColor(this, android.R.color.holo_red_light));
        mTabs.setViewPager(mPager);
        mPager.setCurrentItem(page);
        toolbar.setNavigationIcon(R.mipmap.action_arrow_prev_white);
        toolbar.setNavigationOnClickListener(toolbarOnClickListener);
    }

    private ViewPager.OnPageChangeListener mPagerOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            page = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private View.OnClickListener toolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isSlide) {
                Utils.clearPreviousActivity(CalendarActivity.this);
            } else {
                Intent intent = new Intent(MonthFragment.ACTION);
                intent.putExtra("isSlide", false);
                mgr.sendBroadcast(intent);
            }
        }
    };

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    //region alarm region
    public static void setAlarm(int year, int month, int day, int hourOfDay, int minute, String msg, Calendar mCalendar, ArrayList<Attendee> arrAttendee, Context context) {
        manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.clear();
        calendar.set(year, month - 1, day, hourOfDay, minute, 0);

        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("msg", msg);
        intent.putExtra("calendar", mCalendar);
        intent.putExtra("attendees", arrAttendee);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, mCalendar.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    public static String getStartTime(String startDate) {
        String time = Utils.setCalendarDate(Utils.DATE_FORMAT, "hh:mm a", startDate);
        return time;
    }

    public static String getEndTime(String endTime) {
        String time = Utils.setCalendarDate(Utils.DATE_FORMAT, "hh:mm a", endTime);
        return time;
    }


    public static int getYear(String startDate) {
        String year = Utils.setCalendarDate(Utils.DATE_FORMAT, "yyyy", startDate);
        return Integer.valueOf(year);
    }

    public static int getMonth(String startDate) {
        String month = Utils.setCalendarDate(Utils.DATE_FORMAT, "M", startDate);
        return Integer.valueOf(month);
    }

    public static int getDay(String startDate) {
        String day = Utils.setCalendarDate(Utils.DATE_FORMAT, "d", startDate);
        return Integer.valueOf(day);
    }

    public static int getHour(String startDate, int selectedReminder) {
        java.util.Calendar gc = new GregorianCalendar();
        gc.setTime(Utils.setCalendarDate(Utils.DATE_FORMAT, startDate));
        Date requestedDate = new Date();
        if (selectedReminder == 2) {
            gc.add(java.util.Calendar.HOUR, -1);
            requestedDate = gc.getTime();
        } else if (selectedReminder == 3) {
            gc.add(java.util.Calendar.HOUR, -2);
            requestedDate = gc.getTime();
        } else if (selectedReminder == 4) {
            gc.add(java.util.Calendar.HOUR, -3);
            requestedDate = gc.getTime();
        } else if (selectedReminder == 5) {
            gc.add(java.util.Calendar.HOUR, -4);
            requestedDate = gc.getTime();
        } else if (selectedReminder == 6) {
            gc.add(java.util.Calendar.HOUR, -5);
            requestedDate = gc.getTime();
        } else if (selectedReminder == 7) {
            gc.add(java.util.Calendar.HOUR, -6);
            requestedDate = gc.getTime();
        } else if (selectedReminder == 8) {
            gc.add(java.util.Calendar.HOUR, -7);
            requestedDate = gc.getTime();
        } else if (selectedReminder == 9) {
            gc.add(java.util.Calendar.HOUR, -8);
            requestedDate = gc.getTime();
        } else {
            requestedDate = gc.getTime();
        }
        String hour = Utils.setCalendarDate("HH", requestedDate);
        return Integer.valueOf(hour);
    }

    public static int getMinutes(String startDate, int selectedReminder) {
        java.util.Calendar gc = new GregorianCalendar();
        gc.setTime(Utils.setCalendarDate(Utils.DATE_FORMAT, startDate));
        Date requestedDate = new Date();
        if (selectedReminder == 1) {
            gc.add(java.util.Calendar.MINUTE, -30);
            requestedDate = gc.getTime();
        } else {
            requestedDate = gc.getTime();
        }
        String minutes = Utils.setCalendarDate("mm", requestedDate);
        return Integer.valueOf(minutes);
    }
    //endregion

    @Override
    public void onBackPressed() {
        if (!isSlide) {
            Utils.clearPreviousActivity(CalendarActivity.this);
        } else {
            Intent intent = new Intent(MonthFragment.ACTION);
            intent.putExtra("isSlide", false);
            mgr.sendBroadcast(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_calendar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (drawerFragment.mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.action_new:
                Intent intent = new Intent(getBaseContext(), CalendarDetailsActivity.class);
                startActivity(intent);
                return true;
            case android.R.id.home:
                Utils.clearPreviousActivity(CalendarActivity.this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setPopUpWindow(Cursor cursor) {
        notificationDialogFragment = new NotificationDialogFragment();
        Bundle args = new Bundle();
        ArrayList<Notifications> arrayList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Notifications notifications = new Notifications();
                notifications.setId(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_ID)));
                notifications.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_CREATE_DATE)));
                notifications.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_TITLE)));
                notifications.setDescription(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_DESCRIPTION)));
                notifications.setType(cursor.getString(cursor.getColumnIndex(DBHelper.NOTIFICATION_COLUMN_TYPE)));
                arrayList.add(notifications);
            }
            while (cursor.moveToNext());
            cursor.close();
        }
        args.putParcelableArrayList("Notifications", arrayList);
        notificationDialogFragment.setArguments(args);
        notificationDialogFragment.setCancelable(false);
        notificationDialogFragment.show(getSupportFragmentManager(), "");
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Utils.NOTIFICATION_ACTION)) {
                Uri uri = NotificationProvider.CONTENT_URI;
                String[] projection = {
                        DBHelper.NOTIFICATION_COLUMN_TYPE,
                        DBHelper.NOTIFICATION_COLUMN_TITLE,
                        DBHelper.NOTIFICATION_COLUMN_CREATE_DATE,
                        DBHelper.NOTIFICATION_COLUMN_DESCRIPTION,
                        DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY,
                        DBHelper.NOTIFICATION_COLUMN_ID
                };
                String selection = DBHelper.NOTIFICATION_COLUMN_IS_NOTIFY + "=?";
                String[] selectionArgs = {"0"};
                Cursor cursor = getContentResolver().query(uri, projection, selection, selectionArgs, DBHelper.NOTIFICATION_COLUMN_ID + " DESC ");
                if (notificationDialogFragment != null) {
                    if (!notificationDialogFragment.isHidden()) {
                        notificationDialogFragment.dismiss();
                    }
                }
                setPopUpWindow(cursor);
            } else if (action.equals(ACTION)) {
                isSlide = intent.getBooleanExtra("isSlide", false);
                Log.i("UP", String.valueOf(isSlide));
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(Utils.NOTIFICATION_ACTION);
        iff.addAction(ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }
}
