package com.applab.wcircle_pro.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.applab.wcircle_pro.Dashboard.DashboardActivity;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class MyCancelEventFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipyRefreshLayout.OnRefreshListener {
    private RecyclerView recyclerView;
    private ScheduleAdapter adapter;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private Cursor cursor;
    private String TAG = "CALENDAR";
    private EditText ediSearch;
    private String selection = null;
    private String[] selectionArgs = {String.valueOf(1)};
    private LoaderManager.LoaderCallbacks<Cursor> callBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getSupportLoaderManager().initLoader(7, null, this);
        View layout = inflater.inflate(R.layout.fragment_schedule, container, false);
        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        adapter = new ScheduleAdapter(getActivity(), cursor);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        mSwipyRefreshLayout = (SwipyRefreshLayout) layout.findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        callBack = this;
        ediSearch = (EditText) layout.findViewById(R.id.ediSearch);
        setTouchListener();
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        searchWatcher();
        ediSearch.setOnEditorActionListener(ediSearchOnEditorActionListener);
        return layout;
    }

    private TextView.OnEditorActionListener ediSearchOnEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
            return false;
        }
    };

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!mSwipyRefreshLayout.isRefreshing()) {
                if (totalItemCount > 1) {
                    if (lastVisibleItem >= totalItemCount - 1) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                    } else if (firstVisibleItem == 0) {
                        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
                    }
                }
            }
        }
    };

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, final View v) {
                final Intent mIntent = new Intent(getActivity(), CalendarEventDetailsActivity.class);
                TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
                Calendar calendar = (Calendar) txtTitle.getTag();
                mIntent.putExtra("calendar", calendar);
                mIntent.putExtra("attendees", DashboardActivity.getAttendee(calendar.getId(), getActivity()));
                startActivity(mIntent);
                getActivity().finish();
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    private void searchWatcher() {
        ediSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (ediSearch.hasFocus()) {
                    if (ediSearch.getText().length() > 0) {
                        selection += " AND " + DBHelper.CALENDAR_COLUMN_TITLE + " LIKE ?";
                        selectionArgs = new String[]{String.valueOf(1), "%" + ediSearch.getText().toString() + "%"};
                    } else {
                        selection = null;
                        selectionArgs = new String[]{String.valueOf(1), "%" + ediSearch.getText().toString() + "%"};
                    }
                    getActivity().getSupportLoaderManager().restartLoader(7, null, callBack);
                }
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 7) {
            Uri uri = CalendarProvider.CONTENT_URI;
            return new CursorLoader(getActivity(), uri, null, selection, selectionArgs, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + " DESC ");
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 7) {
            this.cursor = data;
            adapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    public void getGson() {
        final String token = Utils.getToken(getActivity());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Calendar[]> mGsonRequest = new GsonRequest<Calendar[]>(
                Request.Method.GET,
                Utils.API_URL + "api/Employee/" + Utils.getProfile(getActivity()).getId() + "/Calendars",
                Calendar[].class,
                headers,
                responseListener(),
                errorListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<Calendar[]> responseListener() {
        return new Response.Listener<Calendar[]>() {
            @Override
            public void onResponse(Calendar[] response) {
                if (response != null) {
                    getActivity().getContentResolver().delete(AttendeesProvider.CONTENT_URI, null, null);
                    getActivity().getContentResolver().delete(CalendarProvider.CONTENT_URI, null, null);
                    insertCalendar(response);
                }
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMsg = error.getMessage();
                if (errorMsg != null)
                    Log.i("CALENDAR", errorMsg);
                VolleyLog.d(TAG, errorMsg);
            }
        };
    }

    public void insertCalendar(Calendar[] result) {
        DBHelper helper = new DBHelper(getActivity());
        ArrayList<Calendar> arrCalendar = new ArrayList<>(Arrays.asList(result));
        try {
            ContentValues[] contentValueses = new ContentValues[arrCalendar.size()];
            for (int i = 0; i < arrCalendar.size(); i++) {
                ContentValues contentValues = new ContentValues();
                String calendarTo = arrCalendar.get(i).getCalendarTo();
                String calendarFrom = arrCalendar.get(i).getCalendarFrom();
                contentValues.put(DBHelper.CALENDAR_COLUMN_CALENDAR_ID, arrCalendar.get(i).getId());
                contentValues.put(DBHelper.CALENDAR_COLUMN_TITLE, arrCalendar.get(i).getTitle());
                contentValues.put(DBHelper.CALENDAR_COLUMN_DESCRIPTION, arrCalendar.get(i).getDescription());
                contentValues.put(DBHelper.CALENDAR_COLUMN_ALL_DAY, arrCalendar.get(i).getAllDay());
                contentValues.put(DBHelper.CALENDAR_COLUMN_START_DATE, calendarFrom);
                contentValues.put(DBHelper.CALENDAR_COLUMN_END_DATE, calendarTo);
                contentValues.put(DBHelper.CALENDAR_COLUMN_LOCATION, arrCalendar.get(i).getLocation());
                if (arrCalendar.get(i).getAttendees() != null) {
                    for (int j = 0; j < arrCalendar.get(i).getAttendees().size(); j++) {
                        ContentValues contentValuesAttendees = new ContentValues();
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SUBMIT, 1);
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SELECT, 1);
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_NAME, arrCalendar.get(i).getAttendees().get(j).getEmployeyName());
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID, arrCalendar.get(i).getAttendees().get(j).getEmployeeId());
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_CALENDAR_ID, arrCalendar.get(i).getId());
                        getActivity().getContentResolver().insert(AttendeesProvider.CONTENT_URI, contentValuesAttendees);
                    }
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (Object ob : contentValueses) {
                if (ob != null) {
                    isEmpty = false;
                }
            }
            long rowsInserted = 0;
            if (!isEmpty) {
                rowsInserted = getActivity().getContentResolver().bulkInsert(CalendarProvider.CONTENT_URI, contentValueses);
                Log.i(TAG, String.valueOf(rowsInserted));
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }


    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            getGson();
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }
}

