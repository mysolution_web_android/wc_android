package com.applab.wcircle_pro.Calendar;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 6/23/2015.
 */
public class MonthViewHolder extends RecyclerView.ViewHolder {
    ImageView imgColor;
    TextView txtTitle, txtDayMonth, txtDayWeek;
    TextView txtTime;
    TextView txtId;

    public MonthViewHolder(View itemView) {
        super(itemView);
        txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
        txtTime = (TextView) itemView.findViewById(R.id.txtTime);
        txtDayMonth = (TextView) itemView.findViewById(R.id.txtDayMonth);
        txtDayWeek = (TextView) itemView.findViewById(R.id.txtDayWeek);
        imgColor = (ImageView) itemView.findViewById(R.id.imgColor);
        txtId = (TextView) itemView.findViewById(R.id.txtId);
    }
}
