package com.applab.wcircle_pro.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidGridAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MonthCustomFragment extends CaldroidFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private ArrayList<MonthCustomAdapter> arrMonthCustomAdapter = new ArrayList<>();
    private ArrayList<NewCalendar> arrayList;
    private ArrayList<String> arrayCalendarFrom = new ArrayList<>();
    private int id = 568;
    private LoaderManager.LoaderCallbacks callbacks;

    @Override
    public CaldroidGridAdapter getNewDatesGridAdapter(int month, int year) {
        // TODO Auto-generated method stub
        callbacks = this;
        MonthCustomAdapter monthCustomAdapter = new MonthCustomAdapter(getActivity(), month, year,
                getCaldroidData(), extraData, id, arrayList);
        arrMonthCustomAdapter.add(monthCustomAdapter);
        getActivity().getSupportLoaderManager().initLoader(id, null, this);
        return monthCustomAdapter;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == this.id) {
            String[] projection = {
                    DBHelper.MONTH_COLUMN_SHOW_DATE,
                    " CASE " + DBHelper.CALENDAR_COLUMN_TYPE + " WHEN 'P' THEN '" +
                            String.valueOf(true) + "' ELSE '" + String.valueOf(false) + "' END AS " +
                            DBHelper.MONTH_COLUMN_P,
                    " CASE " + DBHelper.CALENDAR_COLUMN_TYPE + " WHEN 'B' THEN '" +
                            String.valueOf(true) + "' ELSE '" + String.valueOf(false) + "' END AS " +
                            DBHelper.MONTH_COLUMN_B,
                    " CASE " + DBHelper.CALENDAR_COLUMN_TYPE + " WHEN 'C' THEN '" +
                            String.valueOf(true) + "' ELSE '" + String.valueOf(false) + "' END AS " +
                            DBHelper.MONTH_COLUMN_C,
                    " CASE " + DBHelper.CALENDAR_COLUMN_TYPE + " WHEN 'L' THEN '" +
                            String.valueOf(true) + "' ELSE '" + String.valueOf(false) + "' END AS " +
                            DBHelper.MONTH_COLUMN_L
            };
            return new CursorLoader(getActivity(), CalendarProvider.CONTENT_URI, projection, " 1 GROUP BY " + DBHelper.CALENDAR_COLUMN_SHOW_DATE, null, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == this.id) {
            this.arrayList = getEventDate(data);
            Handler handler = new Handler();
            Runnable r = new Runnable() {
                public void run() {
                    HashMap<String, Object> extraData = getExtraData();
                    extraData.put("HEIGHT", CaldroidFragment.getCalendarHeights());
                    extraData.put("DATA", arrayList);
                    extraData.put("DATA1", arrayCalendarFrom);
                    refreshView();
                }
            };
            handler.postDelayed(r, 500);
        }
    }

    public ArrayList<NewCalendar> getEventDate(Cursor cursor) {
        ArrayList<NewCalendar> arrayList = new ArrayList<>();
        arrayCalendarFrom.clear();
        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        NewCalendar newCalendar = new NewCalendar();
                        newCalendar.setCalendarFrom(com.applab.wcircle_pro.Utils.Utils.setDate(
                                com.applab.wcircle_pro.Utils.Utils.DATE_FORMAT, "yyyy-M-d", cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_SHOW_DATE))));
                        newCalendar.setIsRed(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_P))));
                        newCalendar.setIsGreen(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_B))));
                        newCalendar.setIsPurple(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_C))));
                        newCalendar.setIsYellow(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.MONTH_COLUMN_L))));
                        arrayList.add(newCalendar);
                        arrayCalendarFrom.add(newCalendar.getCalendarFrom());
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(CalendarActivity.ACTION)) {
                if (intent.getBooleanExtra("refresh", false)) {
                    getActivity().getSupportLoaderManager().restartLoader(MonthCustomFragment.this.id, null, callbacks);
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(com.applab.wcircle_pro.Utils.Utils.NOTIFICATION_ACTION);
        iff.addAction(CalendarActivity.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }
}