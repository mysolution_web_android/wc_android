package com.applab.wcircle_pro.Calendar;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;

/**
 * Created by admin on 24/8/2015.
 */

public class AlarmIntentService extends IntentService {
    private static final int UNIQUEID = 45612;
    NotificationCompat.Builder notification;
    private String TAG = "DASHBOARD CALENDAR ALARM";

    public AlarmIntentService() {
        super("AlarmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Calendar calendar = intent.getParcelableExtra("calendar");
        String msg = intent.getStringExtra("msg");
        ArrayList<Attendee> arrAttendee = intent.getParcelableArrayListExtra("attendees");
        sendNotification(msg, calendar, arrAttendee);
    }

    private void sendNotification(final String msg, Calendar calendar, ArrayList<Attendee> arrAttendee) {
        calendar.setAttendees(arrAttendee);
        if (!MonthFragment.isCalendarExists(calendar.getId(), getBaseContext())) {
            insertCalendar(calendar);
        }
        HttpHelper.getSingleCalendarGson(getBaseContext(), calendar, TAG, null);
        Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
        notification = new NotificationCompat.Builder(this);
        notification.setSmallIcon(R.mipmap.icon);
        notification.setAutoCancel(true);
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("Calendar: " + calendar.getTitle());
        notification.setSound(alarmSound);
        notification.setContentText(msg);
        //notification.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
        notification.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
        Intent intentNotification = new Intent(this, CalendarEventDetailsActivity.class);
        intentNotification.putExtra("calendar", calendar);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, calendar.getId(), intentNotification, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);
        NotificationManager me = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        me.notify(calendar.getId(), notification.build());
    }

    public void insertCalendar(Calendar calendar) {
        DBHelper helper = new DBHelper(getBaseContext());
        try {
            ContentValues contentValues = new ContentValues();
            String calendarTo = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, calendar.getCalendarTo());
            String calendarFrom = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, calendar.getCalendarFrom());
            String reminderDate = calendar.getReminder() != null ? Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, calendar.getReminder()) : null;
            contentValues.put(DBHelper.CALENDAR_COLUMN_CALENDAR_ID, calendar.getId());
            contentValues.put(DBHelper.CALENDAR_COLUMN_TYPE, calendar.getType());
            contentValues.put(DBHelper.CALENDAR_COLUMN_TITLE, calendar.getTitle());
            contentValues.put(DBHelper.CALENDAR_COLUMN_ALL_DAY, String.valueOf(calendar.getAllDay()));
            contentValues.put(DBHelper.CALENDAR_COLUMN_START_DATE, calendarFrom);
            contentValues.put(DBHelper.CALENDAR_COLUMN_END_DATE, calendarTo);
            contentValues.put(DBHelper.CALENDAR_COLUMN_LOCATION, calendar.getLocation());
            contentValues.put(DBHelper.CALENDAR_COLUMN_DESCRIPTION, calendar.getDescription());
            contentValues.put(DBHelper.CALENDAR_COLUMN_REMINDER, reminderDate);
            contentValues.put(DBHelper.CALENDAR_COLUMN_STATUS, calendar.getStatus());
            contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_ID, calendar.getCreateById());
            contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_NAME, calendar.getCreateByName());
            contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_ID, calendar.getUpdateById());
            contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_NAME, calendar.getUpdateByName());
            contentValues.put(DBHelper.CALENDAR_COLUMN_NEW_CREATE, String.valueOf(calendar.getNewCreate()));
            if (calendar.getAttendees() != null) {
                for (int j = 0; j < calendar.getAttendees().size(); j++) {
                    String reminder = calendar.getAttendees().get(j).getReminder() != null ? Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, calendar.getAttendees().get(j).getReminder()) : null;
                    ContentValues contentValuesAttendees = new ContentValues();
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SUBMIT, 1);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SELECT, 1);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_NAME, calendar.getAttendees().get(j).getEmployeyName());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID, calendar.getAttendees().get(j).getEmployeeId());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_CALENDAR_ID, calendar.getId());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_REMINDER, reminder);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_STATUS, calendar.getAttendees().get(j).getStatus());
                    if (!ScheduleFragment.isAttendeesExists(calendar.getId(), calendar.getAttendees().get(j).getEmployeeId(), getBaseContext())) {
                        getContentResolver().insert(AttendeesProvider.CONTENT_URI, contentValuesAttendees);
                    } else {
                        getContentResolver().update(AttendeesProvider.CONTENT_URI,
                                contentValuesAttendees, DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                                new String[]{String.valueOf(calendar.getId()),
                                        String.valueOf(calendar.getAttendees().get(j).getEmployeeId())});
                    }
                }
            }
            if (!MonthFragment.isCalendarExists(calendar.getId(), getBaseContext())) {
                getContentResolver().insert(CalendarProvider.CONTENT_URI, contentValues);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }
}
