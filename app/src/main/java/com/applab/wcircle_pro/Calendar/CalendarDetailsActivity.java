package com.applab.wcircle_pro.Calendar;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.applab.wcircle_pro.Calendar.db.HttpHelper;
import com.applab.wcircle_pro.ErrorDialogFragment;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;
import com.google.api.client.util.DateTime;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import eu.janmuller.android.simplecropimage.Util;

public class CalendarDetailsActivity extends AppCompatActivity
        implements OnDateSetListener, TimePickerDialog.OnTimeSetListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    //region variable region
    private Toolbar toolbar;
    public static final String DATEPICKER_TAG = "datepicker";
    public static final String TIMEPICKER_TAG = "timepicker";
    private TextView txtStartDate, txtEndDate, txtStartTime, txtEndTime;
    private TextView txtDesc, txtToolbarTitle;
    private EditText txtTitle, ediLocation;
    private boolean isAllDay = false;
    private Context context;
    private ToggleButton aSwitch;
    private Bundle extras;
    private int value = 0;
    private Cursor cursor;
    private RecyclerView recyclerView;
    private EditCalendarAdapter adapter;
    private ImageView btnLocation;
    private LinearLayout btnInvitePeople;
    private String TAG = "APPLICATION";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String selection = DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID + "=?";
    private String[] selectionArgs = new String[]{String.valueOf(value)};
    private Spinner spReminder;
    private CalendarSpinnerAdapter calendarSpinnerAdapter;
    private ProgressBar progressBar;
    private Calendar calendar;
    private DatePickerDialog datePickerDialogStart, datePickerDialogEnd;
    private TimePickerDialog timePickerDialogStart, timePickerDialogEnd;
    private SlidingUpPanelLayout slidingUpPanelLayout;
    private ImageView btnPlus;
    private TextView txtInvite;
    private RelativeLayout fadeRL;
    private LinearLayout btnStaff;
    private String txt;
    public static String ACTION = "CALENDAR DETAILS";
    public com.applab.wcircle_pro.Calendar.Calendar calendar1;
    //endregion

    //region on create region
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = getApplicationContext().getSharedPreferences("AddLocation", 0);
        editor = pref.edit();
        editor.apply();
        setContentView(R.layout.activity_add_calendar_item);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        txtToolbarTitle = (TextView) findViewById(R.id.txtTitle);
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getContentResolver().delete(TempAttendeesProvider.CONTENT_URI, null, null);
        extras = getIntent().getExtras();
        initializeSpReminder();
        slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.setPanelSlideListener(panelSlideListener);
        btnStaff = (LinearLayout) findViewById(R.id.btnStaff);
        btnStaff.setOnClickListener(btnStaffOnClickListener);
        calendar = Calendar.getInstance();
        datePickerDialogStart = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);
        datePickerDialogEnd = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);
        timePickerDialogStart = TimePickerDialog.newInstance(this, Integer.valueOf(Utils.setCalendarDate("H", new Date(new Date().getTime() + 3600 * 1000))), 0, false, false);
        timePickerDialogEnd = TimePickerDialog.newInstance(this, Integer.valueOf(Utils.setCalendarDate("H", new Date(new Date().getTime() + 2 * 3600 * 1000))), 0, false, false);
        aSwitch = (ToggleButton) findViewById(R.id.switchDay);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txtDesc = (TextView) findViewById(R.id.txtDesc);
        txtTitle = (EditText) findViewById(R.id.ediTitle);
        txtInvite = (TextView) findViewById(R.id.txtInvite);
        btnPlus = (ImageView) findViewById(R.id.btnPlus);
        ediLocation = (EditText) findViewById(R.id.ediLocation);
        txtStartDate = (TextView) findViewById(R.id.txtStartDate);
        txtEndDate = (TextView) findViewById(R.id.txtEndDate);
        txtStartTime = (TextView) findViewById(R.id.txtStartTime);
        txtEndTime = (TextView) findViewById(R.id.txtEndTime);
        btnInvitePeople = (LinearLayout) findViewById(R.id.btnInvitePeople);
        context = this;
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        fadeRL.setOnClickListener(fadeRLOnClickListener);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new EditCalendarAdapter(this, cursor);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        setOnTouchListener();
        btnLocation = (ImageView) findViewById(R.id.btnLocation);
        btnLocation.setOnClickListener(btnLocationOnClickListener);
        findViewById(R.id.txtStartDate).setOnClickListener(txtStartDateOnClickListener);
        findViewById(R.id.txtStartTime).setOnClickListener(txtStartTimeOnClickListener);
        findViewById(R.id.txtEndDate).setOnClickListener(txtEndDateOnClickListener);
        findViewById(R.id.txtEndTime).setOnClickListener(txtEndTimeOnClickListener);
        txtInvite.setOnClickListener(txtInviteOnClickListener);
        btnPlus.setOnClickListener(btnPlusOnClickListener);
        setDateTime(savedInstanceState);
        setDisplayData();
        getSupportLoaderManager().initLoader(0, null, this);
        Utils.setIsDashboad(false);
        setVisibilityRlAndProgressBar(false);
    }

    private View.OnClickListener btnStaffOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        }
    };

    private View.OnClickListener fadeRLOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private void setVisibilityRlAndProgressBar(boolean isVisible) {
        if (isVisible) {
            fadeRL.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            fadeRL.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    private LinearLayout.OnClickListener btnPlusOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibilityRlAndProgressBar(true);
            Intent intent = new Intent(getBaseContext(), AddContactActivity.class);
            intent.putExtra("calendarId", value);
            if (cursor != null) {
                ArrayList<NewEmployee> arrEmployee = new ArrayList<>();
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    NewEmployee employee = new NewEmployee();
                    employee.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.TEMP_ATTENDEES_COLUMN_EMPLOYEE_ID)));
                    employee.setName(cursor.getString(cursor.getColumnIndex(DBHelper.TEMP_ATTENDEES_COLUMN_NAME)));
                    arrEmployee.add(employee);
                }
                if (arrEmployee.size() > 0) {
                    intent.putParcelableArrayListExtra("addedEmployee", arrEmployee);
                }
            }
            if (Utils.isConnectingToInternet(CalendarDetailsActivity.this)) {
                startActivity(intent);
            } else {
                setVisibilityRlAndProgressBar(false);
                Utils.showNoConnection(CalendarDetailsActivity.this);
            }

        }
    };


    private LinearLayout.OnClickListener txtInviteOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        }
    };

    //region panel slider region
    private SlidingUpPanelLayout.PanelSlideListener panelSlideListener = new SlidingUpPanelLayout.PanelSlideListener() {
        @Override
        public void onPanelSlide(View panel, float slideOffset) {

        }

        @Override
        public void onPanelCollapsed(View panel) {

        }

        @Override
        public void onPanelExpanded(View panel) {
        }

        @Override
        public void onPanelAnchored(View panel) {

        }

        @Override
        public void onPanelHidden(View view) {


        }
    };
    //endregion

    private void setDateTime(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            DatePickerDialog dpd = (DatePickerDialog)
                    getSupportFragmentManager().findFragmentByTag(DATEPICKER_TAG);
            if (dpd != null) {
                dpd.setOnDateSetListener(this);
            }

            TimePickerDialog tpd = (TimePickerDialog)
                    getSupportFragmentManager().findFragmentByTag(TIMEPICKER_TAG);
            if (tpd != null) {
                tpd.setOnTimeSetListener(this);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    private void setDisplayData() {
        try {
            if (extras != null) {
                value = extras.getInt("id");
                displayData(value);
                if (value > 0) {
                    txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_edit_calendar_item));
                    insertTempAttendees();
                } else {
                    txtStartDate.setText(Utils.setCalendarDate("dd MMM yyyy", new Date(new Date().getTime() + 3600 * 1000)));
                    txtEndDate.setText(Utils.setCalendarDate("dd MMM yyyy", new Date(new Date().getTime() + 2 * 3600 * 1000)));
                    txtStartTime.setText(Utils.setCalendarDate("h:'00' a", new Date(new Date().getTime() + 3600 * 1000)));
                    txtEndTime.setText(Utils.setCalendarDate("h:'00' a", new Date(new Date().getTime() + 2 * 3600 * 1000)));
                    txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_add_calendar_item));
                }
            } else {
                txtStartDate.setText(Utils.setCalendarDate("dd MMM yyyy", new Date(new Date().getTime() + 3600 * 1000)));
                txtEndDate.setText(Utils.setCalendarDate("dd MMM yyyy", new Date(new Date().getTime() + 2 * 3600 * 1000)));
                txtStartTime.setText(Utils.setCalendarDate("h:'00' a", new Date(new Date().getTime() + 3600 * 1000)));
                txtEndTime.setText(Utils.setCalendarDate("h:'00' a", new Date(new Date().getTime() + 2 * 3600 * 1000)));
                txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_add_calendar_item));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void displayData(int id) throws ParseException {
        Uri uri = CalendarProvider.CONTENT_URI;
        Cursor cursor = null;
        try {
            cursor = getBaseContext().getContentResolver().query(uri, null, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "= ?",
                    new String[]{Integer.toString(id)}, DBHelper.CALENDAR_COLUMN_TITLE + " LIMIT 1 ");
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    calendar1 = com.applab.wcircle_pro.Calendar.Calendar.getCalendar(cursor, 0);
                    txtTitle.setText(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_TITLE)));
                    String startDate = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_START_DATE)));
                    datePickerDialogStart = DatePickerDialog.newInstance(this, Integer.valueOf(Utils.setCalendarDate(Utils.DATE_FORMAT, "yyyy", startDate)), Integer.valueOf(Utils.setCalendarDate(Utils.DATE_FORMAT, "M", startDate)), Integer.valueOf(Utils.setCalendarDate(Utils.DATE_FORMAT, "d", startDate)), false);
                    startDate = Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy", startDate);
                    txtStartDate.setText(startDate);

                    String endDate = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_END_DATE)));
                    datePickerDialogEnd = DatePickerDialog.newInstance(this, Integer.valueOf(Utils.setCalendarDate(Utils.DATE_FORMAT, "yyyy", endDate)), Integer.valueOf(Utils.setCalendarDate(Utils.DATE_FORMAT, "M", endDate)), Integer.valueOf(Utils.setCalendarDate(Utils.DATE_FORMAT, "d", endDate)), false);
                    endDate = Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy", endDate);
                    txtEndDate.setText(endDate);

                    String startTime = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_START_DATE)));
                    timePickerDialogStart = TimePickerDialog.newInstance(this, Integer.valueOf(Utils.setCalendarDate(Utils.DATE_FORMAT, "H", startTime)), Integer.valueOf(Utils.setCalendarDate(Utils.DATE_FORMAT, "m", startTime)), false, false);
                    startTime = Utils.setCalendarDate(Utils.DATE_FORMAT, "h:mm a", startTime);
                    txtStartTime.setText(startTime);

                    String endTime = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_END_DATE)));
                    timePickerDialogEnd = TimePickerDialog.newInstance(this, Integer.valueOf(Utils.setCalendarDate(Utils.DATE_FORMAT, "H", endTime)), Integer.valueOf(Utils.setCalendarDate(Utils.DATE_FORMAT, "m", endTime)), false, false);
                    endTime = Utils.setCalendarDate(Utils.DATE_FORMAT, "h:mm a", endTime);
                    txtEndTime.setText(endTime);

                    ediLocation.setText(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_LOCATION)));

                    txtDesc.setText(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_DESCRIPTION)));
                    String i = String.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_ALL_DAY)));
                    isAllDay = i.equals("true");
                    aSwitch.setChecked(isAllDay);

                    String reminder = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT,cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_REMINDER)));
                    if (reminder != null) {
                        Date sDate = Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT,
                                cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_COLUMN_START_DATE))));
                        Date rDate = Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, reminder));
                        long minutes = TimeUnit.MILLISECONDS.toMinutes(sDate.getTime() - rDate.getTime());
                        if (minutes == 30) {
                            spReminder.setSelection(1);
                        } else if (minutes == 60) {
                            spReminder.setSelection(2);
                        } else if (minutes == 120) {
                            spReminder.setSelection(3);
                        } else if (minutes == 180) {
                            spReminder.setSelection(4);
                        } else if (minutes == 1440) {
                            spReminder.setSelection(5);
                        } else if (minutes == 240) {
                            spReminder.setSelection(6);
                        } else if (minutes == 300) {
                            spReminder.setSelection(7);
                        } else if (minutes == 360) {
                            spReminder.setSelection(8);
                        } else if (minutes == 420) {
                            spReminder.setSelection(9);
                        } else if (minutes == 480) {
                            spReminder.setSelection(10);
                        } else {
                            spReminder.setSelection(0);
                        }
                    }

                    if (isAllDay) {
                        txtEndTime.setVisibility(View.GONE);
                        txtStartTime.setVisibility(View.VISIBLE);
                        aSwitch.setChecked(true);
                    } else {
                        txtStartTime.setVisibility(View.VISIBLE);
                        txtEndTime.setVisibility(View.VISIBLE);
                        aSwitch.setChecked(false);
                    }
                }
                while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    //endregion

    //region on click listener
    private TextView.OnClickListener txtEndTimeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            timePickerDialogEnd.setVibrate(false);
            timePickerDialogEnd.setCloseOnSingleTapMinute(false);
            timePickerDialogEnd.show(getSupportFragmentManager(), TIMEPICKER_TAG);
            txt = "txtEndTime";
        }
    };

    private TextView.OnClickListener txtEndDateOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            datePickerDialogEnd.setVibrate(false);
            datePickerDialogEnd.setYearRange(1985, 2028);
            datePickerDialogEnd.setCloseOnSingleTapDay(false);
            datePickerDialogEnd.show(getSupportFragmentManager(), DATEPICKER_TAG);
            txt = "txtEndDate";
        }
    };

    private TextView.OnClickListener txtStartTimeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            timePickerDialogStart.setVibrate(false);
            timePickerDialogStart.setCloseOnSingleTapMinute(false);
            timePickerDialogStart.show(getSupportFragmentManager(), TIMEPICKER_TAG);
            txt = "txtStartTime";
        }
    };

    private TextView.OnClickListener txtStartDateOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            datePickerDialogStart.setVibrate(false);
            datePickerDialogStart.setYearRange(1985, 2028);
            datePickerDialogStart.setCloseOnSingleTapDay(false);
            datePickerDialogStart.show(getSupportFragmentManager(), DATEPICKER_TAG);
            txt = "txtStartDate";
        }
    };

    private LinearLayout.OnClickListener btnLocationOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(context, AddLocationActivity.class));
        }
    };
    //endregion

    //region on touch region
    private void setOnTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                /*if (calendar1 != null) {
                    TextView txtName = (TextView) v.findViewById(R.id.txtName);
                    Attendee attendee = (Attendee) txtName.getTag();
                    if (attendee != null) {
                        if (calendar1.getCreateById().equals(String.valueOf(Utils.getProfile(CalendarDetailsActivity.this).getId()))) {
                            if (attendee.getStatus().equals("reject")) {
                                Intent intent = new Intent(CalendarDetailsActivity.ACTION);
                                intent.putExtra("isPending", true);
                                LocalBroadcastManager.getInstance(CalendarDetailsActivity.this).sendBroadcast(intent);
                                HttpHelper.reinviteAttendee(CalendarDetailsActivity.this, attendee.getCalendarId(), attendee.getEmployeeId(), TAG);
                            }
                        }
                    }
                }*/
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    //endregion

    //region reminder region
    private void initializeSpReminder() {
        spReminder = (Spinner) findViewById(R.id.spReminder);
        calendarSpinnerAdapter = new CalendarSpinnerAdapter(getListReminder(), this);
        spReminder.setAdapter(calendarSpinnerAdapter);
    }

    public static ArrayList<String> getListReminder() {
        ArrayList<String> strList = new ArrayList<>();
        strList.add("Select reminder on the day");
        strList.add("30 minutes before");
        strList.add("1 hour before");
        strList.add("2 hours before");
        strList.add("3 hours before");
        strList.add("1 day before");
        return strList;
    }
    //endregion

    //region on back pressed region
    @Override
    public void onBackPressed() {
        if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
            getContentResolver().delete(TempAttendeesProvider.CONTENT_URI,
                    DBHelper.TEMP_ATTENDEES_COLUMN_IS_SUBMIT + "=? AND "
                            + DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID + "=?",
                    new String[]{"0", String.valueOf(value)});
            if (value > 0) {
                com.applab.wcircle_pro.Calendar.Calendar calendar = getCalendar(value);
                Intent intent = new Intent(this, CalendarEventDetailsActivity.class);
                intent.putExtra("calendar", calendar);
                startActivity(intent);
                finish();
            } else {
                finish();
            }
        } else {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }
    }
    //endregion

    public void onToggleClicked(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            isAllDay = true;
            txtEndTime.setVisibility(View.GONE);
            txtStartTime.setVisibility(View.VISIBLE);
        } else {
            isAllDay = false;
            txtStartTime.setVisibility(View.VISIBLE);
            txtEndTime.setVisibility(View.VISIBLE);
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            /*if (action.equals(ACTION)) {
                if (intent.getBooleanExtra("isSuccess", false)) {
                    setVisibilityRlAndProgressBar(false);
                } else if (intent.getBooleanExtra("isFail", false)) {
                    setVisibilityRlAndProgressBar(false);
                } else if (intent.getBooleanExtra("isPending", false)) {
                    setVisibilityRlAndProgressBar(false);
                }
            }*/
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        if (pref.getString("place", null) != null) {
            ediLocation.setText(pref.getString("place", null));
        }
        IntentFilter iff = new IntentFilter(ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
        setVisibilityRlAndProgressBar(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
        setVisibilityRlAndProgressBar(true);
        Utils.setIsActivityOpen(false, getBaseContext(), false);
    }


    public void btnSubmit(View view) throws ParseException {
        setVisibilityRlAndProgressBar(true);
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        DBHelper helper = new DBHelper(getBaseContext());
        String startDate = "";
        String endDate = "";
        String employeeId = "1";

        if (Utils.getProfile(CalendarDetailsActivity.this).getId() != null) {
            if (Utils.getProfile(CalendarDetailsActivity.this).getId() != null) {
                employeeId = Utils.getProfile(CalendarDetailsActivity.this).getId().toString();
            }
        }

        String title = txtTitle.getText().toString();
        String location = ediLocation.getText().toString();
        String description = txtDesc.getText().toString();
        String attendees = "";
        if (cursor != null) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                attendees += "&Attendees[" + i + "]=" + Utils.encode(cursor.getString(cursor.getColumnIndex(DBHelper.TEMP_ATTENDEES_COLUMN_EMPLOYEE_ID)));
            }
        }
        if (attendees.equals("")) {
            attendees = "&Attendees";
        }
        Log.i("CALENDAR", attendees);
        if (isAllDay) {
            if (!txtStartDate.getText().toString().equals("")) {
                startDate = txtStartDate.getText().toString() + " " + txtStartTime.getText().toString();
                startDate = Utils.setToUTCDate("dd MMM yyyy h:mm a", Utils.DATE_FORMAT, startDate);
            }
            if (!txtEndDate.getText().toString().equals("")) {
                endDate = txtEndDate.getText().toString() + " 11:59 PM";
                endDate = Utils.setToUTCDate("dd MMM yyyy h:mm a", Utils.DATE_FORMAT, endDate);
            }
        } else {
            if (!txtStartDate.getText().toString().equals("")) {
                if (!txtStartTime.getText().toString().equals("")) {
                    startDate = txtStartDate.getText().toString() + " " + txtStartTime.getText().toString();
                    startDate = Utils.setToUTCDate("dd MMM yyyy h:mm a", Utils.DATE_FORMAT, startDate);
                }
            }

            if (!txtEndDate.getText().toString().equals("")) {
                if (!txtEndTime.getText().toString().equals("")) {
                    endDate = txtEndDate.getText().toString() + " " + txtEndTime.getText().toString();
                    endDate = Utils.setToUTCDate("dd MMM yyyy h:mm a", Utils.DATE_FORMAT, endDate);
                }
            }
        }
        String message = validation(startDate, endDate, isAllDay);
        if (message == null) {
            if (value > 0) {
                HttpHelper.putCalendarData(CalendarDetailsActivity.this, String.valueOf(value), title, startDate,
                        endDate, isAllDay, location, description, attendees, getReminder(startDate,
                                spReminder.getSelectedItemPosition()), TAG, value, 0, editor, progressBar, fadeRL);
            } else {
                HttpHelper.postCalendarData(CalendarDetailsActivity.this, employeeId, title, startDate, endDate,
                        isAllDay, location, description, attendees, getReminder(startDate,
                                spReminder.getSelectedItemPosition()), TAG, value, 0, editor, progressBar, fadeRL);
            }
        } else {
            setVisibilityRlAndProgressBar(false);
            ErrorDialogFragment leaveErrorDialogFragment = ErrorDialogFragment.newInstance(CalendarDetailsActivity.this, message);
            leaveErrorDialogFragment.setCancelable(false);
            leaveErrorDialogFragment.show(getSupportFragmentManager(), "");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public static String getReminder(String startDate, int selectedReminder) {
        Date requestedDate = new Date();
        if (selectedReminder != 0) {
            java.util.Calendar gc = new GregorianCalendar();
            gc.setTime(Utils.setCalendarDate(Utils.DATE_FORMAT_PRINT, startDate));
            if (selectedReminder == 1) {
                gc.add(java.util.Calendar.MINUTE, -30);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 2) {
                gc.add(java.util.Calendar.HOUR, -1);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 3) {
                gc.add(java.util.Calendar.HOUR, -2);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 4) {
                gc.add(java.util.Calendar.HOUR, -3);
                requestedDate = gc.getTime();
            }else if (selectedReminder == 5) {
                gc.add(java.util.Calendar.HOUR, -24);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 6) {
                gc.add(java.util.Calendar.HOUR, -4);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 7) {
                gc.add(java.util.Calendar.HOUR, -5);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 8) {
                gc.add(java.util.Calendar.HOUR, -6);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 9) {
                gc.add(java.util.Calendar.HOUR, -7);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 10) {
                gc.add(java.util.Calendar.HOUR, -8);
                requestedDate = gc.getTime();
            }
        } else {
            return "";
        }
        return "&Reminder=" + Utils.encode(Utils.setCalendarDate(Utils.DATE_FORMAT, requestedDate));
    }

    private String validation(String startDate, String endDate, boolean isAllDay) {
        String message = null;
        if (txtTitle.getText().toString().equals("")) {
            message = Utils.getDialogMessage(CalendarDetailsActivity.this, Utils.CODE_TITLE_FIELD, Utils.TITLE_FIELD);
        } else if (txtStartDate.getText().equals("")) {
            message = Utils.getDialogMessage(CalendarDetailsActivity.this, Utils.CODE_START_DATE_FIELD, Utils.START_DATE_FIELD);
        } else if (txtEndDate.getText().equals("")) {
            message = Utils.getDialogMessage(CalendarDetailsActivity.this, Utils.CODE_END_DATE_FIELD, Utils.END_DATE_FIELD);
        } else if (txtStartTime.getText().equals("")) {
            message = Utils.getDialogMessage(CalendarDetailsActivity.this, Utils.CODE_START_TIME_FIELD, Utils.START_TIME_FIELD);
        } else if (txtEndTime.getVisibility() == View.VISIBLE && txtEndTime.getText().equals("")) {
            message = Utils.getDialogMessage(CalendarDetailsActivity.this, Utils.CODE_END_TIME_FIELD, Utils.END_TIME_FIELD);
        } else if (!startDate.equals("") && !endDate.equals("")) {
            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT_PRINT, startDate);
            Date date2 = Utils.setCalendarDate(Utils.DATE_FORMAT_PRINT, endDate);
            if (date2.before(date1)) {
                if (!isAllDay) {
                    message = Utils.getDialogMessage(CalendarDetailsActivity.this, Utils.CODE_END_AFTER_START_DATE,
                            Utils.END_AFTER_START_DATE) + Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyy h:mm a", startDate);
                } else {
                    message = Utils.getDialogMessage(CalendarDetailsActivity.this, Utils.CODE_END_AFTER_START_DATE,
                            Utils.END_AFTER_START_DATE) + Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyy h:mm a", startDate);
                }
            }
        }
        return message;
    }

    public void insertTempAttendees() {
        if (value > 0) {
            Uri uri = AttendeesProvider.CONTENT_URI;
            String selection = DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=?";
            String[] selectionArgs = {String.valueOf(value)};
            Cursor cursor = null;
            try {
                cursor = getContentResolver().query(uri, null, selection, selectionArgs, DBHelper.ATTENDEES_COLUMN_ID + " ASC ");
                if (cursor != null) {
                    ContentValues[] contentValueses = new ContentValues[cursor.getCount()];
                    for (int i = 0; i < cursor.getCount(); i++) {
                        cursor.moveToPosition(i);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_ID, cursor.getString(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_ID)));
                        contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_IS_SUBMIT, cursor.getString(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_IS_SUBMIT)));
                        contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_IS_SELECT, cursor.getString(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_IS_SELECT)));
                        contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_EMPLOYEE_ID, cursor.getInt(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID)));
                        contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID, cursor.getString(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_CALENDAR_ID)));
                        contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_NAME, cursor.getString(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_NAME)));
                        contentValues.put(DBHelper.TEMP_ATTENDEES_COLUMN_STATUS, cursor.getString(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_STATUS)));
                        contentValueses[i] = contentValues;
                    }
                    boolean isEmpty = true;
                    for (Object ob : contentValueses) {
                        if (ob != null) {
                            isEmpty = false;
                        }
                    }
                    if (!isEmpty) {
                        getBaseContext().getContentResolver().bulkInsert(TempAttendeesProvider.CONTENT_URI, contentValueses);
                    }
                }
            } catch (Exception ex) {
                ex.fillInStackTrace();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }

    private void alertDialog(String errorMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CalendarDetailsActivity.this);
        builder.setCancelable(false);
        builder.setMessage(errorMessage);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public void btnCancel(View view) {
        editor.clear();
        editor.apply(); // commit changes
        getContentResolver().delete(TempAttendeesProvider.CONTENT_URI, DBHelper.TEMP_ATTENDEES_COLUMN_IS_SUBMIT +
                        "=? AND " + DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID + "=?",
                new String[]{"0", String.valueOf(value)});
        if (value > 0) {
            com.applab.wcircle_pro.Calendar.Calendar calendar = getCalendar(value);
            Intent intent = new Intent(this, CalendarEventDetailsActivity.class);
            intent.putExtra("calendar", calendar);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
    }

    private com.applab.wcircle_pro.Calendar.Calendar getCalendar(Object calendarId) {
        Cursor cursor = null;
        com.applab.wcircle_pro.Calendar.Calendar calendar = new com.applab.wcircle_pro.Calendar.Calendar();
        try {
            cursor = getContentResolver().query(CalendarProvider.CONTENT_URI, null, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(calendarId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                calendar = com.applab.wcircle_pro.Calendar.Calendar.getCalendar(cursor, 0);
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return calendar;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_calendar_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            getContentResolver().delete(AttendeesProvider.CONTENT_URI,
                    DBHelper.TEMP_ATTENDEES_COLUMN_IS_SUBMIT + "=? AND " +
                            DBHelper.TEMP_ATTENDEES_COLUMN_CALENDAR_ID + "=?",
                    new String[]{"0", String.valueOf(value)});
            if (value > 0) {
                com.applab.wcircle_pro.Calendar.Calendar calendar = getCalendar(value);
                Intent intent = new Intent(this, CalendarEventDetailsActivity.class);
                intent.putExtra("calendar", calendar);
                startActivity(intent);
                finish();
            } else {
                finish();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        String[] m = getResources().getStringArray(R.array.month);
        String strMonth = String.valueOf(m[month]);
        String strDay = "";
        if (day < 10) {
            strDay = "0" + String.valueOf(day);
        } else {
            strDay = String.valueOf(day);
        }
        if (txt.equals("txtStartDate")) {
            txtStartDate.setText(strDay + " " + strMonth + " " + year);
        }

        if (txt.equals("txtEndDate")) {
            txtEndDate.setText(strDay + " " + strMonth + " " + year);
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String strMinute = "";
        String strHourOfDay = "";
        String str = "";
        if (hourOfDay >= 12) {
            if (hourOfDay != 12) {
                hourOfDay = hourOfDay - 12;
            }
            strHourOfDay = "" + hourOfDay;
            str = "PM";
        } else {
            if (hourOfDay == 0) {
                strHourOfDay = "" + 12;
            } else {
                strHourOfDay = "" + hourOfDay;
            }
            str = "AM";
        }
        if (minute < 10) {
            strMinute = "0" + minute;
        } else {
            strMinute = "" + minute;
        }

        if (txt.equals("txtStartTime")) {
            txtStartTime.setText(strHourOfDay + ":" + strMinute + " " + str);
        }

        if (txt.equals("txtEndTime")) {
            txtEndTime.setText(strHourOfDay + ":" + strMinute + " " + str);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = TempAttendeesProvider.CONTENT_URI;
        selectionArgs = new String[]{String.valueOf(value)};
        return new CursorLoader(getBaseContext(), uri, null, selection, selectionArgs, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            this.cursor = data;
            txtInvite.setText(cursor.getCount() > 0 ?
                    String.valueOf(CalendarDetailsActivity.this.getResources().getString(R.string.invited_attendees) + " : " +
                            cursor.getCount()) : CalendarDetailsActivity.this.getResources().getString(R.string.invite_attendees));
            adapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}
