package com.applab.wcircle_pro.Calendar;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/7/2015.
 */
public class BranchViewHolder  extends RecyclerView.ViewHolder {
    TextView title;
    ImageView icon,imgCountry,btnAddId;
    RelativeLayout rl;
    TextView position;
    public BranchViewHolder(View itemView) {
        super(itemView);
        title= (TextView) itemView.findViewById( R.id.txtName);
        icon= (ImageView) itemView.findViewById( R.id.imgIcon);
        imgCountry= (ImageView) itemView.findViewById( R.id.imgCountry);
        btnAddId = (ImageView)itemView.findViewById(R.id.btnAddId);
        position = (TextView) itemView.findViewById(R.id.txtPosition);
    }
}
