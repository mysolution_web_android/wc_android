package com.applab.wcircle_pro.Calendar;

import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attendee implements Parcelable {

    @SerializedName("EmployeeId")
    @Expose
    private Integer EmployeeId;
    @SerializedName("EmployeyName")
    @Expose
    private String EmployeyName;
    @SerializedName("Reminder")
    @Expose
    private String Reminder;
    @SerializedName("Status")
    @Expose
    private String Status;
    private int CalendarId;

    public int getCalendarId() {
        return CalendarId;
    }

    public void setCalendarId(int calendarId) {
        CalendarId = calendarId;
    }

    public Attendee() {
    }

    public Attendee(Parcel in) {
        readFromParcel(in);
    }

    /**
     * @return The EmployeeId
     */
    public Integer getEmployeeId() {
        return EmployeeId;
    }

    /**
     * @param EmployeeId The EmployeeId
     */
    public void setEmployeeId(Integer EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    /**
     * @return The EmployeyName
     */
    public String getEmployeyName() {
        return EmployeyName;
    }

    /**
     * @param EmployeyName The EmployeyName
     */
    public void setEmployeyName(String EmployeyName) {
        this.EmployeyName = EmployeyName;
    }

    /**
     * @return The Reminder
     */
    public String getReminder() {
        return Reminder;
    }

    /**
     * @param Reminder The Reminder
     */
    public void setReminder(String Reminder) {
        this.Reminder = Reminder;
    }

    /**
     * @return The Status
     */
    public String getStatus() {
        return Status;
    }

    /**
     * @param Status The Status
     */
    public void setStatus(String Status) {
        this.Status = Status;
    }

    public static Attendee getAttendee(Cursor cursor, int position) {
        Attendee attendee = new Attendee();
        try {
            cursor.moveToPosition(position);
            attendee.setEmployeeId(cursor.getInt(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID)));
            attendee.setEmployeyName(cursor.getString(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_NAME)));
            attendee.setReminder(cursor.getString(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_REMINDER)));
            attendee.setStatus(cursor.getString(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_STATUS)));
            attendee.setCalendarId(cursor.getInt(cursor.getColumnIndex(DBHelper.ATTENDEES_COLUMN_CALENDAR_ID)));
        } catch (CursorIndexOutOfBoundsException | NullPointerException exception) {
            exception.fillInStackTrace();
            attendee = null;
        }
        return attendee;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.EmployeyName);
        dest.writeString(this.Reminder);
        dest.writeString(this.Status);
        dest.writeInt(this.EmployeeId);


    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.EmployeyName = in.readString();
        this.Reminder = in.readString();
        this.Status = in.readString();
        this.EmployeeId = in.readInt();

    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Attendee createFromParcel(Parcel in) {
            return new Attendee(in);
        }

        @Override
        public Attendee[] newArray(int size) {
            return new Attendee[size];
        }
    };

}