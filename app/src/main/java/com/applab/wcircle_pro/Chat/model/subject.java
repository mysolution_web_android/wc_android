package com.applab.wcircle_pro.Chat.model;

/**
 * Created by user on 7/1/2016.
 */
public class subject {
    String isgroup = "false";
    String isimage = "false";
    String isdelete = "false";
    String groupname;
    String groupid;
    String isgroupupdate = "false";
    String senddate;
    String status;
    String from;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSenddate() {
        return senddate;
    }

    public void setSenddate(String senddate) {
        this.senddate = senddate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsgroupupdate() {
        return isgroupupdate;
    }

    public void setIsgroupupdate(String isgroupupdate) {
        this.isgroupupdate = isgroupupdate;
    }

    public String getIsgroup() {
        return isgroup;
    }

    public void setIsgroup(String isgroup) {
        this.isgroup = isgroup;
    }

    public String getIsimage() {
        return isimage;
    }

    public void setIsimage(String isimage) {
        this.isimage = isimage;
    }

    public String getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(String isdelete) {
        this.isdelete = isdelete;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }
}
