package com.applab.wcircle_pro.Chat;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.xmpp.Config;
import com.applab.wcircle_pro.Chat.xmpp.XMPPConn;
import com.applab.wcircle_pro.R;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;

import java.util.ArrayList;
import java.util.List;

public class InviteMemberActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView txtToolbarTitle;
    private AbstractXMPPConnection connection = null;
    private MultiUserChatManager manager;
    private MultiUserChat muc;

    private LinearLayout memberView;
    private String roomName;

    private int id = 0;
    private List<EditText> editTexts = new ArrayList<EditText>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_member);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green_light));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setGravity(Gravity.CENTER);
        txtToolbarTitle.setText("Invite Member");
        txtToolbarTitle.setPadding(0, 0, 100, 0);

        roomName = getIntent().getStringExtra("roomName");
        connection = XMPPConn.getInstance().getConn();
        manager = MultiUserChatManager.getInstanceFor(connection);
        muc = XMPPConn.getInstance().getMuc(roomName);

        Button addEditText = (Button)findViewById(R.id.addEditText);
        final Button inviteMemberBtn = (Button)findViewById(R.id.inviteMemberBtn);
        memberView = (LinearLayout)findViewById(R.id.memberView);


        addEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                addEditText();
            }
        });

        inviteMemberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                for (EditText editText : editTexts) {

                    // whatever u want to do with the strings
                    List<String> usernameList = new ArrayList<String>();
                    usernameList.add(editText.getText().toString());
                    inviteMember(usernameList);
                }
            }
        });
    }

    private void addEditText() {

        EditText editText1 = new EditText(this);
        editText1.setId(id++);
        editText1.setHint("Type the name");
        editText1.setTextColor(ContextCompat.getColor(this, R.color.primary_text));
        editText1.setHintTextColor(ContextCompat.getColor(this, R.color.secondary_text));
        editText1.setInputType(InputType.TYPE_CLASS_TEXT);
        memberView.addView(editText1);
        editTexts.add(editText1);
    }

    private void inviteMember(List<String> usernameList)
    {
        for (String username :usernameList)
        {
            try {
                muc.invite(username +"@"+ Config.SERVER_IP + "/spark", "Meet me in this excellent room");
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        }

        Intent intent = new Intent(InviteMemberActivity.this,ChartRoomGroupActivity.class);
        intent.putExtra("roomId", roomName);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_invite_member, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
