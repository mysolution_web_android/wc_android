package com.applab.wcircle_pro.Chat.model;

/**
 * Created by user on 7/1/2016.
 */
public class array {
    String from;

    String id;
    String type;
    String to;
    String body;
    subject subject;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public com.applab.wcircle_pro.Chat.model.subject getSubject() {
        return subject;
    }

    public void setSubject(com.applab.wcircle_pro.Chat.model.subject subject) {
        this.subject = subject;
    }
}
