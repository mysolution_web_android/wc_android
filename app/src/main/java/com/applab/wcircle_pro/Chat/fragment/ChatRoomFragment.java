package com.applab.wcircle_pro.Chat.fragment;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.applab.wcircle_pro.Chat.ChatActivity;
import com.applab.wcircle_pro.Chat.adapter.ChatRoomAdapter;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.ChatListProvider;
import com.applab.wcircle_pro.Chat.db.GroupUserProvider;
import com.applab.wcircle_pro.Chat.model.MessageModel;
import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.Chat.xmpp.ChatUploadService;
import com.applab.wcircle_pro.Chat.xmpp.OnMessageIncominglistener;
import com.applab.wcircle_pro.Chat.xmpp.XMPPConn;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import org.jivesoftware.smack.AbstractXMPPConnection;

import java.io.File;
import java.util.ArrayList;

public class ChatRoomFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView recyclerView;
    private EditText txtMessage;
    private ChatRoomAdapter adapter;
    private LinearLayoutManager layoutManager;
    private BackgroundXMPPServiceConnection backgroundXMPPServiceConnection;
    private BackgroundXMPPService backgroundXMPPService;
    public AbstractXMPPConnection connection = null;
    private String selfId;
    private String friendId;
    private Cursor cursor;
    public static LoaderManager.LoaderCallbacks callbacks;
    public static LoaderManager loaderManager;
    private ImageView btnSend;
    public static String TAG = "CHART ROOM FRAGMENT";
    public File mFile;

    public static ChatRoomFragment newInstance(File file) {
        ChatRoomFragment fragment = new ChatRoomFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("file", file);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.fragment_chat_room, container, false);
        txtMessage = (EditText) layout.findViewById(R.id.txtMessage);
        txtMessage.setFocusable(true);
        callbacks = this;
        loaderManager = this.getLoaderManager();
        Intent intent = getActivity().getIntent();
        friendId = intent.getStringExtra("friendId");
        selfId = intent.getStringExtra("selfId");
        btnSend = (ImageView) layout.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(btnSendOnClickListener);
        getActivity().getSupportLoaderManager().initLoader(0, null, this);
        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        adapter = new ChatRoomAdapter(getActivity(), cursor);
        recyclerView.setAdapter(adapter);
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        clearNotification();
        mFile = (File) getArguments().getSerializable("file");
        sendFile();
        return layout;
    }

    private void sendFile() {
        if (mFile != null) {
            if (connection != null) {
                if (connection.isConnected()) {
                    String threadId = BackgroundXMPPService.getThreadId(getActivity(), mFile.getAbsolutePath(),
                            friendId, 0, 1, null, null, ChatRoomFragment.loaderManager,
                            ChatRoomFragment.callbacks, 0);
                    if (threadId != null) {
                        uploadImage(mFile, threadId);
                        mFile = null;
                    }
                } else {
                    initChatRoom(friendId);
                }
            } else {
                initChatRoom(friendId);
            }
        }
    }

    private void clearNotification() {
        Cursor cursor = null;
        Integer notificationId = 0;
        try {
            cursor = getActivity().getContentResolver().query(ChatListProvider.CONTENT_URI, null,
                    DBHelper.CHAT_LIST_COLUMN_JID + "=? AND "
                            + DBHelper.CHAT_LIST_COLUMN_GROUP_ID + " IS NULL ", new String[]{friendId}, null);
            if (cursor != null && cursor.moveToLast()) {
                notificationId = cursor.getInt(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_USER_ID));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            NotificationManager notificationManager = (NotificationManager)
                    getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationId);
        }
    }

    private View.OnClickListener btnSendOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            BackgroundXMPPService.sendMessage(getActivity(), txtMessage.getText().toString(), friendId, 0, 0, null, null,
                    TAG, getActivity().getSupportLoaderManager(), callbacks, 0, false);
            getActivity().getSupportLoaderManager().restartLoader(0, null, ChatRoomFragment.this);
            txtMessage.setText("");
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), GroupUserProvider.CONTENT_URI, null,
                DBHelper.CHAT_COLUMN_FROM + " IN (?,?) AND " + DBHelper.CHAT_COLUMN_TO + " IN (?,?) AND " +
                        DBHelper.CHAT_COLUMN_GROUP_ID + " IS NULL ",
                new String[]{selfId, friendId, selfId, friendId}, "DATETIME(" + DBHelper.CHAT_COLUMN_TIME + ") ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            if (data.getCount() > 0) {
                cursor = data;
                adapter.swapCursor(cursor);
                if (adapter.getItemCount() > 1) {
                    recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public class BackgroundXMPPServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            backgroundXMPPService = ((BackgroundXMPPService.LocalBinder) binder).getService();
            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.add(friendId);
            backgroundXMPPService.setArrUserInGroup(arrayList, null);
            backgroundXMPPService.setOnMessageIncominglistener(new OnMessageIncominglistener() {
                @Override
                public void onMessangeIncoming(final MessageModel msg, final String from) {
                    if (from.equalsIgnoreCase(friendId)) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (msg.getMessage() != null) {
                                    getActivity().getSupportLoaderManager().restartLoader(0, null, callbacks);
                                }
                            }
                        });
                    }
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // TODO Auto-generated method stub
            backgroundXMPPService = null;
        }
    }

    private void initChatRoom(String friendId) {
        ChatDBHelper.resetCount(getActivity(), friendId, null);
        connection = XMPPConn.getInstance().getConn();
        if (Utils.isConnectingToInternet(getActivity())) {
            if (connection != null) {
                if (!connection.isConnected()) {
                    connection.disconnect();
                    getActivity().startService(new Intent(getActivity(), BackgroundXMPPService.class));
                } else {
                    try {
                        backgroundXMPPServiceConnection = new BackgroundXMPPServiceConnection();
                        getActivity().bindService(new Intent(getActivity(), BackgroundXMPPService.class),
                                backgroundXMPPServiceConnection, getActivity().BIND_ADJUST_WITH_ACTIVITY);
                    } catch (Exception ex) {
                        ex.fillInStackTrace();
                    }
                }
            } else {
                getActivity().startService(new Intent(getActivity(), BackgroundXMPPService.class));
            }
        } else {
            Utils.showNoConnection(getActivity());
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BackgroundXMPPService.ACTION)) {
                if (intent.getBooleanExtra("error", false)) {
                    startActivity(new Intent(getActivity(), ChatActivity.class));
                    getActivity().finish();
                } else if (intent.getBooleanExtra("refresh", false)) {
                    getActivity().getSupportLoaderManager().restartLoader(0, null, callbacks);
                } else if (intent.getBooleanExtra("login_available", false)) {
                    if (mFile != null) {
                        String threadId = BackgroundXMPPService.getThreadId(getActivity(), mFile.getAbsolutePath(),
                                friendId, 0, 1, null, null, ChatRoomFragment.loaderManager,
                                ChatRoomFragment.callbacks, 0);
                        if (threadId != null) {
                            uploadImage(mFile, threadId);
                            mFile = null;
                        }
                    }
                }
            }
        }
    };

    private boolean uploadImage(File file, String threadId) {
        boolean success = false;
        Intent intent = new Intent(getActivity(), ChatUploadService.class);
        if (file.length() < 3 * 1000000) {
            String[] result = file.getName().split("\\.");
            if (result[result.length - 1].equalsIgnoreCase("jpg") || result[result.length - 1].
                    equalsIgnoreCase("png") || result[result.length - 1].equalsIgnoreCase("jpeg")) {
                intent.putExtra("filePath", file.getAbsolutePath());
                intent.putExtra("threadId", threadId);
                success = true;
                intent.putExtra("oxUser", Utils.getProfile(getActivity()).getOXUser());
                getActivity().startService(intent);
            } else {
                Utils.showError(getActivity(), "", "The image must be jpg or png.");
            }
        } else {
            Utils.showError(getActivity(), "", "The image is too large.");
        }
        return success;
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
        ChatDBHelper.resetCount(getActivity(), friendId, null);
        try {
            if (backgroundXMPPService != null) {
                backgroundXMPPService.setOnMessageIncominglistener(null);
                backgroundXMPPService.setArrUserInGroup(new ArrayList<String>(), null);
            }
            if (backgroundXMPPServiceConnection != null) {
                getActivity().unbindService(backgroundXMPPServiceConnection);
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ChatDBHelper.resetCount(getActivity(), friendId, null);
        IntentFilter iff = new IntentFilter(BackgroundXMPPService.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
        initChatRoom(friendId);
    }
}
