package com.applab.wcircle_pro.Chat.xmpp;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by user on 1/12/2015.
 */
public class MultiPartChat<T> extends Request<T> {

    private MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
    private final Response.Listener<T> mListener;
    private final File mFilePart;
    private final MultipartProgressListener multipartProgressListener;
    private long mFileLength = 0L;
    private Class<T> clazz;
    private Map<String, String> headers;
    private Gson mGson = new Gson();
    private String TAG = "MULTIPART REQUEST";
    private String mOxUser;
    private HttpEntity httpEntitiy;

    public MultiPartChat(int method,
                         String url,
                         Class<T> clazz,
                         Map<String, String> headers,
                         Response.Listener<T> listener,
                         Response.ErrorListener errorListener,
                         File file,
                         MultipartProgressListener progressListener,
                         String oxUser) {
        super(method, url, errorListener);
        mGson = new Gson();
        this.clazz = clazz;
        this.mListener = listener;
        this.mFilePart = file;
        this.mFileLength = file == null ? 0L : file.length();
        this.multipartProgressListener = progressListener;
        this.headers = headers;
        this.mOxUser = oxUser;
        buildMultipartEntity();
    }

    private void buildMultipartEntity() {
        mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        mBuilder.addTextBody("OXUser", mOxUser);
        if (mFilePart != null) {
            String[] result = mFilePart.getName().split("\\.");
            String type = result[result.length - 1].equals("jpg") ||
                    result[result.length - 1].equals("jpeg") ? "jpeg" : "png";
            mBuilder.addBinaryBody("FileImage", mFilePart, ContentType.create("image/" + type), mFilePart.getName());
        }
        httpEntitiy = mBuilder.build();
    }

    @Override
    public String getBodyContentType() {
        if (mFilePart != null) {
            return httpEntitiy.getContentType().getValue();
        } else {
            return ContentType.APPLICATION_FORM_URLENCODED.toString();
        }
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        if (mFilePart != null) {
            mFileLength = httpEntitiy.getContentLength();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                httpEntitiy.writeTo(new CountingOutputStream(bos, mFileLength, multipartProgressListener));
            } catch (IOException e) {
                VolleyLog.e(e.getMessage());
            }
            Log.i(TAG, bos.toString());
            return bos.toByteArray();
        } else {
            String httpBody = "OXUser=" + mOxUser;
            return Utils.encode(httpBody).getBytes();
        }
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Log.i(TAG, "StatusCode: " + response.statusCode);
            Log.i(TAG, "Response Data: " + json);
            return Response.success(mGson.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            Log.i(TAG, "Failure 1: " + e.getMessage());
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            Log.i(TAG, "Failure 2: " + e.getMessage());
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    public static interface MultipartProgressListener {
        void transferred(long transferred, long progress);
    }

    public static class CountingOutputStream extends FilterOutputStream {
        private final MultipartProgressListener progListener;
        private long transferred;
        private long mFileLength;

        public CountingOutputStream(final OutputStream out, long mFileLength,
                                    final MultipartProgressListener listener) {
            super(out);
            this.mFileLength = mFileLength;
            this.progListener = listener;
            this.transferred = 0;
        }

        public void write(byte[] b, int off, int len) throws IOException {
            out.write(b, off, len);
            if (progListener != null) {
                this.transferred += len;
                int prog = (int) (transferred * 100 / mFileLength);
                this.progListener.transferred(this.transferred, prog);
            }
        }

        public void write(int b) throws IOException {
            out.write(b);
            if (progListener != null) {
                this.transferred++;
                int prog = (int) (transferred * 100 / mFileLength);
                this.progListener.transferred(this.transferred, prog);
            }
        }
    }
}