package com.applab.wcircle_pro.Chat.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/7/2015.
 */
public class FavoriteViewHolder extends RecyclerView.ViewHolder {
    public TextView title;
    public ImageView icon;
    public ImageView imgCountry;
    public ImageView btnAddId;
    public TextView department;

    public FavoriteViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.txtName);
        icon = (ImageView) itemView.findViewById(R.id.imgIcon);
        imgCountry = (ImageView) itemView.findViewById(R.id.imgCountry);
        btnAddId = (ImageView) itemView.findViewById(R.id.btnAddId);
        department = (TextView) itemView.findViewById(R.id.txtDepartment);
    }
}