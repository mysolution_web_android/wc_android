package com.applab.wcircle_pro.Chat.fragment;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.AddGroupMemberActivity;
import com.applab.wcircle_pro.Chat.ChartRoomGroupActivity;
import com.applab.wcircle_pro.Chat.ChatActivity;
import com.applab.wcircle_pro.Chat.adapter.ChartRoomGroupProfileImageAdapter;
import com.applab.wcircle_pro.Chat.adapter.ChatRoomAdapter;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.ChatListProvider;
import com.applab.wcircle_pro.Chat.db.GroupChatProvider;
import com.applab.wcircle_pro.Chat.db.GroupProvider;
import com.applab.wcircle_pro.Chat.db.GroupUserProvider;
import com.applab.wcircle_pro.Chat.db.HttpHelper;
import com.applab.wcircle_pro.Chat.model.MessageModel;
import com.applab.wcircle_pro.Chat.model.NewMember;
import com.applab.wcircle_pro.Chat.model.User;
import com.applab.wcircle_pro.Chat.module.ChartRoomGroup;
import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.Chat.xmpp.ChatUploadService;
import com.applab.wcircle_pro.Chat.xmpp.OnMessageIncominglistener;
import com.applab.wcircle_pro.Chat.xmpp.XMPPConn;
import com.applab.wcircle_pro.Employee.EmployeeCallDialogFragment;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;

import org.jivesoftware.smack.AbstractXMPPConnection;

import java.io.File;
import java.util.ArrayList;

public class ChartRoomGroupFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView recyclerView, horizontalRecyclerView;
    private EditText txtMessage;
    private ChatRoomAdapter adapter;
    private ChartRoomGroupProfileImageAdapter chartRoomGroupProfileImageAdapter;
    private LinearLayoutManager layoutManager;
    private AbstractXMPPConnection connection = null;
    private BackgroundXMPPServiceConnection backgroundXMPPServiceConnection;
    private BackgroundXMPPService backgroundXMPPService;
    private String groupId, groupName;
    public Cursor cursor, cursorMessage;
    private String TAG = "GROUP ACTIVITY";
    private ArrayList<String> arrUserInGroup = new ArrayList<>();
    private TextView txtError;
    private ScrollView scrollView;
    public static LoaderManager.LoaderCallbacks callbacks;
    private ImageView btnSend;
    private boolean isRemove = true;
    public static FragmentManager fragmentManager;
    public static LoaderManager loaderManager;
    private File mFile;

    public static ChartRoomGroupFragment newInstance(File file) {
        ChartRoomGroupFragment fragment = new ChartRoomGroupFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("file", file);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.fragment_chart_room_group, container, false);
        Intent intent = getActivity().getIntent();
        groupId = intent.getStringExtra("groupId");
        groupName = intent.getStringExtra("groupName");
        callbacks = this;
        loaderManager = this.getLoaderManager();
        btnSend = (ImageView) layout.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(btnSendOnClickListener);
        txtMessage = (EditText) layout.findViewById(R.id.txtMessage);
        txtMessage.setFocusable(true);
        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        adapter = new ChatRoomAdapter(getActivity(), cursorMessage);
        recyclerView.setAdapter(adapter);
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        txtError = (TextView) layout.findViewById(R.id.txtError);
        scrollView = (ScrollView) layout.findViewById(R.id.scrollView);
        txtError.setVisibility(View.GONE);
        horizontalRecyclerView = (RecyclerView) layout.findViewById(R.id.horizontalRecyclerView);
        chartRoomGroupProfileImageAdapter = new ChartRoomGroupProfileImageAdapter(getActivity(), cursor);
        horizontalRecyclerView.setAdapter(chartRoomGroupProfileImageAdapter);
        horizontalRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        setTouchListener();
        clearNotification();
        fragmentManager = getActivity().getSupportFragmentManager();
        mFile = (File) getArguments().getSerializable("file");
        sendFile();
        return layout;
    }

    private void sendFile() {
        if (mFile != null) {
            if (connection != null) {
                if (connection.isConnected()) {
                    String threadId = BackgroundXMPPService.getThreadId(getActivity(), mFile.getAbsolutePath(), null, 1, 1,
                            groupName, groupId, ChartRoomGroupFragment.loaderManager, ChartRoomGroupFragment.callbacks, 234);
                    if (threadId != null) {
                        uploadImage(mFile, threadId);
                        mFile = null;
                    }
                } else {
                    initChatRoom(groupId);
                }
            } else {
                initChatRoom(groupId);
            }
        }
    }

    private void initChatRoom(String groupId) {
        ChatDBHelper.resetCount(getActivity(), null, groupId);
        connection = XMPPConn.getInstance().getConn();
        if (Utils.isConnectingToInternet(getActivity())) {
            if (connection != null) {
                if (!connection.isConnected()) {
                    connection.disconnect();
                    getActivity().startService(new Intent(getActivity(), BackgroundXMPPService.class));
                } else {
                    try {
                        HttpHelper.GetSingleGroup(getActivity(), TAG, groupId, false);
                        backgroundXMPPServiceConnection = new BackgroundXMPPServiceConnection();
                        getActivity().bindService(new Intent(getActivity(), BackgroundXMPPService.class),
                                backgroundXMPPServiceConnection, getActivity().BIND_ADJUST_WITH_ACTIVITY);
                    } catch (Exception ex) {
                        ex.fillInStackTrace();
                    }
                }
            } else {
                getActivity().startService(new Intent(getActivity(), BackgroundXMPPService.class));
            }
        } else {
            Utils.showNoConnection(getActivity());
        }
    }

    private void clearNotification() {
        Cursor cursor = null;
        Integer notificationId = 0;
        try {
            cursor = getActivity().getContentResolver().query(ChatListProvider.CONTENT_URI, null,
                    DBHelper.CHAT_LIST_COLUMN_GROUP_ID + "=?", new String[]{groupId}, null);
            if (cursor != null && cursor.moveToLast()) {
                notificationId = cursor.getInt(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_USER_ID));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            NotificationManager notificationManager = (NotificationManager)
                    getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationId);
        }
    }


    private View.OnClickListener btnSendOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            BackgroundXMPPService.sendMessage(getActivity(), txtMessage.getText().toString(), null, 1, 0, groupName, groupId, TAG,
                    getActivity().getSupportLoaderManager(), callbacks, 234, false);
            getActivity().getSupportLoaderManager().restartLoader(234, null, ChartRoomGroupFragment.this);
            txtMessage.setText("");
        }
    };

    private void setTouchListener() {
        ItemClickSupport.addTo(horizontalRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                cursor.moveToPosition(position);
                User current = User.getGroupUser(cursor, position);
                User user = ChatDBHelper.getSingleUser(getActivity(), current.getJid());
                if (user != null) {
                    if (user.getEmployeeName() != null) {
                        if (!user.getEmployeeName().equals("")) {
                            if (user.getEmployeeId() != Utils.getProfile(getActivity()).getId()) {
                                Utils.postRecentTrack(getActivity(), "call",
                                        String.valueOf(user.getEmployeeId()));
                                EmployeeCallDialogFragment employeeCallDialogFragment =
                                        EmployeeCallDialogFragment.newInstance(user, getActivity());
                                employeeCallDialogFragment.show(getActivity().getSupportFragmentManager(), "");
                            }
                        }
                    }
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    public class BackgroundXMPPServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            backgroundXMPPService = ((BackgroundXMPPService.LocalBinder) binder).getService();
            backgroundXMPPService.setArrUserInGroup(new ArrayList<String>(), groupId);
            backgroundXMPPService.setOnMessageIncominglistener(new OnMessageIncominglistener() {
                @Override
                public void onMessangeIncoming(final MessageModel msg, final String from) {
                    if (arrUserInGroup.contains(from)) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (msg.getMessage() != null) {
                                    getActivity().getSupportLoaderManager().restartLoader(234, null, callbacks);
                                }
                            }
                        });
                    }
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // TODO Auto-generated method stub
            backgroundXMPPService = null;
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BackgroundXMPPService.ACTION)) {
                if (intent.getBooleanExtra("error", false)) {
                    startActivity(new Intent(getActivity(), ChatActivity.class));
                    getActivity().finish();
                } else if (intent.getBooleanExtra("remove success", false)) {
                    txtError.setText(Utils.getDialogMessage(getActivity(), Utils.CODE_ROOM_CLOSED, Utils.ROOM_CLOSED));
                    txtError.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                } else if (intent.getBooleanExtra("isAddPeople", false)) {
                    if (Utils.isConnectingToInternet(getActivity())) {
                        if (XMPPConn.getInstance().getConn() != null) {
                            if (!XMPPConn.getInstance().getConn().isConnected()) {
                                XMPPConn.getInstance().getConn().disconnect();
                                getActivity().startService(new Intent(getActivity(), BackgroundXMPPService.class));
                            } else {
                                Intent mIntent = new Intent(getActivity(), AddGroupMemberActivity.class);
                                ArrayList<NewMember> addedEmployee = new ArrayList<>();
                                if (cursor != null) {
                                    for (int i = 0; i < cursor.getCount(); i++) {
                                        cursor.moveToPosition(i);
                                        if (!cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_OX_USER)).
                                                equals(Utils.getProfile(getActivity()).getOXUser())) {
                                            NewMember member = new NewMember();
                                            member.setOxUser(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_OX_USER)));
                                            member.setName(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_NAME)));
                                            member.setImage(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_IMAGE)));
                                            member.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_COUNTRY_IMAGE)));
                                            member.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_POSITION)));
                                            member.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID)));
                                            member.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_CONTACT_NO)));
                                            member.setOfficeNo(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_OFFICE_NO)));
                                            Log.i(TAG, member.getOxUser());
                                            addedEmployee.add(member);
                                        }
                                    }
                                }
                                mIntent.putExtra("addedEmployee", addedEmployee);
                                mIntent.putExtra("prevAddedEmployee", addedEmployee);
                                mIntent.putExtra("isAdd", true);
                                mIntent.putExtra("groupId", groupId);
                                mIntent.putExtra("groupName", groupName);
                                startActivity(mIntent);
                            }
                        } else {
                            getActivity().startService(new Intent(getActivity(), BackgroundXMPPService.class));
                        }
                    } else {
                        Utils.showNoConnection(getActivity());
                    }
                } else if (intent.getBooleanExtra("refresh", false)) {
                    getActivity().getSupportLoaderManager().restartLoader(234, null, callbacks);
                } else if (intent.getBooleanExtra("login_available", false)) {
                    if (mFile != null) {
                        String threadId = BackgroundXMPPService.getThreadId(getActivity(), mFile.getAbsolutePath(), null, 1, 1,
                                groupName, groupId, ChartRoomGroupFragment.loaderManager, ChartRoomGroupFragment.callbacks, 234);
                        if (threadId != null) {
                            uploadImage(mFile, threadId);
                            mFile = null;
                        }
                    }
                }
            }
        }
    };

    private void uploadImage(File file, String threadId) {
        Intent intent = new Intent(getActivity(), ChatUploadService.class);
        if (file.length() < 3 * 1000000) {
            String[] result = file.getName().split("\\.");
            if (result[result.length - 1].equalsIgnoreCase("jpg") || result[result.length - 1].
                    equalsIgnoreCase("png") || result[result.length - 1].equalsIgnoreCase("jpeg")) {
                intent.putExtra("filePath", file.getAbsolutePath());
                intent.putExtra("threadId", threadId);
                intent.putExtra("oxUser", Utils.getProfile(getActivity()).getOXUser());
                getActivity().startService(intent);
            } else {
                Utils.showError(getActivity(), "", "The image must be jpg or png.");
            }
        } else {
            Utils.showError(getActivity(), "", "The image is too large.");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ChatDBHelper.resetCount(getActivity(), null, groupId);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
        try {
            if (backgroundXMPPService != null) {
                backgroundXMPPService.setOnMessageIncominglistener(null);
                backgroundXMPPService.setArrUserInGroup(new ArrayList<String>(), null);
            }
            if (backgroundXMPPServiceConnection != null) {
                getActivity().unbindService(backgroundXMPPServiceConnection);
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initChatRoom(groupId);
        ChatDBHelper.resetCount(getActivity(), null, groupId);
        IntentFilter iff = new IntentFilter(BackgroundXMPPService.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
        getActivity().getSupportLoaderManager().initLoader(568, null, this);
        getActivity().getSupportLoaderManager().initLoader(234, null, this);
        getActivity().getSupportLoaderManager().initLoader(7821, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 568) {
            return new CursorLoader(getActivity(), GroupChatProvider.CONTENT_URI, null,
                    DBHelper.GROUP_CHAT_COLUMN_GROUP_ID + "=?", new String[]{groupId},
                    "case when " + DBHelper.GROUP_CHAT_COLUMN_OX_USER + " = " + DBHelper.GROUP_CHAT_COLUMN_OWNER + " then -1 else id end");
        } else if (id == 234) {
            return new CursorLoader(getActivity(), GroupUserProvider.CONTENT_URI, null, DBHelper.CHAT_COLUMN_GROUP_ID + "=?",
                    new String[]{groupId}, "DATETIME(" + DBHelper.CHAT_COLUMN_TIME + ") ASC");
        } else if (id == 7821) {
            return new CursorLoader(getActivity(), GroupProvider.CONTENT_URI, null, DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{groupId}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 568) {
            if (data != null) {
                if (data.getCount() > 0) {
                    cursor = data;
                    if (arrUserInGroup.size() > 0) {
                        arrUserInGroup.clear();
                    }
                    ArrayList<User> users = new ArrayList<>();
                    for (int i = 0; i < data.getCount(); i++) {
                        User user = User.getGroupUser(cursor, i);
                        arrUserInGroup.add(user.getJid());
                        users.add(user);
                        if (Utils.getProfile(getActivity()).getOXUser().equals(user.getJid())) {
                            isRemove = false;
                        }
                    }
                    chartRoomGroupProfileImageAdapter.swapCursor(cursor);
                    if (isRemove) {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(Utils.getDialogMessage(getActivity(), Utils.CODE_ROOM_CLOSED, Utils.ROOM_CLOSED));
                        scrollView.setVisibility(View.GONE);
                    }
                }
            }
        } else if (loader.getId() == 234) {
            if (data != null) {
                if (data.getCount() > 0) {
                    cursorMessage = data;
                    adapter.swapCursor(cursorMessage);
                    if (adapter.getItemCount() > 1) {
                        recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
                    }
                }
            }
        } else if (loader.getId() == 7821) {
            if (data != null) {
                if (data.getCount() > 0) {
                    data.moveToFirst();
                    boolean isDelete = false;
                    if (!data.isNull(data.getColumnIndex(DBHelper.GROUP_COLUMN_IS_DELETE))) {
                        isDelete = Boolean.valueOf(data.getString(data.getColumnIndex(DBHelper.GROUP_COLUMN_IS_DELETE)));
                    }
                    if (isDelete) {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(Utils.getDialogMessage(getActivity(), Utils.CODE_ROOM_CLOSED, Utils.ROOM_CLOSED));
                        scrollView.setVisibility(View.GONE);
                    } else {
                        txtError.setVisibility(View.GONE);
                        scrollView.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
