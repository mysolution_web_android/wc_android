package com.applab.wcircle_pro.Chat.xmpp;


/**
 * Created by user on 22/10/2015.
 */

public class RestApiUtils {
    /*public AuthenticationToken authenticationToken;
    public UserEntity userEntity;
    public RestApiClient restApiClient;
    public MUCRoomEntity chatRoom;
    public SystemProperty systemProperty;
    public GroupEntity groupEntity;
    public RosterItemEntity rosterItemEntity;


    //region authentication region
    // Basic HTTP Authentication
    public AuthenticationToken getAuthenticationToken(String userName, String password) {
        authenticationToken = new AuthenticationToken(userName, password);
        return authenticationToken;
    }

    //Shared secret key
    public AuthenticationToken getAuthenticationToken(String secretKey) {
        authenticationToken = new AuthenticationToken(secretKey);
        return authenticationToken;
    }

    //Set Openfire settings (9090 is the port of Openfire Admin Console)
    public void setRestApiClient(String baseUrl, int port, AuthenticationToken authenticationToken) {
        restApiClient = new RestApiClient(baseUrl, port, authenticationToken);
    }
    //endregion

    //region user region
    //Get all the user
    public UserEntities getUserEntities() {
        return restApiClient.getUsers();
    }

    //Get specific user by userName
    public UserEntity getUserEntity(String userName) {
        return restApiClient.getUser(userName);
    }

    // Search for the user with the userName "test". This act like the wildcard search %String%
    public UserEntities getUserEntities(String wildcard) {
        HashMap<String, String> querys = new HashMap<String, String>();
        querys.put("search", wildcard);
        return restApiClient.getUsers(querys);
    }

    // Create a new user (userName, name, email, passowrd). There are more user settings available.
    public Response createNewUser(String userName, String name, String email, String password) {
        userEntity = new UserEntity(userName, name, email, password);
        return restApiClient.createUser(userEntity);
    }

    //Update a user
    public Response updateUser(String userName) {
        userEntity.setName(userName);
        return restApiClient.updateUser(userEntity);
    }

    //Delete a user
    public Response deleteUser(String userName) {
        return restApiClient.deleteUser(userName);
    }

    //Get all user groups from a user
    public UserGroupsEntity getUserGroups(String userName) {
        return restApiClient.getUserGroups(userName);
    }

    //Add user to groups
    public Response createMultiGroups(String userName, ArrayList<String> groupNames) {
        UserGroupsEntity userGroupsEntity = new UserGroupsEntity(groupNames);
        return restApiClient.addUserToGroups(userName, userGroupsEntity);
    }

    //Add user to group
    public Response addSingleUserToGroup(String userName, String groupName) {
        return restApiClient.addUserToGroup(userName, groupName);
    }

    //Delete user from  a group
    public Response deleteUserFromGroup(String userName, String groupName) {
        return restApiClient.deleteUserFromGroup(userName, groupName);
    }

    //Lockout/Ban a user
    public Response lockoutYser(String userName) {
        return restApiClient.lockoutUser(userName);
    }

    //Unlock/Unban a user
    public Response unlockUser(String userName) {
        return restApiClient.unlockUser(userName);
    }
    //endregion

    //region chat room region
    //Request all public chatrooms
    public MUCRoomEntities getChatRooms() {
        return restApiClient.getChatRooms();
    }

    //Search for the chat room with the room name "test". This act like the wildcard search %String%
    public MUCRoomEntities getChatRooms(String wildcard) {
        HashMap<String, String> querys = new HashMap<String, String>();
        querys.put("search", wildcard);
        return restApiClient.getChatRooms(querys);
    }

    // Create a new chat room (chatroom id, chatroom name, description). There are more chatroom settings available.
    public Response createChatRoom(String id, String chatRoomName, String description) {
        chatRoom = new MUCRoomEntity(id, chatRoomName, description);
        return restApiClient.createChatRoom(chatRoom);
    }

    //Update a chat room
    public Response updateChatRoom(String description) {
        chatRoom.setDescription(description);
        return restApiClient.updateChatRoom(chatRoom);
    }

    //Delete a chat room
    public Response deleteChatRoom(String chatRoomName) {
        return restApiClient.deleteChatRoom(chatRoomName);
    }

    //Add user with role "owner"
    public Response addOwnerChatRoom(String chatRoom, String userName) {
        return restApiClient.addOwner(chatRoom, userName);
    }

    //Add user with role "admin" to a chat room
    public Response addAdminChatRoom(String chatRoomName, String userName) {
        return restApiClient.addAdmin(chatRoomName, userName);
    }

    //Add user with role "member" to a chat room
    public Response addMemberChatRoom(String chatRoomName, String userName) {
        return restApiClient.addMember(chatRoomName, userName);
    }

    //Add user with role "outcast" to a chat room
    public Response addOutCastChatRoom(String chatRoomName, String userName) {
        return restApiClient.addOutcast(chatRoomName, userName);
    }

    //Get all particapants from a specified chat room
    public ParticipantEntities getChatRoomParticipants(String chatRoomName) {
        return restApiClient.getChatRoomParticipants(chatRoomName);
    }
    //endregion

    //region session region
    //Retrieve all system properties
    public SystemProperties getSystemProperties() {
        return restApiClient.getSystemProperties();
    }

    //Retrieve specific system property e.g. "xmpp.domain"
    public SystemProperty getSystemProperties(String propertyName) {
        return restApiClient.getSystemProperty(propertyName);
    }

    //Create a system property
    public Response createSystemProperty(String propertyName, String propertyValue) {
        systemProperty = new SystemProperty(propertyName, propertyValue);
        return restApiClient.createSystemProperty(systemProperty);
    }

    // Update a system property
    public Response updateSystemProperty(String propertyName, String propertyValue) {
        systemProperty = new SystemProperty(propertyName, propertyValue);
        return restApiClient.updateSystemProperty(systemProperty);
    }

    // Delete a system property
    public Response deleteSystemProperty(String propertyName) {
        return restApiClient.deleteSystemProperty(propertyName);
    }

    //endregion

    //region group region
    //Retrieve all groups
    public GroupEntities getGroups() {
        return restApiClient.getGroups();
    }

    //Retrieve specific group
    public GroupEntity getGroup(String groupName) {
        return restApiClient.getGroup(groupName);
    }

    //Create a group
    public Response createGroup(String groupName, String description) {
        groupEntity = new GroupEntity(groupName, description);
        return restApiClient.createGroup(groupEntity);
    }

    //Update a group
    public Response updateGroup(String groupName, String description) {
        groupEntity = new GroupEntity(groupName, description);
        return restApiClient.updateGroup(groupEntity);
    }

    //Delete a group
    public Response deleteGroup(String groupName) {
        return restApiClient.deleteGroup(groupName);
    }
    //endregion

    //region roster region
    //Retrieve user roster
    public RosterEntities getRoster(String userName) {
        return restApiClient.getRoster(userName);
    }

    //Create a user roster entry (Possible values for subscriptionType are: -1 (remove), 0 (none), 1 (to), 2 (from), 3 (both))
    public Response createRoster(String jid, String nickName, int subcriptionType, ArrayList<String> groupName, String userName) {
        rosterItemEntity = new RosterItemEntity(jid, nickName, subcriptionType);
        // Groups are optional
        if (groupName != null) {
            rosterItemEntity.setGroups(groupName);
        }
        return restApiClient.addRosterEntry(userName, rosterItemEntity);
    }

    // Update a user roster entry
    public Response updateRoster(String jid, String nickName, int subcriptionType, String userName) {
        RosterItemEntity rosterItemEntity = new RosterItemEntity(jid, nickName, subcriptionType);
        return restApiClient.updateRosterEntry(userName, rosterItemEntity);
    }

    //Delete a user roster entry
    public Response deleteRoster(String userName, String nickName) {
        return restApiClient.deleteRosterEntry(userName, nickName);
    }
    //endregion*/

}
