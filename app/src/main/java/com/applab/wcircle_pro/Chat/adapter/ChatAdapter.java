package com.applab.wcircle_pro.Chat.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.applab.wcircle_pro.Chat.ChatImageSlidingActivity;
import com.applab.wcircle_pro.Chat.viewholder.ChatViewHolder;
import com.applab.wcircle_pro.Chat.model.User;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class ChatAdapter extends RecyclerView.Adapter<ChatViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;
    private String TAG = "CHAT";

    public ChatAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("ChatViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_chat_row, parent, false);
        ChatViewHolder holder = new ChatViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        User current = User.getUser(cursor, position);
        if (Integer.valueOf(current.getUnread()) > 0) {
            holder.txtNo.setVisibility(View.VISIBLE);
            if (Integer.valueOf(current.getUnread()) > 9) {
                holder.txtNo.setText("+9");
            } else {
                holder.txtNo.setText(current.getUnread());
            }
        } else {
            holder.txtNo.setVisibility(View.GONE);
        }
        if (current.getGroupId() == null) {
            if (current.getEmployeeName() != null) {
                if (!current.getEmployeeName().equals("")) {
                    holder.txtName.setText(current.getEmployeeName());
                } else {
                    holder.txtName.setText(current.getJid());
                }
            } else {
                holder.txtName.setText(current.getJid());
            }
            Glide.with(context)
                    .load(current.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transform(new CircleTransform(context))
                    .placeholder(R.mipmap.ic_action_person_light)
                    .into(holder.img);
            holder.imgCountry.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(current.getCountryImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transform(new CircleTransform(context))
                    .into(holder.imgCountry);
        } else {
            holder.txtName.setText(current.getGroupName());
            holder.imgCountry.setVisibility(View.GONE);
            Glide.with(context)
                    .load(current.getGroupImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transform(new CircleTransform(context))
                    .placeholder(R.mipmap.ic_action_group_light)
                    .into(holder.img);
        }
        String msg = current.getLast_msg();
        if (current.getLast_msg().length() > 15) {
            msg = current.getLast_msg().substring(0, 14) + "...";
        }
        holder.txtDesc.setText(msg);
        holder.txtDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd/MM/yy  h:mm a", current.getLast_seen()));
        holder.rlImage.setTag(current);
        setRlImageClickListener(holder.rlImage);
    }

    private void setRlImageClickListener(final RelativeLayout rlImage) {
        rlImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = (User) rlImage.getTag();
                if (user.getGroupId() == null) {
                    Intent intent = new Intent(context, ChatImageSlidingActivity.class);
                    intent.putExtra("id", String.valueOf(user.getId()));
                    intent.putExtra("position", 0);
                    String url = user.getImage();
                    if (url == null) {
                        url = "";
                    }
                    intent.putExtra("urlImage", url);
                    intent.putExtra("isGallery", false);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, ChatImageSlidingActivity.class);
                    intent.putExtra("id", String.valueOf(user.getId()));
                    intent.putExtra("position", 0);
                    String url = user.getGroupImage();
                    if (url == null) {
                        url = "";
                    }
                    intent.putExtra("urlImage", url);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            ChatAdapter.this.notifyDataSetChanged();
        }
    };
}
