package com.applab.wcircle_pro.Chat;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.db.ChatListProvider;
import com.applab.wcircle_pro.Chat.db.ChatProvider;
import com.applab.wcircle_pro.Chat.db.GroupChatProvider;
import com.applab.wcircle_pro.Chat.db.GroupProvider;
import com.applab.wcircle_pro.Chat.db.HttpHelper;
import com.applab.wcircle_pro.Chat.fragment.ChartRoomGroupFragment;
import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.Chat.xmpp.XMPPConn;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import java.io.File;

public class ChartRoomGroupActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar toolbar;
    private TextView txtToolbarTitle;
    private String groupId, groupName;
    private Menu menu;
    private RelativeLayout container;
    private static final int SELECT_PHOTO_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 101;
    private Uri fileUri;
    private File file;
    private String TAG = "CHART ROOM GROUP";
    private boolean isOwner = true;
    private boolean isDelete = false;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart_room_group);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green_light));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        Intent intent = getIntent();
        groupId = intent.getStringExtra("groupId");
        groupName = intent.getStringExtra("groupName");
        page = intent.getIntExtra("page", 1);
        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setGravity(Gravity.LEFT);
        txtToolbarTitle.setText(groupName);
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        container = (RelativeLayout) findViewById(R.id.container);
        getSupportLoaderManager().initLoader(5678, null, this);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = ChartRoomGroupFragment.newInstance(file);
        ft.replace(container.getId(), fragment);
        ft.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case SELECT_PHOTO_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    fileUri = imageReturnedIntent.getData();
                    File direct = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.folder_name) + "/" +
                            getResources().getString(R.string.messenger));
                    file = GroupActivity.previewSelectedImage(ChartRoomGroupActivity.this, imageReturnedIntent.getData(), null, false, false, 0, false, direct);
                    if (file != null) {
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        Fragment fragment = ChartRoomGroupFragment.newInstance(file);
                        ft.replace(container.getId(), fragment);
                        ft.commit();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    break;
                } else {
                    Utils.showError(ChartRoomGroupActivity.this, Utils.CODE_PROCEED_FAILED, Utils.PROCEED_FAILED);
                }
                break;
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    File direct = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.folder_name) + "/" +
                            getResources().getString(R.string.messenger));
                    file = GroupActivity.previewCapturedImage(ChartRoomGroupActivity.this, fileUri, null, false, false, 0, false, direct);
                    if (file != null) {
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        Fragment fragment = ChartRoomGroupFragment.newInstance(file);
                        ft.replace(container.getId(), fragment);
                        ft.commit();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    break;
                } else {
                    Utils.showError(ChartRoomGroupActivity.this, Utils.CODE_PROCEED_FAILED, Utils.PROCEED_FAILED);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("page", page);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chart_room_group, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            Intent intent = new Intent(this, ChatActivity.class);
            intent.putExtra("page", page);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.action_attach) {
            View v = findViewById(R.id.action_attach);
            PopupMenu popupMenu = new PopupMenu(toolbar.getContext(), v) {
                @Override
                public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_camera:
                            cameraClicked();
                            return true;
                        case R.id.action_gallery:
                            imageClicked();
                            return true;
                        default:
                            return super.onMenuItemSelected(menu, item);
                    }
                }
            };
            popupMenu.inflate(R.menu.menu_custom);
            popupMenu.getMenu().removeItem(R.id.action_folder);
            popupMenu.getMenu().removeItem(R.id.action_upload);
            popupMenu.getMenu().removeItem(R.id.action_new_chat);
            popupMenu.getMenu().removeItem(R.id.action_create_group);
            popupMenu.getMenu().removeItem(R.id.action_clear_employee);
            popupMenu.getMenu().removeItem(R.id.action_close_group);
            popupMenu.getMenu().removeItem(R.id.action_group_info);
            popupMenu.getMenu().removeItem(R.id.action_add_people);
            popupMenu.getMenu().removeItem(R.id.action_exit_group);
            popupMenu.getMenu().removeItem(R.id.action_delete_group);
            popupMenu.show();
            return true;
        } else if (id == R.id.action_menu) {
            View menuItemView = findViewById(R.id.action_menu);
            PopupMenu popupMenu = new PopupMenu(toolbar.getContext(), menuItemView) {
                @Override
                public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_add_people:
                            if (Utils.isConnectingToInternet(ChartRoomGroupActivity.this)) {
                                if (XMPPConn.getInstance().getConn() != null) {
                                    if (XMPPConn.getInstance().getConn().isConnected()) {
                                        LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(ChartRoomGroupActivity.this);
                                        Intent intent = new Intent(BackgroundXMPPService.ACTION);
                                        intent.putExtra("isAddPeople", true);
                                        mgr.sendBroadcast(intent);
                                    }
                                }
                            } else {
                                Utils.showNoConnection(ChartRoomGroupActivity.this);
                            }
                            return true;
                        case R.id.action_group_info:
                            Intent intent = new Intent(ChartRoomGroupActivity.this, GroupInfoActivity.class);
                            intent.putExtra("groupId", groupId);
                            intent.putExtra("groupName", groupName);
                            startActivity(intent);
                            finish();
                            return true;
                        case R.id.action_close_group:
                            if (Utils.isConnectingToInternet(ChartRoomGroupActivity.this)) {
                                HttpHelper.DeleteGroup(ChartRoomGroupActivity.this, TAG, groupId);
                                ContentValues contentValues = new ContentValues();
                                contentValues.put(DBHelper.GROUP_COLUMN_IS_DELETE, String.valueOf(true));
                                getContentResolver().update(GroupProvider.CONTENT_URI, contentValues, DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{groupId});
                                Intent mIntent = new Intent(ChartRoomGroupActivity.this, ChatActivity.class);
                                mIntent.putExtra("page", page);
                                startActivity(mIntent);
                                finish();
                            } else {
                                Utils.showNoConnection(ChartRoomGroupActivity.this);
                            }
                            return true;
                        case R.id.action_exit_group:
                            if (Utils.isConnectingToInternet(ChartRoomGroupActivity.this)) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put(DBHelper.GROUP_COLUMN_IS_DELETE, String.valueOf(true));
                                getContentResolver().update(GroupProvider.CONTENT_URI,
                                        contentValues, DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{groupId});
                                if (ChartRoomGroupActivity.this.menu != null) {
                                    MenuItem item1 = ChartRoomGroupActivity.this.menu.findItem(R.id.action_menu);
                                    MenuItem item2 = ChartRoomGroupActivity.this.menu.findItem(R.id.action_attach);
                                    item1.setVisible(false);
                                    item2.setVisible(false);
                                }
                                HttpHelper.ExitGroup(ChartRoomGroupActivity.this, TAG, groupId);
                                Intent mIntent = new Intent(ChartRoomGroupActivity.this, ChatActivity.class);
                                mIntent.putExtra("page", page);
                                startActivity(mIntent);
                                finish();
                            } else {
                                Utils.showNoConnection(ChartRoomGroupActivity.this);
                            }
                            return true;
                        case R.id.action_delete_group:
                            getContentResolver().delete(GroupProvider.CONTENT_URI, DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{groupId});
                            getContentResolver().delete(GroupChatProvider.CONTENT_URI, DBHelper.GROUP_CHAT_COLUMN_GROUP_ID + "=?", new String[]{groupId});
                            getContentResolver().delete(ChatProvider.CONTENT_URI, DBHelper.CHAT_COLUMN_GROUP_ID + "=?", new String[]{groupId});
                            getContentResolver().delete(ChatListProvider.CONTENT_URI, DBHelper.CHAT_LIST_COLUMN_GROUP_ID + "=?", new String[]{groupId});
                            Intent mIntent = new Intent(ChartRoomGroupActivity.this, ChatActivity.class);
                            mIntent.putExtra("page", page);
                            startActivity(mIntent);
                            finish();
                            return true;
                        default:
                            return super.onMenuItemSelected(menu, item);
                    }
                }
            };
            popupMenu.inflate(R.menu.menu_custom);
            popupMenu.getMenu().removeItem(R.id.action_folder);
            popupMenu.getMenu().removeItem(R.id.action_upload);
            popupMenu.getMenu().removeItem(R.id.action_new_chat);
            popupMenu.getMenu().removeItem(R.id.action_create_group);
            popupMenu.getMenu().removeItem(R.id.action_clear_employee);
            popupMenu.getMenu().removeItem(R.id.action_camera);
            popupMenu.getMenu().removeItem(R.id.action_gallery);
            if (!isOwner) {
                popupMenu.getMenu().removeItem(R.id.action_add_people);
                popupMenu.getMenu().removeItem(R.id.action_group_info);
                popupMenu.getMenu().removeItem(R.id.action_close_group);
                if (isDelete) {
                    popupMenu.getMenu().removeItem(R.id.action_exit_group);
                } else {
                    popupMenu.getMenu().removeItem(R.id.action_delete_group);
                }
            } else {
                if (isDelete) {
                    popupMenu.getMenu().removeItem(R.id.action_add_people);
                    popupMenu.getMenu().removeItem(R.id.action_group_info);
                    popupMenu.getMenu().removeItem(R.id.action_close_group);
                    popupMenu.getMenu().removeItem(R.id.action_exit_group);
                } else {
                    popupMenu.getMenu().removeItem(R.id.action_exit_group);
                    popupMenu.getMenu().removeItem(R.id.action_delete_group);
                }
            }
            popupMenu.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void imageClicked() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO_REQUEST_CODE);
    }

    private void cameraClicked() {
        if (Utils.isDeviceSupportCamera(ChartRoomGroupActivity.this)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.folder_name) + "/" +
                    getResources().getString(R.string.messenger));
            fileUri = GroupActivity.getOutputMediaFileUri(ChartRoomGroupActivity.this, false, file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        } else {
            Utils.showError(ChartRoomGroupActivity.this, "", "Sorry, you don't have supported camera.");
        }
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BackgroundXMPPService.ACTION)) {
                if (intent.getBooleanExtra("error", false)) {
                    startActivity(new Intent(ChartRoomGroupActivity.this, ChatActivity.class));
                    finish();
                }
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        IntentFilter iff = new IntentFilter(BackgroundXMPPService.ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 5678) {
            return new CursorLoader(this, GroupProvider.CONTENT_URI, null, DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{groupId}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 5678) {
            if (data != null) {
                if (data.getCount() > 0) {
                    isDelete = false;
                    data.moveToFirst();
                    txtToolbarTitle.setText(data.getString(data.getColumnIndex(DBHelper.GROUP_COLUMN_NAME)));
                    if (!data.isNull(data.getColumnIndex(DBHelper.GROUP_COLUMN_CREATE_BY))) {
                        if (!data.getString(data.getColumnIndex(DBHelper.GROUP_COLUMN_CREATE_BY)).equals(Utils.getProfile(ChartRoomGroupActivity.this).getOXUser())) {
                            isOwner = false;
                        }
                    }
                    if (!data.isNull(data.getColumnIndex(DBHelper.GROUP_COLUMN_IS_DELETE))) {
                        isDelete = Boolean.valueOf(data.getString(data.getColumnIndex(DBHelper.GROUP_COLUMN_IS_DELETE)));
                        if (isDelete) {
                            if (menu != null) {
                                MenuItem item1 = menu.findItem(R.id.action_attach);
                                item1.setVisible(false);
                            }
                        } else {
                            if (menu != null) {
                                MenuItem item1 = menu.findItem(R.id.action_attach);
                                item1.setVisible(true);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
