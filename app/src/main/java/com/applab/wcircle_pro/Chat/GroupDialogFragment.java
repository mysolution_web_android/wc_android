package com.applab.wcircle_pro.Chat;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.R;

/**
 * Created by user on 31/10/2015.
 */
public class GroupDialogFragment extends DialogFragment {
    private ImageView mBtnCancel;
    private LinearLayout mBtnEdit, mBtnCamera;
    private String TAG = "EMPLOYEE_CALL";
    private static AlertDialog.Builder builder;
    private static Context mContext;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static GroupDialogFragment newInstance(Context context) {
        GroupDialogFragment frag = new GroupDialogFragment();
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_upload_image, null);

        mBtnEdit = (LinearLayout) v.findViewById(R.id.btnEdit);
        mBtnCamera = (LinearLayout) v.findViewById(R.id.btnCamera);

        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        mBtnEdit.setOnClickListener(btnEditOnClickListener);
        mBtnCamera.setOnClickListener(btnCameraOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnCameraOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mContext);
            Intent mIntent = new Intent(BackgroundXMPPService.ACTION);
            mIntent.putExtra("isCamera", true);
            localBroadcastManager.sendBroadcast(mIntent);
            GroupDialogFragment.this.getDialog().cancel();
        }
    };


    private View.OnClickListener btnEditOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mContext);
            Intent mIntent = new Intent(BackgroundXMPPService.ACTION);
            mIntent.putExtra("isPhoto", true);
            localBroadcastManager.sendBroadcast(mIntent);
            GroupDialogFragment.this.getDialog().cancel();
        }
    };
    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            GroupDialogFragment.this.getDialog().cancel();
        }
    };
}

