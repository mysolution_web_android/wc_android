package com.applab.wcircle_pro.Chat.xmpp;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Chat.db.ChatListProvider;
import com.applab.wcircle_pro.Chat.db.ChatProvider;
import com.applab.wcircle_pro.Chat.model.MessageModel;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 2/10/2015.
 */

public class ChatUploadService extends IntentService implements MultiPartChat.MultipartProgressListener {
    private String mFilePath;
    private String mThreadId;
    private String mOxUser;
    private String TAG = "EDIT PROFILE";
    private File mFile;
    public static LocalBroadcastManager mgr;

    public ChatUploadService() {
        super("ChatUploadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle bundle = intent.getExtras();
        mgr = LocalBroadcastManager.getInstance(this);
        mFilePath = bundle.getString("filePath");
        mThreadId = bundle.getString("threadId");
        mOxUser = bundle.getString("oxUser");
        if (mFilePath != null) {
            mFile = new File(mFilePath);
        }
        if (mFile != null) {
            if (Utils.getUsableSpace() < new File(mFilePath).length()) {
                Utils.showError(getBaseContext(), Utils.CODE_LOCAL_SIZE_EXCEEDED, Utils.LOCAL_SIZE_EXCEED);
            } else {
                postChatImage(mFile, mOxUser);
                insertChatImage(mThreadId);
            }
        } else {
            postChatImage(null, mOxUser);
        }
    }

    public void insertChatImage(String threadId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.CHAT_COLUMN_IS_SUCCESS, "pending");
        contentValues.put(DBHelper.CHAT_COLUMN_IS_IMAGE, "true");
        getContentResolver().update(ChatProvider.CONTENT_URI, contentValues, DBHelper.CHAT_COLUMN_THREAD_ID + "=?", new String[]{threadId});

        contentValues = new ContentValues();
        contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_SUCCESS, "pending");
        contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_IMAGE, "true");
        getContentResolver().update(ChatListProvider.CONTENT_URI, contentValues, DBHelper.CHAT_LIST_COLUMN_LAST_THREAD_ID + "=?", new String[]{threadId});

        Intent intent = new Intent(BackgroundXMPPService.ACTION);
        intent.putExtra("refresh", true);
        mgr.sendBroadcast(intent);
    }

    @Override
    public void transferred(long transferred, long progress) {
        final NotificationManager mNotifyManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(mFile.getName())
                .setContentText("Upload in progress")
                .setSmallIcon(R.mipmap.icon);

        // Start a lengthy operation in a background thread
        mBuilder.setProgress(100, (int) progress, false);
        // Displays the progress bar for the first time.
        //mNotifyManager.notify(1511, mBuilder.build());
        // When the loop is finished, updates the notification
        if (100 == progress) {
            mBuilder.setContentText("Upload complete")
                    // Removes the progress bar
                    .setProgress(0, 0, false);
            //mNotifyManager.notify(1511, mBuilder.build());
        }
    }

    public void postChatImage(File file, String oxUser) {
        final String token = Utils.getToken(getBaseContext());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        MultiPartChat<String> mGsonRequest = new MultiPartChat<String>(
                Request.Method.POST,
                Utils.API_URL + "api/Messenger/UploadFile",
                String.class,
                headers,
                responseListener(),
                errorListener(),
                file,
                ChatUploadService.this, oxUser) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }


    private Response.Listener<String> responseListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MessageModel messageModel = getMessageModel(mThreadId);
                ArrayList<String> arrayList = BackgroundXMPPService.sendMessage(getBaseContext(),
                        response, messageModel.getTo(), messageModel.getIsGroup() ? 1 : 0, messageModel.getIsImage() ? 1 : 0,
                        messageModel.getGroupName(), messageModel.getGroupId(), TAG, null, null, 0, false);

                if (arrayList != null) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_SUCCESS, "success");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST_THREAD_ID, arrayList.get(0));
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST, arrayList.get(1));
                    getContentResolver().update(ChatListProvider.CONTENT_URI, contentValues,
                            DBHelper.CHAT_LIST_COLUMN_LAST_THREAD_ID + "=?", new String[]{mThreadId});

                    contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_COLUMN_MSG, response);
                    contentValues.put(DBHelper.CHAT_COLUMN_IS_SUCCESS, "success");
                    contentValues.put(DBHelper.CHAT_COLUMN_TIME, arrayList.get(1));
                    contentValues.put(DBHelper.CHAT_COLUMN_THREAD_ID, arrayList.get(0));
                    getContentResolver().update(ChatProvider.CONTENT_URI, contentValues, DBHelper.CHAT_COLUMN_THREAD_ID + "=?", new String[]{mThreadId});
                } else {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_SUCCESS, "fail");
                    getContentResolver().update(ChatListProvider.CONTENT_URI, contentValues, DBHelper.CHAT_LIST_COLUMN_LAST_THREAD_ID + "=?", new String[]{mThreadId});

                    contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_COLUMN_IS_SUCCESS, "fail");
                    getContentResolver().update(ChatProvider.CONTENT_URI, contentValues, DBHelper.CHAT_COLUMN_THREAD_ID + "=?", new String[]{mThreadId});
                }

                Intent intent = new Intent(BackgroundXMPPService.ACTION);
                intent.putExtra("refresh", true);
                mgr.sendBroadcast(intent);
            }
        };
    }

    private MessageModel getMessageModel(String threadId) {
        Cursor cursor = null;
        MessageModel messageModel = null;
        try {
            cursor = getContentResolver().query(ChatProvider.CONTENT_URI, null, DBHelper.CHAT_COLUMN_THREAD_ID + "=?",
                    new String[]{threadId}, null);
            if (cursor != null && cursor.moveToFirst()) {
                messageModel = new MessageModel();
                messageModel = MessageModel.getForJson(getBaseContext(), cursor, 0);
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return messageModel;
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(getBaseContext(), error);

                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_SUCCESS, "fail");
                getContentResolver().update(ChatListProvider.CONTENT_URI, contentValues,
                        DBHelper.CHAT_LIST_COLUMN_LAST_THREAD_ID + "=?", new String[]{mThreadId});

                contentValues = new ContentValues();
                contentValues.put(DBHelper.CHAT_COLUMN_IS_SUCCESS, "fail");
                getContentResolver().update(ChatProvider.CONTENT_URI, contentValues,
                        DBHelper.CHAT_COLUMN_THREAD_ID + "=?", new String[]{mThreadId});

                Intent intent = new Intent(BackgroundXMPPService.ACTION);
                intent.putExtra("refresh", true);
                mgr.sendBroadcast(intent);
            }
        };
    }
}
