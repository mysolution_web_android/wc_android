package com.applab.wcircle_pro.Chat.xmpp;

/**
 * Created by haoyi on 10/9/15.
 */
public class Config {
    /*public static final String HOST = "habu.senselink.net";*/
    public static final String HOST = "www.workcircle.com.my";
    /*public static final String HOST = "192.168.0.106";*/
    /*public static final String HOST = "app.mintranet.com";*/
    public static final int PORT = 5222;
    /*public static final String SERVICE = "http://app.mintranet.com/";*/
    /*public static final String SERVICE = "habu.senselink.net";*/
    public static final String SERVICE = "www.workcircle.com.my";
    /*public static final String SERVICE = "desktop-0lou2f3";*/
    public static final String RESOURCE = "spark";

    /*public static final String SERVER_IP = "121.121.40.25";*/
    /*public static final String SERVER_IP = "175.139.183.204";*/
    public static final String SERVER_IP = "203.223.143.30";
    /*public static final String SERVER_IP = "desktop-0lou2f3";*/

    public static final String SHARED_PREFERENCES_NAME = "Messanger";
    public static final String SHARED_PREFERENCES_GROUP = "Messanger_GroupChat_Room";
}
