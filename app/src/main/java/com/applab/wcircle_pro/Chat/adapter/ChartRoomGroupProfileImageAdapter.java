package com.applab.wcircle_pro.Chat.adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.wcircle_pro.Chat.model.User;
import com.applab.wcircle_pro.Chat.viewholder.ChartRoomGroupProfileImageViewHolder;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by user on 5/11/2015.
 */
public class ChartRoomGroupProfileImageAdapter extends RecyclerView.Adapter<ChartRoomGroupProfileImageViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;

    public ChartRoomGroupProfileImageAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public ChartRoomGroupProfileImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_horizontal_chart_group, parent, false);
        ChartRoomGroupProfileImageViewHolder holder =
                new ChartRoomGroupProfileImageViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ChartRoomGroupProfileImageViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        User current = User.getGroupUser(cursor, position);
        Glide.with(context)
                .load(current.getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleTransform(context))
                .placeholder(R.mipmap.ic_action_person_light)
                .into(holder.imgProfile);
        Glide.with(context)
                .load(current.getCountryImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleTransform(context))
                .into(holder.imgCountry);
        holder.txtId.setTag(current);
    }

    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        this.cursor = cursor;
        android.os.Message msg = handler.obtainMessage();
        handler.handleMessage(msg);
        return cursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            ChartRoomGroupProfileImageAdapter.this.notifyDataSetChanged();
        }
    };
}
