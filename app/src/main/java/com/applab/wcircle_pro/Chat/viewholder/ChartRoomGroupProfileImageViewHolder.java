package com.applab.wcircle_pro.Chat.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 11/8/2015.
 */
public class ChartRoomGroupProfileImageViewHolder extends RecyclerView.ViewHolder{
    public ImageView imgProfile;
    public ImageView imgCountry;
    public TextView txtId;

    public ChartRoomGroupProfileImageViewHolder(View itemView) {
        super(itemView);
        imgProfile = (ImageView)itemView.findViewById(R.id.imgProfile);
        imgCountry = (ImageView)itemView.findViewById(R.id.imgCountry);
        txtId = (TextView)itemView.findViewById(R.id.txtId);
    }
}
