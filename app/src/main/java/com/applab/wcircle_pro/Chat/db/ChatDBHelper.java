package com.applab.wcircle_pro.Chat.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;

import com.applab.wcircle_pro.Chat.model.MessageModel;
import com.applab.wcircle_pro.Chat.model.User;
import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.Dashboard.DashboardProvider;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Created by haoyi on 10/9/15.
 */
public class ChatDBHelper {
    private SQLiteDatabase db;
    private Context context;

    public ChatDBHelper(Context context) {
        this.context = context;
        this.db = new DBHelper(this.context).getWritableDatabase();
    }

    public static void addMessage(MessageModel msg, Context context) {
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.CHAT_COLUMN_THREAD_ID, msg.getThreadId());
        cv.put(DBHelper.CHAT_COLUMN_FROM, msg.getFrom());
        cv.put(DBHelper.CHAT_COLUMN_TO, msg.getTo());
        cv.put(DBHelper.CHAT_COLUMN_TIME, msg.getTime());
        cv.put(DBHelper.CHAT_COLUMN_MSG, msg.getMessage());
        cv.put(DBHelper.CHAT_COLUMN_EXTRA, msg.getExtra());
        cv.put(DBHelper.CHAT_COLUMN_IS_IMAGE, msg.getIsImage() ? "true" : "false");
        cv.put(DBHelper.CHAT_COLUMN_IS_GROUP, msg.getIsGroup() ? "true" : "false");
        cv.put(DBHelper.CHAT_COLUMN_IS_DELETE, msg.getIsDelete() ? "true" : "false");
        cv.put(DBHelper.CHAT_COLUMN_IS_GROUP_UPDATE, msg.getIsGroupUpdate() ? "true" : "false");
        cv.put(DBHelper.CHAT_COLUMN_GROUP_ID, msg.getGroupId().equals("") ? null : msg.getGroupId());
        cv.put(DBHelper.CHAT_COLUMN_GROUP_NAME, msg.getGroupName().equals("") ? null : msg.getGroupName());
        context.getContentResolver().insert(ChatProvider.CONTENT_URI, cv);
        if (msg.getIsDelete()) {
            HttpHelper.GetSingleGroup(context, "IS DELETE", msg.getGroupId(), true);
        } else if (msg.getIsGroupUpdate()) {
            HttpHelper.GetSingleGroup(context, "IS GROUP UPDATED", msg.getGroupId(), false);
        } else if (!msg.getIsDelete()) {
            if (msg.getIsGroup()) {
                if (!isGroupMemberExists(context, msg.getGroupId(), Utils.getProfile(context).getOXUser())) {
                    HttpHelper.GetSingleGroup(context, "IS MEMBER NOT EXISTS", msg.getGroupId(), false);
                } else if (!HttpHelper.isGroupExist(context, msg.getGroupId())) {
                    HttpHelper.GetSingleGroup(context, "IS GROUP NOT EXISTS", msg.getGroupId(), false);
                } else if (isGroupClose(context, msg.getGroupId())) {
                    HttpHelper.GetSingleGroup(context, "IS GROUP CLOSE", msg.getGroupId(), false);
                }
            }
        }
    }

    public static boolean isGroupClose(Context context, Object id) {
        boolean isClose = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(GroupProvider.CONTENT_URI, null,
                    DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isClose = Boolean.valueOf(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_DELETE)));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isClose;

    }

    public static Date getLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(UserChatProvider.CONTENT_URI, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static boolean isGroupMemberExists(Context context, Object groupid, Object employeeId) {
        boolean isExits = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(GroupChatProvider.CONTENT_URI, null,
                    DBHelper.GROUP_CHAT_COLUMN_GROUP_ID + "=? AND "
                            + DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID + "=?"
                    , new String[]{String.valueOf(groupid), String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExits = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExits;
    }

    public static boolean isGroupMemberOxUserExists(Context context, Object groupid, Object oxUser) {
        boolean isExits = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(GroupChatProvider.CONTENT_URI, null,
                    DBHelper.GROUP_CHAT_COLUMN_GROUP_ID + "=? AND "
                            + DBHelper.GROUP_CHAT_COLUMN_OX_USER + "=?"
                    , new String[]{String.valueOf(groupid), String.valueOf(oxUser)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExits = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExits;
    }

    public static int getUnread(Context context, Object id, Object groupId) {
        int total = 0;
        Cursor cursor = null;
        String selection;
        String[] args;
        if (groupId == null) {
            selection = DBHelper.CHAT_LIST_COLUMN_JID + "=? AND " + DBHelper.CHAT_LIST_COLUMN_GROUP_ID + " IS NULL ";
            args = new String[]{String.valueOf(id)};
        } else {
            selection = DBHelper.CHAT_LIST_COLUMN_GROUP_ID + "=?";
            args = new String[]{String.valueOf(groupId)};
        }
        try {
            cursor = context.getContentResolver().query(ChatListProvider.CONTENT_URI, null, selection, args, null);
            if (cursor != null && cursor.moveToFirst()) {
                total = cursor.getInt(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_UNREAD));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return total;
    }

    public static int getUnread(Context context) {
        int total = 0;
        Cursor cursor = null;
        String[] projection = {"SUM(" + DBHelper.CHAT_LIST_COLUMN_UNREAD + ") AS " + DBHelper.CHAT_LIST_COLUMN_UNREAD};
        try {
            cursor = context.getContentResolver().query(ChatListProvider.CONTENT_URI, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    total += cursor.getInt(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_UNREAD));
                    cursor.moveToNext();
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return total;
    }

    public static Integer addLastMsg(MessageModel msg, Context context) {
        Integer id = 7894;
        Uri uri = null;
        try {
            if (msg.isMine()) {
                Cursor c = null;
                if (msg.getIsGroup()) {
                    c = context.getContentResolver().query(ChatListProvider.CONTENT_URI, null, DBHelper.CHAT_LIST_COLUMN_GROUP_ID +
                            "=?", new String[]{msg.getGroupId()}, null);
                } else {
                    c = context.getContentResolver().query(ChatListProvider.CONTENT_URI, null, DBHelper.CHAT_LIST_COLUMN_JID +
                            "=? AND " + DBHelper.CHAT_LIST_COLUMN_IS_GROUP + "=?", new String[]{msg.getTo(), "false"}, null);
                }
                if (c.getCount() > 0) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST_THREAD_ID, msg.getThreadId());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST_MSG, msg.getIsImage() ? "[Image]" : msg.getMessage());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_GROUP_ID, msg.getGroupId().equals("") ? null : msg.getGroupId());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_GROUP_NAME, msg.getGroupName().equals("") ? null : msg.getGroupName());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST, msg.getTime());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_IMAGE, msg.getIsImage() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_GROUP, msg.getIsGroup() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_DELETE, msg.getIsDelete() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_GROUP_UPDATE, msg.getIsGroupUpdate() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_UNREAD, getUnread(context, msg.getFrom(),
                            msg.getGroupId().equals("") ? null : msg.getGroupId()));
                    if (!msg.getIsGroup()) {
                        context.getContentResolver().update(ChatListProvider.CONTENT_URI, contentValues,
                                DBHelper.CHAT_LIST_COLUMN_JID + "=? AND " + DBHelper.CHAT_LIST_COLUMN_IS_GROUP + "=?", new String[]{msg.getTo(), "false"});
                    } else {
                        context.getContentResolver().update(ChatListProvider.CONTENT_URI, contentValues, DBHelper.CHAT_LIST_COLUMN_GROUP_ID + "=?", new String[]{msg.getGroupId()});
                    }

                    c.moveToPosition(c.getCount() - 1);
                    id = c.getInt(c.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_USER_ID));
                } else {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST_THREAD_ID, msg.getThreadId());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST_MSG, msg.getIsImage() ? "[Image]" : msg.getMessage());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_GROUP_ID, msg.getGroupId().equals("") ? null : msg.getGroupId());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_GROUP_NAME, msg.getGroupName().equals("") ? null : msg.getGroupName());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST, msg.getTime());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_IMAGE, msg.getIsImage() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_GROUP, msg.getIsGroup() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_DELETE, msg.getIsDelete() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_GROUP_UPDATE, msg.getIsGroupUpdate() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_UNREAD, "0");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_NAME, msg.getGroupId().equals("") ? msg.getTo() : msg.getGroupId());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_EXTRA1, msg.getType());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_JID, msg.getTo());
                    uri = context.getContentResolver().insert(ChatListProvider.CONTENT_URI, contentValues);
                }
            } else {
                Cursor c = null;
                if (msg.getIsGroup()) {
                    c = context.getContentResolver().query(ChatListProvider.CONTENT_URI, null, DBHelper.CHAT_LIST_COLUMN_GROUP_ID + "=?", new String[]{msg.getGroupId()}, null);
                } else {
                    c = context.getContentResolver().query(ChatListProvider.CONTENT_URI, null, DBHelper.CHAT_LIST_COLUMN_JID + "=? AND " + DBHelper.CHAT_LIST_COLUMN_IS_GROUP + "=?", new String[]{msg.getFrom(), "false"}, null);
                }
                if (c.getCount() > 0) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST_THREAD_ID, msg.getThreadId());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST_MSG, msg.getIsImage() ? "[Image]" : msg.getMessage());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_GROUP_ID, msg.getGroupId().equals("") ? null : msg.getGroupId());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_GROUP_NAME, msg.getGroupName().equals("") ? null : msg.getGroupName());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_IMAGE, msg.getIsImage() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_GROUP, msg.getIsGroup() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_DELETE, msg.getIsDelete() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_GROUP_UPDATE, msg.getIsGroupUpdate() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST, msg.getTime());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_AVATAR, "empty");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_NAME, msg.getGroupId().equals("") ? msg.getFrom() : msg.getGroupId());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_UNREAD, getUnread(context, msg.getFrom(), msg.getGroupId().equals("") ? null : msg.getGroupId()) + 1);
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_JID, msg.getFrom());
                    if (!msg.getIsGroup()) {
                        context.getContentResolver().update(ChatListProvider.CONTENT_URI, contentValues, DBHelper.CHAT_LIST_COLUMN_JID + "=? AND " + DBHelper.CHAT_LIST_COLUMN_IS_GROUP + "=?", new String[]{msg.getFrom(), "false"});
                    } else {
                        context.getContentResolver().update(ChatListProvider.CONTENT_URI, contentValues, DBHelper.CHAT_LIST_COLUMN_GROUP_ID + "=?", new String[]{msg.getGroupId()});
                    }
                    c.moveToPosition(c.getCount() - 1);
                    id = c.getInt(c.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_USER_ID));
                } else {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST_THREAD_ID, msg.getThreadId());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST_MSG, msg.getIsImage() ? "[Image]" : msg.getMessage());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_IMAGE, msg.getIsImage() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_GROUP, msg.getIsGroup() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_DELETE, msg.getIsDelete() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_IS_GROUP_UPDATE, msg.getIsGroupUpdate() ? "true" : "false");
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_GROUP_ID, msg.getGroupId().equals("") ? null : msg.getGroupId());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_GROUP_NAME, msg.getGroupName().equals("") ? null : msg.getGroupName());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_LAST, msg.getTime());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_UNREAD, getUnread(context, msg.getFrom(), msg.getGroupId().equals("") ? null : msg.getGroupId()) + 1);
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_NAME, msg.getGroupId().equals("") ? msg.getFrom() : msg.getGroupId());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_EXTRA1, msg.getType());
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_JID, msg.getFrom());
                    uri = context.getContentResolver().insert(ChatListProvider.CONTENT_URI, contentValues);
                }
            }
            if (uri != null) {
                id = Integer.valueOf(uri.getLastPathSegment());
            }
            insertMessenger(context);
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(context);
            Intent intent = new Intent(BackgroundXMPPService.ACTION);
            intent.putExtra("refresh", true);
            mgr.sendBroadcast(intent);
        }
        return id;
    }

    public static void insertMessenger(Context context) {
        int unread = getUnread(context);
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, unread);
        context.getContentResolver().update(DashboardProvider.CONTENT_URI, contentValues,
                DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Messenger"});
    }

    public static String getLastUserId(Context context) {
        String userId = null;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(
                    ChatListProvider.CONTENT_URI, null, null, null,
                    DBHelper.CHAT_LIST_COLUMN_USER_ID + " DESC LIMIT 1 ");
            if (cursor != null && cursor.moveToFirst()) {
                userId = cursor.getString(cursor.getColumnIndex(
                        DBHelper.CHAT_LIST_COLUMN_USER_ID));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return userId;
    }

    public static String getEmployeeName(Context context, String oxUser) {
        String employeeName = null;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(UserChatProvider.CONTENT_URI, null,
                    DBHelper.USER_CHAT_COLUMN_OX_USER + "=?", new String[]{oxUser}, null);
            if (cursor != null && cursor.moveToFirst()) {
                employeeName = cursor.getString(cursor.getColumnIndex(
                        DBHelper.USER_CHAT_COLUMN_NAME));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return employeeName;
    }

    public static boolean isUserExist(Context context, String oxUser) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(UserChatProvider.CONTENT_URI, null,
                    DBHelper.USER_CHAT_COLUMN_OX_USER + "=?", new String[]{String.valueOf(oxUser)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static User getSingleUser(Context context, Object jid) {
        Cursor cursor = null;
        User user = null;
        try {
            cursor = context.getContentResolver().query(UserChatProvider.CONTENT_URI, null,
                    DBHelper.USER_CHAT_COLUMN_OX_USER + "=?", new String[]{String.valueOf(jid)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                user = User.getSingleUser(cursor, 0);
            }

        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return user;
    }

    public static int getMessengerCount(Context context) {
        int total = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DashboardProvider.CONTENT_URI, null,
                    DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Messenger"}, null);
            if (cursor != null && cursor.moveToFirst()) {
                total = cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return total;
    }

    public static void resetCount(Context context, String jid, String groupId) {
        int unread = getUnread(context, jid, groupId);
        int unreadMessenger = getMessengerCount(context);
        int total = unreadMessenger - unread < 0 ? 0 : unreadMessenger - unread;
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, total);
        context.getContentResolver().update(DashboardProvider.CONTENT_URI, contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Messenger"});
        ContentValues cv = new ContentValues();
        String selection;
        String[] args;
        if (groupId == null) {
            selection = DBHelper.CHAT_LIST_COLUMN_JID + " = ? AND " + DBHelper.CHAT_LIST_COLUMN_GROUP_ID + " IS NULL ";
            args = new String[]{jid};
        } else {
            selection = DBHelper.CHAT_LIST_COLUMN_GROUP_ID + " =? ";
            args = new String[]{groupId};
        }
        cv.put(DBHelper.CHAT_LIST_COLUMN_UNREAD, 0);
        context.getContentResolver().update(ChatListProvider.CONTENT_URI, cv, selection, args);
        insertMessenger(context);
    }

    public static void resetCount(Context context) {
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.CHAT_LIST_COLUMN_UNREAD, 0);
        context.getContentResolver().update(ChatListProvider.CONTENT_URI, cv, null, null);
        insertMessenger(context);
    }

    public static boolean isMessageExists(String threadId, Context context) {
        Cursor cursor = null;
        boolean exists = false;
        try {
            cursor = context.getContentResolver().query(ChatProvider.CONTENT_URI, null, DBHelper.CHAT_COLUMN_THREAD_ID + "=?", new String[]{threadId}, null);
            if (cursor != null && cursor.moveToFirst()) {
                exists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return exists;
    }

    public static boolean isLastMessageExist(String threadId, Context context) {
        Cursor cursor = null;
        boolean exists = false;
        try {
            cursor = context.getContentResolver().query(ChatListProvider.CONTENT_URI, null, DBHelper.CHAT_LIST_COLUMN_LAST_THREAD_ID + "=?", new String[]{threadId}, null);
            if (cursor != null && cursor.moveToFirst()) {
                exists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return exists;
    }
}
