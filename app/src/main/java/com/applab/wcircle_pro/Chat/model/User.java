package com.applab.wcircle_pro.Chat.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.InstanceCreator;

import java.lang.reflect.Type;

/**
 * Created by haoyi on 10/12/15.
 */
public class User implements InstanceCreator<User>, Parcelable {

    private long id;

    private String userName;
    private String name;
    private String j_id;
    private String password;
    private String avatar;
    private String email;
    private String status;
    private String last_seen;
    private String last_msg;
    private String user_status;
    private String is_friend_in;
    private String is_friend_out;
    private String is_friend;
    private String is_block;
    private String unread;
    private String chat_type;
    private String roomId;
    private String owner;
    private String groupId;
    private String groupName;
    private String groupImage;

    int f_id;
    long time;
    String reject;
    private String key;

    private long employeeId;
    private String image;
    private String employeeName;
    private String countryImage;
    private String oxUser;
    private String contactNo;
    private String officeNo;
    private String position;

    public String getGroupImage() {
        return groupImage;
    }

    public void setGroupImage(String groupImage) {
        this.groupImage = groupImage;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getOfficeNo() {
        return officeNo;
    }

    public void setOfficeNo(String officeNo) {
        this.officeNo = officeNo;
    }

    public String getCountryImage() {
        return countryImage;
    }

    public void setCountryImage(String countryImage) {
        this.countryImage = countryImage;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public User() {
    }

    public User(Parcel in) {
        readFromParcel(in);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the j_id
     */
    public String getJid() {
        return j_id;
    }

    /**
     * @param jid the j_id to set
     */
    public void setJid(String jid) {
        this.j_id = jid;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * @param avatar the avatar to set
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the last_seen
     */
    public String getLast_seen() {
        return last_seen;
    }

    /**
     * @param last_seen the last_seen to set
     */
    public void setLast_seen(String last_seen) {
        this.last_seen = last_seen;
    }

    /**
     * @return the last_msg
     */
    public String getLast_msg() {
        return last_msg;
    }

    /**
     * @param last_msg the last_msg to set
     */
    public void setLast_msg(String last_msg) {
        this.last_msg = last_msg;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the user_status
     */
    public String getUser_status() {
        return user_status;
    }

    /**
     * @param user_status the user_status to set
     */
    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    /**
     * @return the f_id
     */
    public int getF_id() {
        return f_id;
    }

    /**
     * @param f_id the f_id to set
     */
    public void setF_id(int f_id) {
        this.f_id = f_id;
    }

    /**
     * @return the time
     */
    public long getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(long time) {
        this.time = time;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the is_friend
     */
    public String getIs_friend_in() {
        return is_friend_in;
    }

    /**
     * @param is_friend the is_friend to set
     */
    public void setIs_friend_in(String is_friend) {
        this.is_friend_in = is_friend;
    }

    /**
     * @return the reject
     */
    public String getReject() {
        return reject;
    }

    /**
     * @param reject the reject to set
     */
    public void setReject(String reject) {
        this.reject = reject;
    }

    /**
     * @return the is_block
     */
    public String getIs_block() {
        return is_block;
    }

    /**
     * @param is_block the is_block to set
     */
    public void setIs_block(String is_block) {
        this.is_block = is_block;
    }

    /**
     * @return the unread
     */
    public String getUnread() {
        return unread;
    }

    /**
     * @param unread the unread to set
     */
    public void setUnread(String unread) {
        this.unread = unread;
    }

    /**
     * @return the is_friend_out
     */
    public String getIs_friend_out() {
        return is_friend_out;
    }

    /**
     * @param is_friend_out the is_friend_out to set
     */
    public void setIs_friend_out(String is_friend_out) {
        this.is_friend_out = is_friend_out;
    }

    /**
     * @return the is_friend
     */
    public String getIs_friend() {
        return is_friend;
    }

    /**
     * @param is_friend the is_friend to set
     */
    public void setIs_friend(String is_friend) {
        this.is_friend = is_friend;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */

    public void setChat_type(String chat_type) {
        this.chat_type = chat_type;
    }

    public String getChat_type() {
        return chat_type;
    }

    @Override
    public String toString() {
        return "User [userName=" + userName + ", name=" + name + ", j_id="
                + j_id + ", password=" + password + ", avatar=" + avatar
                + ", email=" + email + ", status=" + status + "]";
    }

    public static User getUser(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        User user = new User();
        user.setGroupId(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_GROUP_ID)));
        user.setGroupName(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_GROUP_NAME)));
        user.setChat_type(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_EXTRA1)));
        user.setLast_msg(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_LAST_MSG)));
        user.setUnread(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_UNREAD)));
        user.setLast_seen(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_LAST)));
        user.setJid(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_JID)));
        user.setName(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_NAME)));
        user.setAvatar(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_AVATAR)));
        user.setEmployeeName(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_NAME)));
        user.setEmployeeId(cursor.getInt(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID)));
        user.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_POSITION)));
        user.setOfficeNo(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_OFFICE_NO)));
        user.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_CONTACT_NO)));
        user.setImage(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_IMAGE)));
        user.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE)));
        user.setId(cursor.getLong(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_ID)));
        user.setUnread(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_UNREAD)));
        user.setGroupImage(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_COLUMN_ICON)));
        return user;
    }

    public static User getSingleUser(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        User user = new User();
        user.setEmployeeName(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_NAME)));
        user.setEmployeeId(cursor.getInt(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID)));
        user.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_POSITION)));
        user.setOfficeNo(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_OFFICE_NO)));
        user.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_CONTACT_NO)));
        user.setImage(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_IMAGE)));
        user.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE)));
        return user;
    }

    public static User getGroupUser(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        User user = new User();
        user.setEmployeeName(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_NAME)));
        user.setEmployeeId(cursor.getInt(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID)));
        user.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_POSITION)));
        user.setOfficeNo(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_OFFICE_NO)));
        user.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_CONTACT_NO)));
        user.setImage(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_IMAGE)));
        user.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_COUNTRY_IMAGE)));
        user.setJid(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_OX_USER)));
        user.setOwner(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_OWNER)));
        return user;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.userName);
        dest.writeString(this.j_id);
        dest.writeString(this.password);
        dest.writeString(this.avatar);
        dest.writeString(this.email);
        dest.writeString(this.status);
        dest.writeString(this.last_seen);
        dest.writeString(this.last_msg);
        dest.writeString(this.user_status);
        dest.writeString(this.is_friend_in);
        dest.writeString(this.is_friend_out);
        dest.writeString(this.is_friend);
        dest.writeString(this.is_block);
        dest.writeString(this.unread);
        dest.writeString(this.chat_type);
        dest.writeInt(this.f_id);
        dest.writeLong(this.time);
        dest.writeString(this.reject);
        dest.writeString(this.key);
        dest.writeLong(this.employeeId);
        dest.writeString(this.image);
        dest.writeString(this.employeeName);
        dest.writeString(this.countryImage);
        dest.writeString(this.oxUser);
        dest.writeString(this.contactNo);
        dest.writeString(this.officeNo);
        dest.writeString(this.position);
        dest.writeString(this.groupImage);

    }

    public void readFromParcel(Parcel in) {
        this.id = in.readLong();
        this.userName = in.readString();
        this.j_id = in.readString();
        this.password = in.readString();
        this.avatar = in.readString();
        this.email = in.readString();
        this.status = in.readString();
        this.last_seen = in.readString();
        this.last_msg = in.readString();
        this.user_status = in.readString();
        this.is_friend_in = in.readString();
        this.is_friend_out = in.readString();
        this.is_friend = in.readString();
        this.is_block = in.readString();
        this.unread = in.readString();
        this.chat_type = in.readString();
        this.f_id = in.readInt();
        this.time = in.readLong();
        this.reject = in.readString();
        this.key = in.readString();
        this.employeeId = in.readLong();
        this.image = in.readString();
        this.employeeName = in.readString();
        this.countryImage = in.readString();
        this.oxUser = in.readString();
        this.contactNo = in.readString();
        this.officeNo = in.readString();
        this.position = in.readString();
        this.groupImage = in.readString();
    }

    @SuppressWarnings("unchecked")
    public static final Creator CREATOR = new Creator() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public User createInstance(Type type) {
        return new User();
    }
}