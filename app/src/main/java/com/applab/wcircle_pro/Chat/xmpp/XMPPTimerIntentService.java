package com.applab.wcircle_pro.Chat.xmpp;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;

import com.applab.wcircle_pro.Login.TokenProvider;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import org.jivesoftware.smack.AbstractXMPPConnection;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by user on 11/11/2015.
 */
public class XMPPTimerIntentService extends IntentService {
    private AbstractXMPPConnection connection;
    // constant
    public static final long NOTIFY_INTERVAL = 60 * 1000; // 60 seconds

    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    public XMPPTimerIntentService() {
        super("XMPPTimerIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (mTimer != null) {
            mTimer.cancel();
        } else {
            mTimer = new Timer();
        }
        try {
            mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
    }

    private void initConnection() {
        if (getStatus(getBaseContext()) == 1) {
            connection = XMPPConn.getInstance().getConn();
            if (Utils.isConnectingToInternet(getBaseContext())) {
                if (connection != null) {
                    if (!connection.isConnected()) {
                        connection.disconnect();
                        startService(new Intent(this, BackgroundXMPPService.class));
                    }
                } else {
                    startService(new Intent(this, BackgroundXMPPService.class));
                }
            }
        }
    }

    public static int getStatus(Context context) {
        Cursor cursor = null;
        int status = 0;
        try {
            cursor = context.getContentResolver().query(TokenProvider.CONTENT_URI, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                status = cursor.getInt(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_STATUS));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return status;
    }

    class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    initConnection();
                }
            });
        }
    }
}
