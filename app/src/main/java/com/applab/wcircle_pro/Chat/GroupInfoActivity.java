package com.applab.wcircle_pro.Chat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.db.GroupProvider;
import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.Chat.xmpp.ChatGroupUploadService;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.RoundImage;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;

import eu.janmuller.android.simplecropimage.CropImage;

public class GroupInfoActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar toolbar;
    private ImageView imgGroup;
    private EditText txtGroupName;
    private LinearLayout btnNext, btnCancel;
    private String TAG = "GROUP";
    private Uri fileUri;
    private File file;
    private static final int SELECT_PHOTO_REQUEST_CODE = 100;
    private static final int CROP_IMAGE_REQUEST_CODE = 46;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 101;
    private String groupId;
    private String groupName;
    private RelativeLayout fadeRL;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_info);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green_light));
        TextView txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(getResources().getString(R.string.title_activity_group_info));
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        groupId = getIntent().getStringExtra("groupId");
        groupName = getIntent().getStringExtra("groupName");
        imgGroup = (ImageView) findViewById(R.id.imgGroup);
        txtGroupName = (EditText) findViewById(R.id.txtGroupName);
        txtGroupName.setText(getIntent().getExtras().getString("groupName") == null ? "" : getIntent().getExtras().getString("groupName"));
        btnNext = (LinearLayout) findViewById(R.id.btnNext);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        Bitmap bm = BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_action_group_light);
        RoundImage roundImage = new RoundImage();
        imgGroup.setImageBitmap(roundImage.getCircleBitmap(bm));
        imgGroup.setOnClickListener(imgGroupOnClickListener);
        btnNext.setOnClickListener(btnNextOnClickLister);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        txtGroupName.setOnEditorActionListener(txtGroupNameOnEditorAction);
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        setVisibilityRlAndProgressBar(false);
        fadeRL.setOnClickListener(fadeRLOnClickListener);
        getSupportLoaderManager().initLoader(579, null, this);
    }

    private View.OnClickListener fadeRLOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private void setVisibilityRlAndProgressBar(boolean isVisible) {
        if (isVisible) {
            fadeRL.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            fadeRL.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }


    private TextView.OnEditorActionListener txtGroupNameOnEditorAction = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (v != null) {
                    InputMethodManager inputManager = (InputMethodManager) getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return true;
            }
            return false;
        }
    };

    private LinearLayout.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibilityRlAndProgressBar(true);
            Intent intent = new Intent(GroupInfoActivity.this, ChartRoomGroupActivity.class);
            intent.putExtra("groupId", groupId);
            intent.putExtra("groupName", groupName);
            startActivity(intent);
            finish();
        }
    };

    private LinearLayout.OnClickListener btnNextOnClickLister = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibilityRlAndProgressBar(true);
            if (Utils.isConnectingToInternet(GroupInfoActivity.this)) {
                Intent intentService = new Intent(GroupInfoActivity.this, ChatGroupUploadService.class);
                if (txtGroupName.getText().length() > 0) {
                    intentService.putExtra("groupId", groupId);
                    if (!groupName.equals(txtGroupName.getText().toString())) {
                        intentService.putExtra("groupName", txtGroupName.getText().toString());
                    } else {

                    }
                    intentService.putExtra("isNew", false);
                    if (file != null) {
                        if (file.length() < 3 * 1000000) {
                            String[] result = file.getName().split("\\.");
                            if (result[result.length - 1].equalsIgnoreCase("jpg") || result[result.length - 1].
                                    equalsIgnoreCase("png") || result[result.length - 1].equalsIgnoreCase("jpeg")) {
                                intentService.putExtra("filePath", file.getAbsolutePath());
                                startService(intentService);
                            } else {
                                Utils.showError(GroupInfoActivity.this, "", "The image must be jpg or png.");
                            }
                        } else {
                            Utils.showError(GroupInfoActivity.this, "", "The image is too large.");
                        }
                    } else {
                        startService(intentService);
                    }
                } else {
                    setVisibilityRlAndProgressBar(false);
                    Utils.showError(GroupInfoActivity.this, Utils.CODE_GROUP_NAME, Utils.GROUP_NAME);
                }
            } else {
                setVisibilityRlAndProgressBar(false);
                Utils.showNoConnection(GroupInfoActivity.this);
            }

        }
    };

    private View.OnClickListener imgGroupOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            GroupDialogFragment groupDialogFragment = GroupDialogFragment.newInstance(GroupInfoActivity.this);
            groupDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap result = null;
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case SELECT_PHOTO_REQUEST_CODE:
                file = GroupActivity.previewSelectedImage(this, data.getData(), null, false, false, 0, true, null);
                GroupActivity.performCrop(Uri.fromFile(file), GroupInfoActivity.this, CROP_IMAGE_REQUEST_CODE);
                break;
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                GroupActivity.performCrop(fileUri, GroupInfoActivity.this, CROP_IMAGE_REQUEST_CODE);
                break;
            case CROP_IMAGE_REQUEST_CODE:
                if (data.getStringExtra(CropImage.IMAGE_PATH) == null) {
                    return;
                }
                result = BitmapFactory.decodeFile(data.getStringExtra(CropImage.IMAGE_PATH));
                file = Utils.getFile(this, result, true, null);
                file = GroupActivity.previewCapturedImage(this, Uri.parse(file.getAbsolutePath()), imgGroup, false, true, 0, true, null);
                Glide.with(GroupInfoActivity.this)
                        .load(file.getAbsolutePath())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(new CircleTransform(GroupInfoActivity.this))
                        .placeholder(R.mipmap.ic_action_group_light)
                        .into(imgGroup);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ChartRoomGroupActivity.class);
        intent.putExtra("groupId", groupId);
        intent.putExtra("groupName", groupName);
        setVisibilityRlAndProgressBar(true);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            setVisibilityRlAndProgressBar(true);
            Intent intent = new Intent(this, ChartRoomGroupActivity.class);
            intent.putExtra("groupId", groupId);
            intent.putExtra("groupName", groupName);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BackgroundXMPPService.ACTION)) {
                if (intent.getBooleanExtra("isCamera", false)) {
                    if (Utils.isDeviceSupportCamera(GroupInfoActivity.this)) {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        fileUri = GroupActivity.getOutputMediaFileUri(GroupInfoActivity.this, true, null);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                        startActivityForResult(cameraIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    } else {
                        Utils.showError(GroupInfoActivity.this, "", "Sorry, you don't have supported camera.");
                    }
                } else if (intent.getBooleanExtra("isPhoto", false)) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, SELECT_PHOTO_REQUEST_CODE);
                } else if (intent.getBooleanExtra("isBack", false)) {
                    Intent mIntent = new Intent(GroupInfoActivity.this, ChartRoomGroupActivity.class);
                    mIntent.putExtra("groupId", groupId);
                    if (!groupName.equals(txtGroupName.getText().toString())) {
                        mIntent.putExtra("groupName", txtGroupName.getText().toString());
                    } else {
                        mIntent.putExtra("groupName", groupName);
                    }
                    startActivity(mIntent);
                    finish();
                } else if (intent.getBooleanExtra("isFail", false)) {
                    setVisibilityRlAndProgressBar(false);
                }
            }
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        setVisibilityRlAndProgressBar(false);
        IntentFilter iff = new IntentFilter(BackgroundXMPPService.ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 579) {
            return new CursorLoader(GroupInfoActivity.this, GroupProvider.CONTENT_URI,
                    null, DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{groupId}, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 579) {
            if (data != null) {
                data.moveToFirst();
                String image = data.getString(data.getColumnIndex(DBHelper.GROUP_COLUMN_ICON));
                Glide.with(GroupInfoActivity.this)
                        .load(image)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(new CircleTransform(GroupInfoActivity.this))
                        .placeholder(R.mipmap.ic_action_group_light)
                        .into(imgGroup);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
