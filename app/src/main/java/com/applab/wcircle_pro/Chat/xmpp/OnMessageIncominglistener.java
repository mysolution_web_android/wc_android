package com.applab.wcircle_pro.Chat.xmpp;

import com.applab.wcircle_pro.Chat.model.MessageModel;

/**
 * Created by haoyi on 10/13/15.
 */
public interface OnMessageIncominglistener {
    void onMessangeIncoming(MessageModel msg, String from);
}
