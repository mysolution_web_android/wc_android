package com.applab.wcircle_pro.Chat.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user on 5/11/2015.
 */
public class NewMember implements Parcelable {
    private String oxUser;
    private String name;
    private String image;
    private String position;
    private String countryImage;
    private int selectedPos = -1;
    private int id;
    private String contactNo;
    private String officeNo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getOfficeNo() {
        return officeNo;
    }

    public void setOfficeNo(String officeNo) {
        this.officeNo = officeNo;
    }

    public int getSelectedPos() {
        return selectedPos;
    }

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

    public String getCountryImage() {
        return countryImage;
    }

    public void setCountryImage(String countryImage) {
        this.countryImage = countryImage;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOxUser() {
        return oxUser;
    }

    public void setOxUser(String oxUser) {
        this.oxUser = oxUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NewMember() {
    }

    public NewMember(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.oxUser);
        dest.writeString(this.name);
        dest.writeString(this.image);
        dest.writeString(this.countryImage);
        dest.writeString(this.position);
        dest.writeInt(this.id);
        dest.writeString(this.contactNo);
        dest.writeString(this.officeNo);
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.oxUser = in.readString();
        this.name = in.readString();
        this.image = in.readString();
        this.countryImage = in.readString();
        this.position = in.readString();
        this.id = in.readInt();
        this.contactNo = in.readString();
        this.officeNo = in.readString();
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public NewMember createFromParcel(Parcel in) {
            return new NewMember(in);
        }

        @Override
        public NewMember[] newArray(int size) {
            return new NewMember[size];
        }
    };
}
