package com.applab.wcircle_pro.Chat.db;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Chat.AddGroupMemberActivity;
import com.applab.wcircle_pro.Chat.ChartRoomGroupActivity;
import com.applab.wcircle_pro.Chat.model.Group;
import com.applab.wcircle_pro.Chat.model.GroupChecking;
import com.applab.wcircle_pro.Chat.model.MessageModel;
import com.applab.wcircle_pro.Chat.model.NewMember;
import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.Chat.xmpp.ChatGroupUploadService;
import com.applab.wcircle_pro.Employee.BranchEmployeeProvider;
import com.applab.wcircle_pro.Employee.CountryChecking;
import com.applab.wcircle_pro.Employee.CountryCheckingProvider;
import com.applab.wcircle_pro.Employee.CountryProvider;
import com.applab.wcircle_pro.Employee.DepartmentEmployeeProvider;
import com.applab.wcircle_pro.Employee.EmployeeChecking;
import com.applab.wcircle_pro.Employee.EmployeeCheckingProvider;
import com.applab.wcircle_pro.Favorite.Contact;
import com.applab.wcircle_pro.Favorite.ContactProvider;
import com.applab.wcircle_pro.Favorite.FavoriteContact;
import com.applab.wcircle_pro.Favorite.FavoriteContactProvider;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by user on 16/11/2015.
 */

public class HttpHelper {

    //region Get Employees for Notification
    public static void getEmployeeGson(Context context, final MessageModel messageModel, String TAG, Integer id) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<EmployeeChecking> mGsonRequest = new GsonRequest<EmployeeChecking>(
                Request.Method.GET,
                Utils.API_URL + "api/Employee/ByOXUser?OXUser[0]=" + messageModel.getFrom(),
                EmployeeChecking.class,
                headers,
                responseEmployeeListener(context, messageModel, TAG, id),
                errorEmployeeListener(context, messageModel, TAG, id)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }


    public static Response.Listener<EmployeeChecking> responseEmployeeListener(final Context context, final MessageModel messageModel, final String TAG, final Integer id) {
        return new Response.Listener<EmployeeChecking>() {
            @Override
            public void onResponse(EmployeeChecking response) {
                insertUser(context, response, messageModel, TAG, id);
            }
        };
    }

    public static Response.ErrorListener errorEmployeeListener(final Context context, final MessageModel messageModel, final String TAG, final Integer id) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(context);
                Utils.serverHandlingError(context, error);
                Intent intent = new Intent(BackgroundXMPPService.ACTION);
                intent.putExtra("refresh", true);
                mgr.sendBroadcast(intent);
                BackgroundXMPPService.showNotification(context, messageModel, null, id);
            }
        };
    }

    public static void insertUser(Context context, EmployeeChecking result, MessageModel messageModel, String TAG, Integer id) {
        String employeeName = null;
        DBHelper helper = new DBHelper(context);
        try {
            for (int i = 0; i < result.getEmployees().size(); i++) {
                if (messageModel.getFrom().equals(result.getEmployees().get(i).getOXUser())) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID, result.getEmployees().get(i).getId());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_OX_USER, result.getEmployees().get(i).getOXUser());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_IMAGE, result.getEmployees().get(i).getProfileImage());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE, result.getEmployees().get(i).getCountryImage());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_NAME, result.getEmployees().get(i).getName());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_CONTACT_NO, result.getEmployees().get(i).getContactNo());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_OFFICE_NO, result.getEmployees().get(i).getOfficeNo());
                    contentValues.put(DBHelper.USER_CHAT_COLUMN_POSITION, result.getEmployees().get(i).getPosition());
                    employeeName = result.getEmployees().get(i).getName();
                    if (ChatDBHelper.isUserExist(context, messageModel.getFrom())) {
                        context.getContentResolver().update(UserChatProvider.CONTENT_URI, contentValues, DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID + "=?",
                                new String[]{String.valueOf(result.getEmployees().get(i).getId())});
                    } else {
                        if (ChatDBHelper.getLastUserId(context) != null) {
                            contentValues.put(DBHelper.USER_CHAT_COLUMN_USER_ID, Long.valueOf(ChatDBHelper.getLastUserId(context)));
                        }
                        context.getContentResolver().insert(UserChatProvider.CONTENT_URI, contentValues);
                    }
                    if (messageModel.getIsGroup()) {
                        if (!isGroupExist(context, messageModel.getGroupId())) {
                            HttpHelper.GetSingleGroup(context, TAG, messageModel.getGroupId(), false);
                        }
                        ContentValues contentValues1 = new ContentValues();
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_GROUP_ID, messageModel.getGroupId());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID, result.getEmployees().get(i).getId());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_OX_USER, result.getEmployees().get(i).getOXUser());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_IMAGE, result.getEmployees().get(i).getProfileImage());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_COUNTRY_IMAGE, result.getEmployees().get(i).getCountryImage());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_NAME, result.getEmployees().get(i).getName());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_CONTACT_NO, result.getEmployees().get(i).getContactNo());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_OFFICE_NO, result.getEmployees().get(i).getOfficeNo());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_POSITION, result.getEmployees().get(i).getPosition());
                        if (ChatDBHelper.isGroupMemberExists(context, messageModel.getGroupId(), result.getEmployees().get(i).getId())) {
                            context.getContentResolver().update(GroupChatProvider.CONTENT_URI, contentValues1,
                                    DBHelper.GROUP_CHAT_COLUMN_GROUP_ID + "=? AND " + DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID + "=?",
                                    new String[]{messageModel.getRoomId(), String.valueOf(result.getEmployees().get(i).getId())});
                        } else {
                            context.getContentResolver().insert(GroupChatProvider.CONTENT_URI, contentValues1);
                        }
                    }
                }
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(context);
                Intent intent = new Intent(BackgroundXMPPService.ACTION);
                intent.putExtra("refresh", true);
                mgr.sendBroadcast(intent);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        } finally {
            BackgroundXMPPService.showNotification(context, messageModel, employeeName, id);
        }
    }

    //endregion

    //region Get Employees for Group Chat
    public static void getEmployeeGroupGson(Context context, String TAG, String oxUser, String groupId, String owner) {
        if (!oxUser.equals("")) {
            final String token = Utils.getToken(context);
            String url = Utils.API_URL + "api/Employee/ByOXUser?" + oxUser;
            HashMap<String, String> headers = new HashMap<String, String>();
            headers.put("Authorization", "Bearer " + token);
            GsonRequest<EmployeeChecking> mGsonRequest = new GsonRequest<EmployeeChecking>(
                    Request.Method.GET,
                    url,
                    EmployeeChecking.class,
                    headers,
                    responseEmployeeGroupListener(context, TAG, groupId, owner),
                    errorEmployeeGroupListener(context, TAG)) {
            };
            mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
        }
    }

    public static Response.Listener<EmployeeChecking> responseEmployeeGroupListener(final Context context, final String TAG,
                                                                                    final String groupId, final String owner) {
        return new Response.Listener<EmployeeChecking>() {
            @Override
            public void onResponse(EmployeeChecking response) {
                if (groupId != null) {
                    context.getContentResolver().delete(GroupChatProvider.CONTENT_URI, DBHelper.GROUP_CHAT_COLUMN_GROUP_ID + "=?", new String[]{groupId});
                }
                insertUser(context, response, TAG, groupId, owner);
            }
        };
    }

    public static Response.ErrorListener errorEmployeeGroupListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {

            }
        };
    }

    public static void insertUser(Context context, EmployeeChecking result, String TAG, String groupId, String owner) {
        DBHelper helper = new DBHelper(context);
        try {
            for (int i = 0; i < result.getEmployees().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID, result.getEmployees().get(i).getId());
                contentValues.put(DBHelper.USER_CHAT_COLUMN_OX_USER, result.getEmployees().get(i).getOXUser());
                contentValues.put(DBHelper.USER_CHAT_COLUMN_IMAGE, result.getEmployees().get(i).getProfileImage());
                contentValues.put(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE, result.getEmployees().get(i).getCountryImage());
                contentValues.put(DBHelper.USER_CHAT_COLUMN_NAME, result.getEmployees().get(i).getName());
                contentValues.put(DBHelper.USER_CHAT_COLUMN_CONTACT_NO, result.getEmployees().get(i).getContactNo());
                contentValues.put(DBHelper.USER_CHAT_COLUMN_OFFICE_NO, result.getEmployees().get(i).getOfficeNo());
                contentValues.put(DBHelper.USER_CHAT_COLUMN_POSITION, result.getEmployees().get(i).getPosition());
                if (groupId != null) {
                    ContentValues contentValues1 = new ContentValues();
                    contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_GROUP_ID, groupId);
                    contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID, result.getEmployees().get(i).getId());
                    contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_OX_USER, result.getEmployees().get(i).getOXUser());
                    contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_IMAGE, result.getEmployees().get(i).getProfileImage());
                    contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_COUNTRY_IMAGE, result.getEmployees().get(i).getCountryImage());
                    contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_NAME, result.getEmployees().get(i).getName());
                    contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_CONTACT_NO, result.getEmployees().get(i).getContactNo());
                    contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_OFFICE_NO, result.getEmployees().get(i).getOfficeNo());
                    contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_POSITION, result.getEmployees().get(i).getPosition());
                    if (owner != null) {
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_OWNER, owner);
                    }
                    if (ChatDBHelper.isGroupMemberExists(context, groupId, result.getEmployees().get(i).getId())) {
                        context.getContentResolver().update(GroupChatProvider.CONTENT_URI, contentValues1,
                                DBHelper.GROUP_CHAT_COLUMN_GROUP_ID + "=? AND " + DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID + "=?",
                                new String[]{groupId, String.valueOf(result.getEmployees().get(i).getId())});
                    } else {
                        context.getContentResolver().insert(GroupChatProvider.CONTENT_URI, contentValues1);
                    }
                }
                if (ChatDBHelper.isUserExist(context, result.getEmployees().get(i).getOXUser())) {
                    context.getContentResolver().update(UserChatProvider.CONTENT_URI, contentValues,
                            DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(result.getEmployees().get(i).getId())});

                } else {
                    if (ChatDBHelper.getLastUserId(context) != null) {
                        contentValues.put(DBHelper.USER_CHAT_COLUMN_USER_ID, Long.valueOf(ChatDBHelper.getLastUserId(context)));
                    }
                    context.getContentResolver().insert(UserChatProvider.CONTENT_URI, contentValues);
                }
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        } finally {
            if (groupId != null) {
                Cursor cursor = null;
                boolean isUserExists = false;
                try {
                    cursor = context.getContentResolver().query(GroupChatProvider.CONTENT_URI, null,
                            DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID + "=? AND " + DBHelper.GROUP_CHAT_COLUMN_GROUP_ID + "=?",
                            new String[]{String.valueOf(Utils.getProfile(context).getId()), groupId}, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        isUserExists = true;
                    }
                } catch (Exception ex) {
                    ex.fillInStackTrace();
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                    if (!isUserExists) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DBHelper.GROUP_COLUMN_IS_DELETE, String.valueOf(true));
                        context.getContentResolver().update(GroupProvider.CONTENT_URI, contentValues,
                                DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{groupId});
                    }
                }
            }
        }
    }
    //endregion

    //region Get Country
    public static void getCountryGson(int pageNoCountry, Context context, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String lastUpdate = "2000-09-09T10:16:25z";
        final Date lastUpdateDate = getCountryLastUpdate(context);
        String url = Utils.API_URL + "api/Country?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                + Utils.encode(pageNoCountry) + "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getCountryRow(context) == 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Country?&NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                    + Utils.encode(pageNoCountry) + "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getCountryRow(context) == 0);
        }
        final String finalUrl = url;
        GsonRequest<CountryChecking> mGsonRequest = new GsonRequest<CountryChecking>(
                Request.Method.GET,
                finalUrl,
                CountryChecking.class,
                headers,
                responseCountryListener(pageNoCountry, context, TAG, txtError),
                errorCountryListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<CountryChecking> responseCountryListener(final int pageNoCountry, final Context context, final String TAG, final TextView txtError) {
        return new Response.Listener<CountryChecking>() {
            @Override
            public void onResponse(CountryChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getCountry().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getCountryLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNoCountry == 1) {
                                        context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNoCountry > 1) {
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertCountry(response, context, pageNoCountry, TAG);
                                } else {
                                    if (pageNoCountry == 1) {
                                        context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    } else if (pageNoCountry > 1) {
                                        context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                    }
                                    insertCountry(response, context, pageNoCountry, TAG);
                                }
                            } else {
                                if (pageNoCountry == 1) {
                                    context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                } else if (pageNoCountry > 1) {
                                    context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                }
                                insertCountry(response, context, pageNoCountry, TAG);
                            }
                        } else {
                            if (pageNoCountry == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(CountryProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(CountryCheckingProvider.CONTENT_URI, null, null);
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    public static int getCountryRow(Context context) {
        int totalRow = 0;
        String[] projection = {DBHelper.COUNTRY_COLUMN_ID};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CountryProvider.CONTENT_URI, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getCountryLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.COUNTRY_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CountryCheckingProvider.CONTENT_URI, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.COUNTRY_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static boolean isCountryExist(Object id, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CountryProvider.CONTENT_URI, null,
                    DBHelper.COUNTRY_COLUMN_COUNTRY_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static void insertCountry(CountryChecking result, Context context, final int pageNoCountry, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesCountryChecking = new ContentValues();
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesCountryChecking.put(DBHelper.COUNTRY_CHECKING_COLUMN_LEVEL, pageNoCountry);
            context.getContentResolver().insert(CountryCheckingProvider.CONTENT_URI, contentValuesCountryChecking);
            ContentValues[] contentValueses = new ContentValues[result.getCountry().size()];
            for (int i = 0; i < result.getCountry().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_ID, result.getCountry().get(i).getId());
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_CODE, result.getCountry().get(i).getCountryCode());
                contentValues.put(DBHelper.COUNTRY_COLUMN_LEVEL, pageNoCountry);
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_NAME, result.getCountry().get(i).getCountryName());
                contentValues.put(DBHelper.COUNTRY_COLUMN_COUNTRY_IMAGE, result.getCountry().get(i).getCountryImage());
                contentValues.put(DBHelper.COUNTRY_COLUMN_LAST_UPDATED, result.getCountry().get(i).getLastUpdated());
                if (isCountryExist(result.getCountry().get(i).getId(), context)) {
                    context.getContentResolver().delete(CountryProvider.CONTENT_URI, DBHelper.COUNTRY_COLUMN_COUNTRY_ID + "=?", new String[]{String.valueOf(result.getCountry().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(CountryProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Country Error :" + ex.toString());
        }
    }

    public static Response.ErrorListener errorCountryListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }
    //endregion

    //region Get Employee from Country
    public static void getEmployeeCountryGson(Context context, int pageNoEmployee, boolean isSearch, String countryCode, String searchKey, String TAG, boolean isClick, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(countryCode) + "&NoPerPage=" +
                Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoEmployee) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getBranchEmployeeRow(context, pageNoEmployee, countryCode) > 0);
        final Date lastUpdateDate = getBranchEmployeeLastUpdate(context);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(countryCode) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoEmployee) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getBranchEmployeeRow(context, pageNoEmployee, countryCode) > 0);
        }

        if (isSearch) {
            url = Utils.API_URL + "api/Employee/FilterNameDepartment?SearchKey=" + Utils.encode(searchKey) + "&branchId=" +
                    Utils.encode(Utils.getProfile(context).getDefaultBranchId()) + "&exceptBranch=" + Utils.encode(true) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoEmployee);
        }
        final String finalUrl = url;
        Log.i(TAG, "Url: " + finalUrl);
        GsonRequest<EmployeeChecking> mGsonRequest = new GsonRequest<EmployeeChecking>(
                Request.Method.GET,
                finalUrl,
                EmployeeChecking.class,
                headers,
                responseBranchEmployeeListener(context, pageNoEmployee, isSearch, TAG, searchKey, countryCode, txtError),
                errorBranchEmployeeListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static int getBranchEmployeeRow(Context context, int pageNo, String countryCode) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(BranchEmployeeProvider.CONTENT_URI, null,
                    DBHelper.BRANCH_EMPLOYEE_COLUMN_PAGE_NO + "=? AND " + DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{String.valueOf(pageNo), countryCode}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getBranchEmployeeLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(EmployeeCheckingProvider.CONTENT_URI, projection, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }


    public static Response.Listener<EmployeeChecking> responseBranchEmployeeListener(final Context context, final int pageNoEmployee, final boolean isSearch, final String TAG, final String searchKey, final String countryCode, final TextView txtError) {
        return new Response.Listener<EmployeeChecking>() {
            @Override
            public void onResponse(EmployeeChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getEmployees().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getBranchEmployeeLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNoEmployee == 1) {
                                        context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNoEmployee > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNoEmployee, isSearch, TAG, searchKey, countryCode);
                                } else {
                                    if (pageNoEmployee == 1) {
                                        context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNoEmployee > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNoEmployee, isSearch, TAG, searchKey, countryCode);
                                }
                            } else {
                                if (pageNoEmployee == 1) {
                                    context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                } else if (pageNoEmployee > 1) {
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                }
                                insertEmployee(context, response, pageNoEmployee, isSearch, TAG, searchKey, countryCode);
                            }
                        } else {
                            if (pageNoEmployee == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
                                }
                            }
                        }
                    }
                }
            }

        };
    }

    public static void insertEmployee(Context context, EmployeeChecking result, int pageNoBranch, boolean isSearch, String TAG, String searchKey, String countryCode) {
        DBHelper helper = new DBHelper(context);
        if (isSearch) {
            context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY + "=?", new String[]{countryCode});
            context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH + "=?", new String[]{String.valueOf(true)});
        }
        try {
            ContentValues contentValuesEmployeeChecking = new ContentValues();
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_BRANCH, String.valueOf(true));
            context.getContentResolver().insert(EmployeeCheckingProvider.CONTENT_URI, contentValuesEmployeeChecking);
            ContentValues[] contentValueses = new ContentValues[result.getEmployees().size()];
            for (int i = 0; i < result.getEmployees().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID, result.getEmployees().get(i).getId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IMAGE, result.getEmployees().get(i).getProfileImage());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_BRANCH_ID, result.getEmployees().get(i).getDefaultBranchId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_PAGE_NO, pageNoBranch);
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_BRANCH_NAME, result.getEmployees().get(i).getDefaultBranchName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMAIL, result.getEmployees().get(i).getEmail());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_DEPARTMENT, result.getEmployees().get(i).getDepartment());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_NAME, result.getEmployees().get(i).getName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_CONTACT_NO, result.getEmployees().get(i).getContactNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_GENDER, result.getEmployees().get(i).getGender());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY, result.getEmployees().get(i).getCountry());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY_IMAGE, result.getEmployees().get(i).getCountryImage());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_OXUSER, result.getEmployees().get(i).getOXUser());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_POSITION, result.getEmployees().get(i).getPosition());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_ALL_BRANCH, result.getEmployees().get(i).getAllBranch());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_LAST_UPDATED, result.getEmployees().get(i).getLastUpdated());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_NO, result.getEmployees().get(i).getEmployeeNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_COUNTRY_NAME, result.getEmployees().get(i).getCountryName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_DOB, result.getEmployees().get(i).getDOB());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_OFFICE_NO, result.getEmployees().get(i).getOfficeNo());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_HOD_ID, result.getEmployees().get(i).getHODId());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_HOD_NAME, result.getEmployees().get(i).getHODName());
                contentValues.put(DBHelper.BRANCH_EMPLOYEE_COLUMN_IS_SELECTED, result.getEmployees().get(i).getFavouriteId() > 0 ? String.valueOf(true) : String.valueOf(false));
                if (isBanchExists(result.getEmployees().get(i).getId(), context)) {
                    context.getContentResolver().delete(BranchEmployeeProvider.CONTENT_URI, DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(result.getEmployees().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(BranchEmployeeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    public static boolean isBanchExists(Object employeeId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(BranchEmployeeProvider.CONTENT_URI, null,
                    DBHelper.BRANCH_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?",
                    new String[]{String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static Response.ErrorListener errorBranchEmployeeListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }


    //endregion

    //region Get Employee Department
    public static void getEmployeeDepartmentGson(Context context, int pageNo, boolean isSearch, String searchKey, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(Utils.getProfile(context).getCountry()) + "&NoPerPage=" +
                Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getDepartmentEmployeeRow(context, pageNo) > 0);
        final Date lastUpdateDate = getDepartmentLastUpdate(context);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Employee/ByCountry?CountryCode=" + Utils.encode(Utils.getProfile(context).getCountry()) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo) + "&LastUpdated=" + lastUpdate + "&ReturnEmpty=" + Utils.encode(getDepartmentEmployeeRow(context, pageNo) > 0);
        }
        if (isSearch) {
            url = Utils.API_URL + "api/Employee/FilterNameDepartment?SearchKey=" + Utils.encode(searchKey) + "&branchId=" +
                    Utils.encode(Utils.getProfile(context).getDefaultBranchId()) + "&exceptBranch=" + Utils.encode(false) + "&NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNo);
        }
        final String finalUrl = url;
        GsonRequest<EmployeeChecking> mGsonRequest = new GsonRequest<EmployeeChecking>(
                Request.Method.GET,
                finalUrl,
                EmployeeChecking.class,
                headers,
                responseDepartmentListener(context, pageNo, txtError, TAG, isSearch, searchKey),
                errorDepartmentListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static int getDepartmentEmployeeRow(Context context, int pageNo) {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DepartmentEmployeeProvider.CONTENT_URI, null,
                    DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_PAGE_NO + "=?", new String[]{String.valueOf(pageNo)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Date getDepartmentLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DepartmentEmployeeProvider.CONTENT_URI, projection, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static Response.ErrorListener errorDepartmentListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }

    public static Response.Listener<EmployeeChecking> responseDepartmentListener(final Context context, final int pageNo, final TextView txtError, final String TAG, final boolean isSearch, final String searchKey) {
        return new Response.Listener<EmployeeChecking>() {
            @Override
            public void onResponse(EmployeeChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getEmployees().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getDepartmentLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNo, TAG, isSearch, searchKey);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                    }
                                    insertEmployee(context, response, pageNo, TAG, isSearch, searchKey);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                }
                                insertEmployee(context, response, pageNo, TAG, isSearch, searchKey);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(EmployeeCheckingProvider.CONTENT_URI, DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT + "=?", new String[]{String.valueOf(true)});
                                }
                            }
                        }
                    }
                }
            }

        };
    }

    public static void insertEmployee(Context context, EmployeeChecking result, int pageNo, String TAG, boolean isSearch, String searchKey) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesEmployeeChecking = new ContentValues();
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_PAGE_NO, pageNo);
            contentValuesEmployeeChecking.put(DBHelper.EMPLOYEE_CHECKING_COLUMN_IS_DEPARTMENT, String.valueOf(true));
            context.getContentResolver().insert(EmployeeCheckingProvider.CONTENT_URI, contentValuesEmployeeChecking);
            ContentValues[] contentValueses = new ContentValues[result.getEmployees().size()];
            for (int i = 0; i < result.getEmployees().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID, result.getEmployees().get(i).getId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IMAGE, result.getEmployees().get(i).getProfileImage());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_FAVORITE_ID, result.getEmployees().get(i).getFavouriteId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_ID, result.getEmployees().get(i).getDefaultBranchId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_PAGE_NO, pageNo);
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_BRANCH_NAME, result.getEmployees().get(i).getDefaultBranchName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMAIL, result.getEmployees().get(i).getEmail());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DEPARTMENT, result.getEmployees().get(i).getDepartment());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_NAME, result.getEmployees().get(i).getName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_CONTACT_NO, result.getEmployees().get(i).getContactNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_GENDER, result.getEmployees().get(i).getGender());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY, result.getEmployees().get(i).getCountry());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_IMAGE, result.getEmployees().get(i).getCountryImage());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_OXUSER, result.getEmployees().get(i).getOXUser());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_POSITION, result.getEmployees().get(i).getPosition());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_ALL_BRANCH, result.getEmployees().get(i).getAllBranch());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_LAST_UPDATED, result.getEmployees().get(i).getLastUpdated());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_NO, result.getEmployees().get(i).getEmployeeNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_COUNTRY_NAME, result.getEmployees().get(i).getCountryName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_DOB, result.getEmployees().get(i).getDOB());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_OFFICE_NO, result.getEmployees().get(i).getOfficeNo());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_HOD_ID, result.getEmployees().get(i).getHODId());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_HOD_NAME, result.getEmployees().get(i).getHODName());
                contentValues.put(DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_IS_SELECTED, result.getEmployees().get(i).getFavouriteId() > 0 ? String.valueOf(true) : String.valueOf(false));
                if (isDepartmentExists(result.getEmployees().get(i).getId(), context)) {
                    context.getContentResolver().delete(DepartmentEmployeeProvider.CONTENT_URI, DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=?", new String[]{String.valueOf(result.getEmployees().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(DepartmentEmployeeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }

    public static boolean isDepartmentExists(Object employeeId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DepartmentEmployeeProvider.CONTENT_URI, null,
                    DBHelper.DEPARTMENT_EMPLOYEE_COLUMN_EMPLOYEE_ID + "=? ", new String[]{String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }
    //endregion

    //region Get Favorite
    public static void getFavoriteGson(Context context, int pageNo, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        Log.i(TAG, token);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        final Date lastUpdateDate = getFavoriteLastUpdate(context);
        String lastUpdate = "2000-09-09T10:16:25z";
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
        }
        final String date = lastUpdate;
        GsonRequest<FavoriteContact> mGsonRequest = new GsonRequest<FavoriteContact>(
                Request.Method.GET,
                Utils.API_URL + "api/Employee/FavouriteContact?NoPerPage=" + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                        + Utils.encode(pageNo) + "&LastUpdated=" + Utils.encode(date) + "&ReturnEmpty=" + Utils.encode(getContactRow(context) == 0) + "",
                FavoriteContact.class,
                headers,
                responseFavoriteListener(context, pageNo, TAG, txtError),
                errorFavoriteListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<FavoriteContact> responseFavoriteListener(final Context context, final int pageNo, final String TAG, final TextView txtError) {
        return new Response.Listener<FavoriteContact>() {
            @Override
            public void onResponse(FavoriteContact response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), context);
                    } else {
                        if (response.getContacts().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getFavoriteLastUpdate(context);
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                    }
                                    insertFavoriteContact(context, response, pageNo, TAG);
                                } else {
                                    if (pageNo == 1) {
                                        context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                    } else if (pageNo > 1) {
                                        context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                    }
                                    insertFavoriteContact(context, response, pageNo, TAG);
                                }
                            } else {
                                if (pageNo == 1) {
                                    context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                } else if (pageNo > 1) {
                                    context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                }
                                insertFavoriteContact(context, response, pageNo, TAG);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    context.getContentResolver().delete(ContactProvider.CONTENT_URI, null, null);
                                    context.getContentResolver().delete(FavoriteContactProvider.CONTENT_URI, null, null);
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    public static Date getFavoriteLastUpdate(Context context) {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.FAVORITE_CONTACT_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(FavoriteContactProvider.CONTENT_URI, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.FAVORITE_CONTACT_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public static int getContactRow(Context context) {
        int totalRow = 0;
        String[] projection = {DBHelper.CONTACT_COLUMN_ID};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ContactProvider.CONTENT_URI, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    public static Response.ErrorListener errorFavoriteListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

            }
        };
    }

    public static Contact getContact(Context context, Object contactId) {
        Contact contact = null;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ContactProvider.CONTENT_URI, null,
                    DBHelper.CONTACT_COLUMN_ID + "=?", new String[]{String.valueOf(contactId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                contact = Contact.getContact(cursor, 0);
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return contact;
    }


    public static void insertFavoriteContact(Context context, FavoriteContact result, int pageNo, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValuesFavorite = new ContentValues();
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_NO_PER_PAGE, result.getNoPerPage());
            contentValuesFavorite.put(DBHelper.FAVORITE_CONTACT_COLUMN_PAGE_NO, pageNo);
            context.getContentResolver().insert(FavoriteContactProvider.CONTENT_URI, contentValuesFavorite);
            ContentValues[] contentValueses = new ContentValues[result.getContacts().size()];
            for (int i = 0; i < result.getContacts().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.CONTACT_COLUMN_ALL_BRANCH, result.getContacts().get(i).getAllBranch());
                contentValues.put(DBHelper.CONTACT_COLUMN_BRANCH_ID, result.getContacts().get(i).getDefaultBranchId());
                contentValues.put(DBHelper.CONTACT_COLUMN_BRANCH_NAME, result.getContacts().get(i).getDefaultBranchName());
                contentValues.put(DBHelper.CONTACT_COLUMN_CONTACT_NO, result.getContacts().get(i).getContactNo());
                contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY, result.getContacts().get(i).getCountry());
                contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY_IMAGE, result.getContacts().get(i).getCountryImage());
                contentValues.put(DBHelper.CONTACT_COLUMN_DEPARTMENT, result.getContacts().get(i).getDepartment());
                contentValues.put(DBHelper.CONTACT_COLUMN_PAGE_NO, pageNo);
                contentValues.put(DBHelper.CONTACT_COLUMN_EMAIL, result.getContacts().get(i).getEmail());
                contentValues.put(DBHelper.CONTACT_COLUMN_FAVORITE_ID, result.getContacts().get(i).getFavouriteId());
                contentValues.put(DBHelper.CONTACT_COLUMN_GENDER, result.getContacts().get(i).getGender());
                contentValues.put(DBHelper.CONTACT_COLUMN_NAME, result.getContacts().get(i).getName());
                contentValues.put(DBHelper.CONTACT_COLUMN_LAST_UPDATED, result.getContacts().get(i).getLastUpdated());
                contentValues.put(DBHelper.CONTACT_COLUMN_IMAGE, result.getContacts().get(i).getProfileImage());
                contentValues.put(DBHelper.CONTACT_COLUMN_POSITION, result.getContacts().get(i).getPosition());
                contentValues.put(DBHelper.CONTACT_COLUMN_EMPLOYEE_NO, result.getContacts().get(i).getEmployeeNo());
                contentValues.put(DBHelper.CONTACT_COLUMN_COUNTRY_NAME, result.getContacts().get(i).getCountryName());
                contentValues.put(DBHelper.CONTACT_COLUMN_DOB, result.getContacts().get(i).getDOB());
                contentValues.put(DBHelper.CONTACT_COLUMN_OFFICE_NO, result.getContacts().get(i).getOfficeNo());
                contentValues.put(DBHelper.CONTACT_COLUMN_HOD_ID, result.getContacts().get(i).getHODId());
                contentValues.put(DBHelper.CONTACT_COLUMN_HOD_NAME, result.getContacts().get(i).getHODName());
                contentValues.put(DBHelper.CONTACT_COLUMN_OXUSER, result.getContacts().get(i).getOXUser());
                contentValues.put(DBHelper.CONTACT_COLUMN_ID, result.getContacts().get(i).getId());
                if (getContact(context, result.getContacts().get(i).getId()) != null) {
                    context.getContentResolver().delete(ContactProvider.CONTENT_URI, DBHelper.CONTACT_COLUMN_ID + "=?", new String[]{String.valueOf(result.getContacts().get(i).getId())});
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(ContactProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }
    //endregion

    //region Create New Group region
    public static void CreateNewGroupGroup(Context context, String TAG, final String roomName,
                                           ArrayList<NewMember> addedEmployee, String filePath) {
        String oxUser = "&OXUsers";
        NewMember newMember = new NewMember();
        newMember.setOxUser(Utils.getProfile(context).getOXUser());
        addedEmployee.add(0, newMember);
        for (int i = 0; i < addedEmployee.size(); i++) {
            if (i == 0) {
                oxUser += "[" + i + "]=" + addedEmployee.get(i).getOxUser();
            } else {
                oxUser += "&OXUsers[" + i + "]=" + addedEmployee.get(i).getOxUser();
            }
        }
        final String finaOxUser = oxUser;
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<String> mGsonRequest = new GsonRequest<String>(
                Request.Method.POST,
                Utils.API_URL + "api/Messenger/Group",
                String.class,
                headers,
                responseCreateNewGroupListener(context, TAG, roomName, filePath),
                errorCreateNewGroupListener(context)) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String httpBody = "GroupName=" + roomName + finaOxUser;
                return httpBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);

    }

    public static Response.Listener<String> responseCreateNewGroupListener(final Context context, final String TAG, final String roomName, final String filePath) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                try {
                    HttpHelper.GetSingleGroup(context, TAG, response, false);
                    BackgroundXMPPService.sendMessage(context, "Welcome", null, 1, 0, roomName, response, TAG, null, null, 0, false);

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.GROUP_COLUMN_IS_DELETE, String.valueOf(false));
                    contentValues.put(DBHelper.GROUP_COLUMN_GROUP_ID, response);
                    contentValues.put(DBHelper.GROUP_COLUMN_NAME, roomName);
                    contentValues.put(DBHelper.GROUP_COLUMN_CREATE_BY, Utils.getProfile(context).getOXUser());
                    context.getContentResolver().insert(GroupProvider.CONTENT_URI, contentValues);
                } catch (Exception ex) {
                    ex.fillInStackTrace();
                } finally {
                    Intent intent = new Intent(context, ChartRoomGroupActivity.class);
                    intent.putExtra("groupId", response);
                    intent.putExtra("groupName", roomName);

                    Intent intentService = new Intent(context, ChatGroupUploadService.class);
                    intentService.putExtra("groupId", response);
                    intentService.putExtra("groupName", roomName);
                    intentService.putExtra("isNew", true);
                    if (filePath != null) {
                        intentService.putExtra("filePath", filePath);
                    }
                    context.startService(intentService);

                    context.startActivity(intent);
                    ((Activity) context).finish();
                }
            }
        };
    }

    public static Response.ErrorListener errorCreateNewGroupListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Intent mIntent = new Intent(AddGroupMemberActivity.ACTION);
                mIntent.putExtra("isFail", true);
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(context);
                mgr.sendBroadcast(mIntent);

                Utils.serverHandlingError(context, error);
            }
        };
    }

    //endregion

    //region Post New Group region
    public static void PostNewMemberGroup(Context context, String TAG, final String groupId, ArrayList<NewMember> addedEmployee, ArrayList<NewMember> removeUser) {
        if (addedEmployee.size() > 0) {
            String oxUser = "&OXUsers";
            for (int i = 0; i < addedEmployee.size(); i++) {
                if (i == 0) {
                    oxUser += "[" + i + "]=" + addedEmployee.get(i).getOxUser();
                } else {
                    oxUser += "&OXUsers[" + i + "]=" + addedEmployee.get(i).getOxUser();
                }
            }
            final String finaOxUser = oxUser;
            final String token = Utils.getToken(context);
            HashMap<String, String> headers = new HashMap<String, String>();
            headers.put("Authorization", "Bearer " + token);
            GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                    Request.Method.POST,
                    Utils.API_URL + "api/Messenger/Group/Member",
                    JsonObject.class,
                    headers,
                    responsePostNewGroupListener(context, TAG, groupId, addedEmployee, removeUser),
                    errorPostNewGroupListener(context)) {
                @Override
                public byte[] getBody() throws AuthFailureError {
                    String httpBody = "GroupId=" + groupId + finaOxUser;
                    return httpBody.getBytes();
                }
            };
            mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
        } else {
            HttpHelper.DeleteMemberGroup(context, TAG, groupId, removeUser);
        }
    }

    public static Response.Listener<JsonObject> responsePostNewGroupListener(final Context context, final String TAG, final String groupId, final ArrayList<NewMember> addedMember, final ArrayList<NewMember> removeUser) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                for (int i = 0; i < addedMember.size(); i++) {
                    if (!addedMember.get(i).getOxUser().equals(Utils.getProfile(context).getOXUser())) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID, addedMember.get(i).getId());
                        contentValues.put(DBHelper.USER_CHAT_COLUMN_OX_USER, addedMember.get(i).getOxUser());
                        contentValues.put(DBHelper.USER_CHAT_COLUMN_IMAGE, addedMember.get(i).getImage());
                        contentValues.put(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE, addedMember.get(i).getCountryImage());
                        contentValues.put(DBHelper.USER_CHAT_COLUMN_NAME, addedMember.get(i).getName());
                        contentValues.put(DBHelper.USER_CHAT_COLUMN_CONTACT_NO, addedMember.get(i).getContactNo());
                        contentValues.put(DBHelper.USER_CHAT_COLUMN_OFFICE_NO, addedMember.get(i).getOfficeNo());
                        contentValues.put(DBHelper.USER_CHAT_COLUMN_POSITION, addedMember.get(i).getPosition());
                        if (ChatDBHelper.isUserExist(context, String.valueOf(addedMember.get(i).getOxUser()))) {
                            context.getContentResolver().update(UserChatProvider.CONTENT_URI, contentValues,
                                    DBHelper.USER_CHAT_COLUMN_OX_USER + "=?",
                                    new String[]{String.valueOf(addedMember.get(i).getOxUser())});
                        } else {
                            context.getContentResolver().insert(UserChatProvider.CONTENT_URI, contentValues);
                        }
                        ContentValues contentValues1 = new ContentValues();
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_GROUP_ID, groupId);
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID, addedMember.get(i).getId());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_OX_USER, addedMember.get(i).getOxUser());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_IMAGE, addedMember.get(i).getImage());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_COUNTRY_IMAGE, addedMember.get(i).getCountryImage());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_NAME, addedMember.get(i).getName());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_CONTACT_NO, addedMember.get(i).getContactNo());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_OFFICE_NO, addedMember.get(i).getOfficeNo());
                        contentValues1.put(DBHelper.GROUP_CHAT_COLUMN_POSITION, addedMember.get(i).getPosition());
                        if (ChatDBHelper.isGroupMemberExists(context, groupId, addedMember.get(i).getId())) {
                            context.getContentResolver().update(GroupChatProvider.CONTENT_URI, contentValues1,
                                    DBHelper.GROUP_CHAT_COLUMN_GROUP_ID + "=? AND " + DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID + "=?",
                                    new String[]{groupId, String.valueOf(addedMember.get(i).getId())});
                        } else {
                            context.getContentResolver().insert(GroupChatProvider.CONTENT_URI, contentValues1);
                        }
                    }
                }
                HttpHelper.DeleteMemberGroup(context, TAG, groupId, removeUser);
            }
        };
    }

    public static Response.ErrorListener errorPostNewGroupListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }
    //endregion

    //region Get Single Group with member region
    public static void GetSingleGroup(Context context, String TAG, final String groupId, final boolean isDelete) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Group> mGsonRequest = new GsonRequest<Group>(
                Request.Method.GET,
                Utils.API_URL + "api/Messenger/Group/Member?GroupId=" + groupId,
                Group.class,
                headers,
                responseGetSingleGroupListener(context, TAG, groupId),
                errorGetSingleGroupListener(context, groupId, isDelete)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Group> responseGetSingleGroupListener(final Context context, final String TAG, final String groupId) {
        return new Response.Listener<Group>() {
            @Override
            public void onResponse(Group response) {
                if (response.getMembers().size() > 0) {
                    String oxUser = "";
                    for (int i = 0; i < response.getMembers().size(); i++) {
                        if (i == 0) {
                            oxUser = "OXUser[" + 0 + "]=" + response.getMembers().get(i).getOXUser();
                        } else {
                            oxUser += "&OXUser[" + i + "]=" + response.getMembers().get(i).getOXUser();
                        }
                    }
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.GROUP_COLUMN_GROUP_ID, response.getMembers().get(0).getGroupId());
                    contentValues.put(DBHelper.GROUP_COLUMN_NAME, response.getMembers().get(0).getGroupName());
                    contentValues.put(DBHelper.GROUP_COLUMN_ICON, response.getMembers().get(0).getGroupImage());
                    contentValues.put(DBHelper.GROUP_COLUMN_IS_DELETE, String.valueOf(false));
                    contentValues.put(DBHelper.GROUP_COLUMN_CREATE_BY, response.getMembers().get(0).getCreateBy());
                    if (isGroupExist(context, response.getMembers().get(0).getGroupId())) {
                        context.getContentResolver().update(GroupProvider.CONTENT_URI, contentValues,
                                DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{response.getMembers().get(0).getGroupId()});
                    } else {
                        context.getContentResolver().insert(GroupProvider.CONTENT_URI, contentValues);
                    }
                    contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_GROUP_NAME, response.getMembers().get(0).getGroupName());
                    context.getContentResolver().update(ChatListProvider.CONTENT_URI, contentValues,
                            DBHelper.CHAT_LIST_COLUMN_GROUP_ID + "=?", new String[]{response.getMembers().get(0).getGroupId()});

                    contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_COLUMN_GROUP_NAME, response.getMembers().get(0).getGroupName());
                    context.getContentResolver().update(ChatProvider.CONTENT_URI, contentValues,
                            DBHelper.CHAT_COLUMN_GROUP_ID + "=?", new String[]{response.getMembers().get(0).getGroupId()});

                    HttpHelper.getEmployeeGroupGson(context, TAG, oxUser, groupId, response.getMembers().get(0).getCreateBy());
                }
            }
        };
    }

    public static Response.ErrorListener errorGetSingleGroupListener(final Context context, final String groupId, final boolean isDelete) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                //Utils.serverHandlingError(context, error);
                if (error instanceof ServerError) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.GROUP_COLUMN_IS_DELETE, String.valueOf(true));
                    if (isGroupExist(context, groupId)) {
                        context.getContentResolver().update(GroupProvider.CONTENT_URI,
                                contentValues, DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{groupId});
                    }
                }
            }
        };
    }
    //endregion

    //region Get All Group region
    public static void GetAllGroup(Context context, String TAG, TextView txtError, String groupId, LoaderManager loaderManager, LoaderManager.LoaderCallbacks callbacks, int loaderId) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<GroupChecking> mGsonRequest = new GsonRequest<GroupChecking>(
                Request.Method.GET,
                Utils.API_URL + "api/Messenger/Group",
                GroupChecking.class,
                headers,
                responseGetAllGroupListener(context, TAG, txtError, groupId, loaderManager, callbacks, loaderId),
                errorGetAllGroupListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<GroupChecking> responseGetAllGroupListener(final Context context, final String TAG, final TextView txtError,
                                                                               final String groupId, final LoaderManager loaderManager, final LoaderManager.LoaderCallbacks callbacks, final int loaderId) {
        return new Response.Listener<GroupChecking>() {
            @Override
            public void onResponse(GroupChecking response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                insertGroup(response, context, groupId, TAG, loaderManager, callbacks, loaderId);
            }
        };
    }

    public static Response.ErrorListener errorGetAllGroupListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }
            }
        };
    }

    public static void insertGroup(GroupChecking response, Context context, String groupId, String TAG, LoaderManager loaderManager, LoaderManager.LoaderCallbacks callbacks, int loaderId) {
        try {
            ContentValues[] contentValueses = new ContentValues[response.getGroups().size()];
            for (int i = 0; i < response.getGroups().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.GROUP_COLUMN_GROUP_ID, response.getGroups().get(i).getId());
                contentValues.put(DBHelper.GROUP_COLUMN_NAME, response.getGroups().get(i).getName());
                contentValues.put(DBHelper.GROUP_COLUMN_ICON, response.getGroups().get(i).getImage());
                contentValues.put(DBHelper.GROUP_COLUMN_CREATE_BY, response.getGroups().get(i).getCreateBy());
                contentValues.put(DBHelper.GROUP_COLUMN_CREATE_DATE, response.getGroups().get(i).getCreateDate());
                contentValues.put(DBHelper.GROUP_COLUMN_UPDATE_BY, response.getGroups().get(i).getUpdateBy());
                contentValues.put(DBHelper.GROUP_COLUMN_UPDATE_DATE, response.getGroups().get(i).getUpdateBy());
                contentValues.put(DBHelper.GROUP_COLUMN_IS_DELETE, String.valueOf(false));
                if (!isGroupExist(context, response.getGroups().get(i).getId())) {
                    BackgroundXMPPService.sendMessage(context, "Welcome", null, 1, 0, response.getGroups().get(i).getName(), response.getGroups().get(i).getId(), TAG, loaderManager, callbacks, loaderId, true);
                } else if (isGroupExist(context, response.getGroups().get(i).getId())) {
                    context.getContentResolver().delete(GroupProvider.CONTENT_URI, DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{String.valueOf(response.getGroups().get(i).getId())});
                }
                contentValueses[i] = contentValues;

                contentValues = new ContentValues();
                contentValues.put(DBHelper.CHAT_LIST_COLUMN_GROUP_NAME, response.getGroups().get(i).getName());
                context.getContentResolver().update(ChatListProvider.CONTENT_URI, contentValues,
                        DBHelper.CHAT_LIST_COLUMN_GROUP_ID + "=?", new String[]{response.getGroups().get(i).getId()});

                contentValues = new ContentValues();
                contentValues.put(DBHelper.CHAT_COLUMN_GROUP_NAME, response.getGroups().get(i).getName());
                context.getContentResolver().update(ChatProvider.CONTENT_URI, contentValues,
                        DBHelper.CHAT_COLUMN_GROUP_ID + "=?", new String[]{response.getGroups().get(i).getId()});

            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(GroupProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }
    }

    public static boolean isGroupExist(Context context, Object id) {
        boolean isExist = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(GroupProvider.CONTENT_URI, null,
                    DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExist = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExist;

    }
    //endregion

    //region Send Group Message region
    public static void SendMessageGroup(Context context, String TAG, final String groupId, final int isGroup,
                                        final int isImage, final String threadId, final String message,
                                        final String groupName) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.POST,
                Utils.API_URL + "api/Messenger/Send",
                JsonObject.class,
                headers,
                responseSendMessageGroupListener(context, TAG, groupId, groupName),
                errorSendMessageGroupListener(context, TAG, groupId)) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String httpBody = "GroupId=" + groupId + "&IsImage=" +
                        isImage + "&IsGroup=" + isGroup + "&MessageUUID=" +
                        threadId + "&Message=" + message;
                return httpBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JsonObject> responseSendMessageGroupListener(final Context context,
                                                                                 final String TAG, final String groupId,
                                                                                 final String groupName) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {

            }
        };
    }

    static Response.ErrorListener errorSendMessageGroupListener(final Context context, final String TAG, final String groupId) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                GetAllGroup(context, TAG, null, groupId, null, null, 0);
                Log.i(TAG, "Unable to send message");
                Utils.serverHandlingError(context, error);
            }
        };
    }
    //endregion

    //region Delete Member region
    public static void DeleteMemberGroup(Context context, String TAG, final String groupId, ArrayList<NewMember> removeEmployee) {
        if (removeEmployee.size() > 0) {
            for (int i = 0; i < removeEmployee.size(); i++) {
                final String token = Utils.getToken(context);
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + token);
                String url = Utils.API_URL + "api/Messenger/Group/Member?GroupId=" + groupId + "&OXUser=" + removeEmployee.get(i).getOxUser();
                GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                        Request.Method.DELETE,
                        Utils.API_URL + "api/Messenger/Group/Member?GroupId=" + groupId + "&OXUser=" + removeEmployee.get(i).getOxUser(),
                        JsonObject.class,
                        headers,
                        responseDeleteMemberGroupListener(context, TAG, groupId, removeEmployee),
                        errorDeleteMemberGroupListener(context)) {
                };
                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }
    }

    public static Response.Listener<JsonObject> responseDeleteMemberGroupListener(final Context context, final String TAG, final String groupId, final ArrayList<NewMember> removeEmployee) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                for (int i = 0; i < removeEmployee.size(); i++) {
                    context.getContentResolver().delete(GroupChatProvider.CONTENT_URI, DBHelper.GROUP_CHAT_COLUMN_GROUP_ID + "=? AND " + DBHelper.GROUP_CHAT_COLUMN_OX_USER + "=?",
                            new String[]{groupId, String.valueOf(removeEmployee.get(i).getOxUser())});
                }
            }
        };
    }

    public static Response.ErrorListener errorDeleteMemberGroupListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }
    //endregion

    //region Delete Group region
    public static void DeleteGroup(Context context, String TAG, final String groupId) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String url = Utils.API_URL + "api/Messenger/Group?GroupId=" + groupId;
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.DELETE,
                Utils.API_URL + "api/Messenger/Group?GroupId=" + groupId,
                JsonObject.class,
                headers,
                responseDeleteGroupListener(context, TAG, groupId),
                errorDeleteGroupListener(context, groupId)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JsonObject> responseDeleteGroupListener(final Context context, final String TAG, final String groupId) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {

            }
        };
    }

    public static Response.ErrorListener errorDeleteGroupListener(final Context context, final String groupId) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.GROUP_COLUMN_IS_DELETE, String.valueOf(false));
                context.getContentResolver().update(GroupProvider.CONTENT_URI, contentValues, DBHelper.GROUP_COLUMN_GROUP_ID + "=?",
                        new String[]{groupId});
                Utils.serverHandlingError(context, error);
            }
        };
    }
    //endregion

    //region Exit Group region
    public static void ExitGroup(Context context, String TAG, String groupId) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String url = Utils.API_URL + "api/Messenger/Group/Quit?GroupId=" + groupId;
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.DELETE,
                url,
                JsonObject.class,
                headers,
                responseExitGroupListener(context, TAG, groupId),
                errorExitGroupListener(context, groupId)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JsonObject> responseExitGroupListener(final Context context, final String TAG, final String groupId) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {

            }
        };
    }

    public static Response.ErrorListener errorExitGroupListener(final Context context, final String groupId) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.GROUP_COLUMN_IS_DELETE, String.valueOf(false));
                context.getContentResolver().update(GroupProvider.CONTENT_URI, contentValues, DBHelper.GROUP_COLUMN_GROUP_ID + "=?",
                        new String[]{groupId});
                Utils.serverHandlingError(context, error);
            }
        };
    }
    //endregion

    //region Server Date region
    public static void GetServerDate(Context context, String TAG) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String url = Utils.API_URL + "api/Messenger/ServerDate";
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.GET,
                url,
                JsonObject.class,
                headers,
                responseServerDateListener(context, TAG),
                errorServerDateListener(context)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JsonObject> responseServerDateListener(final Context context, final String TAG) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                String serverDate = Utils.trimMessage(response.toString(), "ServerDate");
                long diff = 0;
                boolean isAfter = false;
                if (serverDate != null) {
                    Date d1 = Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, serverDate));
                    Date d2 = Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, new Date()));
                    if (d1.before(d2)) {
                        diff = d2.getTime() - d1.getTime();
                        isAfter = false;
                    } else if (d1.after(d2)) {
                        diff = d1.getTime() - d2.getTime();
                        isAfter = true;
                    } else if (d1.equals(d2)) {
                        diff = 0;
                    }
                    if (diff < 0) {
                        diff = diff * -1;
                    }
                    SharedPreferences pref;
                    SharedPreferences.Editor editor;
                    pref = context.getSharedPreferences("Server Date", 0);
                    editor = pref.edit();
                    editor.clear();
                    editor.commit();
                    editor.putLong("server_date", diff);
                    editor.putBoolean("isAfter", isAfter);
                    editor.commit();
                }
            }
        };
    }

    public static Response.ErrorListener errorServerDateListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }
    //endregion

    //region Send Message Notification region
    public static void NotificationGroup(Context context, String TAG, final String groupId) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        String url = Utils.API_URL + "api/Messenger/Group/Notification";
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.PUT,
                url,
                JsonObject.class,
                headers,
                responseNotificationGroupListener(context, TAG, groupId),
                errorNotificationGroupListener(context, groupId)) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String httpBody = "GroupId=" + groupId;
                return httpBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<JsonObject> responseNotificationGroupListener(final Context context, final String TAG, final String groupId) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {

            }
        };
    }

    public static Response.ErrorListener errorNotificationGroupListener(final Context context, final String groupId) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(context, error);
            }
        };
    }
    //endregion
}
