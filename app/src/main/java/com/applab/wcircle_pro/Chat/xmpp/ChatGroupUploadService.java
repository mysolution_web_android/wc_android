package com.applab.wcircle_pro.Chat.xmpp;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Chat.db.ChatListProvider;
import com.applab.wcircle_pro.Chat.db.ChatProvider;
import com.applab.wcircle_pro.Chat.db.GroupProvider;
import com.applab.wcircle_pro.Chat.db.HttpHelper;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.HashMap;

public class ChatGroupUploadService extends IntentService implements MultiPartGroup.MultipartProgressListener {
    private String mFilePath;
    private String mGroupId, mGroupName;
    private boolean mIsNew = false;
    private String TAG = "EDIT PROFILE";
    private File mFile;
    public static LocalBroadcastManager mgr;

    public ChatGroupUploadService() {
        super("ChatGroupUploadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle bundle = intent.getExtras();
        mgr = LocalBroadcastManager.getInstance(this);
        mFilePath = bundle.getString("filePath");
        mGroupId = bundle.getString("groupId");
        mGroupName = bundle.getString("groupName");
        mIsNew = bundle.getBoolean("isNew");
        if (mFilePath != null) {
            mFile = new File(mFilePath);
        }

        if (mFile != null) {
            if (Utils.getUsableSpace() < new File(mFilePath).length()) {
                Utils.showError(getBaseContext(),
                        Utils.CODE_LOCAL_SIZE_EXCEEDED, Utils.LOCAL_SIZE_EXCEED);
            } else {
                putChatImage(mFile, mGroupId, mGroupName, mIsNew);
            }
        } else {
            //putChatImage(null, mGroupId, mGroupName, mIsNew);
        }

    }

    @Override
    public void transferred(long transferred, long progress) {
        final NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(mFile.getName())
                .setContentText("Upload in progress")
                .setSmallIcon(R.mipmap.icon);

        // Start a lengthy operation in a background thread
        mBuilder.setProgress(100, (int) progress, false);
        // Displays the progress bar for the first time.
        mNotifyManager.notify(1511, mBuilder.build());
        // When the loop is finished, updates the notification
        if (100 == progress) {
            mBuilder.setContentText("Upload complete")
                    // Removes the progress bar
                    .setProgress(0, 0, false);
            mNotifyManager.notify(1511, mBuilder.build());
        }
    }

    public void putChatImage(File file, String groupId, String groupName, boolean isNew) {
        final String token = Utils.getToken(getBaseContext());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        MultiPartGroup<JsonObject> mGsonRequest = new MultiPartGroup<JsonObject>(
                Request.Method.PUT,
                Utils.API_URL + "api/Messenger/Group",
                JsonObject.class,
                headers,
                responseListener(isNew),
                errorListener(),
                file,
                ChatGroupUploadService.this, groupId, groupName, isNew) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }


    private Response.Listener<JsonObject> responseListener(final boolean isNew) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                if (!isNew) {
                    HttpHelper.NotificationGroup(getBaseContext(), TAG, mGroupId);
                }
                if (mGroupName != null) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.GROUP_COLUMN_GROUP_ID, mGroupId);
                    contentValues.put(DBHelper.GROUP_COLUMN_NAME, mGroupName);
                    contentValues.put(DBHelper.GROUP_COLUMN_IS_DELETE, String.valueOf(false));
                    if (HttpHelper.isGroupExist(getBaseContext(), mGroupId)) {
                        getBaseContext().getContentResolver().update(GroupProvider.CONTENT_URI, contentValues,
                                DBHelper.GROUP_COLUMN_GROUP_ID + "=?", new String[]{mGroupId});
                    } else {
                        getBaseContext().getContentResolver().insert(GroupProvider.CONTENT_URI, contentValues);
                    }
                    contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_LIST_COLUMN_GROUP_NAME, mGroupName);
                    getBaseContext().getContentResolver().update(ChatListProvider.CONTENT_URI, contentValues,
                            DBHelper.CHAT_LIST_COLUMN_GROUP_ID + "=?", new String[]{mGroupId});

                    contentValues = new ContentValues();
                    contentValues.put(DBHelper.CHAT_COLUMN_GROUP_NAME, mGroupName);
                    getBaseContext().getContentResolver().update(ChatProvider.CONTENT_URI, contentValues,
                            DBHelper.CHAT_COLUMN_GROUP_ID + "=?", new String[]{mGroupId});
                }
                if (mFilePath != null) {
                    HttpHelper.GetSingleGroup(getBaseContext(), TAG, mGroupId, false);
                }
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getBaseContext());
                Intent intent = new Intent(BackgroundXMPPService.ACTION);
                intent.putExtra("isBack", true);
                intent.putExtra("refresh", true);
                mgr.sendBroadcast(intent);
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(getBaseContext(), error);

                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getBaseContext());
                Intent intent = new Intent(BackgroundXMPPService.ACTION);
                intent.putExtra("isFail", true);
                mgr.sendBroadcast(intent);
            }
        };
    }
}
