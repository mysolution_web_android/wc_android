package com.applab.wcircle_pro.Chat.xmpp;

/**
 * Created by haoyi on 10/13/15.
 */
public interface IncomingMessage {
    public void onUpdate(long value);
}
