package com.applab.wcircle_pro.Chat.model;

/**
 * Created by user on 24/11/2015.
 */

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Group {

    @SerializedName("member")
    @Expose
    private List<String> member = new ArrayList<String>();
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("isimage")
    @Expose
    private Boolean isimage;
    @SerializedName("isgroup")
    @Expose
    private Boolean isgroup;
    @SerializedName("LastUpdate")
    @Expose
    private String LastUpdate;
    @SerializedName("PageNo")
    @Expose
    private Integer PageNo;
    @SerializedName("NoPerPage")
    @Expose
    private Integer NoPerPage;
    @SerializedName("NoOfRecords")
    @Expose
    private Integer NoOfRecords;
    @SerializedName("NoOfPage")
    @Expose
    private Integer NoOfPage;
    @SerializedName("items")
    @Expose
    private List<Member> items = new ArrayList<Member>();
    @SerializedName("Id")
    @Expose
    private String Id;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Image")
    @Expose
    private String Image;
    @SerializedName("CreateBy")
    @Expose
    private String CreateBy;
    @SerializedName("CreateDate")
    @Expose
    private String CreateDate;
    @SerializedName("UpdateBy")
    @Expose
    private String UpdateBy;
    @SerializedName("UpdateDate")
    @Expose
    private String UpdateDate;
    @SerializedName("groupname")
    @Expose
    private String groupName;
    @SerializedName("groupid")
    @Expose
    private String groupId;
    @SerializedName("msgfrom")
    @Expose
    private String from;
    @SerializedName("msgto")
    @Expose
    private String to;
    @SerializedName("isdelete")
    @Expose
    private Boolean isDelete;
    @SerializedName("isgroupupdate")
    @Expose
    private Boolean isGroupUpdate;
    @SerializedName("senddate")
    @Expose
    private String sendDate;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String setSendDate) {
        this.sendDate = sendDate;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsGroupUpdate() {
        return isGroupUpdate;
    }

    public void setIsGroupUpdate(Boolean isGroupUpdate) {
        this.isGroupUpdate = isGroupUpdate;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * @return The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return The Image
     */
    public String getImage() {
        return Image;
    }

    /**
     * @param Image The Image
     */
    public void setImage(String Image) {
        this.Image = Image;
    }

    /**
     * @return The CreateBy
     */
    public String getCreateBy() {
        return CreateBy;
    }

    /**
     * @param CreateBy The CreateBy
     */
    public void setCreateBy(String CreateBy) {
        this.CreateBy = CreateBy;
    }

    /**
     * @return The CreateDate
     */
    public String getCreateDate() {
        return CreateDate;
    }

    /**
     * @param CreateDate The CreateDate
     */
    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    /**
     * @return The UpdateBy
     */
    public String getUpdateBy() {
        return UpdateBy;
    }

    /**
     * @param UpdateBy The UpdateBy
     */
    public void setUpdateBy(String UpdateBy) {
        this.UpdateBy = UpdateBy;
    }

    /**
     * @return The UpdateDate
     */
    public String getUpdateDate() {
        return UpdateDate;
    }

    /**
     * @param UpdateDate The UpdateDate
     */
    public void setUpdateDate(String UpdateDate) {
        this.UpdateDate = UpdateDate;
    }


    /**
     * @return The LastUpdate
     */
    public String getLastUpdate() {
        return LastUpdate;
    }

    /**
     * @param LastUpdate The LastUpdate
     */
    public void setLastUpdate(String LastUpdate) {
        this.LastUpdate = LastUpdate;
    }

    /**
     * @return The PageNo
     */
    public Integer getPageNo() {
        return PageNo;
    }

    /**
     * @param PageNo The PageNo
     */
    public void setPageNo(Integer PageNo) {
        this.PageNo = PageNo;
    }

    /**
     * @return The NoPerPage
     */
    public Integer getNoPerPage() {
        return NoPerPage;
    }

    /**
     * @param NoPerPage The NoPerPage
     */
    public void setNoPerPage(Integer NoPerPage) {
        this.NoPerPage = NoPerPage;
    }

    /**
     * @return The NoOfRecords
     */
    public Integer getNoOfRecords() {
        return NoOfRecords;
    }

    /**
     * @param NoOfRecords The NoOfRecords
     */
    public void setNoOfRecords(Integer NoOfRecords) {
        this.NoOfRecords = NoOfRecords;
    }

    /**
     * @return The NoOfPage
     */
    public Integer getNoOfPage() {
        return NoOfPage;
    }

    /**
     * @param NoOfPage The NoOfPage
     */
    public void setNoOfPage(Integer NoOfPage) {
        this.NoOfPage = NoOfPage;
    }

    /**
     * @return The items
     */
    public List<Member> getMembers() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setMembers(List<Member> items) {
        this.items = items;
    }

    public Boolean getIsimage() {
        return isimage;
    }

    public void setIsimage(Boolean isimage) {
        this.isimage = isimage;
    }

    public Boolean getIsgroup() {
        return isgroup;
    }

    public void setIsgroup(Boolean isgroup) {
        this.isgroup = isgroup;
    }

    /**
     * @return The member
     */
    public List<String> getMember() {
        return member;
    }

    /**
     * @param member The member
     */
    public void setMember(List<String> member) {
        this.member = member;
    }

    /**
     * @return The owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner The owner
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    public static Group getGroup(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Group group = new Group();
        group.setId(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_COLUMN_GROUP_ID)));
        group.setName(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_COLUMN_NAME)));
        group.setImage(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_COLUMN_ICON)));
        return group;
    }

}