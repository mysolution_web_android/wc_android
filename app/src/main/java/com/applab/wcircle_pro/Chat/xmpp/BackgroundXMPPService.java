package com.applab.wcircle_pro.Chat.xmpp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.applab.wcircle_pro.Chat.ChartRoomGroupActivity;
import com.applab.wcircle_pro.Chat.ChatActivity;
import com.applab.wcircle_pro.Chat.ChatRoomActivity;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.HttpHelper;
import com.applab.wcircle_pro.Chat.model.MessageModel;
import com.applab.wcircle_pro.Chat.xmpp.carbons.Carbon;
import com.applab.wcircle_pro.Chat.xmpp.carbons.CarbonManager;
import com.applab.wcircle_pro.Chat.xmpp.carbons.CarbonManagerProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.bytestreams.ibb.provider.CloseIQProvider;
import org.jivesoftware.smackx.bytestreams.ibb.provider.OpenIQProvider;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;
import org.jivesoftware.smackx.forward.packet.Forwarded;
import org.jivesoftware.smackx.forward.provider.ForwardedProvider;
import org.jivesoftware.smackx.muc.Affiliate;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.si.provider.StreamInitiationProvider;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.leolin.shortcutbadger.ShortcutBadger;


/**
 * Created by haoyi on 10/8/15.
 */

public class BackgroundXMPPService extends Service implements MessageListener {
    public static LocalBroadcastManager mgr;
    public final static String ACTION = "CHAT_ACTION";
    public static String TAG = "BackgroundXMPPService";
    public AbstractXMPPConnection connection = null;
    public static MessageListener messageListener;
    private String usernameSave;
    private String usernameTempSave;
    private String passwordTempSave;
    private ChatDBHelper db;
    private OnMessageIncominglistener mOnMessageIncominglistener;
    private SharedPreferences sharedPreferences;
    private BackgroundXMPPServiceConnection backgroundXMPPServiceConnection;
    private BackgroundXMPPService backgroundXMPPService;
    public ArrayList<String> arrUserInGroup = new ArrayList<>();
    public String groupId;

    public void setArrUserInGroup(ArrayList<String> arrUserInGroup, String groupId) {
        this.arrUserInGroup = arrUserInGroup;
        this.groupId = groupId;
        if (arrUserInGroup.size() > 0) {
            this.arrUserInGroup.add(Utils.getProfile(getBaseContext()).getOXUser());
        }
    }

    private void initializeLoginXMPP() {
        backgroundXMPPServiceConnection = new BackgroundXMPPServiceConnection();
        Intent intent = new Intent(getBaseContext(), BackgroundXMPPService.class);
        bindService(intent, backgroundXMPPServiceConnection, BIND_ADJUST_WITH_ACTIVITY);
        SharedPreferences sharedPreferences = getSharedPreferences("OWNCLOUD PASSWORD", 0);
        String password = sharedPreferences.getString("own_password", null);
        if (password != null) {
            String error = backgroundXMPPService.LoginAndSetting(Utils.getProfile(
                    getBaseContext()).getOXUser(), password);
            if (!error.equals("ok")) {
                android.os.Message msg = loginErrorHandler.obtainMessage();
                loginErrorHandler.handleMessage(msg);
            }
        }
    }

    //region xmpp connection region
    public class BackgroundXMPPServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            backgroundXMPPService = ((BackgroundXMPPService.LocalBinder) binder).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // TODO Auto-generated method stub
            backgroundXMPPService = null;
            unbindService(backgroundXMPPServiceConnection);
        }
    }

    public void setOnMessageIncominglistener(OnMessageIncominglistener mOnMessageIncominglistener) {
        this.mOnMessageIncominglistener = mOnMessageIncominglistener;
    }

    @Override
    public IBinder onBind(Intent intent) {
        connection = XMPPConn.getInstance().getConn();


        return new LocalBinder();
    }

    @Override
    public void onRebind(Intent intent) {

    }

    @Override
    public boolean onUnbind(Intent intent) {
        return true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        db = new ChatDBHelper(this);
        backgroundXMPPService = this;
        setupXMPP();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public ComponentName startService(Intent service) {
        return super.startService(service);
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        mgr = LocalBroadcastManager.getInstance(this);
        messageListener = this;
    }

    public final class LocalBinder extends Binder {
        public BackgroundXMPPService getService() {
            return BackgroundXMPPService.this;
        }
    }

    // setup to msg receiver
    public void setConnection() {
        //set message listner
        setMessageIncomengConnection();
        //set file listner
        setFileIncomingConnection();

        //set multi user chat
        setMultiUsetChatConnection();
    }

    private void setupXMPP() {
        // must run in thread else got error
        Thread thread = new Thread(new Runnable() {
            public void run() {
                boolean makeConnectionSuccess = buildConnection();

                if (makeConnectionSuccess) {

                    ProviderManager.addIQProvider("si", "http://jabber.org/protocol/si", new StreamInitiationProvider());
                    ProviderManager.addIQProvider("query", "http://jabber.org/protocol/bytestreams", new BytestreamsProvider());
                    ProviderManager.addIQProvider("open", "http://jabber.org/protocol/ibb", new OpenIQProvider());
                    ProviderManager.addIQProvider("close", "http://jabber.org/protocol/ibb", new CloseIQProvider());
                    ProviderManager.addExtensionProvider("forwarded", Forwarded.NAMESPACE, new ForwardedProvider());
                    ProviderManager.addExtensionProvider("sent", Carbon.NAMESPACE, new CarbonManagerProvider());
                    ProviderManager.addExtensionProvider("received", Carbon.NAMESPACE, new CarbonManagerProvider());
                    XMPPConn.getInstance().setXMPPConn(connection);

                    android.os.Message msg = connFinishHandler.obtainMessage();
                    connFinishHandler.handleMessage(msg);

                } else {
                    android.os.Message msg = connectionErrorHandler.obtainMessage();
                    connectionErrorHandler.handleMessage(msg);
                }
            }
        });
        thread.start();
    }

    public String LoginAndSetting(String username, String password) {
        String errorMsg = "ok";
        usernameTempSave = username;
        passwordTempSave = password;

        boolean loginSuccess = userlogin();        //user login

        if (loginSuccess) {
            usernameSave = usernameTempSave;

            setConnection();    //receiver register
            keepAlive();        //ping manager register

            sharedPreferences = getSharedPreferences(Config.SHARED_PREFERENCES_NAME, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("username", usernameTempSave);
            editor.putString("password", passwordTempSave);
            editor.apply();

            android.os.Message msg = loginFinishHandler.obtainMessage();
            loginFinishHandler.handleMessage(msg);
        } else {
            errorMsg = "Error password or username";
        }
        return errorMsg;
    }

    Handler connFinishHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(ACTION);
                    intent.putExtra("connection_available", true);
                    mgr.sendBroadcast(intent);
                    try {
                        String user = connection.getUser();
                        if (user == null) {
                            initializeLoginXMPP();
                        }
                    } catch (Exception ex) {
                        ex.fillInStackTrace();
                    }
                }
            }, 1000);

        }
    };

    public void setStatusFromConfig() {
        // TODO: only call this when carbons changed, not on every presence change
        try {
            CarbonManager.getInstanceFor(connection).enableCarbons();
            Presence presence = new Presence(Presence.Type.available);
            connection.sendStanza(presence);
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    Handler loginFinishHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            setStatusFromConfig();
            Intent intent = new Intent(ACTION);
            intent.putExtra("login_available", true);
            mgr.sendBroadcast(intent);
        }
    };

    Handler loginErrorHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Intent intent = new Intent(ACTION);
            intent.putExtra("login_error", true);
            mgr.sendBroadcast(intent);
        }
    };

    Handler connectionErrorHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Intent intent = new Intent(ACTION);
            intent.putExtra("connection_error", true);
            mgr.sendBroadcast(intent);
        }
    };

    public boolean buildConnection() {
        //setup connection config
        XMPPTCPConnectionConfiguration.Builder configBuilder =
                XMPPTCPConnectionConfiguration.builder();
        configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);

        configBuilder.setResource(Config.RESOURCE);
        configBuilder.setServiceName(Config.SERVICE);
        configBuilder.setHost(Config.HOST);
        configBuilder.setPort(Config.PORT);
        configBuilder.setDebuggerEnabled(true);


        connection = new XMPPTCPConnection(configBuilder.build());

        try {
            connection.connect();
        } catch (XMPPException e) {
            e.printStackTrace();
            connection = null;
            return false;
        } catch (SmackException e) {
            e.printStackTrace();
            connection = null;
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            connection = null;
            return false;
        }

        return true;
    }

    //user login
    public boolean userlogin() {
        boolean loginSuccess = true;
        if (connection != null) {
            //change user login username check

            try {
                boolean isConnected = connection.isConnected();

                if (isConnected) {
                    SASLAuthentication.unBlacklistSASLMechanism("PLAIN");
                    SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");

                    connection.login(usernameTempSave, passwordTempSave);
                } else {
                    SASLAuthentication.unBlacklistSASLMechanism("PLAIN");
                    SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");

                    connection.connect().login(usernameTempSave, passwordTempSave);
                }

                loginSuccess = true;
            } catch (SmackException e) {
                loginSuccess = false;
                e.printStackTrace();
                Log.i(TAG, "Failed 1 : " + e.getMessage());
            } catch (IOException e) {
                loginSuccess = false;
                e.printStackTrace();
                Log.i(TAG, "Failed 2 : " + e.getMessage());
            } catch (XMPPException e) {
                loginSuccess = false;
                e.printStackTrace();
                Log.i(TAG, "Failed 3 : " + e.getMessage());
            }
        }
        return loginSuccess;
    }

    public void keepAlive() {
        PingManager pm = PingManager.getInstanceFor(connection);
        pm.setPingInterval(60); //ping every second
        try {
            pm.pingMyServer();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
        pm.registerPingFailedListener(new PingFailedListener() {
            @Override
            public void pingFailed() {
                Log.e(TAG, "pingFailed");
            }
        });
    }

    public static void showNotification(Context context, MessageModel messageModel, String employeeName, Integer id) {
        Intent intent = new Intent(context, ChatActivity.class);

        if (messageModel.getIsGroup()) {
            intent = new Intent(context, ChartRoomGroupActivity.class);
            intent.putExtra("groupId", messageModel.getGroupId());
            intent.putExtra("groupName", messageModel.getGroupName());
        } else if (messageModel.getType().equalsIgnoreCase("chat")) {
            intent = new Intent(context, ChatRoomActivity.class);
            if (employeeName != null) {
                intent.putExtra("employeeName", employeeName);
            } else {
                intent.putExtra("employeeName", messageModel.getFrom());
            }

            intent.putExtra("friendId", messageModel.getFrom());
            intent.putExtra("selfId", Utils.getProfile(context).getOXUser());
        }
        Uri alarmSound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notification);
        PendingIntent pi = PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        Resources r = context.getResources();
        String name = messageModel.getFrom();
        String message = messageModel.getIsImage() ? "[Image]" : messageModel.getMessage();
        if (employeeName != null) {
            if (!employeeName.equals("")) {
                name = employeeName;
            }
        }
        if (messageModel.getIsGroup()) {
            name = messageModel.getGroupName();
            if (employeeName != null) {
                if (!employeeName.equals("")) {
                    message = employeeName + ": " + (messageModel.getIsImage() ? "[Image]" : messageModel.getMessage());
                } else {
                    message = messageModel.getFrom() + ": " + (messageModel.getIsImage() ? "[Image]" : messageModel.getMessage());
                }
            } else {
                message = messageModel.getFrom() + ": " + (messageModel.getIsImage() ? "[Image]" : messageModel.getMessage());
            }
        }
        Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle(name)
                .setSmallIcon(R.mipmap.icon)
                .setContentText(message)
                .setContentIntent(pi)
                .setAutoCancel(true)
                //.setSound(alarmSound)
                .setVibrate(new long[]{1000, 1000})
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(id, notification);
        int total = com.applab.wcircle_pro.Dashboard.HttpHelper.getTotalNotification(context);
        if (total > 0) {
            ShortcutBadger.applyCount(context, 1);
        } else {
            ShortcutBadger.removeCount(context);
        }
    }

    private void setFileIncomingConnection() {
        final FileTransferManager manager = FileTransferManager.getInstanceFor(connection);
        // Create the listener
        manager.addFileTransferListener(new FileTransferListener() {
            public void fileTransferRequest(FileTransferRequest request) {
                // Check to see if the request should be accepted
                Log.e("hello", "get request");

                // Accept it
                IncomingFileTransfer transfer = request.accept();

                String filename = transfer.getFileName();

                try {
                    transfer.recieveFile(new File("/storage/removable/sdcard1/" + filename));
                    System.out.println("File " + filename + "Received Successfully");
                    //InputStream input = transfer.recieveFile();
                } catch (SmackException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setMessageIncomengConnection() {
        ChatManager chatManager = ChatManager.getInstanceFor(connection);
        chatManager.addChatListener(new ChatManagerListener() {
            @Override
            public void chatCreated(Chat chat, boolean createdLocally) {
                chat.addMessageListener(new org.jivesoftware.smack.chat.ChatMessageListener() {
                    @Override
                    public void processMessage(Chat chat, Message message) {
                        proccessMessage(message);
                    }
                });
            }
        });
    }

    private void setMultiUsetChatConnection() {
        MultiUserChatManager.getInstanceFor(connection).addInvitationListener(new InvitationListener() {
            @Override
            public void invitationReceived(XMPPConnection conn, MultiUserChat room, String inviter, String reason, String password, Message message) {

                try {
                    //get the room name and join the room
                    String[] spliteRoomName = room.getRoom().split("@");
                    String roomName = spliteRoomName[0];
                    room.join(usernameSave);
                    List<Affiliate> roomOwners = room.getOwners();
                    for (Affiliate owner : roomOwners) {
                        String j_id = owner.getJid();
                    }

                    //save the MultiUserChat
                    XMPPConn.getInstance().setMUC(roomName, room);
                    room.addMessageListener(BackgroundXMPPService.this);

                    //save the name of room to reenter for lost connection;
                    SharedPreferences sharedPreferencesGroup = getSharedPreferences(Config.SHARED_PREFERENCES_GROUP, 0);
                    SharedPreferences.Editor editor = sharedPreferencesGroup.edit();
                    editor.putString(roomName, roomName);
                    editor.apply();
                } catch (SmackException.NoResponseException e) {
                    e.printStackTrace();
                } catch (XMPPException.XMPPErrorException e) {
                    e.printStackTrace();
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }

        });
    }

    public void processMessage(Message message) {
        Log.e("message", message.getBody());
        proccessMessage(message);
    }

    private void proccessMessage(Message message) {
        //donot parse wrong message
        String message_type = message.getType().toString();
        if (message_type.equalsIgnoreCase("normal")) {
            return;
        }
        MessageModel messageModel = MessageProcessing.getMessageDetail(Utils.getProfile(getBaseContext()).getOXUser(), message);
        boolean isMessageExists = false;
        //if is group chat need to check is message exists
        //else do not need to check
        if (messageModel.getIsGroup()) {
            isMessageExists = ChatDBHelper.isMessageExists(message.getStanzaId(), getBaseContext());
        } else {

            isMessageExists = false;
        }

        if (isMessageExists) {

        } else {
            if (messageModel.getMessage() != null) {
                ChatDBHelper.addMessage(messageModel, getBaseContext());
                Integer id = ChatDBHelper.addLastMsg(messageModel, getBaseContext());
                Log.i(TAG, "Message received");

                // updated current chat room and recent chat list
                if (mOnMessageIncominglistener != null) {
                    //pass to current chat room
                    mOnMessageIncominglistener.onMessangeIncoming(messageModel, messageModel.getFrom());
                }
                int extra = Integer.valueOf(messageModel.getExtra());
                if (messageModel.getIsGroup()) {
                    if (groupId == null) {
                        switch (extra) {
                            case MessageModel.TEXT_MESSAGE: {
                                if (!messageModel.isMine()) {
                                    if (ChatDBHelper.isGroupMemberExists(getBaseContext(), messageModel.getGroupId(), messageModel.getFrom())) {
                                        showNotification(getBaseContext(), messageModel, ChatDBHelper.getEmployeeName(getBaseContext(),
                                                messageModel.getFrom()), id);
                                    } else {
                                        HttpHelper.getEmployeeGson(getBaseContext(), messageModel, TAG, id);
                                    }
                                }
                                break;
                            }
                        }
                    } else {
                        if (groupId.equals(messageModel.getGroupId())) {
                            if (ChatDBHelper.isGroupMemberExists(getBaseContext(), groupId, messageModel.getFrom())) {
                                HttpHelper.getEmployeeGroupGson(getBaseContext(), TAG, messageModel.getFrom(), groupId, null);
                            }
                        } else {
                            switch (extra) {
                                case MessageModel.TEXT_MESSAGE: {
                                    if (!messageModel.isMine()) {
                                        if (ChatDBHelper.isGroupMemberExists(getBaseContext(), messageModel.getGroupId(), messageModel.getFrom())) {
                                            showNotification(getBaseContext(), messageModel, ChatDBHelper.getEmployeeName(getBaseContext(),
                                                    messageModel.getFrom()), id);
                                        } else {
                                            HttpHelper.getEmployeeGson(getBaseContext(), messageModel, TAG, id);
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    if (!arrUserInGroup.contains(messageModel.getFrom())) {
                        switch (extra) {
                            case MessageModel.TEXT_MESSAGE: {
                                if (!messageModel.isMine()) {
                                    if (ChatDBHelper.getEmployeeName(getBaseContext(), messageModel.getFrom()) != null) {
                                        showNotification(getBaseContext(), messageModel, ChatDBHelper.getEmployeeName(getBaseContext(), messageModel.getFrom()), id);
                                    } else {
                                        HttpHelper.getEmployeeGson(getBaseContext(), messageModel, TAG, id);
                                    }
                                }
                                break;
                            }
                        }
                    } else {
                        if (ChatDBHelper.getEmployeeName(getBaseContext(), messageModel.getFrom()) == null) {
                            HttpHelper.getEmployeeGroupGson(getBaseContext(), TAG, messageModel.getFrom(), null, null);
                        }
                    }
                }
            }
        }
    }

    public static ArrayList<String> sendMessage(final Context context, String body, String friendId, int isGroup, int isImage,
                                                String groupName, String groupId, String TAG, LoaderManager loaderManager,
                                                LoaderManager.LoaderCallbacks callbacks, int loaderId, boolean isEmpty) {
        AbstractXMPPConnection connection = XMPPConn.getInstance().getConn();
        SharedPreferences pref;
        pref = context.getSharedPreferences("Server Date", 0);
        String serverDate = Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, new Date()));
        if (pref.getLong("server_date", 0) != 0) {
            if (pref.getBoolean("isAfter", false)) { // server date > current phone date
                Date date = new Date();
                date.setTime(date.getTime() + pref.getLong("server_date", 0));
                serverDate = Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, date));
            } else {
                Date date = new Date();
                date.setTime(date.getTime() - pref.getLong("server_date", 0));
                serverDate = Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, date));
            }
        }
        if (Utils.isConnectingToInternet(context)) {
            if (connection != null) {
                if (connection.isConnected()) {
                    if (connection.getUser() != null) {
                        if (!body.equals("")) {
                            ChatManager chatmanager = ChatManager.getInstanceFor(connection);
                            //org.jivesoftware.smack.chat.Chat newChat = chatmanager.createChat((isGroup == 0 ? friendId : groupId) + "@" + Config.SERVER_IP);
                            org.jivesoftware.smack.chat.Chat newChat = chatmanager.createChat("robot@" + Config.SERVER_IP);
                            String messageBody = body;
                            Message message = new Message();
                            message.setBody(messageBody);
                            boolean group = isGroup == 1;
                            boolean image = isImage == 1;
                            String to = isGroup == 0 ? friendId : groupId;
                            message.setSubject("{\"isgroup\":" + String.valueOf(group) + ",\"isimage\":" + String.valueOf(image) +
                                    ",\"groupname\":\"" + groupName + "\",\"groupid\":\"" + groupId +
                                    "\",\"msgfrom\":\"" + Utils.getProfile(context).getOXUser() + "\",\"senddate\":\"" + serverDate + "\",\"msgto\":\"" + to + "\"}");
                            try {
                                /*if (isGroup == 0) {
                                    newChat.sendMessage(message);
                                }*/
                                newChat.sendMessage(message);
                                message.setFrom(Utils.getProfile(context).getOXUser() + "@" + Config.SERVER_IP);
                                MessageModel messageModel = MessageProcessing.getMessageDetail(Utils.getProfile(context).getOXUser(), message);
                                /*if (isGroup == 1) {
                                    if (!isEmpty) {
                                        HttpHelper.SendMessageGroup(context, TAG, groupId, isGroup, isImage, messageModel.getThreadId(), body, groupName);
                                    }
                                }*/
                                /*if (isImage == 0) {
                                    ChatDBHelper.addMessage(messageModel, context);
                                    ChatDBHelper.addLastMsg(messageModel, context);
                                }*/
                                ChatDBHelper.addMessage(messageModel, context);
                                ChatDBHelper.addLastMsg(messageModel, context);
                                if (loaderManager != null) {
                                    loaderManager.restartLoader(loaderId, null, callbacks);
                                }
                                ArrayList<String> arr = new ArrayList<>();
                                arr.add(messageModel.getThreadId());
                                arr.add(serverDate);
                                return arr;
                            } catch (SmackException.NotConnectedException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        connection.disconnect();
                        Utils.showError(context, Utils.CODE_SEND_MESSAGE_FAILED, Utils.SEND_MESSAGE_FAILED);
                        context.startService(new Intent(context, BackgroundXMPPService.class));
                    }

                } else {
                    connection.disconnect();
                    Utils.showError(context, Utils.CODE_SEND_MESSAGE_FAILED, Utils.SEND_MESSAGE_FAILED);
                    context.startService(new Intent(context, BackgroundXMPPService.class));
                }
            } else {
                Utils.showError(context, Utils.CODE_SEND_MESSAGE_FAILED, Utils.SEND_MESSAGE_FAILED);
                context.startService(new Intent(context, BackgroundXMPPService.class));
            }
        } else {
            Utils.showNoConnection(context);
        }
        return null;
    }

    public static String getThreadId(final Context context, String body, String friendId, int isGroup, int isImage,
                                     String groupName, String groupId, LoaderManager loaderManager,
                                     LoaderManager.LoaderCallbacks callbacks, int loaderId) {
        SharedPreferences pref;
        pref = context.getSharedPreferences("Server Date", 0);
        String serverDate = Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, new Date()));
        if (pref.getLong("server_date", 0) != 0) {
            if (pref.getBoolean("isAfter", false)) { // server date > current phone date
                Date date = new Date();
                date.setTime(date.getTime() + pref.getLong("server_date", 0));
                serverDate = Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, date));
            } else {
                Date date = new Date();
                date.setTime(date.getTime() - pref.getLong("server_date", 0));
                serverDate = Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, date));
            }
        }
        Message message = new Message();
        message.setBody(body);
        message.setFrom(Utils.getProfile(context).getOXUser() + "@" + Config.SERVER_IP);
        message.setTo((isGroup == 0 ? friendId : groupId) + "@" + Config.SERVER_IP);
        boolean group = isGroup == 1;
        boolean image = isImage == 1;
        message.setSubject("{\"isgroup\":" + String.valueOf(group) + ",\"isimage\":" + String.valueOf(image) +
                ",\"groupname\":\"" + groupName + "\",\"groupid\":\"" + groupId +
                "\",\"from\":\"" + Utils.getProfile(context).getOXUser() + "\",\"senddate\":\"" + serverDate + "\"}");
        MessageModel messageModel = MessageProcessing.getMessageDetail(Utils.getProfile(context).getOXUser(), message);
        ChatDBHelper.addMessage(messageModel, context);
        ChatDBHelper.addLastMsg(messageModel, context);
        if (loaderManager != null) {
            loaderManager.restartLoader(loaderId, null, callbacks);
        }
        return messageModel.getThreadId();
    }
}
