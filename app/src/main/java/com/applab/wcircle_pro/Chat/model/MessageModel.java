package com.applab.wcircle_pro.Chat.model;

import android.content.Context;
import android.database.Cursor;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by haoyi on 10/9/15.
 */
public class MessageModel {
    public static final int STATUS_MESSAGE_RECEIVE = 10;
    public static final int STATUS_MESSAGE_READ = 11;
    public static final int TEXT_MESSAGE = 0;
    public static final int IMAGE_MESSAGE = 1;


    long id;
    String from;
    String to;
    String time;
    String type;
    String extra;
    String threadId;

    private long employeeId;
    private String image;
    private String employeeName;
    private String countryImage;
    private String oxUser;
    private String contactNo;
    private String officeNo;
    private String position;
    private String roomId;
    private Boolean isImage = false;
    private Boolean isGroup = false;

    private String groupName;
    private String groupId;

    private Boolean isDelete = false;
    private Boolean isGroupUpdate = false;
    private String isSuccess = "pending";

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsGroupUpdate() {
        return isGroupUpdate;
    }

    public void setIsGroupUpdate(Boolean isGroupUpdate) {
        this.isGroupUpdate = isGroupUpdate;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Boolean getIsImage() {
        return isImage;
    }

    public void setIsImage(Boolean isImage) {
        this.isImage = isImage;
    }

    public Boolean getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(Boolean isGroup) {
        this.isGroup = isGroup;
    }

    public MessageModel() {
    }

    public static MessageModel getMessageModel(Context context, Cursor cursor, int position) {
        cursor.moveToPosition(position);
        try {
            MessageModel msg = new MessageModel();
            msg.setOxUser(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_OX_USER)));
            msg.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_POSITION)));
            msg.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_CONTACT_NO)));
            msg.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE)));
            msg.setEmployeeId(cursor.getInt(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID)));
            msg.setEmployeeName(cursor.getString(cursor.getColumnIndex(DBHelper.USER_CHAT_COLUMN_NAME)));
            msg.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_CHAT_ID)));
            msg.setFrom(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_FROM)));
            msg.setTo(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_TO)));
            msg.setTime(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_TIME)));
            msg.setType(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_MSG_TYPE)));
            msg.setExtra(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_EXTRA)));
            msg.setMessage(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_MSG)));
            msg.setThreadId(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_THREAD_ID)));
            msg.setRoomId(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_NAME)));
            if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_SUCCESS))) {
                msg.setIsSuccess(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_SUCCESS)));
            } else {
                msg.setIsSuccess("success");
            }
            boolean isGroup = false;
            if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_GROUP))) {
                String result = cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_GROUP));
                isGroup = result.equals("true");
            }
            boolean isImage = false;
            if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_IMAGE))) {
                String result = cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_IMAGE));
                isImage = result.equals("true");
            }
            boolean isDelete = false;
            if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_DELETE))) {
                String result = cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_DELETE));
                isDelete = result.equals("true");
            }
            boolean isGroupUpdate = false;
            if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_GROUP_UPDATE))) {
                String result = cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_GROUP_UPDATE));
                isGroupUpdate = result.equals("true");
            }
            msg.setIsImage(isImage);
            msg.setIsGroup(isGroup);
            msg.setIsDelete(isDelete);
            msg.setIsGroupUpdate(isGroupUpdate);
            msg.setMine(false);
            if (msg.getFrom().equals(Utils.getProfile(context).getOXUser())) {
                msg.setMine(true);
            }
            msg.setGroupId(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_GROUP_ID)));
            msg.setGroupName(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_GROUP_NAME)));
            return msg;
        } catch (Exception ex) {
            ex.fillInStackTrace();
            return null;
        }
    }

    public static MessageModel getForJson(Context context, Cursor cursor, int position) {
        cursor.moveToPosition(position);
        try {
            MessageModel msg = new MessageModel();
            msg.setFrom(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_FROM)));
            msg.setTo(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_TO)));
            msg.setTime(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_TIME)));
            msg.setType(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_MSG_TYPE)));
            msg.setExtra(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_EXTRA)));
            msg.setMessage(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_MSG)));
            msg.setThreadId(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_THREAD_ID)));
            if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_SUCCESS))) {
                msg.setIsSuccess(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_SUCCESS)));
            } else {
                msg.setIsSuccess("success");
            }
            boolean isGroup = false;
            if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_GROUP))) {
                String result = cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_GROUP));
                isGroup = result.equals("true");
            }
            boolean isImage = false;
            if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_IMAGE))) {
                String result = cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_IMAGE));
                isImage = result.equals("true");
            }
            boolean isDelete = false;
            if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_DELETE))) {
                String result = cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_DELETE));
                isDelete = result.equals("true");
            }
            boolean isGroupUpdate = false;
            if (!cursor.isNull(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_GROUP_UPDATE))) {
                String result = cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_IS_GROUP_UPDATE));
                isGroupUpdate = result.equals("true");
            }
            msg.setIsImage(isImage);
            msg.setIsGroup(isGroup);
            msg.setIsDelete(isDelete);
            msg.setIsGroupUpdate(isGroupUpdate);
            if (msg.getFrom().equals(Utils.getProfile(context).getOXUser())) {
                msg.setMine(true);
            }
            msg.setGroupId(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_GROUP_ID)));
            msg.setGroupName(cursor.getString(cursor.getColumnIndex(DBHelper.CHAT_COLUMN_GROUP_NAME)));
            return msg;
        } catch (Exception ex) {
            ex.fillInStackTrace();
            return null;
        }
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getCountryImage() {
        return countryImage;
    }

    public void setCountryImage(String countryImage) {
        this.countryImage = countryImage;
    }

    public String getOxUser() {
        return oxUser;
    }

    public void setOxUser(String oxUser) {
        this.oxUser = oxUser;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getOfficeNo() {
        return officeNo;
    }

    public void setOfficeNo(String officeNo) {
        this.officeNo = officeNo;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    enum MessageType {

    }

    public MessageModel(long id, String from, String to, String time, String type,
                        String extra, String message, String threadId) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.time = time;
        this.type = type;
        this.extra = extra;
        this.message = message;
        this.threadId = threadId;
    }

    public MessageModel(String threadId, String from, String to, String time, String type,
                        String extra, String message, boolean isMine, String groupId, String groupName, Boolean isImage, Boolean isGroup, Boolean isDelete, Boolean isGroupUpdate) {
        this.threadId = threadId;
        this.from = from;
        this.to = to;
        this.time = time;
        this.type = type;
        this.extra = extra;
        this.message = message;
        this.isMine = isMine;
        this.groupId = groupId;
        this.groupName = groupName;
        this.isImage = isImage;
        this.isGroup = isGroup;
        this.isDelete = isDelete;
        this.isGroupUpdate = isGroupUpdate;
    }

    //The content of the message
    String message;
    //boolean to determine, who is sender of this message
    boolean isMine;
    /**
     * boolean to determine, whether the message is a status message or not. it
     * reflects the changes/updates about the sender is writing, have entered
     * text etc
     */
    boolean isStatusMessage;


    //Constructor to make a Message object
    public MessageModel(String message, boolean isMine) {
        super();
        this.message = message;
        this.isMine = isMine;
        this.isStatusMessage = false;
    }

    /**
     * Constructor to make a status Message object consider the parameters are
     * swaped from default Message constructor, not a good approach but have to
     * go with it.
     */
    public MessageModel(boolean status, String message) {
        super();
        this.message = message;
        this.isMine = false;
        this.isStatusMessage = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isMine() {
        return isMine;
    }

    public void setMine(boolean isMine) {
        this.isMine = isMine;
    }

    public boolean isStatusMessage() {
        return isStatusMessage;
    }

    public void setStatusMessage(boolean isStatusMessage) {
        this.isStatusMessage = isStatusMessage;
    }


    public String getThreadId() {
        return threadId;
    }

    /**
     * @param threadId the id to set
     */
    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }


    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the extra
     */
    public String getExtra() {
        return extra;
    }

    /**
     * @param extra the extra to set
     */
    public void setExtra(String extra) {
        this.extra = extra;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Message [id=" + id + ", from=" + from + ", to=" + to
                + ", time=" + time + ", type=" + type + ", extra=" + extra
                + ", message=" + message + ", isMine=" + isMine
                + ", isStatusMessage=" + isStatusMessage + "]";
    }
}
