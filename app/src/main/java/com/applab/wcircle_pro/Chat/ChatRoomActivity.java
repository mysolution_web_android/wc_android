package com.applab.wcircle_pro.Chat;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.fragment.ChartRoomGroupFragment;
import com.applab.wcircle_pro.Chat.fragment.ChatRoomFragment;
import com.applab.wcircle_pro.Chat.model.User;
import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.Chat.xmpp.ChatUploadService;
import com.applab.wcircle_pro.Chat.xmpp.XMPPConn;
import com.applab.wcircle_pro.Employee.EmployeeCallDialogFragment;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.jivesoftware.smack.AbstractXMPPConnection;

import java.io.File;

public class ChatRoomActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ImageView imgPerson;
    private TextView txtToolbarTitle;
    private String employeeName, friendId;
    private User user;
    private RelativeLayout container;
    private static final int SELECT_PHOTO_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 101;
    private Uri fileUri;
    private File file;
    private int page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green_light));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);

        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setGravity(Gravity.LEFT);
        imgPerson = (ImageView) toolbar.findViewById(R.id.imgPerson);
        imgPerson.setVisibility(View.VISIBLE);
        imgPerson.setOnClickListener(imgPersonOnClickListener);

        txtToolbarTitle.setPadding(20, 0, 100, 0);
        container = (RelativeLayout) findViewById(R.id.container);

        Intent intent = getIntent();
        friendId = intent.getStringExtra("friendId");
        employeeName = intent.getStringExtra("employeeName");
        page = intent.getIntExtra("page", 0);
        user = intent.getParcelableExtra("user");
        txtToolbarTitle.setText(employeeName);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = ChatRoomFragment.newInstance(file);
        ft.replace(container.getId(), fragment);
        ft.commit();
    }

    private View.OnClickListener imgPersonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            User user = ChatDBHelper.getSingleUser(ChatRoomActivity.this, friendId);
            if (user != null) {
                if (user.getEmployeeName() != null) {
                    if (!user.getEmployeeName().equals("")) {
                        Utils.postRecentTrack(ChatRoomActivity.this, "call",
                                String.valueOf(user.getEmployeeId()));
                        EmployeeCallDialogFragment employeeCallDialogFragment =
                                EmployeeCallDialogFragment.newInstance(user, ChatRoomActivity.this);
                        employeeCallDialogFragment.show(getSupportFragmentManager(), "");
                    }
                }
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case SELECT_PHOTO_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    fileUri = imageReturnedIntent.getData();
                    File direct = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.folder_name) + "/" +
                            getResources().getString(R.string.messenger));
                    file = GroupActivity.previewSelectedImage(ChatRoomActivity.this,
                            imageReturnedIntent.getData(), null, false, false, 0, false, direct);
                    if (file != null) {
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        Fragment fragment = ChatRoomFragment.newInstance(file);
                        ft.replace(container.getId(), fragment);
                        ft.commit();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    break;
                } else {
                    Utils.showError(ChatRoomActivity.this, Utils.CODE_PROCEED_FAILED, Utils.PROCEED_FAILED);
                }
                break;
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    File direct = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.folder_name) + "/" +
                            getResources().getString(R.string.messenger));
                    file = GroupActivity.previewCapturedImage(ChatRoomActivity.this, fileUri, null, false, false, 0, false, direct);
                    if (file != null) {
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        Fragment fragment = ChatRoomFragment.newInstance(file);
                        ft.replace(container.getId(), fragment);
                        ft.commit();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    break;
                } else {
                    Utils.showError(ChatRoomActivity.this, Utils.CODE_PROCEED_FAILED, Utils.PROCEED_FAILED);
                }
                break;
        }
    }

    private boolean uploadImage(File file, String threadId) {
        boolean success = false;
        Intent intent = new Intent(this, ChatUploadService.class);
        if (file.length() < 3 * 1000000) {
            String[] result = file.getName().split("\\.");
            if (result[result.length - 1].equalsIgnoreCase("jpg") || result[result.length - 1].
                    equalsIgnoreCase("png") || result[result.length - 1].equalsIgnoreCase("jpeg")) {
                if (file != null) {
                    intent.putExtra("filePath", file.getAbsolutePath());
                    intent.putExtra("threadId", threadId);
                    success = true;
                } else {
                    Utils.showError(this, "", "The image is empty.");
                }
                intent.putExtra("oxUser", Utils.getProfile(this).getOXUser());
                this.startService(intent);
            } else {
                Utils.showError(this, "", "The image must be jpg or png.");
            }
        } else {
            Utils.showError(this, "", "The image is too large.");
        }
        return success;
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("page", page);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chart_room, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, ChatActivity.class);
            intent.putExtra("page", page);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.action_attach) {
            View v = findViewById(R.id.action_attach);
            PopupMenu popupMenu = new PopupMenu(toolbar.getContext(), v) {
                @Override
                public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_camera:
                            cameraClicked();
                            return true;
                        case R.id.action_gallery:
                            imageClicked();
                            return true;
                        default:
                            return super.onMenuItemSelected(menu, item);
                    }
                }
            };
            popupMenu.inflate(R.menu.menu_custom);
            popupMenu.getMenu().removeItem(R.id.action_folder);
            popupMenu.getMenu().removeItem(R.id.action_upload);
            popupMenu.getMenu().removeItem(R.id.action_new_chat);
            popupMenu.getMenu().removeItem(R.id.action_create_group);
            popupMenu.getMenu().removeItem(R.id.action_clear_employee);
            popupMenu.getMenu().removeItem(R.id.action_group_info);
            popupMenu.getMenu().removeItem(R.id.action_close_group);
            popupMenu.getMenu().removeItem(R.id.action_add_people);
            popupMenu.getMenu().removeItem(R.id.action_exit_group);
            popupMenu.getMenu().removeItem(R.id.action_delete_group);
            popupMenu.show();
            return true;
        } else if (id == R.id.action_call) {
            User user = ChatDBHelper.getSingleUser(ChatRoomActivity.this, friendId);
            if (user != null) {
                if (user.getEmployeeName() != null) {
                    if (!user.getEmployeeName().equals("")) {
                        Utils.postRecentTrack(ChatRoomActivity.this, "call",
                                String.valueOf(user.getEmployeeId()));
                        EmployeeCallDialogFragment employeeCallDialogFragment =
                                EmployeeCallDialogFragment.newInstance(user, ChatRoomActivity.this);
                        employeeCallDialogFragment.show(getSupportFragmentManager(), "");
                    }
                }
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void imageClicked() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO_REQUEST_CODE);
    }

    private void cameraClicked() {
        if (Utils.isDeviceSupportCamera(ChatRoomActivity.this)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.folder_name) + "/" +
                    getResources().getString(R.string.messenger));
            fileUri = GroupActivity.getOutputMediaFileUri(ChatRoomActivity.this, false, file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        } else {
            Utils.showError(ChatRoomActivity.this, "", "Sorry, you don't have supported camera.");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, ChatRoomActivity.this, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        User user = ChatDBHelper.getSingleUser(ChatRoomActivity.this, friendId);
        if (user != null) {
            String image = user.getImage() == null ? "" : user.getImage();
            Glide.with(this)
                    .load(image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transform(new CircleTransform(this))
                    .placeholder(R.mipmap.topmenu_user)
                    .into(imgPerson);
        }
    }
}
