package com.applab.wcircle_pro.Chat;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.db.HttpHelper;
import com.applab.wcircle_pro.Menu.NavigationDrawerFragment;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Tabs.SlidingTabLayout;
import com.applab.wcircle_pro.Utils.Utils;

public class ChatActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private String TAG = "CHAT";
    private ViewPager mPager;
    private SlidingTabLayout mTabs;
    private int page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green_light));
        TextView txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(this.getResources().getString(R.string.title_activity_chat));
        ImageView space1 = (ImageView) toolbar.findViewById(R.id.space1);
        space1.setVisibility(View.INVISIBLE);
        page = getIntent().getIntExtra("page", 0);
        drawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setSelectedPosition(5);
        drawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        toolbar.setNavigationIcon(R.mipmap.action_arrow_prev_white);
        toolbar.setNavigationOnClickListener(toolbarOnClickListener);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(1);
        mPager.addOnPageChangeListener(mPagerOnPageChangeListener);
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setmColor(1);
        mTabs.setDistributeEvenly(true);
        mTabs.setCustomTabView(R.layout.custom_tab_view, R.id.tabText, R.id.tabNumber);
        mTabs.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mTabs.setSelectedIndicatorColors(ContextCompat.getColor(this, R.color.color_green_light));
        mPager.setAdapter(new ChatPagerAdapter(getSupportFragmentManager(), this, getSupportLoaderManager(), mTabs, mPager));
        mTabs.setViewPager(mPager);
        mPager.setCurrentItem(page);
        HttpHelper.GetServerDate(this, TAG);
    }

    private ViewPager.OnPageChangeListener mPagerOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            page = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private View.OnClickListener toolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utils.clearPreviousActivity(ChatActivity.this);
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        Utils.clearPreviousActivity(ChatActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerFragment.mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            Utils.clearPreviousActivity(ChatActivity.this);
            return true;
        } else if (item.getItemId() == R.id.action_menu) {
            View menuItemView = findViewById(R.id.action_menu);
            PopupMenu popupMenu = new PopupMenu(toolbar.getContext(), menuItemView) {
                @Override
                public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_new_chat:
                            Intent intent = new Intent(ChatActivity.this, AddGroupMemberActivity.class);
                            intent.putExtra("isSelectSingle", true);
                            startActivity(intent);
                            finish();
                            return true;
                        case R.id.action_create_group:
                            startActivity(new Intent(ChatActivity.this, GroupActivity.class));
                            finish();
                            return true;
                        default:
                            return super.onMenuItemSelected(menu, item);
                    }
                }
            };
            popupMenu.inflate(R.menu.menu_custom);
            popupMenu.getMenu().removeItem(R.id.action_folder);
            popupMenu.getMenu().removeItem(R.id.action_upload);
            popupMenu.getMenu().removeItem(R.id.action_clear_employee);
            popupMenu.getMenu().removeItem(R.id.action_camera);
            popupMenu.getMenu().removeItem(R.id.action_gallery);
            popupMenu.getMenu().removeItem(R.id.action_close_group);
            popupMenu.getMenu().removeItem(R.id.action_group_info);
            popupMenu.getMenu().removeItem(R.id.action_add_people);
            popupMenu.getMenu().removeItem(R.id.action_exit_group);
            popupMenu.getMenu().removeItem(R.id.action_delete_group);
            popupMenu.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
    }
}
