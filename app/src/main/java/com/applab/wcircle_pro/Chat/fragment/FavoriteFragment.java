package com.applab.wcircle_pro.Chat.fragment;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.*;
import com.applab.wcircle_pro.Chat.AddGroupMemberActivity;
import com.applab.wcircle_pro.Chat.ChatRoomActivity;
import com.applab.wcircle_pro.Chat.adapter.FavoriteAdapter;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.HttpHelper;
import com.applab.wcircle_pro.Chat.db.UserChatProvider;
import com.applab.wcircle_pro.Chat.model.NewMember;
import com.applab.wcircle_pro.Employee.EmployeeActivity;
import com.applab.wcircle_pro.Favorite.Contact;
import com.applab.wcircle_pro.Favorite.ContactProvider;
import com.applab.wcircle_pro.Profile.Profile;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;

public class FavoriteFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    //region variable
    private RecyclerView recyclerView;
    private FavoriteAdapter adapter;
    private Cursor cursor;
    private EditText ediSearch;
    private LoaderManager.LoaderCallbacks callbacks;
    private Intent mIntent;
    private LinearLayout btnCancel, btnSubmit;
    private ArrayList<NewMember> addedEmployee = new ArrayList<>();
    private ArrayList<String> addedId = new ArrayList<>();
    private Profile profile;
    private String selection = DBHelper.CONTACT_COLUMN_ID + "!=?";
    private String[] selectionArgs;
    private String TAG = "MY FAVORITE FRAGMENT";
    private int pageNo = 1;
    private String groupId;
    private boolean isAdd = false;
    private ArrayList<NewMember> prevAddedEmployee = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private String filePath;
    private boolean isSelectSingle = false;
    private LinearLayout lvBtn;
    private View divider;
    private TextView txtError;
    //endregion

    //region create
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_my_favorite, container, false);
        getActivity().getSupportLoaderManager().initLoader(0, null, this);
        profile = Utils.getProfile(getActivity());
        selectionArgs = new String[]{String.valueOf(profile.getId())};
        callbacks = this;
        mIntent = getActivity().getIntent();
        groupId = mIntent.getStringExtra("groupId");
        isSelectSingle = mIntent.getBooleanExtra("isSelectSingle", false);
        isAdd = mIntent.getBooleanExtra("isAdd", false);
        filePath = mIntent.getStringExtra("filePath");
        prevAddedEmployee = mIntent.getParcelableArrayListExtra("prevAddedEmployee");
        if (mIntent.getParcelableArrayListExtra("addedEmployee") != null) {
            addedEmployee = mIntent.getParcelableArrayListExtra("addedEmployee");
            for (int i = 0; i < addedEmployee.size(); i++) {
                NewMember employee = addedEmployee.get(i);
                addedId.add(employee.getOxUser());
            }
        }
        ediSearch = (EditText) layout.findViewById(R.id.ediSearch);
        txtError = (TextView) layout.findViewById(R.id.txtError);
        ediSearch.setOnEditorActionListener(ediSearchOnEditActionListener);

        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        adapter = new FavoriteAdapter(getActivity(), cursor, addedEmployee, addedId, isSelectSingle);
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);

        lvBtn = (LinearLayout) layout.findViewById(R.id.lvBtn);
        divider = (View) layout.findViewById(R.id.divider);
        lvBtn.setVisibility(isSelectSingle ? View.GONE : View.VISIBLE);
        divider.setVisibility(isSelectSingle ? View.GONE : View.VISIBLE);
        btnCancel = (LinearLayout) layout.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        btnSubmit = (LinearLayout) layout.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(btnSubmitOnClickListener);
        setOnTouchListener();
        HttpHelper.getFavoriteGson(getActivity(), pageNo, TAG, txtError);
        return layout;
    }
    //endregion

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (totalItemCount > 1) {
                if (lastVisibleItem >= totalItemCount - 1) {
                    pageNo++;
                    HttpHelper.getFavoriteGson(getActivity(), pageNo, TAG, txtError);
                }
            }
        }
    };

    //region on click region
    private LinearLayout.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                if (!isAdd) {
                    if (addedEmployee.size() > 0) {
                        if (groupId != null) {
                            HttpHelper.CreateNewGroupGroup(getActivity(), TAG, groupId, addedEmployee, filePath);
                        } else {
                            getActivity().finish();
                        }
                    } else {
                        if (groupId != null) {
                            HttpHelper.CreateNewGroupGroup(getActivity(), TAG, groupId, addedEmployee, filePath);
                        } else {
                            getActivity().finish();
                        }
                    }
                } else {
                    ArrayList<NewMember> removeUser = new ArrayList<>();
                    for (int j = 0; j < prevAddedEmployee.size(); j++) {
                        if (!addedId.contains(prevAddedEmployee.get(j).getOxUser())) {
                            removeUser.add(prevAddedEmployee.get(j));
                        }
                    }
                    for (int z = 0; z < addedEmployee.size(); z++) {
                        for (int k = 0; k < prevAddedEmployee.size(); k++) {
                            if (addedEmployee.get(z).getOxUser().equals(prevAddedEmployee.get(k).getOxUser())) {
                                addedEmployee.remove(z);
                            }
                        }
                    }
                    HttpHelper.PostNewMemberGroup(getActivity(), TAG, groupId, addedEmployee, removeUser);
                    getActivity().finish();
                }
            } catch (Exception ex) {
                Log.i("ERROR", "Content Provider Error :" + ex.toString());
            } finally {
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getActivity());
                Intent intent = new Intent(AddGroupMemberActivity.ACTION);
                intent.putExtra("isSubmit", true);
                mgr.sendBroadcast(intent);
            }
        }
    };

    private LinearLayout.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getActivity());
            Intent intent = new Intent(AddGroupMemberActivity.ACTION);
            intent.putExtra("isCancel", true);
            mgr.sendBroadcast(intent);
            getActivity().finish();
        }
    };
//endregion

    //region touch region
    private void setOnTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                ImageView btnAddId = (ImageView) v.findViewById(R.id.btnAddId);
                Contact employee = (Contact) btnAddId.getTag();
                if (isSelectSingle) {
                    Utils.postRecentTrack(getActivity(), "msg", employee.getId().toString());
                    Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
                    try {
                        if (!EmployeeActivity.isUserExist(employee.getId(), getActivity())) {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(DBHelper.USER_CHAT_COLUMN_EMPLOYEE_ID, employee.getId());
                            contentValues.put(DBHelper.USER_CHAT_COLUMN_USER_ID, EmployeeActivity.getLastUserId(getActivity()) + 1);
                            contentValues.put(DBHelper.USER_CHAT_COLUMN_IMAGE, employee.getProfileImage());
                            contentValues.put(DBHelper.USER_CHAT_COLUMN_NAME, employee.getName());
                            contentValues.put(DBHelper.USER_CHAT_COLUMN_OX_USER, employee.getOXUser());
                            contentValues.put(DBHelper.USER_CHAT_COLUMN_CONTACT_NO, employee.getContactNo());
                            contentValues.put(DBHelper.USER_CHAT_COLUMN_OFFICE_NO, employee.getOfficeNo());
                            contentValues.put(DBHelper.USER_CHAT_COLUMN_POSITION, employee.getPosition());
                            contentValues.put(DBHelper.USER_CHAT_COLUMN_COUNTRY_IMAGE, employee.getCountryImage());
                            getActivity().getContentResolver().insert(UserChatProvider.CONTENT_URI, contentValues);
                        }
                    } catch (Exception ex) {
                        ex.fillInStackTrace();
                    } finally {
                        intent.putExtra("user", ChatDBHelper.getSingleUser(getActivity(), employee.getOXUser()));
                    }
                    intent.putExtra("friendId", employee.getOXUser());
                    intent.putExtra("employeeName", employee.getName());
                    intent.putExtra("selfId", Utils.getProfile(getActivity()).getOXUser());
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

//endregion

    //region broadcast
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(AddGroupMemberActivity.ACTION)) {
                if (intent.getBooleanExtra("isFavorite", false)) {
                    addedEmployee = new ArrayList<>();
                    addedId = new ArrayList<>();
                    addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                    for (int i = 0; i < addedEmployee.size(); i++) {
                        addedId.add(addedEmployee.get(i).getOxUser());
                    }
                    adapter = new FavoriteAdapter(getActivity(), cursor, addedEmployee, addedId, isSelectSingle);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                } else if (intent.getBooleanExtra("isFavoriteChange", false)) {
                    addedEmployee = new ArrayList<>();
                    addedId = new ArrayList<>();
                    addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                    for (int i = 0; i < addedEmployee.size(); i++) {
                        addedId.add(addedEmployee.get(i).getOxUser());
                    }
                }
            }
        }
    };
    //endregion

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == getActivity().RESULT_OK && requestCode == 123) {
            if (intent.getBooleanExtra("isFavorite", false)) {
                addedEmployee = new ArrayList<>();
                addedId = new ArrayList<>();
                addedEmployee = intent.getParcelableArrayListExtra("addedEmployee");
                for (int i = 0; i < addedEmployee.size(); i++) {
                    addedId.add(addedEmployee.get(i).getOxUser());
                }
                adapter = new FavoriteAdapter(getActivity(), cursor, addedEmployee, addedId, isSelectSingle);
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);
            }
        }
    }

    //region pause and resume
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(AddGroupMemberActivity.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
        getActivity().getSupportLoaderManager().restartLoader(0, null, callbacks);
    }
//endregion

    //region search
    private TextView.OnEditorActionListener ediSearchOnEditActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                if (ediSearch.getText().length() > 0) {
                    selection = DBHelper.CONTACT_COLUMN_NAME + " LIKE ? OR " + DBHelper.CONTACT_COLUMN_DEPARTMENT + " LIKE ? AND " + DBHelper.CONTACT_COLUMN_ID + "!=?";
                    selectionArgs = new String[]{"%" + ediSearch.getText().toString() + "%", "%" + ediSearch.getText().toString() + "%", String.valueOf(profile.getId())};
                } else {
                    selection = DBHelper.CONTACT_COLUMN_ID + "!=?";
                    selectionArgs = new String[]{String.valueOf(profile.getId())};
                }
                getActivity().getSupportLoaderManager().restartLoader(0, null, callbacks);
                return true;
            }
            return false;
        }
    };
//endregion

    //region loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 0) {
            Uri uri = ContactProvider.CONTENT_URI;
            return new CursorLoader(getActivity(), uri, null, selection, selectionArgs, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 0) {
            if (data != null) {
                this.cursor = data;
                adapter.swapCursor(data);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
    //endregion
}
