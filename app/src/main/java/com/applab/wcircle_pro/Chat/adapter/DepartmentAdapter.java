package com.applab.wcircle_pro.Chat.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.applab.wcircle_pro.Chat.AddGroupMemberActivity;
import com.applab.wcircle_pro.Chat.model.NewMember;
import com.applab.wcircle_pro.Chat.viewholder.DepartmentViewHolder;
import com.applab.wcircle_pro.Employee.Employee;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

/**
 * Created by user on 5/7/2015.
 */

public class DepartmentAdapter extends RecyclerView.Adapter<DepartmentViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<String> addedId;
    private Cursor cursor;
    private ArrayList<NewMember> addedEmployee;
    private boolean isSelectSingle = false;

    public DepartmentAdapter(Context context, Cursor cursor, ArrayList<NewMember> addedEmployee, ArrayList<String> addedId, boolean isSelectSingle) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.addedEmployee = addedEmployee;
        this.addedId = addedId;
        this.context = context;
        this.isSelectSingle = isSelectSingle;
    }

    @Override
    public DepartmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("ScheduleViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_department_row, parent, false);
        DepartmentViewHolder holder = new DepartmentViewHolder(view);
        holder.setIsRecyclable(true);
        return holder;
    }

    @Override
    public void onBindViewHolder(DepartmentViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Employee employee = Employee.getDepartmentData(cursor, position);
        holder.btnAddId.setImageResource(addedId.contains(employee.getOXUser()) ? R.mipmap.info_checked : R.mipmap.info_uncheck);
        employee.setIsSelect(addedId.contains(employee.getOXUser()));
        holder.btnAddId.setVisibility(isSelectSingle ? View.GONE : View.VISIBLE);
        holder.btnAddId.setTag(employee);
        holder.position.setText(employee.getPosition());
        holder.title.setText(employee.getName());
        holder.title.setTextColor(ContextCompat.getColor(context, R.color.color_green_light));
        Glide.with(context)
                .load(employee.getProfileImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleTransform(context))
                .placeholder(R.mipmap.ic_action_person_light)
                .into(holder.icon);
        Glide.with(context)
                .load(employee.getCountryImage())
                .transform(new CircleTransform(context))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgCountry);
        setCheckBoxOnClickListener(holder.btnAddId);
    }

    private void setCheckBoxOnClickListener(final ImageView btnAddId) {
        btnAddId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(context);
                Employee employee = (Employee) btnAddId.getTag();
                if (employee.getIsSelect()) {
                    if (addedId.contains(employee.getOXUser())) {
                        btnAddId.setImageResource(R.mipmap.info_uncheck);
                        Intent intent = new Intent(AddGroupMemberActivity.ACTION);
                        int row = addedId.indexOf(employee.getOXUser());
                        addedId.remove(row);
                        addedEmployee.remove(row);
                        intent.putParcelableArrayListExtra("addedEmployee", addedEmployee);
                        intent.putExtra("isFavorite", true);
                        intent.putExtra("isBranch", true);
                        intent.putExtra("isMain", true);
                        intent.putExtra("isDepartmentChange", true);
                        mgr.sendBroadcast(intent);
                        employee.setIsSelect(false);
                        btnAddId.setTag(employee);
                    }
                } else {
                    if (!addedId.contains(employee.getOXUser())) {
                        btnAddId.setImageResource(R.mipmap.info_checked);
                        Intent intent = new Intent(AddGroupMemberActivity.ACTION);
                        addedId.add(employee.getOXUser());
                        NewMember newEmployee = new NewMember();
                        newEmployee.setOxUser(employee.getOXUser());
                        newEmployee.setName(employee.getName());
                        newEmployee.setImage(employee.getProfileImage());
                        newEmployee.setCountryImage(employee.getCountryImage());
                        newEmployee.setPosition(employee.getPosition());
                        newEmployee.setId(employee.getId());
                        newEmployee.setContactNo(employee.getContactNo());
                        newEmployee.setOfficeNo(employee.getOfficeNo());
                        addedEmployee.add(newEmployee);
                        intent.putParcelableArrayListExtra("addedEmployee", addedEmployee);
                        intent.putExtra("isFavorite", true);
                        intent.putExtra("isBranch", true);
                        intent.putExtra("isMain", true);
                        intent.putExtra("isDepartmentChange", true);
                        mgr.sendBroadcast(intent);
                        employee.setIsSelect(true);
                        btnAddId.setTag(employee);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler1.obtainMessage();
            handler1.handleMessage(msg);
        }
        return oldCursor;
    }

    public void swapAddedEmployee(ArrayList<String> addedId, ArrayList<NewMember> addedEmployee) {
        this.addedId = addedId;
        this.addedEmployee = addedEmployee;
        android.os.Message msg = handler.obtainMessage();
        handler.handleMessage(msg);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            DepartmentAdapter.this.notifyDataSetChanged();
        }
    };

    Handler handler1 = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            DepartmentAdapter.this.notifyDataSetChanged();
        }
    };
}