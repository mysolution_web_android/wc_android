package com.applab.wcircle_pro.Chat.xmpp;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by haoyi on 10/9/15.
 */

public class XMPPConn {

    private AbstractXMPPConnection connection = null;
    private Map<String, MultiUserChat> mucHashMap = null;
    private MultiUserChatManager manager = null;
    private static class XMPPConnHolder
    {
        private static final XMPPConn INSTANCE = new XMPPConn();
    }
    private XMPPConn()
    {
        connection = null;
        mucHashMap = new HashMap<String, MultiUserChat>();
        manager = null;
    }
    private XMPPConn(String username, String password)
    {

    }
    public static final XMPPConn getInstance()
    {
        return XMPPConnHolder.INSTANCE;
    }

    public AbstractXMPPConnection getConn()
    {
        return this.connection;
    }
    public void setXMPPConn(AbstractXMPPConnection connection)
    {
        this.connection = connection;
    }

    public MultiUserChatManager getMultiUserChatManager()
    {
        return this.manager;
    }
    public void setMultiUserChatManager(MultiUserChatManager manager)
    {
        this.manager = manager;
    }

    public void setMUC(String name,MultiUserChat muc)
    {
        mucHashMap.put(name,muc);
    }
    public MultiUserChat getMuc(String name)
    {
        MultiUserChat muc = mucHashMap.get(name);
        return muc;
    }
}
