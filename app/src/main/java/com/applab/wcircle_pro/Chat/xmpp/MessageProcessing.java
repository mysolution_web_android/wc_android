package com.applab.wcircle_pro.Chat.xmpp;


import android.util.Log;

import com.applab.wcircle_pro.Chat.model.Group;
import com.applab.wcircle_pro.Chat.model.MessageModel;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.Gson;

import org.jivesoftware.smack.packet.Message;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;

/**
 * Created by haoyi on 10/9/15.
 */

public class MessageProcessing {

    static final int STATUS_MESSAGE_RECEIVE = 10;

    static String TAG = "MessageProcessing";

    static public MessageModel getMessageDetail(String Username, Message msg) {
        String id = "";
        String to = "";
        String from = "";
        String timestamp = "";
        String type = "";
        String extra = "0";
        String message = "";
        String groupId = "";
        String groupName = "";
        boolean isMine = false;
        boolean isImage = false;
        boolean isGroup = false;
        boolean isDelete = false;
        boolean isGroupUpdate = false;

        Group group = null;
        id = msg.getStanzaId();
        if (msg.getTo() != null) {
            String[] spliteMessageTo = msg.getTo().split("@");
            to = spliteMessageTo[0];                //to
        }
        String[] spliteMessageFrom = msg.getFrom().split("@");
        from = spliteMessageFrom[0];            //from

        timestamp = Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, new Date()));
        type = msg.getType().toString();         //type
        if (msg.getSubject() != null) {
            if (!msg.getSubject().equals("")) {
                Gson gson = new Gson();
                String subject = msg.getSubject();
                String temp = "";
                try {
                    try {
                        temp = new JSONTokener(URLDecoder.decode(subject, "UTF-8")).nextValue().toString();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                group = gson.fromJson(temp, Group.class);
                isImage = group.getIsimage() == null ? false : group.getIsimage();
                isGroup = group.getIsgroup() == null ? false : group.getIsgroup();
                isDelete = group.getIsDelete() == null ? false : group.getIsDelete();
                isGroupUpdate = group.getIsGroupUpdate() == null ? false : group.getIsGroupUpdate();
                Log.i(TAG, group.getSendDate());
                timestamp = group.getSendDate() != null ? group.getSendDate() : Utils.setToUTCDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT,
                        Utils.setCalendarDate(Utils.DATE_FORMAT, new Date()));
                if (isGroup) {
                    groupName = group.getGroupName();
                    groupId = group.getGroupId();
                }
                from = group.getFrom();
                to = group.getTo();
            }
        }
        Log.i(TAG, timestamp);

        String body = null;
        try {
            if (isImage) {
                body = URLDecoder.decode(msg.getBody(), "UTF-8").replaceAll("\"", "");
            } else {
                body = msg.getBody();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        /*
        try
        {
            JSONObject jsonObject = new JSONObject(body);
            Log.d(TAG, jsonObject.toString());

            extra = jsonObject.optString("type", "100");    //type 100 will not be using
            message = jsonObject.optString("body", "error");
        }
        catch (Throwable t)
        {
            Log.e(TAG, "Could not parse malformed JSON: \"" + body + "\"");
        }
        */
        if (from.equalsIgnoreCase(Username)) {
            isMine = true;
        }

        MessageModel mMessageModel = new MessageModel(id, from, to, timestamp, type, extra, body, isMine, groupId, groupName, isImage, isGroup, isDelete, isGroupUpdate);
        return mMessageModel;
    }

    static public String generateMessageBody(String message, int type) {
        String messageBody = "";
        switch (type) {
            case MessageModel.TEXT_MESSAGE: {
                messageBody = generateNormalTextMessageBody(message);
                break;
            }
            case MessageModel.IMAGE_MESSAGE: {
                messageBody = generateNormalTextMessageBody(message);
                break;
            }
            case MessageModel.STATUS_MESSAGE_RECEIVE: {
                messageBody = generateReceiveStatusMessage();
                break;
            }
            case MessageModel.STATUS_MESSAGE_READ: {
                messageBody = generateReadStatusMessage();
                break;
            }
        }
        return messageBody;
    }

    static private String generateImageMessageBody(String message) {
        String messageBody = "{" +              //{
                "\"type\"" +                    //"type"
                ":" +                           //:
                "\"" + MessageModel.TEXT_MESSAGE + "\"" +    //"1"
                "," +                           //,
                "\"body\"" +                    //"body"
                ":" +                           //:
                "\"" + message + "\"" +         //"message"
                "}";                          //}
        return messageBody;
    }

    static private String generateNormalTextMessageBody(String message) {
        String messageBody = "{" +              //{
                "\"type\"" +                    //"type"
                ":" +                           //:
                "\"" + MessageModel.TEXT_MESSAGE + "\"" +    //"1"
                "," +                           //,
                "\"body\"" +                    //"body"
                ":" +                           //:
                "\"" + message + "\"" +         //"message"
                "}";                          //}
        return messageBody;
    }

    static private String generateReceiveStatusMessage() {
        String messageBody = "{" +              //{
                "\"type\"" +                    //"type"
                ":" +                           //:
                "\"" + MessageModel.STATUS_MESSAGE_RECEIVE + "\"" +    //"10"
                "," +                           //,
                "\"body\"" +                    //"body"
                ":" +                           //:
                "\"  \"" +                      //"message"
                "}";                          //}
        return messageBody;
    }

    static private String generateReadStatusMessage() {
        String messageBody = "{" +                      //{
                "\"type\"" +                            //"type"
                ":" +                                   //:
                "\"" + MessageModel.STATUS_MESSAGE_READ + "\"" +     //"10"
                "," +                                   //,
                "\"body\"" +                            //"body"
                ":" +                                   //:
                "\"  \"" +                              //"message"
                "}";                                  //}
        return messageBody;
    }
}
