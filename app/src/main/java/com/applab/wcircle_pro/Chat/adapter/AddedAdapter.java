package com.applab.wcircle_pro.Chat.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.applab.wcircle_pro.Calendar.AddContactActivity;
import com.applab.wcircle_pro.Calendar.NewEmployee;
import com.applab.wcircle_pro.Chat.AddGroupMemberActivity;
import com.applab.wcircle_pro.Chat.model.NewMember;
import com.applab.wcircle_pro.Chat.viewholder.DepartmentViewHolder;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

/**
 * Created by user on 5/7/2015.
 */

public class AddedAdapter extends RecyclerView.Adapter<DepartmentViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<NewMember> addedEmployee;

    public AddedAdapter(Context context,
                        ArrayList<NewMember> addedEmployee) {
        this.inflater = LayoutInflater.from(context);
        this.addedEmployee = addedEmployee;
        this.context = context;
    }

    @Override
    public DepartmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("ScheduleViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_department_row, parent, false);
        DepartmentViewHolder holder = new DepartmentViewHolder(view);
        holder.setIsRecyclable(true);
        return holder;
    }

    @Override
    public void onBindViewHolder(DepartmentViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        NewMember employee = addedEmployee.get(position);
        employee.setSelectedPos(position);
        holder.btnAddId.setImageResource(R.mipmap.info_checked);
        holder.btnAddId.setTag(employee);
        holder.title.setText(employee.getName());
        Glide.with(context)
                .load(employee.getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleTransform(context))
                .placeholder(R.mipmap.ic_action_person_light)
                .into(holder.icon);
        Glide.with(context)
                .load(employee.getCountryImage())
                .transform(new CircleTransform(context))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgCountry);
        holder.btnAddId.setOnClickListener(mBtnAddIdOnClickListener);
    }

    private View.OnClickListener mBtnAddIdOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ImageView btnAddId = (ImageView) v.findViewById(R.id.btnAddId);
            NewMember employee = (NewMember) btnAddId.getTag();
            Intent intent = new Intent(context, AddGroupMemberActivity.class);
            int position = employee.getSelectedPos();
            employee.setSelectedPos(-1);
            int row = addedEmployee.indexOf(employee);
            addedEmployee.remove(row);
            intent.putParcelableArrayListExtra("addedEmployee", addedEmployee);
            intent.putExtra("isFavorite", true);
            intent.putExtra("isBranch", true);
            intent.putExtra("isDepartment", true);
            intent.putExtra("isMainChange", true);
            notifyItemRemoved(position);
            ((Activity) context).startActivityForResult(intent, 123);
            ((Activity) context).finish();
        }
    };

    @Override
    public int getItemCount() {
        return addedEmployee == null ? 0 : addedEmployee.size();
    }

    public ArrayList<NewMember> swapEmployee(ArrayList<NewMember> employees) {
        ArrayList<NewMember> oldEmployees = this.addedEmployee;
        this.addedEmployee = employees;
        android.os.Message msg = handler.obtainMessage();
        handler.handleMessage(msg);
        return oldEmployees;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            AddedAdapter.this.notifyDataSetChanged();
        }
    };
}