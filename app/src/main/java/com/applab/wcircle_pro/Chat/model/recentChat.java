package com.applab.wcircle_pro.Chat.model;

/**
 * Created by haoyi on 10/9/15.
 */
public class recentChat
{
    private int id;             //uni key
    private int staft_id;        //staft id
    private String staft_name;   //staft name
    private int message_type;    //msg type  1:text 2:img
    private String message_body; //msg body
    private long time;          //recevi time
    private int message_status;      //0:unread 1:read 2:sending
    private int is_sender;     //1:send 2:receive

    public recentChat(){}

    public recentChat(int staft_id, String staft_name, int message_type, String message_body, int time, int message_status, int is_sender) {
        super();
        this.staft_id = staft_id;
        this.staft_name = staft_name;
        this.message_type = message_type;
        this.message_body = message_body;
        this.time = time;
        this.message_status = message_status;
        this.is_sender = is_sender;
    }

    //getters & setters
    @Override
    public String toString()
    {
        return "RecentChat " +
                "[id=" + id +
                ", staft_id=" + staft_id +
                ", staft_name=" + staft_name +
                ", message_type=" + message_type +
                ", message_body=" + message_body +
                ", time=" + time +
                ", message_status=" + message_status +
                ", is_sender=" + is_sender +
                "]";
    }
}
