package com.applab.wcircle_pro.Chat.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/11/2015.
 */
public class ChatRoomViewHolder extends RecyclerView.ViewHolder {
    public TextView txtSenderName;
    public TextView txtSenderName1;
    public TextView txtSenderMessage;
    public TextView txtSenderDateTime;

    public TextView txtReceiverMessage;
    public TextView txtReceiverDateTime;
    public TextView txtSuccess;
    public TextView txtStatus;

    public RelativeLayout leftRV;
    public RelativeLayout rightRV;

    public LinearLayout leftLayout, rightLayout;

    public ImageView imgReceiverAttachment, imgSenderAttachment;

    public ProgressBar progressBar;

    public ChatRoomViewHolder(View itemView) {
        super(itemView);
        txtSenderName = (TextView) itemView.findViewById(R.id.txtSenderName);
        txtSenderName1 = (TextView) itemView.findViewById(R.id.txtSenderName1);
        txtSenderMessage = (TextView) itemView.findViewById(R.id.txtSenderMessage);
        txtSenderDateTime = (TextView) itemView.findViewById(R.id.txtSenderDateTime);

        txtReceiverMessage = (TextView) itemView.findViewById(R.id.txtReceiverMessage);
        txtReceiverDateTime = (TextView) itemView.findViewById(R.id.txtReceiverDateTime);
        txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
        txtSuccess = (TextView) itemView.findViewById(R.id.txtSuccess);

        leftRV = (RelativeLayout) itemView.findViewById(R.id.leftRV);
        rightRV = (RelativeLayout) itemView.findViewById(R.id.rightRV);
        leftLayout = (LinearLayout) itemView.findViewById(R.id.leftLayout);
        rightLayout = (LinearLayout) itemView.findViewById(R.id.rightLayout);

        imgReceiverAttachment = (ImageView) itemView.findViewById(R.id.imgReceiverAttachment);
        imgSenderAttachment = (ImageView) itemView.findViewById(R.id.imgSenderAttachment);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
    }
}
