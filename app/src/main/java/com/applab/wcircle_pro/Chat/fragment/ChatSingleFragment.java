package com.applab.wcircle_pro.Chat.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Chat.ChartRoomGroupActivity;
import com.applab.wcircle_pro.Chat.ChatRoomActivity;
import com.applab.wcircle_pro.Chat.adapter.ChatAdapter;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.db.HttpHelper;
import com.applab.wcircle_pro.Chat.db.UserViewProvider;
import com.applab.wcircle_pro.Chat.model.User;
import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.Chat.xmpp.XMPPConn;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.jivesoftware.smack.AbstractXMPPConnection;

public class ChatSingleFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView recyclerView;
    private ChatAdapter adapter;
    private Intent mIntent;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private String TAG = "CHAT";
    private TextView txtError;
    private Cursor cursor;
    private EditText ediSearch;
    private String selection = DBHelper.CHAT_LIST_COLUMN_IS_GROUP + "=?";
    private String[] selectionArgs = new String[]{String.valueOf(false)};
    private LoaderManager.LoaderCallbacks callbacks;
    private int page = 0;
    private boolean isFirstGet = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_chat_single, container, false);
        getActivity().getSupportLoaderManager().initLoader(14, null, this);
        callbacks = this;
        ediSearch = (EditText) layout.findViewById(R.id.ediSearch);
        txtError = (TextView) layout.findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);
        txtError.setTag(false);
        mSwipyRefreshLayout = (SwipyRefreshLayout) layout.findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        SearchWatcher();

        recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        adapter = new ChatAdapter(getActivity(), cursor);
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        setTouchListener();
        return layout;
    }

    private void SearchWatcher() {
        ediSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!ediSearch.getText().toString().equals("")) {
                        selection = DBHelper.USER_CHAT_COLUMN_NAME + " LIKE ? AND " + DBHelper.CHAT_LIST_COLUMN_IS_GROUP + "=?";
                        selectionArgs = new String[]{"%" + ediSearch.getText().toString() + "%", String.valueOf(false)};
                        getActivity().getSupportLoaderManager().restartLoader(14, null, callbacks);
                    } else {
                        selection = DBHelper.CHAT_LIST_COLUMN_IS_GROUP + "=?";
                        selectionArgs = new String[]{String.valueOf(false)};
                        getActivity().getSupportLoaderManager().restartLoader(14, null, callbacks);
                    }
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                RelativeLayout rlImage = (RelativeLayout) v.findViewById(R.id.rlImage);
                User user = (User) rlImage.getTag();
                Utils.postRecentTrack(getActivity(), "msg", String.valueOf(user.getEmployeeId()));
                String friendId = user.getJid();

                if (user.getGroupId() == null) {
                    mIntent = new Intent(getActivity(), ChatRoomActivity.class);
                    mIntent.putExtra("selfId", Utils.getProfile(getActivity()).getOXUser());
                    mIntent.putExtra("friendId", friendId);
                    if (user.getEmployeeName() == null) {
                        if (ChatDBHelper.getEmployeeName(getActivity(), friendId) != null) {
                            if (!ChatDBHelper.getEmployeeName(getActivity(), friendId).equals("")) {
                                mIntent.putExtra("employeeName",
                                        ChatDBHelper.getEmployeeName(getActivity(), friendId));
                            } else {
                                mIntent.putExtra("employeeName", friendId);
                            }
                        } else {
                            mIntent.putExtra("employeeName", friendId);
                        }
                    } else {
                        mIntent.putExtra("employeeName", user.getEmployeeName());
                    }
                    mIntent.putExtra("user", user);
                    mIntent.putExtra("page", page);
                    startActivity(mIntent);
                    getActivity().finish();
                } else {
                    mIntent = new Intent(getActivity(), ChartRoomGroupActivity.class);
                    mIntent.putExtra("groupId", user.getGroupId());
                    mIntent.putExtra("groupName", user.getGroupName());
                    mIntent.putExtra("page", page);
                    startActivity(mIntent);
                    getActivity().finish();
                }
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BackgroundXMPPService.ACTION)) {
                if (intent.getBooleanExtra("error", false)) {
                    txtError.setText(Utils.getDialogMessage(getActivity(), Utils.CODE_UNKNOWN_MESSENGER_ACCOUNT, Utils.UNKNOWN_MESSENGER_ACCOUNT));
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setTag(true);
                    adapter.swapCursor(null);
                } else if (intent.getBooleanExtra("refresh", false)) {
                    getActivity().getSupportLoaderManager().restartLoader(14, null, callbacks);
                } else if (intent.getBooleanExtra("login_available", false)) {
                    txtError.setVisibility(View.GONE);
                    txtError.setTag(false);
                    adapter.swapCursor(cursor);
                    getActivity().getSupportLoaderManager().restartLoader(14, null, callbacks);
                } else if (intent.getBooleanExtra("refresh chat list", false)) {
                    getActivity().getSupportLoaderManager().restartLoader(14, null, callbacks);
                } else if (intent.getBooleanExtra("connection_error", false)) {
                    txtError.setText(Utils.getDialogMessage(getActivity(), Utils.CODE_ACCESS_MESSENGER, Utils.ACCESS_MESSENGER));
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setTag(false);
                } else if (intent.getBooleanExtra("login_error", false)) {
                    txtError.setText(Utils.getDialogMessage(getActivity(), Utils.CODE_UNKNOWN_MESSENGER_ACCOUNT, Utils.UNKNOWN_MESSENGER_ACCOUNT));
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setTag(true);
                    adapter.swapCursor(null);
                }
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getActivity(), false);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(BackgroundXMPPService.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, iff);
        if (Utils.isConnectingToInternet(getActivity())) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        } else {
            txtError.setText(Utils.getDialogMessage(getActivity(), Utils.CODE_CONNECTION, Utils.CONNECTION));
            txtError.setVisibility(View.VISIBLE);
            txtError.setTag(false);
        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            AbstractXMPPConnection connection = XMPPConn.getInstance().getConn();
            if (connection != null) {
                if (connection.isConnected()) {
                    connection.disconnect();
                    getActivity().startService(new Intent(getActivity(), BackgroundXMPPService.class));
                } else {
                    getActivity().startService(new Intent(getActivity(), BackgroundXMPPService.class));
                }
            } else {
                getActivity().startService(new Intent(getActivity(), BackgroundXMPPService.class));
            }
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 14) {
            return new CursorLoader(getActivity(), UserViewProvider.CONTENT_URI, null, selection,
                    selectionArgs, "DATETIME(" + DBHelper.CHAT_LIST_COLUMN_LAST + ") DESC ");
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 14) {
            if (data != null) {
                if (!(Boolean) txtError.getTag()) {
                    cursor = data;
                    adapter.swapCursor(cursor);
                    if (!isFirstGet) {
                        if (cursor.getCount() > 0) {
                            String oxUser = "";
                            for (int i = 0; i < cursor.getCount(); i++) {
                                User user = User.getUser(cursor, i);
                                if (user != null) {
                                    if (user.getJid() != null) {
                                        if (!user.getJid().equals("")) {
                                            if (i == 0) {
                                                oxUser = "OXUser[" + 0 + "]=" + user.getJid();
                                            } else {
                                                oxUser += "&OXUser[" + i + "]=" + user.getJid();
                                            }
                                        }
                                    }
                                }
                            }
                            if (!oxUser.equals("")) {
                                isFirstGet = true;
                                HttpHelper.getEmployeeGroupGson(getActivity(), TAG, oxUser, null, null);
                            }
                        }
                    }
                }
            }
        }
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            if (Utils.isConnectingToInternet(getActivity())) {
                android.os.Message msg = handler.obtainMessage();
                handler.handleMessage(msg);
                if (cursor != null) {
                    if (cursor.getCount() > 0) {
                        String oxUser = "";
                        for (int i = 0; i < cursor.getCount(); i++) {
                            User user = User.getUser(cursor, i);
                            if (user != null) {
                                if (user.getJid() != null) {
                                    if (!user.getJid().equals("")) {
                                        if (i == 0) {
                                            oxUser = "OXUser[" + 0 + "]=" + user.getJid();
                                        } else {
                                            oxUser += "&OXUser[" + i + "]=" + user.getJid();
                                        }
                                    }
                                }
                            }
                        }
                        if (!oxUser.equals("")) {
                            HttpHelper.getEmployeeGroupGson(getActivity(), TAG, oxUser, null, null);
                        }
                    }
                }
            } else {
                txtError.setText(Utils.getDialogMessage(getActivity(), Utils.CODE_CONNECTION, Utils.CONNECTION));
                txtError.setVisibility(View.VISIBLE);
                txtError.setTag(false);
            }
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }
}
