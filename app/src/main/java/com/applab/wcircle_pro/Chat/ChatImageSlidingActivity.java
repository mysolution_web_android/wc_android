package com.applab.wcircle_pro.Chat;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Gallery.ImageSlidingAdapter;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;

/**
 * Created by user on 31/10/2015.
 */
public class ChatImageSlidingActivity extends AppCompatActivity {
    private ImageSlidingAdapter adapter;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private Bundle bundle;
    private String id;
    private int position;
    private TextView title;
    private LinearLayout btnSave;
    private int i = 0;
    private String url = Utils.API_URL + "api/Tracking";
    private String TAG = "NEWS_IMAGE_SLIDING";
    private int selectedPage;
    private String urlImage;
    private ArrayList<String> arrUrl = new ArrayList<>();
    private boolean isGallery = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_image_sliding);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        title = (TextView) findViewById(R.id.imgTitle);
        btnSave = (LinearLayout) findViewById(R.id.btnSave);
        viewPager = (ViewPager) findViewById(R.id.pager);
        bundle = getIntent().getExtras();
        id = bundle.getString("id");
        position = bundle.getInt("position");
        urlImage = String.valueOf(bundle.getString("urlImage").replace("/thumb", "")).replace("\"", "");
        isGallery = bundle.getBoolean("isGallery", false);
        arrUrl.add(urlImage);
        adapter = new ImageSlidingAdapter(getSupportFragmentManager(), ChatImageSlidingActivity.this,
                arrUrl);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);
        selectedPage = position;
        String[] result = urlImage.split("/");
        title.setText(result[result.length - 1]);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                String[] result = urlImage.split("/");
                title.setText(result[result.length - 1]);
                btnSave.setTag(urlImage);
                selectedPage = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isConnectingToInternet(getBaseContext())) {
                    if (urlImage.contains("https://") || urlImage.contains("http://")) {
                        Utils.downloadFile(urlImage, ChatImageSlidingActivity.this, urlImage, getBaseContext().getString(R.string.messenger), "Messenger", id);
                    } else {
                        Utils.showError(ChatImageSlidingActivity.this, "", "This image is not from server.");
                    }
                } else {
                    Utils.showNoConnection(ChatImageSlidingActivity.this);
                }
            }
        });
        hideBar();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void handleAction(View v) {
        if (isGallery) {
            if (title.getVisibility() == View.GONE) {
                Utils.expand(title);
            } else {
                Utils.collapse(title);
            }

            if (btnSave.getVisibility() == View.GONE) {
                Utils.expand(btnSave);
            } else {
                Utils.collapse(btnSave);
            }
        }
    }

    public void hideBar() {
        if (Build.VERSION.SDK_INT < 16) { //ye olde method
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else { // Jellybean and up, new hotness
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            // Remember that you should never show the action bar if the
            // status bar is hidden, so hide that too if necessary.
            ActionBar actionBar = getSupportActionBar();
            actionBar.hide();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_news_image_sliding, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
    }
}
