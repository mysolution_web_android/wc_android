package com.applab.wcircle_pro.Chat.model;

import android.database.Cursor;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Member {

    @SerializedName("Id")
    @Expose
    private String Id;
    @SerializedName("GroupId")
    @Expose
    private String GroupId;
    @SerializedName("OXUser")
    @Expose
    private String OXUser;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("ProfileImage")
    @Expose
    private String ProfileImage;
    @SerializedName("CreateBy")
    @Expose
    private String CreateBy;
    @SerializedName("CreateDate")
    @Expose
    private String CreateDate;
    private String employeeId;
    private String contactNo;
    private String officeNo;
    private String position;
    private String countryImage;
    private String owner;

    @SerializedName("GroupName")
    @Expose
    private String GroupName;
    @SerializedName("GroupImage")
    @Expose
    private String GroupImage;

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    public String getGroupImage() {
        return GroupImage;
    }

    public void setGroupImage(String groupImage) {
        GroupImage = groupImage;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getOfficeNo() {
        return officeNo;
    }

    public void setOfficeNo(String officeNo) {
        this.officeNo = officeNo;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCountryImage() {
        return countryImage;
    }

    public void setCountryImage(String countryImage) {
        this.countryImage = countryImage;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * @return The GroupId
     */
    public String getGroupId() {
        return GroupId;
    }

    /**
     * @param GroupId The GroupId
     */
    public void setGroupId(String GroupId) {
        this.GroupId = GroupId;
    }

    /**
     * @return The OXUser
     */
    public String getOXUser() {
        return OXUser;
    }

    /**
     * @param OXUser The OXUser
     */
    public void setOXUser(String OXUser) {
        this.OXUser = OXUser;
    }

    /**
     * @return The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return The ProfileImage
     */
    public String getProfileImage() {
        return ProfileImage;
    }

    /**
     * @param ProfileImage The ProfileImage
     */
    public void setProfileImage(String ProfileImage) {
        this.ProfileImage = ProfileImage;
    }

    /**
     * @return The CreateBy
     */
    public String getCreateBy() {
        return CreateBy;
    }

    /**
     * @param CreateBy The CreateBy
     */
    public void setCreateBy(String CreateBy) {
        this.CreateBy = CreateBy;
    }

    /**
     * @return The CreateDate
     */
    public String getCreateDate() {
        return CreateDate;
    }

    /**
     * @param CreateDate The CreateDate
     */
    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    public static Member getMember(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Member member = new Member();
        member.setName(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_NAME)));
        member.setProfileImage(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_IMAGE)));
        member.setOwner(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_OWNER)));
        member.setGroupId(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_GROUP_ID)));
        member.setOXUser(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_OX_USER)));
        member.setEmployeeId(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_EMPLOYEE_ID)));
        member.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_CONTACT_NO)));
        member.setOfficeNo(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_OFFICE_NO)));
        member.setPosition(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_POSITION)));
        member.setCountryImage(cursor.getString(cursor.getColumnIndex(DBHelper.GROUP_CHAT_COLUMN_COUNTRY_IMAGE)));
        return member;
    }
}