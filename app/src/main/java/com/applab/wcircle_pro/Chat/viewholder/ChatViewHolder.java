package com.applab.wcircle_pro.Chat.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 5/11/2015.
 */
public class ChatViewHolder extends RecyclerView.ViewHolder {
    public ImageView img,imgCountry;
    public TextView txtName;
    public  TextView txtDesc;
    public TextView txtDate;
    public TextView txtNo;
    public RelativeLayout rlImage;

    public ChatViewHolder(View itemView) {
        super(itemView);
        txtName = (TextView) itemView.findViewById(R.id.txtName);
        rlImage = (RelativeLayout) itemView.findViewById(R.id.rlImage);
        txtNo = (TextView) itemView.findViewById(R.id.txtNo);
        txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);
        txtDate = (TextView) itemView.findViewById(R.id.txtDate);
        img = (ImageView) itemView.findViewById(R.id.img);
        imgCountry = (ImageView) itemView.findViewById(R.id.imgCountry);
    }
}
