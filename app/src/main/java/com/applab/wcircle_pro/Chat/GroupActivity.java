package com.applab.wcircle_pro.Chat;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applab.wcircle_pro.Chat.xmpp.BackgroundXMPPService;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.CircleTransform;
import com.applab.wcircle_pro.Utils.ImageUtils;
import com.applab.wcircle_pro.Utils.RoundImage;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

import eu.janmuller.android.simplecropimage.CropImage;

public class GroupActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ImageView imgGroup;
    private EditText txtGroupName;
    private LinearLayout btnNext, btnCancel;
    private String TAG = "GROUP";
    private TextView txtError;
    private Uri fileUri;
    private File file;
    private static final int SELECT_PHOTO_REQUEST_CODE = 100;
    private static final int CROP_IMAGE_REQUEST_CODE = 32;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 101;
    private RelativeLayout fadeRL;
    private ProgressBar progressBar;
    private String filePath, groupName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green_light));
        TextView txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtToolbarTitle.setText(getResources().getString(R.string.title_activity_group));
        txtToolbarTitle.setPadding(0, 0, 100, 0);
        groupName = getIntent().getStringExtra("groupId");
        filePath = getIntent().getStringExtra("filePath");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.action_arrow_prev_white);
        imgGroup = (ImageView) findViewById(R.id.imgGroup);
        txtGroupName = (EditText) findViewById(R.id.txtGroupName);
        txtGroupName.setText(groupName != null ? groupName : "");
        btnNext = (LinearLayout) findViewById(R.id.btnNext);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        Bitmap bm = BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_action_group_light);
        RoundImage roundImage = new RoundImage();
        imgGroup.setImageBitmap(roundImage.getCircleBitmap(bm));
        imgGroup.setOnClickListener(imgGroupOnClickListener);
        if (filePath != null) {
            file = new File(filePath);
            fileUri = Uri.fromFile(file);
            Glide.with(GroupActivity.this)
                    .load(file.getAbsolutePath())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transform(new CircleTransform(GroupActivity.this))
                    .placeholder(R.mipmap.ic_action_group_light)
                    .into(imgGroup);
        }
        txtError = (TextView) findViewById(R.id.txtError);
        btnNext.setOnClickListener(btnNextOnClickLister);
        btnCancel.setOnClickListener(btnCancelOnClickListener);
        txtGroupName.setOnEditorActionListener(txtGroupNameOnEditorAction);
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        setVisibilityRlAndProgressBar(false);
        fadeRL.setOnClickListener(fadeRLOnClickListener);
    }

    private View.OnClickListener fadeRLOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private void setVisibilityRlAndProgressBar(boolean isVisible) {
        if (isVisible) {
            fadeRL.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            fadeRL.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener imgGroupOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            GroupDialogFragment groupDialogFragment = GroupDialogFragment.newInstance(GroupActivity.this);
            groupDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    //region Load Image
    public static Uri getOutputMediaFileUri(Context context, boolean isDelete, File file) {
        return Uri.fromFile(getOutputMediaFile(context, isDelete, file));
    }

    public static File getOutputMediaFile(Context context, boolean isDelete, File mediaStorageDir) {
        if (mediaStorageDir == null) {
            mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" +
                    context.getResources().getString(R.string.temp));
        }

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        } else {
            if (isDelete) {
                boolean isDirDeleted = Utils.deleteDir(mediaStorageDir);
                if (isDirDeleted) {
                    mediaStorageDir.mkdirs();
                }
            }
        }
        String timeStamp = Utils.setCalendarDate("yyyyMMdd_HHmmss_SSS", new Date());
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
    }

    public static File previewCapturedImage(Context context, Uri fileUri, final ImageView img, final boolean isCrop,
                                            boolean isGroupPicSmall, int requestedCode, boolean isDelete, File direct) {
        ImageUtils.normalizeImageForUri(context, fileUri);
        File file = new File(fileUri.getPath());
        file = Utils.convertImageToSmall(context, file, isGroupPicSmall, isDelete, direct);
        DisplayMetrics mDisplayMetrics = context.getResources().getDisplayMetrics();
        int mHeight = mDisplayMetrics.heightPixels;
        int mWidth = mDisplayMetrics.widthPixels;
        if (!isCrop) {
            if (img != null) {
                Glide.with(context)
                        .load(file.getAbsolutePath())
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.empty_photo)
                        .into(new SimpleTarget<Bitmap>(mWidth, mHeight) {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                img.setImageBitmap(bitmap);
                            }
                        });
            }
        } else {
            boolean isSuccess = performCrop(fileUri, context, requestedCode);
            if (!isSuccess) {
                if (img != null) {
                    Glide.with(context)
                            .load(file.getAbsolutePath())
                            .asBitmap()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.empty_photo)
                            .into(new SimpleTarget<Bitmap>(mWidth, mHeight) {
                                @Override
                                public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                    img.setImageBitmap(bitmap);
                                }
                            });
                }
            }
        }
        return file;
    }

    public static File previewSelectedImage(Context context, Uri selectedImage, final ImageView img,
                                            final boolean isCrop, boolean isGropPicSmall, int requestedCode, boolean isDelete, File direct) {
        if (getRealPathFromURI(context, selectedImage) != null) {
            File file = new File(getRealPathFromURI(context, selectedImage));
            file = Utils.convertImageToSmall(context, file, isGropPicSmall, isDelete, direct);
            DisplayMetrics mDisplayMetrics = context.getResources().getDisplayMetrics();
            int mHeight = mDisplayMetrics.heightPixels;
            int mWidth = mDisplayMetrics.widthPixels;
            if (!isCrop) {
                if (img != null) {
                    Glide.with(context)
                            .load(file.getAbsolutePath())
                            .asBitmap()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.empty_photo)
                            .into(new SimpleTarget<Bitmap>(mWidth, mHeight) {
                                @Override
                                public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                    img.setImageBitmap(bitmap);
                                }
                            });
                }
            } else {
                boolean isSuccess = performCrop(selectedImage, context, requestedCode);
                if (!isSuccess) {
                    if (img != null) {
                        Glide.with(context)
                                .load(file.getAbsolutePath())
                                .asBitmap()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.empty_photo)
                                .into(new SimpleTarget<Bitmap>(mWidth, mHeight) {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImageBitmap(bitmap);
                                    }
                                });
                    }
                }
            }
            return file;
        } else {
            return null;
        }
    }

    /**
     * this function does the crop operation.
     */
    public static boolean performCrop(Uri picUri, Context context, int requestCode) {
        boolean isSuccess = false;
        // take care of exceptions
        try {
            Intent cropIntent = new Intent(context, CropImage.class);
            try {
            /*
             * Depending on the camera app it might be possible to get the orientation
			 * of the image from its EXIF data. Pass the level of rotation to the crop
			 * activity so it can be shown the right way up.
			 */
                ExifInterface exif = new ExifInterface(GroupActivity.getRealPathFromURI(context, picUri));
                int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
                switch (exifOrientation) {
                    case ExifInterface.ORIENTATION_NORMAL:
                        cropIntent.putExtra(CropImage.ROTATION_IN_DEGREES, 0f);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        cropIntent.putExtra(CropImage.ROTATION_IN_DEGREES, 90f);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        cropIntent.putExtra(CropImage.ROTATION_IN_DEGREES, 180f);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        cropIntent.putExtra(CropImage.ROTATION_IN_DEGREES, -90f);
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            // tell CropImage activity to look for image to crop
            cropIntent.putExtra(CropImage.IMAGE_PATH, new File(getRealPathFromURI(context, picUri)).getAbsolutePath());

            // allow CropImage activity to rescale image
            cropIntent.putExtra(CropImage.SCALE, true);

            // if the aspect ratio is fixed to ratio 3/2
            cropIntent.putExtra(CropImage.ASPECT_X, 2);
            cropIntent.putExtra(CropImage.ASPECT_Y, 2);
            // start the activity - we handle returning in onActivityResult
            ((Activity) context).startActivityForResult(cropIntent, requestCode);
            isSuccess = true;
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            Toast toast = Toast.makeText(context, "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
            toast.show();
        }
        return isSuccess;
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        String result = null;
        try {
            cursor = context.getContentResolver().query(contentUri, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                result = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        if (contentUri != null && result == null) {
            try {
                File file = new File(new URI(contentUri.toString()));
                result = file.getAbsolutePath();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    //endregion

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap result = null;
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case SELECT_PHOTO_REQUEST_CODE:
                file = GroupActivity.previewSelectedImage(this, data.getData(), null, false, false, 0, true, null);
                GroupActivity.performCrop(Uri.fromFile(file), GroupActivity.this, CROP_IMAGE_REQUEST_CODE);
                break;
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                GroupActivity.performCrop(fileUri, GroupActivity.this, CROP_IMAGE_REQUEST_CODE);
                break;
            case CROP_IMAGE_REQUEST_CODE:
                if (data.getStringExtra(CropImage.IMAGE_PATH) == null) {
                    return;
                }
                result = BitmapFactory.decodeFile(data.getStringExtra(CropImage.IMAGE_PATH));
                file = Utils.getFile(this, result, true, null);
                file = GroupActivity.previewCapturedImage(this, Uri.parse(file.getAbsolutePath()), imgGroup, false, true, 0, true, null);
                Glide.with(GroupActivity.this)
                        .load(file.getAbsolutePath())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(new CircleTransform(GroupActivity.this))
                        .placeholder(R.mipmap.ic_action_group_light)
                        .into(imgGroup);
                break;
        }
    }

    private TextView.OnEditorActionListener txtGroupNameOnEditorAction = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (v != null) {
                    InputMethodManager inputManager = (InputMethodManager) getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return true;
            }
            return false;
        }
    };

    private LinearLayout.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibilityRlAndProgressBar(true);
            startActivity(new Intent(GroupActivity.this, ChatActivity.class));
            finish();
        }
    };

    private LinearLayout.OnClickListener btnNextOnClickLister = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibilityRlAndProgressBar(true);
            if (Utils.isConnectingToInternet(GroupActivity.this)) {
                if (txtGroupName.getText().length() > 0) {
                    String roomName = txtGroupName.getText().toString();
                    Intent intent = new Intent(new Intent(GroupActivity.this, AddGroupMemberActivity.class));
                    intent.putExtra("groupId", roomName);
                    if (file != null) {
                        if (file.length() < 3 * 1000000) {
                            String[] result = file.getName().split("\\.");
                            if (result[result.length - 1].equalsIgnoreCase("jpg") || result[result.length - 1].
                                    equalsIgnoreCase("png") || result[result.length - 1].equalsIgnoreCase("jpeg")) {
                                intent.putExtra("filePath", file.getAbsolutePath());
                            } else {
                                Utils.showError(GroupActivity.this, "", "The image must be jpg or png.");
                            }
                        } else {
                            Utils.showError(GroupActivity.this, "", "The image is too large.");
                        }
                    }
                    startActivity(intent);
                    finish();
                } else {
                    setVisibilityRlAndProgressBar(false);
                    Utils.showError(GroupActivity.this, Utils.CODE_GROUP_NAME, Utils.GROUP_NAME);
                }
            } else {
                setVisibilityRlAndProgressBar(false);
                Utils.showNoConnection(GroupActivity.this);
            }
        }
    };


    @Override
    public void onBackPressed() {
        setVisibilityRlAndProgressBar(true);
        startActivity(new Intent(this, ChatActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            setVisibilityRlAndProgressBar(true);
            startActivity(new Intent(this, ChatActivity.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BackgroundXMPPService.ACTION)) {
                if (intent.getBooleanExtra("error", false)) {
                    txtError.setText(Utils.getDialogMessage(GroupActivity.this,
                            Utils.CODE_UNKNOWN_MESSENGER_ACCOUNT, Utils.UNKNOWN_MESSENGER_ACCOUNT));
                    txtError.setVisibility(View.VISIBLE);
                } else if (intent.getBooleanExtra("connection_available", false)) {
                } else if (intent.getBooleanExtra("isCamera", false)) {
                    if (Utils.isDeviceSupportCamera(GroupActivity.this)) {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        fileUri = GroupActivity.getOutputMediaFileUri(GroupActivity.this, true, null);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                        startActivityForResult(cameraIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    } else {
                        Utils.showError(GroupActivity.this, "", "Sorry, you don't have supported camera.");
                    }
                } else if (intent.getBooleanExtra("isPhoto", false)) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, SELECT_PHOTO_REQUEST_CODE);
                }
            }
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), false);
        setVisibilityRlAndProgressBar(false);
        IntentFilter iff = new IntentFilter(BackgroundXMPPService.ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }
}
