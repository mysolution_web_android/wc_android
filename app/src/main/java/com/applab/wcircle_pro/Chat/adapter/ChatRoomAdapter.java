package com.applab.wcircle_pro.Chat.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.applab.wcircle_pro.Chat.ChatImageSlidingActivity;
import com.applab.wcircle_pro.Chat.viewholder.ChatRoomViewHolder;
import com.applab.wcircle_pro.Chat.model.MessageModel;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by user on 5/11/2015.
 */

public class ChatRoomAdapter extends RecyclerView.Adapter<ChatRoomViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;
    private String TAG = "CHAT";

    public ChatRoomAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public ChatRoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("ChatRoomViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.chat_room, parent, false);
        ChatRoomViewHolder holder = new ChatRoomViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ChatRoomViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        final MessageModel current = MessageModel.getMessageModel(context, cursor, position);
        if (current != null) {
            Log.i(TAG, "First IS Thumb : " + String.valueOf(current.getMessage().contains("/thumb")) + " Url: " + current.getMessage());
            holder.leftRV.setVisibility(current.isMine() ? View.GONE : View.VISIBLE);
            holder.imgReceiverAttachment.setVisibility(current.getIsImage() ? View.VISIBLE : View.GONE);
            holder.txtReceiverMessage.setVisibility(current.getIsImage() ? View.GONE : View.VISIBLE);
            holder.txtReceiverMessage.setText(current.getMessage());
            holder.progressBar.setVisibility(!current.getIsImage() ? View.GONE : View.VISIBLE);
            if (current.getIsImage()) {
                if (current.getIsSuccess().equals("pending") || current.getIsSuccess().equals("fail")) {
                    Glide.with(context)
                            .load(current.getMessage())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.empty_photo)
                            .into(holder.imgReceiverAttachment);
                    Glide.with(context)
                            .load(current.getMessage())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.empty_photo)
                            .into(holder.imgSenderAttachment);
                } else {
                    if (current.getMessage().contains("/thumb")) {
                        Log.i(TAG, "Second IS Thumb : " + String.valueOf(current.getMessage().contains("/thumb")) + " Url: " + current.getMessage());
                        Glide.with(context)
                                .load(current.getMessage().replace("\"",""))
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.empty_photo)
                                .into(holder.imgReceiverAttachment);
                        Glide.with(context)
                                .load(current.getMessage().replace("\"",""))
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.empty_photo)
                                .into(holder.imgSenderAttachment);
                    } else {
                        holder.imgReceiverAttachment.setImageResource(R.drawable.empty_photo);
                        holder.imgSenderAttachment.setImageResource(R.drawable.empty_photo);
                    }
                }
            } else {
                holder.txtSuccess.setVisibility(View.GONE);
            }
            holder.txtSenderMessage.setVisibility(current.getIsImage() ? View.GONE : View.VISIBLE);
            if (current.getIsImage()) holder.txtSenderName1.setPadding(4, 4, 4, 4);
            else holder.txtSenderName1.setPadding(0, 0, 0, 0);
            if (current.getIsImage())
                holder.txtSenderName1.setBackgroundColor(Color.parseColor("#AA000000"));
            else holder.txtSenderName1.setBackground(null);
            if (current.isMine()) {
                if (current.getIsImage()) {
                    switch (current.getIsSuccess()) {
                        case "pending":
                            holder.progressBar.setVisibility(View.VISIBLE);
                            holder.txtSuccess.setVisibility(View.GONE);
                            break;
                        case "success":
                            holder.progressBar.setVisibility(View.GONE);
                            holder.txtSuccess.setVisibility(View.GONE);
                            break;
                        default:
                            holder.progressBar.setVisibility(View.GONE);
                            holder.txtSuccess.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            }
            holder.leftRV.setTag(current);
            holder.leftLayout.setTag(current);
            holder.txtReceiverDateTime.setText(Utils.setDate(Utils.DATE_FORMAT, "dd/MM/yy   h:mm a", current.getTime()));
            holder.rightRV.setVisibility(current.isMine() ? View.VISIBLE : View.GONE);

            holder.txtSenderMessage.setText(current.getMessage());
            holder.imgSenderAttachment.setVisibility(current.getIsImage() ? View.VISIBLE : View.GONE);

            holder.txtSenderName.setText(current.getEmployeeName() == null ? current.getFrom() : current.getEmployeeName());
            holder.txtSenderName1.setText(current.getEmployeeName() == null ? current.getFrom() : current.getEmployeeName());

            holder.txtSenderName.setVisibility(current.getIsGroup() ? current.getIsImage() ? View.GONE : current.getIsDelete() && !current.getIsGroupUpdate() ?
                    View.GONE : !current.getIsDelete() && current.getIsGroupUpdate() ? View.GONE : View.VISIBLE : View.GONE);
            holder.txtSenderName1.setVisibility(current.getIsGroup() ? current.getIsImage() ? View.VISIBLE : current.getIsDelete() && !current.getIsGroupUpdate() ?
                    View.GONE : !current.getIsDelete() && current.getIsGroupUpdate() ? View.GONE : View.VISIBLE : View.GONE);

            holder.rightRV.setTag(current);
            holder.rightLayout.setTag(current);
            holder.txtSenderDateTime.setText(Utils.setDate(Utils.DATE_FORMAT, "dd/MM/yy   h:mm a", current.getTime()));

            setLeftLayoutOnClickListener(holder.leftLayout);
            setRightLayoutOnClickListener(holder.rightLayout);
        } else {
            holder.leftRV.setVisibility(View.GONE);
            holder.rightRV.setTag(View.GONE);
        }
    }

    private void setLeftLayoutOnClickListener(final LinearLayout leftRV) {
        leftRV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageModel current = (MessageModel) leftRV.getTag();
                if (current.getIsImage()) {
                    Intent intent = new Intent(context, ChatImageSlidingActivity.class);
                    intent.putExtra("id", String.valueOf(current.getId()));
                    intent.putExtra("position", 0);
                    intent.putExtra("isGallery", true);
                    String url = current.getMessage();
                    if (url == null) {
                        url = "";
                    }
                    intent.putExtra("urlImage", url);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        });
    }

    private void setRightLayoutOnClickListener(final LinearLayout rightRV) {
        rightRV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageModel current = (MessageModel) rightRV.getTag();
                if (current.getIsImage()) {
                    Intent intent = new Intent(context, ChatImageSlidingActivity.class);
                    intent.putExtra("id", String.valueOf(current.getId()));
                    intent.putExtra("position", 0);
                    intent.putExtra("isGallery", true);
                    String url = current.getMessage();
                    if (url == null) {
                        url = "";
                    }
                    intent.putExtra("urlImage", url);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            Utils.clearCache(context);
            ChatRoomAdapter.this.notifyDataSetChanged();
        }
    };
}
