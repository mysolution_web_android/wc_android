package com.applab.wcircle_pro.Chat;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;

import com.applab.wcircle_pro.Chat.db.ChatListProvider;
import com.applab.wcircle_pro.Chat.fragment.ChatMultipleFragment;
import com.applab.wcircle_pro.Chat.fragment.ChatSingleFragment;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Tabs.SlidingTabLayout;
import com.applab.wcircle_pro.Utils.DBHelper;

public class ChatPagerAdapter extends FragmentPagerAdapter implements LoaderManager.LoaderCallbacks<Cursor> {
    String[] tabs;
    Context context;
    SlidingTabLayout tabLayout;
    ViewPager viewPager;

    public ChatPagerAdapter(FragmentManager fm, Context context, LoaderManager loaderManager, SlidingTabLayout tabLayout, ViewPager viewPager) {
        super(fm);
        tabs = context.getResources().getStringArray(R.array.chat_tabs);
        this.context = context;
        this.tabLayout = tabLayout;
        this.viewPager = viewPager;
        loaderManager.initLoader(56, null, this);
        loaderManager.initLoader(58, null, this);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment item = null;
        ChatSingleFragment singleFragment = new ChatSingleFragment();
        ChatMultipleFragment multipleFragment = new ChatMultipleFragment();
        if (position == 0) {
            item = singleFragment;
        } else if (position == 1) {
            item = multipleFragment;
        }
        return item;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 56) {
            String[] projection = {"SUM(" + DBHelper.CHAT_LIST_COLUMN_UNREAD + ") AS " + DBHelper.CHAT_LIST_COLUMN_UNREAD};
            return new CursorLoader(context, ChatListProvider.CONTENT_URI, projection, DBHelper.CHAT_LIST_COLUMN_GROUP_ID + " IS NULL ", null, null);
        } else if (id == 58) {
            String[] projection = {"SUM(" + DBHelper.CHAT_LIST_COLUMN_UNREAD + ") AS " + DBHelper.CHAT_LIST_COLUMN_UNREAD};
            return new CursorLoader(context, ChatListProvider.CONTENT_URI, projection, DBHelper.CHAT_LIST_COLUMN_GROUP_ID + " IS NOT NULL ", null, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 56) {
            if (data != null) {
                data.moveToFirst();
                if (!data.isNull(data.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_UNREAD))) {
                    String temp = "";
                    int number = data.getInt(data.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_UNREAD));
                    if (number > 9) {
                        temp = "/" + "+9";
                    } else if (number == 0) {
                        temp = "";
                    } else if (number > 0) {
                        temp = "/" + String.valueOf(number);
                    }
                    tabs[0] = context.getResources().getStringArray(R.array.chat_tabs)[0] + temp;
                    this.notifyDataSetChanged();
                    tabLayout.setViewPager(viewPager);
                }
            }
        } else if (loader.getId() == 58) {
            if (data != null) {
                data.moveToFirst();
                if (!data.isNull(data.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_UNREAD))) {
                    String temp = "";
                    int number = data.getInt(data.getColumnIndex(DBHelper.CHAT_LIST_COLUMN_UNREAD));
                    if (number > 9) {
                        temp = "/" + "+9";
                    } else if (number == 0) {
                        temp = "";
                    } else if (number > 0) {
                        temp = "/" + String.valueOf(number);
                    }
                    tabs[1] = context.getResources().getStringArray(R.array.chat_tabs)[1] + temp;
                    this.notifyDataSetChanged();
                    tabLayout.setViewPager(viewPager);
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}