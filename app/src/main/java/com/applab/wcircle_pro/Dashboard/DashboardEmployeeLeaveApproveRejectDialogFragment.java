package com.applab.wcircle_pro.Dashboard;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Leave.Leave;
import com.applab.wcircle_pro.Leave.LeaveProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.JsonObject;

import java.util.HashMap;

/**
 * Created by user on 13/10/2015.
 */

public class DashboardEmployeeLeaveApproveRejectDialogFragment extends DialogFragment {
    private EditText mEdiRemarks;
    private TextView mTxtTitle;
    private int mStatus;
    private LinearLayout mBtnSubmit, mBtnExit;
    private ImageView mBtnCancel;
    private String TAG = "REMARKS";
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private int mPosition;
    private Leave mLeave;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DashboardEmployeeLeaveApproveRejectDialogFragment newInstance(int status, Leave leave, Context context, int position) {
        DashboardEmployeeLeaveApproveRejectDialogFragment frag = new DashboardEmployeeLeaveApproveRejectDialogFragment();
        Bundle args = new Bundle();
        args.putInt("status", status);
        args.putParcelable("leave", leave);
        args.putInt("position", position);
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mStatus = getArguments().getInt("status");
        mPosition = getArguments().getInt("position");
        mLeave = getArguments().getParcelable("leave");

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_leave_reason, null);

        mEdiRemarks = (EditText) v.findViewById(R.id.ediRemarks);

        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mEdiRemarks = (EditText) v.findViewById(R.id.ediRemarks);
        mEdiRemarks.requestFocus();
        mEdiRemarks.requestFocus();
        mEdiRemarks.setOnEditorActionListener(ediSearchOnEditActionListener);
        if (mStatus == 2) {
            mTxtTitle.setText(getResources().getString(R.string.remarks1));
            mEdiRemarks.setHint(getResources().getString(R.string.remarks1));
        } else {
            mTxtTitle.setText(getResources().getString(R.string.remarks1));
            mEdiRemarks.setHint(getResources().getString(R.string.remarks1));
        }

        mBtnSubmit = (LinearLayout) v.findViewById(R.id.btnSubmit);
        mBtnExit = (LinearLayout) v.findViewById(R.id.btnExit);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);

        mBtnSubmit.setOnClickListener(btnSubmitOnClickListener);
        mBtnExit.setOnClickListener(btnExitOnClickListener);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mStatus == 2) {
                putApprove(mLeave.getId(), mEdiRemarks.getText().toString());
            } else {
                if (mEdiRemarks.getText().toString().length() > 0) {
                    putReject(mLeave.getId(), mEdiRemarks.getText().toString());
                } else {
                    Utils.showError(mContext, Utils.CODE_REMARKS_FIELD, Utils.REMARKS_FIELD);
                }
            }
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            DashboardEmployeeLeaveApproveRejectDialogFragment.this.getDialog().cancel();
        }
    };

    //region search watcher region
    private TextView.OnEditorActionListener ediSearchOnEditActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.
                        INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
            return false;
        }
    };
    //endregion

    private View.OnClickListener btnExitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            DashboardEmployeeLeaveApproveRejectDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            DashboardEmployeeLeaveApproveRejectDialogFragment.this.getDialog().cancel();
        }
    };

    //region approve leave
    public void putApprove(final int id, final String remarks) {
        final String token = Utils.getToken(mContext);
        Log.i(TAG, "Id: " + id + " Token: " + token);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.PUT,
                Utils.API_URL + "api/Leave/Other/Approved",
                JsonObject.class,
                headers,
                responseApproveListener(id, remarks),
                errorApproveListener()) {

            @Override
            public byte[] getBody() {
                String httpPostBody = "Id=" + id + "&Remark=" + Utils.encode(remarks);
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<JsonObject> responseApproveListener(final int id, final String remarks) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                try {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, "approved");
                    contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, remarks);
                    mContext.getContentResolver().update(LeaveProvider.CONTENT_URI, contentValues,
                            DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(id)});
                    Intent intent = new Intent(DashboardActivity.ACTION);
                    intent.putExtra("isCalendar", false);
                    intent.putExtra("isEmployeeLeave", true);
                    intent.putExtra("isMyLeave", false);
                    intent.putExtra("position", mPosition);
                    LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
                    mgr.sendBroadcast(intent);
                } catch (Exception ex) {
                    ex.fillInStackTrace();
                }
                DashboardEmployeeLeaveDialogFragment.setRead(mContext, mLeave, TAG);

            }
        };
    }

    private Response.ErrorListener errorApproveListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(mContext, error);
            }
        };
    }
    //endregion

    //region reject leave
    public void putReject(final int id, final String remarks) {
        final String token = Utils.getToken(mContext);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.PUT,
                Utils.API_URL + "api/Leave/Other/Reject",
                JsonObject.class,
                headers,
                responseRejectListener(id, remarks),
                errorRejectListener()) {
            @Override
            public byte[] getBody() {
                String httpPostBody = "Id=" + id + "&Remark=" + Utils.encode(remarks);
                Log.i(TAG, httpPostBody);
                return httpPostBody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<JsonObject> responseRejectListener(final int id, final String remarks) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                try {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, "rejected");
                    contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, remarks);
                    mContext.getContentResolver().update(LeaveProvider.CONTENT_URI, contentValues,
                            DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(id)});
                    Intent intent = new Intent(DashboardActivity.ACTION);
                    intent.putExtra("isCalendar", false);
                    intent.putExtra("isEmployeeLeave", true);
                    intent.putExtra("isMyLeave", false);
                    intent.putExtra("position", mPosition);
                    LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
                    mgr.sendBroadcast(intent);
                } catch (Exception ex) {
                    ex.fillInStackTrace();
                }
                DashboardEmployeeLeaveDialogFragment.setRead(mContext, mLeave, TAG);
            }
        };
    }

    private Response.ErrorListener errorRejectListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(mContext, error);
            }
        };
    }
    //endregion
}

