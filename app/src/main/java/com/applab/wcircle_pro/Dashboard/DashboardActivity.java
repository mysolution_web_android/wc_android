package com.applab.wcircle_pro.Dashboard;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Calendar.Attendee;
import com.applab.wcircle_pro.Calendar.AttendeesProvider;
import com.applab.wcircle_pro.Calendar.CalendarActivity;
import com.applab.wcircle_pro.Calendar.CalendarChecking;
import com.applab.wcircle_pro.Calendar.CalendarCheckingProvider;
import com.applab.wcircle_pro.Calendar.CalendarProvider;
import com.applab.wcircle_pro.Catalogue.CatalogueActivity;
import com.applab.wcircle_pro.Chat.ChatActivity;
import com.applab.wcircle_pro.Chat.db.ChatDBHelper;
import com.applab.wcircle_pro.Chat.xmpp.XMPPTimerIntentService;
import com.applab.wcircle_pro.Download.DownloadActivity;
import com.applab.wcircle_pro.Drive.DriveActivity;
import com.applab.wcircle_pro.Employee.EmployeeActivity;
import com.applab.wcircle_pro.Event.EventActivity;
import com.applab.wcircle_pro.Favorite.FavoriteActivity;
import com.applab.wcircle_pro.Gallery.GalleryActivity;
import com.applab.wcircle_pro.Leave.Leave;
import com.applab.wcircle_pro.Leave.LeaveActivity;
import com.applab.wcircle_pro.Leave.LeaveChecking;
import com.applab.wcircle_pro.Leave.LeaveCheckingProvider;
import com.applab.wcircle_pro.Leave.LeaveProvider;
import com.applab.wcircle_pro.Menu.NavigationDrawerFragment;
import com.applab.wcircle_pro.News.NewsActivity;
import com.applab.wcircle_pro.Profile.ProfileActivity;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.ItemClickSupport;
import com.applab.wcircle_pro.Utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class DashboardActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, SwipyRefreshLayout.OnRefreshListener {
    private Toolbar toolbar;
    private Button btnOpenPopup;
    private boolean isPopUp = false;
    private RecyclerView recyclerView;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private DashboardAdapter adapter;
    private NavigationDrawerFragment drawerFragment;
    private TextView txtToolbarTitle, txtError;
    private ImageView imgBackground;
    private RelativeLayout fadeRL;
    private String TAG = "DASHBOARD";
    private Cursor cursor;
    private PopupWindow popupWindow = null;
    private TextView txtTitle, txtSubTitle;
    private ChatDBHelper db;
    private int pageNoCalendar = 1;
    private int pageNoEmployeeLeave = 1;
    private ArrayList<com.applab.wcircle_pro.Calendar.Calendar> arrCalendar = new ArrayList<>();
    private ArrayList<Leave> arrEmployeeLeave = new ArrayList<>();
    private ArrayList<Leave> arrMyLeave = new ArrayList<>();
    public static String ACTION = "DASHBOARD ACTION";
    public static LocalBroadcastManager mgr;
    private DashboardCalendarDialogFragment calendarDialogFragment;
    private DashboardEmployeeLeaveDialogFragment employeeLeaveDialogFragment;
    private DashboardMyLeaveDialogFragment myLeaveDialogFragment;
    private Timer mTimer = null;
    private Handler mHandler = new Handler();
    private boolean mIsStateAlreadySaved = false;
    public static final long NOTIFY_INTERVAL = 60 * 1000; // 60 seconds

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        ImageView space1 = (ImageView) toolbar.findViewById(R.id.space1);
        space1.setVisibility(View.INVISIBLE);
        mgr = LocalBroadcastManager.getInstance(this);
        txtToolbarTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtSubTitle = (TextView) findViewById(R.id.txtSubTitle);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        txtError = (TextView) findViewById(R.id.txtError);
        txtTitle.setText(getBaseContext().getResources().getString(R.string.welcome));
        txtSubTitle.setText(getBaseContext().getResources().getString(R.string.wcircle_pro));
        txtToolbarTitle.setText(getBaseContext().getResources().getString(R.string.title_activity_my_dashboard));
        imgBackground = (ImageView) findViewById(R.id.imgBackground);
        imgBackground.setImageResource(R.drawable.background);
        fadeRL = (RelativeLayout) findViewById(R.id.fadeRL);
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        drawerFragment.setSelectedPosition(0);
        btnOpenPopup = (Button) findViewById(R.id.btnHld);
        db = new ChatDBHelper(this);
        getSupportLoaderManager().initLoader(194, null, this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new DashboardAdapter(this, cursor);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        setTouchListener();
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            setPopUpWindow();
        }
    };

    Handler handler1 = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            getGson();
        }
    };

    //region dashbboard

    public static int getNumber(Context context, String title) {
        int total = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DashboardProvider.CONTENT_URI, null, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{title}, null);
            if (cursor != null && cursor.moveToFirst()) {
                total = cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        total = total == 0 ? 0 : total - 1;
        return total;
    }

    private void setPopUpWindow() {
        boolean isCalendar = getIntent().getBooleanExtra("isCalendar", false);
        boolean isEmployeeLeave = getIntent().getBooleanExtra("isEmployeeLeave", false);
        int position = getIntent().getIntExtra("position", 0);
        com.applab.wcircle_pro.Calendar.Calendar calendar = getIntent().getParcelableExtra("calendar");
        Leave leave = getIntent().getParcelableExtra("leave");
        if (isCalendar) {
            if (calendar != null) {
                arrCalendar.add(calendar);
                setPopUpCalendar(arrCalendar, position, true);
            }
        } else if (isEmployeeLeave) {
            if (leave != null) {
                arrEmployeeLeave.add(leave);
                setPopUpEmployeeLeave(arrEmployeeLeave, position, true);
            }
        } else {
            getCalendarGson(pageNoCalendar);
        }
    }

    public static void setCalendarAlarm(int year, int month, int day, int hourOfDay, int minute, String msg,
                                        com.applab.wcircle_pro.Calendar.Calendar mCalendar, ArrayList<Attendee> arrAttendee, Context context) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.clear();
        calendar.set(year, month - 1, day, hourOfDay, minute);

        Intent intent = new Intent(context, DashboardCalendarAlarmReceiver.class);
        intent.putExtra("calendar", mCalendar);
        intent.putExtra("msg", msg);
        intent.putExtra("attendees", arrAttendee);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, mCalendar.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    public static void setLeaveAlarm(int year, int month, int day, int hourOfDay, int minute, String msg,
                                     Leave leave, Context context) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.clear();
        calendar.set(year, month - 1, day, hourOfDay, minute);

        Intent intent = new Intent(context, DashboardLeaveAlarmReceiver.class);
        intent.putExtra("leave", leave);
        intent.putExtra("msg", msg);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, leave.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    public static ArrayList<String> getListReminder() {
        ArrayList<String> strList = new ArrayList<>();
        strList.add("Select reminder on the day");
        strList.add("30 minutes before");
        strList.add("1 hour before");
        strList.add("2 hours before");
        strList.add("3 hours before");
        return strList;
    }

    public static String getReminder(String startDate, int selectedReminder) {
        Date requestedDate = new Date();
        if (selectedReminder != 0) {
            java.util.Calendar gc = new GregorianCalendar();
            gc.setTime(Utils.setCalendarDate(Utils.DATE_FORMAT_PRINT, startDate));
            if (selectedReminder == 1) {
                gc.add(java.util.Calendar.MINUTE, -30);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 2) {
                gc.add(java.util.Calendar.HOUR, -1);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 3) {
                gc.add(java.util.Calendar.HOUR, -2);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 4) {
                gc.add(java.util.Calendar.HOUR, -3);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 5) {
                gc.add(java.util.Calendar.HOUR, -4);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 6) {
                gc.add(java.util.Calendar.HOUR, -5);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 7) {
                gc.add(java.util.Calendar.HOUR, -6);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 8) {
                gc.add(java.util.Calendar.HOUR, -7);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 9) {
                gc.add(java.util.Calendar.HOUR, -8);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 10) {
                gc.add(java.util.Calendar.HOUR, -24);
                requestedDate = gc.getTime();
            } else if (selectedReminder == 11) {
                gc.add(java.util.Calendar.HOUR, -48);
                requestedDate = gc.getTime();
            }
        } else {
            return "";
        }

        return Utils.setCalendarDate(Utils.DATE_FORMAT_PRINT, requestedDate);
    }

    private void setTouchListener() {
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                menuNavigation(position);
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                return false;
            }
        });
    }

    public void menuNavigation(int position) {
        switch (position) {
            case 0:
                startActivity(new Intent(getBaseContext(), FavoriteActivity.class));
                finish();
                break;
            case 1:
                startActivity(new Intent(getBaseContext(), CalendarActivity.class));
                finish();
                break;
            case 2:
                startActivity(new Intent(getBaseContext(), NewsActivity.class));
                finish();
                break;
            case 3:
                startActivity(new Intent(getBaseContext(), EventActivity.class));
                finish();
                break;
            case 4:
                startActivity(new Intent(getBaseContext(), ChatActivity.class));
                finish();
                break;
            case 5:
                startActivity(new Intent(getBaseContext(), EmployeeActivity.class));
                finish();
                break;
            case 6:
                startActivity(new Intent(getBaseContext(), LeaveActivity.class));
                finish();
                break;
            case 7:
                startActivity(new Intent(getBaseContext(), DownloadActivity.class));
                finish();
                break;
            case 8:
                startActivity(new Intent(getBaseContext(), GalleryActivity.class));
                finish();
                break;
            case 9:
                startActivity(new Intent(getBaseContext(), CatalogueActivity.class));
                finish();
                break;
            case 10:
                startActivity(new Intent(getBaseContext(), DriveActivity.class));
                finish();
                break;
            case 11:
                startActivity(new Intent(getBaseContext(), ProfileActivity.class));
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (!isPopUp) {
//            HandlerControl();
        }
    }

    public void getGson() {
        final String token = Utils.getToken(DashboardActivity.this);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Dashboard> mGsonRequest = new GsonRequest<Dashboard>(
                Request.Method.GET,
                Utils.API_URL + "api/Account/indicator",
                Dashboard.class,
                headers,
                responseListener(),
                errorListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<Dashboard> responseListener() {
        return new Response.Listener<Dashboard>() {
            @Override
            public void onResponse(Dashboard response) {
                txtError.setVisibility(View.GONE);
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getCalendarModule());
                getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Calendar"});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getCatalogueModule());
                getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Product Catalogue"});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getDownloadModule());
                getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Download Centre"});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getEventModule());
                getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Events"});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getNewsModule());
                getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"News"});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getLeaveModule());
                getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Leave"});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getGalleryModule());
                getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Media Gallery"});
                android.os.Message msg = handler.obtainMessage();
                handler.handleMessage(msg);
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(DashboardActivity.this, error, txtError);
            }
        };
    }

    public void buttonClick(View view) {
        fadeRL.setVisibility(View.VISIBLE);
        LayoutInflater inflater = (LayoutInflater) getBaseContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.window_dashboard, (ViewGroup) findViewById(R.id.lv), false);
        popupWindow = new PopupWindow(popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        popupWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.WHITE));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        Button btnDismiss = (Button) popupView.findViewById(R.id.dismiss);
        ImageView btnCancel = (ImageView) popupView.findViewById(R.id.btnCancel);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                fadeRL.setVisibility(View.GONE);
            }
        });
        toolbar.setOnClickListener(new Toolbar.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        btnDismiss.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        btnCancel.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent in = new Intent();
        in.setAction("StartkilledService");
        sendBroadcast(in);
        Log.d("debug", "Service Killed");
    }

    public void HandlerControl() {
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                btnOpenPopup.setPressed(true);
                btnOpenPopup.invalidate();
                Handler handler1 = new Handler();
                Runnable r1 = new Runnable() {
                    public void run() {
                        btnOpenPopup.setPressed(false);
                        btnOpenPopup.invalidate();
                        if (!isPopUp) {
                            btnOpenPopup.performClick();
                            isPopUp = true;
                        }
                    }

                };
                handler1.postDelayed(r1, 10);
            }
        };
        handler.postDelayed(r, 10);
    }

    @Override
    public void onBackPressed() {
        /*ExitDialogFragment exitDialogFragment = ExitDialogFragment.newInstance(DashboardActivity.this);
        exitDialogFragment.show(getSupportFragmentManager(), "");*/
        /*Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(a);
        finish();
        android.os.Process.killProcess(android.os.Process.myPid());*/
        moveTaskToBack(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (drawerFragment.mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (isPopUp) {
            outState.putBoolean("isPopUp", true);
        } else {
            outState.putBoolean("isPopUp", false);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (!savedInstanceState.getBoolean("isPopUp")) {
            isPopUp = false;
        } else
            isPopUp = true;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION)) {
                boolean isCalendar = intent.getBooleanExtra("isCalendar", false);
                boolean isEmployeeLeave = intent.getBooleanExtra("isEmployeeLeave", false);
                boolean isMyLeave = intent.getBooleanExtra("isMyLeave", false);
                int position = intent.getIntExtra("position", 0);
                com.applab.wcircle_pro.Calendar.Calendar calendar = intent.getParcelableExtra("calendar");
                Leave leave = intent.getParcelableExtra("leave");
                if (isCalendar) {
                    if (calendar == null) {
                        int i = position + 1;
                        setPopUpCalendar(arrCalendar, i, false);
                    } else {
                        arrCalendar.clear();
                        arrCalendar.add(calendar);
                        setPopUpCalendar(arrCalendar, position, true);
                    }
                } else if (isEmployeeLeave) {
                    if (leave == null) {
                        int i = position + 1;
                        setPopUpEmployeeLeave(arrEmployeeLeave, i, false);
                    } else {
                        arrEmployeeLeave.clear();
                        arrEmployeeLeave.add(leave);
                        setPopUpEmployeeLeave(arrEmployeeLeave, position, true);
                    }
                } else if (isMyLeave) {
                    int i = position + 1;
                    setPopUpMyLeave(arrMyLeave, i);
                }
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        Utils.setIsActivityOpen(false, getBaseContext(), false);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
        mIsStateAlreadySaved = true;
        if (mTimer != null) {
            mTimer.cancel();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setIsActivityOpen(true, getBaseContext(), true);
      /*  IntentFilter iff = new IntentFilter(ACTION);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
        Intent intent = new Intent(getBaseContext(), XMPPTimerIntentService.class);
        startService(intent);*/
        android.os.Message msg = handler1.obtainMessage();
        handler1.handleMessage(msg);
        mIsStateAlreadySaved = false;
        /*if (mTimer != null) {
            mTimer.cancel();
        } else {
            mTimer = new Timer();
        }
        try {
            mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);
        } catch (Exception ex) {
            ex.fillInStackTrace();
        }*/
    }

    class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    getGson();
                }
            });
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == 194) {
            Uri uri = DashboardProvider.CONTENT_URI;
            String selection = DBHelper.DASHBOARD_COLUMN_TITLE + "!=? AND " + DBHelper.DASHBOARD_COLUMN_TITLE + "!=? AND " + DBHelper.DASHBOARD_COLUMN_TITLE + "!=?";
            String[] selectionArgs = new String[]{"My Dashboard", "Log Out", "Settings"};
            return new CursorLoader(getBaseContext(), uri, null, selection, selectionArgs,
                    DBHelper.DASHBOARD_COLUMN_ID + " ASC ");
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == 194) {
            if (data != null) {
                if (data.getCount() > 0) {
                    this.cursor = data;
                    adapter.swapCursor(cursor);
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            ChatDBHelper.insertMessenger(DashboardActivity.this);
            getGson();
        }
        Utils.disableSwipyRefresh(mSwipyRefreshLayout);
    }
    //endregion

    //region calendar gson region
    private void setPopUpCalendar(ArrayList<com.applab.wcircle_pro.Calendar.Calendar> arrCalendar, int position, boolean isNotification) {
        if (calendarDialogFragment != null) {
            if (!calendarDialogFragment.isHidden()) {
                calendarDialogFragment.dismiss();
            }
        }
        if (arrCalendar != null) {
            if (arrCalendar.size() > 0) {
                if (position < arrCalendar.size()) {
                    if (!mIsStateAlreadySaved) {
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        calendarDialogFragment = DashboardCalendarDialogFragment.newInstance(DashboardActivity.this, arrCalendar.get(position), position);
                        calendarDialogFragment.setCancelable(false);
                        calendarDialogFragment.show(ft, "");
                    }
                } else {
                    if (!isNotification) {
                        pageNoEmployeeLeave = 1;
                        getEmployeeLeaveGson(pageNoEmployeeLeave);
                    }
                }
            } else {
                if (!isNotification) {
                    pageNoEmployeeLeave = 1;
                    getEmployeeLeaveGson(pageNoEmployeeLeave);
                }
            }
        } else {
            if (!isNotification) {
                pageNoEmployeeLeave = 1;
                getEmployeeLeaveGson(pageNoEmployeeLeave);
            }
        }
    }

    public static ArrayList<Attendee> getAttendee(Object calendarId, Context context) {
        Cursor cursor = null;
        ArrayList<Attendee> arrAttendee = new ArrayList<>();
        try {
            cursor = context.getContentResolver().query(AttendeesProvider.CONTENT_URI, null,
                    DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=?", new String[]{String.valueOf(calendarId)}, null);
            if (cursor != null) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    Attendee attendee = Attendee.getAttendee(cursor, i);
                    arrAttendee.add(attendee);
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrAttendee;
    }

    public void getCalendarGson(final int pageNoCalendar) {
        final String token = Utils.getToken(getBaseContext());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        final int calendarRow = getCalendarRow();
        final Date lastUpdateDate = getLastCalendarUpdate();
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Calendar/List?FromDate=" + Utils.setToUTCDate(Utils.DATE_FORMAT,
                "dd/MM/yyyy", Utils.setCalendarDate(Utils.DATE_FORMAT, new Date())) + "&NoPerPage="
                + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoCalendar) +
                "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(calendarRow == 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Calendar/List?FromDate=" + Utils.setToUTCDate(Utils.DATE_FORMAT,
                    Utils.DATE_FORMAT, Utils.setCalendarDate(Utils.DATE_FORMAT, new Date())) + "&NoPerPage="
                    + Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoCalendar) +
                    "&LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(calendarRow == 0);
        }
        final String finalUrl = url;
        Log.i(TAG, finalUrl);
        GsonRequest<CalendarChecking> mGsonRequest = new GsonRequest<CalendarChecking>(
                Request.Method.GET,
                finalUrl,
                CalendarChecking.class,
                headers,
                responseCalendarListener(pageNoCalendar),
                errorCalendarListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<CalendarChecking> responseCalendarListener(final int pageNoCalendar) {
        return new Response.Listener<CalendarChecking>() {
            @Override
            public void onResponse(CalendarChecking response) {
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), DashboardActivity.this);
                    } else {
                        if (response.getCalendar().size() > 0) {
                            //getContentResolver().delete(CalendarCheckingProvider.CONTENT_URI, null, null);
                            insertCalendar(response);
                        } else {
                            setPopUpCalendar(arrCalendar, 0, false);
                        }
                    }
                }
            }
        };
    }

    private Date getLastCalendarUpdate() {
        ArrayList<Date> arrayList = new ArrayList<>();
        String field = DBHelper.CALENDAR_CHECKING_COLUMN_LAST_UPDATE;
        String[] projection = {field};
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(CalendarCheckingProvider.CONTENT_URI, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    private int getCalendarRow() {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(CalendarProvider.CONTENT_URI, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return totalRow;
    }

    private boolean isAttendeesExists(Object calendarId, Object employeeId) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(AttendeesProvider.CONTENT_URI, null,
                    DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                    new String[]{String.valueOf(calendarId), String.valueOf(employeeId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    private boolean isCalendarExists(Object calendarId) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(CalendarProvider.CONTENT_URI, null,
                    DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?",
                    new String[]{String.valueOf(calendarId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    private Response.ErrorListener errorCalendarListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {

            }
        };
    }

    public void insertCalendar(CalendarChecking result) {
        DBHelper helper = new DBHelper(DashboardActivity.this);
        try {
            ContentValues contentValuesCalendarChecking = new ContentValues();
            contentValuesCalendarChecking.put(DBHelper.CALENDAR_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesCalendarChecking.put(DBHelper.CALENDAR_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesCalendarChecking.put(DBHelper.CALENDAR_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesCalendarChecking.put(DBHelper.CALENDAR_CHECKING_COLUMN_LEVEL, pageNoCalendar);
            getContentResolver().insert(CalendarCheckingProvider.CONTENT_URI, contentValuesCalendarChecking);
            ContentValues[] contentValueses = new ContentValues[result.getCalendar().size()];
            ArrayList<ContentValues> contentValuesArrayListAttendees = new ArrayList<>();
            for (int i = 0; i < result.getCalendar().size(); i++) {
                ContentValues contentValues = new ContentValues();
                String calendarTo = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, result.getCalendar().get(i).getCalendarTo());
                String showDate = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, result.getCalendar().get(i).getShowDate());
                String calendarFrom = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, result.getCalendar().get(i).getCalendarFrom());
                String reminderDate = result.getCalendar().get(i).getReminder() != null ? Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, result.getCalendar().get(i).getReminder()) : null;
                contentValues.put(DBHelper.CALENDAR_COLUMN_CALENDAR_ID, result.getCalendar().get(i).getId());
                contentValues.put(DBHelper.CALENDAR_COLUMN_TYPE, result.getCalendar().get(i).getType());
                contentValues.put(DBHelper.CALENDAR_COLUMN_TITLE, result.getCalendar().get(i).getTitle());
                contentValues.put(DBHelper.CALENDAR_COLUMN_ALL_DAY, result.getCalendar().get(i).getAllDay());
                contentValues.put(DBHelper.CALENDAR_COLUMN_START_DATE, calendarFrom);
                contentValues.put(DBHelper.CALENDAR_COLUMN_END_DATE, calendarTo);
                contentValues.put(DBHelper.CALENDAR_COLUMN_LOCATION, result.getCalendar().get(i).getLocation());
                contentValues.put(DBHelper.CALENDAR_COLUMN_DESCRIPTION, result.getCalendar().get(i).getDescription());
                contentValues.put(DBHelper.CALENDAR_COLUMN_REMINDER, reminderDate);
                contentValues.put(DBHelper.CALENDAR_COLUMN_STATUS, result.getCalendar().get(i).getStatus());
                contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_ID, result.getCalendar().get(i).getCreateById());
                contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_NAME, result.getCalendar().get(i).getCreateByName());
                contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_ID, result.getCalendar().get(i).getUpdateById());
                contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_NAME, result.getCalendar().get(i).getUpdateByName());
                contentValues.put(DBHelper.CALENDAR_COLUMN_SHOW_DATE, showDate);
                contentValues.put(DBHelper.CALENDAR_COLUMN_NEW_CREATE, String.valueOf(result.getCalendar().get(i).getNewCreate()));
                contentValues.put(DBHelper.CALENDAR_COLUMN_LEVEL, 1);
                contentValueses[i] = contentValues;
                if (result.getCalendar().get(i).getAttendees() != null) {
                    for (int j = 0; j < result.getCalendar().get(i).getAttendees().size(); j++) {
                        String remind = result.getCalendar().get(i).getAttendees().get(j).getReminder() != null ?
                                Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, result.getCalendar().get(i).getAttendees().get(j).getReminder()) : null;
                        ContentValues contentValuesAttendees = new ContentValues();
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SUBMIT, 1);
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SELECT, 1);
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_NAME, result.getCalendar().get(i).getAttendees().get(j).getEmployeyName());
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_LEVEL, pageNoCalendar);
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID, result.getCalendar().get(i).getAttendees().get(j).getEmployeeId());
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_CALENDAR_ID, result.getCalendar().get(i).getId());
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_REMINDER, remind);
                        contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_STATUS, result.getCalendar().get(i).getAttendees().get(j).getStatus());
                        if (!contentValuesArrayListAttendees.contains(contentValuesAttendees)) {
                            contentValuesArrayListAttendees.add(contentValuesAttendees);
                            if (result.getCalendar().get(i).getAttendees().get(j).getStatus().equals("pending") &&
                                    result.getCalendar().get(i).getAttendees().get(j).getEmployeeId().equals(Utils.getProfile(DashboardActivity.this).getId())) {
                                if (remind != null && result.getCalendar().get(i).getStatus().equals("active")) {
                                    Date date = Utils.setCalendarDate(Utils.DATE_FORMAT, remind);
                                    if (date.after(new Date())) {
                                        int year = CalendarActivity.getYear(remind);
                                        int month = CalendarActivity.getMonth(remind);
                                        int day = CalendarActivity.getDay(remind);
                                        int hour = CalendarActivity.getHour(remind, 0);
                                        int minutes = CalendarActivity.getMinutes(remind, 0);
                                        String msg = "";
                                        if (result.getCalendar().get(i).getAllDay()) {
                                            msg += "On " + Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy 'at' h:mm a", calendarFrom) + "\n";
                                        } else {
                                            msg += "On " + Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy 'at' h:mm a", calendarFrom) + "\n";
                                        }
                                        if (result.getCalendar().get(i).getTitle().equals("testing")) {
                                            Log.i(TAG, "Testing: " + msg);
                                        }
                                        CalendarActivity.setAlarm(year, month, day, hour, minutes,
                                                msg, result.getCalendar().get(i),
                                                new ArrayList<Attendee>(result.getCalendar().get(i).getAttendees()), DashboardActivity.this);
                                    }
                                }
                            }
                            /*getContentResolver().delete(AttendeesProvider.CONTENT_URI,
                                    DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " + DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                                    new String[]{String.valueOf(result.getCalendar().get(i).getId()),
                                            String.valueOf(result.getCalendar().get(i).getAttendees().get(j).getEmployeeId())});*/

                            if (result.getCalendar().get(i).getStatus().equals("active")) {
                                if (result.getCalendar().get(i).getType().equals("P")) {
                                    if (result.getCalendar().get(i).getAttendees().get(j).getEmployeeId().equals(Utils.getProfile(DashboardActivity.this).getId())) {
                                        if (result.getCalendar().get(i).getAttendees().get(j).getStatus().equals("pending")) {
                                            if (Utils.setCalendarDate(Utils.DATE_FORMAT, calendarTo).after(new Date())) {
                                                if (!DashboardCalendarReminderDialogFragment.isCalendarReminderExists(result.getCalendar().get(i).getId(), DashboardActivity.this)) {
                                                    arrCalendar.add(result.getCalendar().get(i));
                                                } else {
                                                    String reminder = DashboardCalendarReminderDialogFragment.getReminder(result.getCalendar().get(i).getId(), DashboardActivity.this);
                                                    if (reminder != null) {
                                                        Date d = Utils.setCalendarDate(Utils.DATE_FORMAT, reminder);
                                                        if (d.before(new Date())) {
                                                            arrCalendar.add(result.getCalendar().get(i));
                                                        }
                                                    } else {
                                                        arrCalendar.add(result.getCalendar().get(i));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (result.getCalendar().get(i).getStatus().equals("active") && Utils.setCalendarDate(Utils.DATE_FORMAT, calendarTo).after(new Date()) &&
                        (result.getCalendar().get(i).getType().equals("B") || result.getCalendar().get(i).getType().equals("C"))) {
                    if (result.getCalendar().get(i).getNewCreate()) {
                        arrCalendar.add(result.getCalendar().get(i));
                    }
                }
                /*getContentResolver().delete(CalendarProvider.CONTENT_URI, DBHelper.CALENDAR_COLUMN_CALENDAR_ID + "=?",
                        new String[]{String.valueOf(result.getCalendar().get(i).getId())});*/

                if (result.getCalendar().get(i).getCreateById().equals(String.valueOf(Utils.getProfile(DashboardActivity.this).getId())) ||
                        result.getCalendar().get(i).getType().equals("C") || result.getCalendar().get(i).getType().equals("B")) {
                    if (reminderDate != null && result.getCalendar().get(i).getStatus().equals("active")) {
                        Date date = Utils.setCalendarDate(Utils.DATE_FORMAT, reminderDate);
                        if (date.after(new Date())) {
                            int year = CalendarActivity.getYear(reminderDate);
                            int month = CalendarActivity.getMonth(reminderDate);
                            int day = CalendarActivity.getDay(reminderDate);
                            int hour = CalendarActivity.getHour(reminderDate, 0);
                            int minutes = CalendarActivity.getMinutes(reminderDate, 0);
                            String msg = "";
                            if (result.getCalendar().get(i).getAllDay()) {
                                msg += "On " + Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy 'at' h:mm a", calendarFrom) + "\n";
                            } else {
                                msg += "On " + Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy 'at' h:mm a", calendarFrom) + "\n";
                            }
                            if (result.getCalendar().get(i).getTitle().equals("testing")) {
                                Log.i(TAG, "Testing: " + msg);
                            }
                            CalendarActivity.setAlarm(year, month, day, hour, minutes,
                                    msg, result.getCalendar().get(i),
                                    new ArrayList<Attendee>(result.getCalendar().
                                            get(i).getAttendees()), DashboardActivity.this);
                        }
                    }
                }
            }
            boolean isEmpty = true;
            for (Object ob : contentValueses) {
                if (ob != null) {
                    isEmpty = false;
                }
            }
            long rowsInserted = 0;
            if (!isEmpty) {
                //rowsInserted = getContentResolver().bulkInsert(CalendarProvider.CONTENT_URI, contentValueses);
                Log.i(TAG, String.valueOf(rowsInserted));
            }

            isEmpty = true;
            if (contentValuesArrayListAttendees.size() > 0) {
                contentValueses = new ContentValues[contentValuesArrayListAttendees.size()];
                contentValueses = contentValuesArrayListAttendees.toArray(contentValueses);
                for (Object ob : contentValueses) {
                    if (ob != null) {
                        isEmpty = false;
                    }
                }
                rowsInserted = 0;
                if (!isEmpty) {
                    //rowsInserted = getContentResolver().bulkInsert(AttendeesProvider.CONTENT_URI, contentValueses);
                    Log.i(TAG, String.valueOf(rowsInserted));
                }
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        } finally {
            pageNoCalendar++;
            getCalendarGson(pageNoCalendar);
        }
    }
    //endregion

    //region employee leave gson

    private void setPopUpEmployeeLeave(ArrayList<Leave> arrEmployeeLeave, int position, boolean isNotification) {
        if (employeeLeaveDialogFragment != null) {
            if (!employeeLeaveDialogFragment.isHidden()) {
                employeeLeaveDialogFragment.dismiss();
            }
        }
        if (arrEmployeeLeave != null) {
            if (arrEmployeeLeave.size() > 0) {
                if (position < arrEmployeeLeave.size()) {
                    if (!mIsStateAlreadySaved) {
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        employeeLeaveDialogFragment = DashboardEmployeeLeaveDialogFragment.newInstance(DashboardActivity.this, arrEmployeeLeave.get(position), position);
                        employeeLeaveDialogFragment.setCancelable(false);
                        employeeLeaveDialogFragment.show(ft, "");
                    }
                } else {
                    if (!isNotification) {
                        getMyLeaveGson();
                    }
                }
            } else {
                if (!isNotification) {
                    getMyLeaveGson();
                }
            }
        } else {
            if (!isNotification) {
                getMyLeaveGson();
            }
        }
    }

    public void getEmployeeLeaveGson(final int pageNoEmployeeLeave) {
        final String token = Utils.getToken(DashboardActivity.this);
        final Date lastUpdateDate = getEmployeeLastLeaveUpdate();
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Leave/Other?NoPerPage=" +
                Utils.encode(Utils.PAGING_NUMBER) + "&PageNo="
                + Utils.encode(pageNoEmployeeLeave) + "&LastUpdated=" +
                Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getEmployeeLeaveRow() == 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Leave/Other?NoPerPage=" +
                    Utils.encode(Utils.PAGING_NUMBER) + "&PageNo=" + Utils.encode(pageNoEmployeeLeave)
                    + "&LastUpdated=" + Utils.encode(lastUpdate) +
                    "&ReturnEmpty=" + Utils.encode(getEmployeeLeaveRow() == 0);
        }
        final String finalUrl = url;
        Log.i(TAG, url);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<LeaveChecking> mGsonRequest = new GsonRequest<LeaveChecking>(
                Request.Method.GET,
                finalUrl,
                LeaveChecking.class,
                headers,
                responseEmployeeLeaveListener(pageNoEmployeeLeave),
                errorEmployeeLeaveListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<LeaveChecking> responseEmployeeLeaveListener(final int pageNo) {
        return new Response.Listener<LeaveChecking>() {
            @Override
            public void onResponse(LeaveChecking response) {
                txtError.setVisibility(View.GONE);
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), DashboardActivity.this);
                    } else {
                        if (response.getLeaves().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getEmployeeLastLeaveUpdate();
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    if (pageNo == 1) {
                                        getContentResolver().delete(LeaveProvider.CONTENT_URI,
                                                DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                                new String[]{String.valueOf(true)});
                                        getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                                DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                                new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                                DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=? AND " +
                                                        DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_PAGE + "=?",
                                                new String[]{String.valueOf(true), String.valueOf(pageNo)});
                                    }
                                    insertEmployeeLeave(response);
                                } else {
                                    if (pageNo == 1) {
                                        getContentResolver().delete(LeaveProvider.CONTENT_URI,
                                                DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                                new String[]{String.valueOf(true)});
                                        getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                                DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                                new String[]{String.valueOf(true)});
                                    } else if (pageNo > 1) {
                                        getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                                DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=? AND " +
                                                        DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_PAGE + "=?",
                                                new String[]{String.valueOf(true), String.valueOf(pageNo)});
                                    }
                                    insertEmployeeLeave(response);
                                }
                            } else {
                                if (pageNo == 1) {
                                    getContentResolver().delete(LeaveProvider.CONTENT_URI,
                                            DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                            new String[]{String.valueOf(true)});
                                    getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                            DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                            new String[]{String.valueOf(true)});
                                } else if (pageNo > 1) {
                                    getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                            DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=? AND " +
                                                    DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_PAGE + "=?",
                                            new String[]{String.valueOf(true), String.valueOf(pageNo)});
                                }
                                insertEmployeeLeave(response);
                            }
                        } else {
                            if (pageNo == 1) {
                                if (response.getNoOfRecords() == 0) {
                                    getContentResolver().delete(LeaveProvider.CONTENT_URI,
                                            DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                            new String[]{String.valueOf(true)});
                                    getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI,
                                            DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                                            new String[]{String.valueOf(true)});
                                }
                            }
                            setPopUpEmployeeLeave(arrEmployeeLeave, 0, false);
                        }
                    }
                }
            }
        };
    }

    private Response.ErrorListener errorEmployeeLeaveListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(DashboardActivity.this, error, txtError);
            }
        };
    }

    private int getEmployeeLeaveRow() {
        int totalRow = 0;
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(LeaveProvider.CONTENT_URI, null, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                    new String[]{String.valueOf(true)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return totalRow;
    }

    private Date getEmployeeLastLeaveUpdate() {
        ArrayList<Date> arrayList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(LeaveCheckingProvider.CONTENT_URI, null, DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(true)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public void insertEmployeeLeave(LeaveChecking result) {
        DBHelper helper = new DBHelper(DashboardActivity.this);
        try {
            ContentValues contentValuesLeaveChecking = new ContentValues();
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_PER_PAGE, result.getNoPerPage());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_PAGE_NO, pageNoEmployeeLeave);
            getContentResolver().insert(LeaveCheckingProvider.CONTENT_URI, contentValuesLeaveChecking);
            ContentValues[] contentValueses = new ContentValues[result.getLeaves().size()];
            for (int i = 0; i < result.getLeaves().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_ID, result.getLeaves().get(i).getId());
                contentValues.put(DBHelper.LEAVE_COLUMN_NAME, result.getLeaves().get(i).getName());
                contentValues.put(DBHelper.LEAVE_COLUMN_EMPLOYEE_ID, result.getLeaves().get(i).getEmployeeId());
                contentValues.put(DBHelper.LEAVE_COLUMN_DEPARTMENT, result.getLeaves().get(i).getDepartment());
                contentValues.put(DBHelper.LEAVE_COLUMN_POSITION, result.getLeaves().get(i).getPosition());
                contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR, result.getLeaves().get(i).getSuperior());
                contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR_ID, result.getLeaves().get(i).getSuperiorId());
                contentValues.put(DBHelper.LEAVE_COLUMN_TYPE_CODE, result.getLeaves().get(i).getTypeCode());
                contentValues.put(DBHelper.LEAVE_COLUMN_TYPE, result.getLeaves().get(i).getType());
                contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, result.getLeaves().get(i).getRemarks());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_DAYS, result.getLeaves().get(i).getLeaveDays());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_FROM, result.getLeaves().get(i).getLeaveFrom());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_TO, result.getLeaves().get(i).getLeaveTo());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, result.getLeaves().get(i).getStatus());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_REMARKS, result.getLeaves().get(i).getStatusRemark());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_BY, result.getLeaves().get(i).getStatusUpdateBy());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_ON, result.getLeaves().get(i).getStatusUpdateOn());
                contentValues.put(DBHelper.LEAVE_COLUMN_LAST_UPDATE, result.getLeaves().get(i).getLastUpdate());
                contentValues.put(DBHelper.LEAVE_COLUMN_NEW_CREATE, String.valueOf(result.getLeaves().get(i).getNewCreate()));
                if (result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(DashboardActivity.this).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                } else if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(DashboardActivity.this).getId()) &&
                        !result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(DashboardActivity.this).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                } else if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(DashboardActivity.this).getId()) &&
                        result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(DashboardActivity.this).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
                }
                contentValues.put(DBHelper.LEAVE_COLUMN_PAGE_NO, pageNoEmployeeLeave);
                contentValues.put(DBHelper.LEAVE_COLUMN_CREATE_DATE, result.getLeaves().get(i).getCreateDate());
                if (!DashboardLeaveReminderDialogFragment.isLeaveReminderExists(result.getLeaves().get(i).getId(), DashboardActivity.this)) {
                    if (result.getLeaves().get(i).getStatus().equals("pending")) {
                        if (Utils.setCalendarDate(Utils.DATE_FORMAT, Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, result.getLeaves().get(i).getLeaveFrom())).after(new Date())) {
                            if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(DashboardActivity.this).getId()) &&
                                    !result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(DashboardActivity.this).getId())) {

                            } else {
                                arrEmployeeLeave.add(result.getLeaves().get(i));
                            }
                        }
                    }
                } else {
                    String reminder = DashboardLeaveReminderDialogFragment.getReminder(result.getLeaves().get(i).getId(), DashboardActivity.this);
                    if (result.getLeaves().get(i).getStatus().equals("pending")) {
                        if (reminder != null) {
                            if (Utils.setCalendarDate(Utils.DATE_FORMAT, reminder).before(new Date())) {
                                if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(DashboardActivity.this).getId()) &&
                                        !result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(DashboardActivity.this).getId())) {

                                } else {
                                    arrEmployeeLeave.add(result.getLeaves().get(i));
                                }
                            }
                        } else {
                            if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(DashboardActivity.this).getId()) &&
                                    !result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(DashboardActivity.this).getId())) {

                            } else {
                                arrEmployeeLeave.add(result.getLeaves().get(i));
                            }
                        }
                    }
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                getContentResolver().bulkInsert(LeaveProvider.CONTENT_URI, contentValueses);
            }

        } catch (Exception ex) {
            Log.i(TAG, "Content Provider My Leave Error :" + ex.toString());
        } finally {
            pageNoEmployeeLeave++;
            getEmployeeLeaveGson(pageNoEmployeeLeave);
        }
    }
    //endregion

    //region my leave gson
    private void setPopUpMyLeave(ArrayList<Leave> arrMyLeave, int position) {
        if (myLeaveDialogFragment != null) {
            if (!myLeaveDialogFragment.isHidden()) {
                myLeaveDialogFragment.dismiss();
            }
        }
        if (arrMyLeave != null) {
            if (arrMyLeave.size() > 0) {
                if (position < arrMyLeave.size()) {
                    if (!mIsStateAlreadySaved) {
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        myLeaveDialogFragment = DashboardMyLeaveDialogFragment.newInstance(DashboardActivity.this, arrMyLeave.get(position), position);
                        myLeaveDialogFragment.setCancelable(false);
                        myLeaveDialogFragment.show(ft, "");
                    }
                }
            }
        }
    }

    public void getMyLeaveGson() {
        final String token = Utils.getToken(DashboardActivity.this);
        final Date lastUpdateDate = getMyLastUpdate();
        String lastUpdate = "2000-09-09T10:16:25z";
        String url = Utils.API_URL + "api/Leave/My?LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getMyLeaveRow() == 0);
        if (lastUpdateDate != null) {
            lastUpdate = Utils.setCalendarDate(Utils.DATE_FORMAT, lastUpdateDate);
            url = Utils.API_URL + "api/Leave/My?LastUpdated=" + Utils.encode(lastUpdate) + "&ReturnEmpty=" + Utils.encode(getMyLeaveRow() == 0);
        }
        final String finalUrl = url;
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<LeaveChecking> mGsonRequest = new GsonRequest<LeaveChecking>(
                Request.Method.GET,
                finalUrl,
                LeaveChecking.class,
                headers,
                responseMyLeaveListener(),
                errorMyLeaveListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<LeaveChecking> responseMyLeaveListener() {
        return new Response.Listener<LeaveChecking>() {
            @Override
            public void onResponse(LeaveChecking response) {
                txtError.setVisibility(View.GONE);
                if (response != null) {
                    if (response.getMessage() != null) {
                        Utils.alertDialog(response.getMessage(), DashboardActivity.this);
                    } else {
                        if (response.getLeaves().size() > 0) {
                            Date date1 = Utils.setCalendarDate(Utils.DATE_FORMAT, response.getLastUpdate());
                            Date date2 = getMyLastUpdate();
                            if (date2 != null) {
                                if (date2.before(date1)) {
                                    getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                    getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                    insertMyLeave(response);
                                } else {
                                    getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                    getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                    insertMyLeave(response);
                                }
                            } else {
                                getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                insertMyLeave(response);
                            }
                        } else {
                            if (response.getNoOfRecords() == 0) {
                                getContentResolver().delete(LeaveProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                                getContentResolver().delete(LeaveCheckingProvider.CONTENT_URI, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?", new String[]{String.valueOf(false)});
                            }
                            setPopUpMyLeave(arrMyLeave, 0);
                        }
                    }
                }
            }
        };
    }

    private Response.ErrorListener errorMyLeaveListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(DashboardActivity.this, error, txtError);
            }
        };
    }

    private int getMyLeaveRow() {
        int totalRow = 0;
        Cursor cursor = getBaseContext().getContentResolver().query(LeaveProvider.CONTENT_URI, null, DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                new String[]{String.valueOf(false)}, null);
        try {

            if (cursor != null && cursor.moveToFirst()) {
                totalRow = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return totalRow;
    }

    private Date getMyLastUpdate() {
        ArrayList<Date> arrayList = new ArrayList<>();
        Cursor cursor = getBaseContext().getContentResolver().query(LeaveCheckingProvider.CONTENT_URI,
                null, DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE + "=?",
                new String[]{String.valueOf(false)}, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToFirst();
                do {
                    arrayList.add(Utils.setCalendarDate(Utils.DATE_FORMAT,
                            cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_CHECKING_COLUMN_LAST_UPDATE))));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Collections.sort(arrayList, Collections.reverseOrder());
        return arrayList.size() == 0 ? null : arrayList.get(0);
    }

    public void insertMyLeave(LeaveChecking result) {
        DBHelper helper = new DBHelper(DashboardActivity.this);
        try {
            ContentValues contentValuesLeaveChecking = new ContentValues();
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_LAST_UPDATE, result.getLastUpdate());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_RECORDS, result.getNoOfRecords());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_PER_PAGE, result.getNoPerPage());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_NO_OF_PAGE, result.getNoOfPage());
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
            contentValuesLeaveChecking.put(DBHelper.LEAVE_CHECKING_COLUMN_PAGE_NO, 0);
            getContentResolver().insert(LeaveCheckingProvider.CONTENT_URI, contentValuesLeaveChecking);
            ContentValues[] contentValueses = new ContentValues[result.getLeaves().size()];
            for (int i = 0; i < result.getLeaves().size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_ID, result.getLeaves().get(i).getId());
                contentValues.put(DBHelper.LEAVE_COLUMN_NAME, result.getLeaves().get(i).getName());
                contentValues.put(DBHelper.LEAVE_COLUMN_EMPLOYEE_ID, result.getLeaves().get(i).getEmployeeId());
                contentValues.put(DBHelper.LEAVE_COLUMN_DEPARTMENT, result.getLeaves().get(i).getDepartment());
                contentValues.put(DBHelper.LEAVE_COLUMN_POSITION, result.getLeaves().get(i).getPosition());
                contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR, result.getLeaves().get(i).getSuperior());
                contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR_ID, result.getLeaves().get(i).getSuperiorId());
                contentValues.put(DBHelper.LEAVE_COLUMN_TYPE_CODE, result.getLeaves().get(i).getTypeCode());
                contentValues.put(DBHelper.LEAVE_COLUMN_TYPE, result.getLeaves().get(i).getType());
                contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, result.getLeaves().get(i).getRemarks());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_DAYS, result.getLeaves().get(i).getLeaveDays());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_FROM, result.getLeaves().get(i).getLeaveFrom());
                contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_TO, result.getLeaves().get(i).getLeaveTo());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, result.getLeaves().get(i).getStatus());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_REMARKS, result.getLeaves().get(i).getStatusRemark());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_BY, result.getLeaves().get(i).getStatusUpdateBy());
                contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_ON, result.getLeaves().get(i).getStatusUpdateOn());
                contentValues.put(DBHelper.LEAVE_COLUMN_LAST_UPDATE, result.getLeaves().get(i).getLastUpdate());
                if (result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(DashboardActivity.this).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                } else if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(DashboardActivity.this).getId()) &&
                        !result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(DashboardActivity.this).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
                } else if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(DashboardActivity.this).getId()) &&
                        result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(DashboardActivity.this).getId())) {
                    contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
                }
                contentValues.put(DBHelper.LEAVE_COLUMN_NEW_CREATE, String.valueOf(result.getLeaves().get(i).getNewCreate()));
                contentValues.put(DBHelper.LEAVE_COLUMN_PAGE_NO, 0);
                contentValues.put(DBHelper.LEAVE_COLUMN_CREATE_DATE, result.getLeaves().get(i).getCreateDate());
                if (result.getLeaves().get(i).getNewCreate()) {
                    if (result.getLeaves().get(i).getStatus().equals("approved") || result.getLeaves().get(i).getStatus().equals("rejected")) {
                        if (!result.getLeaves().get(i).getSuperiorId().equals(Utils.getProfile(DashboardActivity.this).getId()) &&
                                !result.getLeaves().get(i).getEmployeeId().equals(Utils.getProfile(DashboardActivity.this).getId())) {
                        } else {
                            arrMyLeave.add(result.getLeaves().get(i));
                        }
                    }
                }
                contentValueses[i] = contentValues;
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                getContentResolver().bulkInsert(LeaveProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider My Leave Error :" + ex.toString());
        } finally {
            setPopUpMyLeave(arrMyLeave, 0);
        }
    }
    //endregion
}
