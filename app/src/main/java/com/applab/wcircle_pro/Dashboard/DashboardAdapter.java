package com.applab.wcircle_pro.Dashboard;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
;
import com.applab.wcircle_pro.R;

/**
 * Created by user on 6/12/2015.
 */
public class DashboardAdapter extends RecyclerView.Adapter<DashboardViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;

    public DashboardAdapter(Context context, Cursor cursor) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public DashboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("DashboardViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_dashboard_row, parent, false);
        DashboardViewHolder holder = new DashboardViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(DashboardViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Dashboard current = Dashboard.getDashboard(cursor, position);
        if (current.getNo() == 0) {
            holder.txtNo.setVisibility(View.GONE);
        } else {
            holder.txtNo.setVisibility(View.VISIBLE);
            if (current.getNo() > 9) {
                holder.txtNo.setText("+9");
            } else {
                holder.txtNo.setText(String.valueOf(current.getNo()));
            }
        }
        switch (current.getTitle()) {
            case "My Favourite":
                holder.imgIcon.setImageResource(R.drawable.dashboard_my_favourite);
                holder.txtTitle.setText(R.string.title_activity_my_favorite);
                break;
            case "Calendar":
                holder.imgIcon.setImageResource(R.drawable.dashboard_calendar);
                holder.txtTitle.setText(R.string.title_activity_calendar);
                break;
            case "News":
                holder.imgIcon.setImageResource(R.drawable.dashboard_news);
                holder.txtTitle.setText(R.string.title_activity_news);
                break;
            case "Events":
                holder.imgIcon.setImageResource(R.drawable.dashboard_events);
                holder.txtTitle.setText(R.string.title_activity_event);
                break;
            case "Messenger":
                holder.imgIcon.setImageResource(R.drawable.dashboard_messenger);
                holder.txtTitle.setText(R.string.title_activity_chat);
                break;
            case "Staff Directory":
                holder.imgIcon.setImageResource(R.drawable.dashboard_staff_directory);
                holder.txtTitle.setText(R.string.title_activity_employee);
                break;
            case "Leave":
                holder.imgIcon.setImageResource(R.drawable.dashboard_leave);
                holder.txtTitle.setText(R.string.title_activity_leave);
                break;
            case "Download Centre":
                holder.imgIcon.setImageResource(R.drawable.dashboard_download_centre);
                holder.txtTitle.setText(R.string.title_activity_download);
                break;
            case "Media Gallery":
                holder.imgIcon.setImageResource(R.drawable.dashboard_media_gallery);
                holder.txtTitle.setText(R.string.title_activity_gallery);
                break;
            case "Product Catalogue":
                holder.imgIcon.setImageResource(R.drawable.dashboard_product_catalog);
                holder.txtTitle.setText(R.string.title_activity_catalogue);
                break;
            case "My Drive":
                holder.imgIcon.setImageResource(R.drawable.dashboard_my_drive);
                holder.txtTitle.setText(R.string.title_activity_drive);
                break;
            case "My Profile":
                holder.imgIcon.setImageResource(R.drawable.dashboard_my_profile);
                holder.txtTitle.setText(R.string.title_activity_profile);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            DashboardAdapter.this.notifyDataSetChanged();
        }
    };
}
