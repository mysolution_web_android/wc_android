package com.applab.wcircle_pro.Dashboard;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Calendar.AttendeesProvider;
import com.applab.wcircle_pro.Calendar.Calendar;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.JsonObject;

import java.util.HashMap;

/**
 * Created by user on 27/10/2015.
 */
public class DashboardCalendarRejectDialogFragment extends DialogFragment {
    private Calendar mCalendar;
    private TextView mTxtTitle, mTxtDetails;
    private LinearLayout mBtnYes, mBtnNo, mBtnExit;
    private ImageView mBtnCancel;
    private String TAG = "REJECT_DELETE";
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private int mPosition;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DashboardCalendarRejectDialogFragment newInstance(Calendar calendar, Context context, int position) {
        DashboardCalendarRejectDialogFragment frag = new DashboardCalendarRejectDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("calendar", calendar);
        args.putInt("position", position);
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mCalendar = getArguments().getParcelable("calendar");
        mPosition = getArguments().getInt("position");

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_calendar_event_details, null);

        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtDetails = (TextView) v.findViewById(R.id.txtDetails);

        mBtnYes = (LinearLayout) v.findViewById(R.id.btnYes);
        mBtnNo = (LinearLayout) v.findViewById(R.id.btnNo);
        mBtnExit = (LinearLayout) v.findViewById(R.id.btnExit);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);

        mBtnYes.setOnClickListener(btnYesOnClickListener);
        mBtnNo.setOnClickListener(btnNoOnClickListener);
        mBtnExit.setOnClickListener(btnExitOnClickListener);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        mTxtTitle.setText(getResources().getString(R.string.reject));
        mTxtDetails.setText(getResources().getString(R.string.reject_msg));

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnYesOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            rejectEvent(true);
            DashboardCalendarRejectDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnNoOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            rejectEvent(false);
            DashboardCalendarRejectDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnExitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DashboardCalendarRejectDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DashboardCalendarRejectDialogFragment.this.getDialog().cancel();
        }
    };

    //region reject region
    public void rejectEvent(boolean removeFromList) {
        final String token = Utils.getToken(mContext);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        final String finalUrl = Utils.API_URL + "api/Calendar/Attendees/Rejected?id="
                + mCalendar.getId() + "&RemoveFromList=" + removeFromList;
        Log.i(TAG, finalUrl);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.PUT,
                finalUrl,
                JsonObject.class,
                headers,
                responseRejectListener(removeFromList),
                errorRejectListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<JsonObject> responseRejectListener(final boolean removeFromList) {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.ATTENDEES_COLUMN_STATUS, "reject");
                if (removeFromList) {
                    mContext.getContentResolver().delete(AttendeesProvider.CONTENT_URI,
                            DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " +
                                    DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                            new String[]{String.valueOf(mCalendar.getId()),
                                    String.valueOf(Utils.getProfile(mContext).getId())});
                } else {
                    mContext.getContentResolver().update(AttendeesProvider.CONTENT_URI, contentValues,
                            DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " +
                                    DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                            new String[]{String.valueOf(mCalendar.getId()),
                                    String.valueOf(Utils.getProfile(mContext).getId())});
                }
                Intent intent = new Intent(DashboardActivity.ACTION);
                intent.putExtra("isCalendar", true);
                intent.putExtra("isEmployeeLeave", false);
                intent.putExtra("isMyLeave", false);
                intent.putExtra("position", mPosition);
                DashboardCalendarDialogFragment.setRead(mContext, mCalendar, TAG);
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
                mgr.sendBroadcast(intent);
            }
        };
    }

    private Response.ErrorListener errorRejectListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(mContext, error);
            }
        };
    }
    //endregion
}

