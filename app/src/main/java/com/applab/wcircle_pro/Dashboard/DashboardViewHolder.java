package com.applab.wcircle_pro.Dashboard;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 6/12/2015.
 */
public class DashboardViewHolder extends RecyclerView.ViewHolder {
    TextView txtTitle;
    ImageView imgIcon;
    TextView txtNo;

    public DashboardViewHolder(View itemView) {
        super(itemView);
        txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
        txtNo = (TextView) itemView.findViewById(R.id.txtNo);
        imgIcon = (ImageView) itemView.findViewById(R.id.imgIcon);
    }
}
