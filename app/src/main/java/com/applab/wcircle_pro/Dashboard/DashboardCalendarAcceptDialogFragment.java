package com.applab.wcircle_pro.Dashboard;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Calendar.Attendee;
import com.applab.wcircle_pro.Calendar.Calendar;
import com.applab.wcircle_pro.Calendar.CalendarActivity;
import com.applab.wcircle_pro.Calendar.CalendarDetailsActivity;
import com.applab.wcircle_pro.Calendar.CalendarSpinnerAdapter;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by user on 27/10/2015.
 */
public class DashboardCalendarAcceptDialogFragment extends DialogFragment {
    private Calendar mCalendar;
    private LinearLayout mBtnSubmit, mBtnExit;
    private ImageView mBtnCancel;
    private String TAG = "CALENDAR ATTEND";
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private CalendarSpinnerAdapter calendarSpinnerAdapter;
    private Spinner mSpReminder;
    private String mStartDate;
    private int mPosition;
    private ArrayList<Attendee> mArrAttendee;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DashboardCalendarAcceptDialogFragment newInstance(Calendar calendar, Context context,
                                                                    String startDate, int position, ArrayList<Attendee> arrAttendee) {
        DashboardCalendarAcceptDialogFragment frag = new DashboardCalendarAcceptDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("calendar", calendar);
        args.putString("startDate", startDate);
        args.putInt("position", position);
        args.putParcelableArrayList("arrAttendee", arrAttendee);
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mCalendar = getArguments().getParcelable("calendar");
        mStartDate = getArguments().getString("startDate");
        mPosition = getArguments().getInt("position");
        mArrAttendee = getArguments().getParcelableArrayList("arrAttendee");

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_reminder, null);

        mSpReminder = (Spinner) v.findViewById(R.id.spReminder);
        calendarSpinnerAdapter = new CalendarSpinnerAdapter(CalendarDetailsActivity.getListReminder(),
                getActivity());
        mSpReminder.setAdapter(calendarSpinnerAdapter);

        mBtnSubmit = (LinearLayout) v.findViewById(R.id.btnSubmit);
        mBtnExit = (LinearLayout) v.findViewById(R.id.btnExit);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);

        mBtnSubmit.setOnClickListener(btnSubmitOnClickListener);
        mBtnExit.setOnClickListener(btnExitOnClickListener);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            attendEvent(CalendarDetailsActivity.getReminder(mStartDate, mSpReminder.getSelectedItemPosition()));
            DashboardCalendarAcceptDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnExitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DashboardCalendarAcceptDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DashboardCalendarAcceptDialogFragment.this.getDialog().cancel();
        }
    };

    //region attend region
    public void attendEvent(String reminder) {
        final String token = Utils.getToken(mContext);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        final String finalUrl = Utils.API_URL + "api/Calendar/Attendees/Attend?id=" + Utils.encode(mCalendar.getId()) + reminder;
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.PUT,
                finalUrl,
                JsonObject.class,
                headers,
                responseAttendListener(),
                errorAttendListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<JsonObject> responseAttendListener() {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                Intent intent = new Intent(DashboardActivity.ACTION);
                intent.putExtra("isCalendar", true);
                intent.putExtra("isEmployeeLeave", false);
                intent.putExtra("isMyLeave", false);
                intent.putExtra("position", mPosition);
                DashboardActivity.mgr.sendBroadcast(intent);
                if (mSpReminder.getSelectedItemPosition() != 0) {
                    String reminder = Utils.setCalendarDate(Utils.DATE_FORMAT_PRINT, Utils.DATE_FORMAT, mStartDate);
                    if (Utils.setCalendarDate(Utils.DATE_FORMAT, reminder).after(new Date())) {
                        int year = CalendarActivity.getYear(reminder);
                        int month = CalendarActivity.getMonth(reminder);
                        int day = CalendarActivity.getDay(reminder);
                        int hour = CalendarActivity.getHour(reminder, mSpReminder.getSelectedItemPosition());
                        int minutes = CalendarActivity.getMinutes(reminder, mSpReminder.getSelectedItemPosition());
                        String msg = "";
                        if (mCalendar.getAllDay()) {
                            msg += "On " + Utils.setDate(Utils.DATE_FORMAT, "d MMM yyyy", mCalendar.getCalendarFrom()) + "\n";
                        } else {
                            msg += "On " + Utils.setDate(Utils.DATE_FORMAT, "d MMM yyyy 'at' h:mm a", mCalendar.getCalendarFrom()) + "\n";
                        }
                        CalendarActivity.setAlarm(year, month, day, hour, minutes,
                                msg, mCalendar, mArrAttendee, mContext);
                    }
                }
                DashboardCalendarDialogFragment.setRead(mContext, mCalendar, TAG);
            }
        };
    }

    private Response.ErrorListener errorAttendListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(mContext, error);
            }
        };
    }

    //endregion
}

