package com.applab.wcircle_pro.Dashboard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.HashMap;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by user on 18/12/2015.
 */
public class HttpHelper {

    public static void getIndicator(Context context, String TAG, TextView txtError) {
        final String token = Utils.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Dashboard> mGsonRequest = new GsonRequest<Dashboard>(
                Request.Method.GET,
                Utils.API_URL + "api/Account/indicator",
                Dashboard.class,
                headers,
                responseIndicatorListener(context, txtError),
                errorIndicatorListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Dashboard> responseIndicatorListener(final Context context, final TextView txtError) {
        return new Response.Listener<Dashboard>() {
            @Override
            public void onResponse(Dashboard response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getCalendarModule());
                context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Calendar"});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getCatalogueModule());
                context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Product Catalogue"});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getDownloadModule());
                context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Download Centre"});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getEventModule());
                context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Events"});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getNewsModule());
                context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"News"});
                contentValues = new ContentValues();
                contentValues.put(DBHelper.DASHBOARD_COLUMN_NO, response.getLeaveModule());
                context.getContentResolver().update(DashboardProvider.CONTENT_URI,
                        contentValues, DBHelper.DASHBOARD_COLUMN_TITLE + "=?", new String[]{"Leave"});
                int total = getTotalNotification(context);
                if (total > 0) {
                    ShortcutBadger.applyCount(context, 1);
                } else {
                    ShortcutBadger.removeCount(context);
                }
            }
        };
    }

    public static Response.ErrorListener errorIndicatorListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utils.serverHandlingError(context, error, txtError);
                } else {
                    Utils.serverHandlingError(context, error);
                }

                int total = getTotalNotification(context);
                if (total > 0) {
                    ShortcutBadger.applyCount(context, 1);
                } else {
                    ShortcutBadger.removeCount(context);
                }
            }
        };
    }

    public static int getTotalNotification(Context context) {
        int total = 0;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(DashboardProvider.CONTENT_URI, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        if (!cursor.isNull(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO))) {
                            total += cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO));
                        }
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return total;
    }
}
