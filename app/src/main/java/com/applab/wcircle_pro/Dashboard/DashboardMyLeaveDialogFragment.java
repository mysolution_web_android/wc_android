package com.applab.wcircle_pro.Dashboard;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Leave.*;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by user on 27/10/2015.
 */
public class DashboardMyLeaveDialogFragment extends DialogFragment {
    private LinearLayout mBtnApprove, mBtnReject, mBtnLater, mAcknowledge;
    private ImageView mBtnCancel;
    private String TAG = "REJECT_DELETE";
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private int mPosition;
    private Leave mLeave;
    private TextView mTxtName, mTxtType, mTxtStartDate, mTxtEndDate, mTxtRemarks, mTxtDays, mTxtStatus, mTxtReason;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DashboardMyLeaveDialogFragment newInstance(Context context, Leave leave, int position) {
        DashboardMyLeaveDialogFragment frag = new DashboardMyLeaveDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("leave", leave);
        args.putInt("position", position);
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mLeave = getArguments().getParcelable("leave");
        mPosition = getArguments().getInt("position");

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_my_leave, null);

        mTxtName = (TextView) v.findViewById(R.id.txtName);
        mTxtStartDate = (TextView) v.findViewById(R.id.txtStartDate);
        mTxtEndDate = (TextView) v.findViewById(R.id.txtEndDate);
        mTxtType = (TextView) v.findViewById(R.id.txtType);
        mTxtRemarks = (TextView) v.findViewById(R.id.txtRemarks);
        mTxtDays = (TextView) v.findViewById(R.id.txtDays);
        mTxtStatus = (TextView) v.findViewById(R.id.txtStatus);
        mTxtReason = (TextView) v.findViewById(R.id.txtReason);

        mTxtName.setText(mLeave.getName());
        mTxtStartDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", mLeave.getLeaveFrom()));
        mTxtEndDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", mLeave.getLeaveTo()));
        mTxtType.setText(mLeave.getType());
        mTxtRemarks.setText(mLeave.getRemarks());
        mTxtDays.setText(String.valueOf(mLeave.getLeaveDays()));
        mTxtStatus.setText(String.valueOf(mLeave.getStatus()));
        mTxtReason.setText(String.valueOf(mLeave.getStatusRemark()));

        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        mAcknowledge = (LinearLayout) v.findViewById(R.id.acknowledge);
        mAcknowledge.setOnClickListener(btnAcknowledgeOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnAcknowledgeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(DashboardActivity.ACTION);
            intent.putExtra("isCalendar", false);
            intent.putExtra("isEmployeeLeave", false);
            intent.putExtra("isMyLeave", true);
            intent.putExtra("position", mPosition);
            com.applab.wcircle_pro.Leave.HttpHelper.getGsonReading(mContext, mLeave.getId(), TAG);
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
            mgr.sendBroadcast(intent);
            DashboardMyLeaveDialogFragment.this.getDialog().cancel();
        }
    };


    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(DashboardActivity.ACTION);
            intent.putExtra("isCalendar", false);
            intent.putExtra("isEmployeeLeave", false);
            intent.putExtra("isMyLeave", true);
            intent.putExtra("position", mPosition);
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
            mgr.sendBroadcast(intent);
            DashboardMyLeaveDialogFragment.this.getDialog().cancel();
        }
    };
}

