package com.applab.wcircle_pro.Dashboard;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.applab.wcircle_pro.Calendar.Attendee;
import com.applab.wcircle_pro.Calendar.AttendeesProvider;
import com.applab.wcircle_pro.Calendar.Calendar;
import com.applab.wcircle_pro.Calendar.CalendarProvider;
import com.applab.wcircle_pro.Calendar.MonthFragment;
import com.applab.wcircle_pro.Calendar.ScheduleFragment;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;

/**
 * Created by admin on 24/8/2015.
 */

public class DashboardCalendarAlarmIntentService extends IntentService {
    private static final int UNIQUEID = 45615;
    NotificationCompat.Builder notification;
    private String TAG = "DASHBOARD CALENDAR ALARM";

    public DashboardCalendarAlarmIntentService() {
        super("DashboardCalendarAlarmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Calendar calendar = intent.getParcelableExtra("calendar");
        ArrayList<Attendee> arrAttendee = intent.getParcelableArrayListExtra("attendees");
        String msg = intent.getStringExtra("msg");
        sendNotification(msg, calendar, arrAttendee);
    }

    private void sendNotification(final String msg, Calendar calendar, ArrayList<Attendee> arrAttendee) {
        calendar.setAttendees(arrAttendee);
        if (!MonthFragment.isCalendarExists(calendar.getId(), getBaseContext())) {
            insertCalendar(calendar);
        }
        if (!Utils.getIsActivityOpen(getBaseContext())) {
            Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
            notification = new NotificationCompat.Builder(this);
            notification.setAutoCancel(true);
            notification.setSmallIcon(R.mipmap.icon);
            notification.setWhen(System.currentTimeMillis());
            notification.setContentTitle("Calendar Event");
            notification.setSound(alarmSound);
            notification.setContentText(msg);
            //notification.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
            notification.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
            String name = this.getBaseContext().getClass().getSimpleName();
            Intent intentNotification = new Intent(this, DashboardActivity.class);
            intentNotification.putExtra("calendar", calendar);
            intentNotification.putExtra("isCalendar", true);
            intentNotification.putExtra("position", 0);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, calendar.getId(), intentNotification, PendingIntent.FLAG_UPDATE_CURRENT);
            notification.setContentIntent(pendingIntent);
            NotificationManager me = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            me.notify(UNIQUEID, notification.build());
        } else {
            if (Utils.getIsDashboad()) {
                Handler mHandler = new Handler(getMainLooper());
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        final MediaPlayer mp = MediaPlayer.create(getApplication(), R.raw.notification);
                        mp.start();
                    }
                });
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(this);
                Intent intent = new Intent(DashboardActivity.ACTION);
                intent.putExtra("calendar", calendar);
                intent.putExtra("isCalendar", true);
                intent.putExtra("position", 0);
                mgr.sendBroadcast(intent);
                Utils.vibrate(1000, getBaseContext());
            } else {
                Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
                notification = new NotificationCompat.Builder(this);
                notification.setAutoCancel(true);
                notification.setSmallIcon(R.mipmap.icon);
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("Calendar event: " + calendar.getTitle());
                notification.setSound(alarmSound);
                //notification.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
                notification.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
                String name = this.getBaseContext().getClass().getSimpleName();
                Intent intentNotification = new Intent(this, DashboardActivity.class);
                intentNotification.putExtra("calendar", calendar);
                intentNotification.putExtra("isCalendar", true);
                intentNotification.putExtra("position", 0);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, calendar.getId(), intentNotification, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);
                NotificationManager me = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                me.notify(UNIQUEID, notification.build());
            }
        }
    }

    public void insertCalendar(Calendar calendar) {
        DBHelper helper = new DBHelper(getBaseContext());
        try {
            ContentValues contentValues = new ContentValues();
            String calendarTo = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, calendar.getCalendarTo());
            String calendarFrom = Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, calendar.getCalendarFrom());
            String reminderDate = calendar.getReminder() != null ? Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, calendar.getReminder()) : null;
            contentValues.put(DBHelper.CALENDAR_COLUMN_CALENDAR_ID, calendar.getId());
            contentValues.put(DBHelper.CALENDAR_COLUMN_TYPE, calendar.getType());
            contentValues.put(DBHelper.CALENDAR_COLUMN_TITLE, calendar.getTitle());
            contentValues.put(DBHelper.CALENDAR_COLUMN_ALL_DAY, String.valueOf(calendar.getAllDay()));
            contentValues.put(DBHelper.CALENDAR_COLUMN_START_DATE, calendarFrom);
            contentValues.put(DBHelper.CALENDAR_COLUMN_END_DATE, calendarTo);
            contentValues.put(DBHelper.CALENDAR_COLUMN_LOCATION, calendar.getLocation());
            contentValues.put(DBHelper.CALENDAR_COLUMN_DESCRIPTION, calendar.getDescription());
            contentValues.put(DBHelper.CALENDAR_COLUMN_REMINDER, reminderDate);
            contentValues.put(DBHelper.CALENDAR_COLUMN_STATUS, calendar.getStatus());
            contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_ID, calendar.getCreateById());
            contentValues.put(DBHelper.CALENDAR_COLUMN_CREATE_BY_NAME, calendar.getCreateByName());
            contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_ID, calendar.getUpdateById());
            contentValues.put(DBHelper.CALENDAR_COLUMN_UPDATE_BY_NAME, calendar.getUpdateByName());
            contentValues.put(DBHelper.CALENDAR_COLUMN_NEW_CREATE, String.valueOf(calendar.getNewCreate()));
            if (calendar.getAttendees() != null) {
                for (int j = 0; j < calendar.getAttendees().size(); j++) {
                    String reminder = calendar.getAttendees().get(j).getReminder() != null ?
                            Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT, calendar.getAttendees().get(j).getReminder()) : null;
                    ContentValues contentValuesAttendees = new ContentValues();
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SUBMIT, 1);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_IS_SELECT, 1);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_NAME, calendar.getAttendees().get(j).getEmployeyName());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID, calendar.getAttendees().get(j).getEmployeeId());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_CALENDAR_ID, calendar.getId());
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_REMINDER, reminder);
                    contentValuesAttendees.put(DBHelper.ATTENDEES_COLUMN_STATUS, calendar.getAttendees().get(j).getStatus());
                    if (!ScheduleFragment.isAttendeesExists(calendar.getId(), calendar.getAttendees().get(j).getEmployeeId(), getBaseContext())) {
                        getContentResolver().insert(AttendeesProvider.CONTENT_URI, contentValuesAttendees);
                    }
                }
            }
            if (!MonthFragment.isCalendarExists(calendar.getId(), getBaseContext())) {
                getContentResolver().insert(CalendarProvider.CONTENT_URI, contentValues);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Error :" + ex.toString());
        }
    }
}
