package com.applab.wcircle_pro.Dashboard;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Leave.Leave;
import com.applab.wcircle_pro.Leave.LeaveProvider;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.HashMap;

/**
 * Created by admin on 24/8/2015.
 */
public class DashboardLeaveAlarmIntentService extends IntentService {
    private static final int UNIQUEID = 45617;
    NotificationCompat.Builder notification;
    private String TAG = "DASHBOARD LEAVE ALARM";

    public DashboardLeaveAlarmIntentService() {
        super("DashboardLeaveAlarmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Leave leave = intent.getParcelableExtra("leave");
        String msg = intent.getStringExtra("msg");
        getGson(msg, leave);
    }

    private void sendNotification(final String msg, Leave leave) {
        if (!Utils.getIsActivityOpen(getBaseContext())) {
            Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
            notification = new NotificationCompat.Builder(this);
            notification.setAutoCancel(true);
            notification.setSmallIcon(R.mipmap.icon);
            notification.setWhen(System.currentTimeMillis());
            notification.setContentTitle("Applied leave by: " + leave.getName());
            notification.setSound(alarmSound);
            notification.setContentText(msg);
            notification.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
            //notification.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
            Intent intentNotification = new Intent(this, DashboardActivity.class);
            intentNotification.putExtra("leave", leave);
            intentNotification.putExtra("isEmployeeLeave", true);
            intentNotification.putExtra("position", 0);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, leave.getId(), intentNotification, PendingIntent.FLAG_UPDATE_CURRENT);
            notification.setContentIntent(pendingIntent);
            NotificationManager me = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            me.notify(UNIQUEID, notification.build());
        } else {
            if (Utils.getIsDashboad()) {
                Handler mHandler = new Handler(getMainLooper());
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        final MediaPlayer mp = MediaPlayer.create(getApplication(), R.raw.notification);
                        mp.start();
                    }
                });
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(this);
                Intent intent = new Intent(DashboardActivity.ACTION);
                intent.putExtra("leave", leave);
                intent.putExtra("isEmployeeLeave", true);
                intent.putExtra("position", 0);
                mgr.sendBroadcast(intent);
                Utils.vibrate(1000, getBaseContext());
            } else {
                Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
                notification = new NotificationCompat.Builder(this);
                notification.setAutoCancel(true);
                notification.setSmallIcon(R.mipmap.icon);
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("Applied leave by: " + leave.getName());
                notification.setSound(alarmSound);
                notification.setContentText(msg);
                notification.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
                Intent intentNotification = new Intent(this, DashboardActivity.class);
                intentNotification.putExtra("leave", leave);
                intentNotification.putExtra("isEmployeeLeave", true);
                intentNotification.putExtra("position", 0);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, leave.getId(), intentNotification, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);
                NotificationManager me = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                me.notify(UNIQUEID, notification.build());
            }
        }
    }

    public static boolean isLeaveExists(Object leaveId, Context context) {
        Cursor cursor = null;
        boolean isExists = false;
        try {
            cursor = context.getContentResolver().query(LeaveProvider.CONTENT_URI,
                    null, DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?", new String[]{String.valueOf(leaveId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public void insertEmployeeLeave(String msg, Leave leave) {
        DBHelper helper = new DBHelper(getBaseContext());
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_ID, leave.getId());
            contentValues.put(DBHelper.LEAVE_COLUMN_NAME, leave.getName());
            contentValues.put(DBHelper.LEAVE_COLUMN_EMPLOYEE_ID, leave.getEmployeeId());
            contentValues.put(DBHelper.LEAVE_COLUMN_DEPARTMENT, leave.getDepartment());
            contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR_ID, leave.getSuperiorId());
            contentValues.put(DBHelper.LEAVE_COLUMN_POSITION, leave.getPosition());
            contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR, leave.getSuperior());
            contentValues.put(DBHelper.LEAVE_COLUMN_TYPE_CODE, leave.getTypeCode());
            contentValues.put(DBHelper.LEAVE_COLUMN_TYPE, leave.getType());
            contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, leave.getRemarks());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_DAYS, leave.getLeaveDays());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_FROM, leave.getLeaveFrom());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_TO, leave.getLeaveTo());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, leave.getStatus());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_REMARKS, leave.getStatusRemark());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_BY, leave.getStatusUpdateBy());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_ON, leave.getStatusUpdateOn());
            contentValues.put(DBHelper.LEAVE_COLUMN_LAST_UPDATE, leave.getLastUpdate());
            contentValues.put(DBHelper.LEAVE_COLUMN_NEW_CREATE, String.valueOf(leave.getNewCreate()));
            if (leave.getSuperiorId().equals(Utils.getProfile(getBaseContext()).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
            } else if (!leave.getSuperiorId().equals(Utils.getProfile(getBaseContext()).getId()) && !leave.getEmployeeId().equals(Utils.getProfile(getBaseContext()).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
            } else if (!leave.getSuperiorId().equals(Utils.getProfile(getBaseContext()).getId()) && leave.getEmployeeId().equals(Utils.getProfile(getBaseContext()).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
            }
            contentValues.put(DBHelper.LEAVE_COLUMN_CREATE_DATE, leave.getCreateDate());
            if (!isLeaveExists(leave.getId(), getBaseContext())) {
                getContentResolver().insert(LeaveProvider.CONTENT_URI, contentValues);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider My Leave Error :" + ex.toString());
        } finally {
            sendNotification(msg, leave);
        }
    }

    //region get single leave region
    public void getGson(String msg, Leave leave) {
        final String token = Utils.getToken(getBaseContext());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        GsonRequest<Leave> mGsonRequest = new GsonRequest<Leave>(
                Request.Method.GET,
                Utils.API_URL + "api/Leave/" + Utils.encode(leave.getId()),
                Leave.class,
                headers,
                responseListener(msg, leave),
                errorListener(msg, leave)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private void insertLeave(String msg, Leave response) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_ID, response.getId());
            contentValues.put(DBHelper.LEAVE_COLUMN_NAME, response.getName());
            contentValues.put(DBHelper.LEAVE_COLUMN_EMPLOYEE_ID, response.getEmployeeId());
            contentValues.put(DBHelper.LEAVE_COLUMN_DEPARTMENT, response.getDepartment());
            contentValues.put(DBHelper.LEAVE_COLUMN_POSITION, response.getPosition());
            contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR, response.getSuperior());
            contentValues.put(DBHelper.LEAVE_COLUMN_SUPERIOR_ID, response.getSuperiorId());
            contentValues.put(DBHelper.LEAVE_COLUMN_TYPE_CODE, response.getTypeCode());
            contentValues.put(DBHelper.LEAVE_COLUMN_TYPE, response.getType());
            contentValues.put(DBHelper.LEAVE_COLUMN_REMARKS, response.getRemarks());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_DAYS, response.getLeaveDays());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_FROM, response.getLeaveFrom());
            contentValues.put(DBHelper.LEAVE_COLUMN_LEAVE_TO, response.getLeaveTo());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS, response.getStatus());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_REMARKS, response.getStatusRemark());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_BY, response.getStatusUpdateBy());
            contentValues.put(DBHelper.LEAVE_COLUMN_STATUS_UPDATE_ON, response.getStatusUpdateOn());
            contentValues.put(DBHelper.LEAVE_COLUMN_LAST_UPDATE, response.getLastUpdate());
            contentValues.put(DBHelper.LEAVE_COLUMN_NEW_CREATE, String.valueOf(response.getNewCreate()));
            if (response.getSuperiorId().equals(Utils.getProfile(getBaseContext()).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
            } else if (!response.getSuperiorId().equals(Utils.getProfile(getBaseContext()).getId()) && !response.getEmployeeId().equals(Utils.getProfile(getBaseContext()).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(true));
            } else if (!response.getSuperiorId().equals(Utils.getProfile(getBaseContext()).getId()) && response.getEmployeeId().equals(Utils.getProfile(getBaseContext()).getId())) {
                contentValues.put(DBHelper.LEAVE_COLUMN_IS_EMPLOYEE_LEAVE, String.valueOf(false));
            }
            contentValues.put(DBHelper.LEAVE_COLUMN_CREATE_DATE, response.getCreateDate());

            if (isLeaveExists(response.getId(), getBaseContext())) {
                getContentResolver().update(LeaveProvider.CONTENT_URI, contentValues, DBHelper.LEAVE_COLUMN_LEAVE_ID + "=?",
                        new String[]{String.valueOf(response.getId())});
            } else {
                getContentResolver().insert(LeaveProvider.CONTENT_URI, contentValues);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Edit Application Error :" + ex.toString());
        } finally {
            sendNotification(msg, response);
        }
    }

    private Response.Listener<Leave> responseListener(final String msg, final Leave leave) {
        return new Response.Listener<Leave>() {
            @Override
            public void onResponse(Leave response) {
                if (!leave.getNewCreate()) {
                    insertLeave(msg, response);
                }
            }
        };
    }

    private Response.ErrorListener errorListener(final String msg, final Leave leave) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                insertEmployeeLeave(msg, leave);
            }
        };
    }
    //endregion
}
