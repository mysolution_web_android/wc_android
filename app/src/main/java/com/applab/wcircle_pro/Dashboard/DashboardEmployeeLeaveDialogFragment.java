package com.applab.wcircle_pro.Dashboard;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.Leave.HttpHelper;
import com.applab.wcircle_pro.Leave.Leave;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by user on 27/10/2015.
 */

public class DashboardEmployeeLeaveDialogFragment extends DialogFragment {
    private LinearLayout mBtnApprove, mBtnReject, mBtnLater;
    private ImageView mBtnCancel;
    private String TAG = "REJECT_DELETE";
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private int mPosition;
    private Leave mLeave;
    private TextView mTxtName, mTxtType, mTxtStartDate, mTxtEndDate, mTxtRemarks, mTxtDays;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DashboardEmployeeLeaveDialogFragment newInstance(Context context, Leave leave, int position) {
        DashboardEmployeeLeaveDialogFragment frag = new DashboardEmployeeLeaveDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("leave", leave);
        args.putInt("position", position);
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mLeave = getArguments().getParcelable("leave");
        mPosition = getArguments().getInt("position");

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_accept_reject_employee_leave, null);

        mTxtName = (TextView) v.findViewById(R.id.txtName);
        mTxtStartDate = (TextView) v.findViewById(R.id.txtStartDate);
        mTxtEndDate = (TextView) v.findViewById(R.id.txtEndDate);
        mTxtType = (TextView) v.findViewById(R.id.txtType);
        mTxtRemarks = (TextView) v.findViewById(R.id.txtRemarks);
        mTxtDays = (TextView) v.findViewById(R.id.txtDays);

        mTxtName.setText(mLeave.getName());
        mTxtStartDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", mLeave.getLeaveFrom()));
        mTxtEndDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", mLeave.getLeaveTo()));
        mTxtType.setText(mLeave.getType());
        mTxtRemarks.setText(mLeave.getRemarks());
        mTxtDays.setText(String.valueOf(mLeave.getLeaveDays()));

        mBtnApprove = (LinearLayout) v.findViewById(R.id.btnApprove);
        mBtnReject = (LinearLayout) v.findViewById(R.id.btnReject);
        mBtnLater = (LinearLayout) v.findViewById(R.id.btnLater);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);

        mBtnCancel.setOnClickListener(btnCancelOnClickListener);
        mBtnApprove.setOnClickListener(btnApproveOnClickListener);
        mBtnReject.setOnClickListener(btnRejectOnClickListener);
        mBtnLater.setOnClickListener(btnLaterOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnLaterOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String reminder = DashboardCalendarDialogFragment.getReminder();
            if (!reminder.equals("")) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.LEAVE_REMINDER_LEAVE_COLUMN_ID, mLeave.getId());
                contentValues.put(DBHelper.LEAVE_REMINDER_COLUMN_DATE, reminder);
                contentValues.put(DBHelper.LEAVE_REMINDER_COLUMN_IS_SEEN, false);

                if (DashboardLeaveReminderDialogFragment.isLeaveReminderExists(mLeave.getId(), mContext)) {
                    mContext.getContentResolver().update(LeaveReminderProvider.CONTENT_URI, contentValues,
                            DBHelper.LEAVE_REMINDER_LEAVE_COLUMN_ID + "=?", new String[]{String.valueOf(mLeave.getId())});
                } else {
                    mContext.getContentResolver().insert(LeaveReminderProvider.CONTENT_URI, contentValues);
                }
            }
            Intent intent = new Intent(DashboardActivity.ACTION);
            intent.putExtra("isCalendar", false);
            intent.putExtra("isEmployeeLeave", true);
            intent.putExtra("position", mPosition);
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
            mgr.sendBroadcast(intent);
            DashboardEmployeeLeaveDialogFragment.this.getDialog().cancel();
        }
    };

    public static void setRead(Context context, Leave leave, String TAG) {
        if (Utils.getStatus(Utils.LEAVE_READ, context)) {
            HttpHelper.getGsonReading(context, leave.getId(), TAG);
        }
    }

    private View.OnClickListener btnRejectOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DashboardEmployeeLeaveApproveRejectDialogFragment reject = DashboardEmployeeLeaveApproveRejectDialogFragment.newInstance(3, mLeave, mContext, mPosition);
            reject.show(getActivity().getSupportFragmentManager(), "");
        }
    };

    private View.OnClickListener btnApproveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DashboardEmployeeLeaveApproveRejectDialogFragment approve = DashboardEmployeeLeaveApproveRejectDialogFragment.newInstance(2, mLeave, mContext, mPosition);
            approve.show(getActivity().getSupportFragmentManager(), "");
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(DashboardActivity.ACTION);
            intent.putExtra("isCalendar", false);
            intent.putExtra("isEmployeeLeave", true);
            intent.putExtra("isMyLeave", false);
            intent.putExtra("position", mPosition);
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
            mgr.sendBroadcast(intent);
            DashboardEmployeeLeaveDialogFragment.this.getDialog().cancel();
        }
    };
}

