package com.applab.wcircle_pro.Dashboard;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.applab.wcircle_pro.Calendar.Attendee;
import com.applab.wcircle_pro.Calendar.Calendar;
import com.applab.wcircle_pro.Calendar.CalendarActivity;
import com.applab.wcircle_pro.Calendar.CalendarSpinnerAdapter;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by user on 27/10/2015.
 */

public class DashboardCalendarReminderDialogFragment extends DialogFragment {
    private Calendar mCalendar;
    private LinearLayout mBtnSubmit, mBtnExit;
    private ImageView mBtnCancel;
    private String TAG = "CALENDAR ATTEND";
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private CalendarSpinnerAdapter calendarSpinnerAdapter;
    private Spinner mSpReminder;
    private String mStartDate;
    private int mPosition;
    private ArrayList<Attendee> mArrAttendee = new ArrayList<>();

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DashboardCalendarReminderDialogFragment newInstance(Calendar calendar, Context context, String startDate, int position, ArrayList<Attendee> arrAttendee) {
        DashboardCalendarReminderDialogFragment frag = new DashboardCalendarReminderDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("Calendar", calendar);
        args.putParcelableArrayList("arrAttendee", arrAttendee);
        args.putString("startDate", startDate);
        args.putInt("position", position);
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mCalendar = getArguments().getParcelable("Calendar");
        mStartDate = getArguments().getString("startDate");
        mPosition = getArguments().getInt("position");
        mArrAttendee = getArguments().getParcelableArrayList("arrAttendee");

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_reminder, null);

        mSpReminder = (Spinner) v.findViewById(R.id.spReminder);
        calendarSpinnerAdapter = new CalendarSpinnerAdapter(DashboardActivity.getListReminder(),
                getActivity());
        mSpReminder.setAdapter(calendarSpinnerAdapter);

        mBtnSubmit = (LinearLayout) v.findViewById(R.id.btnSubmit);
        mBtnExit = (LinearLayout) v.findViewById(R.id.btnExit);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);

        mBtnSubmit.setOnClickListener(btnSubmitOnClickListener);
        mBtnExit.setOnClickListener(btnExitOnClickListener);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    public static boolean isCalendarIsSeen(Object calendarId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CalendarReminderProvider.CONTENT_URI,
                    null, DBHelper.CALENDAR_REMINDER_CALENDAR_COLUMN_ID + "=?", new String[]{String.valueOf(calendarId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                String isSeen = cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_REMINDER_COLUMN_IS_SEEN));
                if (isSeen != null) {
                    if (!isSeen.equals("")) {
                        isExists = cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_REMINDER_COLUMN_IS_SEEN)).equals("true");
                    }
                }
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static String getReminder(Object leaveId, Context context) {
        String reminder = null;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CalendarReminderProvider.CONTENT_URI,
                    null, DBHelper.CALENDAR_REMINDER_CALENDAR_COLUMN_ID + "=?", new String[]{String.valueOf(leaveId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                reminder = cursor.getString(cursor.getColumnIndex(DBHelper.CALENDAR_REMINDER_COLUMN_DATE));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return reminder;
    }

    public static boolean isCalendarReminderExists(Object calendarId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(CalendarReminderProvider.CONTENT_URI,
                    null, DBHelper.CALENDAR_REMINDER_CALENDAR_COLUMN_ID + "=?", new String[]{String.valueOf(calendarId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    private View.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String reminder = DashboardActivity.getReminder(mStartDate, mSpReminder.getSelectedItemPosition());
            if (!reminder.equals("")) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.CALENDAR_REMINDER_CALENDAR_COLUMN_ID, mCalendar.getId());
                contentValues.put(DBHelper.CALENDAR_REMINDER_COLUMN_DATE, reminder);
                contentValues.put(DBHelper.CALENDAR_REMINDER_COLUMN_IS_SEEN, false);
                if (isCalendarReminderExists(mCalendar.getId(), mContext)) {
                    mContext.getContentResolver().update(CalendarReminderProvider.CONTENT_URI, contentValues,
                            DBHelper.CALENDAR_REMINDER_COLUMN_ID + "=?", new String[]{String.valueOf(mCalendar.getId())});
                } else {
                    mContext.getContentResolver().insert(CalendarReminderProvider.CONTENT_URI, contentValues);
                }
                if (Utils.setCalendarDate(Utils.DATE_FORMAT, reminder).after(new Date())) {
                    int year = CalendarActivity.getYear(reminder);
                    int month = CalendarActivity.getMonth(reminder);
                    int day = CalendarActivity.getDay(reminder);
                    int hour = CalendarActivity.getHour(reminder, 0);
                    int minutes = CalendarActivity.getMinutes(reminder, 0);
                    String msg = "";
                    if (mCalendar.getAllDay()) {
                        msg += "On " + Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy 'at' h:mm a", mCalendar.getCalendarFrom()) + "\n";
                    } else {
                        msg += "On " + Utils.setCalendarDate(Utils.DATE_FORMAT, "dd MMM yyyy 'at' h:mm a", mCalendar.getCalendarFrom()) + "\n";
                    }
                    DashboardActivity.setCalendarAlarm(year, month, day, hour, minutes,
                            msg, mCalendar, mArrAttendee, getActivity());
                }
            }
            Intent intent = new Intent(DashboardActivity.ACTION);
            intent.putExtra("isCalendar", true);
            intent.putExtra("isEmployeeLeave", false);
            intent.putExtra("position", mPosition);
            DashboardActivity.mgr.sendBroadcast(intent);
            DashboardCalendarReminderDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnExitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DashboardCalendarReminderDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DashboardCalendarReminderDialogFragment.this.getDialog().cancel();
        }
    };
}

