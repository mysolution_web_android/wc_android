package com.applab.wcircle_pro.Dashboard;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.wcircle_pro.Calendar.AttendeesProvider;
import com.applab.wcircle_pro.Calendar.Calendar;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.AppController;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.GsonRequest;
import com.applab.wcircle_pro.Utils.Utils;
import com.google.gson.JsonObject;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

/**
 * Created by user on 27/10/2015.
 */
public class DashboardCalendarDialogFragment extends DialogFragment {
    private LinearLayout mBtnAccept, mBtnReject, mBtnLater, mBtnAcknowledge;
    private LinearLayout mLvAcceptReject, mLvAcknowdge;
    private ImageView mBtnCancel;
    private String TAG = "REJECT_DELETE";
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private int mPosition;
    private Calendar mCalendar;
    private TextView mTxtTitle, mTxtStartDate, mTxtEndDate,
            mTxtLocation, mTxtDesc, mTxtCreator, mTxtAllDay;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */

    public static DashboardCalendarDialogFragment newInstance(Context context, Calendar calendar, int position) {
        DashboardCalendarDialogFragment frag = new DashboardCalendarDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("calendar", calendar);
        args.putInt("position", position);
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mCalendar = getArguments().getParcelable("calendar");
        mPosition = getArguments().getInt("position");

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_accept_reject_calendar, null);

        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtStartDate = (TextView) v.findViewById(R.id.txtStartDate);
        mTxtEndDate = (TextView) v.findViewById(R.id.txtEndDate);
        mTxtLocation = (TextView) v.findViewById(R.id.txtLocation);
        mTxtDesc = (TextView) v.findViewById(R.id.txtDesc);
        mTxtCreator = (TextView) v.findViewById(R.id.txtCreator);

        mTxtTitle.setText(mCalendar.getTitle());
        if (mCalendar.getAllDay()) {
            mTxtStartDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy h:mm a", mCalendar.getCalendarFrom()));
            mTxtEndDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", mCalendar.getCalendarTo()));
        } else {
            mTxtStartDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy h:mm a", mCalendar.getCalendarFrom()));
            mTxtEndDate.setText(Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy h:mm a", mCalendar.getCalendarTo()));
        }

        mTxtLocation.setText(mCalendar.getLocation());
        mTxtDesc.setText(mCalendar.getDescription());
        mTxtCreator.setText(mCalendar.getCreateByName());

        mLvAcceptReject = (LinearLayout) v.findViewById(R.id.lvAcceptReject);
        mLvAcknowdge = (LinearLayout) v.findViewById(R.id.lvAcknowledge);

        if (mCalendar.getType().equals("C") || mCalendar.getType().equals("B")) {
            mLvAcceptReject.setVisibility(View.GONE);
            mLvAcknowdge.setVisibility(View.VISIBLE);
        } else {
            mLvAcceptReject.setVisibility(View.VISIBLE);
            mLvAcknowdge.setVisibility(View.GONE);
        }

        mBtnAcknowledge = (LinearLayout) v.findViewById(R.id.btnAcknowledge);
        mBtnAccept = (LinearLayout) v.findViewById(R.id.btnAccept);
        mBtnReject = (LinearLayout) v.findViewById(R.id.btnReject);
        mBtnLater = (LinearLayout) v.findViewById(R.id.btnLater);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);

        mBtnCancel.setOnClickListener(btnCancelOnClickListener);
        mBtnAccept.setOnClickListener(btnAcceptOnClickListener);
        mBtnReject.setOnClickListener(btnRejectOnClickListener);
        mBtnLater.setOnClickListener(btnLaterOnClickListener);
        mBtnAcknowledge.setOnClickListener(btnKnowledgeOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    public static String getReminder() {
        Date requestedDate = new Date();
        java.util.Calendar gc = new GregorianCalendar();
        gc.setTime(requestedDate);
        gc.add(java.util.Calendar.MINUTE, 30);
        requestedDate = gc.getTime();
        return Utils.setCalendarDate(Utils.DATE_FORMAT, requestedDate);
    }

    private View.OnClickListener btnLaterOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String reminder = getReminder();
            if (!reminder.equals("")) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.CALENDAR_REMINDER_CALENDAR_COLUMN_ID, mCalendar.getId());
                contentValues.put(DBHelper.CALENDAR_REMINDER_COLUMN_DATE, reminder);
                contentValues.put(DBHelper.CALENDAR_REMINDER_COLUMN_IS_SEEN, false);

                if (DashboardCalendarReminderDialogFragment.isCalendarReminderExists(mCalendar.getId(), mContext)) {
                    mContext.getContentResolver().update(CalendarReminderProvider.CONTENT_URI, contentValues,
                            DBHelper.CALENDAR_REMINDER_CALENDAR_COLUMN_ID + "=?", new String[]{String.valueOf(mCalendar.getId())});
                } else {
                    mContext.getContentResolver().insert(CalendarReminderProvider.CONTENT_URI, contentValues);
                }
            }
            Intent intent = new Intent(DashboardActivity.ACTION);
            intent.putExtra("isCalendar", true);
            intent.putExtra("isEmployeeLeave", false);
            intent.putExtra("position", mPosition);
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
            mgr.sendBroadcast(intent);
            DashboardCalendarDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnKnowledgeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(DashboardActivity.ACTION);
            intent.putExtra("isCalendar", true);
            intent.putExtra("isEmployeeLeave", false);
            intent.putExtra("isMyLeave", false);
            intent.putExtra("position", mPosition);
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.CALENDAR_REMINDER_CALENDAR_COLUMN_ID, mCalendar.getId());
            contentValues.put(DBHelper.CALENDAR_REMINDER_COLUMN_IS_SEEN, true);
            if (DashboardCalendarReminderDialogFragment.isCalendarReminderExists(mCalendar.getId(), mContext)) {
                mContext.getContentResolver().update(CalendarReminderProvider.CONTENT_URI, contentValues,
                        DBHelper.CALENDAR_REMINDER_COLUMN_ID + "=?", new String[]{String.valueOf(mCalendar.getId())});
            } else {
                mContext.getContentResolver().insert(CalendarReminderProvider.CONTENT_URI, contentValues);
            }
            setRead(mContext, mCalendar, TAG);
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
            mgr.sendBroadcast(intent);
            DashboardCalendarDialogFragment.this.getDialog().cancel();
        }
    };

    public static void setRead(Context context, Calendar calendar, String TAG) {
        if (Utils.getStatus(Utils.CALENDAR_READ, context)) {
            com.applab.wcircle_pro.Calendar.db.HttpHelper.getCalendarGsonReading(context, calendar.getId(), TAG);
        }
    }

    private View.OnClickListener btnRejectOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            rejectEvent(true);
            DashboardCalendarDialogFragment.this.getDialog().cancel();
            /*DashboardCalendarRejectDialogFragment reject = DashboardCalendarRejectDialogFragment.newInstance(mCalendar, mContext, mPosition);
            reject.show(getActivity().getSupportFragmentManager(), "");*/
        }
    };

    private View.OnClickListener btnAcceptOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DashboardCalendarAcceptDialogFragment accept = DashboardCalendarAcceptDialogFragment.newInstance(mCalendar, mContext,
                    Utils.setDate(Utils.DATE_FORMAT, Utils.DATE_FORMAT_PRINT, mCalendar.getCalendarFrom()), mPosition, DashboardActivity.getAttendee(mCalendar.getId(), mContext));
            accept.show(getActivity().getSupportFragmentManager(), "");
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(DashboardActivity.ACTION);
            intent.putExtra("isCalendar", true);
            intent.putExtra("isEmployeeLeave", false);
            intent.putExtra("isMyLeave", false);
            intent.putExtra("position", mPosition);
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
            mgr.sendBroadcast(intent);
            DashboardCalendarDialogFragment.this.getDialog().cancel();
        }
    };

    //region reject region
    public void rejectEvent(boolean removeFromList) {
        final String token = Utils.getToken(mContext);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        final String finalUrl = Utils.API_URL + "api/Calendar/Attendees/Rejected?id="
                + mCalendar.getId() + "&RemoveFromList=" + removeFromList;
        Log.i(TAG, finalUrl);
        GsonRequest<JsonObject> mGsonRequest = new GsonRequest<JsonObject>(
                Request.Method.PUT,
                finalUrl,
                JsonObject.class,
                headers,
                responseRejectListener(),
                errorRejectListener()) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utils.INTERNET_LOADING,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    private Response.Listener<JsonObject> responseRejectListener() {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.ATTENDEES_COLUMN_STATUS, "reject");
                mContext.getContentResolver().update(AttendeesProvider.CONTENT_URI, contentValues,
                        DBHelper.ATTENDEES_COLUMN_CALENDAR_ID + "=? AND " +
                                DBHelper.ATTENDEES_COLUMN_EMPLOYEE_ID + "=?",
                        new String[]{String.valueOf(mCalendar.getId()),
                                String.valueOf(Utils.getProfile(mContext).getId())});

                Intent intent = new Intent(DashboardActivity.ACTION);
                intent.putExtra("isCalendar", true);
                intent.putExtra("isEmployeeLeave", false);
                intent.putExtra("isMyLeave", false);
                intent.putExtra("position", mPosition);
                DashboardCalendarDialogFragment.setRead(mContext, mCalendar, TAG);
                LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
                mgr.sendBroadcast(intent);
            }
        };
    }

    private Response.ErrorListener errorRejectListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utils.serverHandlingError(mContext, error);
            }
        };
    }
    //endregion
}

