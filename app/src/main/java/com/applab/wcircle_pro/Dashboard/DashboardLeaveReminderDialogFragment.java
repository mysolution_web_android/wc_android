package com.applab.wcircle_pro.Dashboard;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.applab.wcircle_pro.Calendar.CalendarActivity;
import com.applab.wcircle_pro.Calendar.CalendarSpinnerAdapter;
import com.applab.wcircle_pro.Leave.Leave;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Utils.DBHelper;
import com.applab.wcircle_pro.Utils.Utils;

import java.util.Date;

/**
 * Created by user on 27/10/2015.
 */
public class DashboardLeaveReminderDialogFragment extends DialogFragment {
    private Leave mLeave;
    private LinearLayout mBtnSubmit, mBtnExit;
    private ImageView mBtnCancel;
    private String TAG = "CALENDAR ATTEND";
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private CalendarSpinnerAdapter calendarSpinnerAdapter;
    private Spinner mSpReminder;
    private String mStartDate;
    private int mPosition;
    private boolean mIsMyLeave;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static DashboardLeaveReminderDialogFragment newInstance(Leave leave, Context context, String startDate, int position, boolean isMyLeave) {
        DashboardLeaveReminderDialogFragment frag = new DashboardLeaveReminderDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("leave", leave);
        args.putString("startDate", startDate);
        args.putInt("position", position);
        args.putBoolean("isMyLeave", isMyLeave);
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mLeave = getArguments().getParcelable("leave");
        mStartDate = getArguments().getString("startDate");
        mPosition = getArguments().getInt("position");
        mIsMyLeave = getArguments().getBoolean("isMyLeave");

        // Inflate the layout for the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_reminder, null);

        mSpReminder = (Spinner) v.findViewById(R.id.spReminder);
        calendarSpinnerAdapter = new CalendarSpinnerAdapter(DashboardActivity.getListReminder(),
                getActivity());
        mSpReminder.setAdapter(calendarSpinnerAdapter);

        mBtnSubmit = (LinearLayout) v.findViewById(R.id.btnSubmit);
        mBtnExit = (LinearLayout) v.findViewById(R.id.btnExit);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);

        mBtnSubmit.setOnClickListener(btnSubmitOnClickListener);
        mBtnExit.setOnClickListener(btnExitOnClickListener);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    public static boolean isLeaveReminderIsSeen(Object leaveId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(LeaveReminderProvider.CONTENT_URI,
                    null, DBHelper.LEAVE_REMINDER_LEAVE_COLUMN_ID + "=?", new String[]{String.valueOf(leaveId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                String isSeen = cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_REMINDER_COLUMN_IS_SEEN));
                if (isSeen != null) {
                    if (!isSeen.equals("")) {
                        isExists = isSeen.equals("true");
                    }
                }
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    public static String getReminder(Object leaveId, Context context) {
        String reminder = null;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(LeaveReminderProvider.CONTENT_URI,
                    null, DBHelper.LEAVE_REMINDER_LEAVE_COLUMN_ID + "=?", new String[]{String.valueOf(leaveId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                reminder = cursor.getString(cursor.getColumnIndex(DBHelper.LEAVE_REMINDER_COLUMN_DATE));
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return reminder;
    }

    public static boolean isLeaveReminderExists(Object leaveId, Context context) {
        boolean isExists = false;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(LeaveReminderProvider.CONTENT_URI,
                    null, DBHelper.LEAVE_REMINDER_LEAVE_COLUMN_ID + "=?", new String[]{String.valueOf(leaveId)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isExists = true;
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isExists;
    }

    private View.OnClickListener btnSubmitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String reminder = DashboardCalendarDialogFragment.getReminder();
            if (reminder.equals("")) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.LEAVE_REMINDER_LEAVE_COLUMN_ID, mLeave.getId());
                contentValues.put(DBHelper.LEAVE_REMINDER_COLUMN_DATE, reminder);
                contentValues.put(DBHelper.LEAVE_REMINDER_COLUMN_IS_SEEN, false);
                if (isLeaveReminderExists(mLeave.getId(), mContext)) {
                    mContext.getContentResolver().update(CalendarReminderProvider.CONTENT_URI, contentValues,
                            DBHelper.LEAVE_REMINDER_LEAVE_COLUMN_ID + "=?", new String[]{String.valueOf(mLeave.getId())});
                } else {
                    mContext.getContentResolver().insert(CalendarReminderProvider.CONTENT_URI, contentValues);
                }
                if (Utils.setCalendarDate(Utils.DATE_FORMAT, reminder).after(new Date())) {
                    int year = CalendarActivity.getYear(reminder);
                    int month = CalendarActivity.getMonth(reminder);
                    int day = CalendarActivity.getDay(reminder);
                    int hour = CalendarActivity.getHour(reminder, 0);
                    int minutes = CalendarActivity.getMinutes(reminder, 0);
                    String msg = mLeave.getType() + "\n";
                    msg += "Applied by: " + mLeave.getName() + "\n";
                    msg += "From: " + Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", mLeave.getLeaveFrom()) + "\n";
                    msg += "To: " + Utils.setDate(Utils.DATE_FORMAT, "dd MMM yyyy", mLeave.getLeaveTo()) + "\n";
                    DashboardActivity.setLeaveAlarm(year, month, day, hour, minutes,
                            msg, mLeave, getActivity());
                }
            }
            Intent intent = new Intent(DashboardActivity.ACTION);
            intent.putExtra("isCalendar", false);
            intent.putExtra("isEmployeeLeave", !mIsMyLeave);
            intent.putExtra("position", mPosition);
            DashboardActivity.mgr.sendBroadcast(intent);
            DashboardLeaveReminderDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnExitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DashboardLeaveReminderDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DashboardLeaveReminderDialogFragment.this.getDialog().cancel();
        }
    };
}

