package com.applab.wcircle_pro.Dashboard;

/**
 * Created by user on 6/12/2015.
 */

import android.database.Cursor;

import com.applab.wcircle_pro.Utils.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Dashboard {

    @SerializedName("calendarModule")
    @Expose
    private Integer calendarModule;
    @SerializedName("newsModule")
    @Expose
    private Integer newsModule;
    @SerializedName("eventModule")
    @Expose
    private Integer eventModule;
    @SerializedName("downloadModule")
    @Expose
    private Integer downloadModule;
    @SerializedName("catalogueModule")
    @Expose
    private Integer catalogueModule;
    @SerializedName("leaveModule")
    @Expose
    private Integer leaveModule = 0;
    @SerializedName("galleryModule")
    @Expose
    private Integer galleryModule = 0;

    public Integer getGalleryModule() {
        return galleryModule;
    }

    public void setGalleryModule(Integer galleryModule) {
        this.galleryModule = galleryModule;
    }

    private String title;
    private int id;
    private int no;
    private int imgDashboard;
    private int imgMenu;
    private boolean isSelected = false;

    public Integer getLeaveModule() {
        return leaveModule;
    }

    public void setLeaveModule(Integer leaveModule) {
        this.leaveModule = leaveModule;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getImgDashboard() {
        return imgDashboard;
    }

    public void setImgDashboard(int imgDashboard) {
        this.imgDashboard = imgDashboard;
    }

    public int getImgMenu() {
        return imgMenu;
    }

    public void setImgMenu(int imgMenu) {
        this.imgMenu = imgMenu;
    }

    /**
     * @return The calendarModule
     */
    public Integer getCalendarModule() {
        return calendarModule;
    }

    /**
     * @param calendarModule The calendarModule
     */
    public void setCalendarModule(Integer calendarModule) {
        this.calendarModule = calendarModule;
    }

    /**
     * @return The newsModule
     */
    public Integer getNewsModule() {
        return newsModule;
    }

    /**
     * @param newsModule The newsModule
     */
    public void setNewsModule(Integer newsModule) {
        this.newsModule = newsModule;
    }

    /**
     * @return The eventModule
     */
    public Integer getEventModule() {
        return eventModule;
    }

    /**
     * @param eventModule The eventModule
     */
    public void setEventModule(Integer eventModule) {
        this.eventModule = eventModule;
    }

    /**
     * @return The downloadModule
     */
    public Integer getDownloadModule() {
        return downloadModule;
    }

    /**
     * @param downloadModule The downloadModule
     */
    public void setDownloadModule(Integer downloadModule) {
        this.downloadModule = downloadModule;
    }

    /**
     * @return The catalogueModule
     */
    public Integer getCatalogueModule() {
        return catalogueModule;
    }

    /**
     * @param catalogueModule The catalogueModule
     */
    public void setCatalogueModule(Integer catalogueModule) {
        this.catalogueModule = catalogueModule;
    }

    public static Dashboard getDashboard(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Dashboard dashboard = new Dashboard();
        dashboard.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_ID)));
        dashboard.setImgMenu(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_IMG_MENU)));
        dashboard.setImgDashboard(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_IMG_DASHBOARD)));
        dashboard.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_TITLE)));
        if (!cursor.isNull(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO))) {
            dashboard.setNo(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO)));
        } else {
            dashboard.setNo(0);
        }

        if (!cursor.isNull(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO))) {
            dashboard.setNo(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO)));
        } else {
            dashboard.setNo(0);
        }
        return dashboard;
    }

    public static Dashboard getDashboard(Cursor cursor, int position, int selectedPosition) {
        cursor.moveToPosition(position);
        Dashboard dashboard = new Dashboard();
        dashboard.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_ID)));
        dashboard.setImgMenu(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_IMG_MENU)));
        dashboard.setImgDashboard(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_IMG_DASHBOARD)));
        dashboard.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_TITLE)));
        dashboard.setSelected(position == selectedPosition);
        if (!cursor.isNull(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO))) {
            dashboard.setNo(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO)));
        } else {
            dashboard.setNo(0);
        }
        return dashboard;
    }

    public static ArrayList<Dashboard> getArrayDashboard(Cursor cursor, int selectedPosition) {
        ArrayList<Dashboard> arrayList = new ArrayList<>();
        if (cursor != null) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                Dashboard dashboard = new Dashboard();
                dashboard.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_ID)));
                dashboard.setImgMenu(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_IMG_MENU)));
                dashboard.setImgDashboard(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_IMG_DASHBOARD)));
                dashboard.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_TITLE)));
                dashboard.setSelected(i == selectedPosition);
                if (!cursor.isNull(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO))) {
                    dashboard.setNo(cursor.getInt(cursor.getColumnIndex(DBHelper.DASHBOARD_COLUMN_NO)));
                } else {
                    dashboard.setNo(0);
                }
                arrayList.add(dashboard);
            }
            return arrayList;
        } else {
            return null;
        }
    }
}