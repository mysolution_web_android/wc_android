package com.applab.wcircle_pro.Menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.applab.wcircle_pro.Calendar.CalendarActivity;
import com.applab.wcircle_pro.Catalogue.CatalogueActivity;
import com.applab.wcircle_pro.Chat.ChatActivity;
import com.applab.wcircle_pro.Dashboard.Dashboard;
import com.applab.wcircle_pro.Download.DownloadActivity;
import com.applab.wcircle_pro.Drive.DriveActivity;
import com.applab.wcircle_pro.Employee.EmployeeActivity;
import com.applab.wcircle_pro.Event.EventActivity;
import com.applab.wcircle_pro.Favorite.FavoriteActivity;
import com.applab.wcircle_pro.Gallery.GalleryActivity;
import com.applab.wcircle_pro.Home.HttpHelper;
import com.applab.wcircle_pro.Leave.LeaveActivity;
import com.applab.wcircle_pro.News.NewsActivity;
import com.applab.wcircle_pro.Profile.ProfileActivity;
import com.applab.wcircle_pro.R;
import com.applab.wcircle_pro.Settings.SettingsActivity;
import com.applab.wcircle_pro.Utils.Utils;

/**
 * Created by user on 4/28/2015.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuViewHolder> {
    private LayoutInflater inflater;
    private Cursor cursor;
    private Context context;
    public DrawerLayout mDrawerLayout;
    public int selectedPosition;
    private String TAG = "DRAWER";


    public MenuAdapter(Context context, Cursor cursor, int selectedPosition, DrawerLayout drawerLayout) {
        this.inflater = LayoutInflater.from(context);
        this.cursor = cursor;
        this.context = context;
        this.selectedPosition = selectedPosition;
        this.mDrawerLayout = drawerLayout;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("MenuViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_row, parent, false);
        MenuViewHolder holder = new MenuViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        Dashboard current = Dashboard.getDashboard(cursor, position, selectedPosition);
        holder.title.setText(current.getTitle());
        if(!current.getSelected()){
            holder.title.setTextColor(ContextCompat.getColor(context, R.color.color_text_black));
            holder.rl.setBackgroundColor(ContextCompat.getColor(context, R.color.color_white));
            switch (current.getTitle()) {
                case "My Favourite":
                    holder.icon.setImageResource(R.mipmap.menu_my_favourite);
                    holder.title.setText(R.string.title_activity_my_favorite);
                    break;
                case "Calendar":
                    holder.icon.setImageResource(R.mipmap.menu_calendar);
                    holder.title.setText(R.string.title_activity_calendar);
                    break;
                case "News":
                    holder.icon.setImageResource(R.mipmap.menu_news);
                    holder.title.setText(R.string.title_activity_news);
                    break;
                case "Events":
                    holder.icon.setImageResource(R.mipmap.menu_events);
                    holder.title.setText(R.string.title_activity_event);
                    break;
                case "Messenger":
                    holder.icon.setImageResource(R.mipmap.menu_messenger);
                    holder.title.setText(R.string.title_activity_chat);
                    break;
                case "Staff Directory":
                    holder.icon.setImageResource(R.mipmap.menu_staff_directory);
                    holder.title.setText(R.string.title_activity_employee);
                    break;
                case "Leave":
                    holder.icon.setImageResource(R.mipmap.menu_leave);
                    holder.title.setText(R.string.title_activity_leave);
                    break;
                case "Download Centre":
                    holder.icon.setImageResource(R.mipmap.menu_download);
                    holder.title.setText(R.string.title_activity_download);
                    break;
                case "Media Gallery":
                    holder.icon.setImageResource(R.mipmap.menu_media);
                    holder.title.setText(R.string.title_activity_gallery);
                    break;
                case "Product Catalogue":
                    holder.icon.setImageResource(R.mipmap.menu_product_catalog);
                    holder.title.setText(R.string.title_activity_catalogue);
                    break;
                case "My Drive":
                    holder.icon.setImageResource(R.mipmap.menu_my_drive);
                    holder.title.setText(R.string.title_activity_drive);
                    break;
                case "My Profile":
                    holder.icon.setImageResource(R.mipmap.menu_my_profile);
                    holder.title.setText(R.string.title_activity_profile);
                    break;
                case "My Dashboard":
                    holder.icon.setImageResource(R.mipmap.menu_home);
                    holder.title.setText(R.string.title_activity_my_dashboard);
                    break;
                case "Settings":
                    holder.icon.setImageResource(R.mipmap.menu_setting);
                    holder.title.setText(R.string.title_activity_setting);
                    break;
                case "Log Out":
                    holder.icon.setImageResource(R.mipmap.menu_logout);
                    holder.title.setText(R.string.title_activity_log_out);
                    break;
            }
        }else{
            holder.title.setTextColor(ContextCompat.getColor(context, R.color.color_white));
            holder.rl.setBackgroundColor(ContextCompat.getColor(context, R.color.color_black));
            switch (current.getTitle()) {
                case "My Favourite":
                    holder.icon.setImageResource(R.mipmap.menu_my_favourite_white);
                    holder.title.setText(R.string.title_activity_my_favorite);
                    break;
                case "Calendar":
                    holder.icon.setImageResource(R.mipmap.menu_calendar_white);
                    holder.title.setText(R.string.title_activity_calendar);
                    break;
                case "News":
                    holder.icon.setImageResource(R.mipmap.menu_news_white);
                    holder.title.setText(R.string.title_activity_news);
                    break;
                case "Events":
                    holder.icon.setImageResource(R.mipmap.menu_events_white);
                    holder.title.setText(R.string.title_activity_event);
                    break;
                case "Messenger":
                    holder.icon.setImageResource(R.mipmap.menu_messenger_white);
                    holder.title.setText(R.string.title_activity_chat);
                    break;
                case "Staff Directory":
                    holder.icon.setImageResource(R.mipmap.menu_staff_directory_white);
                    holder.title.setText(R.string.title_activity_employee);
                    break;
                case "Leave":
                    holder.icon.setImageResource(R.mipmap.menu_leave_white);
                    holder.title.setText(R.string.title_activity_leave);
                    break;
                case "Download Centre":
                    holder.icon.setImageResource(R.mipmap.menu_download_white);
                    holder.title.setText(R.string.title_activity_download);
                    break;
                case "Media Gallery":
                    holder.icon.setImageResource(R.mipmap.menu_media_white);
                    holder.title.setText(R.string.title_activity_gallery);
                    break;
                case "Product Catalogue":
                    holder.icon.setImageResource(R.mipmap.menu_product_catalog_white);
                    holder.title.setText(R.string.title_activity_catalogue);
                    break;
                case "My Drive":
                    holder.icon.setImageResource(R.mipmap.menu_my_drive_white);
                    holder.title.setText(R.string.title_activity_drive);
                    break;
                case "My Profile":
                    holder.icon.setImageResource(R.mipmap.menu_my_profile_white);
                    holder.title.setText(R.string.title_activity_profile);
                    break;
                case "My Dashboard":
                    holder.icon.setImageResource(R.mipmap.menu_home_white);
                    holder.title.setText(R.string.title_activity_my_dashboard);
                    break;
                case "Settings":
                    holder.icon.setImageResource(R.mipmap.menu_setting_white);
                    holder.title.setText(R.string.title_activity_setting);
                    break;
                case "Log Out":
                    holder.icon.setImageResource(R.mipmap.menu_logout_white);
                    holder.title.setText(R.string.title_activity_log_out);
                    break;
            }
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 10);
        if (current.getNo() > 0) {
            holder.txtIcon.setVisibility(View.VISIBLE);
            if (current.getNo() > 9) {
                holder.txtIcon.setText("+9");
            } else {
                holder.txtIcon.setText(String.valueOf(current.getNo()));
            }
        } else {
            holder.txtIcon.setVisibility(View.GONE);
        }
        setWholeOnClickListener(position, holder.rl);
    }

    public void menuNavigation(int position) {
        if (selectedPosition == position) {
            mDrawerLayout = (DrawerLayout) ((Activity) context).findViewById(R.id.drawer_layout);
            mDrawerLayout.closeDrawers();
        } else {
            switch (position) {
                case 0:
                    Utils.clearPreviousActivity((Activity) context);
                    break;
                case 1:
                    context.startActivity(new Intent(context, FavoriteActivity.class));
                    ((Activity) context).finish();
                    break;
                case 2:
                    context.startActivity(new Intent(context, CalendarActivity.class));
                    ((Activity) context).finish();
                    break;
                case 3:
                    context.startActivity(new Intent(context, NewsActivity.class));
                    ((Activity) context).finish();
                    break;
                case 4:
                    context.startActivity(new Intent(context, EventActivity.class));
                    ((Activity) context).finish();
                    break;
                case 5:
                    context.startActivity(new Intent(context, ChatActivity.class));
                    ((Activity) context).finish();
                    break;
                case 6:
                    context.startActivity(new Intent(context, EmployeeActivity.class));
                    ((Activity) context).finish();
                    break;
                case 7:
                    context.startActivity(new Intent(context, LeaveActivity.class));
                    ((Activity) context).finish();
                    break;
                case 8:
                    context.startActivity(new Intent(context, DownloadActivity.class));
                    ((Activity) context).finish();
                    break;
                case 9:
                    context.startActivity(new Intent(context, GalleryActivity.class));
                    ((Activity) context).finish();
                    break;
                case 10:
                    context.startActivity(new Intent(context, CatalogueActivity.class));
                    ((Activity) context).finish();
                    break;
                case 11:
                    context.startActivity(new Intent(context, DriveActivity.class));
                    ((Activity) context).finish();
                    break;
                case 12:
                    context.startActivity(new Intent(context, ProfileActivity.class));
                    ((Activity) context).finish();
                    break;
                case 13:
                    context.startActivity(new Intent(context, SettingsActivity.class));
                    ((Activity) context).finish();
                    break;
                case 14:
                    HttpHelper.setLogOut(context);
                    break;
                default:
                    Toast.makeText(context, "onClick " + position, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setWholeOnClickListener(final int positon, final RelativeLayout whole) {
        whole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuNavigation(positon);
            }
        });

    }

    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.cursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.cursor;
        this.cursor = cursor;
        if (cursor != null) {
            android.os.Message msg = handler.obtainMessage();
            handler.handleMessage(msg);
        }
        return oldCursor;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            MenuAdapter.this.notifyDataSetChanged();
        }
    };
}
