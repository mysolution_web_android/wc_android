package com.applab.wcircle_pro.Menu;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.wcircle_pro.R;

/**
 * Created by user on 4/28/2015.
 */
class MenuViewHolder extends RecyclerView.ViewHolder {
    TextView title;
    ImageView icon;
    RelativeLayout rl;
    TextView txtIcon;
    View view;
    LinearLayout whole;

    public MenuViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.listText);
        icon = (ImageView) itemView.findViewById(R.id.listIcon);
        rl = (RelativeLayout) itemView.findViewById(R.id.rl);
        txtIcon = (TextView) itemView.findViewById(R.id.txtIcon);
        view = (View) itemView.findViewById(R.id.view);
        whole = (LinearLayout) itemView.findViewById(R.id.whole);
    }
}